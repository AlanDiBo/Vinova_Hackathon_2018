import React from 'react';
import {AppRegistry, StyleSheet, Text, View, NativeModules,NativeEventEmitter
} from 'react-native';
let subscription

class RecordVoice extends React.Component {
    componentWillMount() {
        // // const { ReactEventEmitter } = NativeModules;
        // const VideoEventEmitter = new NativeEventEmitter(NativeModules.ReactEventEmitter);
        // subscription = VideoEventEmitter.addListener(
        //     'SearchVideo',
        //     (reminder) => {
        //         var UnityViewControllerBase = NativeModules.UnityViewControllerBase;
        //         UnityViewControllerBase.addEvent("video_response",reminder);
        //         fetch('https://vinova-hackathon.herokuapp.com/videos/search?q=' + reminder.query)
        //             .then((responseJson) => {
        //                 UnityViewControllerBase.addEvent("video_response",JSON.stringify(responseJson))
        //             })
        //             .catch((error) => {
        //                 console.error(error);
        //             });
        //     }
        //
        // );
        var UnityViewControllerBase = NativeModules.UnityViewControllerBase;

        fetch('https://vinova-hackathon.herokuapp.com/videos/search?q=movie')
            .then((response) => response.json())
            .then((responseJson) => {
                let result = {
                    movies: responseJson
                }
                UnityViewControllerBase.addEvent("video_response",JSON.stringify(result))
            })
            .catch((error) => {
                console.error(error);
            });
    }
    render() {
        return (
            <View>
            <Text> RecordVoice </Text>
            </View>
                );
    }

    componentWillUnmount(){
        subscription.remove()
    }
}
// Module name
AppRegistry.registerComponent('RecordVoice', () => RecordVoice);