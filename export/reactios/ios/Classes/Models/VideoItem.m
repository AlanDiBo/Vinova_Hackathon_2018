//
//  VideoItem.m
//  Unity-iPhone
//
//  Created by Huynh Tan Ngan on 1/29/18.
//

#import <Foundation/Foundation.h>
struct VideoItem
{
    __unsafe_unretained NSString *title;
    __unsafe_unretained NSString *author;
    __unsafe_unretained NSString *url;
};
