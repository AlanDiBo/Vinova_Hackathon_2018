//
//  Header.h
//  Unity-iPhone
//
//  Created by Huynh Tan Ngan on 2/1/18.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface ReactEventEmitter : RCTEventEmitter <RCTBridgeModule>

@end
