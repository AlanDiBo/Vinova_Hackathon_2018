//
//  ReactEventEmitter.m
//  Unity-iPhone
//
//  Created by Huynh Tan Ngan on 2/1/18.
//

#import "ReactEventEmitter.h"

@implementation ReactEventEmitter

RCT_EXPORT_MODULE();

- (NSArray<NSString *> *)supportedEvents
{
    return @[@"SearchVideo"];
}

- (void)searchVideo:(NSString *)query
{
    [self sendEventWithName:@"SearchVideo" body:@{@"query": query}];
}

@end
