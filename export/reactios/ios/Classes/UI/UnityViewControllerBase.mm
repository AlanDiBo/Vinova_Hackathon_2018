#import "UnityViewControllerBase.h"
#import "UnityAppController.h"
#import "UnityAppController+ViewHandling.h"
#import "PluginBase/UnityViewControllerListener.h"
#import <React/RCTRootView.h>
#import <React/RCTViewManager.h>
#import "ReactEventEmitter.h"
#import "UnityInterface.h"

@implementation UnityViewControllerBase

extern "C"{
    
}

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(addEvent:(NSString *)name location:(NSString *)location)
{
//    NSData *data = [location dataUsingEncoding:NSUTF8StringEncoding];
    
//    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    //                              OR
//    id jsonRes = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    [self callUnityObject: "Panel" Method: "OnDataCallbackSuccess" Parameter:[location UTF8String]];
    
//    UnitySendMessage(<#const char *obj#>, <#const char *method#>, <#const char *msg#>)
//    UnitySendMessageobj: @"Panel" method: @"OnDataCallbackSuccess" msg :@"abc"]);
    NSLog(@"Pretending to create an event %@ at %@", name, location);
}
RCTRootView *rootView;

- (void)callUnityObject:(const char*)object Method:(const char*)method Parameter:(const char*)parameter {
    UnitySendMessage(object, method, parameter);
}
- (id)init
{
    if ((self = [super init]))
    {
    #if PLATFORM_IOS
        AddViewControllerDefaultRotationHandling([UnityViewControllerBase class]);
    #endif
    }

    return self;
}
- (void) onFetchMovie:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"FetchMovie"])
        NSLog (@"Successfully received the fetch movie notification!");
//    double delayInSeconds = 0.5;
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        //code to be executed on the main queue after delay
////        [rootView.bridge enqueueJSCall:@"ReactEventEmitter" method:@"SearchVideo" args:@[@"Thor"] completion:nil];
//        [[rootView.bridge moduleForClass:ReactEventEmitter.class] searchVideo:@"Thor"];
//    });

}


- (void)viewDidLoad
{

//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFetchMovie:) name:@"FetchMovie" object:nil];
//    [[NSNotificationCenter defaultCenter]
//     postNotificationName:@"FetchMovie"
//     object:nil];
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    AppController_SendUnityViewControllerNotification(kUnityViewWillLayoutSubviews);
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    AppController_SendUnityViewControllerNotification(kUnityViewDidLayoutSubviews);
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear: animated];
    AppController_SendUnityViewControllerNotification(kUnityViewDidDisappear);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear: animated];
    AppController_SendUnityViewControllerNotification(kUnityViewWillDisappear);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    AppController_SendUnityViewControllerNotification(kUnityViewDidAppear);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    AppController_SendUnityViewControllerNotification(kUnityViewWillAppear);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFetchMovie:) name:@"FetchMovie" object:nil];
    NSURL *jsCodeLocation = [NSURL URLWithString:@"http://192.168.2.104:8081/index.bundle?platform=ios"];
    RCTRootView *rootView =
    [[RCTRootView alloc] initWithBundleURL: jsCodeLocation
                                moduleName: @"RecordVoice"
                         initialProperties: nil
     //     @{
     //       @"scores" : @[
     //               @{
     //                   @"name" : @"Alex",
     //                   @"value": @"42"
     //                   },
     //               @{
     //                   @"name" : @"Joel",
     //                   @"value": @"10"
     //                   }
     //               ]
     //       }
                             launchOptions: nil];
    //    UIViewController *vc = [[UIViewController alloc] init];
    //    [vc.view addSubview:rootView];
    //    [[rootView.bridge moduleForClass:ReactEventEmitter.class] searchVideo:@"Movie"];
    //    [self presentViewController:vc animated:YES completion:nil];
    rootView.hidden = NO;
    [self.view addSubview:rootView];
}

@end

extern "C" void UnityNotifyAutoOrientationChange()
{
#if UNITY_SUPPORT_ROTATION
    [GetAppController() forceAutorotatingControllerToRefreshEnabledOrientationsIfNeeded];
#endif
}
