﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action
struct Action_t1264377477;
// WebSocketSharp.Server.WebSocketServiceManager
struct WebSocketServiceManager_t2425053;
// System.Text.StringBuilder
struct StringBuilder_t;
// WebSocketSharp.Net.WebHeaderCollection
struct WebHeaderCollection_t1205255311;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// WebSocketSharp.WebSocket
struct WebSocket_t62038747;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext
struct HttpListenerWebSocketContext_t3770402993;
// System.Func`1<System.Boolean>
struct Func_1_t3822001908;
// WebSocketSharp.WebSocketFrame
struct WebSocketFrame_t3926438742;
// System.Action`1<WebSocketSharp.WebSocketFrame>
struct Action_1_t4098906337;
// WebSocketSharp.Net.HttpStreamAsyncResult
struct HttpStreamAsyncResult_t890764446;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1
struct U3CdumpU3Ec__AnonStorey1_t3297801281;
// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext
struct TcpListenerWebSocketContext_t2376318492;
// WebSocketSharp.Net.HttpListenerContext
struct HttpListenerContext_t3723273891;
// WebSocketSharp.Server.HttpServer
struct HttpServer_t3049239368;
// WebSocketSharp.MessageEventArgs
struct MessageEventArgs_t2225057723;
// System.Action`4<WebSocketSharp.CloseEventArgs,System.Boolean,System.Boolean,System.Boolean>
struct Action_4_t765175268;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// WebSocketSharp.Net.Cookie
struct Cookie_t4203102285;
// System.IDisposable
struct IDisposable_t3640265483;
// System.Net.Sockets.TcpClient
struct TcpClient_t822906377;
// WebSocketSharp.Server.WebSocketServer
struct WebSocketServer_t2289661728;
// WebSocketSharp.Server.WebSocketSessionManager
struct WebSocketSessionManager_t336645657;
// System.Func`3<WebSocketSharp.Opcode,System.IO.Stream,System.Boolean>
struct Func_3_t2923582405;
// WebSocketSharp.PayloadData
struct PayloadData_t688932160;
// WebSocketSharp.Net.HttpListener
struct HttpListener_t2467819517;
// WebSocketSharp.Net.HttpConnection
struct HttpConnection_t4177287240;
// WebSocketSharp.Net.HttpListenerRequest
struct HttpListenerRequest_t2959552699;
// WebSocketSharp.Net.HttpListenerResponse
struct HttpListenerResponse_t2818529495;
// System.Security.Principal.IPrincipal
struct IPrincipal_t2343618843;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// WebSocketSharp.Net.HttpListenerAsyncResult
struct HttpListenerAsyncResult_t1598414744;
// System.Exception
struct Exception_t;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t451242010;
// System.Collections.Hashtable
struct Hashtable_t1853889766;
// System.Collections.Specialized.NameObjectCollectionBase/_Item
struct _Item_t2272350267;
// System.Collections.ArrayList
struct ArrayList_t2718874744;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t267601189;
// System.Collections.IComparer
struct IComparer_t1540313114;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t1318642398;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t1493878338;
// System.Collections.Generic.List`1<WebSocketSharp.Net.Cookie>
struct List_1_t1380209731;
// System.Comparison`1<WebSocketSharp.Net.Cookie>
struct Comparison_1_t3978033464;
// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerPrefix>
struct List_1_t2383042196;
// System.Net.IPEndPoint
struct IPEndPoint_t3791887218;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpListenerPrefix,WebSocketSharp.Net.HttpListener>
struct Dictionary_2_t1167433975;
// System.Net.Sockets.Socket
struct Socket_t1119025450;
// WebSocketSharp.Net.ServerSslConfiguration
struct ServerSslConfiguration_t3240511754;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection>
struct Dictionary_2_t1708533168;
// System.Collections.Generic.Dictionary`2<System.Net.IPEndPoint,WebSocketSharp.Net.EndPointListener>
struct Dictionary_2_t3259849252;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Version
struct Version_t3456873960;
// System.Uri
struct Uri_t100236324;
// System.Collections.Generic.Queue`1<System.Byte[]>
struct Queue_1_t3962907151;
// System.Collections.Generic.Queue`1<System.String>
struct Queue_1_t1693710183;
// System.Collections.Generic.Dictionary`2<System.String,System.Char>
struct Dictionary_2_t3419716769;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.IO.Stream
struct Stream_t1273022909;
// System.Text.Encoding
struct Encoding_t1523322056;
// WebSocketSharp.Net.CookieCollection
struct CookieCollection_t962330244;
// WebSocketSharp.Net.ResponseStream
struct ResponseStream_t2828822308;
// System.Action`1<System.Exception>
struct Action_1_t1609204844;
// System.Void
struct Void_t1185182177;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// WebSocketSharp.Logger
struct Logger_t4025333586;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;
// WebSocketSharp.HttpRequest
struct HttpRequest_t1778308387;
// System.IO.MemoryStream
struct MemoryStream_t94973147;
// System.Action`3<System.Byte[],System.Int32,System.Int32>
struct Action_3_t1284011258;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t4177511560;
// WebSocketSharp.Net.RequestStream
struct RequestStream_t1020063535;
// WebSocketSharp.Net.EndPointListener
struct EndPointListener_t884507598;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean>
struct Dictionary_2_t3280968592;
// System.Threading.Timer
struct Timer_t716671026;
// System.Threading.TimerCallback
struct TimerCallback_t1438585625;
// System.Func`2<WebSocketSharp.Net.HttpListenerRequest,WebSocketSharp.Net.AuthenticationSchemes>
struct Func_2_t787507147;
// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerContext>
struct List_1_t900381337;
// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext>
struct Dictionary_2_t2932675044;
// WebSocketSharp.Net.HttpListenerPrefixCollection
struct HttpListenerPrefixCollection_t702486717;
// System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential>
struct Func_2_t2697316325;
// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerAsyncResult>
struct List_1_t3070489486;
// System.Net.IPAddress
struct IPAddress_t241777590;
// System.Threading.Thread
struct Thread_t2300836069;
// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs>
struct EventHandler_1_t3222597438;
// System.Net.Sockets.TcpListener
struct TcpListener_t3499576757;
// WebSocketSharp.Net.AuthenticationChallenge
struct AuthenticationChallenge_t3244447305;
// WebSocketSharp.Net.WebSockets.WebSocketContext
struct WebSocketContext_t619421455;
// WebSocketSharp.Net.NetworkCredential
struct NetworkCredential_t1094796801;
// System.Threading.AutoResetEvent
struct AutoResetEvent_t1333520283;
// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String>
struct Func_2_t1203693676;
// System.Action`1<WebSocketSharp.MessageEventArgs>
struct Action_1_t2397525318;
// System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs>
struct Queue_1_t2071317217;
// WebSocketSharp.Net.ClientSslConfiguration
struct ClientSslConfiguration_t664129470;
// System.EventHandler`1<WebSocketSharp.CloseEventArgs>
struct EventHandler_1_t4095840750;
// System.EventHandler`1<WebSocketSharp.ErrorEventArgs>
struct EventHandler_1_t1344838444;
// System.EventHandler`1<WebSocketSharp.MessageEventArgs>
struct EventHandler_1_t149217156;
// System.EventHandler
struct EventHandler_t1348719766;
// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Server.WebSocketServiceHost>
struct Dictionary_2_t645715037;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Collections.Generic.List`1<WebSocketSharp.Net.Chunk>
struct List_1_t1758269811;
// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Server.IWebSocketSession>
struct Dictionary_2_t1549584620;
// System.Timers.Timer
struct Timer_t1767341190;
// System.Net.Security.LocalCertificateSelectionCallback
struct LocalCertificateSelectionCallback_t2354453884;
// System.Net.Security.RemoteCertificateValidationCallback
struct RemoteCertificateValidationCallback_t3014364904;
// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Net.HttpHeaderInfo>
struct Dictionary_2_t1847514588;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t3399372417;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t714049126;
// System.Func`3<WebSocketSharp.Net.CookieCollection,WebSocketSharp.Net.CookieCollection,System.Boolean>
struct Func_3_t567174798;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t2197129486;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t3450905854;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// System.Func`2<System.String,UniRx.IObservable`1<System.String>>
struct Func_2_t2070424348;
// System.Func`3<System.String,System.String,<>__AnonType0`2<System.String,System.String>>
struct Func_3_t3989337584;
// System.Action`1<<>__AnonType0`2<System.String,System.String>>
struct Action_1_t3625273254;
// System.Action`1<System.String[]>
struct Action_1_t1454256935;
// System.Action`1<System.Single>
struct Action_1_t1569734369;
// System.Action`1<UniRx.WWWErrorException>
struct Action_1_t1943680094;
// System.Func`2<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback,System.Boolean>
struct Func_2_t2219533162;
// System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>
struct Action_1_t2739833248;
// System.Action`1<UniRx.Unit>
struct Action_1_t3534717062;
// UniRx.Subject`1<System.String>
struct Subject_1_t1936107076;
// socket.io.WebSocketWrapper
struct WebSocketWrapper_t3330088300;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CBROADCASTASYNCU3EC__ANONSTOREY2_T1520948972_H
#define U3CBROADCASTASYNCU3EC__ANONSTOREY2_T1520948972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServiceManager/<BroadcastAsync>c__AnonStorey2
struct  U3CBroadcastAsyncU3Ec__AnonStorey2_t1520948972  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Server.WebSocketServiceManager/<BroadcastAsync>c__AnonStorey2::length
	int32_t ___length_0;
	// System.Action WebSocketSharp.Server.WebSocketServiceManager/<BroadcastAsync>c__AnonStorey2::completed
	Action_t1264377477 * ___completed_1;
	// WebSocketSharp.Server.WebSocketServiceManager WebSocketSharp.Server.WebSocketServiceManager/<BroadcastAsync>c__AnonStorey2::$this
	WebSocketServiceManager_t2425053 * ___U24this_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(U3CBroadcastAsyncU3Ec__AnonStorey2_t1520948972, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_completed_1() { return static_cast<int32_t>(offsetof(U3CBroadcastAsyncU3Ec__AnonStorey2_t1520948972, ___completed_1)); }
	inline Action_t1264377477 * get_completed_1() const { return ___completed_1; }
	inline Action_t1264377477 ** get_address_of_completed_1() { return &___completed_1; }
	inline void set_completed_1(Action_t1264377477 * value)
	{
		___completed_1 = value;
		Il2CppCodeGenWriteBarrier((&___completed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CBroadcastAsyncU3Ec__AnonStorey2_t1520948972, ___U24this_2)); }
	inline WebSocketServiceManager_t2425053 * get_U24this_2() const { return ___U24this_2; }
	inline WebSocketServiceManager_t2425053 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebSocketServiceManager_t2425053 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBROADCASTASYNCU3EC__ANONSTOREY2_T1520948972_H
#ifndef U3CTOSTRINGMULTIVALUEU3EC__ANONSTOREY0_T515054707_H
#define U3CTOSTRINGMULTIVALUEU3EC__ANONSTOREY0_T515054707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection/<ToStringMultiValue>c__AnonStorey0
struct  U3CToStringMultiValueU3Ec__AnonStorey0_t515054707  : public RuntimeObject
{
public:
	// System.Boolean WebSocketSharp.Net.WebHeaderCollection/<ToStringMultiValue>c__AnonStorey0::response
	bool ___response_0;
	// System.Text.StringBuilder WebSocketSharp.Net.WebHeaderCollection/<ToStringMultiValue>c__AnonStorey0::buff
	StringBuilder_t * ___buff_1;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.WebHeaderCollection/<ToStringMultiValue>c__AnonStorey0::$this
	WebHeaderCollection_t1205255311 * ___U24this_2;

public:
	inline static int32_t get_offset_of_response_0() { return static_cast<int32_t>(offsetof(U3CToStringMultiValueU3Ec__AnonStorey0_t515054707, ___response_0)); }
	inline bool get_response_0() const { return ___response_0; }
	inline bool* get_address_of_response_0() { return &___response_0; }
	inline void set_response_0(bool value)
	{
		___response_0 = value;
	}

	inline static int32_t get_offset_of_buff_1() { return static_cast<int32_t>(offsetof(U3CToStringMultiValueU3Ec__AnonStorey0_t515054707, ___buff_1)); }
	inline StringBuilder_t * get_buff_1() const { return ___buff_1; }
	inline StringBuilder_t ** get_address_of_buff_1() { return &___buff_1; }
	inline void set_buff_1(StringBuilder_t * value)
	{
		___buff_1 = value;
		Il2CppCodeGenWriteBarrier((&___buff_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CToStringMultiValueU3Ec__AnonStorey0_t515054707, ___U24this_2)); }
	inline WebHeaderCollection_t1205255311 * get_U24this_2() const { return ___U24this_2; }
	inline WebHeaderCollection_t1205255311 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebHeaderCollection_t1205255311 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTOSTRINGMULTIVALUEU3EC__ANONSTOREY0_T515054707_H
#ifndef U3CGETOBJECTDATAU3EC__ANONSTOREY1_T4038268818_H
#define U3CGETOBJECTDATAU3EC__ANONSTOREY1_T4038268818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1
struct  U3CGetObjectDataU3Ec__AnonStorey1_t4038268818  : public RuntimeObject
{
public:
	// System.Runtime.Serialization.SerializationInfo WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1::serializationInfo
	SerializationInfo_t950877179 * ___serializationInfo_0;
	// System.Int32 WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1::cnt
	int32_t ___cnt_1;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.WebHeaderCollection/<GetObjectData>c__AnonStorey1::$this
	WebHeaderCollection_t1205255311 * ___U24this_2;

public:
	inline static int32_t get_offset_of_serializationInfo_0() { return static_cast<int32_t>(offsetof(U3CGetObjectDataU3Ec__AnonStorey1_t4038268818, ___serializationInfo_0)); }
	inline SerializationInfo_t950877179 * get_serializationInfo_0() const { return ___serializationInfo_0; }
	inline SerializationInfo_t950877179 ** get_address_of_serializationInfo_0() { return &___serializationInfo_0; }
	inline void set_serializationInfo_0(SerializationInfo_t950877179 * value)
	{
		___serializationInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___serializationInfo_0), value);
	}

	inline static int32_t get_offset_of_cnt_1() { return static_cast<int32_t>(offsetof(U3CGetObjectDataU3Ec__AnonStorey1_t4038268818, ___cnt_1)); }
	inline int32_t get_cnt_1() const { return ___cnt_1; }
	inline int32_t* get_address_of_cnt_1() { return &___cnt_1; }
	inline void set_cnt_1(int32_t value)
	{
		___cnt_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetObjectDataU3Ec__AnonStorey1_t4038268818, ___U24this_2)); }
	inline WebHeaderCollection_t1205255311 * get_U24this_2() const { return ___U24this_2; }
	inline WebHeaderCollection_t1205255311 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebHeaderCollection_t1205255311 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETOBJECTDATAU3EC__ANONSTOREY1_T4038268818_H
#ifndef U3CTOSTRINGU3EC__ANONSTOREY2_T1832203497_H
#define U3CTOSTRINGU3EC__ANONSTOREY2_T1832203497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2
struct  U3CToStringU3Ec__AnonStorey2_t1832203497  : public RuntimeObject
{
public:
	// System.Text.StringBuilder WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2::buff
	StringBuilder_t * ___buff_0;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.WebHeaderCollection/<ToString>c__AnonStorey2::$this
	WebHeaderCollection_t1205255311 * ___U24this_1;

public:
	inline static int32_t get_offset_of_buff_0() { return static_cast<int32_t>(offsetof(U3CToStringU3Ec__AnonStorey2_t1832203497, ___buff_0)); }
	inline StringBuilder_t * get_buff_0() const { return ___buff_0; }
	inline StringBuilder_t ** get_address_of_buff_0() { return &___buff_0; }
	inline void set_buff_0(StringBuilder_t * value)
	{
		___buff_0 = value;
		Il2CppCodeGenWriteBarrier((&___buff_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CToStringU3Ec__AnonStorey2_t1832203497, ___U24this_1)); }
	inline WebHeaderCollection_t1205255311 * get_U24this_1() const { return ___U24this_1; }
	inline WebHeaderCollection_t1205255311 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebHeaderCollection_t1205255311 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTOSTRINGU3EC__ANONSTOREY2_T1832203497_H
#ifndef U3CSENDASYNCU3EC__ANONSTOREY9_T2988803146_H
#define U3CSENDASYNCU3EC__ANONSTOREY9_T2988803146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey9
struct  U3CSendAsyncU3Ec__AnonStorey9_t2988803146  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey9::length
	int32_t ___length_0;
	// System.Action`1<System.Boolean> WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey9::completed
	Action_1_t269755560 * ___completed_1;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<SendAsync>c__AnonStorey9::$this
	WebSocket_t62038747 * ___U24this_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ec__AnonStorey9_t2988803146, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_completed_1() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ec__AnonStorey9_t2988803146, ___completed_1)); }
	inline Action_1_t269755560 * get_completed_1() const { return ___completed_1; }
	inline Action_1_t269755560 ** get_address_of_completed_1() { return &___completed_1; }
	inline void set_completed_1(Action_1_t269755560 * value)
	{
		___completed_1 = value;
		Il2CppCodeGenWriteBarrier((&___completed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ec__AnonStorey9_t2988803146, ___U24this_2)); }
	inline WebSocket_t62038747 * get_U24this_2() const { return ___U24this_2; }
	inline WebSocket_t62038747 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebSocket_t62038747 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDASYNCU3EC__ANONSTOREY9_T2988803146_H
#ifndef U3CU3EC__ITERATOR0_T2500134981_H
#define U3CU3EC__ITERATOR0_T2500134981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t2500134981  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::<protocols>__0
	String_t* ___U3CprotocolsU3E__0_0;
	// System.String[] WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::$locvar0
	StringU5BU5D_t1281789340* ___U24locvar0_1;
	// System.Int32 WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// System.String WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::<protocol>__1
	String_t* ___U3CprotocolU3E__1_3;
	// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::$this
	HttpListenerWebSocketContext_t3770402993 * ___U24this_4;
	// System.String WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::$current
	String_t* ___U24current_5;
	// System.Boolean WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext/<>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CprotocolsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2500134981, ___U3CprotocolsU3E__0_0)); }
	inline String_t* get_U3CprotocolsU3E__0_0() const { return ___U3CprotocolsU3E__0_0; }
	inline String_t** get_address_of_U3CprotocolsU3E__0_0() { return &___U3CprotocolsU3E__0_0; }
	inline void set_U3CprotocolsU3E__0_0(String_t* value)
	{
		___U3CprotocolsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprotocolsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2500134981, ___U24locvar0_1)); }
	inline StringU5BU5D_t1281789340* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline StringU5BU5D_t1281789340** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(StringU5BU5D_t1281789340* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2500134981, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CprotocolU3E__1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2500134981, ___U3CprotocolU3E__1_3)); }
	inline String_t* get_U3CprotocolU3E__1_3() const { return ___U3CprotocolU3E__1_3; }
	inline String_t** get_address_of_U3CprotocolU3E__1_3() { return &___U3CprotocolU3E__1_3; }
	inline void set_U3CprotocolU3E__1_3(String_t* value)
	{
		___U3CprotocolU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprotocolU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2500134981, ___U24this_4)); }
	inline HttpListenerWebSocketContext_t3770402993 * get_U24this_4() const { return ___U24this_4; }
	inline HttpListenerWebSocketContext_t3770402993 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(HttpListenerWebSocketContext_t3770402993 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2500134981, ___U24current_5)); }
	inline String_t* get_U24current_5() const { return ___U24current_5; }
	inline String_t** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(String_t* value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2500134981, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2500134981, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T2500134981_H
#ifndef U3CCONNECTASYNCU3EC__ANONSTOREY8_T2980504460_H
#define U3CCONNECTASYNCU3EC__ANONSTOREY8_T2980504460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey8
struct  U3CConnectAsyncU3Ec__AnonStorey8_t2980504460  : public RuntimeObject
{
public:
	// System.Func`1<System.Boolean> WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey8::connector
	Func_1_t3822001908 * ___connector_0;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<ConnectAsync>c__AnonStorey8::$this
	WebSocket_t62038747 * ___U24this_1;

public:
	inline static int32_t get_offset_of_connector_0() { return static_cast<int32_t>(offsetof(U3CConnectAsyncU3Ec__AnonStorey8_t2980504460, ___connector_0)); }
	inline Func_1_t3822001908 * get_connector_0() const { return ___connector_0; }
	inline Func_1_t3822001908 ** get_address_of_connector_0() { return &___connector_0; }
	inline void set_connector_0(Func_1_t3822001908 * value)
	{
		___connector_0 = value;
		Il2CppCodeGenWriteBarrier((&___connector_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CConnectAsyncU3Ec__AnonStorey8_t2980504460, ___U24this_1)); }
	inline WebSocket_t62038747 * get_U24this_1() const { return ___U24this_1; }
	inline WebSocket_t62038747 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebSocket_t62038747 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONNECTASYNCU3EC__ANONSTOREY8_T2980504460_H
#ifndef U3CDUMPU3EC__ANONSTOREY1_T3297801281_H
#define U3CDUMPU3EC__ANONSTOREY1_T3297801281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1
struct  U3CdumpU3Ec__AnonStorey1_t3297801281  : public RuntimeObject
{
public:
	// System.Text.StringBuilder WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1::output
	StringBuilder_t * ___output_0;
	// System.String WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1::lineFmt
	String_t* ___lineFmt_1;

public:
	inline static int32_t get_offset_of_output_0() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey1_t3297801281, ___output_0)); }
	inline StringBuilder_t * get_output_0() const { return ___output_0; }
	inline StringBuilder_t ** get_address_of_output_0() { return &___output_0; }
	inline void set_output_0(StringBuilder_t * value)
	{
		___output_0 = value;
		Il2CppCodeGenWriteBarrier((&___output_0), value);
	}

	inline static int32_t get_offset_of_lineFmt_1() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey1_t3297801281, ___lineFmt_1)); }
	inline String_t* get_lineFmt_1() const { return ___lineFmt_1; }
	inline String_t** get_address_of_lineFmt_1() { return &___lineFmt_1; }
	inline void set_lineFmt_1(String_t* value)
	{
		___lineFmt_1 = value;
		Il2CppCodeGenWriteBarrier((&___lineFmt_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDUMPU3EC__ANONSTOREY1_T3297801281_H
#ifndef U3CREADPAYLOADDATAASYNCU3EC__ANONSTOREY6_T2972632382_H
#define U3CREADPAYLOADDATAASYNCU3EC__ANONSTOREY6_T2972632382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6
struct  U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t2972632382  : public RuntimeObject
{
public:
	// System.Int64 WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6::llen
	int64_t ___llen_0;
	// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6::frame
	WebSocketFrame_t3926438742 * ___frame_1;
	// System.Action`1<WebSocketSharp.WebSocketFrame> WebSocketSharp.WebSocketFrame/<readPayloadDataAsync>c__AnonStorey6::completed
	Action_1_t4098906337 * ___completed_2;

public:
	inline static int32_t get_offset_of_llen_0() { return static_cast<int32_t>(offsetof(U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t2972632382, ___llen_0)); }
	inline int64_t get_llen_0() const { return ___llen_0; }
	inline int64_t* get_address_of_llen_0() { return &___llen_0; }
	inline void set_llen_0(int64_t value)
	{
		___llen_0 = value;
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t2972632382, ___frame_1)); }
	inline WebSocketFrame_t3926438742 * get_frame_1() const { return ___frame_1; }
	inline WebSocketFrame_t3926438742 ** get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(WebSocketFrame_t3926438742 * value)
	{
		___frame_1 = value;
		Il2CppCodeGenWriteBarrier((&___frame_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t2972632382, ___completed_2)); }
	inline Action_1_t4098906337 * get_completed_2() const { return ___completed_2; }
	inline Action_1_t4098906337 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_1_t4098906337 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADPAYLOADDATAASYNCU3EC__ANONSTOREY6_T2972632382_H
#ifndef READBUFFERSTATE_T3629953814_H
#define READBUFFERSTATE_T3629953814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ReadBufferState
struct  ReadBufferState_t3629953814  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.HttpStreamAsyncResult WebSocketSharp.Net.ReadBufferState::_asyncResult
	HttpStreamAsyncResult_t890764446 * ____asyncResult_0;
	// System.Byte[] WebSocketSharp.Net.ReadBufferState::_buffer
	ByteU5BU5D_t4116647657* ____buffer_1;
	// System.Int32 WebSocketSharp.Net.ReadBufferState::_count
	int32_t ____count_2;
	// System.Int32 WebSocketSharp.Net.ReadBufferState::_initialCount
	int32_t ____initialCount_3;
	// System.Int32 WebSocketSharp.Net.ReadBufferState::_offset
	int32_t ____offset_4;

public:
	inline static int32_t get_offset_of__asyncResult_0() { return static_cast<int32_t>(offsetof(ReadBufferState_t3629953814, ____asyncResult_0)); }
	inline HttpStreamAsyncResult_t890764446 * get__asyncResult_0() const { return ____asyncResult_0; }
	inline HttpStreamAsyncResult_t890764446 ** get_address_of__asyncResult_0() { return &____asyncResult_0; }
	inline void set__asyncResult_0(HttpStreamAsyncResult_t890764446 * value)
	{
		____asyncResult_0 = value;
		Il2CppCodeGenWriteBarrier((&____asyncResult_0), value);
	}

	inline static int32_t get_offset_of__buffer_1() { return static_cast<int32_t>(offsetof(ReadBufferState_t3629953814, ____buffer_1)); }
	inline ByteU5BU5D_t4116647657* get__buffer_1() const { return ____buffer_1; }
	inline ByteU5BU5D_t4116647657** get_address_of__buffer_1() { return &____buffer_1; }
	inline void set__buffer_1(ByteU5BU5D_t4116647657* value)
	{
		____buffer_1 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_1), value);
	}

	inline static int32_t get_offset_of__count_2() { return static_cast<int32_t>(offsetof(ReadBufferState_t3629953814, ____count_2)); }
	inline int32_t get__count_2() const { return ____count_2; }
	inline int32_t* get_address_of__count_2() { return &____count_2; }
	inline void set__count_2(int32_t value)
	{
		____count_2 = value;
	}

	inline static int32_t get_offset_of__initialCount_3() { return static_cast<int32_t>(offsetof(ReadBufferState_t3629953814, ____initialCount_3)); }
	inline int32_t get__initialCount_3() const { return ____initialCount_3; }
	inline int32_t* get_address_of__initialCount_3() { return &____initialCount_3; }
	inline void set__initialCount_3(int32_t value)
	{
		____initialCount_3 = value;
	}

	inline static int32_t get_offset_of__offset_4() { return static_cast<int32_t>(offsetof(ReadBufferState_t3629953814, ____offset_4)); }
	inline int32_t get__offset_4() const { return ____offset_4; }
	inline int32_t* get_address_of__offset_4() { return &____offset_4; }
	inline void set__offset_4(int32_t value)
	{
		____offset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READBUFFERSTATE_T3629953814_H
#ifndef U3CREADMASKINGKEYASYNCU3EC__ANONSTOREY5_T3831656906_H
#define U3CREADMASKINGKEYASYNCU3EC__ANONSTOREY5_T3831656906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5
struct  U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3831656906  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5::len
	int32_t ___len_0;
	// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5::frame
	WebSocketFrame_t3926438742 * ___frame_1;
	// System.Action`1<WebSocketSharp.WebSocketFrame> WebSocketSharp.WebSocketFrame/<readMaskingKeyAsync>c__AnonStorey5::completed
	Action_1_t4098906337 * ___completed_2;

public:
	inline static int32_t get_offset_of_len_0() { return static_cast<int32_t>(offsetof(U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3831656906, ___len_0)); }
	inline int32_t get_len_0() const { return ___len_0; }
	inline int32_t* get_address_of_len_0() { return &___len_0; }
	inline void set_len_0(int32_t value)
	{
		___len_0 = value;
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3831656906, ___frame_1)); }
	inline WebSocketFrame_t3926438742 * get_frame_1() const { return ___frame_1; }
	inline WebSocketFrame_t3926438742 ** get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(WebSocketFrame_t3926438742 * value)
	{
		___frame_1 = value;
		Il2CppCodeGenWriteBarrier((&___frame_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3831656906, ___completed_2)); }
	inline Action_1_t4098906337 * get_completed_2() const { return ___completed_2; }
	inline Action_1_t4098906337 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_1_t4098906337 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADMASKINGKEYASYNCU3EC__ANONSTOREY5_T3831656906_H
#ifndef U3CREADHEADERASYNCU3EC__ANONSTOREY4_T243395687_H
#define U3CREADHEADERASYNCU3EC__ANONSTOREY4_T243395687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<readHeaderAsync>c__AnonStorey4
struct  U3CreadHeaderAsyncU3Ec__AnonStorey4_t243395687  : public RuntimeObject
{
public:
	// System.Action`1<WebSocketSharp.WebSocketFrame> WebSocketSharp.WebSocketFrame/<readHeaderAsync>c__AnonStorey4::completed
	Action_1_t4098906337 * ___completed_0;

public:
	inline static int32_t get_offset_of_completed_0() { return static_cast<int32_t>(offsetof(U3CreadHeaderAsyncU3Ec__AnonStorey4_t243395687, ___completed_0)); }
	inline Action_1_t4098906337 * get_completed_0() const { return ___completed_0; }
	inline Action_1_t4098906337 ** get_address_of_completed_0() { return &___completed_0; }
	inline void set_completed_0(Action_1_t4098906337 * value)
	{
		___completed_0 = value;
		Il2CppCodeGenWriteBarrier((&___completed_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADHEADERASYNCU3EC__ANONSTOREY4_T243395687_H
#ifndef U3CREADEXTENDEDPAYLOADLENGTHASYNCU3EC__ANONSTOREY3_T74541281_H
#define U3CREADEXTENDEDPAYLOADLENGTHASYNCU3EC__ANONSTOREY3_T74541281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3
struct  U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t74541281  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3::len
	int32_t ___len_0;
	// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3::frame
	WebSocketFrame_t3926438742 * ___frame_1;
	// System.Action`1<WebSocketSharp.WebSocketFrame> WebSocketSharp.WebSocketFrame/<readExtendedPayloadLengthAsync>c__AnonStorey3::completed
	Action_1_t4098906337 * ___completed_2;

public:
	inline static int32_t get_offset_of_len_0() { return static_cast<int32_t>(offsetof(U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t74541281, ___len_0)); }
	inline int32_t get_len_0() const { return ___len_0; }
	inline int32_t* get_address_of_len_0() { return &___len_0; }
	inline void set_len_0(int32_t value)
	{
		___len_0 = value;
	}

	inline static int32_t get_offset_of_frame_1() { return static_cast<int32_t>(offsetof(U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t74541281, ___frame_1)); }
	inline WebSocketFrame_t3926438742 * get_frame_1() const { return ___frame_1; }
	inline WebSocketFrame_t3926438742 ** get_address_of_frame_1() { return &___frame_1; }
	inline void set_frame_1(WebSocketFrame_t3926438742 * value)
	{
		___frame_1 = value;
		Il2CppCodeGenWriteBarrier((&___frame_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t74541281, ___completed_2)); }
	inline Action_1_t4098906337 * get_completed_2() const { return ___completed_2; }
	inline Action_1_t4098906337 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_1_t4098906337 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADEXTENDEDPAYLOADLENGTHASYNCU3EC__ANONSTOREY3_T74541281_H
#ifndef U3CDUMPU3EC__ANONSTOREY2_T2749028009_H
#define U3CDUMPU3EC__ANONSTOREY2_T2749028009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2
struct  U3CdumpU3Ec__AnonStorey2_t2749028009  : public RuntimeObject
{
public:
	// System.Int64 WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2::lineCnt
	int64_t ___lineCnt_0;
	// WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1 WebSocketSharp.WebSocketFrame/<dump>c__AnonStorey1/<dump>c__AnonStorey2::<>f__ref$1
	U3CdumpU3Ec__AnonStorey1_t3297801281 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_lineCnt_0() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey2_t2749028009, ___lineCnt_0)); }
	inline int64_t get_lineCnt_0() const { return ___lineCnt_0; }
	inline int64_t* get_address_of_lineCnt_0() { return &___lineCnt_0; }
	inline void set_lineCnt_0(int64_t value)
	{
		___lineCnt_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CdumpU3Ec__AnonStorey2_t2749028009, ___U3CU3Ef__refU241_1)); }
	inline U3CdumpU3Ec__AnonStorey1_t3297801281 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CdumpU3Ec__AnonStorey1_t3297801281 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CdumpU3Ec__AnonStorey1_t3297801281 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDUMPU3EC__ANONSTOREY2_T2749028009_H
#ifndef U3CU3EC__ITERATOR0_T3615578217_H
#define U3CU3EC__ITERATOR0_T3615578217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t3615578217  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::<protocols>__0
	String_t* ___U3CprotocolsU3E__0_0;
	// System.String[] WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::$locvar0
	StringU5BU5D_t1281789340* ___U24locvar0_1;
	// System.Int32 WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// System.String WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::<protocol>__1
	String_t* ___U3CprotocolU3E__1_3;
	// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::$this
	TcpListenerWebSocketContext_t2376318492 * ___U24this_4;
	// System.String WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::$current
	String_t* ___U24current_5;
	// System.Boolean WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CprotocolsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3615578217, ___U3CprotocolsU3E__0_0)); }
	inline String_t* get_U3CprotocolsU3E__0_0() const { return ___U3CprotocolsU3E__0_0; }
	inline String_t** get_address_of_U3CprotocolsU3E__0_0() { return &___U3CprotocolsU3E__0_0; }
	inline void set_U3CprotocolsU3E__0_0(String_t* value)
	{
		___U3CprotocolsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprotocolsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3615578217, ___U24locvar0_1)); }
	inline StringU5BU5D_t1281789340* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline StringU5BU5D_t1281789340** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(StringU5BU5D_t1281789340* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3615578217, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CprotocolU3E__1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3615578217, ___U3CprotocolU3E__1_3)); }
	inline String_t* get_U3CprotocolU3E__1_3() const { return ___U3CprotocolU3E__1_3; }
	inline String_t** get_address_of_U3CprotocolU3E__1_3() { return &___U3CprotocolU3E__1_3; }
	inline void set_U3CprotocolU3E__1_3(String_t* value)
	{
		___U3CprotocolU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprotocolU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3615578217, ___U24this_4)); }
	inline TcpListenerWebSocketContext_t2376318492 * get_U24this_4() const { return ___U24this_4; }
	inline TcpListenerWebSocketContext_t2376318492 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(TcpListenerWebSocketContext_t2376318492 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3615578217, ___U24current_5)); }
	inline String_t* get_U24current_5() const { return ___U24current_5; }
	inline String_t** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(String_t* value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3615578217, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t3615578217, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T3615578217_H
#ifndef U3CRECEIVEREQUESTU3EC__ANONSTOREY0_T2532472181_H
#define U3CRECEIVEREQUESTU3EC__ANONSTOREY0_T2532472181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.HttpServer/<receiveRequest>c__AnonStorey0
struct  U3CreceiveRequestU3Ec__AnonStorey0_t2532472181  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Server.HttpServer/<receiveRequest>c__AnonStorey0::ctx
	HttpListenerContext_t3723273891 * ___ctx_0;
	// WebSocketSharp.Server.HttpServer WebSocketSharp.Server.HttpServer/<receiveRequest>c__AnonStorey0::$this
	HttpServer_t3049239368 * ___U24this_1;

public:
	inline static int32_t get_offset_of_ctx_0() { return static_cast<int32_t>(offsetof(U3CreceiveRequestU3Ec__AnonStorey0_t2532472181, ___ctx_0)); }
	inline HttpListenerContext_t3723273891 * get_ctx_0() const { return ___ctx_0; }
	inline HttpListenerContext_t3723273891 ** get_address_of_ctx_0() { return &___ctx_0; }
	inline void set_ctx_0(HttpListenerContext_t3723273891 * value)
	{
		___ctx_0 = value;
		Il2CppCodeGenWriteBarrier((&___ctx_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CreceiveRequestU3Ec__AnonStorey0_t2532472181, ___U24this_1)); }
	inline HttpServer_t3049239368 * get_U24this_1() const { return ___U24this_1; }
	inline HttpServer_t3049239368 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(HttpServer_t3049239368 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECEIVEREQUESTU3EC__ANONSTOREY0_T2532472181_H
#ifndef U3CMESSAGESU3EC__ANONSTOREY2_T330977561_H
#define U3CMESSAGESU3EC__ANONSTOREY2_T330977561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<messages>c__AnonStorey2
struct  U3CmessagesU3Ec__AnonStorey2_t330977561  : public RuntimeObject
{
public:
	// WebSocketSharp.MessageEventArgs WebSocketSharp.WebSocket/<messages>c__AnonStorey2::e
	MessageEventArgs_t2225057723 * ___e_0;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<messages>c__AnonStorey2::$this
	WebSocket_t62038747 * ___U24this_1;

public:
	inline static int32_t get_offset_of_e_0() { return static_cast<int32_t>(offsetof(U3CmessagesU3Ec__AnonStorey2_t330977561, ___e_0)); }
	inline MessageEventArgs_t2225057723 * get_e_0() const { return ___e_0; }
	inline MessageEventArgs_t2225057723 ** get_address_of_e_0() { return &___e_0; }
	inline void set_e_0(MessageEventArgs_t2225057723 * value)
	{
		___e_0 = value;
		Il2CppCodeGenWriteBarrier((&___e_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CmessagesU3Ec__AnonStorey2_t330977561, ___U24this_1)); }
	inline WebSocket_t62038747 * get_U24this_1() const { return ___U24this_1; }
	inline WebSocket_t62038747 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebSocket_t62038747 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMESSAGESU3EC__ANONSTOREY2_T330977561_H
#ifndef U3CCLOSEASYNCU3EC__ANONSTOREY1_T2051387318_H
#define U3CCLOSEASYNCU3EC__ANONSTOREY1_T2051387318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<closeAsync>c__AnonStorey1
struct  U3CcloseAsyncU3Ec__AnonStorey1_t2051387318  : public RuntimeObject
{
public:
	// System.Action`4<WebSocketSharp.CloseEventArgs,System.Boolean,System.Boolean,System.Boolean> WebSocketSharp.WebSocket/<closeAsync>c__AnonStorey1::closer
	Action_4_t765175268 * ___closer_0;

public:
	inline static int32_t get_offset_of_closer_0() { return static_cast<int32_t>(offsetof(U3CcloseAsyncU3Ec__AnonStorey1_t2051387318, ___closer_0)); }
	inline Action_4_t765175268 * get_closer_0() const { return ___closer_0; }
	inline Action_4_t765175268 ** get_address_of_closer_0() { return &___closer_0; }
	inline void set_closer_0(Action_4_t765175268 * value)
	{
		___closer_0 = value;
		Il2CppCodeGenWriteBarrier((&___closer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLOSEASYNCU3EC__ANONSTOREY1_T2051387318_H
#ifndef U3CU3EC__ITERATOR0_T2887482940_H
#define U3CU3EC__ITERATOR0_T2887482940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t2887482940  : public RuntimeObject
{
public:
	// System.Object WebSocketSharp.WebSocket/<>c__Iterator0::$locvar0
	RuntimeObject * ___U24locvar0_0;
	// System.Collections.IEnumerator WebSocketSharp.WebSocket/<>c__Iterator0::$locvar1
	RuntimeObject* ___U24locvar1_1;
	// WebSocketSharp.Net.Cookie WebSocketSharp.WebSocket/<>c__Iterator0::<cookie>__1
	Cookie_t4203102285 * ___U3CcookieU3E__1_2;
	// System.IDisposable WebSocketSharp.WebSocket/<>c__Iterator0::$locvar2
	RuntimeObject* ___U24locvar2_3;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<>c__Iterator0::$this
	WebSocket_t62038747 * ___U24this_4;
	// WebSocketSharp.Net.Cookie WebSocketSharp.WebSocket/<>c__Iterator0::$current
	Cookie_t4203102285 * ___U24current_5;
	// System.Boolean WebSocketSharp.WebSocket/<>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 WebSocketSharp.WebSocket/<>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2887482940, ___U24locvar0_0)); }
	inline RuntimeObject * get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline RuntimeObject ** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(RuntimeObject * value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2887482940, ___U24locvar1_1)); }
	inline RuntimeObject* get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline RuntimeObject** get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(RuntimeObject* value)
	{
		___U24locvar1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_1), value);
	}

	inline static int32_t get_offset_of_U3CcookieU3E__1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2887482940, ___U3CcookieU3E__1_2)); }
	inline Cookie_t4203102285 * get_U3CcookieU3E__1_2() const { return ___U3CcookieU3E__1_2; }
	inline Cookie_t4203102285 ** get_address_of_U3CcookieU3E__1_2() { return &___U3CcookieU3E__1_2; }
	inline void set_U3CcookieU3E__1_2(Cookie_t4203102285 * value)
	{
		___U3CcookieU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcookieU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24locvar2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2887482940, ___U24locvar2_3)); }
	inline RuntimeObject* get_U24locvar2_3() const { return ___U24locvar2_3; }
	inline RuntimeObject** get_address_of_U24locvar2_3() { return &___U24locvar2_3; }
	inline void set_U24locvar2_3(RuntimeObject* value)
	{
		___U24locvar2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar2_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2887482940, ___U24this_4)); }
	inline WebSocket_t62038747 * get_U24this_4() const { return ___U24this_4; }
	inline WebSocket_t62038747 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(WebSocket_t62038747 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2887482940, ___U24current_5)); }
	inline Cookie_t4203102285 * get_U24current_5() const { return ___U24current_5; }
	inline Cookie_t4203102285 ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Cookie_t4203102285 * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2887482940, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t2887482940, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T2887482940_H
#ifndef U3CRECEIVEREQUESTU3EC__ANONSTOREY0_T3816822753_H
#define U3CRECEIVEREQUESTU3EC__ANONSTOREY0_T3816822753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServer/<receiveRequest>c__AnonStorey0
struct  U3CreceiveRequestU3Ec__AnonStorey0_t3816822753  : public RuntimeObject
{
public:
	// System.Net.Sockets.TcpClient WebSocketSharp.Server.WebSocketServer/<receiveRequest>c__AnonStorey0::cl
	TcpClient_t822906377 * ___cl_0;
	// WebSocketSharp.Server.WebSocketServer WebSocketSharp.Server.WebSocketServer/<receiveRequest>c__AnonStorey0::$this
	WebSocketServer_t2289661728 * ___U24this_1;

public:
	inline static int32_t get_offset_of_cl_0() { return static_cast<int32_t>(offsetof(U3CreceiveRequestU3Ec__AnonStorey0_t3816822753, ___cl_0)); }
	inline TcpClient_t822906377 * get_cl_0() const { return ___cl_0; }
	inline TcpClient_t822906377 ** get_address_of_cl_0() { return &___cl_0; }
	inline void set_cl_0(TcpClient_t822906377 * value)
	{
		___cl_0 = value;
		Il2CppCodeGenWriteBarrier((&___cl_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CreceiveRequestU3Ec__AnonStorey0_t3816822753, ___U24this_1)); }
	inline WebSocketServer_t2289661728 * get_U24this_1() const { return ___U24this_1; }
	inline WebSocketServer_t2289661728 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebSocketServer_t2289661728 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECEIVEREQUESTU3EC__ANONSTOREY0_T3816822753_H
#ifndef WEBSOCKETSERVICEHOST_T860458738_H
#define WEBSOCKETSERVICEHOST_T860458738_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServiceHost
struct  WebSocketServiceHost_t860458738  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSERVICEHOST_T860458738_H
#ifndef U3CBROADCASTASYNCU3EC__ANONSTOREY4_T1910876960_H
#define U3CBROADCASTASYNCU3EC__ANONSTOREY4_T1910876960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketSessionManager/<BroadcastAsync>c__AnonStorey4
struct  U3CBroadcastAsyncU3Ec__AnonStorey4_t1910876960  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Server.WebSocketSessionManager/<BroadcastAsync>c__AnonStorey4::length
	int32_t ___length_0;
	// System.Action WebSocketSharp.Server.WebSocketSessionManager/<BroadcastAsync>c__AnonStorey4::completed
	Action_t1264377477 * ___completed_1;
	// WebSocketSharp.Server.WebSocketSessionManager WebSocketSharp.Server.WebSocketSessionManager/<BroadcastAsync>c__AnonStorey4::$this
	WebSocketSessionManager_t336645657 * ___U24this_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(U3CBroadcastAsyncU3Ec__AnonStorey4_t1910876960, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_completed_1() { return static_cast<int32_t>(offsetof(U3CBroadcastAsyncU3Ec__AnonStorey4_t1910876960, ___completed_1)); }
	inline Action_t1264377477 * get_completed_1() const { return ___completed_1; }
	inline Action_t1264377477 ** get_address_of_completed_1() { return &___completed_1; }
	inline void set_completed_1(Action_t1264377477 * value)
	{
		___completed_1 = value;
		Il2CppCodeGenWriteBarrier((&___completed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CBroadcastAsyncU3Ec__AnonStorey4_t1910876960, ___U24this_2)); }
	inline WebSocketSessionManager_t336645657 * get_U24this_2() const { return ___U24this_2; }
	inline WebSocketSessionManager_t336645657 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebSocketSessionManager_t336645657 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBROADCASTASYNCU3EC__ANONSTOREY4_T1910876960_H
#ifndef U3CSENDASYNCU3EC__ANONSTOREY3_T3015274107_H
#define U3CSENDASYNCU3EC__ANONSTOREY3_T3015274107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey3
struct  U3CsendAsyncU3Ec__AnonStorey3_t3015274107  : public RuntimeObject
{
public:
	// System.Func`3<WebSocketSharp.Opcode,System.IO.Stream,System.Boolean> WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey3::sender
	Func_3_t2923582405 * ___sender_0;
	// System.Action`1<System.Boolean> WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey3::completed
	Action_1_t269755560 * ___completed_1;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<sendAsync>c__AnonStorey3::$this
	WebSocket_t62038747 * ___U24this_2;

public:
	inline static int32_t get_offset_of_sender_0() { return static_cast<int32_t>(offsetof(U3CsendAsyncU3Ec__AnonStorey3_t3015274107, ___sender_0)); }
	inline Func_3_t2923582405 * get_sender_0() const { return ___sender_0; }
	inline Func_3_t2923582405 ** get_address_of_sender_0() { return &___sender_0; }
	inline void set_sender_0(Func_3_t2923582405 * value)
	{
		___sender_0 = value;
		Il2CppCodeGenWriteBarrier((&___sender_0), value);
	}

	inline static int32_t get_offset_of_completed_1() { return static_cast<int32_t>(offsetof(U3CsendAsyncU3Ec__AnonStorey3_t3015274107, ___completed_1)); }
	inline Action_1_t269755560 * get_completed_1() const { return ___completed_1; }
	inline Action_1_t269755560 ** get_address_of_completed_1() { return &___completed_1; }
	inline void set_completed_1(Action_1_t269755560 * value)
	{
		___completed_1 = value;
		Il2CppCodeGenWriteBarrier((&___completed_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CsendAsyncU3Ec__AnonStorey3_t3015274107, ___U24this_2)); }
	inline WebSocket_t62038747 * get_U24this_2() const { return ___U24this_2; }
	inline WebSocket_t62038747 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebSocket_t62038747 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDASYNCU3EC__ANONSTOREY3_T3015274107_H
#ifndef U3CACCEPTASYNCU3EC__ANONSTOREY7_T3533643647_H
#define U3CACCEPTASYNCU3EC__ANONSTOREY7_T3533643647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<AcceptAsync>c__AnonStorey7
struct  U3CAcceptAsyncU3Ec__AnonStorey7_t3533643647  : public RuntimeObject
{
public:
	// System.Func`1<System.Boolean> WebSocketSharp.WebSocket/<AcceptAsync>c__AnonStorey7::acceptor
	Func_1_t3822001908 * ___acceptor_0;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<AcceptAsync>c__AnonStorey7::$this
	WebSocket_t62038747 * ___U24this_1;

public:
	inline static int32_t get_offset_of_acceptor_0() { return static_cast<int32_t>(offsetof(U3CAcceptAsyncU3Ec__AnonStorey7_t3533643647, ___acceptor_0)); }
	inline Func_1_t3822001908 * get_acceptor_0() const { return ___acceptor_0; }
	inline Func_1_t3822001908 ** get_address_of_acceptor_0() { return &___acceptor_0; }
	inline void set_acceptor_0(Func_1_t3822001908 * value)
	{
		___acceptor_0 = value;
		Il2CppCodeGenWriteBarrier((&___acceptor_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAcceptAsyncU3Ec__AnonStorey7_t3533643647, ___U24this_1)); }
	inline WebSocket_t62038747 * get_U24this_1() const { return ___U24this_1; }
	inline WebSocket_t62038747 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebSocket_t62038747 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CACCEPTASYNCU3EC__ANONSTOREY7_T3533643647_H
#ifndef WEBSOCKETCONTEXT_T619421455_H
#define WEBSOCKETCONTEXT_T619421455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.WebSocketContext
struct  WebSocketContext_t619421455  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETCONTEXT_T619421455_H
#ifndef U3CVALIDATESECWEBSOCKETPROTOCOLSERVERHEADERU3EC__ANONSTOREY6_T2419708675_H
#define U3CVALIDATESECWEBSOCKETPROTOCOLSERVERHEADERU3EC__ANONSTOREY6_T2419708675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<validateSecWebSocketProtocolServerHeader>c__AnonStorey6
struct  U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t2419708675  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.WebSocket/<validateSecWebSocketProtocolServerHeader>c__AnonStorey6::value
	String_t* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t2419708675, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATESECWEBSOCKETPROTOCOLSERVERHEADERU3EC__ANONSTOREY6_T2419708675_H
#ifndef PAYLOADDATA_T688932160_H
#define PAYLOADDATA_T688932160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.PayloadData
struct  PayloadData_t688932160  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.PayloadData::_data
	ByteU5BU5D_t4116647657* ____data_0;
	// System.Int64 WebSocketSharp.PayloadData::_extDataLength
	int64_t ____extDataLength_1;
	// System.Int64 WebSocketSharp.PayloadData::_length
	int64_t ____length_2;

public:
	inline static int32_t get_offset_of__data_0() { return static_cast<int32_t>(offsetof(PayloadData_t688932160, ____data_0)); }
	inline ByteU5BU5D_t4116647657* get__data_0() const { return ____data_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__data_0() { return &____data_0; }
	inline void set__data_0(ByteU5BU5D_t4116647657* value)
	{
		____data_0 = value;
		Il2CppCodeGenWriteBarrier((&____data_0), value);
	}

	inline static int32_t get_offset_of__extDataLength_1() { return static_cast<int32_t>(offsetof(PayloadData_t688932160, ____extDataLength_1)); }
	inline int64_t get__extDataLength_1() const { return ____extDataLength_1; }
	inline int64_t* get_address_of__extDataLength_1() { return &____extDataLength_1; }
	inline void set__extDataLength_1(int64_t value)
	{
		____extDataLength_1 = value;
	}

	inline static int32_t get_offset_of__length_2() { return static_cast<int32_t>(offsetof(PayloadData_t688932160, ____length_2)); }
	inline int64_t get__length_2() const { return ____length_2; }
	inline int64_t* get_address_of__length_2() { return &____length_2; }
	inline void set__length_2(int64_t value)
	{
		____length_2 = value;
	}
};

struct PayloadData_t688932160_StaticFields
{
public:
	// WebSocketSharp.PayloadData WebSocketSharp.PayloadData::Empty
	PayloadData_t688932160 * ___Empty_3;
	// System.UInt64 WebSocketSharp.PayloadData::MaxLength
	uint64_t ___MaxLength_4;

public:
	inline static int32_t get_offset_of_Empty_3() { return static_cast<int32_t>(offsetof(PayloadData_t688932160_StaticFields, ___Empty_3)); }
	inline PayloadData_t688932160 * get_Empty_3() const { return ___Empty_3; }
	inline PayloadData_t688932160 ** get_address_of_Empty_3() { return &___Empty_3; }
	inline void set_Empty_3(PayloadData_t688932160 * value)
	{
		___Empty_3 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_3), value);
	}

	inline static int32_t get_offset_of_MaxLength_4() { return static_cast<int32_t>(offsetof(PayloadData_t688932160_StaticFields, ___MaxLength_4)); }
	inline uint64_t get_MaxLength_4() const { return ___MaxLength_4; }
	inline uint64_t* get_address_of_MaxLength_4() { return &___MaxLength_4; }
	inline void set_MaxLength_4(uint64_t value)
	{
		___MaxLength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYLOADDATA_T688932160_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR0_T268869223_H
#define U3CGETENUMERATORU3EC__ITERATOR0_T268869223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0
struct  U3CGetEnumeratorU3Ec__Iterator0_t268869223  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$locvar0
	ByteU5BU5D_t4116647657* ___U24locvar0_0;
	// System.Int32 WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$locvar1
	int32_t ___U24locvar1_1;
	// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::<b>__1
	uint8_t ___U3CbU3E__1_2;
	// WebSocketSharp.PayloadData WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$this
	PayloadData_t688932160 * ___U24this_3;
	// System.Byte WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$current
	uint8_t ___U24current_4;
	// System.Boolean WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 WebSocketSharp.PayloadData/<GetEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t268869223, ___U24locvar0_0)); }
	inline ByteU5BU5D_t4116647657* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(ByteU5BU5D_t4116647657* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t268869223, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__1_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t268869223, ___U3CbU3E__1_2)); }
	inline uint8_t get_U3CbU3E__1_2() const { return ___U3CbU3E__1_2; }
	inline uint8_t* get_address_of_U3CbU3E__1_2() { return &___U3CbU3E__1_2; }
	inline void set_U3CbU3E__1_2(uint8_t value)
	{
		___U3CbU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t268869223, ___U24this_3)); }
	inline PayloadData_t688932160 * get_U24this_3() const { return ___U24this_3; }
	inline PayloadData_t688932160 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PayloadData_t688932160 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t268869223, ___U24current_4)); }
	inline uint8_t get_U24current_4() const { return ___U24current_4; }
	inline uint8_t* get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(uint8_t value)
	{
		___U24current_4 = value;
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t268869223, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t268869223, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR0_T268869223_H
#ifndef U3CVALIDATESECWEBSOCKETEXTENSIONSSERVERHEADERU3EC__ANONSTOREY5_T349889031_H
#define U3CVALIDATESECWEBSOCKETEXTENSIONSSERVERHEADERU3EC__ANONSTOREY5_T349889031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<validateSecWebSocketExtensionsServerHeader>c__AnonStorey5
struct  U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t349889031  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.WebSocket/<validateSecWebSocketExtensionsServerHeader>c__AnonStorey5::method
	String_t* ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t349889031, ___method_0)); }
	inline String_t* get_method_0() const { return ___method_0; }
	inline String_t** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(String_t* value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATESECWEBSOCKETEXTENSIONSSERVERHEADERU3EC__ANONSTOREY5_T349889031_H
#ifndef U3CSTARTRECEIVINGU3EC__ANONSTOREY4_T3314047616_H
#define U3CSTARTRECEIVINGU3EC__ANONSTOREY4_T3314047616_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4
struct  U3CstartReceivingU3Ec__AnonStorey4_t3314047616  : public RuntimeObject
{
public:
	// System.Action WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4::receive
	Action_t1264377477 * ___receive_0;
	// WebSocketSharp.WebSocket WebSocketSharp.WebSocket/<startReceiving>c__AnonStorey4::$this
	WebSocket_t62038747 * ___U24this_1;

public:
	inline static int32_t get_offset_of_receive_0() { return static_cast<int32_t>(offsetof(U3CstartReceivingU3Ec__AnonStorey4_t3314047616, ___receive_0)); }
	inline Action_t1264377477 * get_receive_0() const { return ___receive_0; }
	inline Action_t1264377477 ** get_address_of_receive_0() { return &___receive_0; }
	inline void set_receive_0(Action_t1264377477 * value)
	{
		___receive_0 = value;
		Il2CppCodeGenWriteBarrier((&___receive_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CstartReceivingU3Ec__AnonStorey4_t3314047616, ___U24this_1)); }
	inline WebSocket_t62038747 * get_U24this_1() const { return ___U24this_1; }
	inline WebSocket_t62038747 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(WebSocket_t62038747 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTRECEIVINGU3EC__ANONSTOREY4_T3314047616_H
#ifndef NETWORKCREDENTIAL_T1094796801_H
#define NETWORKCREDENTIAL_T1094796801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.NetworkCredential
struct  NetworkCredential_t1094796801  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.NetworkCredential::_domain
	String_t* ____domain_0;
	// System.String WebSocketSharp.Net.NetworkCredential::_password
	String_t* ____password_1;
	// System.String[] WebSocketSharp.Net.NetworkCredential::_roles
	StringU5BU5D_t1281789340* ____roles_2;
	// System.String WebSocketSharp.Net.NetworkCredential::_userName
	String_t* ____userName_3;

public:
	inline static int32_t get_offset_of__domain_0() { return static_cast<int32_t>(offsetof(NetworkCredential_t1094796801, ____domain_0)); }
	inline String_t* get__domain_0() const { return ____domain_0; }
	inline String_t** get_address_of__domain_0() { return &____domain_0; }
	inline void set__domain_0(String_t* value)
	{
		____domain_0 = value;
		Il2CppCodeGenWriteBarrier((&____domain_0), value);
	}

	inline static int32_t get_offset_of__password_1() { return static_cast<int32_t>(offsetof(NetworkCredential_t1094796801, ____password_1)); }
	inline String_t* get__password_1() const { return ____password_1; }
	inline String_t** get_address_of__password_1() { return &____password_1; }
	inline void set__password_1(String_t* value)
	{
		____password_1 = value;
		Il2CppCodeGenWriteBarrier((&____password_1), value);
	}

	inline static int32_t get_offset_of__roles_2() { return static_cast<int32_t>(offsetof(NetworkCredential_t1094796801, ____roles_2)); }
	inline StringU5BU5D_t1281789340* get__roles_2() const { return ____roles_2; }
	inline StringU5BU5D_t1281789340** get_address_of__roles_2() { return &____roles_2; }
	inline void set__roles_2(StringU5BU5D_t1281789340* value)
	{
		____roles_2 = value;
		Il2CppCodeGenWriteBarrier((&____roles_2), value);
	}

	inline static int32_t get_offset_of__userName_3() { return static_cast<int32_t>(offsetof(NetworkCredential_t1094796801, ____userName_3)); }
	inline String_t* get__userName_3() const { return ____userName_3; }
	inline String_t** get_address_of__userName_3() { return &____userName_3; }
	inline void set__userName_3(String_t* value)
	{
		____userName_3 = value;
		Il2CppCodeGenWriteBarrier((&____userName_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETWORKCREDENTIAL_T1094796801_H
#ifndef HTTPLISTENERPREFIX_T910967454_H
#define HTTPLISTENERPREFIX_T910967454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerPrefix
struct  HttpListenerPrefix_t910967454  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.HttpListenerPrefix::_host
	String_t* ____host_0;
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.HttpListenerPrefix::_listener
	HttpListener_t2467819517 * ____listener_1;
	// System.String WebSocketSharp.Net.HttpListenerPrefix::_original
	String_t* ____original_2;
	// System.String WebSocketSharp.Net.HttpListenerPrefix::_path
	String_t* ____path_3;
	// System.String WebSocketSharp.Net.HttpListenerPrefix::_port
	String_t* ____port_4;
	// System.String WebSocketSharp.Net.HttpListenerPrefix::_prefix
	String_t* ____prefix_5;
	// System.Boolean WebSocketSharp.Net.HttpListenerPrefix::_secure
	bool ____secure_6;

public:
	inline static int32_t get_offset_of__host_0() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t910967454, ____host_0)); }
	inline String_t* get__host_0() const { return ____host_0; }
	inline String_t** get_address_of__host_0() { return &____host_0; }
	inline void set__host_0(String_t* value)
	{
		____host_0 = value;
		Il2CppCodeGenWriteBarrier((&____host_0), value);
	}

	inline static int32_t get_offset_of__listener_1() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t910967454, ____listener_1)); }
	inline HttpListener_t2467819517 * get__listener_1() const { return ____listener_1; }
	inline HttpListener_t2467819517 ** get_address_of__listener_1() { return &____listener_1; }
	inline void set__listener_1(HttpListener_t2467819517 * value)
	{
		____listener_1 = value;
		Il2CppCodeGenWriteBarrier((&____listener_1), value);
	}

	inline static int32_t get_offset_of__original_2() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t910967454, ____original_2)); }
	inline String_t* get__original_2() const { return ____original_2; }
	inline String_t** get_address_of__original_2() { return &____original_2; }
	inline void set__original_2(String_t* value)
	{
		____original_2 = value;
		Il2CppCodeGenWriteBarrier((&____original_2), value);
	}

	inline static int32_t get_offset_of__path_3() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t910967454, ____path_3)); }
	inline String_t* get__path_3() const { return ____path_3; }
	inline String_t** get_address_of__path_3() { return &____path_3; }
	inline void set__path_3(String_t* value)
	{
		____path_3 = value;
		Il2CppCodeGenWriteBarrier((&____path_3), value);
	}

	inline static int32_t get_offset_of__port_4() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t910967454, ____port_4)); }
	inline String_t* get__port_4() const { return ____port_4; }
	inline String_t** get_address_of__port_4() { return &____port_4; }
	inline void set__port_4(String_t* value)
	{
		____port_4 = value;
		Il2CppCodeGenWriteBarrier((&____port_4), value);
	}

	inline static int32_t get_offset_of__prefix_5() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t910967454, ____prefix_5)); }
	inline String_t* get__prefix_5() const { return ____prefix_5; }
	inline String_t** get_address_of__prefix_5() { return &____prefix_5; }
	inline void set__prefix_5(String_t* value)
	{
		____prefix_5 = value;
		Il2CppCodeGenWriteBarrier((&____prefix_5), value);
	}

	inline static int32_t get_offset_of__secure_6() { return static_cast<int32_t>(offsetof(HttpListenerPrefix_t910967454, ____secure_6)); }
	inline bool get__secure_6() const { return ____secure_6; }
	inline bool* get_address_of__secure_6() { return &____secure_6; }
	inline void set__secure_6(bool value)
	{
		____secure_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERPREFIX_T910967454_H
#ifndef HTTPLISTENERCONTEXT_T3723273891_H
#define HTTPLISTENERCONTEXT_T3723273891_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerContext
struct  HttpListenerContext_t3723273891  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.HttpConnection WebSocketSharp.Net.HttpListenerContext::_connection
	HttpConnection_t4177287240 * ____connection_0;
	// System.String WebSocketSharp.Net.HttpListenerContext::_error
	String_t* ____error_1;
	// System.Int32 WebSocketSharp.Net.HttpListenerContext::_errorStatus
	int32_t ____errorStatus_2;
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.HttpListenerContext::_listener
	HttpListener_t2467819517 * ____listener_3;
	// WebSocketSharp.Net.HttpListenerRequest WebSocketSharp.Net.HttpListenerContext::_request
	HttpListenerRequest_t2959552699 * ____request_4;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Net.HttpListenerContext::_response
	HttpListenerResponse_t2818529495 * ____response_5;
	// System.Security.Principal.IPrincipal WebSocketSharp.Net.HttpListenerContext::_user
	RuntimeObject* ____user_6;
	// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext WebSocketSharp.Net.HttpListenerContext::_websocketContext
	HttpListenerWebSocketContext_t3770402993 * ____websocketContext_7;

public:
	inline static int32_t get_offset_of__connection_0() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3723273891, ____connection_0)); }
	inline HttpConnection_t4177287240 * get__connection_0() const { return ____connection_0; }
	inline HttpConnection_t4177287240 ** get_address_of__connection_0() { return &____connection_0; }
	inline void set__connection_0(HttpConnection_t4177287240 * value)
	{
		____connection_0 = value;
		Il2CppCodeGenWriteBarrier((&____connection_0), value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3723273891, ____error_1)); }
	inline String_t* get__error_1() const { return ____error_1; }
	inline String_t** get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(String_t* value)
	{
		____error_1 = value;
		Il2CppCodeGenWriteBarrier((&____error_1), value);
	}

	inline static int32_t get_offset_of__errorStatus_2() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3723273891, ____errorStatus_2)); }
	inline int32_t get__errorStatus_2() const { return ____errorStatus_2; }
	inline int32_t* get_address_of__errorStatus_2() { return &____errorStatus_2; }
	inline void set__errorStatus_2(int32_t value)
	{
		____errorStatus_2 = value;
	}

	inline static int32_t get_offset_of__listener_3() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3723273891, ____listener_3)); }
	inline HttpListener_t2467819517 * get__listener_3() const { return ____listener_3; }
	inline HttpListener_t2467819517 ** get_address_of__listener_3() { return &____listener_3; }
	inline void set__listener_3(HttpListener_t2467819517 * value)
	{
		____listener_3 = value;
		Il2CppCodeGenWriteBarrier((&____listener_3), value);
	}

	inline static int32_t get_offset_of__request_4() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3723273891, ____request_4)); }
	inline HttpListenerRequest_t2959552699 * get__request_4() const { return ____request_4; }
	inline HttpListenerRequest_t2959552699 ** get_address_of__request_4() { return &____request_4; }
	inline void set__request_4(HttpListenerRequest_t2959552699 * value)
	{
		____request_4 = value;
		Il2CppCodeGenWriteBarrier((&____request_4), value);
	}

	inline static int32_t get_offset_of__response_5() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3723273891, ____response_5)); }
	inline HttpListenerResponse_t2818529495 * get__response_5() const { return ____response_5; }
	inline HttpListenerResponse_t2818529495 ** get_address_of__response_5() { return &____response_5; }
	inline void set__response_5(HttpListenerResponse_t2818529495 * value)
	{
		____response_5 = value;
		Il2CppCodeGenWriteBarrier((&____response_5), value);
	}

	inline static int32_t get_offset_of__user_6() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3723273891, ____user_6)); }
	inline RuntimeObject* get__user_6() const { return ____user_6; }
	inline RuntimeObject** get_address_of__user_6() { return &____user_6; }
	inline void set__user_6(RuntimeObject* value)
	{
		____user_6 = value;
		Il2CppCodeGenWriteBarrier((&____user_6), value);
	}

	inline static int32_t get_offset_of__websocketContext_7() { return static_cast<int32_t>(offsetof(HttpListenerContext_t3723273891, ____websocketContext_7)); }
	inline HttpListenerWebSocketContext_t3770402993 * get__websocketContext_7() const { return ____websocketContext_7; }
	inline HttpListenerWebSocketContext_t3770402993 ** get_address_of__websocketContext_7() { return &____websocketContext_7; }
	inline void set__websocketContext_7(HttpListenerWebSocketContext_t3770402993 * value)
	{
		____websocketContext_7 = value;
		Il2CppCodeGenWriteBarrier((&____websocketContext_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERCONTEXT_T3723273891_H
#ifndef U3CCOMPLETEU3EC__ANONSTOREY0_T1824639194_H
#define U3CCOMPLETEU3EC__ANONSTOREY0_T1824639194_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerAsyncResult/<complete>c__AnonStorey0
struct  U3CcompleteU3Ec__AnonStorey0_t1824639194  : public RuntimeObject
{
public:
	// System.AsyncCallback WebSocketSharp.Net.HttpListenerAsyncResult/<complete>c__AnonStorey0::callback
	AsyncCallback_t3962456242 * ___callback_0;
	// WebSocketSharp.Net.HttpListenerAsyncResult WebSocketSharp.Net.HttpListenerAsyncResult/<complete>c__AnonStorey0::asyncResult
	HttpListenerAsyncResult_t1598414744 * ___asyncResult_1;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CcompleteU3Ec__AnonStorey0_t1824639194, ___callback_0)); }
	inline AsyncCallback_t3962456242 * get_callback_0() const { return ___callback_0; }
	inline AsyncCallback_t3962456242 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(AsyncCallback_t3962456242 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}

	inline static int32_t get_offset_of_asyncResult_1() { return static_cast<int32_t>(offsetof(U3CcompleteU3Ec__AnonStorey0_t1824639194, ___asyncResult_1)); }
	inline HttpListenerAsyncResult_t1598414744 * get_asyncResult_1() const { return ___asyncResult_1; }
	inline HttpListenerAsyncResult_t1598414744 ** get_address_of_asyncResult_1() { return &___asyncResult_1; }
	inline void set_asyncResult_1(HttpListenerAsyncResult_t1598414744 * value)
	{
		___asyncResult_1 = value;
		Il2CppCodeGenWriteBarrier((&___asyncResult_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOMPLETEU3EC__ANONSTOREY0_T1824639194_H
#ifndef HTTPLISTENERASYNCRESULT_T1598414744_H
#define HTTPLISTENERASYNCRESULT_T1598414744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerAsyncResult
struct  HttpListenerAsyncResult_t1598414744  : public RuntimeObject
{
public:
	// System.AsyncCallback WebSocketSharp.Net.HttpListenerAsyncResult::_callback
	AsyncCallback_t3962456242 * ____callback_0;
	// System.Boolean WebSocketSharp.Net.HttpListenerAsyncResult::_completed
	bool ____completed_1;
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.HttpListenerAsyncResult::_context
	HttpListenerContext_t3723273891 * ____context_2;
	// System.Boolean WebSocketSharp.Net.HttpListenerAsyncResult::_endCalled
	bool ____endCalled_3;
	// System.Exception WebSocketSharp.Net.HttpListenerAsyncResult::_exception
	Exception_t * ____exception_4;
	// System.Boolean WebSocketSharp.Net.HttpListenerAsyncResult::_inGet
	bool ____inGet_5;
	// System.Object WebSocketSharp.Net.HttpListenerAsyncResult::_state
	RuntimeObject * ____state_6;
	// System.Object WebSocketSharp.Net.HttpListenerAsyncResult::_sync
	RuntimeObject * ____sync_7;
	// System.Boolean WebSocketSharp.Net.HttpListenerAsyncResult::_syncCompleted
	bool ____syncCompleted_8;
	// System.Threading.ManualResetEvent WebSocketSharp.Net.HttpListenerAsyncResult::_waitHandle
	ManualResetEvent_t451242010 * ____waitHandle_9;

public:
	inline static int32_t get_offset_of__callback_0() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t1598414744, ____callback_0)); }
	inline AsyncCallback_t3962456242 * get__callback_0() const { return ____callback_0; }
	inline AsyncCallback_t3962456242 ** get_address_of__callback_0() { return &____callback_0; }
	inline void set__callback_0(AsyncCallback_t3962456242 * value)
	{
		____callback_0 = value;
		Il2CppCodeGenWriteBarrier((&____callback_0), value);
	}

	inline static int32_t get_offset_of__completed_1() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t1598414744, ____completed_1)); }
	inline bool get__completed_1() const { return ____completed_1; }
	inline bool* get_address_of__completed_1() { return &____completed_1; }
	inline void set__completed_1(bool value)
	{
		____completed_1 = value;
	}

	inline static int32_t get_offset_of__context_2() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t1598414744, ____context_2)); }
	inline HttpListenerContext_t3723273891 * get__context_2() const { return ____context_2; }
	inline HttpListenerContext_t3723273891 ** get_address_of__context_2() { return &____context_2; }
	inline void set__context_2(HttpListenerContext_t3723273891 * value)
	{
		____context_2 = value;
		Il2CppCodeGenWriteBarrier((&____context_2), value);
	}

	inline static int32_t get_offset_of__endCalled_3() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t1598414744, ____endCalled_3)); }
	inline bool get__endCalled_3() const { return ____endCalled_3; }
	inline bool* get_address_of__endCalled_3() { return &____endCalled_3; }
	inline void set__endCalled_3(bool value)
	{
		____endCalled_3 = value;
	}

	inline static int32_t get_offset_of__exception_4() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t1598414744, ____exception_4)); }
	inline Exception_t * get__exception_4() const { return ____exception_4; }
	inline Exception_t ** get_address_of__exception_4() { return &____exception_4; }
	inline void set__exception_4(Exception_t * value)
	{
		____exception_4 = value;
		Il2CppCodeGenWriteBarrier((&____exception_4), value);
	}

	inline static int32_t get_offset_of__inGet_5() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t1598414744, ____inGet_5)); }
	inline bool get__inGet_5() const { return ____inGet_5; }
	inline bool* get_address_of__inGet_5() { return &____inGet_5; }
	inline void set__inGet_5(bool value)
	{
		____inGet_5 = value;
	}

	inline static int32_t get_offset_of__state_6() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t1598414744, ____state_6)); }
	inline RuntimeObject * get__state_6() const { return ____state_6; }
	inline RuntimeObject ** get_address_of__state_6() { return &____state_6; }
	inline void set__state_6(RuntimeObject * value)
	{
		____state_6 = value;
		Il2CppCodeGenWriteBarrier((&____state_6), value);
	}

	inline static int32_t get_offset_of__sync_7() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t1598414744, ____sync_7)); }
	inline RuntimeObject * get__sync_7() const { return ____sync_7; }
	inline RuntimeObject ** get_address_of__sync_7() { return &____sync_7; }
	inline void set__sync_7(RuntimeObject * value)
	{
		____sync_7 = value;
		Il2CppCodeGenWriteBarrier((&____sync_7), value);
	}

	inline static int32_t get_offset_of__syncCompleted_8() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t1598414744, ____syncCompleted_8)); }
	inline bool get__syncCompleted_8() const { return ____syncCompleted_8; }
	inline bool* get_address_of__syncCompleted_8() { return &____syncCompleted_8; }
	inline void set__syncCompleted_8(bool value)
	{
		____syncCompleted_8 = value;
	}

	inline static int32_t get_offset_of__waitHandle_9() { return static_cast<int32_t>(offsetof(HttpListenerAsyncResult_t1598414744, ____waitHandle_9)); }
	inline ManualResetEvent_t451242010 * get__waitHandle_9() const { return ____waitHandle_9; }
	inline ManualResetEvent_t451242010 ** get_address_of__waitHandle_9() { return &____waitHandle_9; }
	inline void set__waitHandle_9(ManualResetEvent_t451242010 * value)
	{
		____waitHandle_9 = value;
		Il2CppCodeGenWriteBarrier((&____waitHandle_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERASYNCRESULT_T1598414744_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public RuntimeObject
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_0)); }
	inline Stream_t1273022909 * get_Null_0() const { return ___Null_0; }
	inline Stream_t1273022909 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t1273022909 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef NAMEOBJECTCOLLECTIONBASE_T2091847364_H
#define NAMEOBJECTCOLLECTIONBASE_T2091847364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_t2091847364  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Collections.Specialized.NameObjectCollectionBase::m_ItemsContainer
	Hashtable_t1853889766 * ___m_ItemsContainer_0;
	// System.Collections.Specialized.NameObjectCollectionBase/_Item System.Collections.Specialized.NameObjectCollectionBase::m_NullKeyItem
	_Item_t2272350267 * ___m_NullKeyItem_1;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::m_ItemsArray
	ArrayList_t2718874744 * ___m_ItemsArray_2;
	// System.Collections.IHashCodeProvider System.Collections.Specialized.NameObjectCollectionBase::m_hashprovider
	RuntimeObject* ___m_hashprovider_3;
	// System.Collections.IComparer System.Collections.Specialized.NameObjectCollectionBase::m_comparer
	RuntimeObject* ___m_comparer_4;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::m_defCapacity
	int32_t ___m_defCapacity_5;
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::m_readonly
	bool ___m_readonly_6;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::infoCopy
	SerializationInfo_t950877179 * ___infoCopy_7;
	// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection System.Collections.Specialized.NameObjectCollectionBase::keyscoll
	KeysCollection_t1318642398 * ___keyscoll_8;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::equality_comparer
	RuntimeObject* ___equality_comparer_9;

public:
	inline static int32_t get_offset_of_m_ItemsContainer_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_ItemsContainer_0)); }
	inline Hashtable_t1853889766 * get_m_ItemsContainer_0() const { return ___m_ItemsContainer_0; }
	inline Hashtable_t1853889766 ** get_address_of_m_ItemsContainer_0() { return &___m_ItemsContainer_0; }
	inline void set_m_ItemsContainer_0(Hashtable_t1853889766 * value)
	{
		___m_ItemsContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsContainer_0), value);
	}

	inline static int32_t get_offset_of_m_NullKeyItem_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_NullKeyItem_1)); }
	inline _Item_t2272350267 * get_m_NullKeyItem_1() const { return ___m_NullKeyItem_1; }
	inline _Item_t2272350267 ** get_address_of_m_NullKeyItem_1() { return &___m_NullKeyItem_1; }
	inline void set_m_NullKeyItem_1(_Item_t2272350267 * value)
	{
		___m_NullKeyItem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NullKeyItem_1), value);
	}

	inline static int32_t get_offset_of_m_ItemsArray_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_ItemsArray_2)); }
	inline ArrayList_t2718874744 * get_m_ItemsArray_2() const { return ___m_ItemsArray_2; }
	inline ArrayList_t2718874744 ** get_address_of_m_ItemsArray_2() { return &___m_ItemsArray_2; }
	inline void set_m_ItemsArray_2(ArrayList_t2718874744 * value)
	{
		___m_ItemsArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsArray_2), value);
	}

	inline static int32_t get_offset_of_m_hashprovider_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_hashprovider_3)); }
	inline RuntimeObject* get_m_hashprovider_3() const { return ___m_hashprovider_3; }
	inline RuntimeObject** get_address_of_m_hashprovider_3() { return &___m_hashprovider_3; }
	inline void set_m_hashprovider_3(RuntimeObject* value)
	{
		___m_hashprovider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashprovider_3), value);
	}

	inline static int32_t get_offset_of_m_comparer_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_comparer_4)); }
	inline RuntimeObject* get_m_comparer_4() const { return ___m_comparer_4; }
	inline RuntimeObject** get_address_of_m_comparer_4() { return &___m_comparer_4; }
	inline void set_m_comparer_4(RuntimeObject* value)
	{
		___m_comparer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_comparer_4), value);
	}

	inline static int32_t get_offset_of_m_defCapacity_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_defCapacity_5)); }
	inline int32_t get_m_defCapacity_5() const { return ___m_defCapacity_5; }
	inline int32_t* get_address_of_m_defCapacity_5() { return &___m_defCapacity_5; }
	inline void set_m_defCapacity_5(int32_t value)
	{
		___m_defCapacity_5 = value;
	}

	inline static int32_t get_offset_of_m_readonly_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___m_readonly_6)); }
	inline bool get_m_readonly_6() const { return ___m_readonly_6; }
	inline bool* get_address_of_m_readonly_6() { return &___m_readonly_6; }
	inline void set_m_readonly_6(bool value)
	{
		___m_readonly_6 = value;
	}

	inline static int32_t get_offset_of_infoCopy_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___infoCopy_7)); }
	inline SerializationInfo_t950877179 * get_infoCopy_7() const { return ___infoCopy_7; }
	inline SerializationInfo_t950877179 ** get_address_of_infoCopy_7() { return &___infoCopy_7; }
	inline void set_infoCopy_7(SerializationInfo_t950877179 * value)
	{
		___infoCopy_7 = value;
		Il2CppCodeGenWriteBarrier((&___infoCopy_7), value);
	}

	inline static int32_t get_offset_of_keyscoll_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___keyscoll_8)); }
	inline KeysCollection_t1318642398 * get_keyscoll_8() const { return ___keyscoll_8; }
	inline KeysCollection_t1318642398 ** get_address_of_keyscoll_8() { return &___keyscoll_8; }
	inline void set_keyscoll_8(KeysCollection_t1318642398 * value)
	{
		___keyscoll_8 = value;
		Il2CppCodeGenWriteBarrier((&___keyscoll_8), value);
	}

	inline static int32_t get_offset_of_equality_comparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_t2091847364, ___equality_comparer_9)); }
	inline RuntimeObject* get_equality_comparer_9() const { return ___equality_comparer_9; }
	inline RuntimeObject** get_address_of_equality_comparer_9() { return &___equality_comparer_9; }
	inline void set_equality_comparer_9(RuntimeObject* value)
	{
		___equality_comparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___equality_comparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_T2091847364_H
#ifndef COOKIECOLLECTION_T962330244_H
#define COOKIECOLLECTION_T962330244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.CookieCollection
struct  CookieCollection_t962330244  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::_list
	List_1_t1380209731 * ____list_0;
	// System.Object WebSocketSharp.Net.CookieCollection::_sync
	RuntimeObject * ____sync_1;

public:
	inline static int32_t get_offset_of__list_0() { return static_cast<int32_t>(offsetof(CookieCollection_t962330244, ____list_0)); }
	inline List_1_t1380209731 * get__list_0() const { return ____list_0; }
	inline List_1_t1380209731 ** get_address_of__list_0() { return &____list_0; }
	inline void set__list_0(List_1_t1380209731 * value)
	{
		____list_0 = value;
		Il2CppCodeGenWriteBarrier((&____list_0), value);
	}

	inline static int32_t get_offset_of__sync_1() { return static_cast<int32_t>(offsetof(CookieCollection_t962330244, ____sync_1)); }
	inline RuntimeObject * get__sync_1() const { return ____sync_1; }
	inline RuntimeObject ** get_address_of__sync_1() { return &____sync_1; }
	inline void set__sync_1(RuntimeObject * value)
	{
		____sync_1 = value;
		Il2CppCodeGenWriteBarrier((&____sync_1), value);
	}
};

struct CookieCollection_t962330244_StaticFields
{
public:
	// System.Comparison`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::<>f__mg$cache0
	Comparison_1_t3978033464 * ___U3CU3Ef__mgU24cache0_2;
	// System.Comparison`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::<>f__mg$cache1
	Comparison_1_t3978033464 * ___U3CU3Ef__mgU24cache1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_2() { return static_cast<int32_t>(offsetof(CookieCollection_t962330244_StaticFields, ___U3CU3Ef__mgU24cache0_2)); }
	inline Comparison_1_t3978033464 * get_U3CU3Ef__mgU24cache0_2() const { return ___U3CU3Ef__mgU24cache0_2; }
	inline Comparison_1_t3978033464 ** get_address_of_U3CU3Ef__mgU24cache0_2() { return &___U3CU3Ef__mgU24cache0_2; }
	inline void set_U3CU3Ef__mgU24cache0_2(Comparison_1_t3978033464 * value)
	{
		___U3CU3Ef__mgU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_3() { return static_cast<int32_t>(offsetof(CookieCollection_t962330244_StaticFields, ___U3CU3Ef__mgU24cache1_3)); }
	inline Comparison_1_t3978033464 * get_U3CU3Ef__mgU24cache1_3() const { return ___U3CU3Ef__mgU24cache1_3; }
	inline Comparison_1_t3978033464 ** get_address_of_U3CU3Ef__mgU24cache1_3() { return &___U3CU3Ef__mgU24cache1_3; }
	inline void set_U3CU3Ef__mgU24cache1_3(Comparison_1_t3978033464 * value)
	{
		___U3CU3Ef__mgU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIECOLLECTION_T962330244_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ENDPOINTLISTENER_T884507598_H
#define ENDPOINTLISTENER_T884507598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.EndPointListener
struct  EndPointListener_t884507598  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerPrefix> WebSocketSharp.Net.EndPointListener::_all
	List_1_t2383042196 * ____all_0;
	// System.Net.IPEndPoint WebSocketSharp.Net.EndPointListener::_endpoint
	IPEndPoint_t3791887218 * ____endpoint_2;
	// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpListenerPrefix,WebSocketSharp.Net.HttpListener> WebSocketSharp.Net.EndPointListener::_prefixes
	Dictionary_2_t1167433975 * ____prefixes_3;
	// System.Boolean WebSocketSharp.Net.EndPointListener::_secure
	bool ____secure_4;
	// System.Net.Sockets.Socket WebSocketSharp.Net.EndPointListener::_socket
	Socket_t1119025450 * ____socket_5;
	// WebSocketSharp.Net.ServerSslConfiguration WebSocketSharp.Net.EndPointListener::_sslConfig
	ServerSslConfiguration_t3240511754 * ____sslConfig_6;
	// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerPrefix> WebSocketSharp.Net.EndPointListener::_unhandled
	List_1_t2383042196 * ____unhandled_7;
	// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection> WebSocketSharp.Net.EndPointListener::_unregistered
	Dictionary_2_t1708533168 * ____unregistered_8;
	// System.Object WebSocketSharp.Net.EndPointListener::_unregisteredSync
	RuntimeObject * ____unregisteredSync_9;

public:
	inline static int32_t get_offset_of__all_0() { return static_cast<int32_t>(offsetof(EndPointListener_t884507598, ____all_0)); }
	inline List_1_t2383042196 * get__all_0() const { return ____all_0; }
	inline List_1_t2383042196 ** get_address_of__all_0() { return &____all_0; }
	inline void set__all_0(List_1_t2383042196 * value)
	{
		____all_0 = value;
		Il2CppCodeGenWriteBarrier((&____all_0), value);
	}

	inline static int32_t get_offset_of__endpoint_2() { return static_cast<int32_t>(offsetof(EndPointListener_t884507598, ____endpoint_2)); }
	inline IPEndPoint_t3791887218 * get__endpoint_2() const { return ____endpoint_2; }
	inline IPEndPoint_t3791887218 ** get_address_of__endpoint_2() { return &____endpoint_2; }
	inline void set__endpoint_2(IPEndPoint_t3791887218 * value)
	{
		____endpoint_2 = value;
		Il2CppCodeGenWriteBarrier((&____endpoint_2), value);
	}

	inline static int32_t get_offset_of__prefixes_3() { return static_cast<int32_t>(offsetof(EndPointListener_t884507598, ____prefixes_3)); }
	inline Dictionary_2_t1167433975 * get__prefixes_3() const { return ____prefixes_3; }
	inline Dictionary_2_t1167433975 ** get_address_of__prefixes_3() { return &____prefixes_3; }
	inline void set__prefixes_3(Dictionary_2_t1167433975 * value)
	{
		____prefixes_3 = value;
		Il2CppCodeGenWriteBarrier((&____prefixes_3), value);
	}

	inline static int32_t get_offset_of__secure_4() { return static_cast<int32_t>(offsetof(EndPointListener_t884507598, ____secure_4)); }
	inline bool get__secure_4() const { return ____secure_4; }
	inline bool* get_address_of__secure_4() { return &____secure_4; }
	inline void set__secure_4(bool value)
	{
		____secure_4 = value;
	}

	inline static int32_t get_offset_of__socket_5() { return static_cast<int32_t>(offsetof(EndPointListener_t884507598, ____socket_5)); }
	inline Socket_t1119025450 * get__socket_5() const { return ____socket_5; }
	inline Socket_t1119025450 ** get_address_of__socket_5() { return &____socket_5; }
	inline void set__socket_5(Socket_t1119025450 * value)
	{
		____socket_5 = value;
		Il2CppCodeGenWriteBarrier((&____socket_5), value);
	}

	inline static int32_t get_offset_of__sslConfig_6() { return static_cast<int32_t>(offsetof(EndPointListener_t884507598, ____sslConfig_6)); }
	inline ServerSslConfiguration_t3240511754 * get__sslConfig_6() const { return ____sslConfig_6; }
	inline ServerSslConfiguration_t3240511754 ** get_address_of__sslConfig_6() { return &____sslConfig_6; }
	inline void set__sslConfig_6(ServerSslConfiguration_t3240511754 * value)
	{
		____sslConfig_6 = value;
		Il2CppCodeGenWriteBarrier((&____sslConfig_6), value);
	}

	inline static int32_t get_offset_of__unhandled_7() { return static_cast<int32_t>(offsetof(EndPointListener_t884507598, ____unhandled_7)); }
	inline List_1_t2383042196 * get__unhandled_7() const { return ____unhandled_7; }
	inline List_1_t2383042196 ** get_address_of__unhandled_7() { return &____unhandled_7; }
	inline void set__unhandled_7(List_1_t2383042196 * value)
	{
		____unhandled_7 = value;
		Il2CppCodeGenWriteBarrier((&____unhandled_7), value);
	}

	inline static int32_t get_offset_of__unregistered_8() { return static_cast<int32_t>(offsetof(EndPointListener_t884507598, ____unregistered_8)); }
	inline Dictionary_2_t1708533168 * get__unregistered_8() const { return ____unregistered_8; }
	inline Dictionary_2_t1708533168 ** get_address_of__unregistered_8() { return &____unregistered_8; }
	inline void set__unregistered_8(Dictionary_2_t1708533168 * value)
	{
		____unregistered_8 = value;
		Il2CppCodeGenWriteBarrier((&____unregistered_8), value);
	}

	inline static int32_t get_offset_of__unregisteredSync_9() { return static_cast<int32_t>(offsetof(EndPointListener_t884507598, ____unregisteredSync_9)); }
	inline RuntimeObject * get__unregisteredSync_9() const { return ____unregisteredSync_9; }
	inline RuntimeObject ** get_address_of__unregisteredSync_9() { return &____unregisteredSync_9; }
	inline void set__unregisteredSync_9(RuntimeObject * value)
	{
		____unregisteredSync_9 = value;
		Il2CppCodeGenWriteBarrier((&____unregisteredSync_9), value);
	}
};

struct EndPointListener_t884507598_StaticFields
{
public:
	// System.String WebSocketSharp.Net.EndPointListener::_defaultCertFolderPath
	String_t* ____defaultCertFolderPath_1;
	// System.AsyncCallback WebSocketSharp.Net.EndPointListener::<>f__mg$cache0
	AsyncCallback_t3962456242 * ___U3CU3Ef__mgU24cache0_10;
	// System.AsyncCallback WebSocketSharp.Net.EndPointListener::<>f__mg$cache1
	AsyncCallback_t3962456242 * ___U3CU3Ef__mgU24cache1_11;

public:
	inline static int32_t get_offset_of__defaultCertFolderPath_1() { return static_cast<int32_t>(offsetof(EndPointListener_t884507598_StaticFields, ____defaultCertFolderPath_1)); }
	inline String_t* get__defaultCertFolderPath_1() const { return ____defaultCertFolderPath_1; }
	inline String_t** get_address_of__defaultCertFolderPath_1() { return &____defaultCertFolderPath_1; }
	inline void set__defaultCertFolderPath_1(String_t* value)
	{
		____defaultCertFolderPath_1 = value;
		Il2CppCodeGenWriteBarrier((&____defaultCertFolderPath_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_10() { return static_cast<int32_t>(offsetof(EndPointListener_t884507598_StaticFields, ___U3CU3Ef__mgU24cache0_10)); }
	inline AsyncCallback_t3962456242 * get_U3CU3Ef__mgU24cache0_10() const { return ___U3CU3Ef__mgU24cache0_10; }
	inline AsyncCallback_t3962456242 ** get_address_of_U3CU3Ef__mgU24cache0_10() { return &___U3CU3Ef__mgU24cache0_10; }
	inline void set_U3CU3Ef__mgU24cache0_10(AsyncCallback_t3962456242 * value)
	{
		___U3CU3Ef__mgU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_11() { return static_cast<int32_t>(offsetof(EndPointListener_t884507598_StaticFields, ___U3CU3Ef__mgU24cache1_11)); }
	inline AsyncCallback_t3962456242 * get_U3CU3Ef__mgU24cache1_11() const { return ___U3CU3Ef__mgU24cache1_11; }
	inline AsyncCallback_t3962456242 ** get_address_of_U3CU3Ef__mgU24cache1_11() { return &___U3CU3Ef__mgU24cache1_11; }
	inline void set_U3CU3Ef__mgU24cache1_11(AsyncCallback_t3962456242 * value)
	{
		___U3CU3Ef__mgU24cache1_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINTLISTENER_T884507598_H
#ifndef ENDPOINTMANAGER_T3599117988_H
#define ENDPOINTMANAGER_T3599117988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.EndPointManager
struct  EndPointManager_t3599117988  : public RuntimeObject
{
public:

public:
};

struct EndPointManager_t3599117988_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Net.IPEndPoint,WebSocketSharp.Net.EndPointListener> WebSocketSharp.Net.EndPointManager::_endpoints
	Dictionary_2_t3259849252 * ____endpoints_0;

public:
	inline static int32_t get_offset_of__endpoints_0() { return static_cast<int32_t>(offsetof(EndPointManager_t3599117988_StaticFields, ____endpoints_0)); }
	inline Dictionary_2_t3259849252 * get__endpoints_0() const { return ____endpoints_0; }
	inline Dictionary_2_t3259849252 ** get_address_of__endpoints_0() { return &____endpoints_0; }
	inline void set__endpoints_0(Dictionary_2_t3259849252 * value)
	{
		____endpoints_0 = value;
		Il2CppCodeGenWriteBarrier((&____endpoints_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENDPOINTMANAGER_T3599117988_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef HTTPLISTENERPREFIXCOLLECTION_T702486717_H
#define HTTPLISTENERPREFIXCOLLECTION_T702486717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerPrefixCollection
struct  HttpListenerPrefixCollection_t702486717  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.HttpListenerPrefixCollection::_listener
	HttpListener_t2467819517 * ____listener_0;
	// System.Collections.Generic.List`1<System.String> WebSocketSharp.Net.HttpListenerPrefixCollection::_prefixes
	List_1_t3319525431 * ____prefixes_1;

public:
	inline static int32_t get_offset_of__listener_0() { return static_cast<int32_t>(offsetof(HttpListenerPrefixCollection_t702486717, ____listener_0)); }
	inline HttpListener_t2467819517 * get__listener_0() const { return ____listener_0; }
	inline HttpListener_t2467819517 ** get_address_of__listener_0() { return &____listener_0; }
	inline void set__listener_0(HttpListener_t2467819517 * value)
	{
		____listener_0 = value;
		Il2CppCodeGenWriteBarrier((&____listener_0), value);
	}

	inline static int32_t get_offset_of__prefixes_1() { return static_cast<int32_t>(offsetof(HttpListenerPrefixCollection_t702486717, ____prefixes_1)); }
	inline List_1_t3319525431 * get__prefixes_1() const { return ____prefixes_1; }
	inline List_1_t3319525431 ** get_address_of__prefixes_1() { return &____prefixes_1; }
	inline void set__prefixes_1(List_1_t3319525431 * value)
	{
		____prefixes_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefixes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERPREFIXCOLLECTION_T702486717_H
#ifndef HTTPSTREAMASYNCRESULT_T890764446_H
#define HTTPSTREAMASYNCRESULT_T890764446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpStreamAsyncResult
struct  HttpStreamAsyncResult_t890764446  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.Net.HttpStreamAsyncResult::_buffer
	ByteU5BU5D_t4116647657* ____buffer_0;
	// System.AsyncCallback WebSocketSharp.Net.HttpStreamAsyncResult::_callback
	AsyncCallback_t3962456242 * ____callback_1;
	// System.Boolean WebSocketSharp.Net.HttpStreamAsyncResult::_completed
	bool ____completed_2;
	// System.Int32 WebSocketSharp.Net.HttpStreamAsyncResult::_count
	int32_t ____count_3;
	// System.Exception WebSocketSharp.Net.HttpStreamAsyncResult::_exception
	Exception_t * ____exception_4;
	// System.Int32 WebSocketSharp.Net.HttpStreamAsyncResult::_offset
	int32_t ____offset_5;
	// System.Object WebSocketSharp.Net.HttpStreamAsyncResult::_state
	RuntimeObject * ____state_6;
	// System.Object WebSocketSharp.Net.HttpStreamAsyncResult::_sync
	RuntimeObject * ____sync_7;
	// System.Int32 WebSocketSharp.Net.HttpStreamAsyncResult::_syncRead
	int32_t ____syncRead_8;
	// System.Threading.ManualResetEvent WebSocketSharp.Net.HttpStreamAsyncResult::_waitHandle
	ManualResetEvent_t451242010 * ____waitHandle_9;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t890764446, ____buffer_0)); }
	inline ByteU5BU5D_t4116647657* get__buffer_0() const { return ____buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(ByteU5BU5D_t4116647657* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_0), value);
	}

	inline static int32_t get_offset_of__callback_1() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t890764446, ____callback_1)); }
	inline AsyncCallback_t3962456242 * get__callback_1() const { return ____callback_1; }
	inline AsyncCallback_t3962456242 ** get_address_of__callback_1() { return &____callback_1; }
	inline void set__callback_1(AsyncCallback_t3962456242 * value)
	{
		____callback_1 = value;
		Il2CppCodeGenWriteBarrier((&____callback_1), value);
	}

	inline static int32_t get_offset_of__completed_2() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t890764446, ____completed_2)); }
	inline bool get__completed_2() const { return ____completed_2; }
	inline bool* get_address_of__completed_2() { return &____completed_2; }
	inline void set__completed_2(bool value)
	{
		____completed_2 = value;
	}

	inline static int32_t get_offset_of__count_3() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t890764446, ____count_3)); }
	inline int32_t get__count_3() const { return ____count_3; }
	inline int32_t* get_address_of__count_3() { return &____count_3; }
	inline void set__count_3(int32_t value)
	{
		____count_3 = value;
	}

	inline static int32_t get_offset_of__exception_4() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t890764446, ____exception_4)); }
	inline Exception_t * get__exception_4() const { return ____exception_4; }
	inline Exception_t ** get_address_of__exception_4() { return &____exception_4; }
	inline void set__exception_4(Exception_t * value)
	{
		____exception_4 = value;
		Il2CppCodeGenWriteBarrier((&____exception_4), value);
	}

	inline static int32_t get_offset_of__offset_5() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t890764446, ____offset_5)); }
	inline int32_t get__offset_5() const { return ____offset_5; }
	inline int32_t* get_address_of__offset_5() { return &____offset_5; }
	inline void set__offset_5(int32_t value)
	{
		____offset_5 = value;
	}

	inline static int32_t get_offset_of__state_6() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t890764446, ____state_6)); }
	inline RuntimeObject * get__state_6() const { return ____state_6; }
	inline RuntimeObject ** get_address_of__state_6() { return &____state_6; }
	inline void set__state_6(RuntimeObject * value)
	{
		____state_6 = value;
		Il2CppCodeGenWriteBarrier((&____state_6), value);
	}

	inline static int32_t get_offset_of__sync_7() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t890764446, ____sync_7)); }
	inline RuntimeObject * get__sync_7() const { return ____sync_7; }
	inline RuntimeObject ** get_address_of__sync_7() { return &____sync_7; }
	inline void set__sync_7(RuntimeObject * value)
	{
		____sync_7 = value;
		Il2CppCodeGenWriteBarrier((&____sync_7), value);
	}

	inline static int32_t get_offset_of__syncRead_8() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t890764446, ____syncRead_8)); }
	inline int32_t get__syncRead_8() const { return ____syncRead_8; }
	inline int32_t* get_address_of__syncRead_8() { return &____syncRead_8; }
	inline void set__syncRead_8(int32_t value)
	{
		____syncRead_8 = value;
	}

	inline static int32_t get_offset_of__waitHandle_9() { return static_cast<int32_t>(offsetof(HttpStreamAsyncResult_t890764446, ____waitHandle_9)); }
	inline ManualResetEvent_t451242010 * get__waitHandle_9() const { return ____waitHandle_9; }
	inline ManualResetEvent_t451242010 ** get_address_of__waitHandle_9() { return &____waitHandle_9; }
	inline void set__waitHandle_9(ManualResetEvent_t451242010 * value)
	{
		____waitHandle_9 = value;
		Il2CppCodeGenWriteBarrier((&____waitHandle_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTREAMASYNCRESULT_T890764446_H
#ifndef HTTPVERSION_T1629733732_H
#define HTTPVERSION_T1629733732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpVersion
struct  HttpVersion_t1629733732  : public RuntimeObject
{
public:

public:
};

struct HttpVersion_t1629733732_StaticFields
{
public:
	// System.Version WebSocketSharp.Net.HttpVersion::Version10
	Version_t3456873960 * ___Version10_0;
	// System.Version WebSocketSharp.Net.HttpVersion::Version11
	Version_t3456873960 * ___Version11_1;

public:
	inline static int32_t get_offset_of_Version10_0() { return static_cast<int32_t>(offsetof(HttpVersion_t1629733732_StaticFields, ___Version10_0)); }
	inline Version_t3456873960 * get_Version10_0() const { return ___Version10_0; }
	inline Version_t3456873960 ** get_address_of_Version10_0() { return &___Version10_0; }
	inline void set_Version10_0(Version_t3456873960 * value)
	{
		___Version10_0 = value;
		Il2CppCodeGenWriteBarrier((&___Version10_0), value);
	}

	inline static int32_t get_offset_of_Version11_1() { return static_cast<int32_t>(offsetof(HttpVersion_t1629733732_StaticFields, ___Version11_1)); }
	inline Version_t3456873960 * get_Version11_1() const { return ___Version11_1; }
	inline Version_t3456873960 ** get_address_of_Version11_1() { return &___Version11_1; }
	inline void set_Version11_1(Version_t3456873960 * value)
	{
		___Version11_1 = value;
		Il2CppCodeGenWriteBarrier((&___Version11_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPVERSION_T1629733732_H
#ifndef WEBSOCKETWRAPPER_T3330088300_H
#define WEBSOCKETWRAPPER_T3330088300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.WebSocketWrapper
struct  WebSocketWrapper_t3330088300  : public RuntimeObject
{
public:
	// System.Uri socket.io.WebSocketWrapper::<Url>k__BackingField
	Uri_t100236324 * ___U3CUrlU3Ek__BackingField_0;
	// WebSocketSharp.WebSocket socket.io.WebSocketWrapper::_webSocket
	WebSocket_t62038747 * ____webSocket_1;
	// System.Collections.Generic.Queue`1<System.Byte[]> socket.io.WebSocketWrapper::_messages
	Queue_1_t3962907151 * ____messages_2;
	// System.Collections.Generic.Queue`1<System.String> socket.io.WebSocketWrapper::_errors
	Queue_1_t1693710183 * ____errors_3;
	// System.Object socket.io.WebSocketWrapper::_recvLock
	RuntimeObject * ____recvLock_4;

public:
	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WebSocketWrapper_t3330088300, ___U3CUrlU3Ek__BackingField_0)); }
	inline Uri_t100236324 * get_U3CUrlU3Ek__BackingField_0() const { return ___U3CUrlU3Ek__BackingField_0; }
	inline Uri_t100236324 ** get_address_of_U3CUrlU3Ek__BackingField_0() { return &___U3CUrlU3Ek__BackingField_0; }
	inline void set_U3CUrlU3Ek__BackingField_0(Uri_t100236324 * value)
	{
		___U3CUrlU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUrlU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of__webSocket_1() { return static_cast<int32_t>(offsetof(WebSocketWrapper_t3330088300, ____webSocket_1)); }
	inline WebSocket_t62038747 * get__webSocket_1() const { return ____webSocket_1; }
	inline WebSocket_t62038747 ** get_address_of__webSocket_1() { return &____webSocket_1; }
	inline void set__webSocket_1(WebSocket_t62038747 * value)
	{
		____webSocket_1 = value;
		Il2CppCodeGenWriteBarrier((&____webSocket_1), value);
	}

	inline static int32_t get_offset_of__messages_2() { return static_cast<int32_t>(offsetof(WebSocketWrapper_t3330088300, ____messages_2)); }
	inline Queue_1_t3962907151 * get__messages_2() const { return ____messages_2; }
	inline Queue_1_t3962907151 ** get_address_of__messages_2() { return &____messages_2; }
	inline void set__messages_2(Queue_1_t3962907151 * value)
	{
		____messages_2 = value;
		Il2CppCodeGenWriteBarrier((&____messages_2), value);
	}

	inline static int32_t get_offset_of__errors_3() { return static_cast<int32_t>(offsetof(WebSocketWrapper_t3330088300, ____errors_3)); }
	inline Queue_1_t1693710183 * get__errors_3() const { return ____errors_3; }
	inline Queue_1_t1693710183 ** get_address_of__errors_3() { return &____errors_3; }
	inline void set__errors_3(Queue_1_t1693710183 * value)
	{
		____errors_3 = value;
		Il2CppCodeGenWriteBarrier((&____errors_3), value);
	}

	inline static int32_t get_offset_of__recvLock_4() { return static_cast<int32_t>(offsetof(WebSocketWrapper_t3330088300, ____recvLock_4)); }
	inline RuntimeObject * get__recvLock_4() const { return ____recvLock_4; }
	inline RuntimeObject ** get_address_of__recvLock_4() { return &____recvLock_4; }
	inline void set__recvLock_4(RuntimeObject * value)
	{
		____recvLock_4 = value;
		Il2CppCodeGenWriteBarrier((&____recvLock_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETWRAPPER_T3330088300_H
#ifndef GENERICIDENTITY_T2319019448_H
#define GENERICIDENTITY_T2319019448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.GenericIdentity
struct  GenericIdentity_t2319019448  : public RuntimeObject
{
public:
	// System.String System.Security.Principal.GenericIdentity::m_name
	String_t* ___m_name_0;
	// System.String System.Security.Principal.GenericIdentity::m_type
	String_t* ___m_type_1;

public:
	inline static int32_t get_offset_of_m_name_0() { return static_cast<int32_t>(offsetof(GenericIdentity_t2319019448, ___m_name_0)); }
	inline String_t* get_m_name_0() const { return ___m_name_0; }
	inline String_t** get_address_of_m_name_0() { return &___m_name_0; }
	inline void set_m_name_0(String_t* value)
	{
		___m_name_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_0), value);
	}

	inline static int32_t get_offset_of_m_type_1() { return static_cast<int32_t>(offsetof(GenericIdentity_t2319019448, ___m_type_1)); }
	inline String_t* get_m_type_1() const { return ___m_type_1; }
	inline String_t** get_address_of_m_type_1() { return &___m_type_1; }
	inline void set_m_type_1(String_t* value)
	{
		___m_type_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICIDENTITY_T2319019448_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR0_T3460275121_H
#define U3CGETENUMERATORU3EC__ITERATOR0_T3460275121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0
struct  U3CGetEnumeratorU3Ec__Iterator0_t3460275121  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::$locvar0
	ByteU5BU5D_t4116647657* ___U24locvar0_0;
	// System.Int32 WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::$locvar1
	int32_t ___U24locvar1_1;
	// System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::<b>__1
	uint8_t ___U3CbU3E__1_2;
	// WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::$this
	WebSocketFrame_t3926438742 * ___U24this_3;
	// System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::$current
	uint8_t ___U24current_4;
	// System.Boolean WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 WebSocketSharp.WebSocketFrame/<GetEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3460275121, ___U24locvar0_0)); }
	inline ByteU5BU5D_t4116647657* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline ByteU5BU5D_t4116647657** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(ByteU5BU5D_t4116647657* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U24locvar1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3460275121, ___U24locvar1_1)); }
	inline int32_t get_U24locvar1_1() const { return ___U24locvar1_1; }
	inline int32_t* get_address_of_U24locvar1_1() { return &___U24locvar1_1; }
	inline void set_U24locvar1_1(int32_t value)
	{
		___U24locvar1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbU3E__1_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3460275121, ___U3CbU3E__1_2)); }
	inline uint8_t get_U3CbU3E__1_2() const { return ___U3CbU3E__1_2; }
	inline uint8_t* get_address_of_U3CbU3E__1_2() { return &___U3CbU3E__1_2; }
	inline void set_U3CbU3E__1_2(uint8_t value)
	{
		___U3CbU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3460275121, ___U24this_3)); }
	inline WebSocketFrame_t3926438742 * get_U24this_3() const { return ___U24this_3; }
	inline WebSocketFrame_t3926438742 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(WebSocketFrame_t3926438742 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3460275121, ___U24current_4)); }
	inline uint8_t get_U24current_4() const { return ___U24current_4; }
	inline uint8_t* get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(uint8_t value)
	{
		___U24current_4 = value;
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3460275121, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t3460275121, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR0_T3460275121_H
#ifndef HTTPUTILITY_T3452211165_H
#define HTTPUTILITY_T3452211165_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpUtility
struct  HttpUtility_t3452211165  : public RuntimeObject
{
public:

public:
};

struct HttpUtility_t3452211165_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Char> WebSocketSharp.Net.HttpUtility::_entities
	Dictionary_2_t3419716769 * ____entities_0;
	// System.Char[] WebSocketSharp.Net.HttpUtility::_hexChars
	CharU5BU5D_t3528271667* ____hexChars_1;
	// System.Object WebSocketSharp.Net.HttpUtility::_sync
	RuntimeObject * ____sync_2;

public:
	inline static int32_t get_offset_of__entities_0() { return static_cast<int32_t>(offsetof(HttpUtility_t3452211165_StaticFields, ____entities_0)); }
	inline Dictionary_2_t3419716769 * get__entities_0() const { return ____entities_0; }
	inline Dictionary_2_t3419716769 ** get_address_of__entities_0() { return &____entities_0; }
	inline void set__entities_0(Dictionary_2_t3419716769 * value)
	{
		____entities_0 = value;
		Il2CppCodeGenWriteBarrier((&____entities_0), value);
	}

	inline static int32_t get_offset_of__hexChars_1() { return static_cast<int32_t>(offsetof(HttpUtility_t3452211165_StaticFields, ____hexChars_1)); }
	inline CharU5BU5D_t3528271667* get__hexChars_1() const { return ____hexChars_1; }
	inline CharU5BU5D_t3528271667** get_address_of__hexChars_1() { return &____hexChars_1; }
	inline void set__hexChars_1(CharU5BU5D_t3528271667* value)
	{
		____hexChars_1 = value;
		Il2CppCodeGenWriteBarrier((&____hexChars_1), value);
	}

	inline static int32_t get_offset_of__sync_2() { return static_cast<int32_t>(offsetof(HttpUtility_t3452211165_StaticFields, ____sync_2)); }
	inline RuntimeObject * get__sync_2() const { return ____sync_2; }
	inline RuntimeObject ** get_address_of__sync_2() { return &____sync_2; }
	inline void set__sync_2(RuntimeObject * value)
	{
		____sync_2 = value;
		Il2CppCodeGenWriteBarrier((&____sync_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPUTILITY_T3452211165_H
#ifndef U3CCLOSEU3EC__ANONSTOREY1_T3055466982_H
#define U3CCLOSEU3EC__ANONSTOREY1_T3055466982_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerResponse/<Close>c__AnonStorey1
struct  U3CCloseU3Ec__AnonStorey1_t3055466982  : public RuntimeObject
{
public:
	// System.IO.Stream WebSocketSharp.Net.HttpListenerResponse/<Close>c__AnonStorey1::output
	Stream_t1273022909 * ___output_0;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Net.HttpListenerResponse/<Close>c__AnonStorey1::$this
	HttpListenerResponse_t2818529495 * ___U24this_1;

public:
	inline static int32_t get_offset_of_output_0() { return static_cast<int32_t>(offsetof(U3CCloseU3Ec__AnonStorey1_t3055466982, ___output_0)); }
	inline Stream_t1273022909 * get_output_0() const { return ___output_0; }
	inline Stream_t1273022909 ** get_address_of_output_0() { return &___output_0; }
	inline void set_output_0(Stream_t1273022909 * value)
	{
		___output_0 = value;
		Il2CppCodeGenWriteBarrier((&___output_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCloseU3Ec__AnonStorey1_t3055466982, ___U24this_1)); }
	inline HttpListenerResponse_t2818529495 * get_U24this_1() const { return ___U24this_1; }
	inline HttpListenerResponse_t2818529495 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(HttpListenerResponse_t2818529495 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCLOSEU3EC__ANONSTOREY1_T3055466982_H
#ifndef U3CFINDCOOKIEU3EC__ITERATOR0_T3171476380_H
#define U3CFINDCOOKIEU3EC__ITERATOR0_T3171476380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0
struct  U3CfindCookieU3Ec__Iterator0_t3171476380  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.Cookie WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::cookie
	Cookie_t4203102285 * ___cookie_0;
	// System.String WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::<name>__0
	String_t* ___U3CnameU3E__0_1;
	// System.String WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::<domain>__0
	String_t* ___U3CdomainU3E__0_2;
	// System.String WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::<path>__0
	String_t* ___U3CpathU3E__0_3;
	// System.Collections.IEnumerator WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_4;
	// WebSocketSharp.Net.Cookie WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::<c>__1
	Cookie_t4203102285 * ___U3CcU3E__1_5;
	// System.IDisposable WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::$locvar1
	RuntimeObject* ___U24locvar1_6;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::$this
	HttpListenerResponse_t2818529495 * ___U24this_7;
	// WebSocketSharp.Net.Cookie WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::$current
	Cookie_t4203102285 * ___U24current_8;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::$disposing
	bool ___U24disposing_9;
	// System.Int32 WebSocketSharp.Net.HttpListenerResponse/<findCookie>c__Iterator0::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_cookie_0() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t3171476380, ___cookie_0)); }
	inline Cookie_t4203102285 * get_cookie_0() const { return ___cookie_0; }
	inline Cookie_t4203102285 ** get_address_of_cookie_0() { return &___cookie_0; }
	inline void set_cookie_0(Cookie_t4203102285 * value)
	{
		___cookie_0 = value;
		Il2CppCodeGenWriteBarrier((&___cookie_0), value);
	}

	inline static int32_t get_offset_of_U3CnameU3E__0_1() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t3171476380, ___U3CnameU3E__0_1)); }
	inline String_t* get_U3CnameU3E__0_1() const { return ___U3CnameU3E__0_1; }
	inline String_t** get_address_of_U3CnameU3E__0_1() { return &___U3CnameU3E__0_1; }
	inline void set_U3CnameU3E__0_1(String_t* value)
	{
		___U3CnameU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnameU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CdomainU3E__0_2() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t3171476380, ___U3CdomainU3E__0_2)); }
	inline String_t* get_U3CdomainU3E__0_2() const { return ___U3CdomainU3E__0_2; }
	inline String_t** get_address_of_U3CdomainU3E__0_2() { return &___U3CdomainU3E__0_2; }
	inline void set_U3CdomainU3E__0_2(String_t* value)
	{
		___U3CdomainU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdomainU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CpathU3E__0_3() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t3171476380, ___U3CpathU3E__0_3)); }
	inline String_t* get_U3CpathU3E__0_3() const { return ___U3CpathU3E__0_3; }
	inline String_t** get_address_of_U3CpathU3E__0_3() { return &___U3CpathU3E__0_3; }
	inline void set_U3CpathU3E__0_3(String_t* value)
	{
		___U3CpathU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24locvar0_4() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t3171476380, ___U24locvar0_4)); }
	inline RuntimeObject* get_U24locvar0_4() const { return ___U24locvar0_4; }
	inline RuntimeObject** get_address_of_U24locvar0_4() { return &___U24locvar0_4; }
	inline void set_U24locvar0_4(RuntimeObject* value)
	{
		___U24locvar0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_4), value);
	}

	inline static int32_t get_offset_of_U3CcU3E__1_5() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t3171476380, ___U3CcU3E__1_5)); }
	inline Cookie_t4203102285 * get_U3CcU3E__1_5() const { return ___U3CcU3E__1_5; }
	inline Cookie_t4203102285 ** get_address_of_U3CcU3E__1_5() { return &___U3CcU3E__1_5; }
	inline void set_U3CcU3E__1_5(Cookie_t4203102285 * value)
	{
		___U3CcU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U24locvar1_6() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t3171476380, ___U24locvar1_6)); }
	inline RuntimeObject* get_U24locvar1_6() const { return ___U24locvar1_6; }
	inline RuntimeObject** get_address_of_U24locvar1_6() { return &___U24locvar1_6; }
	inline void set_U24locvar1_6(RuntimeObject* value)
	{
		___U24locvar1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_6), value);
	}

	inline static int32_t get_offset_of_U24this_7() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t3171476380, ___U24this_7)); }
	inline HttpListenerResponse_t2818529495 * get_U24this_7() const { return ___U24this_7; }
	inline HttpListenerResponse_t2818529495 ** get_address_of_U24this_7() { return &___U24this_7; }
	inline void set_U24this_7(HttpListenerResponse_t2818529495 * value)
	{
		___U24this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_7), value);
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t3171476380, ___U24current_8)); }
	inline Cookie_t4203102285 * get_U24current_8() const { return ___U24current_8; }
	inline Cookie_t4203102285 ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Cookie_t4203102285 * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t3171476380, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CfindCookieU3Ec__Iterator0_t3171476380, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFINDCOOKIEU3EC__ITERATOR0_T3171476380_H
#ifndef HTTPLISTENERRESPONSE_T2818529495_H
#define HTTPLISTENERRESPONSE_T2818529495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerResponse
struct  HttpListenerResponse_t2818529495  : public RuntimeObject
{
public:
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_closeConnection
	bool ____closeConnection_0;
	// System.Text.Encoding WebSocketSharp.Net.HttpListenerResponse::_contentEncoding
	Encoding_t1523322056 * ____contentEncoding_1;
	// System.Int64 WebSocketSharp.Net.HttpListenerResponse::_contentLength
	int64_t ____contentLength_2;
	// System.String WebSocketSharp.Net.HttpListenerResponse::_contentType
	String_t* ____contentType_3;
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.HttpListenerResponse::_context
	HttpListenerContext_t3723273891 * ____context_4;
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.HttpListenerResponse::_cookies
	CookieCollection_t962330244 * ____cookies_5;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_disposed
	bool ____disposed_6;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.HttpListenerResponse::_headers
	WebHeaderCollection_t1205255311 * ____headers_7;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_headersSent
	bool ____headersSent_8;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_keepAlive
	bool ____keepAlive_9;
	// System.String WebSocketSharp.Net.HttpListenerResponse::_location
	String_t* ____location_10;
	// WebSocketSharp.Net.ResponseStream WebSocketSharp.Net.HttpListenerResponse::_outputStream
	ResponseStream_t2828822308 * ____outputStream_11;
	// System.Boolean WebSocketSharp.Net.HttpListenerResponse::_sendChunked
	bool ____sendChunked_12;
	// System.Int32 WebSocketSharp.Net.HttpListenerResponse::_statusCode
	int32_t ____statusCode_13;
	// System.String WebSocketSharp.Net.HttpListenerResponse::_statusDescription
	String_t* ____statusDescription_14;
	// System.Version WebSocketSharp.Net.HttpListenerResponse::_version
	Version_t3456873960 * ____version_15;

public:
	inline static int32_t get_offset_of__closeConnection_0() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____closeConnection_0)); }
	inline bool get__closeConnection_0() const { return ____closeConnection_0; }
	inline bool* get_address_of__closeConnection_0() { return &____closeConnection_0; }
	inline void set__closeConnection_0(bool value)
	{
		____closeConnection_0 = value;
	}

	inline static int32_t get_offset_of__contentEncoding_1() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____contentEncoding_1)); }
	inline Encoding_t1523322056 * get__contentEncoding_1() const { return ____contentEncoding_1; }
	inline Encoding_t1523322056 ** get_address_of__contentEncoding_1() { return &____contentEncoding_1; }
	inline void set__contentEncoding_1(Encoding_t1523322056 * value)
	{
		____contentEncoding_1 = value;
		Il2CppCodeGenWriteBarrier((&____contentEncoding_1), value);
	}

	inline static int32_t get_offset_of__contentLength_2() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____contentLength_2)); }
	inline int64_t get__contentLength_2() const { return ____contentLength_2; }
	inline int64_t* get_address_of__contentLength_2() { return &____contentLength_2; }
	inline void set__contentLength_2(int64_t value)
	{
		____contentLength_2 = value;
	}

	inline static int32_t get_offset_of__contentType_3() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____contentType_3)); }
	inline String_t* get__contentType_3() const { return ____contentType_3; }
	inline String_t** get_address_of__contentType_3() { return &____contentType_3; }
	inline void set__contentType_3(String_t* value)
	{
		____contentType_3 = value;
		Il2CppCodeGenWriteBarrier((&____contentType_3), value);
	}

	inline static int32_t get_offset_of__context_4() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____context_4)); }
	inline HttpListenerContext_t3723273891 * get__context_4() const { return ____context_4; }
	inline HttpListenerContext_t3723273891 ** get_address_of__context_4() { return &____context_4; }
	inline void set__context_4(HttpListenerContext_t3723273891 * value)
	{
		____context_4 = value;
		Il2CppCodeGenWriteBarrier((&____context_4), value);
	}

	inline static int32_t get_offset_of__cookies_5() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____cookies_5)); }
	inline CookieCollection_t962330244 * get__cookies_5() const { return ____cookies_5; }
	inline CookieCollection_t962330244 ** get_address_of__cookies_5() { return &____cookies_5; }
	inline void set__cookies_5(CookieCollection_t962330244 * value)
	{
		____cookies_5 = value;
		Il2CppCodeGenWriteBarrier((&____cookies_5), value);
	}

	inline static int32_t get_offset_of__disposed_6() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____disposed_6)); }
	inline bool get__disposed_6() const { return ____disposed_6; }
	inline bool* get_address_of__disposed_6() { return &____disposed_6; }
	inline void set__disposed_6(bool value)
	{
		____disposed_6 = value;
	}

	inline static int32_t get_offset_of__headers_7() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____headers_7)); }
	inline WebHeaderCollection_t1205255311 * get__headers_7() const { return ____headers_7; }
	inline WebHeaderCollection_t1205255311 ** get_address_of__headers_7() { return &____headers_7; }
	inline void set__headers_7(WebHeaderCollection_t1205255311 * value)
	{
		____headers_7 = value;
		Il2CppCodeGenWriteBarrier((&____headers_7), value);
	}

	inline static int32_t get_offset_of__headersSent_8() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____headersSent_8)); }
	inline bool get__headersSent_8() const { return ____headersSent_8; }
	inline bool* get_address_of__headersSent_8() { return &____headersSent_8; }
	inline void set__headersSent_8(bool value)
	{
		____headersSent_8 = value;
	}

	inline static int32_t get_offset_of__keepAlive_9() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____keepAlive_9)); }
	inline bool get__keepAlive_9() const { return ____keepAlive_9; }
	inline bool* get_address_of__keepAlive_9() { return &____keepAlive_9; }
	inline void set__keepAlive_9(bool value)
	{
		____keepAlive_9 = value;
	}

	inline static int32_t get_offset_of__location_10() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____location_10)); }
	inline String_t* get__location_10() const { return ____location_10; }
	inline String_t** get_address_of__location_10() { return &____location_10; }
	inline void set__location_10(String_t* value)
	{
		____location_10 = value;
		Il2CppCodeGenWriteBarrier((&____location_10), value);
	}

	inline static int32_t get_offset_of__outputStream_11() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____outputStream_11)); }
	inline ResponseStream_t2828822308 * get__outputStream_11() const { return ____outputStream_11; }
	inline ResponseStream_t2828822308 ** get_address_of__outputStream_11() { return &____outputStream_11; }
	inline void set__outputStream_11(ResponseStream_t2828822308 * value)
	{
		____outputStream_11 = value;
		Il2CppCodeGenWriteBarrier((&____outputStream_11), value);
	}

	inline static int32_t get_offset_of__sendChunked_12() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____sendChunked_12)); }
	inline bool get__sendChunked_12() const { return ____sendChunked_12; }
	inline bool* get_address_of__sendChunked_12() { return &____sendChunked_12; }
	inline void set__sendChunked_12(bool value)
	{
		____sendChunked_12 = value;
	}

	inline static int32_t get_offset_of__statusCode_13() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____statusCode_13)); }
	inline int32_t get__statusCode_13() const { return ____statusCode_13; }
	inline int32_t* get_address_of__statusCode_13() { return &____statusCode_13; }
	inline void set__statusCode_13(int32_t value)
	{
		____statusCode_13 = value;
	}

	inline static int32_t get_offset_of__statusDescription_14() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____statusDescription_14)); }
	inline String_t* get__statusDescription_14() const { return ____statusDescription_14; }
	inline String_t** get_address_of__statusDescription_14() { return &____statusDescription_14; }
	inline void set__statusDescription_14(String_t* value)
	{
		____statusDescription_14 = value;
		Il2CppCodeGenWriteBarrier((&____statusDescription_14), value);
	}

	inline static int32_t get_offset_of__version_15() { return static_cast<int32_t>(offsetof(HttpListenerResponse_t2818529495, ____version_15)); }
	inline Version_t3456873960 * get__version_15() const { return ____version_15; }
	inline Version_t3456873960 ** get_address_of__version_15() { return &____version_15; }
	inline void set__version_15(Version_t3456873960 * value)
	{
		____version_15 = value;
		Il2CppCodeGenWriteBarrier((&____version_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERRESPONSE_T2818529495_H
#ifndef U3CREADFRAMEASYNCU3EC__ANONSTOREY7_T2345772275_H
#define U3CREADFRAMEASYNCU3EC__ANONSTOREY7_T2345772275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7
struct  U3CReadFrameAsyncU3Ec__AnonStorey7_t2345772275  : public RuntimeObject
{
public:
	// System.IO.Stream WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::stream
	Stream_t1273022909 * ___stream_0;
	// System.Action`1<System.Exception> WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::error
	Action_1_t1609204844 * ___error_1;
	// System.Boolean WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::unmask
	bool ___unmask_2;
	// System.Action`1<WebSocketSharp.WebSocketFrame> WebSocketSharp.WebSocketFrame/<ReadFrameAsync>c__AnonStorey7::completed
	Action_1_t4098906337 * ___completed_3;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CReadFrameAsyncU3Ec__AnonStorey7_t2345772275, ___stream_0)); }
	inline Stream_t1273022909 * get_stream_0() const { return ___stream_0; }
	inline Stream_t1273022909 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Stream_t1273022909 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___stream_0), value);
	}

	inline static int32_t get_offset_of_error_1() { return static_cast<int32_t>(offsetof(U3CReadFrameAsyncU3Ec__AnonStorey7_t2345772275, ___error_1)); }
	inline Action_1_t1609204844 * get_error_1() const { return ___error_1; }
	inline Action_1_t1609204844 ** get_address_of_error_1() { return &___error_1; }
	inline void set_error_1(Action_1_t1609204844 * value)
	{
		___error_1 = value;
		Il2CppCodeGenWriteBarrier((&___error_1), value);
	}

	inline static int32_t get_offset_of_unmask_2() { return static_cast<int32_t>(offsetof(U3CReadFrameAsyncU3Ec__AnonStorey7_t2345772275, ___unmask_2)); }
	inline bool get_unmask_2() const { return ___unmask_2; }
	inline bool* get_address_of_unmask_2() { return &___unmask_2; }
	inline void set_unmask_2(bool value)
	{
		___unmask_2 = value;
	}

	inline static int32_t get_offset_of_completed_3() { return static_cast<int32_t>(offsetof(U3CReadFrameAsyncU3Ec__AnonStorey7_t2345772275, ___completed_3)); }
	inline Action_1_t4098906337 * get_completed_3() const { return ___completed_3; }
	inline Action_1_t4098906337 ** get_address_of_completed_3() { return &___completed_3; }
	inline void set_completed_3(Action_1_t4098906337 * value)
	{
		___completed_3 = value;
		Il2CppCodeGenWriteBarrier((&___completed_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADFRAMEASYNCU3EC__ANONSTOREY7_T2345772275_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t386037858 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t386037858 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef NAMEVALUECOLLECTION_T407452768_H
#define NAMEVALUECOLLECTION_T407452768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_t407452768  : public NameObjectCollectionBase_t2091847364
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAllKeys
	StringU5BU5D_t1281789340* ___cachedAllKeys_10;
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAll
	StringU5BU5D_t1281789340* ___cachedAll_11;

public:
	inline static int32_t get_offset_of_cachedAllKeys_10() { return static_cast<int32_t>(offsetof(NameValueCollection_t407452768, ___cachedAllKeys_10)); }
	inline StringU5BU5D_t1281789340* get_cachedAllKeys_10() const { return ___cachedAllKeys_10; }
	inline StringU5BU5D_t1281789340** get_address_of_cachedAllKeys_10() { return &___cachedAllKeys_10; }
	inline void set_cachedAllKeys_10(StringU5BU5D_t1281789340* value)
	{
		___cachedAllKeys_10 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAllKeys_10), value);
	}

	inline static int32_t get_offset_of_cachedAll_11() { return static_cast<int32_t>(offsetof(NameValueCollection_t407452768, ___cachedAll_11)); }
	inline StringU5BU5D_t1281789340* get_cachedAll_11() const { return ___cachedAll_11; }
	inline StringU5BU5D_t1281789340** get_address_of_cachedAll_11() { return &___cachedAll_11; }
	inline void set_cachedAll_11(StringU5BU5D_t1281789340* value)
	{
		___cachedAll_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAll_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUECOLLECTION_T407452768_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef KEYVALUEPAIR_2_T2280216431_H
#define KEYVALUEPAIR_2_T2280216431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
struct  KeyValuePair_2_t2280216431 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	bool ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2280216431, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2280216431, ___value_1)); }
	inline bool get_value_1() const { return ___value_1; }
	inline bool* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(bool value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2280216431_H
#ifndef HTTPLISTENERWEBSOCKETCONTEXT_T3770402993_H
#define HTTPLISTENERWEBSOCKETCONTEXT_T3770402993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext
struct  HttpListenerWebSocketContext_t3770402993  : public WebSocketContext_t619421455
{
public:
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext::_context
	HttpListenerContext_t3723273891 * ____context_0;
	// WebSocketSharp.WebSocket WebSocketSharp.Net.WebSockets.HttpListenerWebSocketContext::_websocket
	WebSocket_t62038747 * ____websocket_1;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(HttpListenerWebSocketContext_t3770402993, ____context_0)); }
	inline HttpListenerContext_t3723273891 * get__context_0() const { return ____context_0; }
	inline HttpListenerContext_t3723273891 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(HttpListenerContext_t3723273891 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}

	inline static int32_t get_offset_of__websocket_1() { return static_cast<int32_t>(offsetof(HttpListenerWebSocketContext_t3770402993, ____websocket_1)); }
	inline WebSocket_t62038747 * get__websocket_1() const { return ____websocket_1; }
	inline WebSocket_t62038747 ** get_address_of__websocket_1() { return &____websocket_1; }
	inline void set__websocket_1(WebSocket_t62038747 * value)
	{
		____websocket_1 = value;
		Il2CppCodeGenWriteBarrier((&____websocket_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERWEBSOCKETCONTEXT_T3770402993_H
#ifndef TCPLISTENERWEBSOCKETCONTEXT_T2376318492_H
#define TCPLISTENERWEBSOCKETCONTEXT_T2376318492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext
struct  TcpListenerWebSocketContext_t2376318492  : public WebSocketContext_t619421455
{
public:
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_cookies
	CookieCollection_t962330244 * ____cookies_0;
	// WebSocketSharp.Logger WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_logger
	Logger_t4025333586 * ____logger_1;
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_queryString
	NameValueCollection_t407452768 * ____queryString_2;
	// WebSocketSharp.HttpRequest WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_request
	HttpRequest_t1778308387 * ____request_3;
	// System.Boolean WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_secure
	bool ____secure_4;
	// System.IO.Stream WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_stream
	Stream_t1273022909 * ____stream_5;
	// System.Net.Sockets.TcpClient WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_tcpClient
	TcpClient_t822906377 * ____tcpClient_6;
	// System.Uri WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_uri
	Uri_t100236324 * ____uri_7;
	// System.Security.Principal.IPrincipal WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_user
	RuntimeObject* ____user_8;
	// WebSocketSharp.WebSocket WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext::_websocket
	WebSocket_t62038747 * ____websocket_9;

public:
	inline static int32_t get_offset_of__cookies_0() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t2376318492, ____cookies_0)); }
	inline CookieCollection_t962330244 * get__cookies_0() const { return ____cookies_0; }
	inline CookieCollection_t962330244 ** get_address_of__cookies_0() { return &____cookies_0; }
	inline void set__cookies_0(CookieCollection_t962330244 * value)
	{
		____cookies_0 = value;
		Il2CppCodeGenWriteBarrier((&____cookies_0), value);
	}

	inline static int32_t get_offset_of__logger_1() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t2376318492, ____logger_1)); }
	inline Logger_t4025333586 * get__logger_1() const { return ____logger_1; }
	inline Logger_t4025333586 ** get_address_of__logger_1() { return &____logger_1; }
	inline void set__logger_1(Logger_t4025333586 * value)
	{
		____logger_1 = value;
		Il2CppCodeGenWriteBarrier((&____logger_1), value);
	}

	inline static int32_t get_offset_of__queryString_2() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t2376318492, ____queryString_2)); }
	inline NameValueCollection_t407452768 * get__queryString_2() const { return ____queryString_2; }
	inline NameValueCollection_t407452768 ** get_address_of__queryString_2() { return &____queryString_2; }
	inline void set__queryString_2(NameValueCollection_t407452768 * value)
	{
		____queryString_2 = value;
		Il2CppCodeGenWriteBarrier((&____queryString_2), value);
	}

	inline static int32_t get_offset_of__request_3() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t2376318492, ____request_3)); }
	inline HttpRequest_t1778308387 * get__request_3() const { return ____request_3; }
	inline HttpRequest_t1778308387 ** get_address_of__request_3() { return &____request_3; }
	inline void set__request_3(HttpRequest_t1778308387 * value)
	{
		____request_3 = value;
		Il2CppCodeGenWriteBarrier((&____request_3), value);
	}

	inline static int32_t get_offset_of__secure_4() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t2376318492, ____secure_4)); }
	inline bool get__secure_4() const { return ____secure_4; }
	inline bool* get_address_of__secure_4() { return &____secure_4; }
	inline void set__secure_4(bool value)
	{
		____secure_4 = value;
	}

	inline static int32_t get_offset_of__stream_5() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t2376318492, ____stream_5)); }
	inline Stream_t1273022909 * get__stream_5() const { return ____stream_5; }
	inline Stream_t1273022909 ** get_address_of__stream_5() { return &____stream_5; }
	inline void set__stream_5(Stream_t1273022909 * value)
	{
		____stream_5 = value;
		Il2CppCodeGenWriteBarrier((&____stream_5), value);
	}

	inline static int32_t get_offset_of__tcpClient_6() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t2376318492, ____tcpClient_6)); }
	inline TcpClient_t822906377 * get__tcpClient_6() const { return ____tcpClient_6; }
	inline TcpClient_t822906377 ** get_address_of__tcpClient_6() { return &____tcpClient_6; }
	inline void set__tcpClient_6(TcpClient_t822906377 * value)
	{
		____tcpClient_6 = value;
		Il2CppCodeGenWriteBarrier((&____tcpClient_6), value);
	}

	inline static int32_t get_offset_of__uri_7() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t2376318492, ____uri_7)); }
	inline Uri_t100236324 * get__uri_7() const { return ____uri_7; }
	inline Uri_t100236324 ** get_address_of__uri_7() { return &____uri_7; }
	inline void set__uri_7(Uri_t100236324 * value)
	{
		____uri_7 = value;
		Il2CppCodeGenWriteBarrier((&____uri_7), value);
	}

	inline static int32_t get_offset_of__user_8() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t2376318492, ____user_8)); }
	inline RuntimeObject* get__user_8() const { return ____user_8; }
	inline RuntimeObject** get_address_of__user_8() { return &____user_8; }
	inline void set__user_8(RuntimeObject* value)
	{
		____user_8 = value;
		Il2CppCodeGenWriteBarrier((&____user_8), value);
	}

	inline static int32_t get_offset_of__websocket_9() { return static_cast<int32_t>(offsetof(TcpListenerWebSocketContext_t2376318492, ____websocket_9)); }
	inline WebSocket_t62038747 * get__websocket_9() const { return ____websocket_9; }
	inline WebSocket_t62038747 ** get_address_of__websocket_9() { return &____websocket_9; }
	inline void set__websocket_9(WebSocket_t62038747 * value)
	{
		____websocket_9 = value;
		Il2CppCodeGenWriteBarrier((&____websocket_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TCPLISTENERWEBSOCKETCONTEXT_T2376318492_H
#ifndef RESPONSESTREAM_T2828822308_H
#define RESPONSESTREAM_T2828822308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ResponseStream
struct  ResponseStream_t2828822308  : public Stream_t1273022909
{
public:
	// System.IO.MemoryStream WebSocketSharp.Net.ResponseStream::_body
	MemoryStream_t94973147 * ____body_1;
	// System.Boolean WebSocketSharp.Net.ResponseStream::_disposed
	bool ____disposed_3;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Net.ResponseStream::_response
	HttpListenerResponse_t2818529495 * ____response_4;
	// System.Boolean WebSocketSharp.Net.ResponseStream::_sendChunked
	bool ____sendChunked_5;
	// System.IO.Stream WebSocketSharp.Net.ResponseStream::_stream
	Stream_t1273022909 * ____stream_6;
	// System.Action`3<System.Byte[],System.Int32,System.Int32> WebSocketSharp.Net.ResponseStream::_write
	Action_3_t1284011258 * ____write_7;
	// System.Action`3<System.Byte[],System.Int32,System.Int32> WebSocketSharp.Net.ResponseStream::_writeBody
	Action_3_t1284011258 * ____writeBody_8;
	// System.Action`3<System.Byte[],System.Int32,System.Int32> WebSocketSharp.Net.ResponseStream::_writeChunked
	Action_3_t1284011258 * ____writeChunked_9;

public:
	inline static int32_t get_offset_of__body_1() { return static_cast<int32_t>(offsetof(ResponseStream_t2828822308, ____body_1)); }
	inline MemoryStream_t94973147 * get__body_1() const { return ____body_1; }
	inline MemoryStream_t94973147 ** get_address_of__body_1() { return &____body_1; }
	inline void set__body_1(MemoryStream_t94973147 * value)
	{
		____body_1 = value;
		Il2CppCodeGenWriteBarrier((&____body_1), value);
	}

	inline static int32_t get_offset_of__disposed_3() { return static_cast<int32_t>(offsetof(ResponseStream_t2828822308, ____disposed_3)); }
	inline bool get__disposed_3() const { return ____disposed_3; }
	inline bool* get_address_of__disposed_3() { return &____disposed_3; }
	inline void set__disposed_3(bool value)
	{
		____disposed_3 = value;
	}

	inline static int32_t get_offset_of__response_4() { return static_cast<int32_t>(offsetof(ResponseStream_t2828822308, ____response_4)); }
	inline HttpListenerResponse_t2818529495 * get__response_4() const { return ____response_4; }
	inline HttpListenerResponse_t2818529495 ** get_address_of__response_4() { return &____response_4; }
	inline void set__response_4(HttpListenerResponse_t2818529495 * value)
	{
		____response_4 = value;
		Il2CppCodeGenWriteBarrier((&____response_4), value);
	}

	inline static int32_t get_offset_of__sendChunked_5() { return static_cast<int32_t>(offsetof(ResponseStream_t2828822308, ____sendChunked_5)); }
	inline bool get__sendChunked_5() const { return ____sendChunked_5; }
	inline bool* get_address_of__sendChunked_5() { return &____sendChunked_5; }
	inline void set__sendChunked_5(bool value)
	{
		____sendChunked_5 = value;
	}

	inline static int32_t get_offset_of__stream_6() { return static_cast<int32_t>(offsetof(ResponseStream_t2828822308, ____stream_6)); }
	inline Stream_t1273022909 * get__stream_6() const { return ____stream_6; }
	inline Stream_t1273022909 ** get_address_of__stream_6() { return &____stream_6; }
	inline void set__stream_6(Stream_t1273022909 * value)
	{
		____stream_6 = value;
		Il2CppCodeGenWriteBarrier((&____stream_6), value);
	}

	inline static int32_t get_offset_of__write_7() { return static_cast<int32_t>(offsetof(ResponseStream_t2828822308, ____write_7)); }
	inline Action_3_t1284011258 * get__write_7() const { return ____write_7; }
	inline Action_3_t1284011258 ** get_address_of__write_7() { return &____write_7; }
	inline void set__write_7(Action_3_t1284011258 * value)
	{
		____write_7 = value;
		Il2CppCodeGenWriteBarrier((&____write_7), value);
	}

	inline static int32_t get_offset_of__writeBody_8() { return static_cast<int32_t>(offsetof(ResponseStream_t2828822308, ____writeBody_8)); }
	inline Action_3_t1284011258 * get__writeBody_8() const { return ____writeBody_8; }
	inline Action_3_t1284011258 ** get_address_of__writeBody_8() { return &____writeBody_8; }
	inline void set__writeBody_8(Action_3_t1284011258 * value)
	{
		____writeBody_8 = value;
		Il2CppCodeGenWriteBarrier((&____writeBody_8), value);
	}

	inline static int32_t get_offset_of__writeChunked_9() { return static_cast<int32_t>(offsetof(ResponseStream_t2828822308, ____writeChunked_9)); }
	inline Action_3_t1284011258 * get__writeChunked_9() const { return ____writeChunked_9; }
	inline Action_3_t1284011258 ** get_address_of__writeChunked_9() { return &____writeChunked_9; }
	inline void set__writeChunked_9(Action_3_t1284011258 * value)
	{
		____writeChunked_9 = value;
		Il2CppCodeGenWriteBarrier((&____writeChunked_9), value);
	}
};

struct ResponseStream_t2828822308_StaticFields
{
public:
	// System.Byte[] WebSocketSharp.Net.ResponseStream::_crlf
	ByteU5BU5D_t4116647657* ____crlf_2;

public:
	inline static int32_t get_offset_of__crlf_2() { return static_cast<int32_t>(offsetof(ResponseStream_t2828822308_StaticFields, ____crlf_2)); }
	inline ByteU5BU5D_t4116647657* get__crlf_2() const { return ____crlf_2; }
	inline ByteU5BU5D_t4116647657** get_address_of__crlf_2() { return &____crlf_2; }
	inline void set__crlf_2(ByteU5BU5D_t4116647657* value)
	{
		____crlf_2 = value;
		Il2CppCodeGenWriteBarrier((&____crlf_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESPONSESTREAM_T2828822308_H
#ifndef HTTPREQUESTEVENTARGS_T1003470709_H
#define HTTPREQUESTEVENTARGS_T1003470709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.HttpRequestEventArgs
struct  HttpRequestEventArgs_t1003470709  : public EventArgs_t3591816995
{
public:
	// WebSocketSharp.Net.HttpListenerRequest WebSocketSharp.Server.HttpRequestEventArgs::_request
	HttpListenerRequest_t2959552699 * ____request_1;
	// WebSocketSharp.Net.HttpListenerResponse WebSocketSharp.Server.HttpRequestEventArgs::_response
	HttpListenerResponse_t2818529495 * ____response_2;

public:
	inline static int32_t get_offset_of__request_1() { return static_cast<int32_t>(offsetof(HttpRequestEventArgs_t1003470709, ____request_1)); }
	inline HttpListenerRequest_t2959552699 * get__request_1() const { return ____request_1; }
	inline HttpListenerRequest_t2959552699 ** get_address_of__request_1() { return &____request_1; }
	inline void set__request_1(HttpListenerRequest_t2959552699 * value)
	{
		____request_1 = value;
		Il2CppCodeGenWriteBarrier((&____request_1), value);
	}

	inline static int32_t get_offset_of__response_2() { return static_cast<int32_t>(offsetof(HttpRequestEventArgs_t1003470709, ____response_2)); }
	inline HttpListenerResponse_t2818529495 * get__response_2() const { return ____response_2; }
	inline HttpListenerResponse_t2818529495 ** get_address_of__response_2() { return &____response_2; }
	inline void set__response_2(HttpListenerResponse_t2818529495 * value)
	{
		____response_2 = value;
		Il2CppCodeGenWriteBarrier((&____response_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTEVENTARGS_T1003470709_H
#ifndef REQUESTSTREAM_T1020063535_H
#define REQUESTSTREAM_T1020063535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.RequestStream
struct  RequestStream_t1020063535  : public Stream_t1273022909
{
public:
	// System.Int64 WebSocketSharp.Net.RequestStream::_bodyLeft
	int64_t ____bodyLeft_1;
	// System.Byte[] WebSocketSharp.Net.RequestStream::_buffer
	ByteU5BU5D_t4116647657* ____buffer_2;
	// System.Int32 WebSocketSharp.Net.RequestStream::_count
	int32_t ____count_3;
	// System.Boolean WebSocketSharp.Net.RequestStream::_disposed
	bool ____disposed_4;
	// System.Int32 WebSocketSharp.Net.RequestStream::_offset
	int32_t ____offset_5;
	// System.IO.Stream WebSocketSharp.Net.RequestStream::_stream
	Stream_t1273022909 * ____stream_6;

public:
	inline static int32_t get_offset_of__bodyLeft_1() { return static_cast<int32_t>(offsetof(RequestStream_t1020063535, ____bodyLeft_1)); }
	inline int64_t get__bodyLeft_1() const { return ____bodyLeft_1; }
	inline int64_t* get_address_of__bodyLeft_1() { return &____bodyLeft_1; }
	inline void set__bodyLeft_1(int64_t value)
	{
		____bodyLeft_1 = value;
	}

	inline static int32_t get_offset_of__buffer_2() { return static_cast<int32_t>(offsetof(RequestStream_t1020063535, ____buffer_2)); }
	inline ByteU5BU5D_t4116647657* get__buffer_2() const { return ____buffer_2; }
	inline ByteU5BU5D_t4116647657** get_address_of__buffer_2() { return &____buffer_2; }
	inline void set__buffer_2(ByteU5BU5D_t4116647657* value)
	{
		____buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_2), value);
	}

	inline static int32_t get_offset_of__count_3() { return static_cast<int32_t>(offsetof(RequestStream_t1020063535, ____count_3)); }
	inline int32_t get__count_3() const { return ____count_3; }
	inline int32_t* get_address_of__count_3() { return &____count_3; }
	inline void set__count_3(int32_t value)
	{
		____count_3 = value;
	}

	inline static int32_t get_offset_of__disposed_4() { return static_cast<int32_t>(offsetof(RequestStream_t1020063535, ____disposed_4)); }
	inline bool get__disposed_4() const { return ____disposed_4; }
	inline bool* get_address_of__disposed_4() { return &____disposed_4; }
	inline void set__disposed_4(bool value)
	{
		____disposed_4 = value;
	}

	inline static int32_t get_offset_of__offset_5() { return static_cast<int32_t>(offsetof(RequestStream_t1020063535, ____offset_5)); }
	inline int32_t get__offset_5() const { return ____offset_5; }
	inline int32_t* get_address_of__offset_5() { return &____offset_5; }
	inline void set__offset_5(int32_t value)
	{
		____offset_5 = value;
	}

	inline static int32_t get_offset_of__stream_6() { return static_cast<int32_t>(offsetof(RequestStream_t1020063535, ____stream_6)); }
	inline Stream_t1273022909 * get__stream_6() const { return ____stream_6; }
	inline Stream_t1273022909 ** get_address_of__stream_6() { return &____stream_6; }
	inline void set__stream_6(Stream_t1273022909 * value)
	{
		____stream_6 = value;
		Il2CppCodeGenWriteBarrier((&____stream_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTREAM_T1020063535_H
#ifndef HTTPBASICIDENTITY_T251442525_H
#define HTTPBASICIDENTITY_T251442525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpBasicIdentity
struct  HttpBasicIdentity_t251442525  : public GenericIdentity_t2319019448
{
public:
	// System.String WebSocketSharp.Net.HttpBasicIdentity::_password
	String_t* ____password_2;

public:
	inline static int32_t get_offset_of__password_2() { return static_cast<int32_t>(offsetof(HttpBasicIdentity_t251442525, ____password_2)); }
	inline String_t* get__password_2() const { return ____password_2; }
	inline String_t** get_address_of__password_2() { return &____password_2; }
	inline void set__password_2(String_t* value)
	{
		____password_2 = value;
		Il2CppCodeGenWriteBarrier((&____password_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPBASICIDENTITY_T251442525_H
#ifndef HTTPDIGESTIDENTITY_T1241393384_H
#define HTTPDIGESTIDENTITY_T1241393384_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpDigestIdentity
struct  HttpDigestIdentity_t1241393384  : public GenericIdentity_t2319019448
{
public:
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.HttpDigestIdentity::_parameters
	NameValueCollection_t407452768 * ____parameters_2;

public:
	inline static int32_t get_offset_of__parameters_2() { return static_cast<int32_t>(offsetof(HttpDigestIdentity_t1241393384, ____parameters_2)); }
	inline NameValueCollection_t407452768 * get__parameters_2() const { return ____parameters_2; }
	inline NameValueCollection_t407452768 ** get_address_of__parameters_2() { return &____parameters_2; }
	inline void set__parameters_2(NameValueCollection_t407452768 * value)
	{
		____parameters_2 = value;
		Il2CppCodeGenWriteBarrier((&____parameters_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPDIGESTIDENTITY_T1241393384_H
#ifndef HTTPRESPONSEHEADER_T1598204158_H
#define HTTPRESPONSEHEADER_T1598204158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpResponseHeader
struct  HttpResponseHeader_t1598204158 
{
public:
	// System.Int32 WebSocketSharp.Net.HttpResponseHeader::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpResponseHeader_t1598204158, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPRESPONSEHEADER_T1598204158_H
#ifndef SSLPROTOCOLS_T928472600_H
#define SSLPROTOCOLS_T928472600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Authentication.SslProtocols
struct  SslProtocols_t928472600 
{
public:
	// System.Int32 System.Security.Authentication.SslProtocols::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SslProtocols_t928472600, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPROTOCOLS_T928472600_H
#ifndef HTTPHEADERTYPE_T2889339440_H
#define HTTPHEADERTYPE_T2889339440_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpHeaderType
struct  HttpHeaderType_t2889339440 
{
public:
	// System.Int32 WebSocketSharp.Net.HttpHeaderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpHeaderType_t2889339440, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPHEADERTYPE_T2889339440_H
#ifndef HTTPSTATUSCODE_T4029129360_H
#define HTTPSTATUSCODE_T4029129360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpStatusCode
struct  HttpStatusCode_t4029129360 
{
public:
	// System.Int32 WebSocketSharp.Net.HttpStatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpStatusCode_t4029129360, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_T4029129360_H
#ifndef HTTPREQUESTHEADER_T3011722756_H
#define HTTPREQUESTHEADER_T3011722756_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpRequestHeader
struct  HttpRequestHeader_t3011722756 
{
public:
	// System.Int32 WebSocketSharp.Net.HttpRequestHeader::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpRequestHeader_t3011722756, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTHEADER_T3011722756_H
#ifndef INPUTCHUNKSTATE_T1224634762_H
#define INPUTCHUNKSTATE_T1224634762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.InputChunkState
struct  InputChunkState_t1224634762 
{
public:
	// System.Int32 WebSocketSharp.Net.InputChunkState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputChunkState_t1224634762, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTCHUNKSTATE_T1224634762_H
#ifndef QUERYSTRINGCOLLECTION_T3336584429_H
#define QUERYSTRINGCOLLECTION_T3336584429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.QueryStringCollection
struct  QueryStringCollection_t3336584429  : public NameValueCollection_t407452768
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUERYSTRINGCOLLECTION_T3336584429_H
#ifndef LINESTATE_T320376362_H
#define LINESTATE_T320376362_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.LineState
struct  LineState_t320376362 
{
public:
	// System.Int32 WebSocketSharp.Net.LineState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LineState_t320376362, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINESTATE_T320376362_H
#ifndef HTTPLISTENERREQUEST_T2959552699_H
#define HTTPLISTENERREQUEST_T2959552699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerRequest
struct  HttpListenerRequest_t2959552699  : public RuntimeObject
{
public:
	// System.String[] WebSocketSharp.Net.HttpListenerRequest::_acceptTypes
	StringU5BU5D_t1281789340* ____acceptTypes_1;
	// System.Boolean WebSocketSharp.Net.HttpListenerRequest::_chunked
	bool ____chunked_2;
	// System.Text.Encoding WebSocketSharp.Net.HttpListenerRequest::_contentEncoding
	Encoding_t1523322056 * ____contentEncoding_3;
	// System.Int64 WebSocketSharp.Net.HttpListenerRequest::_contentLength
	int64_t ____contentLength_4;
	// System.Boolean WebSocketSharp.Net.HttpListenerRequest::_contentLengthSet
	bool ____contentLengthSet_5;
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.HttpListenerRequest::_context
	HttpListenerContext_t3723273891 * ____context_6;
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.HttpListenerRequest::_cookies
	CookieCollection_t962330244 * ____cookies_7;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.HttpListenerRequest::_headers
	WebHeaderCollection_t1205255311 * ____headers_8;
	// System.Guid WebSocketSharp.Net.HttpListenerRequest::_identifier
	Guid_t  ____identifier_9;
	// System.IO.Stream WebSocketSharp.Net.HttpListenerRequest::_inputStream
	Stream_t1273022909 * ____inputStream_10;
	// System.Boolean WebSocketSharp.Net.HttpListenerRequest::_keepAlive
	bool ____keepAlive_11;
	// System.Boolean WebSocketSharp.Net.HttpListenerRequest::_keepAliveSet
	bool ____keepAliveSet_12;
	// System.String WebSocketSharp.Net.HttpListenerRequest::_method
	String_t* ____method_13;
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.HttpListenerRequest::_queryString
	NameValueCollection_t407452768 * ____queryString_14;
	// System.Uri WebSocketSharp.Net.HttpListenerRequest::_referer
	Uri_t100236324 * ____referer_15;
	// System.String WebSocketSharp.Net.HttpListenerRequest::_uri
	String_t* ____uri_16;
	// System.Uri WebSocketSharp.Net.HttpListenerRequest::_url
	Uri_t100236324 * ____url_17;
	// System.String[] WebSocketSharp.Net.HttpListenerRequest::_userLanguages
	StringU5BU5D_t1281789340* ____userLanguages_18;
	// System.Version WebSocketSharp.Net.HttpListenerRequest::_version
	Version_t3456873960 * ____version_19;
	// System.Boolean WebSocketSharp.Net.HttpListenerRequest::_websocketRequest
	bool ____websocketRequest_20;
	// System.Boolean WebSocketSharp.Net.HttpListenerRequest::_websocketRequestSet
	bool ____websocketRequestSet_21;

public:
	inline static int32_t get_offset_of__acceptTypes_1() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____acceptTypes_1)); }
	inline StringU5BU5D_t1281789340* get__acceptTypes_1() const { return ____acceptTypes_1; }
	inline StringU5BU5D_t1281789340** get_address_of__acceptTypes_1() { return &____acceptTypes_1; }
	inline void set__acceptTypes_1(StringU5BU5D_t1281789340* value)
	{
		____acceptTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&____acceptTypes_1), value);
	}

	inline static int32_t get_offset_of__chunked_2() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____chunked_2)); }
	inline bool get__chunked_2() const { return ____chunked_2; }
	inline bool* get_address_of__chunked_2() { return &____chunked_2; }
	inline void set__chunked_2(bool value)
	{
		____chunked_2 = value;
	}

	inline static int32_t get_offset_of__contentEncoding_3() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____contentEncoding_3)); }
	inline Encoding_t1523322056 * get__contentEncoding_3() const { return ____contentEncoding_3; }
	inline Encoding_t1523322056 ** get_address_of__contentEncoding_3() { return &____contentEncoding_3; }
	inline void set__contentEncoding_3(Encoding_t1523322056 * value)
	{
		____contentEncoding_3 = value;
		Il2CppCodeGenWriteBarrier((&____contentEncoding_3), value);
	}

	inline static int32_t get_offset_of__contentLength_4() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____contentLength_4)); }
	inline int64_t get__contentLength_4() const { return ____contentLength_4; }
	inline int64_t* get_address_of__contentLength_4() { return &____contentLength_4; }
	inline void set__contentLength_4(int64_t value)
	{
		____contentLength_4 = value;
	}

	inline static int32_t get_offset_of__contentLengthSet_5() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____contentLengthSet_5)); }
	inline bool get__contentLengthSet_5() const { return ____contentLengthSet_5; }
	inline bool* get_address_of__contentLengthSet_5() { return &____contentLengthSet_5; }
	inline void set__contentLengthSet_5(bool value)
	{
		____contentLengthSet_5 = value;
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____context_6)); }
	inline HttpListenerContext_t3723273891 * get__context_6() const { return ____context_6; }
	inline HttpListenerContext_t3723273891 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(HttpListenerContext_t3723273891 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}

	inline static int32_t get_offset_of__cookies_7() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____cookies_7)); }
	inline CookieCollection_t962330244 * get__cookies_7() const { return ____cookies_7; }
	inline CookieCollection_t962330244 ** get_address_of__cookies_7() { return &____cookies_7; }
	inline void set__cookies_7(CookieCollection_t962330244 * value)
	{
		____cookies_7 = value;
		Il2CppCodeGenWriteBarrier((&____cookies_7), value);
	}

	inline static int32_t get_offset_of__headers_8() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____headers_8)); }
	inline WebHeaderCollection_t1205255311 * get__headers_8() const { return ____headers_8; }
	inline WebHeaderCollection_t1205255311 ** get_address_of__headers_8() { return &____headers_8; }
	inline void set__headers_8(WebHeaderCollection_t1205255311 * value)
	{
		____headers_8 = value;
		Il2CppCodeGenWriteBarrier((&____headers_8), value);
	}

	inline static int32_t get_offset_of__identifier_9() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____identifier_9)); }
	inline Guid_t  get__identifier_9() const { return ____identifier_9; }
	inline Guid_t * get_address_of__identifier_9() { return &____identifier_9; }
	inline void set__identifier_9(Guid_t  value)
	{
		____identifier_9 = value;
	}

	inline static int32_t get_offset_of__inputStream_10() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____inputStream_10)); }
	inline Stream_t1273022909 * get__inputStream_10() const { return ____inputStream_10; }
	inline Stream_t1273022909 ** get_address_of__inputStream_10() { return &____inputStream_10; }
	inline void set__inputStream_10(Stream_t1273022909 * value)
	{
		____inputStream_10 = value;
		Il2CppCodeGenWriteBarrier((&____inputStream_10), value);
	}

	inline static int32_t get_offset_of__keepAlive_11() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____keepAlive_11)); }
	inline bool get__keepAlive_11() const { return ____keepAlive_11; }
	inline bool* get_address_of__keepAlive_11() { return &____keepAlive_11; }
	inline void set__keepAlive_11(bool value)
	{
		____keepAlive_11 = value;
	}

	inline static int32_t get_offset_of__keepAliveSet_12() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____keepAliveSet_12)); }
	inline bool get__keepAliveSet_12() const { return ____keepAliveSet_12; }
	inline bool* get_address_of__keepAliveSet_12() { return &____keepAliveSet_12; }
	inline void set__keepAliveSet_12(bool value)
	{
		____keepAliveSet_12 = value;
	}

	inline static int32_t get_offset_of__method_13() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____method_13)); }
	inline String_t* get__method_13() const { return ____method_13; }
	inline String_t** get_address_of__method_13() { return &____method_13; }
	inline void set__method_13(String_t* value)
	{
		____method_13 = value;
		Il2CppCodeGenWriteBarrier((&____method_13), value);
	}

	inline static int32_t get_offset_of__queryString_14() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____queryString_14)); }
	inline NameValueCollection_t407452768 * get__queryString_14() const { return ____queryString_14; }
	inline NameValueCollection_t407452768 ** get_address_of__queryString_14() { return &____queryString_14; }
	inline void set__queryString_14(NameValueCollection_t407452768 * value)
	{
		____queryString_14 = value;
		Il2CppCodeGenWriteBarrier((&____queryString_14), value);
	}

	inline static int32_t get_offset_of__referer_15() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____referer_15)); }
	inline Uri_t100236324 * get__referer_15() const { return ____referer_15; }
	inline Uri_t100236324 ** get_address_of__referer_15() { return &____referer_15; }
	inline void set__referer_15(Uri_t100236324 * value)
	{
		____referer_15 = value;
		Il2CppCodeGenWriteBarrier((&____referer_15), value);
	}

	inline static int32_t get_offset_of__uri_16() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____uri_16)); }
	inline String_t* get__uri_16() const { return ____uri_16; }
	inline String_t** get_address_of__uri_16() { return &____uri_16; }
	inline void set__uri_16(String_t* value)
	{
		____uri_16 = value;
		Il2CppCodeGenWriteBarrier((&____uri_16), value);
	}

	inline static int32_t get_offset_of__url_17() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____url_17)); }
	inline Uri_t100236324 * get__url_17() const { return ____url_17; }
	inline Uri_t100236324 ** get_address_of__url_17() { return &____url_17; }
	inline void set__url_17(Uri_t100236324 * value)
	{
		____url_17 = value;
		Il2CppCodeGenWriteBarrier((&____url_17), value);
	}

	inline static int32_t get_offset_of__userLanguages_18() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____userLanguages_18)); }
	inline StringU5BU5D_t1281789340* get__userLanguages_18() const { return ____userLanguages_18; }
	inline StringU5BU5D_t1281789340** get_address_of__userLanguages_18() { return &____userLanguages_18; }
	inline void set__userLanguages_18(StringU5BU5D_t1281789340* value)
	{
		____userLanguages_18 = value;
		Il2CppCodeGenWriteBarrier((&____userLanguages_18), value);
	}

	inline static int32_t get_offset_of__version_19() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____version_19)); }
	inline Version_t3456873960 * get__version_19() const { return ____version_19; }
	inline Version_t3456873960 ** get_address_of__version_19() { return &____version_19; }
	inline void set__version_19(Version_t3456873960 * value)
	{
		____version_19 = value;
		Il2CppCodeGenWriteBarrier((&____version_19), value);
	}

	inline static int32_t get_offset_of__websocketRequest_20() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____websocketRequest_20)); }
	inline bool get__websocketRequest_20() const { return ____websocketRequest_20; }
	inline bool* get_address_of__websocketRequest_20() { return &____websocketRequest_20; }
	inline void set__websocketRequest_20(bool value)
	{
		____websocketRequest_20 = value;
	}

	inline static int32_t get_offset_of__websocketRequestSet_21() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699, ____websocketRequestSet_21)); }
	inline bool get__websocketRequestSet_21() const { return ____websocketRequestSet_21; }
	inline bool* get_address_of__websocketRequestSet_21() { return &____websocketRequestSet_21; }
	inline void set__websocketRequestSet_21(bool value)
	{
		____websocketRequestSet_21 = value;
	}
};

struct HttpListenerRequest_t2959552699_StaticFields
{
public:
	// System.Byte[] WebSocketSharp.Net.HttpListenerRequest::_100continue
	ByteU5BU5D_t4116647657* ____100continue_0;

public:
	inline static int32_t get_offset_of__100continue_0() { return static_cast<int32_t>(offsetof(HttpListenerRequest_t2959552699_StaticFields, ____100continue_0)); }
	inline ByteU5BU5D_t4116647657* get__100continue_0() const { return ____100continue_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__100continue_0() { return &____100continue_0; }
	inline void set__100continue_0(ByteU5BU5D_t4116647657* value)
	{
		____100continue_0 = value;
		Il2CppCodeGenWriteBarrier((&____100continue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENERREQUEST_T2959552699_H
#ifndef INPUTSTATE_T4053609843_H
#define INPUTSTATE_T4053609843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.InputState
struct  InputState_t4053609843 
{
public:
	// System.Int32 WebSocketSharp.Net.InputState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InputState_t4053609843, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTSTATE_T4053609843_H
#ifndef WEBSOCKETSTATE_T45461673_H
#define WEBSOCKETSTATE_T45461673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketState
struct  WebSocketState_t45461673 
{
public:
	// System.UInt16 WebSocketSharp.WebSocketState::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WebSocketState_t45461673, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSTATE_T45461673_H
#ifndef AUTHENTICATIONSCHEMES_T3195200892_H
#define AUTHENTICATIONSCHEMES_T3195200892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.AuthenticationSchemes
struct  AuthenticationSchemes_t3195200892 
{
public:
	// System.Int32 WebSocketSharp.Net.AuthenticationSchemes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthenticationSchemes_t3195200892, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONSCHEMES_T3195200892_H
#ifndef COMPRESSIONMETHOD_T1062973517_H
#define COMPRESSIONMETHOD_T1062973517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CompressionMethod
struct  CompressionMethod_t1062973517 
{
public:
	// System.Byte WebSocketSharp.CompressionMethod::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompressionMethod_t1062973517, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMETHOD_T1062973517_H
#ifndef CLOSESTATUSCODE_T3786097442_H
#define CLOSESTATUSCODE_T3786097442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CloseStatusCode
struct  CloseStatusCode_t3786097442 
{
public:
	// System.UInt16 WebSocketSharp.CloseStatusCode::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloseStatusCode_t3786097442, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSESTATUSCODE_T3786097442_H
#ifndef FIN_T411169233_H
#define FIN_T411169233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Fin
struct  Fin_t411169233 
{
public:
	// System.Byte WebSocketSharp.Fin::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Fin_t411169233, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIN_T411169233_H
#ifndef MASK_T3471462035_H
#define MASK_T3471462035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Mask
struct  Mask_t3471462035 
{
public:
	// System.Byte WebSocketSharp.Mask::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mask_t3471462035, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASK_T3471462035_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef EXTERNALEXCEPTION_T3544951457_H
#define EXTERNALEXCEPTION_T3544951457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.InteropServices.ExternalException
struct  ExternalException_t3544951457  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNALEXCEPTION_T3544951457_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef SERVERSTATE_T358351827_H
#define SERVERSTATE_T358351827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.ServerState
struct  ServerState_t358351827 
{
public:
	// System.Int32 WebSocketSharp.Server.ServerState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ServerState_t358351827, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSTATE_T358351827_H
#ifndef FORMATEXCEPTION_T154580423_H
#define FORMATEXCEPTION_T154580423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t154580423  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T154580423_H
#ifndef ENUMERATOR_T1836727039_H
#define ENUMERATOR_T1836727039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>
struct  Enumerator_t1836727039 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t4177511560 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2280216431  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t1836727039, ___dictionary_0)); }
	inline Dictionary_2_t4177511560 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t4177511560 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t4177511560 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t1836727039, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t1836727039, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t1836727039, ___current_3)); }
	inline KeyValuePair_2_t2280216431  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2280216431 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2280216431  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1836727039_H
#ifndef OPCODE_T2755924248_H
#define OPCODE_T2755924248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Opcode
struct  Opcode_t2755924248 
{
public:
	// System.Byte WebSocketSharp.Opcode::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Opcode_t2755924248, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODE_T2755924248_H
#ifndef RSV_T2704667083_H
#define RSV_T2704667083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Rsv
struct  Rsv_t2704667083 
{
public:
	// System.Byte WebSocketSharp.Rsv::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Rsv_t2704667083, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSV_T2704667083_H
#ifndef HTTPCONNECTION_T4177287240_H
#define HTTPCONNECTION_T4177287240_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpConnection
struct  HttpConnection_t4177287240  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.Net.HttpConnection::_buffer
	ByteU5BU5D_t4116647657* ____buffer_0;
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.HttpConnection::_context
	HttpListenerContext_t3723273891 * ____context_2;
	// System.Boolean WebSocketSharp.Net.HttpConnection::_contextRegistered
	bool ____contextRegistered_3;
	// System.Text.StringBuilder WebSocketSharp.Net.HttpConnection::_currentLine
	StringBuilder_t * ____currentLine_4;
	// WebSocketSharp.Net.InputState WebSocketSharp.Net.HttpConnection::_inputState
	int32_t ____inputState_5;
	// WebSocketSharp.Net.RequestStream WebSocketSharp.Net.HttpConnection::_inputStream
	RequestStream_t1020063535 * ____inputStream_6;
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Net.HttpConnection::_lastListener
	HttpListener_t2467819517 * ____lastListener_7;
	// WebSocketSharp.Net.LineState WebSocketSharp.Net.HttpConnection::_lineState
	int32_t ____lineState_8;
	// WebSocketSharp.Net.EndPointListener WebSocketSharp.Net.HttpConnection::_listener
	EndPointListener_t884507598 * ____listener_9;
	// WebSocketSharp.Net.ResponseStream WebSocketSharp.Net.HttpConnection::_outputStream
	ResponseStream_t2828822308 * ____outputStream_10;
	// System.Int32 WebSocketSharp.Net.HttpConnection::_position
	int32_t ____position_11;
	// System.IO.MemoryStream WebSocketSharp.Net.HttpConnection::_requestBuffer
	MemoryStream_t94973147 * ____requestBuffer_12;
	// System.Int32 WebSocketSharp.Net.HttpConnection::_reuses
	int32_t ____reuses_13;
	// System.Boolean WebSocketSharp.Net.HttpConnection::_secure
	bool ____secure_14;
	// System.Net.Sockets.Socket WebSocketSharp.Net.HttpConnection::_socket
	Socket_t1119025450 * ____socket_15;
	// System.IO.Stream WebSocketSharp.Net.HttpConnection::_stream
	Stream_t1273022909 * ____stream_16;
	// System.Object WebSocketSharp.Net.HttpConnection::_sync
	RuntimeObject * ____sync_17;
	// System.Int32 WebSocketSharp.Net.HttpConnection::_timeout
	int32_t ____timeout_18;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Boolean> WebSocketSharp.Net.HttpConnection::_timeoutCanceled
	Dictionary_2_t3280968592 * ____timeoutCanceled_19;
	// System.Threading.Timer WebSocketSharp.Net.HttpConnection::_timer
	Timer_t716671026 * ____timer_20;

public:
	inline static int32_t get_offset_of__buffer_0() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____buffer_0)); }
	inline ByteU5BU5D_t4116647657* get__buffer_0() const { return ____buffer_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__buffer_0() { return &____buffer_0; }
	inline void set__buffer_0(ByteU5BU5D_t4116647657* value)
	{
		____buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_0), value);
	}

	inline static int32_t get_offset_of__context_2() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____context_2)); }
	inline HttpListenerContext_t3723273891 * get__context_2() const { return ____context_2; }
	inline HttpListenerContext_t3723273891 ** get_address_of__context_2() { return &____context_2; }
	inline void set__context_2(HttpListenerContext_t3723273891 * value)
	{
		____context_2 = value;
		Il2CppCodeGenWriteBarrier((&____context_2), value);
	}

	inline static int32_t get_offset_of__contextRegistered_3() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____contextRegistered_3)); }
	inline bool get__contextRegistered_3() const { return ____contextRegistered_3; }
	inline bool* get_address_of__contextRegistered_3() { return &____contextRegistered_3; }
	inline void set__contextRegistered_3(bool value)
	{
		____contextRegistered_3 = value;
	}

	inline static int32_t get_offset_of__currentLine_4() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____currentLine_4)); }
	inline StringBuilder_t * get__currentLine_4() const { return ____currentLine_4; }
	inline StringBuilder_t ** get_address_of__currentLine_4() { return &____currentLine_4; }
	inline void set__currentLine_4(StringBuilder_t * value)
	{
		____currentLine_4 = value;
		Il2CppCodeGenWriteBarrier((&____currentLine_4), value);
	}

	inline static int32_t get_offset_of__inputState_5() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____inputState_5)); }
	inline int32_t get__inputState_5() const { return ____inputState_5; }
	inline int32_t* get_address_of__inputState_5() { return &____inputState_5; }
	inline void set__inputState_5(int32_t value)
	{
		____inputState_5 = value;
	}

	inline static int32_t get_offset_of__inputStream_6() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____inputStream_6)); }
	inline RequestStream_t1020063535 * get__inputStream_6() const { return ____inputStream_6; }
	inline RequestStream_t1020063535 ** get_address_of__inputStream_6() { return &____inputStream_6; }
	inline void set__inputStream_6(RequestStream_t1020063535 * value)
	{
		____inputStream_6 = value;
		Il2CppCodeGenWriteBarrier((&____inputStream_6), value);
	}

	inline static int32_t get_offset_of__lastListener_7() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____lastListener_7)); }
	inline HttpListener_t2467819517 * get__lastListener_7() const { return ____lastListener_7; }
	inline HttpListener_t2467819517 ** get_address_of__lastListener_7() { return &____lastListener_7; }
	inline void set__lastListener_7(HttpListener_t2467819517 * value)
	{
		____lastListener_7 = value;
		Il2CppCodeGenWriteBarrier((&____lastListener_7), value);
	}

	inline static int32_t get_offset_of__lineState_8() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____lineState_8)); }
	inline int32_t get__lineState_8() const { return ____lineState_8; }
	inline int32_t* get_address_of__lineState_8() { return &____lineState_8; }
	inline void set__lineState_8(int32_t value)
	{
		____lineState_8 = value;
	}

	inline static int32_t get_offset_of__listener_9() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____listener_9)); }
	inline EndPointListener_t884507598 * get__listener_9() const { return ____listener_9; }
	inline EndPointListener_t884507598 ** get_address_of__listener_9() { return &____listener_9; }
	inline void set__listener_9(EndPointListener_t884507598 * value)
	{
		____listener_9 = value;
		Il2CppCodeGenWriteBarrier((&____listener_9), value);
	}

	inline static int32_t get_offset_of__outputStream_10() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____outputStream_10)); }
	inline ResponseStream_t2828822308 * get__outputStream_10() const { return ____outputStream_10; }
	inline ResponseStream_t2828822308 ** get_address_of__outputStream_10() { return &____outputStream_10; }
	inline void set__outputStream_10(ResponseStream_t2828822308 * value)
	{
		____outputStream_10 = value;
		Il2CppCodeGenWriteBarrier((&____outputStream_10), value);
	}

	inline static int32_t get_offset_of__position_11() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____position_11)); }
	inline int32_t get__position_11() const { return ____position_11; }
	inline int32_t* get_address_of__position_11() { return &____position_11; }
	inline void set__position_11(int32_t value)
	{
		____position_11 = value;
	}

	inline static int32_t get_offset_of__requestBuffer_12() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____requestBuffer_12)); }
	inline MemoryStream_t94973147 * get__requestBuffer_12() const { return ____requestBuffer_12; }
	inline MemoryStream_t94973147 ** get_address_of__requestBuffer_12() { return &____requestBuffer_12; }
	inline void set__requestBuffer_12(MemoryStream_t94973147 * value)
	{
		____requestBuffer_12 = value;
		Il2CppCodeGenWriteBarrier((&____requestBuffer_12), value);
	}

	inline static int32_t get_offset_of__reuses_13() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____reuses_13)); }
	inline int32_t get__reuses_13() const { return ____reuses_13; }
	inline int32_t* get_address_of__reuses_13() { return &____reuses_13; }
	inline void set__reuses_13(int32_t value)
	{
		____reuses_13 = value;
	}

	inline static int32_t get_offset_of__secure_14() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____secure_14)); }
	inline bool get__secure_14() const { return ____secure_14; }
	inline bool* get_address_of__secure_14() { return &____secure_14; }
	inline void set__secure_14(bool value)
	{
		____secure_14 = value;
	}

	inline static int32_t get_offset_of__socket_15() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____socket_15)); }
	inline Socket_t1119025450 * get__socket_15() const { return ____socket_15; }
	inline Socket_t1119025450 ** get_address_of__socket_15() { return &____socket_15; }
	inline void set__socket_15(Socket_t1119025450 * value)
	{
		____socket_15 = value;
		Il2CppCodeGenWriteBarrier((&____socket_15), value);
	}

	inline static int32_t get_offset_of__stream_16() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____stream_16)); }
	inline Stream_t1273022909 * get__stream_16() const { return ____stream_16; }
	inline Stream_t1273022909 ** get_address_of__stream_16() { return &____stream_16; }
	inline void set__stream_16(Stream_t1273022909 * value)
	{
		____stream_16 = value;
		Il2CppCodeGenWriteBarrier((&____stream_16), value);
	}

	inline static int32_t get_offset_of__sync_17() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____sync_17)); }
	inline RuntimeObject * get__sync_17() const { return ____sync_17; }
	inline RuntimeObject ** get_address_of__sync_17() { return &____sync_17; }
	inline void set__sync_17(RuntimeObject * value)
	{
		____sync_17 = value;
		Il2CppCodeGenWriteBarrier((&____sync_17), value);
	}

	inline static int32_t get_offset_of__timeout_18() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____timeout_18)); }
	inline int32_t get__timeout_18() const { return ____timeout_18; }
	inline int32_t* get_address_of__timeout_18() { return &____timeout_18; }
	inline void set__timeout_18(int32_t value)
	{
		____timeout_18 = value;
	}

	inline static int32_t get_offset_of__timeoutCanceled_19() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____timeoutCanceled_19)); }
	inline Dictionary_2_t3280968592 * get__timeoutCanceled_19() const { return ____timeoutCanceled_19; }
	inline Dictionary_2_t3280968592 ** get_address_of__timeoutCanceled_19() { return &____timeoutCanceled_19; }
	inline void set__timeoutCanceled_19(Dictionary_2_t3280968592 * value)
	{
		____timeoutCanceled_19 = value;
		Il2CppCodeGenWriteBarrier((&____timeoutCanceled_19), value);
	}

	inline static int32_t get_offset_of__timer_20() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240, ____timer_20)); }
	inline Timer_t716671026 * get__timer_20() const { return ____timer_20; }
	inline Timer_t716671026 ** get_address_of__timer_20() { return &____timer_20; }
	inline void set__timer_20(Timer_t716671026 * value)
	{
		____timer_20 = value;
		Il2CppCodeGenWriteBarrier((&____timer_20), value);
	}
};

struct HttpConnection_t4177287240_StaticFields
{
public:
	// System.Threading.TimerCallback WebSocketSharp.Net.HttpConnection::<>f__mg$cache0
	TimerCallback_t1438585625 * ___U3CU3Ef__mgU24cache0_21;
	// System.AsyncCallback WebSocketSharp.Net.HttpConnection::<>f__mg$cache1
	AsyncCallback_t3962456242 * ___U3CU3Ef__mgU24cache1_22;
	// System.AsyncCallback WebSocketSharp.Net.HttpConnection::<>f__mg$cache2
	AsyncCallback_t3962456242 * ___U3CU3Ef__mgU24cache2_23;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_21() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240_StaticFields, ___U3CU3Ef__mgU24cache0_21)); }
	inline TimerCallback_t1438585625 * get_U3CU3Ef__mgU24cache0_21() const { return ___U3CU3Ef__mgU24cache0_21; }
	inline TimerCallback_t1438585625 ** get_address_of_U3CU3Ef__mgU24cache0_21() { return &___U3CU3Ef__mgU24cache0_21; }
	inline void set_U3CU3Ef__mgU24cache0_21(TimerCallback_t1438585625 * value)
	{
		___U3CU3Ef__mgU24cache0_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_22() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240_StaticFields, ___U3CU3Ef__mgU24cache1_22)); }
	inline AsyncCallback_t3962456242 * get_U3CU3Ef__mgU24cache1_22() const { return ___U3CU3Ef__mgU24cache1_22; }
	inline AsyncCallback_t3962456242 ** get_address_of_U3CU3Ef__mgU24cache1_22() { return &___U3CU3Ef__mgU24cache1_22; }
	inline void set_U3CU3Ef__mgU24cache1_22(AsyncCallback_t3962456242 * value)
	{
		___U3CU3Ef__mgU24cache1_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_23() { return static_cast<int32_t>(offsetof(HttpConnection_t4177287240_StaticFields, ___U3CU3Ef__mgU24cache2_23)); }
	inline AsyncCallback_t3962456242 * get_U3CU3Ef__mgU24cache2_23() const { return ___U3CU3Ef__mgU24cache2_23; }
	inline AsyncCallback_t3962456242 ** get_address_of_U3CU3Ef__mgU24cache2_23() { return &___U3CU3Ef__mgU24cache2_23; }
	inline void set_U3CU3Ef__mgU24cache2_23(AsyncCallback_t3962456242 * value)
	{
		___U3CU3Ef__mgU24cache2_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONNECTION_T4177287240_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef HTTPHEADERINFO_T2062258289_H
#define HTTPHEADERINFO_T2062258289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpHeaderInfo
struct  HttpHeaderInfo_t2062258289  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.HttpHeaderInfo::_name
	String_t* ____name_0;
	// WebSocketSharp.Net.HttpHeaderType WebSocketSharp.Net.HttpHeaderInfo::_type
	int32_t ____type_1;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(HttpHeaderInfo_t2062258289, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier((&____name_0), value);
	}

	inline static int32_t get_offset_of__type_1() { return static_cast<int32_t>(offsetof(HttpHeaderInfo_t2062258289, ____type_1)); }
	inline int32_t get__type_1() const { return ____type_1; }
	inline int32_t* get_address_of__type_1() { return &____type_1; }
	inline void set__type_1(int32_t value)
	{
		____type_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPHEADERINFO_T2062258289_H
#ifndef WIN32EXCEPTION_T3234146298_H
#define WIN32EXCEPTION_T3234146298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.Win32Exception
struct  Win32Exception_t3234146298  : public ExternalException_t3544951457
{
public:
	// System.Int32 System.ComponentModel.Win32Exception::native_error_code
	int32_t ___native_error_code_11;

public:
	inline static int32_t get_offset_of_native_error_code_11() { return static_cast<int32_t>(offsetof(Win32Exception_t3234146298, ___native_error_code_11)); }
	inline int32_t get_native_error_code_11() const { return ___native_error_code_11; }
	inline int32_t* get_address_of_native_error_code_11() { return &___native_error_code_11; }
	inline void set_native_error_code_11(int32_t value)
	{
		___native_error_code_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIN32EXCEPTION_T3234146298_H
#ifndef COOKIEEXCEPTION_T1899604716_H
#define COOKIEEXCEPTION_T1899604716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.CookieException
struct  CookieException_t1899604716  : public FormatException_t154580423
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIEEXCEPTION_T1899604716_H
#ifndef HTTPLISTENER_T2467819517_H
#define HTTPLISTENER_T2467819517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListener
struct  HttpListener_t2467819517  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Net.HttpListener::_authSchemes
	int32_t ____authSchemes_0;
	// System.Func`2<WebSocketSharp.Net.HttpListenerRequest,WebSocketSharp.Net.AuthenticationSchemes> WebSocketSharp.Net.HttpListener::_authSchemeSelector
	Func_2_t787507147 * ____authSchemeSelector_1;
	// System.String WebSocketSharp.Net.HttpListener::_certFolderPath
	String_t* ____certFolderPath_2;
	// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpConnection,WebSocketSharp.Net.HttpConnection> WebSocketSharp.Net.HttpListener::_connections
	Dictionary_2_t1708533168 * ____connections_3;
	// System.Object WebSocketSharp.Net.HttpListener::_connectionsSync
	RuntimeObject * ____connectionsSync_4;
	// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerContext> WebSocketSharp.Net.HttpListener::_ctxQueue
	List_1_t900381337 * ____ctxQueue_5;
	// System.Object WebSocketSharp.Net.HttpListener::_ctxQueueSync
	RuntimeObject * ____ctxQueueSync_6;
	// System.Collections.Generic.Dictionary`2<WebSocketSharp.Net.HttpListenerContext,WebSocketSharp.Net.HttpListenerContext> WebSocketSharp.Net.HttpListener::_ctxRegistry
	Dictionary_2_t2932675044 * ____ctxRegistry_7;
	// System.Object WebSocketSharp.Net.HttpListener::_ctxRegistrySync
	RuntimeObject * ____ctxRegistrySync_8;
	// System.Boolean WebSocketSharp.Net.HttpListener::_disposed
	bool ____disposed_10;
	// System.Boolean WebSocketSharp.Net.HttpListener::_ignoreWriteExceptions
	bool ____ignoreWriteExceptions_11;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Net.HttpListener::_listening
	bool ____listening_12;
	// WebSocketSharp.Logger WebSocketSharp.Net.HttpListener::_logger
	Logger_t4025333586 * ____logger_13;
	// WebSocketSharp.Net.HttpListenerPrefixCollection WebSocketSharp.Net.HttpListener::_prefixes
	HttpListenerPrefixCollection_t702486717 * ____prefixes_14;
	// System.String WebSocketSharp.Net.HttpListener::_realm
	String_t* ____realm_15;
	// System.Boolean WebSocketSharp.Net.HttpListener::_reuseAddress
	bool ____reuseAddress_16;
	// WebSocketSharp.Net.ServerSslConfiguration WebSocketSharp.Net.HttpListener::_sslConfig
	ServerSslConfiguration_t3240511754 * ____sslConfig_17;
	// System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential> WebSocketSharp.Net.HttpListener::_userCredFinder
	Func_2_t2697316325 * ____userCredFinder_18;
	// System.Collections.Generic.List`1<WebSocketSharp.Net.HttpListenerAsyncResult> WebSocketSharp.Net.HttpListener::_waitQueue
	List_1_t3070489486 * ____waitQueue_19;
	// System.Object WebSocketSharp.Net.HttpListener::_waitQueueSync
	RuntimeObject * ____waitQueueSync_20;

public:
	inline static int32_t get_offset_of__authSchemes_0() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____authSchemes_0)); }
	inline int32_t get__authSchemes_0() const { return ____authSchemes_0; }
	inline int32_t* get_address_of__authSchemes_0() { return &____authSchemes_0; }
	inline void set__authSchemes_0(int32_t value)
	{
		____authSchemes_0 = value;
	}

	inline static int32_t get_offset_of__authSchemeSelector_1() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____authSchemeSelector_1)); }
	inline Func_2_t787507147 * get__authSchemeSelector_1() const { return ____authSchemeSelector_1; }
	inline Func_2_t787507147 ** get_address_of__authSchemeSelector_1() { return &____authSchemeSelector_1; }
	inline void set__authSchemeSelector_1(Func_2_t787507147 * value)
	{
		____authSchemeSelector_1 = value;
		Il2CppCodeGenWriteBarrier((&____authSchemeSelector_1), value);
	}

	inline static int32_t get_offset_of__certFolderPath_2() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____certFolderPath_2)); }
	inline String_t* get__certFolderPath_2() const { return ____certFolderPath_2; }
	inline String_t** get_address_of__certFolderPath_2() { return &____certFolderPath_2; }
	inline void set__certFolderPath_2(String_t* value)
	{
		____certFolderPath_2 = value;
		Il2CppCodeGenWriteBarrier((&____certFolderPath_2), value);
	}

	inline static int32_t get_offset_of__connections_3() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____connections_3)); }
	inline Dictionary_2_t1708533168 * get__connections_3() const { return ____connections_3; }
	inline Dictionary_2_t1708533168 ** get_address_of__connections_3() { return &____connections_3; }
	inline void set__connections_3(Dictionary_2_t1708533168 * value)
	{
		____connections_3 = value;
		Il2CppCodeGenWriteBarrier((&____connections_3), value);
	}

	inline static int32_t get_offset_of__connectionsSync_4() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____connectionsSync_4)); }
	inline RuntimeObject * get__connectionsSync_4() const { return ____connectionsSync_4; }
	inline RuntimeObject ** get_address_of__connectionsSync_4() { return &____connectionsSync_4; }
	inline void set__connectionsSync_4(RuntimeObject * value)
	{
		____connectionsSync_4 = value;
		Il2CppCodeGenWriteBarrier((&____connectionsSync_4), value);
	}

	inline static int32_t get_offset_of__ctxQueue_5() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____ctxQueue_5)); }
	inline List_1_t900381337 * get__ctxQueue_5() const { return ____ctxQueue_5; }
	inline List_1_t900381337 ** get_address_of__ctxQueue_5() { return &____ctxQueue_5; }
	inline void set__ctxQueue_5(List_1_t900381337 * value)
	{
		____ctxQueue_5 = value;
		Il2CppCodeGenWriteBarrier((&____ctxQueue_5), value);
	}

	inline static int32_t get_offset_of__ctxQueueSync_6() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____ctxQueueSync_6)); }
	inline RuntimeObject * get__ctxQueueSync_6() const { return ____ctxQueueSync_6; }
	inline RuntimeObject ** get_address_of__ctxQueueSync_6() { return &____ctxQueueSync_6; }
	inline void set__ctxQueueSync_6(RuntimeObject * value)
	{
		____ctxQueueSync_6 = value;
		Il2CppCodeGenWriteBarrier((&____ctxQueueSync_6), value);
	}

	inline static int32_t get_offset_of__ctxRegistry_7() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____ctxRegistry_7)); }
	inline Dictionary_2_t2932675044 * get__ctxRegistry_7() const { return ____ctxRegistry_7; }
	inline Dictionary_2_t2932675044 ** get_address_of__ctxRegistry_7() { return &____ctxRegistry_7; }
	inline void set__ctxRegistry_7(Dictionary_2_t2932675044 * value)
	{
		____ctxRegistry_7 = value;
		Il2CppCodeGenWriteBarrier((&____ctxRegistry_7), value);
	}

	inline static int32_t get_offset_of__ctxRegistrySync_8() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____ctxRegistrySync_8)); }
	inline RuntimeObject * get__ctxRegistrySync_8() const { return ____ctxRegistrySync_8; }
	inline RuntimeObject ** get_address_of__ctxRegistrySync_8() { return &____ctxRegistrySync_8; }
	inline void set__ctxRegistrySync_8(RuntimeObject * value)
	{
		____ctxRegistrySync_8 = value;
		Il2CppCodeGenWriteBarrier((&____ctxRegistrySync_8), value);
	}

	inline static int32_t get_offset_of__disposed_10() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____disposed_10)); }
	inline bool get__disposed_10() const { return ____disposed_10; }
	inline bool* get_address_of__disposed_10() { return &____disposed_10; }
	inline void set__disposed_10(bool value)
	{
		____disposed_10 = value;
	}

	inline static int32_t get_offset_of__ignoreWriteExceptions_11() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____ignoreWriteExceptions_11)); }
	inline bool get__ignoreWriteExceptions_11() const { return ____ignoreWriteExceptions_11; }
	inline bool* get_address_of__ignoreWriteExceptions_11() { return &____ignoreWriteExceptions_11; }
	inline void set__ignoreWriteExceptions_11(bool value)
	{
		____ignoreWriteExceptions_11 = value;
	}

	inline static int32_t get_offset_of__listening_12() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____listening_12)); }
	inline bool get__listening_12() const { return ____listening_12; }
	inline bool* get_address_of__listening_12() { return &____listening_12; }
	inline void set__listening_12(bool value)
	{
		____listening_12 = value;
	}

	inline static int32_t get_offset_of__logger_13() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____logger_13)); }
	inline Logger_t4025333586 * get__logger_13() const { return ____logger_13; }
	inline Logger_t4025333586 ** get_address_of__logger_13() { return &____logger_13; }
	inline void set__logger_13(Logger_t4025333586 * value)
	{
		____logger_13 = value;
		Il2CppCodeGenWriteBarrier((&____logger_13), value);
	}

	inline static int32_t get_offset_of__prefixes_14() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____prefixes_14)); }
	inline HttpListenerPrefixCollection_t702486717 * get__prefixes_14() const { return ____prefixes_14; }
	inline HttpListenerPrefixCollection_t702486717 ** get_address_of__prefixes_14() { return &____prefixes_14; }
	inline void set__prefixes_14(HttpListenerPrefixCollection_t702486717 * value)
	{
		____prefixes_14 = value;
		Il2CppCodeGenWriteBarrier((&____prefixes_14), value);
	}

	inline static int32_t get_offset_of__realm_15() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____realm_15)); }
	inline String_t* get__realm_15() const { return ____realm_15; }
	inline String_t** get_address_of__realm_15() { return &____realm_15; }
	inline void set__realm_15(String_t* value)
	{
		____realm_15 = value;
		Il2CppCodeGenWriteBarrier((&____realm_15), value);
	}

	inline static int32_t get_offset_of__reuseAddress_16() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____reuseAddress_16)); }
	inline bool get__reuseAddress_16() const { return ____reuseAddress_16; }
	inline bool* get_address_of__reuseAddress_16() { return &____reuseAddress_16; }
	inline void set__reuseAddress_16(bool value)
	{
		____reuseAddress_16 = value;
	}

	inline static int32_t get_offset_of__sslConfig_17() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____sslConfig_17)); }
	inline ServerSslConfiguration_t3240511754 * get__sslConfig_17() const { return ____sslConfig_17; }
	inline ServerSslConfiguration_t3240511754 ** get_address_of__sslConfig_17() { return &____sslConfig_17; }
	inline void set__sslConfig_17(ServerSslConfiguration_t3240511754 * value)
	{
		____sslConfig_17 = value;
		Il2CppCodeGenWriteBarrier((&____sslConfig_17), value);
	}

	inline static int32_t get_offset_of__userCredFinder_18() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____userCredFinder_18)); }
	inline Func_2_t2697316325 * get__userCredFinder_18() const { return ____userCredFinder_18; }
	inline Func_2_t2697316325 ** get_address_of__userCredFinder_18() { return &____userCredFinder_18; }
	inline void set__userCredFinder_18(Func_2_t2697316325 * value)
	{
		____userCredFinder_18 = value;
		Il2CppCodeGenWriteBarrier((&____userCredFinder_18), value);
	}

	inline static int32_t get_offset_of__waitQueue_19() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____waitQueue_19)); }
	inline List_1_t3070489486 * get__waitQueue_19() const { return ____waitQueue_19; }
	inline List_1_t3070489486 ** get_address_of__waitQueue_19() { return &____waitQueue_19; }
	inline void set__waitQueue_19(List_1_t3070489486 * value)
	{
		____waitQueue_19 = value;
		Il2CppCodeGenWriteBarrier((&____waitQueue_19), value);
	}

	inline static int32_t get_offset_of__waitQueueSync_20() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517, ____waitQueueSync_20)); }
	inline RuntimeObject * get__waitQueueSync_20() const { return ____waitQueueSync_20; }
	inline RuntimeObject ** get_address_of__waitQueueSync_20() { return &____waitQueueSync_20; }
	inline void set__waitQueueSync_20(RuntimeObject * value)
	{
		____waitQueueSync_20 = value;
		Il2CppCodeGenWriteBarrier((&____waitQueueSync_20), value);
	}
};

struct HttpListener_t2467819517_StaticFields
{
public:
	// System.String WebSocketSharp.Net.HttpListener::_defaultRealm
	String_t* ____defaultRealm_9;

public:
	inline static int32_t get_offset_of__defaultRealm_9() { return static_cast<int32_t>(offsetof(HttpListener_t2467819517_StaticFields, ____defaultRealm_9)); }
	inline String_t* get__defaultRealm_9() const { return ____defaultRealm_9; }
	inline String_t** get_address_of__defaultRealm_9() { return &____defaultRealm_9; }
	inline void set__defaultRealm_9(String_t* value)
	{
		____defaultRealm_9 = value;
		Il2CppCodeGenWriteBarrier((&____defaultRealm_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENER_T2467819517_H
#ifndef U3CBROADCASTASYNCU3EC__ANONSTOREY1_T818115800_H
#define U3CBROADCASTASYNCU3EC__ANONSTOREY1_T818115800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey1
struct  U3CbroadcastAsyncU3Ec__AnonStorey1_t818115800  : public RuntimeObject
{
public:
	// WebSocketSharp.Opcode WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey1::opcode
	uint8_t ___opcode_0;
	// System.IO.Stream WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey1::stream
	Stream_t1273022909 * ___stream_1;
	// System.Action WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey1::completed
	Action_t1264377477 * ___completed_2;
	// WebSocketSharp.Server.WebSocketServiceManager WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey1::$this
	WebSocketServiceManager_t2425053 * ___U24this_3;

public:
	inline static int32_t get_offset_of_opcode_0() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey1_t818115800, ___opcode_0)); }
	inline uint8_t get_opcode_0() const { return ___opcode_0; }
	inline uint8_t* get_address_of_opcode_0() { return &___opcode_0; }
	inline void set_opcode_0(uint8_t value)
	{
		___opcode_0 = value;
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey1_t818115800, ___stream_1)); }
	inline Stream_t1273022909 * get_stream_1() const { return ___stream_1; }
	inline Stream_t1273022909 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t1273022909 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey1_t818115800, ___completed_2)); }
	inline Action_t1264377477 * get_completed_2() const { return ___completed_2; }
	inline Action_t1264377477 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_t1264377477 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey1_t818115800, ___U24this_3)); }
	inline WebSocketServiceManager_t2425053 * get_U24this_3() const { return ___U24this_3; }
	inline WebSocketServiceManager_t2425053 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(WebSocketServiceManager_t2425053 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBROADCASTASYNCU3EC__ANONSTOREY1_T818115800_H
#ifndef WEBSOCKETEXCEPTION_T618477455_H
#define WEBSOCKETEXCEPTION_T618477455_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketException
struct  WebSocketException_t618477455  : public Exception_t
{
public:
	// WebSocketSharp.CloseStatusCode WebSocketSharp.WebSocketException::_code
	uint16_t ____code_11;

public:
	inline static int32_t get_offset_of__code_11() { return static_cast<int32_t>(offsetof(WebSocketException_t618477455, ____code_11)); }
	inline uint16_t get__code_11() const { return ____code_11; }
	inline uint16_t* get_address_of__code_11() { return &____code_11; }
	inline void set__code_11(uint16_t value)
	{
		____code_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETEXCEPTION_T618477455_H
#ifndef U3CAUTHENTICATEU3EC__ANONSTOREY1_T3142200144_H
#define U3CAUTHENTICATEU3EC__ANONSTOREY1_T3142200144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1
struct  U3CAuthenticateU3Ec__AnonStorey1_t3142200144  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::retry
	int32_t ___retry_0;
	// WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::scheme
	int32_t ___scheme_1;
	// System.String WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::realm
	String_t* ___realm_2;
	// System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential> WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::credentialsFinder
	Func_2_t2697316325 * ___credentialsFinder_3;
	// System.String WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::chal
	String_t* ___chal_4;
	// System.Func`1<System.Boolean> WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::auth
	Func_1_t3822001908 * ___auth_5;
	// WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext WebSocketSharp.Net.WebSockets.TcpListenerWebSocketContext/<Authenticate>c__AnonStorey1::$this
	TcpListenerWebSocketContext_t2376318492 * ___U24this_6;

public:
	inline static int32_t get_offset_of_retry_0() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t3142200144, ___retry_0)); }
	inline int32_t get_retry_0() const { return ___retry_0; }
	inline int32_t* get_address_of_retry_0() { return &___retry_0; }
	inline void set_retry_0(int32_t value)
	{
		___retry_0 = value;
	}

	inline static int32_t get_offset_of_scheme_1() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t3142200144, ___scheme_1)); }
	inline int32_t get_scheme_1() const { return ___scheme_1; }
	inline int32_t* get_address_of_scheme_1() { return &___scheme_1; }
	inline void set_scheme_1(int32_t value)
	{
		___scheme_1 = value;
	}

	inline static int32_t get_offset_of_realm_2() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t3142200144, ___realm_2)); }
	inline String_t* get_realm_2() const { return ___realm_2; }
	inline String_t** get_address_of_realm_2() { return &___realm_2; }
	inline void set_realm_2(String_t* value)
	{
		___realm_2 = value;
		Il2CppCodeGenWriteBarrier((&___realm_2), value);
	}

	inline static int32_t get_offset_of_credentialsFinder_3() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t3142200144, ___credentialsFinder_3)); }
	inline Func_2_t2697316325 * get_credentialsFinder_3() const { return ___credentialsFinder_3; }
	inline Func_2_t2697316325 ** get_address_of_credentialsFinder_3() { return &___credentialsFinder_3; }
	inline void set_credentialsFinder_3(Func_2_t2697316325 * value)
	{
		___credentialsFinder_3 = value;
		Il2CppCodeGenWriteBarrier((&___credentialsFinder_3), value);
	}

	inline static int32_t get_offset_of_chal_4() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t3142200144, ___chal_4)); }
	inline String_t* get_chal_4() const { return ___chal_4; }
	inline String_t** get_address_of_chal_4() { return &___chal_4; }
	inline void set_chal_4(String_t* value)
	{
		___chal_4 = value;
		Il2CppCodeGenWriteBarrier((&___chal_4), value);
	}

	inline static int32_t get_offset_of_auth_5() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t3142200144, ___auth_5)); }
	inline Func_1_t3822001908 * get_auth_5() const { return ___auth_5; }
	inline Func_1_t3822001908 ** get_address_of_auth_5() { return &___auth_5; }
	inline void set_auth_5(Func_1_t3822001908 * value)
	{
		___auth_5 = value;
		Il2CppCodeGenWriteBarrier((&___auth_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CAuthenticateU3Ec__AnonStorey1_t3142200144, ___U24this_6)); }
	inline TcpListenerWebSocketContext_t2376318492 * get_U24this_6() const { return ___U24this_6; }
	inline TcpListenerWebSocketContext_t2376318492 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(TcpListenerWebSocketContext_t2376318492 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CAUTHENTICATEU3EC__ANONSTOREY1_T3142200144_H
#ifndef HTTPSERVER_T3049239368_H
#define HTTPSERVER_T3049239368_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.HttpServer
struct  HttpServer_t3049239368  : public RuntimeObject
{
public:
	// System.Net.IPAddress WebSocketSharp.Server.HttpServer::_address
	IPAddress_t241777590 * ____address_0;
	// System.String WebSocketSharp.Server.HttpServer::_hostname
	String_t* ____hostname_1;
	// WebSocketSharp.Net.HttpListener WebSocketSharp.Server.HttpServer::_listener
	HttpListener_t2467819517 * ____listener_2;
	// WebSocketSharp.Logger WebSocketSharp.Server.HttpServer::_logger
	Logger_t4025333586 * ____logger_3;
	// System.Int32 WebSocketSharp.Server.HttpServer::_port
	int32_t ____port_4;
	// System.Threading.Thread WebSocketSharp.Server.HttpServer::_receiveThread
	Thread_t2300836069 * ____receiveThread_5;
	// System.String WebSocketSharp.Server.HttpServer::_rootPath
	String_t* ____rootPath_6;
	// System.Boolean WebSocketSharp.Server.HttpServer::_secure
	bool ____secure_7;
	// WebSocketSharp.Server.WebSocketServiceManager WebSocketSharp.Server.HttpServer::_services
	WebSocketServiceManager_t2425053 * ____services_8;
	// WebSocketSharp.Server.ServerState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.HttpServer::_state
	int32_t ____state_9;
	// System.Object WebSocketSharp.Server.HttpServer::_sync
	RuntimeObject * ____sync_10;
	// System.Boolean WebSocketSharp.Server.HttpServer::_windows
	bool ____windows_11;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnConnect
	EventHandler_1_t3222597438 * ___OnConnect_12;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnDelete
	EventHandler_1_t3222597438 * ___OnDelete_13;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnGet
	EventHandler_1_t3222597438 * ___OnGet_14;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnHead
	EventHandler_1_t3222597438 * ___OnHead_15;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnOptions
	EventHandler_1_t3222597438 * ___OnOptions_16;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnPatch
	EventHandler_1_t3222597438 * ___OnPatch_17;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnPost
	EventHandler_1_t3222597438 * ___OnPost_18;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnPut
	EventHandler_1_t3222597438 * ___OnPut_19;
	// System.EventHandler`1<WebSocketSharp.Server.HttpRequestEventArgs> WebSocketSharp.Server.HttpServer::OnTrace
	EventHandler_1_t3222597438 * ___OnTrace_20;

public:
	inline static int32_t get_offset_of__address_0() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ____address_0)); }
	inline IPAddress_t241777590 * get__address_0() const { return ____address_0; }
	inline IPAddress_t241777590 ** get_address_of__address_0() { return &____address_0; }
	inline void set__address_0(IPAddress_t241777590 * value)
	{
		____address_0 = value;
		Il2CppCodeGenWriteBarrier((&____address_0), value);
	}

	inline static int32_t get_offset_of__hostname_1() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ____hostname_1)); }
	inline String_t* get__hostname_1() const { return ____hostname_1; }
	inline String_t** get_address_of__hostname_1() { return &____hostname_1; }
	inline void set__hostname_1(String_t* value)
	{
		____hostname_1 = value;
		Il2CppCodeGenWriteBarrier((&____hostname_1), value);
	}

	inline static int32_t get_offset_of__listener_2() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ____listener_2)); }
	inline HttpListener_t2467819517 * get__listener_2() const { return ____listener_2; }
	inline HttpListener_t2467819517 ** get_address_of__listener_2() { return &____listener_2; }
	inline void set__listener_2(HttpListener_t2467819517 * value)
	{
		____listener_2 = value;
		Il2CppCodeGenWriteBarrier((&____listener_2), value);
	}

	inline static int32_t get_offset_of__logger_3() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ____logger_3)); }
	inline Logger_t4025333586 * get__logger_3() const { return ____logger_3; }
	inline Logger_t4025333586 ** get_address_of__logger_3() { return &____logger_3; }
	inline void set__logger_3(Logger_t4025333586 * value)
	{
		____logger_3 = value;
		Il2CppCodeGenWriteBarrier((&____logger_3), value);
	}

	inline static int32_t get_offset_of__port_4() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ____port_4)); }
	inline int32_t get__port_4() const { return ____port_4; }
	inline int32_t* get_address_of__port_4() { return &____port_4; }
	inline void set__port_4(int32_t value)
	{
		____port_4 = value;
	}

	inline static int32_t get_offset_of__receiveThread_5() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ____receiveThread_5)); }
	inline Thread_t2300836069 * get__receiveThread_5() const { return ____receiveThread_5; }
	inline Thread_t2300836069 ** get_address_of__receiveThread_5() { return &____receiveThread_5; }
	inline void set__receiveThread_5(Thread_t2300836069 * value)
	{
		____receiveThread_5 = value;
		Il2CppCodeGenWriteBarrier((&____receiveThread_5), value);
	}

	inline static int32_t get_offset_of__rootPath_6() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ____rootPath_6)); }
	inline String_t* get__rootPath_6() const { return ____rootPath_6; }
	inline String_t** get_address_of__rootPath_6() { return &____rootPath_6; }
	inline void set__rootPath_6(String_t* value)
	{
		____rootPath_6 = value;
		Il2CppCodeGenWriteBarrier((&____rootPath_6), value);
	}

	inline static int32_t get_offset_of__secure_7() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ____secure_7)); }
	inline bool get__secure_7() const { return ____secure_7; }
	inline bool* get_address_of__secure_7() { return &____secure_7; }
	inline void set__secure_7(bool value)
	{
		____secure_7 = value;
	}

	inline static int32_t get_offset_of__services_8() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ____services_8)); }
	inline WebSocketServiceManager_t2425053 * get__services_8() const { return ____services_8; }
	inline WebSocketServiceManager_t2425053 ** get_address_of__services_8() { return &____services_8; }
	inline void set__services_8(WebSocketServiceManager_t2425053 * value)
	{
		____services_8 = value;
		Il2CppCodeGenWriteBarrier((&____services_8), value);
	}

	inline static int32_t get_offset_of__state_9() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ____state_9)); }
	inline int32_t get__state_9() const { return ____state_9; }
	inline int32_t* get_address_of__state_9() { return &____state_9; }
	inline void set__state_9(int32_t value)
	{
		____state_9 = value;
	}

	inline static int32_t get_offset_of__sync_10() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ____sync_10)); }
	inline RuntimeObject * get__sync_10() const { return ____sync_10; }
	inline RuntimeObject ** get_address_of__sync_10() { return &____sync_10; }
	inline void set__sync_10(RuntimeObject * value)
	{
		____sync_10 = value;
		Il2CppCodeGenWriteBarrier((&____sync_10), value);
	}

	inline static int32_t get_offset_of__windows_11() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ____windows_11)); }
	inline bool get__windows_11() const { return ____windows_11; }
	inline bool* get_address_of__windows_11() { return &____windows_11; }
	inline void set__windows_11(bool value)
	{
		____windows_11 = value;
	}

	inline static int32_t get_offset_of_OnConnect_12() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ___OnConnect_12)); }
	inline EventHandler_1_t3222597438 * get_OnConnect_12() const { return ___OnConnect_12; }
	inline EventHandler_1_t3222597438 ** get_address_of_OnConnect_12() { return &___OnConnect_12; }
	inline void set_OnConnect_12(EventHandler_1_t3222597438 * value)
	{
		___OnConnect_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnConnect_12), value);
	}

	inline static int32_t get_offset_of_OnDelete_13() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ___OnDelete_13)); }
	inline EventHandler_1_t3222597438 * get_OnDelete_13() const { return ___OnDelete_13; }
	inline EventHandler_1_t3222597438 ** get_address_of_OnDelete_13() { return &___OnDelete_13; }
	inline void set_OnDelete_13(EventHandler_1_t3222597438 * value)
	{
		___OnDelete_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnDelete_13), value);
	}

	inline static int32_t get_offset_of_OnGet_14() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ___OnGet_14)); }
	inline EventHandler_1_t3222597438 * get_OnGet_14() const { return ___OnGet_14; }
	inline EventHandler_1_t3222597438 ** get_address_of_OnGet_14() { return &___OnGet_14; }
	inline void set_OnGet_14(EventHandler_1_t3222597438 * value)
	{
		___OnGet_14 = value;
		Il2CppCodeGenWriteBarrier((&___OnGet_14), value);
	}

	inline static int32_t get_offset_of_OnHead_15() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ___OnHead_15)); }
	inline EventHandler_1_t3222597438 * get_OnHead_15() const { return ___OnHead_15; }
	inline EventHandler_1_t3222597438 ** get_address_of_OnHead_15() { return &___OnHead_15; }
	inline void set_OnHead_15(EventHandler_1_t3222597438 * value)
	{
		___OnHead_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnHead_15), value);
	}

	inline static int32_t get_offset_of_OnOptions_16() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ___OnOptions_16)); }
	inline EventHandler_1_t3222597438 * get_OnOptions_16() const { return ___OnOptions_16; }
	inline EventHandler_1_t3222597438 ** get_address_of_OnOptions_16() { return &___OnOptions_16; }
	inline void set_OnOptions_16(EventHandler_1_t3222597438 * value)
	{
		___OnOptions_16 = value;
		Il2CppCodeGenWriteBarrier((&___OnOptions_16), value);
	}

	inline static int32_t get_offset_of_OnPatch_17() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ___OnPatch_17)); }
	inline EventHandler_1_t3222597438 * get_OnPatch_17() const { return ___OnPatch_17; }
	inline EventHandler_1_t3222597438 ** get_address_of_OnPatch_17() { return &___OnPatch_17; }
	inline void set_OnPatch_17(EventHandler_1_t3222597438 * value)
	{
		___OnPatch_17 = value;
		Il2CppCodeGenWriteBarrier((&___OnPatch_17), value);
	}

	inline static int32_t get_offset_of_OnPost_18() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ___OnPost_18)); }
	inline EventHandler_1_t3222597438 * get_OnPost_18() const { return ___OnPost_18; }
	inline EventHandler_1_t3222597438 ** get_address_of_OnPost_18() { return &___OnPost_18; }
	inline void set_OnPost_18(EventHandler_1_t3222597438 * value)
	{
		___OnPost_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnPost_18), value);
	}

	inline static int32_t get_offset_of_OnPut_19() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ___OnPut_19)); }
	inline EventHandler_1_t3222597438 * get_OnPut_19() const { return ___OnPut_19; }
	inline EventHandler_1_t3222597438 ** get_address_of_OnPut_19() { return &___OnPut_19; }
	inline void set_OnPut_19(EventHandler_1_t3222597438 * value)
	{
		___OnPut_19 = value;
		Il2CppCodeGenWriteBarrier((&___OnPut_19), value);
	}

	inline static int32_t get_offset_of_OnTrace_20() { return static_cast<int32_t>(offsetof(HttpServer_t3049239368, ___OnTrace_20)); }
	inline EventHandler_1_t3222597438 * get_OnTrace_20() const { return ___OnTrace_20; }
	inline EventHandler_1_t3222597438 ** get_address_of_OnTrace_20() { return &___OnTrace_20; }
	inline void set_OnTrace_20(EventHandler_1_t3222597438 * value)
	{
		___OnTrace_20 = value;
		Il2CppCodeGenWriteBarrier((&___OnTrace_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSERVER_T3049239368_H
#ifndef WEBSOCKETSERVER_T2289661728_H
#define WEBSOCKETSERVER_T2289661728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServer
struct  WebSocketServer_t2289661728  : public RuntimeObject
{
public:
	// System.Net.IPAddress WebSocketSharp.Server.WebSocketServer::_address
	IPAddress_t241777590 * ____address_0;
	// WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Server.WebSocketServer::_authSchemes
	int32_t ____authSchemes_1;
	// System.Boolean WebSocketSharp.Server.WebSocketServer::_dnsStyle
	bool ____dnsStyle_3;
	// System.String WebSocketSharp.Server.WebSocketServer::_hostname
	String_t* ____hostname_4;
	// System.Net.Sockets.TcpListener WebSocketSharp.Server.WebSocketServer::_listener
	TcpListener_t3499576757 * ____listener_5;
	// WebSocketSharp.Logger WebSocketSharp.Server.WebSocketServer::_logger
	Logger_t4025333586 * ____logger_6;
	// System.Int32 WebSocketSharp.Server.WebSocketServer::_port
	int32_t ____port_7;
	// System.String WebSocketSharp.Server.WebSocketServer::_realm
	String_t* ____realm_8;
	// System.Threading.Thread WebSocketSharp.Server.WebSocketServer::_receiveThread
	Thread_t2300836069 * ____receiveThread_9;
	// System.Boolean WebSocketSharp.Server.WebSocketServer::_reuseAddress
	bool ____reuseAddress_10;
	// System.Boolean WebSocketSharp.Server.WebSocketServer::_secure
	bool ____secure_11;
	// WebSocketSharp.Server.WebSocketServiceManager WebSocketSharp.Server.WebSocketServer::_services
	WebSocketServiceManager_t2425053 * ____services_12;
	// WebSocketSharp.Net.ServerSslConfiguration WebSocketSharp.Server.WebSocketServer::_sslConfig
	ServerSslConfiguration_t3240511754 * ____sslConfig_13;
	// WebSocketSharp.Server.ServerState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.WebSocketServer::_state
	int32_t ____state_14;
	// System.Object WebSocketSharp.Server.WebSocketServer::_sync
	RuntimeObject * ____sync_15;
	// System.Func`2<System.Security.Principal.IIdentity,WebSocketSharp.Net.NetworkCredential> WebSocketSharp.Server.WebSocketServer::_userCredFinder
	Func_2_t2697316325 * ____userCredFinder_16;

public:
	inline static int32_t get_offset_of__address_0() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____address_0)); }
	inline IPAddress_t241777590 * get__address_0() const { return ____address_0; }
	inline IPAddress_t241777590 ** get_address_of__address_0() { return &____address_0; }
	inline void set__address_0(IPAddress_t241777590 * value)
	{
		____address_0 = value;
		Il2CppCodeGenWriteBarrier((&____address_0), value);
	}

	inline static int32_t get_offset_of__authSchemes_1() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____authSchemes_1)); }
	inline int32_t get__authSchemes_1() const { return ____authSchemes_1; }
	inline int32_t* get_address_of__authSchemes_1() { return &____authSchemes_1; }
	inline void set__authSchemes_1(int32_t value)
	{
		____authSchemes_1 = value;
	}

	inline static int32_t get_offset_of__dnsStyle_3() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____dnsStyle_3)); }
	inline bool get__dnsStyle_3() const { return ____dnsStyle_3; }
	inline bool* get_address_of__dnsStyle_3() { return &____dnsStyle_3; }
	inline void set__dnsStyle_3(bool value)
	{
		____dnsStyle_3 = value;
	}

	inline static int32_t get_offset_of__hostname_4() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____hostname_4)); }
	inline String_t* get__hostname_4() const { return ____hostname_4; }
	inline String_t** get_address_of__hostname_4() { return &____hostname_4; }
	inline void set__hostname_4(String_t* value)
	{
		____hostname_4 = value;
		Il2CppCodeGenWriteBarrier((&____hostname_4), value);
	}

	inline static int32_t get_offset_of__listener_5() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____listener_5)); }
	inline TcpListener_t3499576757 * get__listener_5() const { return ____listener_5; }
	inline TcpListener_t3499576757 ** get_address_of__listener_5() { return &____listener_5; }
	inline void set__listener_5(TcpListener_t3499576757 * value)
	{
		____listener_5 = value;
		Il2CppCodeGenWriteBarrier((&____listener_5), value);
	}

	inline static int32_t get_offset_of__logger_6() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____logger_6)); }
	inline Logger_t4025333586 * get__logger_6() const { return ____logger_6; }
	inline Logger_t4025333586 ** get_address_of__logger_6() { return &____logger_6; }
	inline void set__logger_6(Logger_t4025333586 * value)
	{
		____logger_6 = value;
		Il2CppCodeGenWriteBarrier((&____logger_6), value);
	}

	inline static int32_t get_offset_of__port_7() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____port_7)); }
	inline int32_t get__port_7() const { return ____port_7; }
	inline int32_t* get_address_of__port_7() { return &____port_7; }
	inline void set__port_7(int32_t value)
	{
		____port_7 = value;
	}

	inline static int32_t get_offset_of__realm_8() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____realm_8)); }
	inline String_t* get__realm_8() const { return ____realm_8; }
	inline String_t** get_address_of__realm_8() { return &____realm_8; }
	inline void set__realm_8(String_t* value)
	{
		____realm_8 = value;
		Il2CppCodeGenWriteBarrier((&____realm_8), value);
	}

	inline static int32_t get_offset_of__receiveThread_9() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____receiveThread_9)); }
	inline Thread_t2300836069 * get__receiveThread_9() const { return ____receiveThread_9; }
	inline Thread_t2300836069 ** get_address_of__receiveThread_9() { return &____receiveThread_9; }
	inline void set__receiveThread_9(Thread_t2300836069 * value)
	{
		____receiveThread_9 = value;
		Il2CppCodeGenWriteBarrier((&____receiveThread_9), value);
	}

	inline static int32_t get_offset_of__reuseAddress_10() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____reuseAddress_10)); }
	inline bool get__reuseAddress_10() const { return ____reuseAddress_10; }
	inline bool* get_address_of__reuseAddress_10() { return &____reuseAddress_10; }
	inline void set__reuseAddress_10(bool value)
	{
		____reuseAddress_10 = value;
	}

	inline static int32_t get_offset_of__secure_11() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____secure_11)); }
	inline bool get__secure_11() const { return ____secure_11; }
	inline bool* get_address_of__secure_11() { return &____secure_11; }
	inline void set__secure_11(bool value)
	{
		____secure_11 = value;
	}

	inline static int32_t get_offset_of__services_12() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____services_12)); }
	inline WebSocketServiceManager_t2425053 * get__services_12() const { return ____services_12; }
	inline WebSocketServiceManager_t2425053 ** get_address_of__services_12() { return &____services_12; }
	inline void set__services_12(WebSocketServiceManager_t2425053 * value)
	{
		____services_12 = value;
		Il2CppCodeGenWriteBarrier((&____services_12), value);
	}

	inline static int32_t get_offset_of__sslConfig_13() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____sslConfig_13)); }
	inline ServerSslConfiguration_t3240511754 * get__sslConfig_13() const { return ____sslConfig_13; }
	inline ServerSslConfiguration_t3240511754 ** get_address_of__sslConfig_13() { return &____sslConfig_13; }
	inline void set__sslConfig_13(ServerSslConfiguration_t3240511754 * value)
	{
		____sslConfig_13 = value;
		Il2CppCodeGenWriteBarrier((&____sslConfig_13), value);
	}

	inline static int32_t get_offset_of__state_14() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____state_14)); }
	inline int32_t get__state_14() const { return ____state_14; }
	inline int32_t* get_address_of__state_14() { return &____state_14; }
	inline void set__state_14(int32_t value)
	{
		____state_14 = value;
	}

	inline static int32_t get_offset_of__sync_15() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____sync_15)); }
	inline RuntimeObject * get__sync_15() const { return ____sync_15; }
	inline RuntimeObject ** get_address_of__sync_15() { return &____sync_15; }
	inline void set__sync_15(RuntimeObject * value)
	{
		____sync_15 = value;
		Il2CppCodeGenWriteBarrier((&____sync_15), value);
	}

	inline static int32_t get_offset_of__userCredFinder_16() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728, ____userCredFinder_16)); }
	inline Func_2_t2697316325 * get__userCredFinder_16() const { return ____userCredFinder_16; }
	inline Func_2_t2697316325 ** get_address_of__userCredFinder_16() { return &____userCredFinder_16; }
	inline void set__userCredFinder_16(Func_2_t2697316325 * value)
	{
		____userCredFinder_16 = value;
		Il2CppCodeGenWriteBarrier((&____userCredFinder_16), value);
	}
};

struct WebSocketServer_t2289661728_StaticFields
{
public:
	// System.String WebSocketSharp.Server.WebSocketServer::_defaultRealm
	String_t* ____defaultRealm_2;

public:
	inline static int32_t get_offset_of__defaultRealm_2() { return static_cast<int32_t>(offsetof(WebSocketServer_t2289661728_StaticFields, ____defaultRealm_2)); }
	inline String_t* get__defaultRealm_2() const { return ____defaultRealm_2; }
	inline String_t** get_address_of__defaultRealm_2() { return &____defaultRealm_2; }
	inline void set__defaultRealm_2(String_t* value)
	{
		____defaultRealm_2 = value;
		Il2CppCodeGenWriteBarrier((&____defaultRealm_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSERVER_T2289661728_H
#ifndef WEBSOCKET_T62038747_H
#define WEBSOCKET_T62038747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocket
struct  WebSocket_t62038747  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.AuthenticationChallenge WebSocketSharp.WebSocket::_authChallenge
	AuthenticationChallenge_t3244447305 * ____authChallenge_0;
	// System.String WebSocketSharp.WebSocket::_base64Key
	String_t* ____base64Key_1;
	// System.Boolean WebSocketSharp.WebSocket::_client
	bool ____client_2;
	// System.Action WebSocketSharp.WebSocket::_closeContext
	Action_t1264377477 * ____closeContext_3;
	// WebSocketSharp.CompressionMethod WebSocketSharp.WebSocket::_compression
	uint8_t ____compression_4;
	// WebSocketSharp.Net.WebSockets.WebSocketContext WebSocketSharp.WebSocket::_context
	WebSocketContext_t619421455 * ____context_5;
	// WebSocketSharp.Net.CookieCollection WebSocketSharp.WebSocket::_cookies
	CookieCollection_t962330244 * ____cookies_6;
	// WebSocketSharp.Net.NetworkCredential WebSocketSharp.WebSocket::_credentials
	NetworkCredential_t1094796801 * ____credentials_7;
	// System.Boolean WebSocketSharp.WebSocket::_emitOnPing
	bool ____emitOnPing_8;
	// System.Boolean WebSocketSharp.WebSocket::_enableRedirection
	bool ____enableRedirection_9;
	// System.Threading.AutoResetEvent WebSocketSharp.WebSocket::_exitReceiving
	AutoResetEvent_t1333520283 * ____exitReceiving_10;
	// System.String WebSocketSharp.WebSocket::_extensions
	String_t* ____extensions_11;
	// System.Boolean WebSocketSharp.WebSocket::_extensionsRequested
	bool ____extensionsRequested_12;
	// System.Object WebSocketSharp.WebSocket::_forConn
	RuntimeObject * ____forConn_13;
	// System.Object WebSocketSharp.WebSocket::_forMessageEventQueue
	RuntimeObject * ____forMessageEventQueue_14;
	// System.Object WebSocketSharp.WebSocket::_forSend
	RuntimeObject * ____forSend_15;
	// System.IO.MemoryStream WebSocketSharp.WebSocket::_fragmentsBuffer
	MemoryStream_t94973147 * ____fragmentsBuffer_16;
	// System.Boolean WebSocketSharp.WebSocket::_fragmentsCompressed
	bool ____fragmentsCompressed_17;
	// WebSocketSharp.Opcode WebSocketSharp.WebSocket::_fragmentsOpcode
	uint8_t ____fragmentsOpcode_18;
	// System.Func`2<WebSocketSharp.Net.WebSockets.WebSocketContext,System.String> WebSocketSharp.WebSocket::_handshakeRequestChecker
	Func_2_t1203693676 * ____handshakeRequestChecker_20;
	// System.Boolean WebSocketSharp.WebSocket::_ignoreExtensions
	bool ____ignoreExtensions_21;
	// System.Boolean WebSocketSharp.WebSocket::_inContinuation
	bool ____inContinuation_22;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_inMessage
	bool ____inMessage_23;
	// WebSocketSharp.Logger modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_logger
	Logger_t4025333586 * ____logger_24;
	// System.Action`1<WebSocketSharp.MessageEventArgs> WebSocketSharp.WebSocket::_message
	Action_1_t2397525318 * ____message_25;
	// System.Collections.Generic.Queue`1<WebSocketSharp.MessageEventArgs> WebSocketSharp.WebSocket::_messageEventQueue
	Queue_1_t2071317217 * ____messageEventQueue_26;
	// System.UInt32 WebSocketSharp.WebSocket::_nonceCount
	uint32_t ____nonceCount_27;
	// System.String WebSocketSharp.WebSocket::_origin
	String_t* ____origin_28;
	// System.Boolean WebSocketSharp.WebSocket::_preAuth
	bool ____preAuth_29;
	// System.String WebSocketSharp.WebSocket::_protocol
	String_t* ____protocol_30;
	// System.String[] WebSocketSharp.WebSocket::_protocols
	StringU5BU5D_t1281789340* ____protocols_31;
	// System.Boolean WebSocketSharp.WebSocket::_protocolsRequested
	bool ____protocolsRequested_32;
	// WebSocketSharp.Net.NetworkCredential WebSocketSharp.WebSocket::_proxyCredentials
	NetworkCredential_t1094796801 * ____proxyCredentials_33;
	// System.Uri WebSocketSharp.WebSocket::_proxyUri
	Uri_t100236324 * ____proxyUri_34;
	// WebSocketSharp.WebSocketState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.WebSocket::_readyState
	uint16_t ____readyState_35;
	// System.Threading.AutoResetEvent WebSocketSharp.WebSocket::_receivePong
	AutoResetEvent_t1333520283 * ____receivePong_36;
	// System.Boolean WebSocketSharp.WebSocket::_secure
	bool ____secure_37;
	// WebSocketSharp.Net.ClientSslConfiguration WebSocketSharp.WebSocket::_sslConfig
	ClientSslConfiguration_t664129470 * ____sslConfig_38;
	// System.IO.Stream WebSocketSharp.WebSocket::_stream
	Stream_t1273022909 * ____stream_39;
	// System.Net.Sockets.TcpClient WebSocketSharp.WebSocket::_tcpClient
	TcpClient_t822906377 * ____tcpClient_40;
	// System.Uri WebSocketSharp.WebSocket::_uri
	Uri_t100236324 * ____uri_41;
	// System.TimeSpan WebSocketSharp.WebSocket::_waitTime
	TimeSpan_t881159249  ____waitTime_43;
	// System.EventHandler`1<WebSocketSharp.CloseEventArgs> WebSocketSharp.WebSocket::OnClose
	EventHandler_1_t4095840750 * ___OnClose_47;
	// System.EventHandler`1<WebSocketSharp.ErrorEventArgs> WebSocketSharp.WebSocket::OnError
	EventHandler_1_t1344838444 * ___OnError_48;
	// System.EventHandler`1<WebSocketSharp.MessageEventArgs> WebSocketSharp.WebSocket::OnMessage
	EventHandler_1_t149217156 * ___OnMessage_49;
	// System.EventHandler WebSocketSharp.WebSocket::OnOpen
	EventHandler_t1348719766 * ___OnOpen_50;

public:
	inline static int32_t get_offset_of__authChallenge_0() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____authChallenge_0)); }
	inline AuthenticationChallenge_t3244447305 * get__authChallenge_0() const { return ____authChallenge_0; }
	inline AuthenticationChallenge_t3244447305 ** get_address_of__authChallenge_0() { return &____authChallenge_0; }
	inline void set__authChallenge_0(AuthenticationChallenge_t3244447305 * value)
	{
		____authChallenge_0 = value;
		Il2CppCodeGenWriteBarrier((&____authChallenge_0), value);
	}

	inline static int32_t get_offset_of__base64Key_1() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____base64Key_1)); }
	inline String_t* get__base64Key_1() const { return ____base64Key_1; }
	inline String_t** get_address_of__base64Key_1() { return &____base64Key_1; }
	inline void set__base64Key_1(String_t* value)
	{
		____base64Key_1 = value;
		Il2CppCodeGenWriteBarrier((&____base64Key_1), value);
	}

	inline static int32_t get_offset_of__client_2() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____client_2)); }
	inline bool get__client_2() const { return ____client_2; }
	inline bool* get_address_of__client_2() { return &____client_2; }
	inline void set__client_2(bool value)
	{
		____client_2 = value;
	}

	inline static int32_t get_offset_of__closeContext_3() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____closeContext_3)); }
	inline Action_t1264377477 * get__closeContext_3() const { return ____closeContext_3; }
	inline Action_t1264377477 ** get_address_of__closeContext_3() { return &____closeContext_3; }
	inline void set__closeContext_3(Action_t1264377477 * value)
	{
		____closeContext_3 = value;
		Il2CppCodeGenWriteBarrier((&____closeContext_3), value);
	}

	inline static int32_t get_offset_of__compression_4() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____compression_4)); }
	inline uint8_t get__compression_4() const { return ____compression_4; }
	inline uint8_t* get_address_of__compression_4() { return &____compression_4; }
	inline void set__compression_4(uint8_t value)
	{
		____compression_4 = value;
	}

	inline static int32_t get_offset_of__context_5() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____context_5)); }
	inline WebSocketContext_t619421455 * get__context_5() const { return ____context_5; }
	inline WebSocketContext_t619421455 ** get_address_of__context_5() { return &____context_5; }
	inline void set__context_5(WebSocketContext_t619421455 * value)
	{
		____context_5 = value;
		Il2CppCodeGenWriteBarrier((&____context_5), value);
	}

	inline static int32_t get_offset_of__cookies_6() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____cookies_6)); }
	inline CookieCollection_t962330244 * get__cookies_6() const { return ____cookies_6; }
	inline CookieCollection_t962330244 ** get_address_of__cookies_6() { return &____cookies_6; }
	inline void set__cookies_6(CookieCollection_t962330244 * value)
	{
		____cookies_6 = value;
		Il2CppCodeGenWriteBarrier((&____cookies_6), value);
	}

	inline static int32_t get_offset_of__credentials_7() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____credentials_7)); }
	inline NetworkCredential_t1094796801 * get__credentials_7() const { return ____credentials_7; }
	inline NetworkCredential_t1094796801 ** get_address_of__credentials_7() { return &____credentials_7; }
	inline void set__credentials_7(NetworkCredential_t1094796801 * value)
	{
		____credentials_7 = value;
		Il2CppCodeGenWriteBarrier((&____credentials_7), value);
	}

	inline static int32_t get_offset_of__emitOnPing_8() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____emitOnPing_8)); }
	inline bool get__emitOnPing_8() const { return ____emitOnPing_8; }
	inline bool* get_address_of__emitOnPing_8() { return &____emitOnPing_8; }
	inline void set__emitOnPing_8(bool value)
	{
		____emitOnPing_8 = value;
	}

	inline static int32_t get_offset_of__enableRedirection_9() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____enableRedirection_9)); }
	inline bool get__enableRedirection_9() const { return ____enableRedirection_9; }
	inline bool* get_address_of__enableRedirection_9() { return &____enableRedirection_9; }
	inline void set__enableRedirection_9(bool value)
	{
		____enableRedirection_9 = value;
	}

	inline static int32_t get_offset_of__exitReceiving_10() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____exitReceiving_10)); }
	inline AutoResetEvent_t1333520283 * get__exitReceiving_10() const { return ____exitReceiving_10; }
	inline AutoResetEvent_t1333520283 ** get_address_of__exitReceiving_10() { return &____exitReceiving_10; }
	inline void set__exitReceiving_10(AutoResetEvent_t1333520283 * value)
	{
		____exitReceiving_10 = value;
		Il2CppCodeGenWriteBarrier((&____exitReceiving_10), value);
	}

	inline static int32_t get_offset_of__extensions_11() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____extensions_11)); }
	inline String_t* get__extensions_11() const { return ____extensions_11; }
	inline String_t** get_address_of__extensions_11() { return &____extensions_11; }
	inline void set__extensions_11(String_t* value)
	{
		____extensions_11 = value;
		Il2CppCodeGenWriteBarrier((&____extensions_11), value);
	}

	inline static int32_t get_offset_of__extensionsRequested_12() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____extensionsRequested_12)); }
	inline bool get__extensionsRequested_12() const { return ____extensionsRequested_12; }
	inline bool* get_address_of__extensionsRequested_12() { return &____extensionsRequested_12; }
	inline void set__extensionsRequested_12(bool value)
	{
		____extensionsRequested_12 = value;
	}

	inline static int32_t get_offset_of__forConn_13() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____forConn_13)); }
	inline RuntimeObject * get__forConn_13() const { return ____forConn_13; }
	inline RuntimeObject ** get_address_of__forConn_13() { return &____forConn_13; }
	inline void set__forConn_13(RuntimeObject * value)
	{
		____forConn_13 = value;
		Il2CppCodeGenWriteBarrier((&____forConn_13), value);
	}

	inline static int32_t get_offset_of__forMessageEventQueue_14() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____forMessageEventQueue_14)); }
	inline RuntimeObject * get__forMessageEventQueue_14() const { return ____forMessageEventQueue_14; }
	inline RuntimeObject ** get_address_of__forMessageEventQueue_14() { return &____forMessageEventQueue_14; }
	inline void set__forMessageEventQueue_14(RuntimeObject * value)
	{
		____forMessageEventQueue_14 = value;
		Il2CppCodeGenWriteBarrier((&____forMessageEventQueue_14), value);
	}

	inline static int32_t get_offset_of__forSend_15() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____forSend_15)); }
	inline RuntimeObject * get__forSend_15() const { return ____forSend_15; }
	inline RuntimeObject ** get_address_of__forSend_15() { return &____forSend_15; }
	inline void set__forSend_15(RuntimeObject * value)
	{
		____forSend_15 = value;
		Il2CppCodeGenWriteBarrier((&____forSend_15), value);
	}

	inline static int32_t get_offset_of__fragmentsBuffer_16() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____fragmentsBuffer_16)); }
	inline MemoryStream_t94973147 * get__fragmentsBuffer_16() const { return ____fragmentsBuffer_16; }
	inline MemoryStream_t94973147 ** get_address_of__fragmentsBuffer_16() { return &____fragmentsBuffer_16; }
	inline void set__fragmentsBuffer_16(MemoryStream_t94973147 * value)
	{
		____fragmentsBuffer_16 = value;
		Il2CppCodeGenWriteBarrier((&____fragmentsBuffer_16), value);
	}

	inline static int32_t get_offset_of__fragmentsCompressed_17() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____fragmentsCompressed_17)); }
	inline bool get__fragmentsCompressed_17() const { return ____fragmentsCompressed_17; }
	inline bool* get_address_of__fragmentsCompressed_17() { return &____fragmentsCompressed_17; }
	inline void set__fragmentsCompressed_17(bool value)
	{
		____fragmentsCompressed_17 = value;
	}

	inline static int32_t get_offset_of__fragmentsOpcode_18() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____fragmentsOpcode_18)); }
	inline uint8_t get__fragmentsOpcode_18() const { return ____fragmentsOpcode_18; }
	inline uint8_t* get_address_of__fragmentsOpcode_18() { return &____fragmentsOpcode_18; }
	inline void set__fragmentsOpcode_18(uint8_t value)
	{
		____fragmentsOpcode_18 = value;
	}

	inline static int32_t get_offset_of__handshakeRequestChecker_20() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____handshakeRequestChecker_20)); }
	inline Func_2_t1203693676 * get__handshakeRequestChecker_20() const { return ____handshakeRequestChecker_20; }
	inline Func_2_t1203693676 ** get_address_of__handshakeRequestChecker_20() { return &____handshakeRequestChecker_20; }
	inline void set__handshakeRequestChecker_20(Func_2_t1203693676 * value)
	{
		____handshakeRequestChecker_20 = value;
		Il2CppCodeGenWriteBarrier((&____handshakeRequestChecker_20), value);
	}

	inline static int32_t get_offset_of__ignoreExtensions_21() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____ignoreExtensions_21)); }
	inline bool get__ignoreExtensions_21() const { return ____ignoreExtensions_21; }
	inline bool* get_address_of__ignoreExtensions_21() { return &____ignoreExtensions_21; }
	inline void set__ignoreExtensions_21(bool value)
	{
		____ignoreExtensions_21 = value;
	}

	inline static int32_t get_offset_of__inContinuation_22() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____inContinuation_22)); }
	inline bool get__inContinuation_22() const { return ____inContinuation_22; }
	inline bool* get_address_of__inContinuation_22() { return &____inContinuation_22; }
	inline void set__inContinuation_22(bool value)
	{
		____inContinuation_22 = value;
	}

	inline static int32_t get_offset_of__inMessage_23() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____inMessage_23)); }
	inline bool get__inMessage_23() const { return ____inMessage_23; }
	inline bool* get_address_of__inMessage_23() { return &____inMessage_23; }
	inline void set__inMessage_23(bool value)
	{
		____inMessage_23 = value;
	}

	inline static int32_t get_offset_of__logger_24() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____logger_24)); }
	inline Logger_t4025333586 * get__logger_24() const { return ____logger_24; }
	inline Logger_t4025333586 ** get_address_of__logger_24() { return &____logger_24; }
	inline void set__logger_24(Logger_t4025333586 * value)
	{
		____logger_24 = value;
		Il2CppCodeGenWriteBarrier((&____logger_24), value);
	}

	inline static int32_t get_offset_of__message_25() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____message_25)); }
	inline Action_1_t2397525318 * get__message_25() const { return ____message_25; }
	inline Action_1_t2397525318 ** get_address_of__message_25() { return &____message_25; }
	inline void set__message_25(Action_1_t2397525318 * value)
	{
		____message_25 = value;
		Il2CppCodeGenWriteBarrier((&____message_25), value);
	}

	inline static int32_t get_offset_of__messageEventQueue_26() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____messageEventQueue_26)); }
	inline Queue_1_t2071317217 * get__messageEventQueue_26() const { return ____messageEventQueue_26; }
	inline Queue_1_t2071317217 ** get_address_of__messageEventQueue_26() { return &____messageEventQueue_26; }
	inline void set__messageEventQueue_26(Queue_1_t2071317217 * value)
	{
		____messageEventQueue_26 = value;
		Il2CppCodeGenWriteBarrier((&____messageEventQueue_26), value);
	}

	inline static int32_t get_offset_of__nonceCount_27() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____nonceCount_27)); }
	inline uint32_t get__nonceCount_27() const { return ____nonceCount_27; }
	inline uint32_t* get_address_of__nonceCount_27() { return &____nonceCount_27; }
	inline void set__nonceCount_27(uint32_t value)
	{
		____nonceCount_27 = value;
	}

	inline static int32_t get_offset_of__origin_28() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____origin_28)); }
	inline String_t* get__origin_28() const { return ____origin_28; }
	inline String_t** get_address_of__origin_28() { return &____origin_28; }
	inline void set__origin_28(String_t* value)
	{
		____origin_28 = value;
		Il2CppCodeGenWriteBarrier((&____origin_28), value);
	}

	inline static int32_t get_offset_of__preAuth_29() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____preAuth_29)); }
	inline bool get__preAuth_29() const { return ____preAuth_29; }
	inline bool* get_address_of__preAuth_29() { return &____preAuth_29; }
	inline void set__preAuth_29(bool value)
	{
		____preAuth_29 = value;
	}

	inline static int32_t get_offset_of__protocol_30() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____protocol_30)); }
	inline String_t* get__protocol_30() const { return ____protocol_30; }
	inline String_t** get_address_of__protocol_30() { return &____protocol_30; }
	inline void set__protocol_30(String_t* value)
	{
		____protocol_30 = value;
		Il2CppCodeGenWriteBarrier((&____protocol_30), value);
	}

	inline static int32_t get_offset_of__protocols_31() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____protocols_31)); }
	inline StringU5BU5D_t1281789340* get__protocols_31() const { return ____protocols_31; }
	inline StringU5BU5D_t1281789340** get_address_of__protocols_31() { return &____protocols_31; }
	inline void set__protocols_31(StringU5BU5D_t1281789340* value)
	{
		____protocols_31 = value;
		Il2CppCodeGenWriteBarrier((&____protocols_31), value);
	}

	inline static int32_t get_offset_of__protocolsRequested_32() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____protocolsRequested_32)); }
	inline bool get__protocolsRequested_32() const { return ____protocolsRequested_32; }
	inline bool* get_address_of__protocolsRequested_32() { return &____protocolsRequested_32; }
	inline void set__protocolsRequested_32(bool value)
	{
		____protocolsRequested_32 = value;
	}

	inline static int32_t get_offset_of__proxyCredentials_33() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____proxyCredentials_33)); }
	inline NetworkCredential_t1094796801 * get__proxyCredentials_33() const { return ____proxyCredentials_33; }
	inline NetworkCredential_t1094796801 ** get_address_of__proxyCredentials_33() { return &____proxyCredentials_33; }
	inline void set__proxyCredentials_33(NetworkCredential_t1094796801 * value)
	{
		____proxyCredentials_33 = value;
		Il2CppCodeGenWriteBarrier((&____proxyCredentials_33), value);
	}

	inline static int32_t get_offset_of__proxyUri_34() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____proxyUri_34)); }
	inline Uri_t100236324 * get__proxyUri_34() const { return ____proxyUri_34; }
	inline Uri_t100236324 ** get_address_of__proxyUri_34() { return &____proxyUri_34; }
	inline void set__proxyUri_34(Uri_t100236324 * value)
	{
		____proxyUri_34 = value;
		Il2CppCodeGenWriteBarrier((&____proxyUri_34), value);
	}

	inline static int32_t get_offset_of__readyState_35() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____readyState_35)); }
	inline uint16_t get__readyState_35() const { return ____readyState_35; }
	inline uint16_t* get_address_of__readyState_35() { return &____readyState_35; }
	inline void set__readyState_35(uint16_t value)
	{
		____readyState_35 = value;
	}

	inline static int32_t get_offset_of__receivePong_36() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____receivePong_36)); }
	inline AutoResetEvent_t1333520283 * get__receivePong_36() const { return ____receivePong_36; }
	inline AutoResetEvent_t1333520283 ** get_address_of__receivePong_36() { return &____receivePong_36; }
	inline void set__receivePong_36(AutoResetEvent_t1333520283 * value)
	{
		____receivePong_36 = value;
		Il2CppCodeGenWriteBarrier((&____receivePong_36), value);
	}

	inline static int32_t get_offset_of__secure_37() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____secure_37)); }
	inline bool get__secure_37() const { return ____secure_37; }
	inline bool* get_address_of__secure_37() { return &____secure_37; }
	inline void set__secure_37(bool value)
	{
		____secure_37 = value;
	}

	inline static int32_t get_offset_of__sslConfig_38() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____sslConfig_38)); }
	inline ClientSslConfiguration_t664129470 * get__sslConfig_38() const { return ____sslConfig_38; }
	inline ClientSslConfiguration_t664129470 ** get_address_of__sslConfig_38() { return &____sslConfig_38; }
	inline void set__sslConfig_38(ClientSslConfiguration_t664129470 * value)
	{
		____sslConfig_38 = value;
		Il2CppCodeGenWriteBarrier((&____sslConfig_38), value);
	}

	inline static int32_t get_offset_of__stream_39() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____stream_39)); }
	inline Stream_t1273022909 * get__stream_39() const { return ____stream_39; }
	inline Stream_t1273022909 ** get_address_of__stream_39() { return &____stream_39; }
	inline void set__stream_39(Stream_t1273022909 * value)
	{
		____stream_39 = value;
		Il2CppCodeGenWriteBarrier((&____stream_39), value);
	}

	inline static int32_t get_offset_of__tcpClient_40() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____tcpClient_40)); }
	inline TcpClient_t822906377 * get__tcpClient_40() const { return ____tcpClient_40; }
	inline TcpClient_t822906377 ** get_address_of__tcpClient_40() { return &____tcpClient_40; }
	inline void set__tcpClient_40(TcpClient_t822906377 * value)
	{
		____tcpClient_40 = value;
		Il2CppCodeGenWriteBarrier((&____tcpClient_40), value);
	}

	inline static int32_t get_offset_of__uri_41() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____uri_41)); }
	inline Uri_t100236324 * get__uri_41() const { return ____uri_41; }
	inline Uri_t100236324 ** get_address_of__uri_41() { return &____uri_41; }
	inline void set__uri_41(Uri_t100236324 * value)
	{
		____uri_41 = value;
		Il2CppCodeGenWriteBarrier((&____uri_41), value);
	}

	inline static int32_t get_offset_of__waitTime_43() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ____waitTime_43)); }
	inline TimeSpan_t881159249  get__waitTime_43() const { return ____waitTime_43; }
	inline TimeSpan_t881159249 * get_address_of__waitTime_43() { return &____waitTime_43; }
	inline void set__waitTime_43(TimeSpan_t881159249  value)
	{
		____waitTime_43 = value;
	}

	inline static int32_t get_offset_of_OnClose_47() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ___OnClose_47)); }
	inline EventHandler_1_t4095840750 * get_OnClose_47() const { return ___OnClose_47; }
	inline EventHandler_1_t4095840750 ** get_address_of_OnClose_47() { return &___OnClose_47; }
	inline void set_OnClose_47(EventHandler_1_t4095840750 * value)
	{
		___OnClose_47 = value;
		Il2CppCodeGenWriteBarrier((&___OnClose_47), value);
	}

	inline static int32_t get_offset_of_OnError_48() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ___OnError_48)); }
	inline EventHandler_1_t1344838444 * get_OnError_48() const { return ___OnError_48; }
	inline EventHandler_1_t1344838444 ** get_address_of_OnError_48() { return &___OnError_48; }
	inline void set_OnError_48(EventHandler_1_t1344838444 * value)
	{
		___OnError_48 = value;
		Il2CppCodeGenWriteBarrier((&___OnError_48), value);
	}

	inline static int32_t get_offset_of_OnMessage_49() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ___OnMessage_49)); }
	inline EventHandler_1_t149217156 * get_OnMessage_49() const { return ___OnMessage_49; }
	inline EventHandler_1_t149217156 ** get_address_of_OnMessage_49() { return &___OnMessage_49; }
	inline void set_OnMessage_49(EventHandler_1_t149217156 * value)
	{
		___OnMessage_49 = value;
		Il2CppCodeGenWriteBarrier((&___OnMessage_49), value);
	}

	inline static int32_t get_offset_of_OnOpen_50() { return static_cast<int32_t>(offsetof(WebSocket_t62038747, ___OnOpen_50)); }
	inline EventHandler_t1348719766 * get_OnOpen_50() const { return ___OnOpen_50; }
	inline EventHandler_t1348719766 ** get_address_of_OnOpen_50() { return &___OnOpen_50; }
	inline void set_OnOpen_50(EventHandler_t1348719766 * value)
	{
		___OnOpen_50 = value;
		Il2CppCodeGenWriteBarrier((&___OnOpen_50), value);
	}
};

struct WebSocket_t62038747_StaticFields
{
public:
	// System.Byte[] WebSocketSharp.WebSocket::EmptyBytes
	ByteU5BU5D_t4116647657* ___EmptyBytes_44;
	// System.Int32 WebSocketSharp.WebSocket::FragmentLength
	int32_t ___FragmentLength_45;
	// System.Security.Cryptography.RandomNumberGenerator WebSocketSharp.WebSocket::RandomNumber
	RandomNumberGenerator_t386037858 * ___RandomNumber_46;

public:
	inline static int32_t get_offset_of_EmptyBytes_44() { return static_cast<int32_t>(offsetof(WebSocket_t62038747_StaticFields, ___EmptyBytes_44)); }
	inline ByteU5BU5D_t4116647657* get_EmptyBytes_44() const { return ___EmptyBytes_44; }
	inline ByteU5BU5D_t4116647657** get_address_of_EmptyBytes_44() { return &___EmptyBytes_44; }
	inline void set_EmptyBytes_44(ByteU5BU5D_t4116647657* value)
	{
		___EmptyBytes_44 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyBytes_44), value);
	}

	inline static int32_t get_offset_of_FragmentLength_45() { return static_cast<int32_t>(offsetof(WebSocket_t62038747_StaticFields, ___FragmentLength_45)); }
	inline int32_t get_FragmentLength_45() const { return ___FragmentLength_45; }
	inline int32_t* get_address_of_FragmentLength_45() { return &___FragmentLength_45; }
	inline void set_FragmentLength_45(int32_t value)
	{
		___FragmentLength_45 = value;
	}

	inline static int32_t get_offset_of_RandomNumber_46() { return static_cast<int32_t>(offsetof(WebSocket_t62038747_StaticFields, ___RandomNumber_46)); }
	inline RandomNumberGenerator_t386037858 * get_RandomNumber_46() const { return ___RandomNumber_46; }
	inline RandomNumberGenerator_t386037858 ** get_address_of_RandomNumber_46() { return &___RandomNumber_46; }
	inline void set_RandomNumber_46(RandomNumberGenerator_t386037858 * value)
	{
		___RandomNumber_46 = value;
		Il2CppCodeGenWriteBarrier((&___RandomNumber_46), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKET_T62038747_H
#ifndef WEBSOCKETSERVICEMANAGER_T2425053_H
#define WEBSOCKETSERVICEMANAGER_T2425053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServiceManager
struct  WebSocketServiceManager_t2425053  : public RuntimeObject
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.WebSocketServiceManager::_clean
	bool ____clean_0;
	// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Server.WebSocketServiceHost> WebSocketSharp.Server.WebSocketServiceManager::_hosts
	Dictionary_2_t645715037 * ____hosts_1;
	// WebSocketSharp.Logger WebSocketSharp.Server.WebSocketServiceManager::_logger
	Logger_t4025333586 * ____logger_2;
	// WebSocketSharp.Server.ServerState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.WebSocketServiceManager::_state
	int32_t ____state_3;
	// System.Object WebSocketSharp.Server.WebSocketServiceManager::_sync
	RuntimeObject * ____sync_4;
	// System.TimeSpan WebSocketSharp.Server.WebSocketServiceManager::_waitTime
	TimeSpan_t881159249  ____waitTime_5;

public:
	inline static int32_t get_offset_of__clean_0() { return static_cast<int32_t>(offsetof(WebSocketServiceManager_t2425053, ____clean_0)); }
	inline bool get__clean_0() const { return ____clean_0; }
	inline bool* get_address_of__clean_0() { return &____clean_0; }
	inline void set__clean_0(bool value)
	{
		____clean_0 = value;
	}

	inline static int32_t get_offset_of__hosts_1() { return static_cast<int32_t>(offsetof(WebSocketServiceManager_t2425053, ____hosts_1)); }
	inline Dictionary_2_t645715037 * get__hosts_1() const { return ____hosts_1; }
	inline Dictionary_2_t645715037 ** get_address_of__hosts_1() { return &____hosts_1; }
	inline void set__hosts_1(Dictionary_2_t645715037 * value)
	{
		____hosts_1 = value;
		Il2CppCodeGenWriteBarrier((&____hosts_1), value);
	}

	inline static int32_t get_offset_of__logger_2() { return static_cast<int32_t>(offsetof(WebSocketServiceManager_t2425053, ____logger_2)); }
	inline Logger_t4025333586 * get__logger_2() const { return ____logger_2; }
	inline Logger_t4025333586 ** get_address_of__logger_2() { return &____logger_2; }
	inline void set__logger_2(Logger_t4025333586 * value)
	{
		____logger_2 = value;
		Il2CppCodeGenWriteBarrier((&____logger_2), value);
	}

	inline static int32_t get_offset_of__state_3() { return static_cast<int32_t>(offsetof(WebSocketServiceManager_t2425053, ____state_3)); }
	inline int32_t get__state_3() const { return ____state_3; }
	inline int32_t* get_address_of__state_3() { return &____state_3; }
	inline void set__state_3(int32_t value)
	{
		____state_3 = value;
	}

	inline static int32_t get_offset_of__sync_4() { return static_cast<int32_t>(offsetof(WebSocketServiceManager_t2425053, ____sync_4)); }
	inline RuntimeObject * get__sync_4() const { return ____sync_4; }
	inline RuntimeObject ** get_address_of__sync_4() { return &____sync_4; }
	inline void set__sync_4(RuntimeObject * value)
	{
		____sync_4 = value;
		Il2CppCodeGenWriteBarrier((&____sync_4), value);
	}

	inline static int32_t get_offset_of__waitTime_5() { return static_cast<int32_t>(offsetof(WebSocketServiceManager_t2425053, ____waitTime_5)); }
	inline TimeSpan_t881159249  get__waitTime_5() const { return ____waitTime_5; }
	inline TimeSpan_t881159249 * get_address_of__waitTime_5() { return &____waitTime_5; }
	inline void set__waitTime_5(TimeSpan_t881159249  value)
	{
		____waitTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSERVICEMANAGER_T2425053_H
#ifndef U3CBROADCASTASYNCU3EC__ANONSTOREY3_T1507099532_H
#define U3CBROADCASTASYNCU3EC__ANONSTOREY3_T1507099532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey3
struct  U3CbroadcastAsyncU3Ec__AnonStorey3_t1507099532  : public RuntimeObject
{
public:
	// WebSocketSharp.Opcode WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey3::opcode
	uint8_t ___opcode_0;
	// System.IO.Stream WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey3::stream
	Stream_t1273022909 * ___stream_1;
	// System.Action WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey3::completed
	Action_t1264377477 * ___completed_2;
	// WebSocketSharp.Server.WebSocketSessionManager WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey3::$this
	WebSocketSessionManager_t336645657 * ___U24this_3;

public:
	inline static int32_t get_offset_of_opcode_0() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey3_t1507099532, ___opcode_0)); }
	inline uint8_t get_opcode_0() const { return ___opcode_0; }
	inline uint8_t* get_address_of_opcode_0() { return &___opcode_0; }
	inline void set_opcode_0(uint8_t value)
	{
		___opcode_0 = value;
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey3_t1507099532, ___stream_1)); }
	inline Stream_t1273022909 * get_stream_1() const { return ___stream_1; }
	inline Stream_t1273022909 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t1273022909 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey3_t1507099532, ___completed_2)); }
	inline Action_t1264377477 * get_completed_2() const { return ___completed_2; }
	inline Action_t1264377477 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_t1264377477 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey3_t1507099532, ___U24this_3)); }
	inline WebSocketSessionManager_t336645657 * get_U24this_3() const { return ___U24this_3; }
	inline WebSocketSessionManager_t336645657 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(WebSocketSessionManager_t336645657 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBROADCASTASYNCU3EC__ANONSTOREY3_T1507099532_H
#ifndef U3CU3EC__ITERATOR1_T3058295315_H
#define U3CU3EC__ITERATOR1_T3058295315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1
struct  U3CU3Ec__Iterator1_t3058295315  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean> WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1::$locvar0
	Enumerator_t1836727039  ___U24locvar0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean> WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1::<res>__1
	KeyValuePair_2_t2280216431  ___U3CresU3E__1_1;
	// WebSocketSharp.Server.WebSocketSessionManager WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1::$this
	WebSocketSessionManager_t336645657 * ___U24this_2;
	// System.String WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1::$current
	String_t* ___U24current_3;
	// System.Boolean WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1::$disposing
	bool ___U24disposing_4;
	// System.Int32 WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator1::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3058295315, ___U24locvar0_0)); }
	inline Enumerator_t1836727039  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t1836727039 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t1836727039  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CresU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3058295315, ___U3CresU3E__1_1)); }
	inline KeyValuePair_2_t2280216431  get_U3CresU3E__1_1() const { return ___U3CresU3E__1_1; }
	inline KeyValuePair_2_t2280216431 * get_address_of_U3CresU3E__1_1() { return &___U3CresU3E__1_1; }
	inline void set_U3CresU3E__1_1(KeyValuePair_2_t2280216431  value)
	{
		___U3CresU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3058295315, ___U24this_2)); }
	inline WebSocketSessionManager_t336645657 * get_U24this_2() const { return ___U24this_2; }
	inline WebSocketSessionManager_t336645657 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebSocketSessionManager_t336645657 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3058295315, ___U24current_3)); }
	inline String_t* get_U24current_3() const { return ___U24current_3; }
	inline String_t** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(String_t* value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3058295315, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_t3058295315, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR1_T3058295315_H
#ifndef WEBSOCKETFRAME_T3926438742_H
#define WEBSOCKETFRAME_T3926438742_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.WebSocketFrame
struct  WebSocketFrame_t3926438742  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.WebSocketFrame::_extPayloadLength
	ByteU5BU5D_t4116647657* ____extPayloadLength_0;
	// WebSocketSharp.Fin WebSocketSharp.WebSocketFrame::_fin
	uint8_t ____fin_1;
	// WebSocketSharp.Mask WebSocketSharp.WebSocketFrame::_mask
	uint8_t ____mask_2;
	// System.Byte[] WebSocketSharp.WebSocketFrame::_maskingKey
	ByteU5BU5D_t4116647657* ____maskingKey_3;
	// WebSocketSharp.Opcode WebSocketSharp.WebSocketFrame::_opcode
	uint8_t ____opcode_4;
	// WebSocketSharp.PayloadData WebSocketSharp.WebSocketFrame::_payloadData
	PayloadData_t688932160 * ____payloadData_5;
	// System.Byte WebSocketSharp.WebSocketFrame::_payloadLength
	uint8_t ____payloadLength_6;
	// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::_rsv1
	uint8_t ____rsv1_7;
	// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::_rsv2
	uint8_t ____rsv2_8;
	// WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::_rsv3
	uint8_t ____rsv3_9;

public:
	inline static int32_t get_offset_of__extPayloadLength_0() { return static_cast<int32_t>(offsetof(WebSocketFrame_t3926438742, ____extPayloadLength_0)); }
	inline ByteU5BU5D_t4116647657* get__extPayloadLength_0() const { return ____extPayloadLength_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__extPayloadLength_0() { return &____extPayloadLength_0; }
	inline void set__extPayloadLength_0(ByteU5BU5D_t4116647657* value)
	{
		____extPayloadLength_0 = value;
		Il2CppCodeGenWriteBarrier((&____extPayloadLength_0), value);
	}

	inline static int32_t get_offset_of__fin_1() { return static_cast<int32_t>(offsetof(WebSocketFrame_t3926438742, ____fin_1)); }
	inline uint8_t get__fin_1() const { return ____fin_1; }
	inline uint8_t* get_address_of__fin_1() { return &____fin_1; }
	inline void set__fin_1(uint8_t value)
	{
		____fin_1 = value;
	}

	inline static int32_t get_offset_of__mask_2() { return static_cast<int32_t>(offsetof(WebSocketFrame_t3926438742, ____mask_2)); }
	inline uint8_t get__mask_2() const { return ____mask_2; }
	inline uint8_t* get_address_of__mask_2() { return &____mask_2; }
	inline void set__mask_2(uint8_t value)
	{
		____mask_2 = value;
	}

	inline static int32_t get_offset_of__maskingKey_3() { return static_cast<int32_t>(offsetof(WebSocketFrame_t3926438742, ____maskingKey_3)); }
	inline ByteU5BU5D_t4116647657* get__maskingKey_3() const { return ____maskingKey_3; }
	inline ByteU5BU5D_t4116647657** get_address_of__maskingKey_3() { return &____maskingKey_3; }
	inline void set__maskingKey_3(ByteU5BU5D_t4116647657* value)
	{
		____maskingKey_3 = value;
		Il2CppCodeGenWriteBarrier((&____maskingKey_3), value);
	}

	inline static int32_t get_offset_of__opcode_4() { return static_cast<int32_t>(offsetof(WebSocketFrame_t3926438742, ____opcode_4)); }
	inline uint8_t get__opcode_4() const { return ____opcode_4; }
	inline uint8_t* get_address_of__opcode_4() { return &____opcode_4; }
	inline void set__opcode_4(uint8_t value)
	{
		____opcode_4 = value;
	}

	inline static int32_t get_offset_of__payloadData_5() { return static_cast<int32_t>(offsetof(WebSocketFrame_t3926438742, ____payloadData_5)); }
	inline PayloadData_t688932160 * get__payloadData_5() const { return ____payloadData_5; }
	inline PayloadData_t688932160 ** get_address_of__payloadData_5() { return &____payloadData_5; }
	inline void set__payloadData_5(PayloadData_t688932160 * value)
	{
		____payloadData_5 = value;
		Il2CppCodeGenWriteBarrier((&____payloadData_5), value);
	}

	inline static int32_t get_offset_of__payloadLength_6() { return static_cast<int32_t>(offsetof(WebSocketFrame_t3926438742, ____payloadLength_6)); }
	inline uint8_t get__payloadLength_6() const { return ____payloadLength_6; }
	inline uint8_t* get_address_of__payloadLength_6() { return &____payloadLength_6; }
	inline void set__payloadLength_6(uint8_t value)
	{
		____payloadLength_6 = value;
	}

	inline static int32_t get_offset_of__rsv1_7() { return static_cast<int32_t>(offsetof(WebSocketFrame_t3926438742, ____rsv1_7)); }
	inline uint8_t get__rsv1_7() const { return ____rsv1_7; }
	inline uint8_t* get_address_of__rsv1_7() { return &____rsv1_7; }
	inline void set__rsv1_7(uint8_t value)
	{
		____rsv1_7 = value;
	}

	inline static int32_t get_offset_of__rsv2_8() { return static_cast<int32_t>(offsetof(WebSocketFrame_t3926438742, ____rsv2_8)); }
	inline uint8_t get__rsv2_8() const { return ____rsv2_8; }
	inline uint8_t* get_address_of__rsv2_8() { return &____rsv2_8; }
	inline void set__rsv2_8(uint8_t value)
	{
		____rsv2_8 = value;
	}

	inline static int32_t get_offset_of__rsv3_9() { return static_cast<int32_t>(offsetof(WebSocketFrame_t3926438742, ____rsv3_9)); }
	inline uint8_t get__rsv3_9() const { return ____rsv3_9; }
	inline uint8_t* get_address_of__rsv3_9() { return &____rsv3_9; }
	inline void set__rsv3_9(uint8_t value)
	{
		____rsv3_9 = value;
	}
};

struct WebSocketFrame_t3926438742_StaticFields
{
public:
	// System.Byte[] WebSocketSharp.WebSocketFrame::EmptyPingBytes
	ByteU5BU5D_t4116647657* ___EmptyPingBytes_10;

public:
	inline static int32_t get_offset_of_EmptyPingBytes_10() { return static_cast<int32_t>(offsetof(WebSocketFrame_t3926438742_StaticFields, ___EmptyPingBytes_10)); }
	inline ByteU5BU5D_t4116647657* get_EmptyPingBytes_10() const { return ___EmptyPingBytes_10; }
	inline ByteU5BU5D_t4116647657** get_address_of_EmptyPingBytes_10() { return &___EmptyPingBytes_10; }
	inline void set_EmptyPingBytes_10(ByteU5BU5D_t4116647657* value)
	{
		___EmptyPingBytes_10 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyPingBytes_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETFRAME_T3926438742_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef U3CBROADCASTASYNCU3EC__ANONSTOREY0_T3156767960_H
#define U3CBROADCASTASYNCU3EC__ANONSTOREY0_T3156767960_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey0
struct  U3CbroadcastAsyncU3Ec__AnonStorey0_t3156767960  : public RuntimeObject
{
public:
	// WebSocketSharp.Opcode WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey0::opcode
	uint8_t ___opcode_0;
	// System.Byte[] WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey0::data
	ByteU5BU5D_t4116647657* ___data_1;
	// System.Action WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey0::completed
	Action_t1264377477 * ___completed_2;
	// WebSocketSharp.Server.WebSocketServiceManager WebSocketSharp.Server.WebSocketServiceManager/<broadcastAsync>c__AnonStorey0::$this
	WebSocketServiceManager_t2425053 * ___U24this_3;

public:
	inline static int32_t get_offset_of_opcode_0() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey0_t3156767960, ___opcode_0)); }
	inline uint8_t get_opcode_0() const { return ___opcode_0; }
	inline uint8_t* get_address_of_opcode_0() { return &___opcode_0; }
	inline void set_opcode_0(uint8_t value)
	{
		___opcode_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey0_t3156767960, ___data_1)); }
	inline ByteU5BU5D_t4116647657* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_t4116647657* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey0_t3156767960, ___completed_2)); }
	inline Action_t1264377477 * get_completed_2() const { return ___completed_2; }
	inline Action_t1264377477 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_t1264377477 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey0_t3156767960, ___U24this_3)); }
	inline WebSocketServiceManager_t2425053 * get_U24this_3() const { return ___U24this_3; }
	inline WebSocketServiceManager_t2425053 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(WebSocketServiceManager_t2425053 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBROADCASTASYNCU3EC__ANONSTOREY0_T3156767960_H
#ifndef CHUNKSTREAM_T2280345721_H
#define CHUNKSTREAM_T2280345721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ChunkStream
struct  ChunkStream_t2280345721  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Net.ChunkStream::_chunkRead
	int32_t ____chunkRead_0;
	// System.Int32 WebSocketSharp.Net.ChunkStream::_chunkSize
	int32_t ____chunkSize_1;
	// System.Collections.Generic.List`1<WebSocketSharp.Net.Chunk> WebSocketSharp.Net.ChunkStream::_chunks
	List_1_t1758269811 * ____chunks_2;
	// System.Boolean WebSocketSharp.Net.ChunkStream::_gotIt
	bool ____gotIt_3;
	// WebSocketSharp.Net.WebHeaderCollection WebSocketSharp.Net.ChunkStream::_headers
	WebHeaderCollection_t1205255311 * ____headers_4;
	// System.Text.StringBuilder WebSocketSharp.Net.ChunkStream::_saved
	StringBuilder_t * ____saved_5;
	// System.Boolean WebSocketSharp.Net.ChunkStream::_sawCr
	bool ____sawCr_6;
	// WebSocketSharp.Net.InputChunkState WebSocketSharp.Net.ChunkStream::_state
	int32_t ____state_7;
	// System.Int32 WebSocketSharp.Net.ChunkStream::_trailerState
	int32_t ____trailerState_8;

public:
	inline static int32_t get_offset_of__chunkRead_0() { return static_cast<int32_t>(offsetof(ChunkStream_t2280345721, ____chunkRead_0)); }
	inline int32_t get__chunkRead_0() const { return ____chunkRead_0; }
	inline int32_t* get_address_of__chunkRead_0() { return &____chunkRead_0; }
	inline void set__chunkRead_0(int32_t value)
	{
		____chunkRead_0 = value;
	}

	inline static int32_t get_offset_of__chunkSize_1() { return static_cast<int32_t>(offsetof(ChunkStream_t2280345721, ____chunkSize_1)); }
	inline int32_t get__chunkSize_1() const { return ____chunkSize_1; }
	inline int32_t* get_address_of__chunkSize_1() { return &____chunkSize_1; }
	inline void set__chunkSize_1(int32_t value)
	{
		____chunkSize_1 = value;
	}

	inline static int32_t get_offset_of__chunks_2() { return static_cast<int32_t>(offsetof(ChunkStream_t2280345721, ____chunks_2)); }
	inline List_1_t1758269811 * get__chunks_2() const { return ____chunks_2; }
	inline List_1_t1758269811 ** get_address_of__chunks_2() { return &____chunks_2; }
	inline void set__chunks_2(List_1_t1758269811 * value)
	{
		____chunks_2 = value;
		Il2CppCodeGenWriteBarrier((&____chunks_2), value);
	}

	inline static int32_t get_offset_of__gotIt_3() { return static_cast<int32_t>(offsetof(ChunkStream_t2280345721, ____gotIt_3)); }
	inline bool get__gotIt_3() const { return ____gotIt_3; }
	inline bool* get_address_of__gotIt_3() { return &____gotIt_3; }
	inline void set__gotIt_3(bool value)
	{
		____gotIt_3 = value;
	}

	inline static int32_t get_offset_of__headers_4() { return static_cast<int32_t>(offsetof(ChunkStream_t2280345721, ____headers_4)); }
	inline WebHeaderCollection_t1205255311 * get__headers_4() const { return ____headers_4; }
	inline WebHeaderCollection_t1205255311 ** get_address_of__headers_4() { return &____headers_4; }
	inline void set__headers_4(WebHeaderCollection_t1205255311 * value)
	{
		____headers_4 = value;
		Il2CppCodeGenWriteBarrier((&____headers_4), value);
	}

	inline static int32_t get_offset_of__saved_5() { return static_cast<int32_t>(offsetof(ChunkStream_t2280345721, ____saved_5)); }
	inline StringBuilder_t * get__saved_5() const { return ____saved_5; }
	inline StringBuilder_t ** get_address_of__saved_5() { return &____saved_5; }
	inline void set__saved_5(StringBuilder_t * value)
	{
		____saved_5 = value;
		Il2CppCodeGenWriteBarrier((&____saved_5), value);
	}

	inline static int32_t get_offset_of__sawCr_6() { return static_cast<int32_t>(offsetof(ChunkStream_t2280345721, ____sawCr_6)); }
	inline bool get__sawCr_6() const { return ____sawCr_6; }
	inline bool* get_address_of__sawCr_6() { return &____sawCr_6; }
	inline void set__sawCr_6(bool value)
	{
		____sawCr_6 = value;
	}

	inline static int32_t get_offset_of__state_7() { return static_cast<int32_t>(offsetof(ChunkStream_t2280345721, ____state_7)); }
	inline int32_t get__state_7() const { return ____state_7; }
	inline int32_t* get_address_of__state_7() { return &____state_7; }
	inline void set__state_7(int32_t value)
	{
		____state_7 = value;
	}

	inline static int32_t get_offset_of__trailerState_8() { return static_cast<int32_t>(offsetof(ChunkStream_t2280345721, ____trailerState_8)); }
	inline int32_t get__trailerState_8() const { return ____trailerState_8; }
	inline int32_t* get_address_of__trailerState_8() { return &____trailerState_8; }
	inline void set__trailerState_8(int32_t value)
	{
		____trailerState_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNKSTREAM_T2280345721_H
#ifndef WEBSOCKETSESSIONMANAGER_T336645657_H
#define WEBSOCKETSESSIONMANAGER_T336645657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketSessionManager
struct  WebSocketSessionManager_t336645657  : public RuntimeObject
{
public:
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.WebSocketSessionManager::_clean
	bool ____clean_0;
	// System.Object WebSocketSharp.Server.WebSocketSessionManager::_forSweep
	RuntimeObject * ____forSweep_1;
	// WebSocketSharp.Logger WebSocketSharp.Server.WebSocketSessionManager::_logger
	Logger_t4025333586 * ____logger_2;
	// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Server.IWebSocketSession> WebSocketSharp.Server.WebSocketSessionManager::_sessions
	Dictionary_2_t1549584620 * ____sessions_3;
	// WebSocketSharp.Server.ServerState modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.WebSocketSessionManager::_state
	int32_t ____state_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Server.WebSocketSessionManager::_sweeping
	bool ____sweeping_5;
	// System.Timers.Timer WebSocketSharp.Server.WebSocketSessionManager::_sweepTimer
	Timer_t1767341190 * ____sweepTimer_6;
	// System.Object WebSocketSharp.Server.WebSocketSessionManager::_sync
	RuntimeObject * ____sync_7;
	// System.TimeSpan WebSocketSharp.Server.WebSocketSessionManager::_waitTime
	TimeSpan_t881159249  ____waitTime_8;

public:
	inline static int32_t get_offset_of__clean_0() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t336645657, ____clean_0)); }
	inline bool get__clean_0() const { return ____clean_0; }
	inline bool* get_address_of__clean_0() { return &____clean_0; }
	inline void set__clean_0(bool value)
	{
		____clean_0 = value;
	}

	inline static int32_t get_offset_of__forSweep_1() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t336645657, ____forSweep_1)); }
	inline RuntimeObject * get__forSweep_1() const { return ____forSweep_1; }
	inline RuntimeObject ** get_address_of__forSweep_1() { return &____forSweep_1; }
	inline void set__forSweep_1(RuntimeObject * value)
	{
		____forSweep_1 = value;
		Il2CppCodeGenWriteBarrier((&____forSweep_1), value);
	}

	inline static int32_t get_offset_of__logger_2() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t336645657, ____logger_2)); }
	inline Logger_t4025333586 * get__logger_2() const { return ____logger_2; }
	inline Logger_t4025333586 ** get_address_of__logger_2() { return &____logger_2; }
	inline void set__logger_2(Logger_t4025333586 * value)
	{
		____logger_2 = value;
		Il2CppCodeGenWriteBarrier((&____logger_2), value);
	}

	inline static int32_t get_offset_of__sessions_3() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t336645657, ____sessions_3)); }
	inline Dictionary_2_t1549584620 * get__sessions_3() const { return ____sessions_3; }
	inline Dictionary_2_t1549584620 ** get_address_of__sessions_3() { return &____sessions_3; }
	inline void set__sessions_3(Dictionary_2_t1549584620 * value)
	{
		____sessions_3 = value;
		Il2CppCodeGenWriteBarrier((&____sessions_3), value);
	}

	inline static int32_t get_offset_of__state_4() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t336645657, ____state_4)); }
	inline int32_t get__state_4() const { return ____state_4; }
	inline int32_t* get_address_of__state_4() { return &____state_4; }
	inline void set__state_4(int32_t value)
	{
		____state_4 = value;
	}

	inline static int32_t get_offset_of__sweeping_5() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t336645657, ____sweeping_5)); }
	inline bool get__sweeping_5() const { return ____sweeping_5; }
	inline bool* get_address_of__sweeping_5() { return &____sweeping_5; }
	inline void set__sweeping_5(bool value)
	{
		____sweeping_5 = value;
	}

	inline static int32_t get_offset_of__sweepTimer_6() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t336645657, ____sweepTimer_6)); }
	inline Timer_t1767341190 * get__sweepTimer_6() const { return ____sweepTimer_6; }
	inline Timer_t1767341190 ** get_address_of__sweepTimer_6() { return &____sweepTimer_6; }
	inline void set__sweepTimer_6(Timer_t1767341190 * value)
	{
		____sweepTimer_6 = value;
		Il2CppCodeGenWriteBarrier((&____sweepTimer_6), value);
	}

	inline static int32_t get_offset_of__sync_7() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t336645657, ____sync_7)); }
	inline RuntimeObject * get__sync_7() const { return ____sync_7; }
	inline RuntimeObject ** get_address_of__sync_7() { return &____sync_7; }
	inline void set__sync_7(RuntimeObject * value)
	{
		____sync_7 = value;
		Il2CppCodeGenWriteBarrier((&____sync_7), value);
	}

	inline static int32_t get_offset_of__waitTime_8() { return static_cast<int32_t>(offsetof(WebSocketSessionManager_t336645657, ____waitTime_8)); }
	inline TimeSpan_t881159249  get__waitTime_8() const { return ____waitTime_8; }
	inline TimeSpan_t881159249 * get_address_of__waitTime_8() { return &____waitTime_8; }
	inline void set__waitTime_8(TimeSpan_t881159249  value)
	{
		____waitTime_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETSESSIONMANAGER_T336645657_H
#ifndef U3CBROADCASTASYNCU3EC__ANONSTOREY2_T3463414668_H
#define U3CBROADCASTASYNCU3EC__ANONSTOREY2_T3463414668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey2
struct  U3CbroadcastAsyncU3Ec__AnonStorey2_t3463414668  : public RuntimeObject
{
public:
	// WebSocketSharp.Opcode WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey2::opcode
	uint8_t ___opcode_0;
	// System.Byte[] WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey2::data
	ByteU5BU5D_t4116647657* ___data_1;
	// System.Action WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey2::completed
	Action_t1264377477 * ___completed_2;
	// WebSocketSharp.Server.WebSocketSessionManager WebSocketSharp.Server.WebSocketSessionManager/<broadcastAsync>c__AnonStorey2::$this
	WebSocketSessionManager_t336645657 * ___U24this_3;

public:
	inline static int32_t get_offset_of_opcode_0() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey2_t3463414668, ___opcode_0)); }
	inline uint8_t get_opcode_0() const { return ___opcode_0; }
	inline uint8_t* get_address_of_opcode_0() { return &___opcode_0; }
	inline void set_opcode_0(uint8_t value)
	{
		___opcode_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey2_t3463414668, ___data_1)); }
	inline ByteU5BU5D_t4116647657* get_data_1() const { return ___data_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(ByteU5BU5D_t4116647657* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_completed_2() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey2_t3463414668, ___completed_2)); }
	inline Action_t1264377477 * get_completed_2() const { return ___completed_2; }
	inline Action_t1264377477 ** get_address_of_completed_2() { return &___completed_2; }
	inline void set_completed_2(Action_t1264377477 * value)
	{
		___completed_2 = value;
		Il2CppCodeGenWriteBarrier((&___completed_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CbroadcastAsyncU3Ec__AnonStorey2_t3463414668, ___U24this_3)); }
	inline WebSocketSessionManager_t336645657 * get_U24this_3() const { return ___U24this_3; }
	inline WebSocketSessionManager_t336645657 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(WebSocketSessionManager_t336645657 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CBROADCASTASYNCU3EC__ANONSTOREY2_T3463414668_H
#ifndef U3CU3EC__ITERATOR0_T719643155_H
#define U3CU3EC__ITERATOR0_T719643155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0
struct  U3CU3Ec__Iterator0_t719643155  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean> WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0::$locvar0
	Enumerator_t1836727039  ___U24locvar0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean> WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0::<res>__1
	KeyValuePair_2_t2280216431  ___U3CresU3E__1_1;
	// WebSocketSharp.Server.WebSocketSessionManager WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0::$this
	WebSocketSessionManager_t336645657 * ___U24this_2;
	// System.String WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0::$current
	String_t* ___U24current_3;
	// System.Boolean WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 WebSocketSharp.Server.WebSocketSessionManager/<>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t719643155, ___U24locvar0_0)); }
	inline Enumerator_t1836727039  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t1836727039 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t1836727039  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CresU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t719643155, ___U3CresU3E__1_1)); }
	inline KeyValuePair_2_t2280216431  get_U3CresU3E__1_1() const { return ___U3CresU3E__1_1; }
	inline KeyValuePair_2_t2280216431 * get_address_of_U3CresU3E__1_1() { return &___U3CresU3E__1_1; }
	inline void set_U3CresU3E__1_1(KeyValuePair_2_t2280216431  value)
	{
		___U3CresU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t719643155, ___U24this_2)); }
	inline WebSocketSessionManager_t336645657 * get_U24this_2() const { return ___U24this_2; }
	inline WebSocketSessionManager_t336645657 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(WebSocketSessionManager_t336645657 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t719643155, ___U24current_3)); }
	inline String_t* get_U24current_3() const { return ___U24current_3; }
	inline String_t** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(String_t* value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t719643155, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t719643155, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T719643155_H
#ifndef SSLCONFIGURATION_T3726145939_H
#define SSLCONFIGURATION_T3726145939_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.SslConfiguration
struct  SslConfiguration_t3726145939  : public RuntimeObject
{
public:
	// System.Net.Security.LocalCertificateSelectionCallback WebSocketSharp.Net.SslConfiguration::_certSelectionCallback
	LocalCertificateSelectionCallback_t2354453884 * ____certSelectionCallback_0;
	// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.Net.SslConfiguration::_certValidationCallback
	RemoteCertificateValidationCallback_t3014364904 * ____certValidationCallback_1;
	// System.Boolean WebSocketSharp.Net.SslConfiguration::_checkCertRevocation
	bool ____checkCertRevocation_2;
	// System.Security.Authentication.SslProtocols WebSocketSharp.Net.SslConfiguration::_enabledProtocols
	int32_t ____enabledProtocols_3;

public:
	inline static int32_t get_offset_of__certSelectionCallback_0() { return static_cast<int32_t>(offsetof(SslConfiguration_t3726145939, ____certSelectionCallback_0)); }
	inline LocalCertificateSelectionCallback_t2354453884 * get__certSelectionCallback_0() const { return ____certSelectionCallback_0; }
	inline LocalCertificateSelectionCallback_t2354453884 ** get_address_of__certSelectionCallback_0() { return &____certSelectionCallback_0; }
	inline void set__certSelectionCallback_0(LocalCertificateSelectionCallback_t2354453884 * value)
	{
		____certSelectionCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&____certSelectionCallback_0), value);
	}

	inline static int32_t get_offset_of__certValidationCallback_1() { return static_cast<int32_t>(offsetof(SslConfiguration_t3726145939, ____certValidationCallback_1)); }
	inline RemoteCertificateValidationCallback_t3014364904 * get__certValidationCallback_1() const { return ____certValidationCallback_1; }
	inline RemoteCertificateValidationCallback_t3014364904 ** get_address_of__certValidationCallback_1() { return &____certValidationCallback_1; }
	inline void set__certValidationCallback_1(RemoteCertificateValidationCallback_t3014364904 * value)
	{
		____certValidationCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&____certValidationCallback_1), value);
	}

	inline static int32_t get_offset_of__checkCertRevocation_2() { return static_cast<int32_t>(offsetof(SslConfiguration_t3726145939, ____checkCertRevocation_2)); }
	inline bool get__checkCertRevocation_2() const { return ____checkCertRevocation_2; }
	inline bool* get_address_of__checkCertRevocation_2() { return &____checkCertRevocation_2; }
	inline void set__checkCertRevocation_2(bool value)
	{
		____checkCertRevocation_2 = value;
	}

	inline static int32_t get_offset_of__enabledProtocols_3() { return static_cast<int32_t>(offsetof(SslConfiguration_t3726145939, ____enabledProtocols_3)); }
	inline int32_t get__enabledProtocols_3() const { return ____enabledProtocols_3; }
	inline int32_t* get_address_of__enabledProtocols_3() { return &____enabledProtocols_3; }
	inline void set__enabledProtocols_3(int32_t value)
	{
		____enabledProtocols_3 = value;
	}
};

struct SslConfiguration_t3726145939_StaticFields
{
public:
	// System.Net.Security.LocalCertificateSelectionCallback WebSocketSharp.Net.SslConfiguration::<>f__am$cache0
	LocalCertificateSelectionCallback_t2354453884 * ___U3CU3Ef__amU24cache0_4;
	// System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.Net.SslConfiguration::<>f__am$cache1
	RemoteCertificateValidationCallback_t3014364904 * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(SslConfiguration_t3726145939_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline LocalCertificateSelectionCallback_t2354453884 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline LocalCertificateSelectionCallback_t2354453884 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(LocalCertificateSelectionCallback_t2354453884 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(SslConfiguration_t3726145939_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline RemoteCertificateValidationCallback_t3014364904 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline RemoteCertificateValidationCallback_t3014364904 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(RemoteCertificateValidationCallback_t3014364904 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLCONFIGURATION_T3726145939_H
#ifndef WEBHEADERCOLLECTION_T1205255311_H
#define WEBHEADERCOLLECTION_T1205255311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.WebHeaderCollection
struct  WebHeaderCollection_t1205255311  : public NameValueCollection_t407452768
{
public:
	// System.Boolean WebSocketSharp.Net.WebHeaderCollection::_internallyUsed
	bool ____internallyUsed_13;
	// WebSocketSharp.Net.HttpHeaderType WebSocketSharp.Net.WebHeaderCollection::_state
	int32_t ____state_14;

public:
	inline static int32_t get_offset_of__internallyUsed_13() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1205255311, ____internallyUsed_13)); }
	inline bool get__internallyUsed_13() const { return ____internallyUsed_13; }
	inline bool* get_address_of__internallyUsed_13() { return &____internallyUsed_13; }
	inline void set__internallyUsed_13(bool value)
	{
		____internallyUsed_13 = value;
	}

	inline static int32_t get_offset_of__state_14() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1205255311, ____state_14)); }
	inline int32_t get__state_14() const { return ____state_14; }
	inline int32_t* get_address_of__state_14() { return &____state_14; }
	inline void set__state_14(int32_t value)
	{
		____state_14 = value;
	}
};

struct WebHeaderCollection_t1205255311_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,WebSocketSharp.Net.HttpHeaderInfo> WebSocketSharp.Net.WebHeaderCollection::_headers
	Dictionary_2_t1847514588 * ____headers_12;

public:
	inline static int32_t get_offset_of__headers_12() { return static_cast<int32_t>(offsetof(WebHeaderCollection_t1205255311_StaticFields, ____headers_12)); }
	inline Dictionary_2_t1847514588 * get__headers_12() const { return ____headers_12; }
	inline Dictionary_2_t1847514588 ** get_address_of__headers_12() { return &____headers_12; }
	inline void set__headers_12(Dictionary_2_t1847514588 * value)
	{
		____headers_12 = value;
		Il2CppCodeGenWriteBarrier((&____headers_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBHEADERCOLLECTION_T1205255311_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef CLIENTSSLCONFIGURATION_T664129470_H
#define CLIENTSSLCONFIGURATION_T664129470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ClientSslConfiguration
struct  ClientSslConfiguration_t664129470  : public SslConfiguration_t3726145939
{
public:
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection WebSocketSharp.Net.ClientSslConfiguration::_certs
	X509CertificateCollection_t3399372417 * ____certs_6;
	// System.String WebSocketSharp.Net.ClientSslConfiguration::_host
	String_t* ____host_7;

public:
	inline static int32_t get_offset_of__certs_6() { return static_cast<int32_t>(offsetof(ClientSslConfiguration_t664129470, ____certs_6)); }
	inline X509CertificateCollection_t3399372417 * get__certs_6() const { return ____certs_6; }
	inline X509CertificateCollection_t3399372417 ** get_address_of__certs_6() { return &____certs_6; }
	inline void set__certs_6(X509CertificateCollection_t3399372417 * value)
	{
		____certs_6 = value;
		Il2CppCodeGenWriteBarrier((&____certs_6), value);
	}

	inline static int32_t get_offset_of__host_7() { return static_cast<int32_t>(offsetof(ClientSslConfiguration_t664129470, ____host_7)); }
	inline String_t* get__host_7() const { return ____host_7; }
	inline String_t** get_address_of__host_7() { return &____host_7; }
	inline void set__host_7(String_t* value)
	{
		____host_7 = value;
		Il2CppCodeGenWriteBarrier((&____host_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTSSLCONFIGURATION_T664129470_H
#ifndef COOKIE_T4203102285_H
#define COOKIE_T4203102285_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.Cookie
struct  Cookie_t4203102285  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Net.Cookie::_comment
	String_t* ____comment_0;
	// System.Uri WebSocketSharp.Net.Cookie::_commentUri
	Uri_t100236324 * ____commentUri_1;
	// System.Boolean WebSocketSharp.Net.Cookie::_discard
	bool ____discard_2;
	// System.String WebSocketSharp.Net.Cookie::_domain
	String_t* ____domain_3;
	// System.DateTime WebSocketSharp.Net.Cookie::_expires
	DateTime_t3738529785  ____expires_4;
	// System.Boolean WebSocketSharp.Net.Cookie::_httpOnly
	bool ____httpOnly_5;
	// System.String WebSocketSharp.Net.Cookie::_name
	String_t* ____name_6;
	// System.String WebSocketSharp.Net.Cookie::_path
	String_t* ____path_7;
	// System.String WebSocketSharp.Net.Cookie::_port
	String_t* ____port_8;
	// System.Int32[] WebSocketSharp.Net.Cookie::_ports
	Int32U5BU5D_t385246372* ____ports_9;
	// System.Boolean WebSocketSharp.Net.Cookie::_secure
	bool ____secure_12;
	// System.DateTime WebSocketSharp.Net.Cookie::_timestamp
	DateTime_t3738529785  ____timestamp_13;
	// System.String WebSocketSharp.Net.Cookie::_value
	String_t* ____value_14;
	// System.Int32 WebSocketSharp.Net.Cookie::_version
	int32_t ____version_15;
	// System.Boolean WebSocketSharp.Net.Cookie::<ExactDomain>k__BackingField
	bool ___U3CExactDomainU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of__comment_0() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____comment_0)); }
	inline String_t* get__comment_0() const { return ____comment_0; }
	inline String_t** get_address_of__comment_0() { return &____comment_0; }
	inline void set__comment_0(String_t* value)
	{
		____comment_0 = value;
		Il2CppCodeGenWriteBarrier((&____comment_0), value);
	}

	inline static int32_t get_offset_of__commentUri_1() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____commentUri_1)); }
	inline Uri_t100236324 * get__commentUri_1() const { return ____commentUri_1; }
	inline Uri_t100236324 ** get_address_of__commentUri_1() { return &____commentUri_1; }
	inline void set__commentUri_1(Uri_t100236324 * value)
	{
		____commentUri_1 = value;
		Il2CppCodeGenWriteBarrier((&____commentUri_1), value);
	}

	inline static int32_t get_offset_of__discard_2() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____discard_2)); }
	inline bool get__discard_2() const { return ____discard_2; }
	inline bool* get_address_of__discard_2() { return &____discard_2; }
	inline void set__discard_2(bool value)
	{
		____discard_2 = value;
	}

	inline static int32_t get_offset_of__domain_3() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____domain_3)); }
	inline String_t* get__domain_3() const { return ____domain_3; }
	inline String_t** get_address_of__domain_3() { return &____domain_3; }
	inline void set__domain_3(String_t* value)
	{
		____domain_3 = value;
		Il2CppCodeGenWriteBarrier((&____domain_3), value);
	}

	inline static int32_t get_offset_of__expires_4() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____expires_4)); }
	inline DateTime_t3738529785  get__expires_4() const { return ____expires_4; }
	inline DateTime_t3738529785 * get_address_of__expires_4() { return &____expires_4; }
	inline void set__expires_4(DateTime_t3738529785  value)
	{
		____expires_4 = value;
	}

	inline static int32_t get_offset_of__httpOnly_5() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____httpOnly_5)); }
	inline bool get__httpOnly_5() const { return ____httpOnly_5; }
	inline bool* get_address_of__httpOnly_5() { return &____httpOnly_5; }
	inline void set__httpOnly_5(bool value)
	{
		____httpOnly_5 = value;
	}

	inline static int32_t get_offset_of__name_6() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____name_6)); }
	inline String_t* get__name_6() const { return ____name_6; }
	inline String_t** get_address_of__name_6() { return &____name_6; }
	inline void set__name_6(String_t* value)
	{
		____name_6 = value;
		Il2CppCodeGenWriteBarrier((&____name_6), value);
	}

	inline static int32_t get_offset_of__path_7() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____path_7)); }
	inline String_t* get__path_7() const { return ____path_7; }
	inline String_t** get_address_of__path_7() { return &____path_7; }
	inline void set__path_7(String_t* value)
	{
		____path_7 = value;
		Il2CppCodeGenWriteBarrier((&____path_7), value);
	}

	inline static int32_t get_offset_of__port_8() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____port_8)); }
	inline String_t* get__port_8() const { return ____port_8; }
	inline String_t** get_address_of__port_8() { return &____port_8; }
	inline void set__port_8(String_t* value)
	{
		____port_8 = value;
		Il2CppCodeGenWriteBarrier((&____port_8), value);
	}

	inline static int32_t get_offset_of__ports_9() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____ports_9)); }
	inline Int32U5BU5D_t385246372* get__ports_9() const { return ____ports_9; }
	inline Int32U5BU5D_t385246372** get_address_of__ports_9() { return &____ports_9; }
	inline void set__ports_9(Int32U5BU5D_t385246372* value)
	{
		____ports_9 = value;
		Il2CppCodeGenWriteBarrier((&____ports_9), value);
	}

	inline static int32_t get_offset_of__secure_12() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____secure_12)); }
	inline bool get__secure_12() const { return ____secure_12; }
	inline bool* get_address_of__secure_12() { return &____secure_12; }
	inline void set__secure_12(bool value)
	{
		____secure_12 = value;
	}

	inline static int32_t get_offset_of__timestamp_13() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____timestamp_13)); }
	inline DateTime_t3738529785  get__timestamp_13() const { return ____timestamp_13; }
	inline DateTime_t3738529785 * get_address_of__timestamp_13() { return &____timestamp_13; }
	inline void set__timestamp_13(DateTime_t3738529785  value)
	{
		____timestamp_13 = value;
	}

	inline static int32_t get_offset_of__value_14() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____value_14)); }
	inline String_t* get__value_14() const { return ____value_14; }
	inline String_t** get_address_of__value_14() { return &____value_14; }
	inline void set__value_14(String_t* value)
	{
		____value_14 = value;
		Il2CppCodeGenWriteBarrier((&____value_14), value);
	}

	inline static int32_t get_offset_of__version_15() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ____version_15)); }
	inline int32_t get__version_15() const { return ____version_15; }
	inline int32_t* get_address_of__version_15() { return &____version_15; }
	inline void set__version_15(int32_t value)
	{
		____version_15 = value;
	}

	inline static int32_t get_offset_of_U3CExactDomainU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Cookie_t4203102285, ___U3CExactDomainU3Ek__BackingField_16)); }
	inline bool get_U3CExactDomainU3Ek__BackingField_16() const { return ___U3CExactDomainU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CExactDomainU3Ek__BackingField_16() { return &___U3CExactDomainU3Ek__BackingField_16; }
	inline void set_U3CExactDomainU3Ek__BackingField_16(bool value)
	{
		___U3CExactDomainU3Ek__BackingField_16 = value;
	}
};

struct Cookie_t4203102285_StaticFields
{
public:
	// System.Char[] WebSocketSharp.Net.Cookie::_reservedCharsForName
	CharU5BU5D_t3528271667* ____reservedCharsForName_10;
	// System.Char[] WebSocketSharp.Net.Cookie::_reservedCharsForValue
	CharU5BU5D_t3528271667* ____reservedCharsForValue_11;

public:
	inline static int32_t get_offset_of__reservedCharsForName_10() { return static_cast<int32_t>(offsetof(Cookie_t4203102285_StaticFields, ____reservedCharsForName_10)); }
	inline CharU5BU5D_t3528271667* get__reservedCharsForName_10() const { return ____reservedCharsForName_10; }
	inline CharU5BU5D_t3528271667** get_address_of__reservedCharsForName_10() { return &____reservedCharsForName_10; }
	inline void set__reservedCharsForName_10(CharU5BU5D_t3528271667* value)
	{
		____reservedCharsForName_10 = value;
		Il2CppCodeGenWriteBarrier((&____reservedCharsForName_10), value);
	}

	inline static int32_t get_offset_of__reservedCharsForValue_11() { return static_cast<int32_t>(offsetof(Cookie_t4203102285_StaticFields, ____reservedCharsForValue_11)); }
	inline CharU5BU5D_t3528271667* get__reservedCharsForValue_11() const { return ____reservedCharsForValue_11; }
	inline CharU5BU5D_t3528271667** get_address_of__reservedCharsForValue_11() { return &____reservedCharsForValue_11; }
	inline void set__reservedCharsForValue_11(CharU5BU5D_t3528271667* value)
	{
		____reservedCharsForValue_11 = value;
		Il2CppCodeGenWriteBarrier((&____reservedCharsForValue_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COOKIE_T4203102285_H
#ifndef HTTPLISTENEREXCEPTION_T3074104922_H
#define HTTPLISTENEREXCEPTION_T3074104922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.HttpListenerException
struct  HttpListenerException_t3074104922  : public Win32Exception_t3234146298
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPLISTENEREXCEPTION_T3074104922_H
#ifndef SERVERSSLCONFIGURATION_T3240511754_H
#define SERVERSSLCONFIGURATION_T3240511754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ServerSslConfiguration
struct  ServerSslConfiguration_t3240511754  : public SslConfiguration_t3726145939
{
public:
	// System.Security.Cryptography.X509Certificates.X509Certificate2 WebSocketSharp.Net.ServerSslConfiguration::_cert
	X509Certificate2_t714049126 * ____cert_6;
	// System.Boolean WebSocketSharp.Net.ServerSslConfiguration::_clientCertRequired
	bool ____clientCertRequired_7;

public:
	inline static int32_t get_offset_of__cert_6() { return static_cast<int32_t>(offsetof(ServerSslConfiguration_t3240511754, ____cert_6)); }
	inline X509Certificate2_t714049126 * get__cert_6() const { return ____cert_6; }
	inline X509Certificate2_t714049126 ** get_address_of__cert_6() { return &____cert_6; }
	inline void set__cert_6(X509Certificate2_t714049126 * value)
	{
		____cert_6 = value;
		Il2CppCodeGenWriteBarrier((&____cert_6), value);
	}

	inline static int32_t get_offset_of__clientCertRequired_7() { return static_cast<int32_t>(offsetof(ServerSslConfiguration_t3240511754, ____clientCertRequired_7)); }
	inline bool get__clientCertRequired_7() const { return ____clientCertRequired_7; }
	inline bool* get_address_of__clientCertRequired_7() { return &____clientCertRequired_7; }
	inline void set__clientCertRequired_7(bool value)
	{
		____clientCertRequired_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERVERSSLCONFIGURATION_T3240511754_H
#ifndef WEBSOCKETBEHAVIOR_T229673147_H
#define WEBSOCKETBEHAVIOR_T229673147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Server.WebSocketBehavior
struct  WebSocketBehavior_t229673147  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.WebSockets.WebSocketContext WebSocketSharp.Server.WebSocketBehavior::_context
	WebSocketContext_t619421455 * ____context_0;
	// System.Func`3<WebSocketSharp.Net.CookieCollection,WebSocketSharp.Net.CookieCollection,System.Boolean> WebSocketSharp.Server.WebSocketBehavior::_cookiesValidator
	Func_3_t567174798 * ____cookiesValidator_1;
	// System.Boolean WebSocketSharp.Server.WebSocketBehavior::_emitOnPing
	bool ____emitOnPing_2;
	// System.String WebSocketSharp.Server.WebSocketBehavior::_id
	String_t* ____id_3;
	// System.Boolean WebSocketSharp.Server.WebSocketBehavior::_ignoreExtensions
	bool ____ignoreExtensions_4;
	// System.Func`2<System.String,System.Boolean> WebSocketSharp.Server.WebSocketBehavior::_originValidator
	Func_2_t2197129486 * ____originValidator_5;
	// System.String WebSocketSharp.Server.WebSocketBehavior::_protocol
	String_t* ____protocol_6;
	// WebSocketSharp.Server.WebSocketSessionManager WebSocketSharp.Server.WebSocketBehavior::_sessions
	WebSocketSessionManager_t336645657 * ____sessions_7;
	// System.DateTime WebSocketSharp.Server.WebSocketBehavior::_startTime
	DateTime_t3738529785  ____startTime_8;
	// WebSocketSharp.WebSocket WebSocketSharp.Server.WebSocketBehavior::_websocket
	WebSocket_t62038747 * ____websocket_9;

public:
	inline static int32_t get_offset_of__context_0() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t229673147, ____context_0)); }
	inline WebSocketContext_t619421455 * get__context_0() const { return ____context_0; }
	inline WebSocketContext_t619421455 ** get_address_of__context_0() { return &____context_0; }
	inline void set__context_0(WebSocketContext_t619421455 * value)
	{
		____context_0 = value;
		Il2CppCodeGenWriteBarrier((&____context_0), value);
	}

	inline static int32_t get_offset_of__cookiesValidator_1() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t229673147, ____cookiesValidator_1)); }
	inline Func_3_t567174798 * get__cookiesValidator_1() const { return ____cookiesValidator_1; }
	inline Func_3_t567174798 ** get_address_of__cookiesValidator_1() { return &____cookiesValidator_1; }
	inline void set__cookiesValidator_1(Func_3_t567174798 * value)
	{
		____cookiesValidator_1 = value;
		Il2CppCodeGenWriteBarrier((&____cookiesValidator_1), value);
	}

	inline static int32_t get_offset_of__emitOnPing_2() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t229673147, ____emitOnPing_2)); }
	inline bool get__emitOnPing_2() const { return ____emitOnPing_2; }
	inline bool* get_address_of__emitOnPing_2() { return &____emitOnPing_2; }
	inline void set__emitOnPing_2(bool value)
	{
		____emitOnPing_2 = value;
	}

	inline static int32_t get_offset_of__id_3() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t229673147, ____id_3)); }
	inline String_t* get__id_3() const { return ____id_3; }
	inline String_t** get_address_of__id_3() { return &____id_3; }
	inline void set__id_3(String_t* value)
	{
		____id_3 = value;
		Il2CppCodeGenWriteBarrier((&____id_3), value);
	}

	inline static int32_t get_offset_of__ignoreExtensions_4() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t229673147, ____ignoreExtensions_4)); }
	inline bool get__ignoreExtensions_4() const { return ____ignoreExtensions_4; }
	inline bool* get_address_of__ignoreExtensions_4() { return &____ignoreExtensions_4; }
	inline void set__ignoreExtensions_4(bool value)
	{
		____ignoreExtensions_4 = value;
	}

	inline static int32_t get_offset_of__originValidator_5() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t229673147, ____originValidator_5)); }
	inline Func_2_t2197129486 * get__originValidator_5() const { return ____originValidator_5; }
	inline Func_2_t2197129486 ** get_address_of__originValidator_5() { return &____originValidator_5; }
	inline void set__originValidator_5(Func_2_t2197129486 * value)
	{
		____originValidator_5 = value;
		Il2CppCodeGenWriteBarrier((&____originValidator_5), value);
	}

	inline static int32_t get_offset_of__protocol_6() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t229673147, ____protocol_6)); }
	inline String_t* get__protocol_6() const { return ____protocol_6; }
	inline String_t** get_address_of__protocol_6() { return &____protocol_6; }
	inline void set__protocol_6(String_t* value)
	{
		____protocol_6 = value;
		Il2CppCodeGenWriteBarrier((&____protocol_6), value);
	}

	inline static int32_t get_offset_of__sessions_7() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t229673147, ____sessions_7)); }
	inline WebSocketSessionManager_t336645657 * get__sessions_7() const { return ____sessions_7; }
	inline WebSocketSessionManager_t336645657 ** get_address_of__sessions_7() { return &____sessions_7; }
	inline void set__sessions_7(WebSocketSessionManager_t336645657 * value)
	{
		____sessions_7 = value;
		Il2CppCodeGenWriteBarrier((&____sessions_7), value);
	}

	inline static int32_t get_offset_of__startTime_8() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t229673147, ____startTime_8)); }
	inline DateTime_t3738529785  get__startTime_8() const { return ____startTime_8; }
	inline DateTime_t3738529785 * get_address_of__startTime_8() { return &____startTime_8; }
	inline void set__startTime_8(DateTime_t3738529785  value)
	{
		____startTime_8 = value;
	}

	inline static int32_t get_offset_of__websocket_9() { return static_cast<int32_t>(offsetof(WebSocketBehavior_t229673147, ____websocket_9)); }
	inline WebSocket_t62038747 * get__websocket_9() const { return ____websocket_9; }
	inline WebSocket_t62038747 ** get_address_of__websocket_9() { return &____websocket_9; }
	inline void set__websocket_9(WebSocket_t62038747 * value)
	{
		____websocket_9 = value;
		Il2CppCodeGenWriteBarrier((&____websocket_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETBEHAVIOR_T229673147_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef OBSERVABLETRIGGERBASE_T3020654715_H
#define OBSERVABLETRIGGERBASE_T3020654715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableTriggerBase
struct  ObservableTriggerBase_t3020654715  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UniRx.Triggers.ObservableTriggerBase::calledAwake
	bool ___calledAwake_2;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerBase::awake
	Subject_1_t3450905854 * ___awake_3;
	// System.Boolean UniRx.Triggers.ObservableTriggerBase::calledStart
	bool ___calledStart_4;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerBase::start
	Subject_1_t3450905854 * ___start_5;
	// System.Boolean UniRx.Triggers.ObservableTriggerBase::calledDestroy
	bool ___calledDestroy_6;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerBase::onDestroy
	Subject_1_t3450905854 * ___onDestroy_7;

public:
	inline static int32_t get_offset_of_calledAwake_2() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t3020654715, ___calledAwake_2)); }
	inline bool get_calledAwake_2() const { return ___calledAwake_2; }
	inline bool* get_address_of_calledAwake_2() { return &___calledAwake_2; }
	inline void set_calledAwake_2(bool value)
	{
		___calledAwake_2 = value;
	}

	inline static int32_t get_offset_of_awake_3() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t3020654715, ___awake_3)); }
	inline Subject_1_t3450905854 * get_awake_3() const { return ___awake_3; }
	inline Subject_1_t3450905854 ** get_address_of_awake_3() { return &___awake_3; }
	inline void set_awake_3(Subject_1_t3450905854 * value)
	{
		___awake_3 = value;
		Il2CppCodeGenWriteBarrier((&___awake_3), value);
	}

	inline static int32_t get_offset_of_calledStart_4() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t3020654715, ___calledStart_4)); }
	inline bool get_calledStart_4() const { return ___calledStart_4; }
	inline bool* get_address_of_calledStart_4() { return &___calledStart_4; }
	inline void set_calledStart_4(bool value)
	{
		___calledStart_4 = value;
	}

	inline static int32_t get_offset_of_start_5() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t3020654715, ___start_5)); }
	inline Subject_1_t3450905854 * get_start_5() const { return ___start_5; }
	inline Subject_1_t3450905854 ** get_address_of_start_5() { return &___start_5; }
	inline void set_start_5(Subject_1_t3450905854 * value)
	{
		___start_5 = value;
		Il2CppCodeGenWriteBarrier((&___start_5), value);
	}

	inline static int32_t get_offset_of_calledDestroy_6() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t3020654715, ___calledDestroy_6)); }
	inline bool get_calledDestroy_6() const { return ___calledDestroy_6; }
	inline bool* get_address_of_calledDestroy_6() { return &___calledDestroy_6; }
	inline void set_calledDestroy_6(bool value)
	{
		___calledDestroy_6 = value;
	}

	inline static int32_t get_offset_of_onDestroy_7() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t3020654715, ___onDestroy_7)); }
	inline Subject_1_t3450905854 * get_onDestroy_7() const { return ___onDestroy_7; }
	inline Subject_1_t3450905854 ** get_address_of_onDestroy_7() { return &___onDestroy_7; }
	inline void set_onDestroy_7(Subject_1_t3450905854 * value)
	{
		___onDestroy_7 = value;
		Il2CppCodeGenWriteBarrier((&___onDestroy_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLETRIGGERBASE_T3020654715_H
#ifndef SAMPLE01_OBSERVABLEWWW_T2301706502_H
#define SAMPLE01_OBSERVABLEWWW_T2301706502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample01_ObservableWWW
struct  Sample01_ObservableWWW_t2301706502  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Sample01_ObservableWWW_t2301706502_StaticFields
{
public:
	// System.Action`1<System.String> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache0
	Action_1_t2019918284 * ___U3CU3Ef__amU24cache0_2;
	// System.Action`1<System.Exception> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache1
	Action_1_t1609204844 * ___U3CU3Ef__amU24cache1_3;
	// System.Func`2<System.String,UniRx.IObservable`1<System.String>> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache2
	Func_2_t2070424348 * ___U3CU3Ef__amU24cache2_4;
	// System.Func`3<System.String,System.String,<>__AnonType0`2<System.String,System.String>> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache3
	Func_3_t3989337584 * ___U3CU3Ef__amU24cache3_5;
	// System.Action`1<<>__AnonType0`2<System.String,System.String>> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache4
	Action_1_t3625273254 * ___U3CU3Ef__amU24cache4_6;
	// System.Action`1<System.String[]> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache5
	Action_1_t1454256935 * ___U3CU3Ef__amU24cache5_7;
	// System.Action`1<System.Single> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache6
	Action_1_t1569734369 * ___U3CU3Ef__amU24cache6_8;
	// System.Action`1<UniRx.WWWErrorException> UniRx.Examples.Sample01_ObservableWWW::<>f__am$cache7
	Action_1_t1943680094 * ___U3CU3Ef__amU24cache7_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t2301706502_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_1_t2019918284 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_1_t2019918284 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t2301706502_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_1_t1609204844 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_1_t1609204844 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_1_t1609204844 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t2301706502_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Func_2_t2070424348 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Func_2_t2070424348 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Func_2_t2070424348 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_5() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t2301706502_StaticFields, ___U3CU3Ef__amU24cache3_5)); }
	inline Func_3_t3989337584 * get_U3CU3Ef__amU24cache3_5() const { return ___U3CU3Ef__amU24cache3_5; }
	inline Func_3_t3989337584 ** get_address_of_U3CU3Ef__amU24cache3_5() { return &___U3CU3Ef__amU24cache3_5; }
	inline void set_U3CU3Ef__amU24cache3_5(Func_3_t3989337584 * value)
	{
		___U3CU3Ef__amU24cache3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_6() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t2301706502_StaticFields, ___U3CU3Ef__amU24cache4_6)); }
	inline Action_1_t3625273254 * get_U3CU3Ef__amU24cache4_6() const { return ___U3CU3Ef__amU24cache4_6; }
	inline Action_1_t3625273254 ** get_address_of_U3CU3Ef__amU24cache4_6() { return &___U3CU3Ef__amU24cache4_6; }
	inline void set_U3CU3Ef__amU24cache4_6(Action_1_t3625273254 * value)
	{
		___U3CU3Ef__amU24cache4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_7() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t2301706502_StaticFields, ___U3CU3Ef__amU24cache5_7)); }
	inline Action_1_t1454256935 * get_U3CU3Ef__amU24cache5_7() const { return ___U3CU3Ef__amU24cache5_7; }
	inline Action_1_t1454256935 ** get_address_of_U3CU3Ef__amU24cache5_7() { return &___U3CU3Ef__amU24cache5_7; }
	inline void set_U3CU3Ef__amU24cache5_7(Action_1_t1454256935 * value)
	{
		___U3CU3Ef__amU24cache5_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t2301706502_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Action_1_t1569734369 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Action_1_t1569734369 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Action_1_t1569734369 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(Sample01_ObservableWWW_t2301706502_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_1_t1943680094 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_1_t1943680094 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_1_t1943680094 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE01_OBSERVABLEWWW_T2301706502_H
#ifndef SAMPLE04_CONVERTFROMUNITYCALLBACK_T972102556_H
#define SAMPLE04_CONVERTFROMUNITYCALLBACK_T972102556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample04_ConvertFromUnityCallback
struct  Sample04_ConvertFromUnityCallback_t972102556  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Sample04_ConvertFromUnityCallback_t972102556_StaticFields
{
public:
	// System.Func`2<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback,System.Boolean> UniRx.Examples.Sample04_ConvertFromUnityCallback::<>f__am$cache0
	Func_2_t2219533162 * ___U3CU3Ef__amU24cache0_2;
	// System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback> UniRx.Examples.Sample04_ConvertFromUnityCallback::<>f__am$cache1
	Action_1_t2739833248 * ___U3CU3Ef__amU24cache1_3;
	// System.Func`2<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback,System.Boolean> UniRx.Examples.Sample04_ConvertFromUnityCallback::<>f__am$cache2
	Func_2_t2219533162 * ___U3CU3Ef__amU24cache2_4;
	// System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback> UniRx.Examples.Sample04_ConvertFromUnityCallback::<>f__am$cache3
	Action_1_t2739833248 * ___U3CU3Ef__amU24cache3_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(Sample04_ConvertFromUnityCallback_t972102556_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t2219533162 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t2219533162 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t2219533162 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(Sample04_ConvertFromUnityCallback_t972102556_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_1_t2739833248 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_1_t2739833248 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_1_t2739833248 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(Sample04_ConvertFromUnityCallback_t972102556_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Func_2_t2219533162 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Func_2_t2219533162 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Func_2_t2219533162 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_5() { return static_cast<int32_t>(offsetof(Sample04_ConvertFromUnityCallback_t972102556_StaticFields, ___U3CU3Ef__amU24cache3_5)); }
	inline Action_1_t2739833248 * get_U3CU3Ef__amU24cache3_5() const { return ___U3CU3Ef__amU24cache3_5; }
	inline Action_1_t2739833248 ** get_address_of_U3CU3Ef__amU24cache3_5() { return &___U3CU3Ef__amU24cache3_5; }
	inline void set_U3CU3Ef__amU24cache3_5(Action_1_t2739833248 * value)
	{
		___U3CU3Ef__amU24cache3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE04_CONVERTFROMUNITYCALLBACK_T972102556_H
#ifndef SAMPLE02_OBSERVABLETRIGGERS_T1329218277_H
#define SAMPLE02_OBSERVABLETRIGGERS_T1329218277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample02_ObservableTriggers
struct  Sample02_ObservableTriggers_t1329218277  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Sample02_ObservableTriggers_t1329218277_StaticFields
{
public:
	// System.Action`1<UniRx.Unit> UniRx.Examples.Sample02_ObservableTriggers::<>f__am$cache0
	Action_1_t3534717062 * ___U3CU3Ef__amU24cache0_2;
	// System.Action UniRx.Examples.Sample02_ObservableTriggers::<>f__am$cache1
	Action_t1264377477 * ___U3CU3Ef__amU24cache1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(Sample02_ObservableTriggers_t1329218277_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_1_t3534717062 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_1_t3534717062 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_1_t3534717062 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(Sample02_ObservableTriggers_t1329218277_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE02_OBSERVABLETRIGGERS_T1329218277_H
#ifndef WEBSOCKETTRIGGER_T496382349_H
#define WEBSOCKETTRIGGER_T496382349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.WebSocketTrigger
struct  WebSocketTrigger_t496382349  : public ObservableTriggerBase_t3020654715
{
public:
	// System.IDisposable socket.io.WebSocketTrigger::_cancelPingPong
	RuntimeObject* ____cancelPingPong_8;
	// UniRx.Subject`1<System.String> socket.io.WebSocketTrigger::_onRecv
	Subject_1_t1936107076 * ____onRecv_9;
	// socket.io.WebSocketWrapper socket.io.WebSocketTrigger::<WebSocket>k__BackingField
	WebSocketWrapper_t3330088300 * ___U3CWebSocketU3Ek__BackingField_10;
	// System.String socket.io.WebSocketTrigger::<LastWebSocketError>k__BackingField
	String_t* ___U3CLastWebSocketErrorU3Ek__BackingField_11;
	// System.Boolean socket.io.WebSocketTrigger::<IsProbed>k__BackingField
	bool ___U3CIsProbedU3Ek__BackingField_12;
	// System.Boolean socket.io.WebSocketTrigger::<IsUpgraded>k__BackingField
	bool ___U3CIsUpgradedU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of__cancelPingPong_8() { return static_cast<int32_t>(offsetof(WebSocketTrigger_t496382349, ____cancelPingPong_8)); }
	inline RuntimeObject* get__cancelPingPong_8() const { return ____cancelPingPong_8; }
	inline RuntimeObject** get_address_of__cancelPingPong_8() { return &____cancelPingPong_8; }
	inline void set__cancelPingPong_8(RuntimeObject* value)
	{
		____cancelPingPong_8 = value;
		Il2CppCodeGenWriteBarrier((&____cancelPingPong_8), value);
	}

	inline static int32_t get_offset_of__onRecv_9() { return static_cast<int32_t>(offsetof(WebSocketTrigger_t496382349, ____onRecv_9)); }
	inline Subject_1_t1936107076 * get__onRecv_9() const { return ____onRecv_9; }
	inline Subject_1_t1936107076 ** get_address_of__onRecv_9() { return &____onRecv_9; }
	inline void set__onRecv_9(Subject_1_t1936107076 * value)
	{
		____onRecv_9 = value;
		Il2CppCodeGenWriteBarrier((&____onRecv_9), value);
	}

	inline static int32_t get_offset_of_U3CWebSocketU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(WebSocketTrigger_t496382349, ___U3CWebSocketU3Ek__BackingField_10)); }
	inline WebSocketWrapper_t3330088300 * get_U3CWebSocketU3Ek__BackingField_10() const { return ___U3CWebSocketU3Ek__BackingField_10; }
	inline WebSocketWrapper_t3330088300 ** get_address_of_U3CWebSocketU3Ek__BackingField_10() { return &___U3CWebSocketU3Ek__BackingField_10; }
	inline void set_U3CWebSocketU3Ek__BackingField_10(WebSocketWrapper_t3330088300 * value)
	{
		___U3CWebSocketU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWebSocketU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CLastWebSocketErrorU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(WebSocketTrigger_t496382349, ___U3CLastWebSocketErrorU3Ek__BackingField_11)); }
	inline String_t* get_U3CLastWebSocketErrorU3Ek__BackingField_11() const { return ___U3CLastWebSocketErrorU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CLastWebSocketErrorU3Ek__BackingField_11() { return &___U3CLastWebSocketErrorU3Ek__BackingField_11; }
	inline void set_U3CLastWebSocketErrorU3Ek__BackingField_11(String_t* value)
	{
		___U3CLastWebSocketErrorU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastWebSocketErrorU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CIsProbedU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(WebSocketTrigger_t496382349, ___U3CIsProbedU3Ek__BackingField_12)); }
	inline bool get_U3CIsProbedU3Ek__BackingField_12() const { return ___U3CIsProbedU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CIsProbedU3Ek__BackingField_12() { return &___U3CIsProbedU3Ek__BackingField_12; }
	inline void set_U3CIsProbedU3Ek__BackingField_12(bool value)
	{
		___U3CIsProbedU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CIsUpgradedU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(WebSocketTrigger_t496382349, ___U3CIsUpgradedU3Ek__BackingField_13)); }
	inline bool get_U3CIsUpgradedU3Ek__BackingField_13() const { return ___U3CIsUpgradedU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsUpgradedU3Ek__BackingField_13() { return &___U3CIsUpgradedU3Ek__BackingField_13; }
	inline void set_U3CIsUpgradedU3Ek__BackingField_13(bool value)
	{
		___U3CIsUpgradedU3Ek__BackingField_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBSOCKETTRIGGER_T496382349_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (ChunkStream_t2280345721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[9] = 
{
	ChunkStream_t2280345721::get_offset_of__chunkRead_0(),
	ChunkStream_t2280345721::get_offset_of__chunkSize_1(),
	ChunkStream_t2280345721::get_offset_of__chunks_2(),
	ChunkStream_t2280345721::get_offset_of__gotIt_3(),
	ChunkStream_t2280345721::get_offset_of__headers_4(),
	ChunkStream_t2280345721::get_offset_of__saved_5(),
	ChunkStream_t2280345721::get_offset_of__sawCr_6(),
	ChunkStream_t2280345721::get_offset_of__state_7(),
	ChunkStream_t2280345721::get_offset_of__trailerState_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (ClientSslConfiguration_t664129470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[2] = 
{
	ClientSslConfiguration_t664129470::get_offset_of__certs_6(),
	ClientSslConfiguration_t664129470::get_offset_of__host_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (Cookie_t4203102285), -1, sizeof(Cookie_t4203102285_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2302[17] = 
{
	Cookie_t4203102285::get_offset_of__comment_0(),
	Cookie_t4203102285::get_offset_of__commentUri_1(),
	Cookie_t4203102285::get_offset_of__discard_2(),
	Cookie_t4203102285::get_offset_of__domain_3(),
	Cookie_t4203102285::get_offset_of__expires_4(),
	Cookie_t4203102285::get_offset_of__httpOnly_5(),
	Cookie_t4203102285::get_offset_of__name_6(),
	Cookie_t4203102285::get_offset_of__path_7(),
	Cookie_t4203102285::get_offset_of__port_8(),
	Cookie_t4203102285::get_offset_of__ports_9(),
	Cookie_t4203102285_StaticFields::get_offset_of__reservedCharsForName_10(),
	Cookie_t4203102285_StaticFields::get_offset_of__reservedCharsForValue_11(),
	Cookie_t4203102285::get_offset_of__secure_12(),
	Cookie_t4203102285::get_offset_of__timestamp_13(),
	Cookie_t4203102285::get_offset_of__value_14(),
	Cookie_t4203102285::get_offset_of__version_15(),
	Cookie_t4203102285::get_offset_of_U3CExactDomainU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (CookieCollection_t962330244), -1, sizeof(CookieCollection_t962330244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2303[4] = 
{
	CookieCollection_t962330244::get_offset_of__list_0(),
	CookieCollection_t962330244::get_offset_of__sync_1(),
	CookieCollection_t962330244_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_2(),
	CookieCollection_t962330244_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (CookieException_t1899604716), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (EndPointListener_t884507598), -1, sizeof(EndPointListener_t884507598_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2305[12] = 
{
	EndPointListener_t884507598::get_offset_of__all_0(),
	EndPointListener_t884507598_StaticFields::get_offset_of__defaultCertFolderPath_1(),
	EndPointListener_t884507598::get_offset_of__endpoint_2(),
	EndPointListener_t884507598::get_offset_of__prefixes_3(),
	EndPointListener_t884507598::get_offset_of__secure_4(),
	EndPointListener_t884507598::get_offset_of__socket_5(),
	EndPointListener_t884507598::get_offset_of__sslConfig_6(),
	EndPointListener_t884507598::get_offset_of__unhandled_7(),
	EndPointListener_t884507598::get_offset_of__unregistered_8(),
	EndPointListener_t884507598::get_offset_of__unregisteredSync_9(),
	EndPointListener_t884507598_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_10(),
	EndPointListener_t884507598_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (EndPointManager_t3599117988), -1, sizeof(EndPointManager_t3599117988_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2306[1] = 
{
	EndPointManager_t3599117988_StaticFields::get_offset_of__endpoints_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (HttpBasicIdentity_t251442525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[1] = 
{
	HttpBasicIdentity_t251442525::get_offset_of__password_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (HttpConnection_t4177287240), -1, sizeof(HttpConnection_t4177287240_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2308[24] = 
{
	HttpConnection_t4177287240::get_offset_of__buffer_0(),
	0,
	HttpConnection_t4177287240::get_offset_of__context_2(),
	HttpConnection_t4177287240::get_offset_of__contextRegistered_3(),
	HttpConnection_t4177287240::get_offset_of__currentLine_4(),
	HttpConnection_t4177287240::get_offset_of__inputState_5(),
	HttpConnection_t4177287240::get_offset_of__inputStream_6(),
	HttpConnection_t4177287240::get_offset_of__lastListener_7(),
	HttpConnection_t4177287240::get_offset_of__lineState_8(),
	HttpConnection_t4177287240::get_offset_of__listener_9(),
	HttpConnection_t4177287240::get_offset_of__outputStream_10(),
	HttpConnection_t4177287240::get_offset_of__position_11(),
	HttpConnection_t4177287240::get_offset_of__requestBuffer_12(),
	HttpConnection_t4177287240::get_offset_of__reuses_13(),
	HttpConnection_t4177287240::get_offset_of__secure_14(),
	HttpConnection_t4177287240::get_offset_of__socket_15(),
	HttpConnection_t4177287240::get_offset_of__stream_16(),
	HttpConnection_t4177287240::get_offset_of__sync_17(),
	HttpConnection_t4177287240::get_offset_of__timeout_18(),
	HttpConnection_t4177287240::get_offset_of__timeoutCanceled_19(),
	HttpConnection_t4177287240::get_offset_of__timer_20(),
	HttpConnection_t4177287240_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_21(),
	HttpConnection_t4177287240_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_22(),
	HttpConnection_t4177287240_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (HttpDigestIdentity_t1241393384), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[1] = 
{
	HttpDigestIdentity_t1241393384::get_offset_of__parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (HttpHeaderInfo_t2062258289), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[2] = 
{
	HttpHeaderInfo_t2062258289::get_offset_of__name_0(),
	HttpHeaderInfo_t2062258289::get_offset_of__type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (HttpHeaderType_t2889339440)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2311[8] = 
{
	HttpHeaderType_t2889339440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (HttpListener_t2467819517), -1, sizeof(HttpListener_t2467819517_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2312[21] = 
{
	HttpListener_t2467819517::get_offset_of__authSchemes_0(),
	HttpListener_t2467819517::get_offset_of__authSchemeSelector_1(),
	HttpListener_t2467819517::get_offset_of__certFolderPath_2(),
	HttpListener_t2467819517::get_offset_of__connections_3(),
	HttpListener_t2467819517::get_offset_of__connectionsSync_4(),
	HttpListener_t2467819517::get_offset_of__ctxQueue_5(),
	HttpListener_t2467819517::get_offset_of__ctxQueueSync_6(),
	HttpListener_t2467819517::get_offset_of__ctxRegistry_7(),
	HttpListener_t2467819517::get_offset_of__ctxRegistrySync_8(),
	HttpListener_t2467819517_StaticFields::get_offset_of__defaultRealm_9(),
	HttpListener_t2467819517::get_offset_of__disposed_10(),
	HttpListener_t2467819517::get_offset_of__ignoreWriteExceptions_11(),
	HttpListener_t2467819517::get_offset_of__listening_12(),
	HttpListener_t2467819517::get_offset_of__logger_13(),
	HttpListener_t2467819517::get_offset_of__prefixes_14(),
	HttpListener_t2467819517::get_offset_of__realm_15(),
	HttpListener_t2467819517::get_offset_of__reuseAddress_16(),
	HttpListener_t2467819517::get_offset_of__sslConfig_17(),
	HttpListener_t2467819517::get_offset_of__userCredFinder_18(),
	HttpListener_t2467819517::get_offset_of__waitQueue_19(),
	HttpListener_t2467819517::get_offset_of__waitQueueSync_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (HttpListenerAsyncResult_t1598414744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2313[10] = 
{
	HttpListenerAsyncResult_t1598414744::get_offset_of__callback_0(),
	HttpListenerAsyncResult_t1598414744::get_offset_of__completed_1(),
	HttpListenerAsyncResult_t1598414744::get_offset_of__context_2(),
	HttpListenerAsyncResult_t1598414744::get_offset_of__endCalled_3(),
	HttpListenerAsyncResult_t1598414744::get_offset_of__exception_4(),
	HttpListenerAsyncResult_t1598414744::get_offset_of__inGet_5(),
	HttpListenerAsyncResult_t1598414744::get_offset_of__state_6(),
	HttpListenerAsyncResult_t1598414744::get_offset_of__sync_7(),
	HttpListenerAsyncResult_t1598414744::get_offset_of__syncCompleted_8(),
	HttpListenerAsyncResult_t1598414744::get_offset_of__waitHandle_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (U3CcompleteU3Ec__AnonStorey0_t1824639194), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[2] = 
{
	U3CcompleteU3Ec__AnonStorey0_t1824639194::get_offset_of_callback_0(),
	U3CcompleteU3Ec__AnonStorey0_t1824639194::get_offset_of_asyncResult_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (HttpListenerContext_t3723273891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[8] = 
{
	HttpListenerContext_t3723273891::get_offset_of__connection_0(),
	HttpListenerContext_t3723273891::get_offset_of__error_1(),
	HttpListenerContext_t3723273891::get_offset_of__errorStatus_2(),
	HttpListenerContext_t3723273891::get_offset_of__listener_3(),
	HttpListenerContext_t3723273891::get_offset_of__request_4(),
	HttpListenerContext_t3723273891::get_offset_of__response_5(),
	HttpListenerContext_t3723273891::get_offset_of__user_6(),
	HttpListenerContext_t3723273891::get_offset_of__websocketContext_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (HttpListenerException_t3074104922), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (HttpListenerPrefix_t910967454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2317[7] = 
{
	HttpListenerPrefix_t910967454::get_offset_of__host_0(),
	HttpListenerPrefix_t910967454::get_offset_of__listener_1(),
	HttpListenerPrefix_t910967454::get_offset_of__original_2(),
	HttpListenerPrefix_t910967454::get_offset_of__path_3(),
	HttpListenerPrefix_t910967454::get_offset_of__port_4(),
	HttpListenerPrefix_t910967454::get_offset_of__prefix_5(),
	HttpListenerPrefix_t910967454::get_offset_of__secure_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (HttpListenerPrefixCollection_t702486717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[2] = 
{
	HttpListenerPrefixCollection_t702486717::get_offset_of__listener_0(),
	HttpListenerPrefixCollection_t702486717::get_offset_of__prefixes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (HttpListenerRequest_t2959552699), -1, sizeof(HttpListenerRequest_t2959552699_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2319[22] = 
{
	HttpListenerRequest_t2959552699_StaticFields::get_offset_of__100continue_0(),
	HttpListenerRequest_t2959552699::get_offset_of__acceptTypes_1(),
	HttpListenerRequest_t2959552699::get_offset_of__chunked_2(),
	HttpListenerRequest_t2959552699::get_offset_of__contentEncoding_3(),
	HttpListenerRequest_t2959552699::get_offset_of__contentLength_4(),
	HttpListenerRequest_t2959552699::get_offset_of__contentLengthSet_5(),
	HttpListenerRequest_t2959552699::get_offset_of__context_6(),
	HttpListenerRequest_t2959552699::get_offset_of__cookies_7(),
	HttpListenerRequest_t2959552699::get_offset_of__headers_8(),
	HttpListenerRequest_t2959552699::get_offset_of__identifier_9(),
	HttpListenerRequest_t2959552699::get_offset_of__inputStream_10(),
	HttpListenerRequest_t2959552699::get_offset_of__keepAlive_11(),
	HttpListenerRequest_t2959552699::get_offset_of__keepAliveSet_12(),
	HttpListenerRequest_t2959552699::get_offset_of__method_13(),
	HttpListenerRequest_t2959552699::get_offset_of__queryString_14(),
	HttpListenerRequest_t2959552699::get_offset_of__referer_15(),
	HttpListenerRequest_t2959552699::get_offset_of__uri_16(),
	HttpListenerRequest_t2959552699::get_offset_of__url_17(),
	HttpListenerRequest_t2959552699::get_offset_of__userLanguages_18(),
	HttpListenerRequest_t2959552699::get_offset_of__version_19(),
	HttpListenerRequest_t2959552699::get_offset_of__websocketRequest_20(),
	HttpListenerRequest_t2959552699::get_offset_of__websocketRequestSet_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (HttpListenerResponse_t2818529495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[16] = 
{
	HttpListenerResponse_t2818529495::get_offset_of__closeConnection_0(),
	HttpListenerResponse_t2818529495::get_offset_of__contentEncoding_1(),
	HttpListenerResponse_t2818529495::get_offset_of__contentLength_2(),
	HttpListenerResponse_t2818529495::get_offset_of__contentType_3(),
	HttpListenerResponse_t2818529495::get_offset_of__context_4(),
	HttpListenerResponse_t2818529495::get_offset_of__cookies_5(),
	HttpListenerResponse_t2818529495::get_offset_of__disposed_6(),
	HttpListenerResponse_t2818529495::get_offset_of__headers_7(),
	HttpListenerResponse_t2818529495::get_offset_of__headersSent_8(),
	HttpListenerResponse_t2818529495::get_offset_of__keepAlive_9(),
	HttpListenerResponse_t2818529495::get_offset_of__location_10(),
	HttpListenerResponse_t2818529495::get_offset_of__outputStream_11(),
	HttpListenerResponse_t2818529495::get_offset_of__sendChunked_12(),
	HttpListenerResponse_t2818529495::get_offset_of__statusCode_13(),
	HttpListenerResponse_t2818529495::get_offset_of__statusDescription_14(),
	HttpListenerResponse_t2818529495::get_offset_of__version_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (U3CfindCookieU3Ec__Iterator0_t3171476380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[11] = 
{
	U3CfindCookieU3Ec__Iterator0_t3171476380::get_offset_of_cookie_0(),
	U3CfindCookieU3Ec__Iterator0_t3171476380::get_offset_of_U3CnameU3E__0_1(),
	U3CfindCookieU3Ec__Iterator0_t3171476380::get_offset_of_U3CdomainU3E__0_2(),
	U3CfindCookieU3Ec__Iterator0_t3171476380::get_offset_of_U3CpathU3E__0_3(),
	U3CfindCookieU3Ec__Iterator0_t3171476380::get_offset_of_U24locvar0_4(),
	U3CfindCookieU3Ec__Iterator0_t3171476380::get_offset_of_U3CcU3E__1_5(),
	U3CfindCookieU3Ec__Iterator0_t3171476380::get_offset_of_U24locvar1_6(),
	U3CfindCookieU3Ec__Iterator0_t3171476380::get_offset_of_U24this_7(),
	U3CfindCookieU3Ec__Iterator0_t3171476380::get_offset_of_U24current_8(),
	U3CfindCookieU3Ec__Iterator0_t3171476380::get_offset_of_U24disposing_9(),
	U3CfindCookieU3Ec__Iterator0_t3171476380::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (U3CCloseU3Ec__AnonStorey1_t3055466982), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[2] = 
{
	U3CCloseU3Ec__AnonStorey1_t3055466982::get_offset_of_output_0(),
	U3CCloseU3Ec__AnonStorey1_t3055466982::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (HttpRequestHeader_t3011722756)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2323[46] = 
{
	HttpRequestHeader_t3011722756::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (HttpResponseHeader_t1598204158)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2324[35] = 
{
	HttpResponseHeader_t1598204158::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (HttpStatusCode_t4029129360)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2325[47] = 
{
	HttpStatusCode_t4029129360::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (HttpStreamAsyncResult_t890764446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[10] = 
{
	HttpStreamAsyncResult_t890764446::get_offset_of__buffer_0(),
	HttpStreamAsyncResult_t890764446::get_offset_of__callback_1(),
	HttpStreamAsyncResult_t890764446::get_offset_of__completed_2(),
	HttpStreamAsyncResult_t890764446::get_offset_of__count_3(),
	HttpStreamAsyncResult_t890764446::get_offset_of__exception_4(),
	HttpStreamAsyncResult_t890764446::get_offset_of__offset_5(),
	HttpStreamAsyncResult_t890764446::get_offset_of__state_6(),
	HttpStreamAsyncResult_t890764446::get_offset_of__sync_7(),
	HttpStreamAsyncResult_t890764446::get_offset_of__syncRead_8(),
	HttpStreamAsyncResult_t890764446::get_offset_of__waitHandle_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (HttpUtility_t3452211165), -1, sizeof(HttpUtility_t3452211165_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2327[3] = 
{
	HttpUtility_t3452211165_StaticFields::get_offset_of__entities_0(),
	HttpUtility_t3452211165_StaticFields::get_offset_of__hexChars_1(),
	HttpUtility_t3452211165_StaticFields::get_offset_of__sync_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (HttpVersion_t1629733732), -1, sizeof(HttpVersion_t1629733732_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2328[2] = 
{
	HttpVersion_t1629733732_StaticFields::get_offset_of_Version10_0(),
	HttpVersion_t1629733732_StaticFields::get_offset_of_Version11_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (InputChunkState_t1224634762)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2329[6] = 
{
	InputChunkState_t1224634762::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (InputState_t4053609843)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2330[3] = 
{
	InputState_t4053609843::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (LineState_t320376362)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2331[4] = 
{
	LineState_t320376362::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (NetworkCredential_t1094796801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[4] = 
{
	NetworkCredential_t1094796801::get_offset_of__domain_0(),
	NetworkCredential_t1094796801::get_offset_of__password_1(),
	NetworkCredential_t1094796801::get_offset_of__roles_2(),
	NetworkCredential_t1094796801::get_offset_of__userName_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (QueryStringCollection_t3336584429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (ReadBufferState_t3629953814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[5] = 
{
	ReadBufferState_t3629953814::get_offset_of__asyncResult_0(),
	ReadBufferState_t3629953814::get_offset_of__buffer_1(),
	ReadBufferState_t3629953814::get_offset_of__count_2(),
	ReadBufferState_t3629953814::get_offset_of__initialCount_3(),
	ReadBufferState_t3629953814::get_offset_of__offset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (RequestStream_t1020063535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[6] = 
{
	RequestStream_t1020063535::get_offset_of__bodyLeft_1(),
	RequestStream_t1020063535::get_offset_of__buffer_2(),
	RequestStream_t1020063535::get_offset_of__count_3(),
	RequestStream_t1020063535::get_offset_of__disposed_4(),
	RequestStream_t1020063535::get_offset_of__offset_5(),
	RequestStream_t1020063535::get_offset_of__stream_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (ResponseStream_t2828822308), -1, sizeof(ResponseStream_t2828822308_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2336[9] = 
{
	ResponseStream_t2828822308::get_offset_of__body_1(),
	ResponseStream_t2828822308_StaticFields::get_offset_of__crlf_2(),
	ResponseStream_t2828822308::get_offset_of__disposed_3(),
	ResponseStream_t2828822308::get_offset_of__response_4(),
	ResponseStream_t2828822308::get_offset_of__sendChunked_5(),
	ResponseStream_t2828822308::get_offset_of__stream_6(),
	ResponseStream_t2828822308::get_offset_of__write_7(),
	ResponseStream_t2828822308::get_offset_of__writeBody_8(),
	ResponseStream_t2828822308::get_offset_of__writeChunked_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (ServerSslConfiguration_t3240511754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[2] = 
{
	ServerSslConfiguration_t3240511754::get_offset_of__cert_6(),
	ServerSslConfiguration_t3240511754::get_offset_of__clientCertRequired_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (SslConfiguration_t3726145939), -1, sizeof(SslConfiguration_t3726145939_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2338[6] = 
{
	SslConfiguration_t3726145939::get_offset_of__certSelectionCallback_0(),
	SslConfiguration_t3726145939::get_offset_of__certValidationCallback_1(),
	SslConfiguration_t3726145939::get_offset_of__checkCertRevocation_2(),
	SslConfiguration_t3726145939::get_offset_of__enabledProtocols_3(),
	SslConfiguration_t3726145939_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	SslConfiguration_t3726145939_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (WebHeaderCollection_t1205255311), -1, sizeof(WebHeaderCollection_t1205255311_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2339[3] = 
{
	WebHeaderCollection_t1205255311_StaticFields::get_offset_of__headers_12(),
	WebHeaderCollection_t1205255311::get_offset_of__internallyUsed_13(),
	WebHeaderCollection_t1205255311::get_offset_of__state_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (U3CToStringMultiValueU3Ec__AnonStorey0_t515054707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[3] = 
{
	U3CToStringMultiValueU3Ec__AnonStorey0_t515054707::get_offset_of_response_0(),
	U3CToStringMultiValueU3Ec__AnonStorey0_t515054707::get_offset_of_buff_1(),
	U3CToStringMultiValueU3Ec__AnonStorey0_t515054707::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (U3CGetObjectDataU3Ec__AnonStorey1_t4038268818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[3] = 
{
	U3CGetObjectDataU3Ec__AnonStorey1_t4038268818::get_offset_of_serializationInfo_0(),
	U3CGetObjectDataU3Ec__AnonStorey1_t4038268818::get_offset_of_cnt_1(),
	U3CGetObjectDataU3Ec__AnonStorey1_t4038268818::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (U3CToStringU3Ec__AnonStorey2_t1832203497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[2] = 
{
	U3CToStringU3Ec__AnonStorey2_t1832203497::get_offset_of_buff_0(),
	U3CToStringU3Ec__AnonStorey2_t1832203497::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (HttpListenerWebSocketContext_t3770402993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[2] = 
{
	HttpListenerWebSocketContext_t3770402993::get_offset_of__context_0(),
	HttpListenerWebSocketContext_t3770402993::get_offset_of__websocket_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (U3CU3Ec__Iterator0_t2500134981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[8] = 
{
	U3CU3Ec__Iterator0_t2500134981::get_offset_of_U3CprotocolsU3E__0_0(),
	U3CU3Ec__Iterator0_t2500134981::get_offset_of_U24locvar0_1(),
	U3CU3Ec__Iterator0_t2500134981::get_offset_of_U24locvar1_2(),
	U3CU3Ec__Iterator0_t2500134981::get_offset_of_U3CprotocolU3E__1_3(),
	U3CU3Ec__Iterator0_t2500134981::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator0_t2500134981::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator0_t2500134981::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator0_t2500134981::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (TcpListenerWebSocketContext_t2376318492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[10] = 
{
	TcpListenerWebSocketContext_t2376318492::get_offset_of__cookies_0(),
	TcpListenerWebSocketContext_t2376318492::get_offset_of__logger_1(),
	TcpListenerWebSocketContext_t2376318492::get_offset_of__queryString_2(),
	TcpListenerWebSocketContext_t2376318492::get_offset_of__request_3(),
	TcpListenerWebSocketContext_t2376318492::get_offset_of__secure_4(),
	TcpListenerWebSocketContext_t2376318492::get_offset_of__stream_5(),
	TcpListenerWebSocketContext_t2376318492::get_offset_of__tcpClient_6(),
	TcpListenerWebSocketContext_t2376318492::get_offset_of__uri_7(),
	TcpListenerWebSocketContext_t2376318492::get_offset_of__user_8(),
	TcpListenerWebSocketContext_t2376318492::get_offset_of__websocket_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (U3CU3Ec__Iterator0_t3615578217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[8] = 
{
	U3CU3Ec__Iterator0_t3615578217::get_offset_of_U3CprotocolsU3E__0_0(),
	U3CU3Ec__Iterator0_t3615578217::get_offset_of_U24locvar0_1(),
	U3CU3Ec__Iterator0_t3615578217::get_offset_of_U24locvar1_2(),
	U3CU3Ec__Iterator0_t3615578217::get_offset_of_U3CprotocolU3E__1_3(),
	U3CU3Ec__Iterator0_t3615578217::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator0_t3615578217::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator0_t3615578217::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator0_t3615578217::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (U3CAuthenticateU3Ec__AnonStorey1_t3142200144), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[7] = 
{
	U3CAuthenticateU3Ec__AnonStorey1_t3142200144::get_offset_of_retry_0(),
	U3CAuthenticateU3Ec__AnonStorey1_t3142200144::get_offset_of_scheme_1(),
	U3CAuthenticateU3Ec__AnonStorey1_t3142200144::get_offset_of_realm_2(),
	U3CAuthenticateU3Ec__AnonStorey1_t3142200144::get_offset_of_credentialsFinder_3(),
	U3CAuthenticateU3Ec__AnonStorey1_t3142200144::get_offset_of_chal_4(),
	U3CAuthenticateU3Ec__AnonStorey1_t3142200144::get_offset_of_auth_5(),
	U3CAuthenticateU3Ec__AnonStorey1_t3142200144::get_offset_of_U24this_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (WebSocketContext_t619421455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (Opcode_t2755924248)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2349[7] = 
{
	Opcode_t2755924248::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (PayloadData_t688932160), -1, sizeof(PayloadData_t688932160_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2350[5] = 
{
	PayloadData_t688932160::get_offset_of__data_0(),
	PayloadData_t688932160::get_offset_of__extDataLength_1(),
	PayloadData_t688932160::get_offset_of__length_2(),
	PayloadData_t688932160_StaticFields::get_offset_of_Empty_3(),
	PayloadData_t688932160_StaticFields::get_offset_of_MaxLength_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t268869223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[7] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t268869223::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t268869223::get_offset_of_U24locvar1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t268869223::get_offset_of_U3CbU3E__1_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t268869223::get_offset_of_U24this_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t268869223::get_offset_of_U24current_4(),
	U3CGetEnumeratorU3Ec__Iterator0_t268869223::get_offset_of_U24disposing_5(),
	U3CGetEnumeratorU3Ec__Iterator0_t268869223::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (Rsv_t2704667083)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2352[3] = 
{
	Rsv_t2704667083::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (HttpRequestEventArgs_t1003470709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2353[2] = 
{
	HttpRequestEventArgs_t1003470709::get_offset_of__request_1(),
	HttpRequestEventArgs_t1003470709::get_offset_of__response_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (HttpServer_t3049239368), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[21] = 
{
	HttpServer_t3049239368::get_offset_of__address_0(),
	HttpServer_t3049239368::get_offset_of__hostname_1(),
	HttpServer_t3049239368::get_offset_of__listener_2(),
	HttpServer_t3049239368::get_offset_of__logger_3(),
	HttpServer_t3049239368::get_offset_of__port_4(),
	HttpServer_t3049239368::get_offset_of__receiveThread_5(),
	HttpServer_t3049239368::get_offset_of__rootPath_6(),
	HttpServer_t3049239368::get_offset_of__secure_7(),
	HttpServer_t3049239368::get_offset_of__services_8(),
	HttpServer_t3049239368::get_offset_of__state_9(),
	HttpServer_t3049239368::get_offset_of__sync_10(),
	HttpServer_t3049239368::get_offset_of__windows_11(),
	HttpServer_t3049239368::get_offset_of_OnConnect_12(),
	HttpServer_t3049239368::get_offset_of_OnDelete_13(),
	HttpServer_t3049239368::get_offset_of_OnGet_14(),
	HttpServer_t3049239368::get_offset_of_OnHead_15(),
	HttpServer_t3049239368::get_offset_of_OnOptions_16(),
	HttpServer_t3049239368::get_offset_of_OnPatch_17(),
	HttpServer_t3049239368::get_offset_of_OnPost_18(),
	HttpServer_t3049239368::get_offset_of_OnPut_19(),
	HttpServer_t3049239368::get_offset_of_OnTrace_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (U3CreceiveRequestU3Ec__AnonStorey0_t2532472181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[2] = 
{
	U3CreceiveRequestU3Ec__AnonStorey0_t2532472181::get_offset_of_ctx_0(),
	U3CreceiveRequestU3Ec__AnonStorey0_t2532472181::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (ServerState_t358351827)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2357[5] = 
{
	ServerState_t358351827::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (WebSocketBehavior_t229673147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[10] = 
{
	WebSocketBehavior_t229673147::get_offset_of__context_0(),
	WebSocketBehavior_t229673147::get_offset_of__cookiesValidator_1(),
	WebSocketBehavior_t229673147::get_offset_of__emitOnPing_2(),
	WebSocketBehavior_t229673147::get_offset_of__id_3(),
	WebSocketBehavior_t229673147::get_offset_of__ignoreExtensions_4(),
	WebSocketBehavior_t229673147::get_offset_of__originValidator_5(),
	WebSocketBehavior_t229673147::get_offset_of__protocol_6(),
	WebSocketBehavior_t229673147::get_offset_of__sessions_7(),
	WebSocketBehavior_t229673147::get_offset_of__startTime_8(),
	WebSocketBehavior_t229673147::get_offset_of__websocket_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (WebSocketServer_t2289661728), -1, sizeof(WebSocketServer_t2289661728_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2359[17] = 
{
	WebSocketServer_t2289661728::get_offset_of__address_0(),
	WebSocketServer_t2289661728::get_offset_of__authSchemes_1(),
	WebSocketServer_t2289661728_StaticFields::get_offset_of__defaultRealm_2(),
	WebSocketServer_t2289661728::get_offset_of__dnsStyle_3(),
	WebSocketServer_t2289661728::get_offset_of__hostname_4(),
	WebSocketServer_t2289661728::get_offset_of__listener_5(),
	WebSocketServer_t2289661728::get_offset_of__logger_6(),
	WebSocketServer_t2289661728::get_offset_of__port_7(),
	WebSocketServer_t2289661728::get_offset_of__realm_8(),
	WebSocketServer_t2289661728::get_offset_of__receiveThread_9(),
	WebSocketServer_t2289661728::get_offset_of__reuseAddress_10(),
	WebSocketServer_t2289661728::get_offset_of__secure_11(),
	WebSocketServer_t2289661728::get_offset_of__services_12(),
	WebSocketServer_t2289661728::get_offset_of__sslConfig_13(),
	WebSocketServer_t2289661728::get_offset_of__state_14(),
	WebSocketServer_t2289661728::get_offset_of__sync_15(),
	WebSocketServer_t2289661728::get_offset_of__userCredFinder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (U3CreceiveRequestU3Ec__AnonStorey0_t3816822753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[2] = 
{
	U3CreceiveRequestU3Ec__AnonStorey0_t3816822753::get_offset_of_cl_0(),
	U3CreceiveRequestU3Ec__AnonStorey0_t3816822753::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (WebSocketServiceHost_t860458738), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (WebSocketServiceManager_t2425053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2363[6] = 
{
	WebSocketServiceManager_t2425053::get_offset_of__clean_0(),
	WebSocketServiceManager_t2425053::get_offset_of__hosts_1(),
	WebSocketServiceManager_t2425053::get_offset_of__logger_2(),
	WebSocketServiceManager_t2425053::get_offset_of__state_3(),
	WebSocketServiceManager_t2425053::get_offset_of__sync_4(),
	WebSocketServiceManager_t2425053::get_offset_of__waitTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (U3CbroadcastAsyncU3Ec__AnonStorey0_t3156767960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[4] = 
{
	U3CbroadcastAsyncU3Ec__AnonStorey0_t3156767960::get_offset_of_opcode_0(),
	U3CbroadcastAsyncU3Ec__AnonStorey0_t3156767960::get_offset_of_data_1(),
	U3CbroadcastAsyncU3Ec__AnonStorey0_t3156767960::get_offset_of_completed_2(),
	U3CbroadcastAsyncU3Ec__AnonStorey0_t3156767960::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (U3CbroadcastAsyncU3Ec__AnonStorey1_t818115800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2365[4] = 
{
	U3CbroadcastAsyncU3Ec__AnonStorey1_t818115800::get_offset_of_opcode_0(),
	U3CbroadcastAsyncU3Ec__AnonStorey1_t818115800::get_offset_of_stream_1(),
	U3CbroadcastAsyncU3Ec__AnonStorey1_t818115800::get_offset_of_completed_2(),
	U3CbroadcastAsyncU3Ec__AnonStorey1_t818115800::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (U3CBroadcastAsyncU3Ec__AnonStorey2_t1520948972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[3] = 
{
	U3CBroadcastAsyncU3Ec__AnonStorey2_t1520948972::get_offset_of_length_0(),
	U3CBroadcastAsyncU3Ec__AnonStorey2_t1520948972::get_offset_of_completed_1(),
	U3CBroadcastAsyncU3Ec__AnonStorey2_t1520948972::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (WebSocketSessionManager_t336645657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2367[9] = 
{
	WebSocketSessionManager_t336645657::get_offset_of__clean_0(),
	WebSocketSessionManager_t336645657::get_offset_of__forSweep_1(),
	WebSocketSessionManager_t336645657::get_offset_of__logger_2(),
	WebSocketSessionManager_t336645657::get_offset_of__sessions_3(),
	WebSocketSessionManager_t336645657::get_offset_of__state_4(),
	WebSocketSessionManager_t336645657::get_offset_of__sweeping_5(),
	WebSocketSessionManager_t336645657::get_offset_of__sweepTimer_6(),
	WebSocketSessionManager_t336645657::get_offset_of__sync_7(),
	WebSocketSessionManager_t336645657::get_offset_of__waitTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (U3CU3Ec__Iterator0_t719643155), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2368[6] = 
{
	U3CU3Ec__Iterator0_t719643155::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t719643155::get_offset_of_U3CresU3E__1_1(),
	U3CU3Ec__Iterator0_t719643155::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t719643155::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t719643155::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t719643155::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (U3CU3Ec__Iterator1_t3058295315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2369[6] = 
{
	U3CU3Ec__Iterator1_t3058295315::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator1_t3058295315::get_offset_of_U3CresU3E__1_1(),
	U3CU3Ec__Iterator1_t3058295315::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator1_t3058295315::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator1_t3058295315::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator1_t3058295315::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (U3CbroadcastAsyncU3Ec__AnonStorey2_t3463414668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2370[4] = 
{
	U3CbroadcastAsyncU3Ec__AnonStorey2_t3463414668::get_offset_of_opcode_0(),
	U3CbroadcastAsyncU3Ec__AnonStorey2_t3463414668::get_offset_of_data_1(),
	U3CbroadcastAsyncU3Ec__AnonStorey2_t3463414668::get_offset_of_completed_2(),
	U3CbroadcastAsyncU3Ec__AnonStorey2_t3463414668::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (U3CbroadcastAsyncU3Ec__AnonStorey3_t1507099532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2371[4] = 
{
	U3CbroadcastAsyncU3Ec__AnonStorey3_t1507099532::get_offset_of_opcode_0(),
	U3CbroadcastAsyncU3Ec__AnonStorey3_t1507099532::get_offset_of_stream_1(),
	U3CbroadcastAsyncU3Ec__AnonStorey3_t1507099532::get_offset_of_completed_2(),
	U3CbroadcastAsyncU3Ec__AnonStorey3_t1507099532::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (U3CBroadcastAsyncU3Ec__AnonStorey4_t1910876960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2372[3] = 
{
	U3CBroadcastAsyncU3Ec__AnonStorey4_t1910876960::get_offset_of_length_0(),
	U3CBroadcastAsyncU3Ec__AnonStorey4_t1910876960::get_offset_of_completed_1(),
	U3CBroadcastAsyncU3Ec__AnonStorey4_t1910876960::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (WebSocket_t62038747), -1, sizeof(WebSocket_t62038747_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2373[51] = 
{
	WebSocket_t62038747::get_offset_of__authChallenge_0(),
	WebSocket_t62038747::get_offset_of__base64Key_1(),
	WebSocket_t62038747::get_offset_of__client_2(),
	WebSocket_t62038747::get_offset_of__closeContext_3(),
	WebSocket_t62038747::get_offset_of__compression_4(),
	WebSocket_t62038747::get_offset_of__context_5(),
	WebSocket_t62038747::get_offset_of__cookies_6(),
	WebSocket_t62038747::get_offset_of__credentials_7(),
	WebSocket_t62038747::get_offset_of__emitOnPing_8(),
	WebSocket_t62038747::get_offset_of__enableRedirection_9(),
	WebSocket_t62038747::get_offset_of__exitReceiving_10(),
	WebSocket_t62038747::get_offset_of__extensions_11(),
	WebSocket_t62038747::get_offset_of__extensionsRequested_12(),
	WebSocket_t62038747::get_offset_of__forConn_13(),
	WebSocket_t62038747::get_offset_of__forMessageEventQueue_14(),
	WebSocket_t62038747::get_offset_of__forSend_15(),
	WebSocket_t62038747::get_offset_of__fragmentsBuffer_16(),
	WebSocket_t62038747::get_offset_of__fragmentsCompressed_17(),
	WebSocket_t62038747::get_offset_of__fragmentsOpcode_18(),
	0,
	WebSocket_t62038747::get_offset_of__handshakeRequestChecker_20(),
	WebSocket_t62038747::get_offset_of__ignoreExtensions_21(),
	WebSocket_t62038747::get_offset_of__inContinuation_22(),
	WebSocket_t62038747::get_offset_of__inMessage_23(),
	WebSocket_t62038747::get_offset_of__logger_24(),
	WebSocket_t62038747::get_offset_of__message_25(),
	WebSocket_t62038747::get_offset_of__messageEventQueue_26(),
	WebSocket_t62038747::get_offset_of__nonceCount_27(),
	WebSocket_t62038747::get_offset_of__origin_28(),
	WebSocket_t62038747::get_offset_of__preAuth_29(),
	WebSocket_t62038747::get_offset_of__protocol_30(),
	WebSocket_t62038747::get_offset_of__protocols_31(),
	WebSocket_t62038747::get_offset_of__protocolsRequested_32(),
	WebSocket_t62038747::get_offset_of__proxyCredentials_33(),
	WebSocket_t62038747::get_offset_of__proxyUri_34(),
	WebSocket_t62038747::get_offset_of__readyState_35(),
	WebSocket_t62038747::get_offset_of__receivePong_36(),
	WebSocket_t62038747::get_offset_of__secure_37(),
	WebSocket_t62038747::get_offset_of__sslConfig_38(),
	WebSocket_t62038747::get_offset_of__stream_39(),
	WebSocket_t62038747::get_offset_of__tcpClient_40(),
	WebSocket_t62038747::get_offset_of__uri_41(),
	0,
	WebSocket_t62038747::get_offset_of__waitTime_43(),
	WebSocket_t62038747_StaticFields::get_offset_of_EmptyBytes_44(),
	WebSocket_t62038747_StaticFields::get_offset_of_FragmentLength_45(),
	WebSocket_t62038747_StaticFields::get_offset_of_RandomNumber_46(),
	WebSocket_t62038747::get_offset_of_OnClose_47(),
	WebSocket_t62038747::get_offset_of_OnError_48(),
	WebSocket_t62038747::get_offset_of_OnMessage_49(),
	WebSocket_t62038747::get_offset_of_OnOpen_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (U3CU3Ec__Iterator0_t2887482940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2374[8] = 
{
	U3CU3Ec__Iterator0_t2887482940::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t2887482940::get_offset_of_U24locvar1_1(),
	U3CU3Ec__Iterator0_t2887482940::get_offset_of_U3CcookieU3E__1_2(),
	U3CU3Ec__Iterator0_t2887482940::get_offset_of_U24locvar2_3(),
	U3CU3Ec__Iterator0_t2887482940::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator0_t2887482940::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator0_t2887482940::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator0_t2887482940::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (U3CcloseAsyncU3Ec__AnonStorey1_t2051387318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[1] = 
{
	U3CcloseAsyncU3Ec__AnonStorey1_t2051387318::get_offset_of_closer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (U3CmessagesU3Ec__AnonStorey2_t330977561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[2] = 
{
	U3CmessagesU3Ec__AnonStorey2_t330977561::get_offset_of_e_0(),
	U3CmessagesU3Ec__AnonStorey2_t330977561::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (U3CsendAsyncU3Ec__AnonStorey3_t3015274107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2377[3] = 
{
	U3CsendAsyncU3Ec__AnonStorey3_t3015274107::get_offset_of_sender_0(),
	U3CsendAsyncU3Ec__AnonStorey3_t3015274107::get_offset_of_completed_1(),
	U3CsendAsyncU3Ec__AnonStorey3_t3015274107::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (U3CstartReceivingU3Ec__AnonStorey4_t3314047616), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[2] = 
{
	U3CstartReceivingU3Ec__AnonStorey4_t3314047616::get_offset_of_receive_0(),
	U3CstartReceivingU3Ec__AnonStorey4_t3314047616::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t349889031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2379[1] = 
{
	U3CvalidateSecWebSocketExtensionsServerHeaderU3Ec__AnonStorey5_t349889031::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t2419708675), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2380[1] = 
{
	U3CvalidateSecWebSocketProtocolServerHeaderU3Ec__AnonStorey6_t2419708675::get_offset_of_value_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (U3CAcceptAsyncU3Ec__AnonStorey7_t3533643647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2381[2] = 
{
	U3CAcceptAsyncU3Ec__AnonStorey7_t3533643647::get_offset_of_acceptor_0(),
	U3CAcceptAsyncU3Ec__AnonStorey7_t3533643647::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (U3CConnectAsyncU3Ec__AnonStorey8_t2980504460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2382[2] = 
{
	U3CConnectAsyncU3Ec__AnonStorey8_t2980504460::get_offset_of_connector_0(),
	U3CConnectAsyncU3Ec__AnonStorey8_t2980504460::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (U3CSendAsyncU3Ec__AnonStorey9_t2988803146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[3] = 
{
	U3CSendAsyncU3Ec__AnonStorey9_t2988803146::get_offset_of_length_0(),
	U3CSendAsyncU3Ec__AnonStorey9_t2988803146::get_offset_of_completed_1(),
	U3CSendAsyncU3Ec__AnonStorey9_t2988803146::get_offset_of_U24this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { sizeof (WebSocketException_t618477455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2384[1] = 
{
	WebSocketException_t618477455::get_offset_of__code_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (WebSocketFrame_t3926438742), -1, sizeof(WebSocketFrame_t3926438742_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2385[11] = 
{
	WebSocketFrame_t3926438742::get_offset_of__extPayloadLength_0(),
	WebSocketFrame_t3926438742::get_offset_of__fin_1(),
	WebSocketFrame_t3926438742::get_offset_of__mask_2(),
	WebSocketFrame_t3926438742::get_offset_of__maskingKey_3(),
	WebSocketFrame_t3926438742::get_offset_of__opcode_4(),
	WebSocketFrame_t3926438742::get_offset_of__payloadData_5(),
	WebSocketFrame_t3926438742::get_offset_of__payloadLength_6(),
	WebSocketFrame_t3926438742::get_offset_of__rsv1_7(),
	WebSocketFrame_t3926438742::get_offset_of__rsv2_8(),
	WebSocketFrame_t3926438742::get_offset_of__rsv3_9(),
	WebSocketFrame_t3926438742_StaticFields::get_offset_of_EmptyPingBytes_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (U3CdumpU3Ec__AnonStorey1_t3297801281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2386[2] = 
{
	U3CdumpU3Ec__AnonStorey1_t3297801281::get_offset_of_output_0(),
	U3CdumpU3Ec__AnonStorey1_t3297801281::get_offset_of_lineFmt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (U3CdumpU3Ec__AnonStorey2_t2749028009), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2387[2] = 
{
	U3CdumpU3Ec__AnonStorey2_t2749028009::get_offset_of_lineCnt_0(),
	U3CdumpU3Ec__AnonStorey2_t2749028009::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t74541281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[3] = 
{
	U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t74541281::get_offset_of_len_0(),
	U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t74541281::get_offset_of_frame_1(),
	U3CreadExtendedPayloadLengthAsyncU3Ec__AnonStorey3_t74541281::get_offset_of_completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (U3CreadHeaderAsyncU3Ec__AnonStorey4_t243395687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[1] = 
{
	U3CreadHeaderAsyncU3Ec__AnonStorey4_t243395687::get_offset_of_completed_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3831656906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[3] = 
{
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3831656906::get_offset_of_len_0(),
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3831656906::get_offset_of_frame_1(),
	U3CreadMaskingKeyAsyncU3Ec__AnonStorey5_t3831656906::get_offset_of_completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t2972632382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[3] = 
{
	U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t2972632382::get_offset_of_llen_0(),
	U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t2972632382::get_offset_of_frame_1(),
	U3CreadPayloadDataAsyncU3Ec__AnonStorey6_t2972632382::get_offset_of_completed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (U3CReadFrameAsyncU3Ec__AnonStorey7_t2345772275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[4] = 
{
	U3CReadFrameAsyncU3Ec__AnonStorey7_t2345772275::get_offset_of_stream_0(),
	U3CReadFrameAsyncU3Ec__AnonStorey7_t2345772275::get_offset_of_error_1(),
	U3CReadFrameAsyncU3Ec__AnonStorey7_t2345772275::get_offset_of_unmask_2(),
	U3CReadFrameAsyncU3Ec__AnonStorey7_t2345772275::get_offset_of_completed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t3460275121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[7] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t3460275121::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t3460275121::get_offset_of_U24locvar1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t3460275121::get_offset_of_U3CbU3E__1_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t3460275121::get_offset_of_U24this_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t3460275121::get_offset_of_U24current_4(),
	U3CGetEnumeratorU3Ec__Iterator0_t3460275121::get_offset_of_U24disposing_5(),
	U3CGetEnumeratorU3Ec__Iterator0_t3460275121::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (WebSocketState_t45461673)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2394[5] = 
{
	WebSocketState_t45461673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (WebSocketTrigger_t496382349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[6] = 
{
	WebSocketTrigger_t496382349::get_offset_of__cancelPingPong_8(),
	WebSocketTrigger_t496382349::get_offset_of__onRecv_9(),
	WebSocketTrigger_t496382349::get_offset_of_U3CWebSocketU3Ek__BackingField_10(),
	WebSocketTrigger_t496382349::get_offset_of_U3CLastWebSocketErrorU3Ek__BackingField_11(),
	WebSocketTrigger_t496382349::get_offset_of_U3CIsProbedU3Ek__BackingField_12(),
	WebSocketTrigger_t496382349::get_offset_of_U3CIsUpgradedU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (WebSocketWrapper_t3330088300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[5] = 
{
	WebSocketWrapper_t3330088300::get_offset_of_U3CUrlU3Ek__BackingField_0(),
	WebSocketWrapper_t3330088300::get_offset_of__webSocket_1(),
	WebSocketWrapper_t3330088300::get_offset_of__messages_2(),
	WebSocketWrapper_t3330088300::get_offset_of__errors_3(),
	WebSocketWrapper_t3330088300::get_offset_of__recvLock_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (Sample01_ObservableWWW_t2301706502), -1, sizeof(Sample01_ObservableWWW_t2301706502_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2397[8] = 
{
	Sample01_ObservableWWW_t2301706502_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	Sample01_ObservableWWW_t2301706502_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
	Sample01_ObservableWWW_t2301706502_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	Sample01_ObservableWWW_t2301706502_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
	Sample01_ObservableWWW_t2301706502_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_6(),
	Sample01_ObservableWWW_t2301706502_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_7(),
	Sample01_ObservableWWW_t2301706502_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_8(),
	Sample01_ObservableWWW_t2301706502_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (Sample02_ObservableTriggers_t1329218277), -1, sizeof(Sample02_ObservableTriggers_t1329218277_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2398[2] = 
{
	Sample02_ObservableTriggers_t1329218277_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	Sample02_ObservableTriggers_t1329218277_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (Sample04_ConvertFromUnityCallback_t972102556), -1, sizeof(Sample04_ConvertFromUnityCallback_t972102556_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2399[4] = 
{
	Sample04_ConvertFromUnityCallback_t972102556_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	Sample04_ConvertFromUnityCallback_t972102556_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
	Sample04_ConvertFromUnityCallback_t972102556_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	Sample04_ConvertFromUnityCallback_t972102556_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
