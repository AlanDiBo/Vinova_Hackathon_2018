﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t1185869587;
// System.IDisposable
struct IDisposable_t3640265483;
// UniRx.IScheduler
struct IScheduler_t411218504;
// UniRx.Operators.WhenAllObservable/WhenAll_
struct WhenAll__t3629299119;
// UniRx.Operators.WhenAllObservable/WhenAll
struct WhenAll_t3625170351;
// UniRx.Operators.TimerObservable/Timer
struct Timer_t623737574;
// UniRx.Operators.TimerObservable
struct TimerObservable_t2723123263;
// UniRx.ISchedulerPeriodic
struct ISchedulerPeriodic_t1846564281;
// UniRx.SerialDisposable
struct SerialDisposable_t3525249344;
// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey0
struct U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993;
// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey1
struct U3CSubscribeCoreU3Ec__AnonStorey1_t3331905994;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t811551750;
// UniRx.IObservable`1<UniRx.Unit>[]
struct IObservable_1U5BU5D_t3038720744;
// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<UniRx.Unit>>
struct IEnumerable_1_t465234494;
// System.Collections.Generic.IList`1<UniRx.IObservable`1<UniRx.Unit>>
struct IList_1_t3300701388;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[]
struct Int32U5BU5D_t385246372;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef OPERATOROBSERVABLEBASE_1_T729037393_H
#define OPERATOROBSERVABLEBASE_1_T729037393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<System.Int64>
struct  OperatorObservableBase_1_t729037393  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t729037393, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T729037393_H
#ifndef OPERATOROBSERVERBASE_2_T431626514_H
#define OPERATOROBSERVERBASE_2_T431626514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObserverBase`2<System.Int64,System.Int64>
struct  OperatorObserverBase_2_t431626514  : public RuntimeObject
{
public:
	// UniRx.IObserver`1<TResult> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.OperatorObserverBase`2::observer
	RuntimeObject* ___observer_0;
	// System.IDisposable UniRx.Operators.OperatorObserverBase`2::cancel
	RuntimeObject* ___cancel_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t431626514, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((&___observer_0), value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t431626514, ___cancel_1)); }
	inline RuntimeObject* get_cancel_1() const { return ___cancel_1; }
	inline RuntimeObject** get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(RuntimeObject* value)
	{
		___cancel_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancel_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVERBASE_2_T431626514_H
#ifndef SCHEDULER_T1572131957_H
#define SCHEDULER_T1572131957_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Scheduler
struct  Scheduler_t1572131957  : public RuntimeObject
{
public:

public:
};

struct Scheduler_t1572131957_StaticFields
{
public:
	// UniRx.IScheduler UniRx.Scheduler::CurrentThread
	RuntimeObject* ___CurrentThread_0;
	// UniRx.IScheduler UniRx.Scheduler::Immediate
	RuntimeObject* ___Immediate_1;
	// UniRx.IScheduler UniRx.Scheduler::ThreadPool
	RuntimeObject* ___ThreadPool_2;
	// UniRx.IScheduler UniRx.Scheduler::mainThread
	RuntimeObject* ___mainThread_3;
	// UniRx.IScheduler UniRx.Scheduler::mainThreadIgnoreTimeScale
	RuntimeObject* ___mainThreadIgnoreTimeScale_4;
	// UniRx.IScheduler UniRx.Scheduler::mainThreadFixedUpdate
	RuntimeObject* ___mainThreadFixedUpdate_5;
	// UniRx.IScheduler UniRx.Scheduler::mainThreadEndOfFrame
	RuntimeObject* ___mainThreadEndOfFrame_6;

public:
	inline static int32_t get_offset_of_CurrentThread_0() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___CurrentThread_0)); }
	inline RuntimeObject* get_CurrentThread_0() const { return ___CurrentThread_0; }
	inline RuntimeObject** get_address_of_CurrentThread_0() { return &___CurrentThread_0; }
	inline void set_CurrentThread_0(RuntimeObject* value)
	{
		___CurrentThread_0 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentThread_0), value);
	}

	inline static int32_t get_offset_of_Immediate_1() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___Immediate_1)); }
	inline RuntimeObject* get_Immediate_1() const { return ___Immediate_1; }
	inline RuntimeObject** get_address_of_Immediate_1() { return &___Immediate_1; }
	inline void set_Immediate_1(RuntimeObject* value)
	{
		___Immediate_1 = value;
		Il2CppCodeGenWriteBarrier((&___Immediate_1), value);
	}

	inline static int32_t get_offset_of_ThreadPool_2() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___ThreadPool_2)); }
	inline RuntimeObject* get_ThreadPool_2() const { return ___ThreadPool_2; }
	inline RuntimeObject** get_address_of_ThreadPool_2() { return &___ThreadPool_2; }
	inline void set_ThreadPool_2(RuntimeObject* value)
	{
		___ThreadPool_2 = value;
		Il2CppCodeGenWriteBarrier((&___ThreadPool_2), value);
	}

	inline static int32_t get_offset_of_mainThread_3() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___mainThread_3)); }
	inline RuntimeObject* get_mainThread_3() const { return ___mainThread_3; }
	inline RuntimeObject** get_address_of_mainThread_3() { return &___mainThread_3; }
	inline void set_mainThread_3(RuntimeObject* value)
	{
		___mainThread_3 = value;
		Il2CppCodeGenWriteBarrier((&___mainThread_3), value);
	}

	inline static int32_t get_offset_of_mainThreadIgnoreTimeScale_4() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___mainThreadIgnoreTimeScale_4)); }
	inline RuntimeObject* get_mainThreadIgnoreTimeScale_4() const { return ___mainThreadIgnoreTimeScale_4; }
	inline RuntimeObject** get_address_of_mainThreadIgnoreTimeScale_4() { return &___mainThreadIgnoreTimeScale_4; }
	inline void set_mainThreadIgnoreTimeScale_4(RuntimeObject* value)
	{
		___mainThreadIgnoreTimeScale_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainThreadIgnoreTimeScale_4), value);
	}

	inline static int32_t get_offset_of_mainThreadFixedUpdate_5() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___mainThreadFixedUpdate_5)); }
	inline RuntimeObject* get_mainThreadFixedUpdate_5() const { return ___mainThreadFixedUpdate_5; }
	inline RuntimeObject** get_address_of_mainThreadFixedUpdate_5() { return &___mainThreadFixedUpdate_5; }
	inline void set_mainThreadFixedUpdate_5(RuntimeObject* value)
	{
		___mainThreadFixedUpdate_5 = value;
		Il2CppCodeGenWriteBarrier((&___mainThreadFixedUpdate_5), value);
	}

	inline static int32_t get_offset_of_mainThreadEndOfFrame_6() { return static_cast<int32_t>(offsetof(Scheduler_t1572131957_StaticFields, ___mainThreadEndOfFrame_6)); }
	inline RuntimeObject* get_mainThreadEndOfFrame_6() const { return ___mainThreadEndOfFrame_6; }
	inline RuntimeObject** get_address_of_mainThreadEndOfFrame_6() { return &___mainThreadEndOfFrame_6; }
	inline void set_mainThreadEndOfFrame_6(RuntimeObject* value)
	{
		___mainThreadEndOfFrame_6 = value;
		Il2CppCodeGenWriteBarrier((&___mainThreadEndOfFrame_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEDULER_T1572131957_H
#ifndef WHENALLCOLLECTIONOBSERVER_T3221558212_H
#define WHENALLCOLLECTIONOBSERVER_T3221558212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WhenAllObservable/WhenAll_/WhenAllCollectionObserver
struct  WhenAllCollectionObserver_t3221558212  : public RuntimeObject
{
public:
	// UniRx.Operators.WhenAllObservable/WhenAll_ UniRx.Operators.WhenAllObservable/WhenAll_/WhenAllCollectionObserver::parent
	WhenAll__t3629299119 * ___parent_0;
	// System.Boolean UniRx.Operators.WhenAllObservable/WhenAll_/WhenAllCollectionObserver::isCompleted
	bool ___isCompleted_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(WhenAllCollectionObserver_t3221558212, ___parent_0)); }
	inline WhenAll__t3629299119 * get_parent_0() const { return ___parent_0; }
	inline WhenAll__t3629299119 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(WhenAll__t3629299119 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_isCompleted_1() { return static_cast<int32_t>(offsetof(WhenAllCollectionObserver_t3221558212, ___isCompleted_1)); }
	inline bool get_isCompleted_1() const { return ___isCompleted_1; }
	inline bool* get_address_of_isCompleted_1() { return &___isCompleted_1; }
	inline void set_isCompleted_1(bool value)
	{
		___isCompleted_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHENALLCOLLECTIONOBSERVER_T3221558212_H
#ifndef WHENALLCOLLECTIONOBSERVER_T535337770_H
#define WHENALLCOLLECTIONOBSERVER_T535337770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WhenAllObservable/WhenAll/WhenAllCollectionObserver
struct  WhenAllCollectionObserver_t535337770  : public RuntimeObject
{
public:
	// UniRx.Operators.WhenAllObservable/WhenAll UniRx.Operators.WhenAllObservable/WhenAll/WhenAllCollectionObserver::parent
	WhenAll_t3625170351 * ___parent_0;
	// System.Boolean UniRx.Operators.WhenAllObservable/WhenAll/WhenAllCollectionObserver::isCompleted
	bool ___isCompleted_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(WhenAllCollectionObserver_t535337770, ___parent_0)); }
	inline WhenAll_t3625170351 * get_parent_0() const { return ___parent_0; }
	inline WhenAll_t3625170351 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(WhenAll_t3625170351 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_isCompleted_1() { return static_cast<int32_t>(offsetof(WhenAllCollectionObserver_t535337770, ___isCompleted_1)); }
	inline bool get_isCompleted_1() const { return ___isCompleted_1; }
	inline bool* get_address_of_isCompleted_1() { return &___isCompleted_1; }
	inline void set_isCompleted_1(bool value)
	{
		___isCompleted_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHENALLCOLLECTIONOBSERVER_T535337770_H
#ifndef OPERATOROBSERVABLEBASE_1_T354719556_H
#define OPERATOROBSERVABLEBASE_1_T354719556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>
struct  OperatorObservableBase_1_t354719556  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t354719556, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T354719556_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CSUBSCRIBECOREU3EC__ANONSTOREY0_T3331905993_H
#define U3CSUBSCRIBECOREU3EC__ANONSTOREY0_T3331905993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey0
struct  U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993  : public RuntimeObject
{
public:
	// UniRx.Operators.TimerObservable/Timer UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey0::timerObserver
	Timer_t623737574 * ___timerObserver_0;
	// UniRx.Operators.TimerObservable UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey0::$this
	TimerObservable_t2723123263 * ___U24this_1;

public:
	inline static int32_t get_offset_of_timerObserver_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993, ___timerObserver_0)); }
	inline Timer_t623737574 * get_timerObserver_0() const { return ___timerObserver_0; }
	inline Timer_t623737574 ** get_address_of_timerObserver_0() { return &___timerObserver_0; }
	inline void set_timerObserver_0(Timer_t623737574 * value)
	{
		___timerObserver_0 = value;
		Il2CppCodeGenWriteBarrier((&___timerObserver_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993, ___U24this_1)); }
	inline TimerObservable_t2723123263 * get_U24this_1() const { return ___U24this_1; }
	inline TimerObservable_t2723123263 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TimerObservable_t2723123263 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSUBSCRIBECOREU3EC__ANONSTOREY0_T3331905993_H
#ifndef U3CSUBSCRIBECOREU3EC__ANONSTOREY1_T3331905994_H
#define U3CSUBSCRIBECOREU3EC__ANONSTOREY1_T3331905994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey1
struct  U3CSubscribeCoreU3Ec__AnonStorey1_t3331905994  : public RuntimeObject
{
public:
	// UniRx.ISchedulerPeriodic UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey1::periodicScheduler
	RuntimeObject* ___periodicScheduler_0;

public:
	inline static int32_t get_offset_of_periodicScheduler_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey1_t3331905994, ___periodicScheduler_0)); }
	inline RuntimeObject* get_periodicScheduler_0() const { return ___periodicScheduler_0; }
	inline RuntimeObject** get_address_of_periodicScheduler_0() { return &___periodicScheduler_0; }
	inline void set_periodicScheduler_0(RuntimeObject* value)
	{
		___periodicScheduler_0 = value;
		Il2CppCodeGenWriteBarrier((&___periodicScheduler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSUBSCRIBECOREU3EC__ANONSTOREY1_T3331905994_H
#ifndef U3CSUBSCRIBECOREU3EC__ANONSTOREY2_T3331905991_H
#define U3CSUBSCRIBECOREU3EC__ANONSTOREY2_T3331905991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey2
struct  U3CSubscribeCoreU3Ec__AnonStorey2_t3331905991  : public RuntimeObject
{
public:
	// UniRx.SerialDisposable UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey2::disposable
	SerialDisposable_t3525249344 * ___disposable_0;
	// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey0 UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey2::<>f__ref$0
	U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993 * ___U3CU3Ef__refU240_1;
	// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey1 UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey2::<>f__ref$1
	U3CSubscribeCoreU3Ec__AnonStorey1_t3331905994 * ___U3CU3Ef__refU241_2;

public:
	inline static int32_t get_offset_of_disposable_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey2_t3331905991, ___disposable_0)); }
	inline SerialDisposable_t3525249344 * get_disposable_0() const { return ___disposable_0; }
	inline SerialDisposable_t3525249344 ** get_address_of_disposable_0() { return &___disposable_0; }
	inline void set_disposable_0(SerialDisposable_t3525249344 * value)
	{
		___disposable_0 = value;
		Il2CppCodeGenWriteBarrier((&___disposable_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey2_t3331905991, ___U3CU3Ef__refU240_1)); }
	inline U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_2() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey2_t3331905991, ___U3CU3Ef__refU241_2)); }
	inline U3CSubscribeCoreU3Ec__AnonStorey1_t3331905994 * get_U3CU3Ef__refU241_2() const { return ___U3CU3Ef__refU241_2; }
	inline U3CSubscribeCoreU3Ec__AnonStorey1_t3331905994 ** get_address_of_U3CU3Ef__refU241_2() { return &___U3CU3Ef__refU241_2; }
	inline void set_U3CU3Ef__refU241_2(U3CSubscribeCoreU3Ec__AnonStorey1_t3331905994 * value)
	{
		___U3CU3Ef__refU241_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSUBSCRIBECOREU3EC__ANONSTOREY2_T3331905991_H
#ifndef OPERATOROBSERVERBASE_2_T3781804006_H
#define OPERATOROBSERVERBASE_2_T3781804006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>
struct  OperatorObserverBase_2_t3781804006  : public RuntimeObject
{
public:
	// UniRx.IObserver`1<TResult> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.OperatorObserverBase`2::observer
	RuntimeObject* ___observer_0;
	// System.IDisposable UniRx.Operators.OperatorObserverBase`2::cancel
	RuntimeObject* ___cancel_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t3781804006, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((&___observer_0), value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t3781804006, ___cancel_1)); }
	inline RuntimeObject* get_cancel_1() const { return ___cancel_1; }
	inline RuntimeObject** get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(RuntimeObject* value)
	{
		___cancel_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancel_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVERBASE_2_T3781804006_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef WHENALLOBSERVABLE_T722905145_H
#define WHENALLOBSERVABLE_T722905145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WhenAllObservable
struct  WhenAllObservable_t722905145  : public OperatorObservableBase_1_t354719556
{
public:
	// UniRx.IObservable`1<UniRx.Unit>[] UniRx.Operators.WhenAllObservable::sources
	IObservable_1U5BU5D_t3038720744* ___sources_1;
	// System.Collections.Generic.IEnumerable`1<UniRx.IObservable`1<UniRx.Unit>> UniRx.Operators.WhenAllObservable::sourcesEnumerable
	RuntimeObject* ___sourcesEnumerable_2;

public:
	inline static int32_t get_offset_of_sources_1() { return static_cast<int32_t>(offsetof(WhenAllObservable_t722905145, ___sources_1)); }
	inline IObservable_1U5BU5D_t3038720744* get_sources_1() const { return ___sources_1; }
	inline IObservable_1U5BU5D_t3038720744** get_address_of_sources_1() { return &___sources_1; }
	inline void set_sources_1(IObservable_1U5BU5D_t3038720744* value)
	{
		___sources_1 = value;
		Il2CppCodeGenWriteBarrier((&___sources_1), value);
	}

	inline static int32_t get_offset_of_sourcesEnumerable_2() { return static_cast<int32_t>(offsetof(WhenAllObservable_t722905145, ___sourcesEnumerable_2)); }
	inline RuntimeObject* get_sourcesEnumerable_2() const { return ___sourcesEnumerable_2; }
	inline RuntimeObject** get_address_of_sourcesEnumerable_2() { return &___sourcesEnumerable_2; }
	inline void set_sourcesEnumerable_2(RuntimeObject* value)
	{
		___sourcesEnumerable_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourcesEnumerable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHENALLOBSERVABLE_T722905145_H
#ifndef TIMER_T623737574_H
#define TIMER_T623737574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimerObservable/Timer
struct  Timer_t623737574  : public OperatorObserverBase_2_t431626514
{
public:
	// System.Int64 UniRx.Operators.TimerObservable/Timer::index
	int64_t ___index_2;

public:
	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Timer_t623737574, ___index_2)); }
	inline int64_t get_index_2() const { return ___index_2; }
	inline int64_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int64_t value)
	{
		___index_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMER_T623737574_H
#ifndef WHENALL_T3625170351_H
#define WHENALL_T3625170351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WhenAllObservable/WhenAll
struct  WhenAll_t3625170351  : public OperatorObserverBase_2_t3781804006
{
public:
	// UniRx.IObservable`1<UniRx.Unit>[] UniRx.Operators.WhenAllObservable/WhenAll::sources
	IObservable_1U5BU5D_t3038720744* ___sources_2;
	// System.Object UniRx.Operators.WhenAllObservable/WhenAll::gate
	RuntimeObject * ___gate_3;
	// System.Int32 UniRx.Operators.WhenAllObservable/WhenAll::completedCount
	int32_t ___completedCount_4;
	// System.Int32 UniRx.Operators.WhenAllObservable/WhenAll::length
	int32_t ___length_5;

public:
	inline static int32_t get_offset_of_sources_2() { return static_cast<int32_t>(offsetof(WhenAll_t3625170351, ___sources_2)); }
	inline IObservable_1U5BU5D_t3038720744* get_sources_2() const { return ___sources_2; }
	inline IObservable_1U5BU5D_t3038720744** get_address_of_sources_2() { return &___sources_2; }
	inline void set_sources_2(IObservable_1U5BU5D_t3038720744* value)
	{
		___sources_2 = value;
		Il2CppCodeGenWriteBarrier((&___sources_2), value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(WhenAll_t3625170351, ___gate_3)); }
	inline RuntimeObject * get_gate_3() const { return ___gate_3; }
	inline RuntimeObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(RuntimeObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier((&___gate_3), value);
	}

	inline static int32_t get_offset_of_completedCount_4() { return static_cast<int32_t>(offsetof(WhenAll_t3625170351, ___completedCount_4)); }
	inline int32_t get_completedCount_4() const { return ___completedCount_4; }
	inline int32_t* get_address_of_completedCount_4() { return &___completedCount_4; }
	inline void set_completedCount_4(int32_t value)
	{
		___completedCount_4 = value;
	}

	inline static int32_t get_offset_of_length_5() { return static_cast<int32_t>(offsetof(WhenAll_t3625170351, ___length_5)); }
	inline int32_t get_length_5() const { return ___length_5; }
	inline int32_t* get_address_of_length_5() { return &___length_5; }
	inline void set_length_5(int32_t value)
	{
		___length_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHENALL_T3625170351_H
#ifndef WHENALL__T3629299119_H
#define WHENALL__T3629299119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.WhenAllObservable/WhenAll_
struct  WhenAll__t3629299119  : public OperatorObserverBase_2_t3781804006
{
public:
	// System.Collections.Generic.IList`1<UniRx.IObservable`1<UniRx.Unit>> UniRx.Operators.WhenAllObservable/WhenAll_::sources
	RuntimeObject* ___sources_2;
	// System.Object UniRx.Operators.WhenAllObservable/WhenAll_::gate
	RuntimeObject * ___gate_3;
	// System.Int32 UniRx.Operators.WhenAllObservable/WhenAll_::completedCount
	int32_t ___completedCount_4;
	// System.Int32 UniRx.Operators.WhenAllObservable/WhenAll_::length
	int32_t ___length_5;

public:
	inline static int32_t get_offset_of_sources_2() { return static_cast<int32_t>(offsetof(WhenAll__t3629299119, ___sources_2)); }
	inline RuntimeObject* get_sources_2() const { return ___sources_2; }
	inline RuntimeObject** get_address_of_sources_2() { return &___sources_2; }
	inline void set_sources_2(RuntimeObject* value)
	{
		___sources_2 = value;
		Il2CppCodeGenWriteBarrier((&___sources_2), value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(WhenAll__t3629299119, ___gate_3)); }
	inline RuntimeObject * get_gate_3() const { return ___gate_3; }
	inline RuntimeObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(RuntimeObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier((&___gate_3), value);
	}

	inline static int32_t get_offset_of_completedCount_4() { return static_cast<int32_t>(offsetof(WhenAll__t3629299119, ___completedCount_4)); }
	inline int32_t get_completedCount_4() const { return ___completedCount_4; }
	inline int32_t* get_address_of_completedCount_4() { return &___completedCount_4; }
	inline void set_completedCount_4(int32_t value)
	{
		___completedCount_4 = value;
	}

	inline static int32_t get_offset_of_length_5() { return static_cast<int32_t>(offsetof(WhenAll__t3629299119, ___length_5)); }
	inline int32_t get_length_5() const { return ___length_5; }
	inline int32_t* get_address_of_length_5() { return &___length_5; }
	inline void set_length_5(int32_t value)
	{
		___length_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHENALL__T3629299119_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef U3CSUBSCRIBECOREU3EC__ANONSTOREY3_T3331905992_H
#define U3CSUBSCRIBECOREU3EC__ANONSTOREY3_T3331905992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey3
struct  U3CSubscribeCoreU3Ec__AnonStorey3_t3331905992  : public RuntimeObject
{
public:
	// System.TimeSpan UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey3::timeP
	TimeSpan_t881159249  ___timeP_0;
	// UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey0 UniRx.Operators.TimerObservable/<SubscribeCore>c__AnonStorey3::<>f__ref$0
	U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_timeP_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey3_t3331905992, ___timeP_0)); }
	inline TimeSpan_t881159249  get_timeP_0() const { return ___timeP_0; }
	inline TimeSpan_t881159249 * get_address_of_timeP_0() { return &___timeP_0; }
	inline void set_timeP_0(TimeSpan_t881159249  value)
	{
		___timeP_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey3_t3331905992, ___U3CU3Ef__refU240_1)); }
	inline U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSUBSCRIBECOREU3EC__ANONSTOREY3_T3331905992_H
#ifndef NULLABLE_1_T2603721331_H
#define NULLABLE_1_T2603721331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.TimeSpan>
struct  Nullable_1_t2603721331 
{
public:
	// T System.Nullable`1::value
	TimeSpan_t881159249  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2603721331, ___value_0)); }
	inline TimeSpan_t881159249  get_value_0() const { return ___value_0; }
	inline TimeSpan_t881159249 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(TimeSpan_t881159249  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2603721331, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2603721331_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef DATETIMEOFFSET_T3229287507_H
#define DATETIMEOFFSET_T3229287507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeOffset
struct  DateTimeOffset_t3229287507 
{
public:
	// System.DateTime System.DateTimeOffset::dt
	DateTime_t3738529785  ___dt_2;
	// System.TimeSpan System.DateTimeOffset::utc_offset
	TimeSpan_t881159249  ___utc_offset_3;

public:
	inline static int32_t get_offset_of_dt_2() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507, ___dt_2)); }
	inline DateTime_t3738529785  get_dt_2() const { return ___dt_2; }
	inline DateTime_t3738529785 * get_address_of_dt_2() { return &___dt_2; }
	inline void set_dt_2(DateTime_t3738529785  value)
	{
		___dt_2 = value;
	}

	inline static int32_t get_offset_of_utc_offset_3() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507, ___utc_offset_3)); }
	inline TimeSpan_t881159249  get_utc_offset_3() const { return ___utc_offset_3; }
	inline TimeSpan_t881159249 * get_address_of_utc_offset_3() { return &___utc_offset_3; }
	inline void set_utc_offset_3(TimeSpan_t881159249  value)
	{
		___utc_offset_3 = value;
	}
};

struct DateTimeOffset_t3229287507_StaticFields
{
public:
	// System.DateTimeOffset System.DateTimeOffset::MaxValue
	DateTimeOffset_t3229287507  ___MaxValue_0;
	// System.DateTimeOffset System.DateTimeOffset::MinValue
	DateTimeOffset_t3229287507  ___MinValue_1;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507_StaticFields, ___MaxValue_0)); }
	inline DateTimeOffset_t3229287507  get_MaxValue_0() const { return ___MaxValue_0; }
	inline DateTimeOffset_t3229287507 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(DateTimeOffset_t3229287507  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(DateTimeOffset_t3229287507_StaticFields, ___MinValue_1)); }
	inline DateTimeOffset_t3229287507  get_MinValue_1() const { return ___MinValue_1; }
	inline DateTimeOffset_t3229287507 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(DateTimeOffset_t3229287507  value)
	{
		___MinValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOFFSET_T3229287507_H
#ifndef NULLABLE_1_T656882293_H
#define NULLABLE_1_T656882293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTimeOffset>
struct  Nullable_1_t656882293 
{
public:
	// T System.Nullable`1::value
	DateTimeOffset_t3229287507  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t656882293, ___value_0)); }
	inline DateTimeOffset_t3229287507  get_value_0() const { return ___value_0; }
	inline DateTimeOffset_t3229287507 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTimeOffset_t3229287507  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t656882293, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T656882293_H
#ifndef TIMEROBSERVABLE_T2723123263_H
#define TIMEROBSERVABLE_T2723123263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.TimerObservable
struct  TimerObservable_t2723123263  : public OperatorObservableBase_1_t729037393
{
public:
	// System.Nullable`1<System.DateTimeOffset> UniRx.Operators.TimerObservable::dueTimeA
	Nullable_1_t656882293  ___dueTimeA_1;
	// System.Nullable`1<System.TimeSpan> UniRx.Operators.TimerObservable::dueTimeB
	Nullable_1_t2603721331  ___dueTimeB_2;
	// System.Nullable`1<System.TimeSpan> UniRx.Operators.TimerObservable::period
	Nullable_1_t2603721331  ___period_3;
	// UniRx.IScheduler UniRx.Operators.TimerObservable::scheduler
	RuntimeObject* ___scheduler_4;

public:
	inline static int32_t get_offset_of_dueTimeA_1() { return static_cast<int32_t>(offsetof(TimerObservable_t2723123263, ___dueTimeA_1)); }
	inline Nullable_1_t656882293  get_dueTimeA_1() const { return ___dueTimeA_1; }
	inline Nullable_1_t656882293 * get_address_of_dueTimeA_1() { return &___dueTimeA_1; }
	inline void set_dueTimeA_1(Nullable_1_t656882293  value)
	{
		___dueTimeA_1 = value;
	}

	inline static int32_t get_offset_of_dueTimeB_2() { return static_cast<int32_t>(offsetof(TimerObservable_t2723123263, ___dueTimeB_2)); }
	inline Nullable_1_t2603721331  get_dueTimeB_2() const { return ___dueTimeB_2; }
	inline Nullable_1_t2603721331 * get_address_of_dueTimeB_2() { return &___dueTimeB_2; }
	inline void set_dueTimeB_2(Nullable_1_t2603721331  value)
	{
		___dueTimeB_2 = value;
	}

	inline static int32_t get_offset_of_period_3() { return static_cast<int32_t>(offsetof(TimerObservable_t2723123263, ___period_3)); }
	inline Nullable_1_t2603721331  get_period_3() const { return ___period_3; }
	inline Nullable_1_t2603721331 * get_address_of_period_3() { return &___period_3; }
	inline void set_period_3(Nullable_1_t2603721331  value)
	{
		___period_3 = value;
	}

	inline static int32_t get_offset_of_scheduler_4() { return static_cast<int32_t>(offsetof(TimerObservable_t2723123263, ___scheduler_4)); }
	inline RuntimeObject* get_scheduler_4() const { return ___scheduler_4; }
	inline RuntimeObject** get_address_of_scheduler_4() { return &___scheduler_4; }
	inline void set_scheduler_4(RuntimeObject* value)
	{
		___scheduler_4 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEROBSERVABLE_T2723123263_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2800[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2802[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2803[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2805[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2806[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2807[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2808[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2810[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2811[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (TimerObservable_t2723123263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2812[4] = 
{
	TimerObservable_t2723123263::get_offset_of_dueTimeA_1(),
	TimerObservable_t2723123263::get_offset_of_dueTimeB_2(),
	TimerObservable_t2723123263::get_offset_of_period_3(),
	TimerObservable_t2723123263::get_offset_of_scheduler_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { sizeof (Timer_t623737574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2813[1] = 
{
	Timer_t623737574::get_offset_of_index_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { sizeof (U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[2] = 
{
	U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993::get_offset_of_timerObserver_0(),
	U3CSubscribeCoreU3Ec__AnonStorey0_t3331905993::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { sizeof (U3CSubscribeCoreU3Ec__AnonStorey1_t3331905994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[1] = 
{
	U3CSubscribeCoreU3Ec__AnonStorey1_t3331905994::get_offset_of_periodicScheduler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { sizeof (U3CSubscribeCoreU3Ec__AnonStorey2_t3331905991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[3] = 
{
	U3CSubscribeCoreU3Ec__AnonStorey2_t3331905991::get_offset_of_disposable_0(),
	U3CSubscribeCoreU3Ec__AnonStorey2_t3331905991::get_offset_of_U3CU3Ef__refU240_1(),
	U3CSubscribeCoreU3Ec__AnonStorey2_t3331905991::get_offset_of_U3CU3Ef__refU241_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (U3CSubscribeCoreU3Ec__AnonStorey3_t3331905992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2817[2] = 
{
	U3CSubscribeCoreU3Ec__AnonStorey3_t3331905992::get_offset_of_timeP_0(),
	U3CSubscribeCoreU3Ec__AnonStorey3_t3331905992::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2818[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2819[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2820[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2821[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2822[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2823[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2824[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2825[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2826[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2828[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2829[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2830[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2831[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (WhenAllObservable_t722905145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2833[2] = 
{
	WhenAllObservable_t722905145::get_offset_of_sources_1(),
	WhenAllObservable_t722905145::get_offset_of_sourcesEnumerable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (WhenAll_t3625170351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2834[4] = 
{
	WhenAll_t3625170351::get_offset_of_sources_2(),
	WhenAll_t3625170351::get_offset_of_gate_3(),
	WhenAll_t3625170351::get_offset_of_completedCount_4(),
	WhenAll_t3625170351::get_offset_of_length_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (WhenAllCollectionObserver_t535337770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2835[2] = 
{
	WhenAllCollectionObserver_t535337770::get_offset_of_parent_0(),
	WhenAllCollectionObserver_t535337770::get_offset_of_isCompleted_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (WhenAll__t3629299119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2836[4] = 
{
	WhenAll__t3629299119::get_offset_of_sources_2(),
	WhenAll__t3629299119::get_offset_of_gate_3(),
	WhenAll__t3629299119::get_offset_of_completedCount_4(),
	WhenAll__t3629299119::get_offset_of_length_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (WhenAllCollectionObserver_t3221558212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2837[2] = 
{
	WhenAllCollectionObserver_t3221558212::get_offset_of_parent_0(),
	WhenAllCollectionObserver_t3221558212::get_offset_of_isCompleted_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2838[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2839[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2840[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2841[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2842[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2843[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2847[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2853[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2854[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2855[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2857[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2858[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2859[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2860[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2861[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2865[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2867[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2868[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2881[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2882[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2883[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2884[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2885[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2886[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2887[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2888[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2889[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2891[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2892[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2893[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2894[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2896[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2897[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2898[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (Scheduler_t1572131957), -1, sizeof(Scheduler_t1572131957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2899[7] = 
{
	Scheduler_t1572131957_StaticFields::get_offset_of_CurrentThread_0(),
	Scheduler_t1572131957_StaticFields::get_offset_of_Immediate_1(),
	Scheduler_t1572131957_StaticFields::get_offset_of_ThreadPool_2(),
	Scheduler_t1572131957_StaticFields::get_offset_of_mainThread_3(),
	Scheduler_t1572131957_StaticFields::get_offset_of_mainThreadIgnoreTimeScale_4(),
	Scheduler_t1572131957_StaticFields::get_offset_of_mainThreadFixedUpdate_5(),
	Scheduler_t1572131957_StaticFields::get_offset_of_mainThreadEndOfFrame_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
