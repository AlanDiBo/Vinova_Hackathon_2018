﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.Queue`1<System.Action>
struct Queue_1_t1110636971;
// UniRx.CountNotifier
struct CountNotifier_t3944288434;
// UniRx.IMessageBroker
struct IMessageBroker_t2790640530;
// System.Collections.Generic.Dictionary`2<System.Type,System.Object>
struct Dictionary_2_t1229485932;
// UniRx.IAsyncMessageBroker
struct IAsyncMessageBroker_t503716257;
// System.Action
struct Action_t1264377477;
// System.Action`1<System.Exception>
struct Action_1_t1609204844;
// UniRx.Subject`1<UniRx.CountChangedStatus>
struct Subject_1_t3482580559;
// System.Collections.IEnumerator[]
struct IEnumeratorU5BU5D_t2446662267;
// System.Collections.Generic.Queue`1<System.Collections.IEnumerator>
struct Queue_1_t1699543732;
// UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem>
struct PriorityQueue_1_t229995551;
// System.Action`1<System.Object>[]
struct Action_1U5BU5D_t2840438246;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// UniRx.Subject`1<System.Boolean>
struct Subject_1_t185944352;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UniRx.BooleanDisposable
struct BooleanDisposable_t84760918;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASYNCLOCK_T301382185_H
#define ASYNCLOCK_T301382185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.AsyncLock
struct  AsyncLock_t301382185  : public RuntimeObject
{
public:
	// System.Collections.Generic.Queue`1<System.Action> UniRx.InternalUtil.AsyncLock::queue
	Queue_1_t1110636971 * ___queue_0;
	// System.Boolean UniRx.InternalUtil.AsyncLock::isAcquired
	bool ___isAcquired_1;
	// System.Boolean UniRx.InternalUtil.AsyncLock::hasFaulted
	bool ___hasFaulted_2;

public:
	inline static int32_t get_offset_of_queue_0() { return static_cast<int32_t>(offsetof(AsyncLock_t301382185, ___queue_0)); }
	inline Queue_1_t1110636971 * get_queue_0() const { return ___queue_0; }
	inline Queue_1_t1110636971 ** get_address_of_queue_0() { return &___queue_0; }
	inline void set_queue_0(Queue_1_t1110636971 * value)
	{
		___queue_0 = value;
		Il2CppCodeGenWriteBarrier((&___queue_0), value);
	}

	inline static int32_t get_offset_of_isAcquired_1() { return static_cast<int32_t>(offsetof(AsyncLock_t301382185, ___isAcquired_1)); }
	inline bool get_isAcquired_1() const { return ___isAcquired_1; }
	inline bool* get_address_of_isAcquired_1() { return &___isAcquired_1; }
	inline void set_isAcquired_1(bool value)
	{
		___isAcquired_1 = value;
	}

	inline static int32_t get_offset_of_hasFaulted_2() { return static_cast<int32_t>(offsetof(AsyncLock_t301382185, ___hasFaulted_2)); }
	inline bool get_hasFaulted_2() const { return ___hasFaulted_2; }
	inline bool* get_address_of_hasFaulted_2() { return &___hasFaulted_2; }
	inline void set_hasFaulted_2(bool value)
	{
		___hasFaulted_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCLOCK_T301382185_H
#ifndef U3CINCREMENTU3EC__ANONSTOREY0_T2788993591_H
#define U3CINCREMENTU3EC__ANONSTOREY0_T2788993591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CountNotifier/<Increment>c__AnonStorey0
struct  U3CIncrementU3Ec__AnonStorey0_t2788993591  : public RuntimeObject
{
public:
	// System.Int32 UniRx.CountNotifier/<Increment>c__AnonStorey0::incrementCount
	int32_t ___incrementCount_0;
	// UniRx.CountNotifier UniRx.CountNotifier/<Increment>c__AnonStorey0::$this
	CountNotifier_t3944288434 * ___U24this_1;

public:
	inline static int32_t get_offset_of_incrementCount_0() { return static_cast<int32_t>(offsetof(U3CIncrementU3Ec__AnonStorey0_t2788993591, ___incrementCount_0)); }
	inline int32_t get_incrementCount_0() const { return ___incrementCount_0; }
	inline int32_t* get_address_of_incrementCount_0() { return &___incrementCount_0; }
	inline void set_incrementCount_0(int32_t value)
	{
		___incrementCount_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CIncrementU3Ec__AnonStorey0_t2788993591, ___U24this_1)); }
	inline CountNotifier_t3944288434 * get_U24this_1() const { return ___U24this_1; }
	inline CountNotifier_t3944288434 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CountNotifier_t3944288434 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINCREMENTU3EC__ANONSTOREY0_T2788993591_H
#ifndef MESSAGEBROKER_T3452936682_H
#define MESSAGEBROKER_T3452936682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MessageBroker
struct  MessageBroker_t3452936682  : public RuntimeObject
{
public:
	// System.Boolean UniRx.MessageBroker::isDisposed
	bool ___isDisposed_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Object> UniRx.MessageBroker::notifiers
	Dictionary_2_t1229485932 * ___notifiers_2;

public:
	inline static int32_t get_offset_of_isDisposed_1() { return static_cast<int32_t>(offsetof(MessageBroker_t3452936682, ___isDisposed_1)); }
	inline bool get_isDisposed_1() const { return ___isDisposed_1; }
	inline bool* get_address_of_isDisposed_1() { return &___isDisposed_1; }
	inline void set_isDisposed_1(bool value)
	{
		___isDisposed_1 = value;
	}

	inline static int32_t get_offset_of_notifiers_2() { return static_cast<int32_t>(offsetof(MessageBroker_t3452936682, ___notifiers_2)); }
	inline Dictionary_2_t1229485932 * get_notifiers_2() const { return ___notifiers_2; }
	inline Dictionary_2_t1229485932 ** get_address_of_notifiers_2() { return &___notifiers_2; }
	inline void set_notifiers_2(Dictionary_2_t1229485932 * value)
	{
		___notifiers_2 = value;
		Il2CppCodeGenWriteBarrier((&___notifiers_2), value);
	}
};

struct MessageBroker_t3452936682_StaticFields
{
public:
	// UniRx.IMessageBroker UniRx.MessageBroker::Default
	RuntimeObject* ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(MessageBroker_t3452936682_StaticFields, ___Default_0)); }
	inline RuntimeObject* get_Default_0() const { return ___Default_0; }
	inline RuntimeObject** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(RuntimeObject* value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEBROKER_T3452936682_H
#ifndef ASYNCMESSAGEBROKER_T3406290114_H
#define ASYNCMESSAGEBROKER_T3406290114_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AsyncMessageBroker
struct  AsyncMessageBroker_t3406290114  : public RuntimeObject
{
public:
	// System.Boolean UniRx.AsyncMessageBroker::isDisposed
	bool ___isDisposed_1;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Object> UniRx.AsyncMessageBroker::notifiers
	Dictionary_2_t1229485932 * ___notifiers_2;

public:
	inline static int32_t get_offset_of_isDisposed_1() { return static_cast<int32_t>(offsetof(AsyncMessageBroker_t3406290114, ___isDisposed_1)); }
	inline bool get_isDisposed_1() const { return ___isDisposed_1; }
	inline bool* get_address_of_isDisposed_1() { return &___isDisposed_1; }
	inline void set_isDisposed_1(bool value)
	{
		___isDisposed_1 = value;
	}

	inline static int32_t get_offset_of_notifiers_2() { return static_cast<int32_t>(offsetof(AsyncMessageBroker_t3406290114, ___notifiers_2)); }
	inline Dictionary_2_t1229485932 * get_notifiers_2() const { return ___notifiers_2; }
	inline Dictionary_2_t1229485932 ** get_address_of_notifiers_2() { return &___notifiers_2; }
	inline void set_notifiers_2(Dictionary_2_t1229485932 * value)
	{
		___notifiers_2 = value;
		Il2CppCodeGenWriteBarrier((&___notifiers_2), value);
	}
};

struct AsyncMessageBroker_t3406290114_StaticFields
{
public:
	// UniRx.IAsyncMessageBroker UniRx.AsyncMessageBroker::Default
	RuntimeObject* ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(AsyncMessageBroker_t3406290114_StaticFields, ___Default_0)); }
	inline RuntimeObject* get_Default_0() const { return ___Default_0; }
	inline RuntimeObject** get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(RuntimeObject* value)
	{
		___Default_0 = value;
		Il2CppCodeGenWriteBarrier((&___Default_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCMESSAGEBROKER_T3406290114_H
#ifndef OBSERVER_T4122299270_H
#define OBSERVER_T4122299270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observer
struct  Observer_t4122299270  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVER_T4122299270_H
#ifndef OBSERVEREXTENSIONS_T2168473419_H
#define OBSERVEREXTENSIONS_T2168473419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObserverExtensions
struct  ObserverExtensions_t2168473419  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVEREXTENSIONS_T2168473419_H
#ifndef OBSERVABLEEXTENSIONS_T1214079256_H
#define OBSERVABLEEXTENSIONS_T1214079256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableExtensions
struct  ObservableExtensions_t1214079256  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEEXTENSIONS_T1214079256_H
#ifndef STUBS_T4137823816_H
#define STUBS_T4137823816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Stubs
struct  Stubs_t4137823816  : public RuntimeObject
{
public:

public:
};

struct Stubs_t4137823816_StaticFields
{
public:
	// System.Action UniRx.Stubs::Nop
	Action_t1264377477 * ___Nop_0;
	// System.Action`1<System.Exception> UniRx.Stubs::Throw
	Action_1_t1609204844 * ___Throw_1;

public:
	inline static int32_t get_offset_of_Nop_0() { return static_cast<int32_t>(offsetof(Stubs_t4137823816_StaticFields, ___Nop_0)); }
	inline Action_t1264377477 * get_Nop_0() const { return ___Nop_0; }
	inline Action_t1264377477 ** get_address_of_Nop_0() { return &___Nop_0; }
	inline void set_Nop_0(Action_t1264377477 * value)
	{
		___Nop_0 = value;
		Il2CppCodeGenWriteBarrier((&___Nop_0), value);
	}

	inline static int32_t get_offset_of_Throw_1() { return static_cast<int32_t>(offsetof(Stubs_t4137823816_StaticFields, ___Throw_1)); }
	inline Action_1_t1609204844 * get_Throw_1() const { return ___Throw_1; }
	inline Action_1_t1609204844 ** get_address_of_Throw_1() { return &___Throw_1; }
	inline void set_Throw_1(Action_1_t1609204844 * value)
	{
		___Throw_1 = value;
		Il2CppCodeGenWriteBarrier((&___Throw_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STUBS_T4137823816_H
#ifndef COUNTNOTIFIER_T3944288434_H
#define COUNTNOTIFIER_T3944288434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CountNotifier
struct  CountNotifier_t3944288434  : public RuntimeObject
{
public:
	// System.Object UniRx.CountNotifier::lockObject
	RuntimeObject * ___lockObject_0;
	// UniRx.Subject`1<UniRx.CountChangedStatus> UniRx.CountNotifier::statusChanged
	Subject_1_t3482580559 * ___statusChanged_1;
	// System.Int32 UniRx.CountNotifier::max
	int32_t ___max_2;
	// System.Int32 UniRx.CountNotifier::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_lockObject_0() { return static_cast<int32_t>(offsetof(CountNotifier_t3944288434, ___lockObject_0)); }
	inline RuntimeObject * get_lockObject_0() const { return ___lockObject_0; }
	inline RuntimeObject ** get_address_of_lockObject_0() { return &___lockObject_0; }
	inline void set_lockObject_0(RuntimeObject * value)
	{
		___lockObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___lockObject_0), value);
	}

	inline static int32_t get_offset_of_statusChanged_1() { return static_cast<int32_t>(offsetof(CountNotifier_t3944288434, ___statusChanged_1)); }
	inline Subject_1_t3482580559 * get_statusChanged_1() const { return ___statusChanged_1; }
	inline Subject_1_t3482580559 ** get_address_of_statusChanged_1() { return &___statusChanged_1; }
	inline void set_statusChanged_1(Subject_1_t3482580559 * value)
	{
		___statusChanged_1 = value;
		Il2CppCodeGenWriteBarrier((&___statusChanged_1), value);
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(CountNotifier_t3944288434, ___max_2)); }
	inline int32_t get_max_2() const { return ___max_2; }
	inline int32_t* get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(int32_t value)
	{
		___max_2 = value;
	}

	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CountNotifier_t3944288434, ___U3CCountU3Ek__BackingField_3)); }
	inline int32_t get_U3CCountU3Ek__BackingField_3() const { return ___U3CCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_3() { return &___U3CCountU3Ek__BackingField_3; }
	inline void set_U3CCountU3Ek__BackingField_3(int32_t value)
	{
		___U3CCountU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTNOTIFIER_T3944288434_H
#ifndef MICROCOROUTINE_T3506682403_H
#define MICROCOROUTINE_T3506682403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.MicroCoroutine
struct  MicroCoroutine_t3506682403  : public RuntimeObject
{
public:
	// System.Object UniRx.InternalUtil.MicroCoroutine::runningAndQueueLock
	RuntimeObject * ___runningAndQueueLock_1;
	// System.Object UniRx.InternalUtil.MicroCoroutine::arrayLock
	RuntimeObject * ___arrayLock_2;
	// System.Action`1<System.Exception> UniRx.InternalUtil.MicroCoroutine::unhandledExceptionCallback
	Action_1_t1609204844 * ___unhandledExceptionCallback_3;
	// System.Int32 UniRx.InternalUtil.MicroCoroutine::tail
	int32_t ___tail_4;
	// System.Boolean UniRx.InternalUtil.MicroCoroutine::running
	bool ___running_5;
	// System.Collections.IEnumerator[] UniRx.InternalUtil.MicroCoroutine::coroutines
	IEnumeratorU5BU5D_t2446662267* ___coroutines_6;
	// System.Collections.Generic.Queue`1<System.Collections.IEnumerator> UniRx.InternalUtil.MicroCoroutine::waitQueue
	Queue_1_t1699543732 * ___waitQueue_7;

public:
	inline static int32_t get_offset_of_runningAndQueueLock_1() { return static_cast<int32_t>(offsetof(MicroCoroutine_t3506682403, ___runningAndQueueLock_1)); }
	inline RuntimeObject * get_runningAndQueueLock_1() const { return ___runningAndQueueLock_1; }
	inline RuntimeObject ** get_address_of_runningAndQueueLock_1() { return &___runningAndQueueLock_1; }
	inline void set_runningAndQueueLock_1(RuntimeObject * value)
	{
		___runningAndQueueLock_1 = value;
		Il2CppCodeGenWriteBarrier((&___runningAndQueueLock_1), value);
	}

	inline static int32_t get_offset_of_arrayLock_2() { return static_cast<int32_t>(offsetof(MicroCoroutine_t3506682403, ___arrayLock_2)); }
	inline RuntimeObject * get_arrayLock_2() const { return ___arrayLock_2; }
	inline RuntimeObject ** get_address_of_arrayLock_2() { return &___arrayLock_2; }
	inline void set_arrayLock_2(RuntimeObject * value)
	{
		___arrayLock_2 = value;
		Il2CppCodeGenWriteBarrier((&___arrayLock_2), value);
	}

	inline static int32_t get_offset_of_unhandledExceptionCallback_3() { return static_cast<int32_t>(offsetof(MicroCoroutine_t3506682403, ___unhandledExceptionCallback_3)); }
	inline Action_1_t1609204844 * get_unhandledExceptionCallback_3() const { return ___unhandledExceptionCallback_3; }
	inline Action_1_t1609204844 ** get_address_of_unhandledExceptionCallback_3() { return &___unhandledExceptionCallback_3; }
	inline void set_unhandledExceptionCallback_3(Action_1_t1609204844 * value)
	{
		___unhandledExceptionCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___unhandledExceptionCallback_3), value);
	}

	inline static int32_t get_offset_of_tail_4() { return static_cast<int32_t>(offsetof(MicroCoroutine_t3506682403, ___tail_4)); }
	inline int32_t get_tail_4() const { return ___tail_4; }
	inline int32_t* get_address_of_tail_4() { return &___tail_4; }
	inline void set_tail_4(int32_t value)
	{
		___tail_4 = value;
	}

	inline static int32_t get_offset_of_running_5() { return static_cast<int32_t>(offsetof(MicroCoroutine_t3506682403, ___running_5)); }
	inline bool get_running_5() const { return ___running_5; }
	inline bool* get_address_of_running_5() { return &___running_5; }
	inline void set_running_5(bool value)
	{
		___running_5 = value;
	}

	inline static int32_t get_offset_of_coroutines_6() { return static_cast<int32_t>(offsetof(MicroCoroutine_t3506682403, ___coroutines_6)); }
	inline IEnumeratorU5BU5D_t2446662267* get_coroutines_6() const { return ___coroutines_6; }
	inline IEnumeratorU5BU5D_t2446662267** get_address_of_coroutines_6() { return &___coroutines_6; }
	inline void set_coroutines_6(IEnumeratorU5BU5D_t2446662267* value)
	{
		___coroutines_6 = value;
		Il2CppCodeGenWriteBarrier((&___coroutines_6), value);
	}

	inline static int32_t get_offset_of_waitQueue_7() { return static_cast<int32_t>(offsetof(MicroCoroutine_t3506682403, ___waitQueue_7)); }
	inline Queue_1_t1699543732 * get_waitQueue_7() const { return ___waitQueue_7; }
	inline Queue_1_t1699543732 ** get_address_of_waitQueue_7() { return &___waitQueue_7; }
	inline void set_waitQueue_7(Queue_1_t1699543732 * value)
	{
		___waitQueue_7 = value;
		Il2CppCodeGenWriteBarrier((&___waitQueue_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MICROCOROUTINE_T3506682403_H
#ifndef SCHEDULERQUEUE_T1565164295_H
#define SCHEDULERQUEUE_T1565164295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.SchedulerQueue
struct  SchedulerQueue_t1565164295  : public RuntimeObject
{
public:
	// UniRx.InternalUtil.PriorityQueue`1<UniRx.InternalUtil.ScheduledItem> UniRx.InternalUtil.SchedulerQueue::_queue
	PriorityQueue_1_t229995551 * ____queue_0;

public:
	inline static int32_t get_offset_of__queue_0() { return static_cast<int32_t>(offsetof(SchedulerQueue_t1565164295, ____queue_0)); }
	inline PriorityQueue_1_t229995551 * get__queue_0() const { return ____queue_0; }
	inline PriorityQueue_1_t229995551 ** get_address_of__queue_0() { return &____queue_0; }
	inline void set__queue_0(PriorityQueue_1_t229995551 * value)
	{
		____queue_0 = value;
		Il2CppCodeGenWriteBarrier((&____queue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEDULERQUEUE_T1565164295_H
#ifndef THREADSAFEQUEUEWORKER_T2171548691_H
#define THREADSAFEQUEUEWORKER_T2171548691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.ThreadSafeQueueWorker
struct  ThreadSafeQueueWorker_t2171548691  : public RuntimeObject
{
public:
	// System.Object UniRx.InternalUtil.ThreadSafeQueueWorker::gate
	RuntimeObject * ___gate_2;
	// System.Boolean UniRx.InternalUtil.ThreadSafeQueueWorker::dequing
	bool ___dequing_3;
	// System.Int32 UniRx.InternalUtil.ThreadSafeQueueWorker::actionListCount
	int32_t ___actionListCount_4;
	// System.Action`1<System.Object>[] UniRx.InternalUtil.ThreadSafeQueueWorker::actionList
	Action_1U5BU5D_t2840438246* ___actionList_5;
	// System.Object[] UniRx.InternalUtil.ThreadSafeQueueWorker::actionStates
	ObjectU5BU5D_t2843939325* ___actionStates_6;
	// System.Int32 UniRx.InternalUtil.ThreadSafeQueueWorker::waitingListCount
	int32_t ___waitingListCount_7;
	// System.Action`1<System.Object>[] UniRx.InternalUtil.ThreadSafeQueueWorker::waitingList
	Action_1U5BU5D_t2840438246* ___waitingList_8;
	// System.Object[] UniRx.InternalUtil.ThreadSafeQueueWorker::waitingStates
	ObjectU5BU5D_t2843939325* ___waitingStates_9;

public:
	inline static int32_t get_offset_of_gate_2() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t2171548691, ___gate_2)); }
	inline RuntimeObject * get_gate_2() const { return ___gate_2; }
	inline RuntimeObject ** get_address_of_gate_2() { return &___gate_2; }
	inline void set_gate_2(RuntimeObject * value)
	{
		___gate_2 = value;
		Il2CppCodeGenWriteBarrier((&___gate_2), value);
	}

	inline static int32_t get_offset_of_dequing_3() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t2171548691, ___dequing_3)); }
	inline bool get_dequing_3() const { return ___dequing_3; }
	inline bool* get_address_of_dequing_3() { return &___dequing_3; }
	inline void set_dequing_3(bool value)
	{
		___dequing_3 = value;
	}

	inline static int32_t get_offset_of_actionListCount_4() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t2171548691, ___actionListCount_4)); }
	inline int32_t get_actionListCount_4() const { return ___actionListCount_4; }
	inline int32_t* get_address_of_actionListCount_4() { return &___actionListCount_4; }
	inline void set_actionListCount_4(int32_t value)
	{
		___actionListCount_4 = value;
	}

	inline static int32_t get_offset_of_actionList_5() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t2171548691, ___actionList_5)); }
	inline Action_1U5BU5D_t2840438246* get_actionList_5() const { return ___actionList_5; }
	inline Action_1U5BU5D_t2840438246** get_address_of_actionList_5() { return &___actionList_5; }
	inline void set_actionList_5(Action_1U5BU5D_t2840438246* value)
	{
		___actionList_5 = value;
		Il2CppCodeGenWriteBarrier((&___actionList_5), value);
	}

	inline static int32_t get_offset_of_actionStates_6() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t2171548691, ___actionStates_6)); }
	inline ObjectU5BU5D_t2843939325* get_actionStates_6() const { return ___actionStates_6; }
	inline ObjectU5BU5D_t2843939325** get_address_of_actionStates_6() { return &___actionStates_6; }
	inline void set_actionStates_6(ObjectU5BU5D_t2843939325* value)
	{
		___actionStates_6 = value;
		Il2CppCodeGenWriteBarrier((&___actionStates_6), value);
	}

	inline static int32_t get_offset_of_waitingListCount_7() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t2171548691, ___waitingListCount_7)); }
	inline int32_t get_waitingListCount_7() const { return ___waitingListCount_7; }
	inline int32_t* get_address_of_waitingListCount_7() { return &___waitingListCount_7; }
	inline void set_waitingListCount_7(int32_t value)
	{
		___waitingListCount_7 = value;
	}

	inline static int32_t get_offset_of_waitingList_8() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t2171548691, ___waitingList_8)); }
	inline Action_1U5BU5D_t2840438246* get_waitingList_8() const { return ___waitingList_8; }
	inline Action_1U5BU5D_t2840438246** get_address_of_waitingList_8() { return &___waitingList_8; }
	inline void set_waitingList_8(Action_1U5BU5D_t2840438246* value)
	{
		___waitingList_8 = value;
		Il2CppCodeGenWriteBarrier((&___waitingList_8), value);
	}

	inline static int32_t get_offset_of_waitingStates_9() { return static_cast<int32_t>(offsetof(ThreadSafeQueueWorker_t2171548691, ___waitingStates_9)); }
	inline ObjectU5BU5D_t2843939325* get_waitingStates_9() const { return ___waitingStates_9; }
	inline ObjectU5BU5D_t2843939325** get_address_of_waitingStates_9() { return &___waitingStates_9; }
	inline void set_waitingStates_9(ObjectU5BU5D_t2843939325* value)
	{
		___waitingStates_9 = value;
		Il2CppCodeGenWriteBarrier((&___waitingStates_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSAFEQUEUEWORKER_T2171548691_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef NOTIFICATION_T2788794668_H
#define NOTIFICATION_T2788794668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Notification
struct  Notification_t2788794668  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATION_T2788794668_H
#ifndef BOOLEANNOTIFIER_T2404233217_H
#define BOOLEANNOTIFIER_T2404233217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.BooleanNotifier
struct  BooleanNotifier_t2404233217  : public RuntimeObject
{
public:
	// UniRx.Subject`1<System.Boolean> UniRx.BooleanNotifier::boolTrigger
	Subject_1_t185944352 * ___boolTrigger_0;
	// System.Boolean UniRx.BooleanNotifier::boolValue
	bool ___boolValue_1;

public:
	inline static int32_t get_offset_of_boolTrigger_0() { return static_cast<int32_t>(offsetof(BooleanNotifier_t2404233217, ___boolTrigger_0)); }
	inline Subject_1_t185944352 * get_boolTrigger_0() const { return ___boolTrigger_0; }
	inline Subject_1_t185944352 ** get_address_of_boolTrigger_0() { return &___boolTrigger_0; }
	inline void set_boolTrigger_0(Subject_1_t185944352 * value)
	{
		___boolTrigger_0 = value;
		Il2CppCodeGenWriteBarrier((&___boolTrigger_0), value);
	}

	inline static int32_t get_offset_of_boolValue_1() { return static_cast<int32_t>(offsetof(BooleanNotifier_t2404233217, ___boolValue_1)); }
	inline bool get_boolValue_1() const { return ___boolValue_1; }
	inline bool* get_address_of_boolValue_1() { return &___boolValue_1; }
	inline void set_boolValue_1(bool value)
	{
		___boolValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEANNOTIFIER_T2404233217_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef COUNTCHANGEDSTATUS_T3393924172_H
#define COUNTCHANGEDSTATUS_T3393924172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CountChangedStatus
struct  CountChangedStatus_t3393924172 
{
public:
	// System.Int32 UniRx.CountChangedStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CountChangedStatus_t3393924172, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COUNTCHANGEDSTATUS_T3393924172_H
#ifndef NOTIFICATIONKIND_T4115529521_H
#define NOTIFICATIONKIND_T4115529521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.NotificationKind
struct  NotificationKind_t4115529521 
{
public:
	// System.Int32 UniRx.NotificationKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NotificationKind_t4115529521, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONKIND_T4115529521_H
#ifndef SCHEDULEDITEM_T407811980_H
#define SCHEDULEDITEM_T407811980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InternalUtil.ScheduledItem
struct  ScheduledItem_t407811980  : public RuntimeObject
{
public:
	// UniRx.BooleanDisposable UniRx.InternalUtil.ScheduledItem::_disposable
	BooleanDisposable_t84760918 * ____disposable_0;
	// System.TimeSpan UniRx.InternalUtil.ScheduledItem::_dueTime
	TimeSpan_t881159249  ____dueTime_1;
	// System.Action UniRx.InternalUtil.ScheduledItem::_action
	Action_t1264377477 * ____action_2;

public:
	inline static int32_t get_offset_of__disposable_0() { return static_cast<int32_t>(offsetof(ScheduledItem_t407811980, ____disposable_0)); }
	inline BooleanDisposable_t84760918 * get__disposable_0() const { return ____disposable_0; }
	inline BooleanDisposable_t84760918 ** get_address_of__disposable_0() { return &____disposable_0; }
	inline void set__disposable_0(BooleanDisposable_t84760918 * value)
	{
		____disposable_0 = value;
		Il2CppCodeGenWriteBarrier((&____disposable_0), value);
	}

	inline static int32_t get_offset_of__dueTime_1() { return static_cast<int32_t>(offsetof(ScheduledItem_t407811980, ____dueTime_1)); }
	inline TimeSpan_t881159249  get__dueTime_1() const { return ____dueTime_1; }
	inline TimeSpan_t881159249 * get_address_of__dueTime_1() { return &____dueTime_1; }
	inline void set__dueTime_1(TimeSpan_t881159249  value)
	{
		____dueTime_1 = value;
	}

	inline static int32_t get_offset_of__action_2() { return static_cast<int32_t>(offsetof(ScheduledItem_t407811980, ____action_2)); }
	inline Action_t1264377477 * get__action_2() const { return ____action_2; }
	inline Action_t1264377477 ** get_address_of__action_2() { return &____action_2; }
	inline void set__action_2(Action_t1264377477 * value)
	{
		____action_2 = value;
		Il2CppCodeGenWriteBarrier((&____action_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEDULEDITEM_T407811980_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2500[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (AsyncLock_t301382185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[3] = 
{
	AsyncLock_t301382185::get_offset_of_queue_0(),
	AsyncLock_t301382185::get_offset_of_isAcquired_1(),
	AsyncLock_t301382185::get_offset_of_hasFaulted_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2502[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2503[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2504[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2505[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2506[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (MicroCoroutine_t3506682403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[8] = 
{
	0,
	MicroCoroutine_t3506682403::get_offset_of_runningAndQueueLock_1(),
	MicroCoroutine_t3506682403::get_offset_of_arrayLock_2(),
	MicroCoroutine_t3506682403::get_offset_of_unhandledExceptionCallback_3(),
	MicroCoroutine_t3506682403::get_offset_of_tail_4(),
	MicroCoroutine_t3506682403::get_offset_of_running_5(),
	MicroCoroutine_t3506682403::get_offset_of_coroutines_6(),
	MicroCoroutine_t3506682403::get_offset_of_waitQueue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2508[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (ScheduledItem_t407811980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2510[3] = 
{
	ScheduledItem_t407811980::get_offset_of__disposable_0(),
	ScheduledItem_t407811980::get_offset_of__dueTime_1(),
	ScheduledItem_t407811980::get_offset_of__action_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (SchedulerQueue_t1565164295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[1] = 
{
	SchedulerQueue_t1565164295::get_offset_of__queue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (ThreadSafeQueueWorker_t2171548691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[10] = 
{
	0,
	0,
	ThreadSafeQueueWorker_t2171548691::get_offset_of_gate_2(),
	ThreadSafeQueueWorker_t2171548691::get_offset_of_dequing_3(),
	ThreadSafeQueueWorker_t2171548691::get_offset_of_actionListCount_4(),
	ThreadSafeQueueWorker_t2171548691::get_offset_of_actionList_5(),
	ThreadSafeQueueWorker_t2171548691::get_offset_of_actionStates_6(),
	ThreadSafeQueueWorker_t2171548691::get_offset_of_waitingListCount_7(),
	ThreadSafeQueueWorker_t2171548691::get_offset_of_waitingList_8(),
	ThreadSafeQueueWorker_t2171548691::get_offset_of_waitingStates_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { sizeof (NotificationKind_t4115529521)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2514[4] = 
{
	NotificationKind_t4115529521::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2517[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (Notification_t2788794668), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (BooleanNotifier_t2404233217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2522[2] = 
{
	BooleanNotifier_t2404233217::get_offset_of_boolTrigger_0(),
	BooleanNotifier_t2404233217::get_offset_of_boolValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (CountChangedStatus_t3393924172)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2523[5] = 
{
	CountChangedStatus_t3393924172::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (CountNotifier_t3944288434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[4] = 
{
	CountNotifier_t3944288434::get_offset_of_lockObject_0(),
	CountNotifier_t3944288434::get_offset_of_statusChanged_1(),
	CountNotifier_t3944288434::get_offset_of_max_2(),
	CountNotifier_t3944288434::get_offset_of_U3CCountU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (U3CIncrementU3Ec__AnonStorey0_t2788993591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[2] = 
{
	U3CIncrementU3Ec__AnonStorey0_t2788993591::get_offset_of_incrementCount_0(),
	U3CIncrementU3Ec__AnonStorey0_t2788993591::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (MessageBroker_t3452936682), -1, sizeof(MessageBroker_t3452936682_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2532[3] = 
{
	MessageBroker_t3452936682_StaticFields::get_offset_of_Default_0(),
	MessageBroker_t3452936682::get_offset_of_isDisposed_1(),
	MessageBroker_t3452936682::get_offset_of_notifiers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (AsyncMessageBroker_t3406290114), -1, sizeof(AsyncMessageBroker_t3406290114_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2533[3] = 
{
	AsyncMessageBroker_t3406290114_StaticFields::get_offset_of_Default_0(),
	AsyncMessageBroker_t3406290114::get_offset_of_isDisposed_1(),
	AsyncMessageBroker_t3406290114::get_offset_of_notifiers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2534[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2535[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2536[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2537[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (Observer_t4122299270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2540[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2542[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (ObserverExtensions_t2168473419), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (ObservableExtensions_t1214079256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (Stubs_t4137823816), -1, sizeof(Stubs_t4137823816_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2550[2] = 
{
	Stubs_t4137823816_StaticFields::get_offset_of_Nop_0(),
	Stubs_t4137823816_StaticFields::get_offset_of_Throw_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2551[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2554[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2555[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2557[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2558[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2559[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2562[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2563[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2564[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2565[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2567[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2569[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2571[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2572[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2573[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2575[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2576[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2577[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2579[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2582[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2596[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
