﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t811551750;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStorey3
struct U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey3_t1873439977;
// UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStorey5
struct U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey5_t2034475028;
// System.Action`2<System.String,UnityEngine.UI.Text>
struct Action_2_t4024602699;
// System.Action`2<System.Boolean,UnityEngine.UI.Selectable>
struct Action_2_t2693409982;
// System.Func`3<UnityEngine.UI.Toggle,UniRx.IObserver`1<System.Boolean>,System.IDisposable>
struct Func_3_t51310657;
// System.Func`3<UnityEngine.UI.Scrollbar,UniRx.IObserver`1<System.Single>,System.IDisposable>
struct Func_3_t3131458176;
// System.Func`3<UnityEngine.UI.ScrollRect,UniRx.IObserver`1<UnityEngine.Vector2>,System.IDisposable>
struct Func_3_t453008428;
// System.Func`3<UnityEngine.UI.Slider,UniRx.IObserver`1<System.Single>,System.IDisposable>
struct Func_3_t4145638333;
// System.Func`3<UnityEngine.UI.InputField,UniRx.IObserver`1<System.String>,System.IDisposable>
struct Func_3_t455873487;
// UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey1
struct U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey1_t849930747;
// System.Func`2<System.Action,UnityEngine.Events.UnityAction>
struct Func_2_t157146996;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1314943911;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t1634918743;
// UnityEngine.Animator
struct Animator_t434523843;
// System.Func`2<System.Collections.Generic.IList`1<System.Boolean>,System.Boolean>
struct Func_2_t3308430047;
// System.Action`2<UniRx.Unit,UniRx.AsyncReactiveCommand`1<UniRx.Unit>>
struct Action_2_t2753643464;
// System.Action`2<UniRx.Unit,UniRx.ReactiveCommand`1<UniRx.Unit>>
struct Action_2_t4180013452;
// TMPro.Examples.Benchmark01
struct Benchmark01_t1571072624;
// TMPro.Examples.Benchmark01_UGUI
struct Benchmark01_UGUI_t3264177817;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t3450905854;
// System.IDisposable
struct IDisposable_t3640265483;
// UniRx.ReactiveProperty`1<System.Boolean>
struct ReactiveProperty_1_t3626980155;
// System.Action`2<System.Boolean,UniRx.ReactiveProperty`1<System.Boolean>>
struct Action_2_t3070361696;
// UniRx.InternalUtil.ImmutableList`1<System.Func`2<UniRx.Unit,UniRx.IObservable`1<UniRx.Unit>>>
struct ImmutableList_1_t698161389;
// UniRx.IReactiveProperty`1<System.Boolean>
struct IReactiveProperty_1_t1999751154;
// UniRx.IReadOnlyReactiveProperty`1<System.Boolean>
struct IReadOnlyReactiveProperty_1_t2839249823;
// System.Func`3<System.Boolean,System.Boolean,System.Boolean>
struct Func_3_t4257939362;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2>
struct IEqualityComparer_1_t4263561541;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3>
struct IEqualityComparer_1_t1534678186;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector4>
struct IEqualityComparer_1_t1131393659;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Color>
struct IEqualityComparer_1_t368051046;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rect>
struct IEqualityComparer_1_t172844581;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Bounds>
struct IEqualityComparer_1_t79202632;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Quaternion>
struct IEqualityComparer_1_t114293053;
// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo>
struct Subject_1_t4044785546;
// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo>
struct Subject_1_t1228025867;
// UnityEngine.Font
struct Font_t1956802104;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// TMPro.TextContainer
struct TextContainer_t97923372;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// System.String
struct String_t;
// UnityEngine.Material
struct Material_t340375123;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_t845872552;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UniRx.CompositeDisposable
struct CompositeDisposable_t3924054141;
// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData>
struct Subject_1_t3896557479;
// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData>
struct Subject_1_t3991683920;
// UniRx.Subject`1<System.Int32>
struct Subject_1_t3039602140;
// UniRx.Subject`1<UnityEngine.Collision2D>
struct Subject_1_t2931612718;
// UniRx.Subject`1<UnityEngine.Collision>
struct Subject_1_t55769541;
// UniRx.Subject`1<UnityEngine.Collider2D>
struct Subject_1_t2895456013;
// UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData>
struct Subject_1_t2419900039;
// UniRx.Subject`1<UnityEngine.GameObject>
struct Subject_1_t1202293006;
// UniRx.Subject`1<UnityEngine.Collider>
struct Subject_1_t1862003397;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CDIRTYVERTICESCALLBACKASOBSERVABLEU3EC__ANONSTOREY5_T2034475028_H
#define U3CDIRTYVERTICESCALLBACKASOBSERVABLEU3EC__ANONSTOREY5_T2034475028_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStorey5
struct  U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey5_t2034475028  : public RuntimeObject
{
public:
	// UnityEngine.UI.Graphic UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStorey5::graphic
	Graphic_t1660335611 * ___graphic_0;

public:
	inline static int32_t get_offset_of_graphic_0() { return static_cast<int32_t>(offsetof(U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey5_t2034475028, ___graphic_0)); }
	inline Graphic_t1660335611 * get_graphic_0() const { return ___graphic_0; }
	inline Graphic_t1660335611 ** get_address_of_graphic_0() { return &___graphic_0; }
	inline void set_graphic_0(Graphic_t1660335611 * value)
	{
		___graphic_0 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDIRTYVERTICESCALLBACKASOBSERVABLEU3EC__ANONSTOREY5_T2034475028_H
#ifndef U3CDIRTYMATERIALCALLBACKASOBSERVABLEU3EC__ANONSTOREY3_T1873439977_H
#define U3CDIRTYMATERIALCALLBACKASOBSERVABLEU3EC__ANONSTOREY3_T1873439977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStorey3
struct  U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey3_t1873439977  : public RuntimeObject
{
public:
	// UnityEngine.UI.Graphic UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStorey3::graphic
	Graphic_t1660335611 * ___graphic_0;

public:
	inline static int32_t get_offset_of_graphic_0() { return static_cast<int32_t>(offsetof(U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey3_t1873439977, ___graphic_0)); }
	inline Graphic_t1660335611 * get_graphic_0() const { return ___graphic_0; }
	inline Graphic_t1660335611 ** get_address_of_graphic_0() { return &___graphic_0; }
	inline void set_graphic_0(Graphic_t1660335611 * value)
	{
		___graphic_0 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDIRTYMATERIALCALLBACKASOBSERVABLEU3EC__ANONSTOREY3_T1873439977_H
#ifndef U3CDIRTYMATERIALCALLBACKASOBSERVABLEU3EC__ANONSTOREY2_T1705548818_H
#define U3CDIRTYMATERIALCALLBACKASOBSERVABLEU3EC__ANONSTOREY2_T1705548818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStorey3/<DirtyMaterialCallbackAsObservable>c__AnonStorey2
struct  U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey2_t1705548818  : public RuntimeObject
{
public:
	// UniRx.IObserver`1<UniRx.Unit> UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStorey3/<DirtyMaterialCallbackAsObservable>c__AnonStorey2::observer
	RuntimeObject* ___observer_0;
	// UnityEngine.Events.UnityAction UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStorey3/<DirtyMaterialCallbackAsObservable>c__AnonStorey2::registerHandler
	UnityAction_t3245792599 * ___registerHandler_1;
	// UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStorey3 UniRx.UnityGraphicExtensions/<DirtyMaterialCallbackAsObservable>c__AnonStorey3/<DirtyMaterialCallbackAsObservable>c__AnonStorey2::<>f__ref$3
	U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey3_t1873439977 * ___U3CU3Ef__refU243_2;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey2_t1705548818, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((&___observer_0), value);
	}

	inline static int32_t get_offset_of_registerHandler_1() { return static_cast<int32_t>(offsetof(U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey2_t1705548818, ___registerHandler_1)); }
	inline UnityAction_t3245792599 * get_registerHandler_1() const { return ___registerHandler_1; }
	inline UnityAction_t3245792599 ** get_address_of_registerHandler_1() { return &___registerHandler_1; }
	inline void set_registerHandler_1(UnityAction_t3245792599 * value)
	{
		___registerHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___registerHandler_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU243_2() { return static_cast<int32_t>(offsetof(U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey2_t1705548818, ___U3CU3Ef__refU243_2)); }
	inline U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey3_t1873439977 * get_U3CU3Ef__refU243_2() const { return ___U3CU3Ef__refU243_2; }
	inline U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey3_t1873439977 ** get_address_of_U3CU3Ef__refU243_2() { return &___U3CU3Ef__refU243_2; }
	inline void set_U3CU3Ef__refU243_2(U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey3_t1873439977 * value)
	{
		___U3CU3Ef__refU243_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU243_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDIRTYMATERIALCALLBACKASOBSERVABLEU3EC__ANONSTOREY2_T1705548818_H
#ifndef VECTOR4EQUALITYCOMPARER_T1796746215_H
#define VECTOR4EQUALITYCOMPARER_T1796746215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEqualityComparer/Vector4EqualityComparer
struct  Vector4EqualityComparer_t1796746215  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4EQUALITYCOMPARER_T1796746215_H
#ifndef U3CDIRTYVERTICESCALLBACKASOBSERVABLEU3EC__ANONSTOREY4_T3780349813_H
#define U3CDIRTYVERTICESCALLBACKASOBSERVABLEU3EC__ANONSTOREY4_T3780349813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStorey5/<DirtyVerticesCallbackAsObservable>c__AnonStorey4
struct  U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey4_t3780349813  : public RuntimeObject
{
public:
	// UniRx.IObserver`1<UniRx.Unit> UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStorey5/<DirtyVerticesCallbackAsObservable>c__AnonStorey4::observer
	RuntimeObject* ___observer_0;
	// UnityEngine.Events.UnityAction UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStorey5/<DirtyVerticesCallbackAsObservable>c__AnonStorey4::registerHandler
	UnityAction_t3245792599 * ___registerHandler_1;
	// UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStorey5 UniRx.UnityGraphicExtensions/<DirtyVerticesCallbackAsObservable>c__AnonStorey5/<DirtyVerticesCallbackAsObservable>c__AnonStorey4::<>f__ref$5
	U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey5_t2034475028 * ___U3CU3Ef__refU245_2;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey4_t3780349813, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((&___observer_0), value);
	}

	inline static int32_t get_offset_of_registerHandler_1() { return static_cast<int32_t>(offsetof(U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey4_t3780349813, ___registerHandler_1)); }
	inline UnityAction_t3245792599 * get_registerHandler_1() const { return ___registerHandler_1; }
	inline UnityAction_t3245792599 ** get_address_of_registerHandler_1() { return &___registerHandler_1; }
	inline void set_registerHandler_1(UnityAction_t3245792599 * value)
	{
		___registerHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___registerHandler_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU245_2() { return static_cast<int32_t>(offsetof(U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey4_t3780349813, ___U3CU3Ef__refU245_2)); }
	inline U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey5_t2034475028 * get_U3CU3Ef__refU245_2() const { return ___U3CU3Ef__refU245_2; }
	inline U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey5_t2034475028 ** get_address_of_U3CU3Ef__refU245_2() { return &___U3CU3Ef__refU245_2; }
	inline void set_U3CU3Ef__refU245_2(U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey5_t2034475028 * value)
	{
		___U3CU3Ef__refU245_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU245_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDIRTYVERTICESCALLBACKASOBSERVABLEU3EC__ANONSTOREY4_T3780349813_H
#ifndef UNITYUICOMPONENTEXTENSIONS_T484683978_H
#define UNITYUICOMPONENTEXTENSIONS_T484683978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityUIComponentExtensions
struct  UnityUIComponentExtensions_t484683978  : public RuntimeObject
{
public:

public:
};

struct UnityUIComponentExtensions_t484683978_StaticFields
{
public:
	// System.Action`2<System.String,UnityEngine.UI.Text> UniRx.UnityUIComponentExtensions::<>f__am$cache0
	Action_2_t4024602699 * ___U3CU3Ef__amU24cache0_0;
	// System.Action`2<System.Boolean,UnityEngine.UI.Selectable> UniRx.UnityUIComponentExtensions::<>f__am$cache1
	Action_2_t2693409982 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`3<UnityEngine.UI.Toggle,UniRx.IObserver`1<System.Boolean>,System.IDisposable> UniRx.UnityUIComponentExtensions::<>f__am$cache2
	Func_3_t51310657 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`3<UnityEngine.UI.Scrollbar,UniRx.IObserver`1<System.Single>,System.IDisposable> UniRx.UnityUIComponentExtensions::<>f__am$cache3
	Func_3_t3131458176 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`3<UnityEngine.UI.ScrollRect,UniRx.IObserver`1<UnityEngine.Vector2>,System.IDisposable> UniRx.UnityUIComponentExtensions::<>f__am$cache4
	Func_3_t453008428 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`3<UnityEngine.UI.Slider,UniRx.IObserver`1<System.Single>,System.IDisposable> UniRx.UnityUIComponentExtensions::<>f__am$cache5
	Func_3_t4145638333 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`3<UnityEngine.UI.InputField,UniRx.IObserver`1<System.String>,System.IDisposable> UniRx.UnityUIComponentExtensions::<>f__am$cache6
	Func_3_t455873487 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`3<UnityEngine.UI.InputField,UniRx.IObserver`1<System.String>,System.IDisposable> UniRx.UnityUIComponentExtensions::<>f__am$cache7
	Func_3_t455873487 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UnityUIComponentExtensions_t484683978_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Action_2_t4024602699 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Action_2_t4024602699 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Action_2_t4024602699 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(UnityUIComponentExtensions_t484683978_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Action_2_t2693409982 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Action_2_t2693409982 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Action_2_t2693409982 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(UnityUIComponentExtensions_t484683978_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_3_t51310657 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_3_t51310657 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_3_t51310657 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(UnityUIComponentExtensions_t484683978_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_3_t3131458176 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_3_t3131458176 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_3_t3131458176 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(UnityUIComponentExtensions_t484683978_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_3_t453008428 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_3_t453008428 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_3_t453008428 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(UnityUIComponentExtensions_t484683978_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_3_t4145638333 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_3_t4145638333 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_3_t4145638333 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(UnityUIComponentExtensions_t484683978_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_3_t455873487 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_3_t455873487 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_3_t455873487 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(UnityUIComponentExtensions_t484683978_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_3_t455873487 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_3_t455873487 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_3_t455873487 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYUICOMPONENTEXTENSIONS_T484683978_H
#ifndef U3CDIRTYLAYOUTCALLBACKASOBSERVABLEU3EC__ANONSTOREY0_T159790967_H
#define U3CDIRTYLAYOUTCALLBACKASOBSERVABLEU3EC__ANONSTOREY0_T159790967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey1/<DirtyLayoutCallbackAsObservable>c__AnonStorey0
struct  U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey0_t159790967  : public RuntimeObject
{
public:
	// UniRx.IObserver`1<UniRx.Unit> UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey1/<DirtyLayoutCallbackAsObservable>c__AnonStorey0::observer
	RuntimeObject* ___observer_0;
	// UnityEngine.Events.UnityAction UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey1/<DirtyLayoutCallbackAsObservable>c__AnonStorey0::registerHandler
	UnityAction_t3245792599 * ___registerHandler_1;
	// UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey1 UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey1/<DirtyLayoutCallbackAsObservable>c__AnonStorey0::<>f__ref$1
	U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey1_t849930747 * ___U3CU3Ef__refU241_2;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey0_t159790967, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((&___observer_0), value);
	}

	inline static int32_t get_offset_of_registerHandler_1() { return static_cast<int32_t>(offsetof(U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey0_t159790967, ___registerHandler_1)); }
	inline UnityAction_t3245792599 * get_registerHandler_1() const { return ___registerHandler_1; }
	inline UnityAction_t3245792599 ** get_address_of_registerHandler_1() { return &___registerHandler_1; }
	inline void set_registerHandler_1(UnityAction_t3245792599 * value)
	{
		___registerHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___registerHandler_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_2() { return static_cast<int32_t>(offsetof(U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey0_t159790967, ___U3CU3Ef__refU241_2)); }
	inline U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey1_t849930747 * get_U3CU3Ef__refU241_2() const { return ___U3CU3Ef__refU241_2; }
	inline U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey1_t849930747 ** get_address_of_U3CU3Ef__refU241_2() { return &___U3CU3Ef__refU241_2; }
	inline void set_U3CU3Ef__refU241_2(U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey1_t849930747 * value)
	{
		___U3CU3Ef__refU241_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDIRTYLAYOUTCALLBACKASOBSERVABLEU3EC__ANONSTOREY0_T159790967_H
#ifndef RECTEQUALITYCOMPARER_T3798131596_H
#define RECTEQUALITYCOMPARER_T3798131596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEqualityComparer/RectEqualityComparer
struct  RectEqualityComparer_t3798131596  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTEQUALITYCOMPARER_T3798131596_H
#ifndef BOUNDSEQUALITYCOMPARER_T1049822962_H
#define BOUNDSEQUALITYCOMPARER_T1049822962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEqualityComparer/BoundsEqualityComparer
struct  BoundsEqualityComparer_t1049822962  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDSEQUALITYCOMPARER_T1049822962_H
#ifndef QUATERNIONEQUALITYCOMPARER_T2214622964_H
#define QUATERNIONEQUALITYCOMPARER_T2214622964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEqualityComparer/QuaternionEqualityComparer
struct  QuaternionEqualityComparer_t2214622964  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONEQUALITYCOMPARER_T2214622964_H
#ifndef UNITYEVENTEXTENSIONS_T3448704846_H
#define UNITYEVENTEXTENSIONS_T3448704846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions
struct  UnityEventExtensions_t3448704846  : public RuntimeObject
{
public:

public:
};

struct UnityEventExtensions_t3448704846_StaticFields
{
public:
	// System.Func`2<System.Action,UnityEngine.Events.UnityAction> UniRx.UnityEventExtensions::<>f__am$cache0
	Func_2_t157146996 * ___U3CU3Ef__amU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(UnityEventExtensions_t3448704846_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t157146996 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t157146996 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t157146996 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTEXTENSIONS_T3448704846_H
#ifndef U3CASOBSERVABLEU3EC__ANONSTOREY0_T805315905_H
#define U3CASOBSERVABLEU3EC__ANONSTOREY0_T805315905_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey0
struct  U3CAsObservableU3Ec__AnonStorey0_t805315905  : public RuntimeObject
{
public:
	// UnityEngine.Events.UnityEvent UniRx.UnityEventExtensions/<AsObservable>c__AnonStorey0::unityEvent
	UnityEvent_t2581268647 * ___unityEvent_0;

public:
	inline static int32_t get_offset_of_unityEvent_0() { return static_cast<int32_t>(offsetof(U3CAsObservableU3Ec__AnonStorey0_t805315905, ___unityEvent_0)); }
	inline UnityEvent_t2581268647 * get_unityEvent_0() const { return ___unityEvent_0; }
	inline UnityEvent_t2581268647 ** get_address_of_unityEvent_0() { return &___unityEvent_0; }
	inline void set_unityEvent_0(UnityEvent_t2581268647 * value)
	{
		___unityEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___unityEvent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASOBSERVABLEU3EC__ANONSTOREY0_T805315905_H
#ifndef UNITYGRAPHICEXTENSIONS_T1598321454_H
#define UNITYGRAPHICEXTENSIONS_T1598321454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityGraphicExtensions
struct  UnityGraphicExtensions_t1598321454  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYGRAPHICEXTENSIONS_T1598321454_H
#ifndef U3CDIRTYLAYOUTCALLBACKASOBSERVABLEU3EC__ANONSTOREY1_T849930747_H
#define U3CDIRTYLAYOUTCALLBACKASOBSERVABLEU3EC__ANONSTOREY1_T849930747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey1
struct  U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey1_t849930747  : public RuntimeObject
{
public:
	// UnityEngine.UI.Graphic UniRx.UnityGraphicExtensions/<DirtyLayoutCallbackAsObservable>c__AnonStorey1::graphic
	Graphic_t1660335611 * ___graphic_0;

public:
	inline static int32_t get_offset_of_graphic_0() { return static_cast<int32_t>(offsetof(U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey1_t849930747, ___graphic_0)); }
	inline Graphic_t1660335611 * get_graphic_0() const { return ___graphic_0; }
	inline Graphic_t1660335611 ** get_address_of_graphic_0() { return &___graphic_0; }
	inline void set_graphic_0(Graphic_t1660335611 * value)
	{
		___graphic_0 = value;
		Il2CppCodeGenWriteBarrier((&___graphic_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDIRTYLAYOUTCALLBACKASOBSERVABLEU3EC__ANONSTOREY1_T849930747_H
#ifndef YIELDINSTRUCTIONCACHE_T455193914_H
#define YIELDINSTRUCTIONCACHE_T455193914_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.YieldInstructionCache
struct  YieldInstructionCache_t455193914  : public RuntimeObject
{
public:

public:
};

struct YieldInstructionCache_t455193914_StaticFields
{
public:
	// UnityEngine.WaitForEndOfFrame UniRx.YieldInstructionCache::WaitForEndOfFrame
	WaitForEndOfFrame_t1314943911 * ___WaitForEndOfFrame_0;
	// UnityEngine.WaitForFixedUpdate UniRx.YieldInstructionCache::WaitForFixedUpdate
	WaitForFixedUpdate_t1634918743 * ___WaitForFixedUpdate_1;

public:
	inline static int32_t get_offset_of_WaitForEndOfFrame_0() { return static_cast<int32_t>(offsetof(YieldInstructionCache_t455193914_StaticFields, ___WaitForEndOfFrame_0)); }
	inline WaitForEndOfFrame_t1314943911 * get_WaitForEndOfFrame_0() const { return ___WaitForEndOfFrame_0; }
	inline WaitForEndOfFrame_t1314943911 ** get_address_of_WaitForEndOfFrame_0() { return &___WaitForEndOfFrame_0; }
	inline void set_WaitForEndOfFrame_0(WaitForEndOfFrame_t1314943911 * value)
	{
		___WaitForEndOfFrame_0 = value;
		Il2CppCodeGenWriteBarrier((&___WaitForEndOfFrame_0), value);
	}

	inline static int32_t get_offset_of_WaitForFixedUpdate_1() { return static_cast<int32_t>(offsetof(YieldInstructionCache_t455193914_StaticFields, ___WaitForFixedUpdate_1)); }
	inline WaitForFixedUpdate_t1634918743 * get_WaitForFixedUpdate_1() const { return ___WaitForFixedUpdate_1; }
	inline WaitForFixedUpdate_t1634918743 ** get_address_of_WaitForFixedUpdate_1() { return &___WaitForFixedUpdate_1; }
	inline void set_WaitForFixedUpdate_1(WaitForFixedUpdate_t1634918743 * value)
	{
		___WaitForFixedUpdate_1 = value;
		Il2CppCodeGenWriteBarrier((&___WaitForFixedUpdate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // YIELDINSTRUCTIONCACHE_T455193914_H
#ifndef ONSTATEMACHINEINFO_T1139369480_H
#define ONSTATEMACHINEINFO_T1139369480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo
struct  OnStateMachineInfo_t1139369480  : public RuntimeObject
{
public:
	// UnityEngine.Animator UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo::<Animator>k__BackingField
	Animator_t434523843 * ___U3CAnimatorU3Ek__BackingField_0;
	// System.Int32 UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo::<StateMachinePathHash>k__BackingField
	int32_t ___U3CStateMachinePathHashU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CAnimatorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OnStateMachineInfo_t1139369480, ___U3CAnimatorU3Ek__BackingField_0)); }
	inline Animator_t434523843 * get_U3CAnimatorU3Ek__BackingField_0() const { return ___U3CAnimatorU3Ek__BackingField_0; }
	inline Animator_t434523843 ** get_address_of_U3CAnimatorU3Ek__BackingField_0() { return &___U3CAnimatorU3Ek__BackingField_0; }
	inline void set_U3CAnimatorU3Ek__BackingField_0(Animator_t434523843 * value)
	{
		___U3CAnimatorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnimatorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CStateMachinePathHashU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OnStateMachineInfo_t1139369480, ___U3CStateMachinePathHashU3Ek__BackingField_1)); }
	inline int32_t get_U3CStateMachinePathHashU3Ek__BackingField_1() const { return ___U3CStateMachinePathHashU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStateMachinePathHashU3Ek__BackingField_1() { return &___U3CStateMachinePathHashU3Ek__BackingField_1; }
	inline void set_U3CStateMachinePathHashU3Ek__BackingField_1(int32_t value)
	{
		___U3CStateMachinePathHashU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSTATEMACHINEINFO_T1139369480_H
#ifndef REACTIVEPROPERTYEXTENSIONS_T2855915014_H
#define REACTIVEPROPERTYEXTENSIONS_T2855915014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactivePropertyExtensions
struct  ReactivePropertyExtensions_t2855915014  : public RuntimeObject
{
public:

public:
};

struct ReactivePropertyExtensions_t2855915014_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.IList`1<System.Boolean>,System.Boolean> UniRx.ReactivePropertyExtensions::<>f__am$cache0
	Func_2_t3308430047 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<System.Collections.Generic.IList`1<System.Boolean>,System.Boolean> UniRx.ReactivePropertyExtensions::<>f__am$cache1
	Func_2_t3308430047 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(ReactivePropertyExtensions_t2855915014_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t3308430047 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t3308430047 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t3308430047 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(ReactivePropertyExtensions_t2855915014_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t3308430047 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t3308430047 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t3308430047 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTYEXTENSIONS_T2855915014_H
#ifndef REACTIVEDICTIONARYEXTENSIONS_T2909995766_H
#define REACTIVEDICTIONARYEXTENSIONS_T2909995766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveDictionaryExtensions
struct  ReactiveDictionaryExtensions_t2909995766  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEDICTIONARYEXTENSIONS_T2909995766_H
#ifndef ASYNCREACTIVECOMMANDEXTENSIONS_T454737267_H
#define ASYNCREACTIVECOMMANDEXTENSIONS_T454737267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AsyncReactiveCommandExtensions
struct  AsyncReactiveCommandExtensions_t454737267  : public RuntimeObject
{
public:

public:
};

struct AsyncReactiveCommandExtensions_t454737267_StaticFields
{
public:
	// System.Action`2<UniRx.Unit,UniRx.AsyncReactiveCommand`1<UniRx.Unit>> UniRx.AsyncReactiveCommandExtensions::<>f__am$cache0
	Action_2_t2753643464 * ___U3CU3Ef__amU24cache0_0;
	// System.Action`2<UniRx.Unit,UniRx.AsyncReactiveCommand`1<UniRx.Unit>> UniRx.AsyncReactiveCommandExtensions::<>f__am$cache1
	Action_2_t2753643464 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(AsyncReactiveCommandExtensions_t454737267_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Action_2_t2753643464 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Action_2_t2753643464 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Action_2_t2753643464 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(AsyncReactiveCommandExtensions_t454737267_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Action_2_t2753643464 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Action_2_t2753643464 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Action_2_t2753643464 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREACTIVECOMMANDEXTENSIONS_T454737267_H
#ifndef REACTIVECOMMANDEXTENSIONS_T1223375871_H
#define REACTIVECOMMANDEXTENSIONS_T1223375871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveCommandExtensions
struct  ReactiveCommandExtensions_t1223375871  : public RuntimeObject
{
public:

public:
};

struct ReactiveCommandExtensions_t1223375871_StaticFields
{
public:
	// System.Action`2<UniRx.Unit,UniRx.ReactiveCommand`1<UniRx.Unit>> UniRx.ReactiveCommandExtensions::<>f__am$cache0
	Action_2_t4180013452 * ___U3CU3Ef__amU24cache0_0;
	// System.Action`2<UniRx.Unit,UniRx.ReactiveCommand`1<UniRx.Unit>> UniRx.ReactiveCommandExtensions::<>f__am$cache1
	Action_2_t4180013452 * ___U3CU3Ef__amU24cache1_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(ReactiveCommandExtensions_t1223375871_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Action_2_t4180013452 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Action_2_t4180013452 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Action_2_t4180013452 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(ReactiveCommandExtensions_t1223375871_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Action_2_t4180013452 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Action_2_t4180013452 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Action_2_t4180013452 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVECOMMANDEXTENSIONS_T1223375871_H
#ifndef OBSERVABLETRIGGEREXTENSIONS_T3436728118_H
#define OBSERVABLETRIGGEREXTENSIONS_T3436728118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableTriggerExtensions
struct  ObservableTriggerExtensions_t3436728118  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLETRIGGEREXTENSIONS_T3436728118_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2216151886_H
#define U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2216151886  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$this
	Benchmark01_t1571072624 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24this_1)); }
	inline Benchmark01_t1571072624 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_t1571072624 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_t1571072624 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2622988697_H
#define U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2622988697  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01_UGUI TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$this
	Benchmark01_UGUI_t3264177817 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24this_1)); }
	inline Benchmark01_UGUI_t3264177817 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_UGUI_t3264177817 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_UGUI_t3264177817 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifndef REACTIVECOMMAND_1_T1350381949_H
#define REACTIVECOMMAND_1_T1350381949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveCommand`1<UniRx.Unit>
struct  ReactiveCommand_1_t1350381949  : public RuntimeObject
{
public:
	// UniRx.Subject`1<T> UniRx.ReactiveCommand`1::trigger
	Subject_1_t3450905854 * ___trigger_0;
	// System.IDisposable UniRx.ReactiveCommand`1::canExecuteSubscription
	RuntimeObject* ___canExecuteSubscription_1;
	// UniRx.ReactiveProperty`1<System.Boolean> UniRx.ReactiveCommand`1::canExecute
	ReactiveProperty_1_t3626980155 * ___canExecute_2;
	// System.Boolean UniRx.ReactiveCommand`1::<IsDisposed>k__BackingField
	bool ___U3CIsDisposedU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_trigger_0() { return static_cast<int32_t>(offsetof(ReactiveCommand_1_t1350381949, ___trigger_0)); }
	inline Subject_1_t3450905854 * get_trigger_0() const { return ___trigger_0; }
	inline Subject_1_t3450905854 ** get_address_of_trigger_0() { return &___trigger_0; }
	inline void set_trigger_0(Subject_1_t3450905854 * value)
	{
		___trigger_0 = value;
		Il2CppCodeGenWriteBarrier((&___trigger_0), value);
	}

	inline static int32_t get_offset_of_canExecuteSubscription_1() { return static_cast<int32_t>(offsetof(ReactiveCommand_1_t1350381949, ___canExecuteSubscription_1)); }
	inline RuntimeObject* get_canExecuteSubscription_1() const { return ___canExecuteSubscription_1; }
	inline RuntimeObject** get_address_of_canExecuteSubscription_1() { return &___canExecuteSubscription_1; }
	inline void set_canExecuteSubscription_1(RuntimeObject* value)
	{
		___canExecuteSubscription_1 = value;
		Il2CppCodeGenWriteBarrier((&___canExecuteSubscription_1), value);
	}

	inline static int32_t get_offset_of_canExecute_2() { return static_cast<int32_t>(offsetof(ReactiveCommand_1_t1350381949, ___canExecute_2)); }
	inline ReactiveProperty_1_t3626980155 * get_canExecute_2() const { return ___canExecute_2; }
	inline ReactiveProperty_1_t3626980155 ** get_address_of_canExecute_2() { return &___canExecute_2; }
	inline void set_canExecute_2(ReactiveProperty_1_t3626980155 * value)
	{
		___canExecute_2 = value;
		Il2CppCodeGenWriteBarrier((&___canExecute_2), value);
	}

	inline static int32_t get_offset_of_U3CIsDisposedU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ReactiveCommand_1_t1350381949, ___U3CIsDisposedU3Ek__BackingField_3)); }
	inline bool get_U3CIsDisposedU3Ek__BackingField_3() const { return ___U3CIsDisposedU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsDisposedU3Ek__BackingField_3() { return &___U3CIsDisposedU3Ek__BackingField_3; }
	inline void set_U3CIsDisposedU3Ek__BackingField_3(bool value)
	{
		___U3CIsDisposedU3Ek__BackingField_3 = value;
	}
};

struct ReactiveCommand_1_t1350381949_StaticFields
{
public:
	// System.Action`2<System.Boolean,UniRx.ReactiveProperty`1<System.Boolean>> UniRx.ReactiveCommand`1::<>f__am$cache0
	Action_2_t3070361696 * ___U3CU3Ef__amU24cache0_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(ReactiveCommand_1_t1350381949_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Action_2_t3070361696 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Action_2_t3070361696 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Action_2_t3070361696 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVECOMMAND_1_T1350381949_H
#ifndef ASYNCREACTIVECOMMAND_1_T4218979257_H
#define ASYNCREACTIVECOMMAND_1_T4218979257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AsyncReactiveCommand`1<UniRx.Unit>
struct  AsyncReactiveCommand_1_t4218979257  : public RuntimeObject
{
public:
	// UniRx.InternalUtil.ImmutableList`1<System.Func`2<T,UniRx.IObservable`1<UniRx.Unit>>> UniRx.AsyncReactiveCommand`1::asyncActions
	ImmutableList_1_t698161389 * ___asyncActions_0;
	// System.Object UniRx.AsyncReactiveCommand`1::gate
	RuntimeObject * ___gate_1;
	// UniRx.IReactiveProperty`1<System.Boolean> UniRx.AsyncReactiveCommand`1::canExecuteSource
	RuntimeObject* ___canExecuteSource_2;
	// UniRx.IReadOnlyReactiveProperty`1<System.Boolean> UniRx.AsyncReactiveCommand`1::canExecute
	RuntimeObject* ___canExecute_3;
	// System.Boolean UniRx.AsyncReactiveCommand`1::<IsDisposed>k__BackingField
	bool ___U3CIsDisposedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_asyncActions_0() { return static_cast<int32_t>(offsetof(AsyncReactiveCommand_1_t4218979257, ___asyncActions_0)); }
	inline ImmutableList_1_t698161389 * get_asyncActions_0() const { return ___asyncActions_0; }
	inline ImmutableList_1_t698161389 ** get_address_of_asyncActions_0() { return &___asyncActions_0; }
	inline void set_asyncActions_0(ImmutableList_1_t698161389 * value)
	{
		___asyncActions_0 = value;
		Il2CppCodeGenWriteBarrier((&___asyncActions_0), value);
	}

	inline static int32_t get_offset_of_gate_1() { return static_cast<int32_t>(offsetof(AsyncReactiveCommand_1_t4218979257, ___gate_1)); }
	inline RuntimeObject * get_gate_1() const { return ___gate_1; }
	inline RuntimeObject ** get_address_of_gate_1() { return &___gate_1; }
	inline void set_gate_1(RuntimeObject * value)
	{
		___gate_1 = value;
		Il2CppCodeGenWriteBarrier((&___gate_1), value);
	}

	inline static int32_t get_offset_of_canExecuteSource_2() { return static_cast<int32_t>(offsetof(AsyncReactiveCommand_1_t4218979257, ___canExecuteSource_2)); }
	inline RuntimeObject* get_canExecuteSource_2() const { return ___canExecuteSource_2; }
	inline RuntimeObject** get_address_of_canExecuteSource_2() { return &___canExecuteSource_2; }
	inline void set_canExecuteSource_2(RuntimeObject* value)
	{
		___canExecuteSource_2 = value;
		Il2CppCodeGenWriteBarrier((&___canExecuteSource_2), value);
	}

	inline static int32_t get_offset_of_canExecute_3() { return static_cast<int32_t>(offsetof(AsyncReactiveCommand_1_t4218979257, ___canExecute_3)); }
	inline RuntimeObject* get_canExecute_3() const { return ___canExecute_3; }
	inline RuntimeObject** get_address_of_canExecute_3() { return &___canExecute_3; }
	inline void set_canExecute_3(RuntimeObject* value)
	{
		___canExecute_3 = value;
		Il2CppCodeGenWriteBarrier((&___canExecute_3), value);
	}

	inline static int32_t get_offset_of_U3CIsDisposedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AsyncReactiveCommand_1_t4218979257, ___U3CIsDisposedU3Ek__BackingField_4)); }
	inline bool get_U3CIsDisposedU3Ek__BackingField_4() const { return ___U3CIsDisposedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsDisposedU3Ek__BackingField_4() { return &___U3CIsDisposedU3Ek__BackingField_4; }
	inline void set_U3CIsDisposedU3Ek__BackingField_4(bool value)
	{
		___U3CIsDisposedU3Ek__BackingField_4 = value;
	}
};

struct AsyncReactiveCommand_1_t4218979257_StaticFields
{
public:
	// System.Func`3<System.Boolean,System.Boolean,System.Boolean> UniRx.AsyncReactiveCommand`1::<>f__am$cache0
	Func_3_t4257939362 * ___U3CU3Ef__amU24cache0_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(AsyncReactiveCommand_1_t4218979257_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_3_t4257939362 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_3_t4257939362 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_3_t4257939362 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREACTIVECOMMAND_1_T4218979257_H
#ifndef VECTOR3EQUALITYCOMPARER_T4055612966_H
#define VECTOR3EQUALITYCOMPARER_T4055612966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEqualityComparer/Vector3EqualityComparer
struct  Vector3EqualityComparer_t4055612966  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3EQUALITYCOMPARER_T4055612966_H
#ifndef VECTOR2EQUALITYCOMPARER_T3698555085_H
#define VECTOR2EQUALITYCOMPARER_T3698555085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEqualityComparer/Vector2EqualityComparer
struct  Vector2EqualityComparer_t3698555085  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2EQUALITYCOMPARER_T3698555085_H
#ifndef COLOREQUALITYCOMPARER_T1673499971_H
#define COLOREQUALITYCOMPARER_T1673499971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEqualityComparer/ColorEqualityComparer
struct  ColorEqualityComparer_t1673499971  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOREQUALITYCOMPARER_T1673499971_H
#ifndef REACTIVECOMMAND_T2994750062_H
#define REACTIVECOMMAND_T2994750062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveCommand
struct  ReactiveCommand_t2994750062  : public ReactiveCommand_1_t1350381949
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVECOMMAND_T2994750062_H
#ifndef ANIMATORSTATEINFO_T509032636_H
#define ANIMATORSTATEINFO_T509032636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_t509032636 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t509032636, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTATEINFO_T509032636_H
#ifndef ASYNCREACTIVECOMMAND_T3750662548_H
#define ASYNCREACTIVECOMMAND_T3750662548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.AsyncReactiveCommand
struct  AsyncReactiveCommand_t3750662548  : public AsyncReactiveCommand_1_t4218979257
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREACTIVECOMMAND_T3750662548_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef ONSTATEINFO_T3956129159_H
#define ONSTATEINFO_T3956129159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo
struct  OnStateInfo_t3956129159  : public RuntimeObject
{
public:
	// UnityEngine.Animator UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::<Animator>k__BackingField
	Animator_t434523843 * ___U3CAnimatorU3Ek__BackingField_0;
	// UnityEngine.AnimatorStateInfo UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::<StateInfo>k__BackingField
	AnimatorStateInfo_t509032636  ___U3CStateInfoU3Ek__BackingField_1;
	// System.Int32 UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo::<LayerIndex>k__BackingField
	int32_t ___U3CLayerIndexU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CAnimatorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OnStateInfo_t3956129159, ___U3CAnimatorU3Ek__BackingField_0)); }
	inline Animator_t434523843 * get_U3CAnimatorU3Ek__BackingField_0() const { return ___U3CAnimatorU3Ek__BackingField_0; }
	inline Animator_t434523843 ** get_address_of_U3CAnimatorU3Ek__BackingField_0() { return &___U3CAnimatorU3Ek__BackingField_0; }
	inline void set_U3CAnimatorU3Ek__BackingField_0(Animator_t434523843 * value)
	{
		___U3CAnimatorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnimatorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CStateInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OnStateInfo_t3956129159, ___U3CStateInfoU3Ek__BackingField_1)); }
	inline AnimatorStateInfo_t509032636  get_U3CStateInfoU3Ek__BackingField_1() const { return ___U3CStateInfoU3Ek__BackingField_1; }
	inline AnimatorStateInfo_t509032636 * get_address_of_U3CStateInfoU3Ek__BackingField_1() { return &___U3CStateInfoU3Ek__BackingField_1; }
	inline void set_U3CStateInfoU3Ek__BackingField_1(AnimatorStateInfo_t509032636  value)
	{
		___U3CStateInfoU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLayerIndexU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OnStateInfo_t3956129159, ___U3CLayerIndexU3Ek__BackingField_2)); }
	inline int32_t get_U3CLayerIndexU3Ek__BackingField_2() const { return ___U3CLayerIndexU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CLayerIndexU3Ek__BackingField_2() { return &___U3CLayerIndexU3Ek__BackingField_2; }
	inline void set_U3CLayerIndexU3Ek__BackingField_2(int32_t value)
	{
		___U3CLayerIndexU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSTATEINFO_T3956129159_H
#ifndef UNITYEQUALITYCOMPARER_T3411193022_H
#define UNITYEQUALITYCOMPARER_T3411193022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.UnityEqualityComparer
struct  UnityEqualityComparer_t3411193022  : public RuntimeObject
{
public:

public:
};

struct UnityEqualityComparer_t3411193022_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector2> UniRx.UnityEqualityComparer::Vector2
	RuntimeObject* ___Vector2_0;
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector3> UniRx.UnityEqualityComparer::Vector3
	RuntimeObject* ___Vector3_1;
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Vector4> UniRx.UnityEqualityComparer::Vector4
	RuntimeObject* ___Vector4_2;
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Color> UniRx.UnityEqualityComparer::Color
	RuntimeObject* ___Color_3;
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Rect> UniRx.UnityEqualityComparer::Rect
	RuntimeObject* ___Rect_4;
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Bounds> UniRx.UnityEqualityComparer::Bounds
	RuntimeObject* ___Bounds_5;
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Quaternion> UniRx.UnityEqualityComparer::Quaternion
	RuntimeObject* ___Quaternion_6;
	// System.RuntimeTypeHandle UniRx.UnityEqualityComparer::vector2Type
	RuntimeTypeHandle_t3027515415  ___vector2Type_7;
	// System.RuntimeTypeHandle UniRx.UnityEqualityComparer::vector3Type
	RuntimeTypeHandle_t3027515415  ___vector3Type_8;
	// System.RuntimeTypeHandle UniRx.UnityEqualityComparer::vector4Type
	RuntimeTypeHandle_t3027515415  ___vector4Type_9;
	// System.RuntimeTypeHandle UniRx.UnityEqualityComparer::colorType
	RuntimeTypeHandle_t3027515415  ___colorType_10;
	// System.RuntimeTypeHandle UniRx.UnityEqualityComparer::rectType
	RuntimeTypeHandle_t3027515415  ___rectType_11;
	// System.RuntimeTypeHandle UniRx.UnityEqualityComparer::boundsType
	RuntimeTypeHandle_t3027515415  ___boundsType_12;
	// System.RuntimeTypeHandle UniRx.UnityEqualityComparer::quaternionType
	RuntimeTypeHandle_t3027515415  ___quaternionType_13;

public:
	inline static int32_t get_offset_of_Vector2_0() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___Vector2_0)); }
	inline RuntimeObject* get_Vector2_0() const { return ___Vector2_0; }
	inline RuntimeObject** get_address_of_Vector2_0() { return &___Vector2_0; }
	inline void set_Vector2_0(RuntimeObject* value)
	{
		___Vector2_0 = value;
		Il2CppCodeGenWriteBarrier((&___Vector2_0), value);
	}

	inline static int32_t get_offset_of_Vector3_1() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___Vector3_1)); }
	inline RuntimeObject* get_Vector3_1() const { return ___Vector3_1; }
	inline RuntimeObject** get_address_of_Vector3_1() { return &___Vector3_1; }
	inline void set_Vector3_1(RuntimeObject* value)
	{
		___Vector3_1 = value;
		Il2CppCodeGenWriteBarrier((&___Vector3_1), value);
	}

	inline static int32_t get_offset_of_Vector4_2() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___Vector4_2)); }
	inline RuntimeObject* get_Vector4_2() const { return ___Vector4_2; }
	inline RuntimeObject** get_address_of_Vector4_2() { return &___Vector4_2; }
	inline void set_Vector4_2(RuntimeObject* value)
	{
		___Vector4_2 = value;
		Il2CppCodeGenWriteBarrier((&___Vector4_2), value);
	}

	inline static int32_t get_offset_of_Color_3() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___Color_3)); }
	inline RuntimeObject* get_Color_3() const { return ___Color_3; }
	inline RuntimeObject** get_address_of_Color_3() { return &___Color_3; }
	inline void set_Color_3(RuntimeObject* value)
	{
		___Color_3 = value;
		Il2CppCodeGenWriteBarrier((&___Color_3), value);
	}

	inline static int32_t get_offset_of_Rect_4() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___Rect_4)); }
	inline RuntimeObject* get_Rect_4() const { return ___Rect_4; }
	inline RuntimeObject** get_address_of_Rect_4() { return &___Rect_4; }
	inline void set_Rect_4(RuntimeObject* value)
	{
		___Rect_4 = value;
		Il2CppCodeGenWriteBarrier((&___Rect_4), value);
	}

	inline static int32_t get_offset_of_Bounds_5() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___Bounds_5)); }
	inline RuntimeObject* get_Bounds_5() const { return ___Bounds_5; }
	inline RuntimeObject** get_address_of_Bounds_5() { return &___Bounds_5; }
	inline void set_Bounds_5(RuntimeObject* value)
	{
		___Bounds_5 = value;
		Il2CppCodeGenWriteBarrier((&___Bounds_5), value);
	}

	inline static int32_t get_offset_of_Quaternion_6() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___Quaternion_6)); }
	inline RuntimeObject* get_Quaternion_6() const { return ___Quaternion_6; }
	inline RuntimeObject** get_address_of_Quaternion_6() { return &___Quaternion_6; }
	inline void set_Quaternion_6(RuntimeObject* value)
	{
		___Quaternion_6 = value;
		Il2CppCodeGenWriteBarrier((&___Quaternion_6), value);
	}

	inline static int32_t get_offset_of_vector2Type_7() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___vector2Type_7)); }
	inline RuntimeTypeHandle_t3027515415  get_vector2Type_7() const { return ___vector2Type_7; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of_vector2Type_7() { return &___vector2Type_7; }
	inline void set_vector2Type_7(RuntimeTypeHandle_t3027515415  value)
	{
		___vector2Type_7 = value;
	}

	inline static int32_t get_offset_of_vector3Type_8() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___vector3Type_8)); }
	inline RuntimeTypeHandle_t3027515415  get_vector3Type_8() const { return ___vector3Type_8; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of_vector3Type_8() { return &___vector3Type_8; }
	inline void set_vector3Type_8(RuntimeTypeHandle_t3027515415  value)
	{
		___vector3Type_8 = value;
	}

	inline static int32_t get_offset_of_vector4Type_9() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___vector4Type_9)); }
	inline RuntimeTypeHandle_t3027515415  get_vector4Type_9() const { return ___vector4Type_9; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of_vector4Type_9() { return &___vector4Type_9; }
	inline void set_vector4Type_9(RuntimeTypeHandle_t3027515415  value)
	{
		___vector4Type_9 = value;
	}

	inline static int32_t get_offset_of_colorType_10() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___colorType_10)); }
	inline RuntimeTypeHandle_t3027515415  get_colorType_10() const { return ___colorType_10; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of_colorType_10() { return &___colorType_10; }
	inline void set_colorType_10(RuntimeTypeHandle_t3027515415  value)
	{
		___colorType_10 = value;
	}

	inline static int32_t get_offset_of_rectType_11() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___rectType_11)); }
	inline RuntimeTypeHandle_t3027515415  get_rectType_11() const { return ___rectType_11; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of_rectType_11() { return &___rectType_11; }
	inline void set_rectType_11(RuntimeTypeHandle_t3027515415  value)
	{
		___rectType_11 = value;
	}

	inline static int32_t get_offset_of_boundsType_12() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___boundsType_12)); }
	inline RuntimeTypeHandle_t3027515415  get_boundsType_12() const { return ___boundsType_12; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of_boundsType_12() { return &___boundsType_12; }
	inline void set_boundsType_12(RuntimeTypeHandle_t3027515415  value)
	{
		___boundsType_12 = value;
	}

	inline static int32_t get_offset_of_quaternionType_13() { return static_cast<int32_t>(offsetof(UnityEqualityComparer_t3411193022_StaticFields, ___quaternionType_13)); }
	inline RuntimeTypeHandle_t3027515415  get_quaternionType_13() const { return ___quaternionType_13; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of_quaternionType_13() { return &___quaternionType_13; }
	inline void set_quaternionType_13(RuntimeTypeHandle_t3027515415  value)
	{
		___quaternionType_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEQUALITYCOMPARER_T3411193022_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef STATEMACHINEBEHAVIOUR_T957311111_H
#define STATEMACHINEBEHAVIOUR_T957311111_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.StateMachineBehaviour
struct  StateMachineBehaviour_t957311111  : public ScriptableObject_t2528358522
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMACHINEBEHAVIOUR_T957311111_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef OBSERVABLESTATEMACHINETRIGGER_T514432755_H
#define OBSERVABLESTATEMACHINETRIGGER_T514432755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableStateMachineTrigger
struct  ObservableStateMachineTrigger_t514432755  : public StateMachineBehaviour_t957311111
{
public:
	// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo> UniRx.Triggers.ObservableStateMachineTrigger::onStateExit
	Subject_1_t4044785546 * ___onStateExit_2;
	// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo> UniRx.Triggers.ObservableStateMachineTrigger::onStateEnter
	Subject_1_t4044785546 * ___onStateEnter_3;
	// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo> UniRx.Triggers.ObservableStateMachineTrigger::onStateIK
	Subject_1_t4044785546 * ___onStateIK_4;
	// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateInfo> UniRx.Triggers.ObservableStateMachineTrigger::onStateUpdate
	Subject_1_t4044785546 * ___onStateUpdate_5;
	// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo> UniRx.Triggers.ObservableStateMachineTrigger::onStateMachineEnter
	Subject_1_t1228025867 * ___onStateMachineEnter_6;
	// UniRx.Subject`1<UniRx.Triggers.ObservableStateMachineTrigger/OnStateMachineInfo> UniRx.Triggers.ObservableStateMachineTrigger::onStateMachineExit
	Subject_1_t1228025867 * ___onStateMachineExit_7;

public:
	inline static int32_t get_offset_of_onStateExit_2() { return static_cast<int32_t>(offsetof(ObservableStateMachineTrigger_t514432755, ___onStateExit_2)); }
	inline Subject_1_t4044785546 * get_onStateExit_2() const { return ___onStateExit_2; }
	inline Subject_1_t4044785546 ** get_address_of_onStateExit_2() { return &___onStateExit_2; }
	inline void set_onStateExit_2(Subject_1_t4044785546 * value)
	{
		___onStateExit_2 = value;
		Il2CppCodeGenWriteBarrier((&___onStateExit_2), value);
	}

	inline static int32_t get_offset_of_onStateEnter_3() { return static_cast<int32_t>(offsetof(ObservableStateMachineTrigger_t514432755, ___onStateEnter_3)); }
	inline Subject_1_t4044785546 * get_onStateEnter_3() const { return ___onStateEnter_3; }
	inline Subject_1_t4044785546 ** get_address_of_onStateEnter_3() { return &___onStateEnter_3; }
	inline void set_onStateEnter_3(Subject_1_t4044785546 * value)
	{
		___onStateEnter_3 = value;
		Il2CppCodeGenWriteBarrier((&___onStateEnter_3), value);
	}

	inline static int32_t get_offset_of_onStateIK_4() { return static_cast<int32_t>(offsetof(ObservableStateMachineTrigger_t514432755, ___onStateIK_4)); }
	inline Subject_1_t4044785546 * get_onStateIK_4() const { return ___onStateIK_4; }
	inline Subject_1_t4044785546 ** get_address_of_onStateIK_4() { return &___onStateIK_4; }
	inline void set_onStateIK_4(Subject_1_t4044785546 * value)
	{
		___onStateIK_4 = value;
		Il2CppCodeGenWriteBarrier((&___onStateIK_4), value);
	}

	inline static int32_t get_offset_of_onStateUpdate_5() { return static_cast<int32_t>(offsetof(ObservableStateMachineTrigger_t514432755, ___onStateUpdate_5)); }
	inline Subject_1_t4044785546 * get_onStateUpdate_5() const { return ___onStateUpdate_5; }
	inline Subject_1_t4044785546 ** get_address_of_onStateUpdate_5() { return &___onStateUpdate_5; }
	inline void set_onStateUpdate_5(Subject_1_t4044785546 * value)
	{
		___onStateUpdate_5 = value;
		Il2CppCodeGenWriteBarrier((&___onStateUpdate_5), value);
	}

	inline static int32_t get_offset_of_onStateMachineEnter_6() { return static_cast<int32_t>(offsetof(ObservableStateMachineTrigger_t514432755, ___onStateMachineEnter_6)); }
	inline Subject_1_t1228025867 * get_onStateMachineEnter_6() const { return ___onStateMachineEnter_6; }
	inline Subject_1_t1228025867 ** get_address_of_onStateMachineEnter_6() { return &___onStateMachineEnter_6; }
	inline void set_onStateMachineEnter_6(Subject_1_t1228025867 * value)
	{
		___onStateMachineEnter_6 = value;
		Il2CppCodeGenWriteBarrier((&___onStateMachineEnter_6), value);
	}

	inline static int32_t get_offset_of_onStateMachineExit_7() { return static_cast<int32_t>(offsetof(ObservableStateMachineTrigger_t514432755, ___onStateMachineExit_7)); }
	inline Subject_1_t1228025867 * get_onStateMachineExit_7() const { return ___onStateMachineExit_7; }
	inline Subject_1_t1228025867 ** get_address_of_onStateMachineExit_7() { return &___onStateMachineExit_7; }
	inline void set_onStateMachineExit_7(Subject_1_t1228025867 * value)
	{
		___onStateMachineExit_7 = value;
		Il2CppCodeGenWriteBarrier((&___onStateMachineExit_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLESTATEMACHINETRIGGER_T514432755_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef BENCHMARK03_T1571203696_H
#define BENCHMARK03_T1571203696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark03
struct  Benchmark03_t1571203696  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark03::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark03::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// UnityEngine.Font TMPro.Examples.Benchmark03::TheFont
	Font_t1956802104 * ___TheFont_4;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___TheFont_4)); }
	inline Font_t1956802104 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_t1956802104 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_t1956802104 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK03_T1571203696_H
#ifndef OBSERVABLETRIGGERBASE_T3020654715_H
#define OBSERVABLETRIGGERBASE_T3020654715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableTriggerBase
struct  ObservableTriggerBase_t3020654715  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UniRx.Triggers.ObservableTriggerBase::calledAwake
	bool ___calledAwake_2;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerBase::awake
	Subject_1_t3450905854 * ___awake_3;
	// System.Boolean UniRx.Triggers.ObservableTriggerBase::calledStart
	bool ___calledStart_4;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerBase::start
	Subject_1_t3450905854 * ___start_5;
	// System.Boolean UniRx.Triggers.ObservableTriggerBase::calledDestroy
	bool ___calledDestroy_6;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTriggerBase::onDestroy
	Subject_1_t3450905854 * ___onDestroy_7;

public:
	inline static int32_t get_offset_of_calledAwake_2() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t3020654715, ___calledAwake_2)); }
	inline bool get_calledAwake_2() const { return ___calledAwake_2; }
	inline bool* get_address_of_calledAwake_2() { return &___calledAwake_2; }
	inline void set_calledAwake_2(bool value)
	{
		___calledAwake_2 = value;
	}

	inline static int32_t get_offset_of_awake_3() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t3020654715, ___awake_3)); }
	inline Subject_1_t3450905854 * get_awake_3() const { return ___awake_3; }
	inline Subject_1_t3450905854 ** get_address_of_awake_3() { return &___awake_3; }
	inline void set_awake_3(Subject_1_t3450905854 * value)
	{
		___awake_3 = value;
		Il2CppCodeGenWriteBarrier((&___awake_3), value);
	}

	inline static int32_t get_offset_of_calledStart_4() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t3020654715, ___calledStart_4)); }
	inline bool get_calledStart_4() const { return ___calledStart_4; }
	inline bool* get_address_of_calledStart_4() { return &___calledStart_4; }
	inline void set_calledStart_4(bool value)
	{
		___calledStart_4 = value;
	}

	inline static int32_t get_offset_of_start_5() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t3020654715, ___start_5)); }
	inline Subject_1_t3450905854 * get_start_5() const { return ___start_5; }
	inline Subject_1_t3450905854 ** get_address_of_start_5() { return &___start_5; }
	inline void set_start_5(Subject_1_t3450905854 * value)
	{
		___start_5 = value;
		Il2CppCodeGenWriteBarrier((&___start_5), value);
	}

	inline static int32_t get_offset_of_calledDestroy_6() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t3020654715, ___calledDestroy_6)); }
	inline bool get_calledDestroy_6() const { return ___calledDestroy_6; }
	inline bool* get_address_of_calledDestroy_6() { return &___calledDestroy_6; }
	inline void set_calledDestroy_6(bool value)
	{
		___calledDestroy_6 = value;
	}

	inline static int32_t get_offset_of_onDestroy_7() { return static_cast<int32_t>(offsetof(ObservableTriggerBase_t3020654715, ___onDestroy_7)); }
	inline Subject_1_t3450905854 * get_onDestroy_7() const { return ___onDestroy_7; }
	inline Subject_1_t3450905854 ** get_address_of_onDestroy_7() { return &___onDestroy_7; }
	inline void set_onDestroy_7(Subject_1_t3450905854 * value)
	{
		___onDestroy_7 = value;
		Il2CppCodeGenWriteBarrier((&___onDestroy_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLETRIGGERBASE_T3020654715_H
#ifndef BENCHMARK01_T1571072624_H
#define BENCHMARK01_T1571072624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01
struct  Benchmark01_t1571072624  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01::BenchmarkType
	int32_t ___BenchmarkType_2;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_3;
	// UnityEngine.Font TMPro.Examples.Benchmark01::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_4;
	// TMPro.TextMeshPro TMPro.Examples.Benchmark01::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_5;
	// TMPro.TextContainer TMPro.Examples.Benchmark01::m_textContainer
	TextContainer_t97923372 * ___m_textContainer_6;
	// UnityEngine.TextMesh TMPro.Examples.Benchmark01::m_textMesh
	TextMesh_t1536577757 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material01
	Material_t340375123 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material02
	Material_t340375123 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_TMProFont_3() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TMProFont_3)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_3() const { return ___TMProFont_3; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_3() { return &___TMProFont_3; }
	inline void set_TMProFont_3(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_3 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_3), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TextMeshFont_4)); }
	inline Font_t1956802104 * get_TextMeshFont_4() const { return ___TextMeshFont_4; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_4() { return &___TextMeshFont_4; }
	inline void set_TextMeshFont_4(Font_t1956802104 * value)
	{
		___TextMeshFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_4), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_5() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMeshPro_5)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_5() const { return ___m_textMeshPro_5; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_5() { return &___m_textMeshPro_5; }
	inline void set_m_textMeshPro_5(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_5), value);
	}

	inline static int32_t get_offset_of_m_textContainer_6() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textContainer_6)); }
	inline TextContainer_t97923372 * get_m_textContainer_6() const { return ___m_textContainer_6; }
	inline TextContainer_t97923372 ** get_address_of_m_textContainer_6() { return &___m_textContainer_6; }
	inline void set_m_textContainer_6(TextContainer_t97923372 * value)
	{
		___m_textContainer_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMesh_7)); }
	inline TextMesh_t1536577757 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline TextMesh_t1536577757 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(TextMesh_t1536577757 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material01_10)); }
	inline Material_t340375123 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t340375123 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t340375123 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material02_11)); }
	inline Material_t340375123 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t340375123 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t340375123 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_T1571072624_H
#ifndef BENCHMARK02_T1571269232_H
#define BENCHMARK02_T1571269232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark02
struct  Benchmark02_t1571269232  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark02::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark02::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.Benchmark02::floatingText_Script
	TextMeshProFloatingText_t845872552 * ___floatingText_Script_4;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_floatingText_Script_4() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___floatingText_Script_4)); }
	inline TextMeshProFloatingText_t845872552 * get_floatingText_Script_4() const { return ___floatingText_Script_4; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_floatingText_Script_4() { return &___floatingText_Script_4; }
	inline void set_floatingText_Script_4(TextMeshProFloatingText_t845872552 * value)
	{
		___floatingText_Script_4 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK02_T1571269232_H
#ifndef BENCHMARK01_UGUI_T3264177817_H
#define BENCHMARK01_UGUI_T3264177817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI
struct  Benchmark01_UGUI_t3264177817  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI::BenchmarkType
	int32_t ___BenchmarkType_2;
	// UnityEngine.Canvas TMPro.Examples.Benchmark01_UGUI::canvas
	Canvas_t3310196443 * ___canvas_3;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01_UGUI::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_4;
	// UnityEngine.Font TMPro.Examples.Benchmark01_UGUI::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_5;
	// TMPro.TextMeshProUGUI TMPro.Examples.Benchmark01_UGUI::m_textMeshPro
	TextMeshProUGUI_t529313277 * ___m_textMeshPro_6;
	// UnityEngine.UI.Text TMPro.Examples.Benchmark01_UGUI::m_textMesh
	Text_t1901882714 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material01
	Material_t340375123 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material02
	Material_t340375123 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_canvas_3() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___canvas_3)); }
	inline Canvas_t3310196443 * get_canvas_3() const { return ___canvas_3; }
	inline Canvas_t3310196443 ** get_address_of_canvas_3() { return &___canvas_3; }
	inline void set_canvas_3(Canvas_t3310196443 * value)
	{
		___canvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_3), value);
	}

	inline static int32_t get_offset_of_TMProFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TMProFont_4)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_4() const { return ___TMProFont_4; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_4() { return &___TMProFont_4; }
	inline void set_TMProFont_4(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_4), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_5() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TextMeshFont_5)); }
	inline Font_t1956802104 * get_TextMeshFont_5() const { return ___TextMeshFont_5; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_5() { return &___TextMeshFont_5; }
	inline void set_TextMeshFont_5(Font_t1956802104 * value)
	{
		___TextMeshFont_5 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_5), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMeshPro_6)); }
	inline TextMeshProUGUI_t529313277 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TextMeshProUGUI_t529313277 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMesh_7)); }
	inline Text_t1901882714 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline Text_t1901882714 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(Text_t1901882714 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material01_10)); }
	inline Material_t340375123 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t340375123 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t340375123 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material02_11)); }
	inline Material_t340375123 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t340375123 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t340375123 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_UGUI_T3264177817_H
#ifndef OBSERVABLEDESTROYTRIGGER_T3144203490_H
#define OBSERVABLEDESTROYTRIGGER_T3144203490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableDestroyTrigger
struct  ObservableDestroyTrigger_t3144203490  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean UniRx.Triggers.ObservableDestroyTrigger::calledDestroy
	bool ___calledDestroy_2;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableDestroyTrigger::onDestroy
	Subject_1_t3450905854 * ___onDestroy_3;
	// UniRx.CompositeDisposable UniRx.Triggers.ObservableDestroyTrigger::disposablesOnDestroy
	CompositeDisposable_t3924054141 * ___disposablesOnDestroy_4;
	// System.Boolean UniRx.Triggers.ObservableDestroyTrigger::<IsMonitoredActivate>k__BackingField
	bool ___U3CIsMonitoredActivateU3Ek__BackingField_5;
	// System.Boolean UniRx.Triggers.ObservableDestroyTrigger::<IsActivated>k__BackingField
	bool ___U3CIsActivatedU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_calledDestroy_2() { return static_cast<int32_t>(offsetof(ObservableDestroyTrigger_t3144203490, ___calledDestroy_2)); }
	inline bool get_calledDestroy_2() const { return ___calledDestroy_2; }
	inline bool* get_address_of_calledDestroy_2() { return &___calledDestroy_2; }
	inline void set_calledDestroy_2(bool value)
	{
		___calledDestroy_2 = value;
	}

	inline static int32_t get_offset_of_onDestroy_3() { return static_cast<int32_t>(offsetof(ObservableDestroyTrigger_t3144203490, ___onDestroy_3)); }
	inline Subject_1_t3450905854 * get_onDestroy_3() const { return ___onDestroy_3; }
	inline Subject_1_t3450905854 ** get_address_of_onDestroy_3() { return &___onDestroy_3; }
	inline void set_onDestroy_3(Subject_1_t3450905854 * value)
	{
		___onDestroy_3 = value;
		Il2CppCodeGenWriteBarrier((&___onDestroy_3), value);
	}

	inline static int32_t get_offset_of_disposablesOnDestroy_4() { return static_cast<int32_t>(offsetof(ObservableDestroyTrigger_t3144203490, ___disposablesOnDestroy_4)); }
	inline CompositeDisposable_t3924054141 * get_disposablesOnDestroy_4() const { return ___disposablesOnDestroy_4; }
	inline CompositeDisposable_t3924054141 ** get_address_of_disposablesOnDestroy_4() { return &___disposablesOnDestroy_4; }
	inline void set_disposablesOnDestroy_4(CompositeDisposable_t3924054141 * value)
	{
		___disposablesOnDestroy_4 = value;
		Il2CppCodeGenWriteBarrier((&___disposablesOnDestroy_4), value);
	}

	inline static int32_t get_offset_of_U3CIsMonitoredActivateU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ObservableDestroyTrigger_t3144203490, ___U3CIsMonitoredActivateU3Ek__BackingField_5)); }
	inline bool get_U3CIsMonitoredActivateU3Ek__BackingField_5() const { return ___U3CIsMonitoredActivateU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIsMonitoredActivateU3Ek__BackingField_5() { return &___U3CIsMonitoredActivateU3Ek__BackingField_5; }
	inline void set_U3CIsMonitoredActivateU3Ek__BackingField_5(bool value)
	{
		___U3CIsMonitoredActivateU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CIsActivatedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ObservableDestroyTrigger_t3144203490, ___U3CIsActivatedU3Ek__BackingField_6)); }
	inline bool get_U3CIsActivatedU3Ek__BackingField_6() const { return ___U3CIsActivatedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CIsActivatedU3Ek__BackingField_6() { return &___U3CIsActivatedU3Ek__BackingField_6; }
	inline void set_U3CIsActivatedU3Ek__BackingField_6(bool value)
	{
		___U3CIsActivatedU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEDESTROYTRIGGER_T3144203490_H
#ifndef OBSERVABLEDRAGTRIGGER_T1354517533_H
#define OBSERVABLEDRAGTRIGGER_T1354517533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableDragTrigger
struct  ObservableDragTrigger_t1354517533  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableDragTrigger::onDrag
	Subject_1_t3896557479 * ___onDrag_8;

public:
	inline static int32_t get_offset_of_onDrag_8() { return static_cast<int32_t>(offsetof(ObservableDragTrigger_t1354517533, ___onDrag_8)); }
	inline Subject_1_t3896557479 * get_onDrag_8() const { return ___onDrag_8; }
	inline Subject_1_t3896557479 ** get_address_of_onDrag_8() { return &___onDrag_8; }
	inline void set_onDrag_8(Subject_1_t3896557479 * value)
	{
		___onDrag_8 = value;
		Il2CppCodeGenWriteBarrier((&___onDrag_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEDRAGTRIGGER_T1354517533_H
#ifndef OBSERVABLEDROPTRIGGER_T3729934644_H
#define OBSERVABLEDROPTRIGGER_T3729934644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableDropTrigger
struct  ObservableDropTrigger_t3729934644  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableDropTrigger::onDrop
	Subject_1_t3896557479 * ___onDrop_8;

public:
	inline static int32_t get_offset_of_onDrop_8() { return static_cast<int32_t>(offsetof(ObservableDropTrigger_t3729934644, ___onDrop_8)); }
	inline Subject_1_t3896557479 * get_onDrop_8() const { return ___onDrop_8; }
	inline Subject_1_t3896557479 ** get_address_of_onDrop_8() { return &___onDrop_8; }
	inline void set_onDrop_8(Subject_1_t3896557479 * value)
	{
		___onDrop_8 = value;
		Il2CppCodeGenWriteBarrier((&___onDrop_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEDROPTRIGGER_T3729934644_H
#ifndef OBSERVABLEDESELECTTRIGGER_T981271967_H
#define OBSERVABLEDESELECTTRIGGER_T981271967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableDeselectTrigger
struct  ObservableDeselectTrigger_t981271967  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableDeselectTrigger::onDeselect
	Subject_1_t3991683920 * ___onDeselect_8;

public:
	inline static int32_t get_offset_of_onDeselect_8() { return static_cast<int32_t>(offsetof(ObservableDeselectTrigger_t981271967, ___onDeselect_8)); }
	inline Subject_1_t3991683920 * get_onDeselect_8() const { return ___onDeselect_8; }
	inline Subject_1_t3991683920 ** get_address_of_onDeselect_8() { return &___onDeselect_8; }
	inline void set_onDeselect_8(Subject_1_t3991683920 * value)
	{
		___onDeselect_8 = value;
		Il2CppCodeGenWriteBarrier((&___onDeselect_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEDESELECTTRIGGER_T981271967_H
#ifndef OBSERVABLEANIMATORTRIGGER_T4059379110_H
#define OBSERVABLEANIMATORTRIGGER_T4059379110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableAnimatorTrigger
struct  ObservableAnimatorTrigger_t4059379110  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<System.Int32> UniRx.Triggers.ObservableAnimatorTrigger::onAnimatorIK
	Subject_1_t3039602140 * ___onAnimatorIK_8;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableAnimatorTrigger::onAnimatorMove
	Subject_1_t3450905854 * ___onAnimatorMove_9;

public:
	inline static int32_t get_offset_of_onAnimatorIK_8() { return static_cast<int32_t>(offsetof(ObservableAnimatorTrigger_t4059379110, ___onAnimatorIK_8)); }
	inline Subject_1_t3039602140 * get_onAnimatorIK_8() const { return ___onAnimatorIK_8; }
	inline Subject_1_t3039602140 ** get_address_of_onAnimatorIK_8() { return &___onAnimatorIK_8; }
	inline void set_onAnimatorIK_8(Subject_1_t3039602140 * value)
	{
		___onAnimatorIK_8 = value;
		Il2CppCodeGenWriteBarrier((&___onAnimatorIK_8), value);
	}

	inline static int32_t get_offset_of_onAnimatorMove_9() { return static_cast<int32_t>(offsetof(ObservableAnimatorTrigger_t4059379110, ___onAnimatorMove_9)); }
	inline Subject_1_t3450905854 * get_onAnimatorMove_9() const { return ___onAnimatorMove_9; }
	inline Subject_1_t3450905854 ** get_address_of_onAnimatorMove_9() { return &___onAnimatorMove_9; }
	inline void set_onAnimatorMove_9(Subject_1_t3450905854 * value)
	{
		___onAnimatorMove_9 = value;
		Il2CppCodeGenWriteBarrier((&___onAnimatorMove_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEANIMATORTRIGGER_T4059379110_H
#ifndef OBSERVABLEBEGINDRAGTRIGGER_T457401454_H
#define OBSERVABLEBEGINDRAGTRIGGER_T457401454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableBeginDragTrigger
struct  ObservableBeginDragTrigger_t457401454  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableBeginDragTrigger::onBeginDrag
	Subject_1_t3896557479 * ___onBeginDrag_8;

public:
	inline static int32_t get_offset_of_onBeginDrag_8() { return static_cast<int32_t>(offsetof(ObservableBeginDragTrigger_t457401454, ___onBeginDrag_8)); }
	inline Subject_1_t3896557479 * get_onBeginDrag_8() const { return ___onBeginDrag_8; }
	inline Subject_1_t3896557479 ** get_address_of_onBeginDrag_8() { return &___onBeginDrag_8; }
	inline void set_onBeginDrag_8(Subject_1_t3896557479 * value)
	{
		___onBeginDrag_8 = value;
		Il2CppCodeGenWriteBarrier((&___onBeginDrag_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEBEGINDRAGTRIGGER_T457401454_H
#ifndef OBSERVABLECANCELTRIGGER_T3203487127_H
#define OBSERVABLECANCELTRIGGER_T3203487127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableCancelTrigger
struct  ObservableCancelTrigger_t3203487127  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableCancelTrigger::onCancel
	Subject_1_t3991683920 * ___onCancel_8;

public:
	inline static int32_t get_offset_of_onCancel_8() { return static_cast<int32_t>(offsetof(ObservableCancelTrigger_t3203487127, ___onCancel_8)); }
	inline Subject_1_t3991683920 * get_onCancel_8() const { return ___onCancel_8; }
	inline Subject_1_t3991683920 ** get_address_of_onCancel_8() { return &___onCancel_8; }
	inline void set_onCancel_8(Subject_1_t3991683920 * value)
	{
		___onCancel_8 = value;
		Il2CppCodeGenWriteBarrier((&___onCancel_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLECANCELTRIGGER_T3203487127_H
#ifndef OBSERVABLECANVASGROUPCHANGEDTRIGGER_T3835595784_H
#define OBSERVABLECANVASGROUPCHANGEDTRIGGER_T3835595784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableCanvasGroupChangedTrigger
struct  ObservableCanvasGroupChangedTrigger_t3835595784  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableCanvasGroupChangedTrigger::onCanvasGroupChanged
	Subject_1_t3450905854 * ___onCanvasGroupChanged_8;

public:
	inline static int32_t get_offset_of_onCanvasGroupChanged_8() { return static_cast<int32_t>(offsetof(ObservableCanvasGroupChangedTrigger_t3835595784, ___onCanvasGroupChanged_8)); }
	inline Subject_1_t3450905854 * get_onCanvasGroupChanged_8() const { return ___onCanvasGroupChanged_8; }
	inline Subject_1_t3450905854 ** get_address_of_onCanvasGroupChanged_8() { return &___onCanvasGroupChanged_8; }
	inline void set_onCanvasGroupChanged_8(Subject_1_t3450905854 * value)
	{
		___onCanvasGroupChanged_8 = value;
		Il2CppCodeGenWriteBarrier((&___onCanvasGroupChanged_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLECANVASGROUPCHANGEDTRIGGER_T3835595784_H
#ifndef OBSERVABLETRANSFORMCHANGEDTRIGGER_T829483795_H
#define OBSERVABLETRANSFORMCHANGEDTRIGGER_T829483795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableTransformChangedTrigger
struct  ObservableTransformChangedTrigger_t829483795  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTransformChangedTrigger::onBeforeTransformParentChanged
	Subject_1_t3450905854 * ___onBeforeTransformParentChanged_8;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTransformChangedTrigger::onTransformParentChanged
	Subject_1_t3450905854 * ___onTransformParentChanged_9;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableTransformChangedTrigger::onTransformChildrenChanged
	Subject_1_t3450905854 * ___onTransformChildrenChanged_10;

public:
	inline static int32_t get_offset_of_onBeforeTransformParentChanged_8() { return static_cast<int32_t>(offsetof(ObservableTransformChangedTrigger_t829483795, ___onBeforeTransformParentChanged_8)); }
	inline Subject_1_t3450905854 * get_onBeforeTransformParentChanged_8() const { return ___onBeforeTransformParentChanged_8; }
	inline Subject_1_t3450905854 ** get_address_of_onBeforeTransformParentChanged_8() { return &___onBeforeTransformParentChanged_8; }
	inline void set_onBeforeTransformParentChanged_8(Subject_1_t3450905854 * value)
	{
		___onBeforeTransformParentChanged_8 = value;
		Il2CppCodeGenWriteBarrier((&___onBeforeTransformParentChanged_8), value);
	}

	inline static int32_t get_offset_of_onTransformParentChanged_9() { return static_cast<int32_t>(offsetof(ObservableTransformChangedTrigger_t829483795, ___onTransformParentChanged_9)); }
	inline Subject_1_t3450905854 * get_onTransformParentChanged_9() const { return ___onTransformParentChanged_9; }
	inline Subject_1_t3450905854 ** get_address_of_onTransformParentChanged_9() { return &___onTransformParentChanged_9; }
	inline void set_onTransformParentChanged_9(Subject_1_t3450905854 * value)
	{
		___onTransformParentChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___onTransformParentChanged_9), value);
	}

	inline static int32_t get_offset_of_onTransformChildrenChanged_10() { return static_cast<int32_t>(offsetof(ObservableTransformChangedTrigger_t829483795, ___onTransformChildrenChanged_10)); }
	inline Subject_1_t3450905854 * get_onTransformChildrenChanged_10() const { return ___onTransformChildrenChanged_10; }
	inline Subject_1_t3450905854 ** get_address_of_onTransformChildrenChanged_10() { return &___onTransformChildrenChanged_10; }
	inline void set_onTransformChildrenChanged_10(Subject_1_t3450905854 * value)
	{
		___onTransformChildrenChanged_10 = value;
		Il2CppCodeGenWriteBarrier((&___onTransformChildrenChanged_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLETRANSFORMCHANGEDTRIGGER_T829483795_H
#ifndef OBSERVABLECOLLISION2DTRIGGER_T573452484_H
#define OBSERVABLECOLLISION2DTRIGGER_T573452484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableCollision2DTrigger
struct  ObservableCollision2DTrigger_t573452484  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableCollision2DTrigger::onCollisionEnter2D
	Subject_1_t2931612718 * ___onCollisionEnter2D_8;
	// UniRx.Subject`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableCollision2DTrigger::onCollisionExit2D
	Subject_1_t2931612718 * ___onCollisionExit2D_9;
	// UniRx.Subject`1<UnityEngine.Collision2D> UniRx.Triggers.ObservableCollision2DTrigger::onCollisionStay2D
	Subject_1_t2931612718 * ___onCollisionStay2D_10;

public:
	inline static int32_t get_offset_of_onCollisionEnter2D_8() { return static_cast<int32_t>(offsetof(ObservableCollision2DTrigger_t573452484, ___onCollisionEnter2D_8)); }
	inline Subject_1_t2931612718 * get_onCollisionEnter2D_8() const { return ___onCollisionEnter2D_8; }
	inline Subject_1_t2931612718 ** get_address_of_onCollisionEnter2D_8() { return &___onCollisionEnter2D_8; }
	inline void set_onCollisionEnter2D_8(Subject_1_t2931612718 * value)
	{
		___onCollisionEnter2D_8 = value;
		Il2CppCodeGenWriteBarrier((&___onCollisionEnter2D_8), value);
	}

	inline static int32_t get_offset_of_onCollisionExit2D_9() { return static_cast<int32_t>(offsetof(ObservableCollision2DTrigger_t573452484, ___onCollisionExit2D_9)); }
	inline Subject_1_t2931612718 * get_onCollisionExit2D_9() const { return ___onCollisionExit2D_9; }
	inline Subject_1_t2931612718 ** get_address_of_onCollisionExit2D_9() { return &___onCollisionExit2D_9; }
	inline void set_onCollisionExit2D_9(Subject_1_t2931612718 * value)
	{
		___onCollisionExit2D_9 = value;
		Il2CppCodeGenWriteBarrier((&___onCollisionExit2D_9), value);
	}

	inline static int32_t get_offset_of_onCollisionStay2D_10() { return static_cast<int32_t>(offsetof(ObservableCollision2DTrigger_t573452484, ___onCollisionStay2D_10)); }
	inline Subject_1_t2931612718 * get_onCollisionStay2D_10() const { return ___onCollisionStay2D_10; }
	inline Subject_1_t2931612718 ** get_address_of_onCollisionStay2D_10() { return &___onCollisionStay2D_10; }
	inline void set_onCollisionStay2D_10(Subject_1_t2931612718 * value)
	{
		___onCollisionStay2D_10 = value;
		Il2CppCodeGenWriteBarrier((&___onCollisionStay2D_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLECOLLISION2DTRIGGER_T573452484_H
#ifndef OBSERVABLECOLLISIONTRIGGER_T3942043460_H
#define OBSERVABLECOLLISIONTRIGGER_T3942043460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableCollisionTrigger
struct  ObservableCollisionTrigger_t3942043460  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.Collision> UniRx.Triggers.ObservableCollisionTrigger::onCollisionEnter
	Subject_1_t55769541 * ___onCollisionEnter_8;
	// UniRx.Subject`1<UnityEngine.Collision> UniRx.Triggers.ObservableCollisionTrigger::onCollisionExit
	Subject_1_t55769541 * ___onCollisionExit_9;
	// UniRx.Subject`1<UnityEngine.Collision> UniRx.Triggers.ObservableCollisionTrigger::onCollisionStay
	Subject_1_t55769541 * ___onCollisionStay_10;

public:
	inline static int32_t get_offset_of_onCollisionEnter_8() { return static_cast<int32_t>(offsetof(ObservableCollisionTrigger_t3942043460, ___onCollisionEnter_8)); }
	inline Subject_1_t55769541 * get_onCollisionEnter_8() const { return ___onCollisionEnter_8; }
	inline Subject_1_t55769541 ** get_address_of_onCollisionEnter_8() { return &___onCollisionEnter_8; }
	inline void set_onCollisionEnter_8(Subject_1_t55769541 * value)
	{
		___onCollisionEnter_8 = value;
		Il2CppCodeGenWriteBarrier((&___onCollisionEnter_8), value);
	}

	inline static int32_t get_offset_of_onCollisionExit_9() { return static_cast<int32_t>(offsetof(ObservableCollisionTrigger_t3942043460, ___onCollisionExit_9)); }
	inline Subject_1_t55769541 * get_onCollisionExit_9() const { return ___onCollisionExit_9; }
	inline Subject_1_t55769541 ** get_address_of_onCollisionExit_9() { return &___onCollisionExit_9; }
	inline void set_onCollisionExit_9(Subject_1_t55769541 * value)
	{
		___onCollisionExit_9 = value;
		Il2CppCodeGenWriteBarrier((&___onCollisionExit_9), value);
	}

	inline static int32_t get_offset_of_onCollisionStay_10() { return static_cast<int32_t>(offsetof(ObservableCollisionTrigger_t3942043460, ___onCollisionStay_10)); }
	inline Subject_1_t55769541 * get_onCollisionStay_10() const { return ___onCollisionStay_10; }
	inline Subject_1_t55769541 ** get_address_of_onCollisionStay_10() { return &___onCollisionStay_10; }
	inline void set_onCollisionStay_10(Subject_1_t55769541 * value)
	{
		___onCollisionStay_10 = value;
		Il2CppCodeGenWriteBarrier((&___onCollisionStay_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLECOLLISIONTRIGGER_T3942043460_H
#ifndef OBSERVABLEENABLETRIGGER_T332491237_H
#define OBSERVABLEENABLETRIGGER_T332491237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableEnableTrigger
struct  ObservableEnableTrigger_t332491237  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableEnableTrigger::onEnable
	Subject_1_t3450905854 * ___onEnable_8;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableEnableTrigger::onDisable
	Subject_1_t3450905854 * ___onDisable_9;

public:
	inline static int32_t get_offset_of_onEnable_8() { return static_cast<int32_t>(offsetof(ObservableEnableTrigger_t332491237, ___onEnable_8)); }
	inline Subject_1_t3450905854 * get_onEnable_8() const { return ___onEnable_8; }
	inline Subject_1_t3450905854 ** get_address_of_onEnable_8() { return &___onEnable_8; }
	inline void set_onEnable_8(Subject_1_t3450905854 * value)
	{
		___onEnable_8 = value;
		Il2CppCodeGenWriteBarrier((&___onEnable_8), value);
	}

	inline static int32_t get_offset_of_onDisable_9() { return static_cast<int32_t>(offsetof(ObservableEnableTrigger_t332491237, ___onDisable_9)); }
	inline Subject_1_t3450905854 * get_onDisable_9() const { return ___onDisable_9; }
	inline Subject_1_t3450905854 ** get_address_of_onDisable_9() { return &___onDisable_9; }
	inline void set_onDisable_9(Subject_1_t3450905854 * value)
	{
		___onDisable_9 = value;
		Il2CppCodeGenWriteBarrier((&___onDisable_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEENABLETRIGGER_T332491237_H
#ifndef OBSERVABLEPOINTERUPTRIGGER_T3544017431_H
#define OBSERVABLEPOINTERUPTRIGGER_T3544017431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservablePointerUpTrigger
struct  ObservablePointerUpTrigger_t3544017431  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservablePointerUpTrigger::onPointerUp
	Subject_1_t3896557479 * ___onPointerUp_8;

public:
	inline static int32_t get_offset_of_onPointerUp_8() { return static_cast<int32_t>(offsetof(ObservablePointerUpTrigger_t3544017431, ___onPointerUp_8)); }
	inline Subject_1_t3896557479 * get_onPointerUp_8() const { return ___onPointerUp_8; }
	inline Subject_1_t3896557479 ** get_address_of_onPointerUp_8() { return &___onPointerUp_8; }
	inline void set_onPointerUp_8(Subject_1_t3896557479 * value)
	{
		___onPointerUp_8 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerUp_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEPOINTERUPTRIGGER_T3544017431_H
#ifndef OBSERVABLERECTTRANSFORMTRIGGER_T4048819422_H
#define OBSERVABLERECTTRANSFORMTRIGGER_T4048819422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableRectTransformTrigger
struct  ObservableRectTransformTrigger_t4048819422  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableRectTransformTrigger::onRectTransformDimensionsChange
	Subject_1_t3450905854 * ___onRectTransformDimensionsChange_8;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableRectTransformTrigger::onRectTransformRemoved
	Subject_1_t3450905854 * ___onRectTransformRemoved_9;

public:
	inline static int32_t get_offset_of_onRectTransformDimensionsChange_8() { return static_cast<int32_t>(offsetof(ObservableRectTransformTrigger_t4048819422, ___onRectTransformDimensionsChange_8)); }
	inline Subject_1_t3450905854 * get_onRectTransformDimensionsChange_8() const { return ___onRectTransformDimensionsChange_8; }
	inline Subject_1_t3450905854 ** get_address_of_onRectTransformDimensionsChange_8() { return &___onRectTransformDimensionsChange_8; }
	inline void set_onRectTransformDimensionsChange_8(Subject_1_t3450905854 * value)
	{
		___onRectTransformDimensionsChange_8 = value;
		Il2CppCodeGenWriteBarrier((&___onRectTransformDimensionsChange_8), value);
	}

	inline static int32_t get_offset_of_onRectTransformRemoved_9() { return static_cast<int32_t>(offsetof(ObservableRectTransformTrigger_t4048819422, ___onRectTransformRemoved_9)); }
	inline Subject_1_t3450905854 * get_onRectTransformRemoved_9() const { return ___onRectTransformRemoved_9; }
	inline Subject_1_t3450905854 ** get_address_of_onRectTransformRemoved_9() { return &___onRectTransformRemoved_9; }
	inline void set_onRectTransformRemoved_9(Subject_1_t3450905854 * value)
	{
		___onRectTransformRemoved_9 = value;
		Il2CppCodeGenWriteBarrier((&___onRectTransformRemoved_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLERECTTRANSFORMTRIGGER_T4048819422_H
#ifndef OBSERVABLESCROLLTRIGGER_T4106240645_H
#define OBSERVABLESCROLLTRIGGER_T4106240645_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableScrollTrigger
struct  ObservableScrollTrigger_t4106240645  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableScrollTrigger::onScroll
	Subject_1_t3896557479 * ___onScroll_8;

public:
	inline static int32_t get_offset_of_onScroll_8() { return static_cast<int32_t>(offsetof(ObservableScrollTrigger_t4106240645, ___onScroll_8)); }
	inline Subject_1_t3896557479 * get_onScroll_8() const { return ___onScroll_8; }
	inline Subject_1_t3896557479 ** get_address_of_onScroll_8() { return &___onScroll_8; }
	inline void set_onScroll_8(Subject_1_t3896557479 * value)
	{
		___onScroll_8 = value;
		Il2CppCodeGenWriteBarrier((&___onScroll_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLESCROLLTRIGGER_T4106240645_H
#ifndef OBSERVABLESELECTTRIGGER_T629978404_H
#define OBSERVABLESELECTTRIGGER_T629978404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableSelectTrigger
struct  ObservableSelectTrigger_t629978404  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableSelectTrigger::onSelect
	Subject_1_t3991683920 * ___onSelect_8;

public:
	inline static int32_t get_offset_of_onSelect_8() { return static_cast<int32_t>(offsetof(ObservableSelectTrigger_t629978404, ___onSelect_8)); }
	inline Subject_1_t3991683920 * get_onSelect_8() const { return ___onSelect_8; }
	inline Subject_1_t3991683920 ** get_address_of_onSelect_8() { return &___onSelect_8; }
	inline void set_onSelect_8(Subject_1_t3991683920 * value)
	{
		___onSelect_8 = value;
		Il2CppCodeGenWriteBarrier((&___onSelect_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLESELECTTRIGGER_T629978404_H
#ifndef OBSERVABLEVISIBLETRIGGER_T1755347736_H
#define OBSERVABLEVISIBLETRIGGER_T1755347736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableVisibleTrigger
struct  ObservableVisibleTrigger_t1755347736  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableVisibleTrigger::onBecameInvisible
	Subject_1_t3450905854 * ___onBecameInvisible_8;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableVisibleTrigger::onBecameVisible
	Subject_1_t3450905854 * ___onBecameVisible_9;

public:
	inline static int32_t get_offset_of_onBecameInvisible_8() { return static_cast<int32_t>(offsetof(ObservableVisibleTrigger_t1755347736, ___onBecameInvisible_8)); }
	inline Subject_1_t3450905854 * get_onBecameInvisible_8() const { return ___onBecameInvisible_8; }
	inline Subject_1_t3450905854 ** get_address_of_onBecameInvisible_8() { return &___onBecameInvisible_8; }
	inline void set_onBecameInvisible_8(Subject_1_t3450905854 * value)
	{
		___onBecameInvisible_8 = value;
		Il2CppCodeGenWriteBarrier((&___onBecameInvisible_8), value);
	}

	inline static int32_t get_offset_of_onBecameVisible_9() { return static_cast<int32_t>(offsetof(ObservableVisibleTrigger_t1755347736, ___onBecameVisible_9)); }
	inline Subject_1_t3450905854 * get_onBecameVisible_9() const { return ___onBecameVisible_9; }
	inline Subject_1_t3450905854 ** get_address_of_onBecameVisible_9() { return &___onBecameVisible_9; }
	inline void set_onBecameVisible_9(Subject_1_t3450905854 * value)
	{
		___onBecameVisible_9 = value;
		Il2CppCodeGenWriteBarrier((&___onBecameVisible_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEVISIBLETRIGGER_T1755347736_H
#ifndef OBSERVABLEUPDATETRIGGER_T3947578800_H
#define OBSERVABLEUPDATETRIGGER_T3947578800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableUpdateTrigger
struct  ObservableUpdateTrigger_t3947578800  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableUpdateTrigger::update
	Subject_1_t3450905854 * ___update_8;

public:
	inline static int32_t get_offset_of_update_8() { return static_cast<int32_t>(offsetof(ObservableUpdateTrigger_t3947578800, ___update_8)); }
	inline Subject_1_t3450905854 * get_update_8() const { return ___update_8; }
	inline Subject_1_t3450905854 ** get_address_of_update_8() { return &___update_8; }
	inline void set_update_8(Subject_1_t3450905854 * value)
	{
		___update_8 = value;
		Il2CppCodeGenWriteBarrier((&___update_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEUPDATETRIGGER_T3947578800_H
#ifndef OBSERVABLEUPDATESELECTEDTRIGGER_T2582606763_H
#define OBSERVABLEUPDATESELECTEDTRIGGER_T2582606763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableUpdateSelectedTrigger
struct  ObservableUpdateSelectedTrigger_t2582606763  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableUpdateSelectedTrigger::onUpdateSelected
	Subject_1_t3991683920 * ___onUpdateSelected_8;

public:
	inline static int32_t get_offset_of_onUpdateSelected_8() { return static_cast<int32_t>(offsetof(ObservableUpdateSelectedTrigger_t2582606763, ___onUpdateSelected_8)); }
	inline Subject_1_t3991683920 * get_onUpdateSelected_8() const { return ___onUpdateSelected_8; }
	inline Subject_1_t3991683920 ** get_address_of_onUpdateSelected_8() { return &___onUpdateSelected_8; }
	inline void set_onUpdateSelected_8(Subject_1_t3991683920 * value)
	{
		___onUpdateSelected_8 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdateSelected_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEUPDATESELECTEDTRIGGER_T2582606763_H
#ifndef OBSERVABLESUBMITTRIGGER_T3261832817_H
#define OBSERVABLESUBMITTRIGGER_T3261832817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableSubmitTrigger
struct  ObservableSubmitTrigger_t3261832817  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableSubmitTrigger::onSubmit
	Subject_1_t3991683920 * ___onSubmit_8;

public:
	inline static int32_t get_offset_of_onSubmit_8() { return static_cast<int32_t>(offsetof(ObservableSubmitTrigger_t3261832817, ___onSubmit_8)); }
	inline Subject_1_t3991683920 * get_onSubmit_8() const { return ___onSubmit_8; }
	inline Subject_1_t3991683920 ** get_address_of_onSubmit_8() { return &___onSubmit_8; }
	inline void set_onSubmit_8(Subject_1_t3991683920 * value)
	{
		___onSubmit_8 = value;
		Il2CppCodeGenWriteBarrier((&___onSubmit_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLESUBMITTRIGGER_T3261832817_H
#ifndef OBSERVABLETRIGGER2DTRIGGER_T3116596712_H
#define OBSERVABLETRIGGER2DTRIGGER_T3116596712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableTrigger2DTrigger
struct  ObservableTrigger2DTrigger_t3116596712  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTrigger2DTrigger::onTriggerEnter2D
	Subject_1_t2895456013 * ___onTriggerEnter2D_8;
	// UniRx.Subject`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTrigger2DTrigger::onTriggerExit2D
	Subject_1_t2895456013 * ___onTriggerExit2D_9;
	// UniRx.Subject`1<UnityEngine.Collider2D> UniRx.Triggers.ObservableTrigger2DTrigger::onTriggerStay2D
	Subject_1_t2895456013 * ___onTriggerStay2D_10;

public:
	inline static int32_t get_offset_of_onTriggerEnter2D_8() { return static_cast<int32_t>(offsetof(ObservableTrigger2DTrigger_t3116596712, ___onTriggerEnter2D_8)); }
	inline Subject_1_t2895456013 * get_onTriggerEnter2D_8() const { return ___onTriggerEnter2D_8; }
	inline Subject_1_t2895456013 ** get_address_of_onTriggerEnter2D_8() { return &___onTriggerEnter2D_8; }
	inline void set_onTriggerEnter2D_8(Subject_1_t2895456013 * value)
	{
		___onTriggerEnter2D_8 = value;
		Il2CppCodeGenWriteBarrier((&___onTriggerEnter2D_8), value);
	}

	inline static int32_t get_offset_of_onTriggerExit2D_9() { return static_cast<int32_t>(offsetof(ObservableTrigger2DTrigger_t3116596712, ___onTriggerExit2D_9)); }
	inline Subject_1_t2895456013 * get_onTriggerExit2D_9() const { return ___onTriggerExit2D_9; }
	inline Subject_1_t2895456013 ** get_address_of_onTriggerExit2D_9() { return &___onTriggerExit2D_9; }
	inline void set_onTriggerExit2D_9(Subject_1_t2895456013 * value)
	{
		___onTriggerExit2D_9 = value;
		Il2CppCodeGenWriteBarrier((&___onTriggerExit2D_9), value);
	}

	inline static int32_t get_offset_of_onTriggerStay2D_10() { return static_cast<int32_t>(offsetof(ObservableTrigger2DTrigger_t3116596712, ___onTriggerStay2D_10)); }
	inline Subject_1_t2895456013 * get_onTriggerStay2D_10() const { return ___onTriggerStay2D_10; }
	inline Subject_1_t2895456013 ** get_address_of_onTriggerStay2D_10() { return &___onTriggerStay2D_10; }
	inline void set_onTriggerStay2D_10(Subject_1_t2895456013 * value)
	{
		___onTriggerStay2D_10 = value;
		Il2CppCodeGenWriteBarrier((&___onTriggerStay2D_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLETRIGGER2DTRIGGER_T3116596712_H
#ifndef OBSERVABLEPOINTEREXITTRIGGER_T1106360683_H
#define OBSERVABLEPOINTEREXITTRIGGER_T1106360683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservablePointerExitTrigger
struct  ObservablePointerExitTrigger_t1106360683  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservablePointerExitTrigger::onPointerExit
	Subject_1_t3896557479 * ___onPointerExit_8;

public:
	inline static int32_t get_offset_of_onPointerExit_8() { return static_cast<int32_t>(offsetof(ObservablePointerExitTrigger_t1106360683, ___onPointerExit_8)); }
	inline Subject_1_t3896557479 * get_onPointerExit_8() const { return ___onPointerExit_8; }
	inline Subject_1_t3896557479 ** get_address_of_onPointerExit_8() { return &___onPointerExit_8; }
	inline void set_onPointerExit_8(Subject_1_t3896557479 * value)
	{
		___onPointerExit_8 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerExit_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEPOINTEREXITTRIGGER_T1106360683_H
#ifndef OBSERVABLEENDDRAGTRIGGER_T832788659_H
#define OBSERVABLEENDDRAGTRIGGER_T832788659_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableEndDragTrigger
struct  ObservableEndDragTrigger_t832788659  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEndDragTrigger::onEndDrag
	Subject_1_t3896557479 * ___onEndDrag_8;

public:
	inline static int32_t get_offset_of_onEndDrag_8() { return static_cast<int32_t>(offsetof(ObservableEndDragTrigger_t832788659, ___onEndDrag_8)); }
	inline Subject_1_t3896557479 * get_onEndDrag_8() const { return ___onEndDrag_8; }
	inline Subject_1_t3896557479 ** get_address_of_onEndDrag_8() { return &___onEndDrag_8; }
	inline void set_onEndDrag_8(Subject_1_t3896557479 * value)
	{
		___onEndDrag_8 = value;
		Il2CppCodeGenWriteBarrier((&___onEndDrag_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEENDDRAGTRIGGER_T832788659_H
#ifndef OBSERVABLEEVENTTRIGGER_T2448499564_H
#define OBSERVABLEEVENTTRIGGER_T2448499564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableEventTrigger
struct  ObservableEventTrigger_t2448499564  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::onDeselect
	Subject_1_t3991683920 * ___onDeselect_8;
	// UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData> UniRx.Triggers.ObservableEventTrigger::onMove
	Subject_1_t2419900039 * ___onMove_9;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onPointerDown
	Subject_1_t3896557479 * ___onPointerDown_10;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onPointerEnter
	Subject_1_t3896557479 * ___onPointerEnter_11;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onPointerExit
	Subject_1_t3896557479 * ___onPointerExit_12;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onPointerUp
	Subject_1_t3896557479 * ___onPointerUp_13;
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::onSelect
	Subject_1_t3991683920 * ___onSelect_14;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onPointerClick
	Subject_1_t3896557479 * ___onPointerClick_15;
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::onSubmit
	Subject_1_t3991683920 * ___onSubmit_16;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onDrag
	Subject_1_t3896557479 * ___onDrag_17;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onBeginDrag
	Subject_1_t3896557479 * ___onBeginDrag_18;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onEndDrag
	Subject_1_t3896557479 * ___onEndDrag_19;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onDrop
	Subject_1_t3896557479 * ___onDrop_20;
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::onUpdateSelected
	Subject_1_t3991683920 * ___onUpdateSelected_21;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onInitializePotentialDrag
	Subject_1_t3896557479 * ___onInitializePotentialDrag_22;
	// UniRx.Subject`1<UnityEngine.EventSystems.BaseEventData> UniRx.Triggers.ObservableEventTrigger::onCancel
	Subject_1_t3991683920 * ___onCancel_23;
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableEventTrigger::onScroll
	Subject_1_t3896557479 * ___onScroll_24;

public:
	inline static int32_t get_offset_of_onDeselect_8() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onDeselect_8)); }
	inline Subject_1_t3991683920 * get_onDeselect_8() const { return ___onDeselect_8; }
	inline Subject_1_t3991683920 ** get_address_of_onDeselect_8() { return &___onDeselect_8; }
	inline void set_onDeselect_8(Subject_1_t3991683920 * value)
	{
		___onDeselect_8 = value;
		Il2CppCodeGenWriteBarrier((&___onDeselect_8), value);
	}

	inline static int32_t get_offset_of_onMove_9() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onMove_9)); }
	inline Subject_1_t2419900039 * get_onMove_9() const { return ___onMove_9; }
	inline Subject_1_t2419900039 ** get_address_of_onMove_9() { return &___onMove_9; }
	inline void set_onMove_9(Subject_1_t2419900039 * value)
	{
		___onMove_9 = value;
		Il2CppCodeGenWriteBarrier((&___onMove_9), value);
	}

	inline static int32_t get_offset_of_onPointerDown_10() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onPointerDown_10)); }
	inline Subject_1_t3896557479 * get_onPointerDown_10() const { return ___onPointerDown_10; }
	inline Subject_1_t3896557479 ** get_address_of_onPointerDown_10() { return &___onPointerDown_10; }
	inline void set_onPointerDown_10(Subject_1_t3896557479 * value)
	{
		___onPointerDown_10 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerDown_10), value);
	}

	inline static int32_t get_offset_of_onPointerEnter_11() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onPointerEnter_11)); }
	inline Subject_1_t3896557479 * get_onPointerEnter_11() const { return ___onPointerEnter_11; }
	inline Subject_1_t3896557479 ** get_address_of_onPointerEnter_11() { return &___onPointerEnter_11; }
	inline void set_onPointerEnter_11(Subject_1_t3896557479 * value)
	{
		___onPointerEnter_11 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerEnter_11), value);
	}

	inline static int32_t get_offset_of_onPointerExit_12() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onPointerExit_12)); }
	inline Subject_1_t3896557479 * get_onPointerExit_12() const { return ___onPointerExit_12; }
	inline Subject_1_t3896557479 ** get_address_of_onPointerExit_12() { return &___onPointerExit_12; }
	inline void set_onPointerExit_12(Subject_1_t3896557479 * value)
	{
		___onPointerExit_12 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerExit_12), value);
	}

	inline static int32_t get_offset_of_onPointerUp_13() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onPointerUp_13)); }
	inline Subject_1_t3896557479 * get_onPointerUp_13() const { return ___onPointerUp_13; }
	inline Subject_1_t3896557479 ** get_address_of_onPointerUp_13() { return &___onPointerUp_13; }
	inline void set_onPointerUp_13(Subject_1_t3896557479 * value)
	{
		___onPointerUp_13 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerUp_13), value);
	}

	inline static int32_t get_offset_of_onSelect_14() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onSelect_14)); }
	inline Subject_1_t3991683920 * get_onSelect_14() const { return ___onSelect_14; }
	inline Subject_1_t3991683920 ** get_address_of_onSelect_14() { return &___onSelect_14; }
	inline void set_onSelect_14(Subject_1_t3991683920 * value)
	{
		___onSelect_14 = value;
		Il2CppCodeGenWriteBarrier((&___onSelect_14), value);
	}

	inline static int32_t get_offset_of_onPointerClick_15() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onPointerClick_15)); }
	inline Subject_1_t3896557479 * get_onPointerClick_15() const { return ___onPointerClick_15; }
	inline Subject_1_t3896557479 ** get_address_of_onPointerClick_15() { return &___onPointerClick_15; }
	inline void set_onPointerClick_15(Subject_1_t3896557479 * value)
	{
		___onPointerClick_15 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerClick_15), value);
	}

	inline static int32_t get_offset_of_onSubmit_16() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onSubmit_16)); }
	inline Subject_1_t3991683920 * get_onSubmit_16() const { return ___onSubmit_16; }
	inline Subject_1_t3991683920 ** get_address_of_onSubmit_16() { return &___onSubmit_16; }
	inline void set_onSubmit_16(Subject_1_t3991683920 * value)
	{
		___onSubmit_16 = value;
		Il2CppCodeGenWriteBarrier((&___onSubmit_16), value);
	}

	inline static int32_t get_offset_of_onDrag_17() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onDrag_17)); }
	inline Subject_1_t3896557479 * get_onDrag_17() const { return ___onDrag_17; }
	inline Subject_1_t3896557479 ** get_address_of_onDrag_17() { return &___onDrag_17; }
	inline void set_onDrag_17(Subject_1_t3896557479 * value)
	{
		___onDrag_17 = value;
		Il2CppCodeGenWriteBarrier((&___onDrag_17), value);
	}

	inline static int32_t get_offset_of_onBeginDrag_18() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onBeginDrag_18)); }
	inline Subject_1_t3896557479 * get_onBeginDrag_18() const { return ___onBeginDrag_18; }
	inline Subject_1_t3896557479 ** get_address_of_onBeginDrag_18() { return &___onBeginDrag_18; }
	inline void set_onBeginDrag_18(Subject_1_t3896557479 * value)
	{
		___onBeginDrag_18 = value;
		Il2CppCodeGenWriteBarrier((&___onBeginDrag_18), value);
	}

	inline static int32_t get_offset_of_onEndDrag_19() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onEndDrag_19)); }
	inline Subject_1_t3896557479 * get_onEndDrag_19() const { return ___onEndDrag_19; }
	inline Subject_1_t3896557479 ** get_address_of_onEndDrag_19() { return &___onEndDrag_19; }
	inline void set_onEndDrag_19(Subject_1_t3896557479 * value)
	{
		___onEndDrag_19 = value;
		Il2CppCodeGenWriteBarrier((&___onEndDrag_19), value);
	}

	inline static int32_t get_offset_of_onDrop_20() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onDrop_20)); }
	inline Subject_1_t3896557479 * get_onDrop_20() const { return ___onDrop_20; }
	inline Subject_1_t3896557479 ** get_address_of_onDrop_20() { return &___onDrop_20; }
	inline void set_onDrop_20(Subject_1_t3896557479 * value)
	{
		___onDrop_20 = value;
		Il2CppCodeGenWriteBarrier((&___onDrop_20), value);
	}

	inline static int32_t get_offset_of_onUpdateSelected_21() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onUpdateSelected_21)); }
	inline Subject_1_t3991683920 * get_onUpdateSelected_21() const { return ___onUpdateSelected_21; }
	inline Subject_1_t3991683920 ** get_address_of_onUpdateSelected_21() { return &___onUpdateSelected_21; }
	inline void set_onUpdateSelected_21(Subject_1_t3991683920 * value)
	{
		___onUpdateSelected_21 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdateSelected_21), value);
	}

	inline static int32_t get_offset_of_onInitializePotentialDrag_22() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onInitializePotentialDrag_22)); }
	inline Subject_1_t3896557479 * get_onInitializePotentialDrag_22() const { return ___onInitializePotentialDrag_22; }
	inline Subject_1_t3896557479 ** get_address_of_onInitializePotentialDrag_22() { return &___onInitializePotentialDrag_22; }
	inline void set_onInitializePotentialDrag_22(Subject_1_t3896557479 * value)
	{
		___onInitializePotentialDrag_22 = value;
		Il2CppCodeGenWriteBarrier((&___onInitializePotentialDrag_22), value);
	}

	inline static int32_t get_offset_of_onCancel_23() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onCancel_23)); }
	inline Subject_1_t3991683920 * get_onCancel_23() const { return ___onCancel_23; }
	inline Subject_1_t3991683920 ** get_address_of_onCancel_23() { return &___onCancel_23; }
	inline void set_onCancel_23(Subject_1_t3991683920 * value)
	{
		___onCancel_23 = value;
		Il2CppCodeGenWriteBarrier((&___onCancel_23), value);
	}

	inline static int32_t get_offset_of_onScroll_24() { return static_cast<int32_t>(offsetof(ObservableEventTrigger_t2448499564, ___onScroll_24)); }
	inline Subject_1_t3896557479 * get_onScroll_24() const { return ___onScroll_24; }
	inline Subject_1_t3896557479 ** get_address_of_onScroll_24() { return &___onScroll_24; }
	inline void set_onScroll_24(Subject_1_t3896557479 * value)
	{
		___onScroll_24 = value;
		Il2CppCodeGenWriteBarrier((&___onScroll_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEEVENTTRIGGER_T2448499564_H
#ifndef OBSERVABLEFIXEDUPDATETRIGGER_T1561083989_H
#define OBSERVABLEFIXEDUPDATETRIGGER_T1561083989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableFixedUpdateTrigger
struct  ObservableFixedUpdateTrigger_t1561083989  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableFixedUpdateTrigger::fixedUpdate
	Subject_1_t3450905854 * ___fixedUpdate_8;

public:
	inline static int32_t get_offset_of_fixedUpdate_8() { return static_cast<int32_t>(offsetof(ObservableFixedUpdateTrigger_t1561083989, ___fixedUpdate_8)); }
	inline Subject_1_t3450905854 * get_fixedUpdate_8() const { return ___fixedUpdate_8; }
	inline Subject_1_t3450905854 ** get_address_of_fixedUpdate_8() { return &___fixedUpdate_8; }
	inline void set_fixedUpdate_8(Subject_1_t3450905854 * value)
	{
		___fixedUpdate_8 = value;
		Il2CppCodeGenWriteBarrier((&___fixedUpdate_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEFIXEDUPDATETRIGGER_T1561083989_H
#ifndef OBSERVABLEINITIALIZEPOTENTIALDRAGTRIGGER_T1310560508_H
#define OBSERVABLEINITIALIZEPOTENTIALDRAGTRIGGER_T1310560508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableInitializePotentialDragTrigger
struct  ObservableInitializePotentialDragTrigger_t1310560508  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservableInitializePotentialDragTrigger::onInitializePotentialDrag
	Subject_1_t3896557479 * ___onInitializePotentialDrag_8;

public:
	inline static int32_t get_offset_of_onInitializePotentialDrag_8() { return static_cast<int32_t>(offsetof(ObservableInitializePotentialDragTrigger_t1310560508, ___onInitializePotentialDrag_8)); }
	inline Subject_1_t3896557479 * get_onInitializePotentialDrag_8() const { return ___onInitializePotentialDrag_8; }
	inline Subject_1_t3896557479 ** get_address_of_onInitializePotentialDrag_8() { return &___onInitializePotentialDrag_8; }
	inline void set_onInitializePotentialDrag_8(Subject_1_t3896557479 * value)
	{
		___onInitializePotentialDrag_8 = value;
		Il2CppCodeGenWriteBarrier((&___onInitializePotentialDrag_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEINITIALIZEPOTENTIALDRAGTRIGGER_T1310560508_H
#ifndef OBSERVABLELATEUPDATETRIGGER_T376606132_H
#define OBSERVABLELATEUPDATETRIGGER_T376606132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableLateUpdateTrigger
struct  ObservableLateUpdateTrigger_t376606132  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableLateUpdateTrigger::lateUpdate
	Subject_1_t3450905854 * ___lateUpdate_8;

public:
	inline static int32_t get_offset_of_lateUpdate_8() { return static_cast<int32_t>(offsetof(ObservableLateUpdateTrigger_t376606132, ___lateUpdate_8)); }
	inline Subject_1_t3450905854 * get_lateUpdate_8() const { return ___lateUpdate_8; }
	inline Subject_1_t3450905854 ** get_address_of_lateUpdate_8() { return &___lateUpdate_8; }
	inline void set_lateUpdate_8(Subject_1_t3450905854 * value)
	{
		___lateUpdate_8 = value;
		Il2CppCodeGenWriteBarrier((&___lateUpdate_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLELATEUPDATETRIGGER_T376606132_H
#ifndef OBSERVABLEMOVETRIGGER_T613944432_H
#define OBSERVABLEMOVETRIGGER_T613944432_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableMoveTrigger
struct  ObservableMoveTrigger_t613944432  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.AxisEventData> UniRx.Triggers.ObservableMoveTrigger::onMove
	Subject_1_t2419900039 * ___onMove_8;

public:
	inline static int32_t get_offset_of_onMove_8() { return static_cast<int32_t>(offsetof(ObservableMoveTrigger_t613944432, ___onMove_8)); }
	inline Subject_1_t2419900039 * get_onMove_8() const { return ___onMove_8; }
	inline Subject_1_t2419900039 ** get_address_of_onMove_8() { return &___onMove_8; }
	inline void set_onMove_8(Subject_1_t2419900039 * value)
	{
		___onMove_8 = value;
		Il2CppCodeGenWriteBarrier((&___onMove_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEMOVETRIGGER_T613944432_H
#ifndef OBSERVABLEPARTICLETRIGGER_T795844121_H
#define OBSERVABLEPARTICLETRIGGER_T795844121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableParticleTrigger
struct  ObservableParticleTrigger_t795844121  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.GameObject> UniRx.Triggers.ObservableParticleTrigger::onParticleCollision
	Subject_1_t1202293006 * ___onParticleCollision_8;
	// UniRx.Subject`1<UniRx.Unit> UniRx.Triggers.ObservableParticleTrigger::onParticleTrigger
	Subject_1_t3450905854 * ___onParticleTrigger_9;

public:
	inline static int32_t get_offset_of_onParticleCollision_8() { return static_cast<int32_t>(offsetof(ObservableParticleTrigger_t795844121, ___onParticleCollision_8)); }
	inline Subject_1_t1202293006 * get_onParticleCollision_8() const { return ___onParticleCollision_8; }
	inline Subject_1_t1202293006 ** get_address_of_onParticleCollision_8() { return &___onParticleCollision_8; }
	inline void set_onParticleCollision_8(Subject_1_t1202293006 * value)
	{
		___onParticleCollision_8 = value;
		Il2CppCodeGenWriteBarrier((&___onParticleCollision_8), value);
	}

	inline static int32_t get_offset_of_onParticleTrigger_9() { return static_cast<int32_t>(offsetof(ObservableParticleTrigger_t795844121, ___onParticleTrigger_9)); }
	inline Subject_1_t3450905854 * get_onParticleTrigger_9() const { return ___onParticleTrigger_9; }
	inline Subject_1_t3450905854 ** get_address_of_onParticleTrigger_9() { return &___onParticleTrigger_9; }
	inline void set_onParticleTrigger_9(Subject_1_t3450905854 * value)
	{
		___onParticleTrigger_9 = value;
		Il2CppCodeGenWriteBarrier((&___onParticleTrigger_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEPARTICLETRIGGER_T795844121_H
#ifndef OBSERVABLEPOINTERCLICKTRIGGER_T1811202022_H
#define OBSERVABLEPOINTERCLICKTRIGGER_T1811202022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservablePointerClickTrigger
struct  ObservablePointerClickTrigger_t1811202022  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservablePointerClickTrigger::onPointerClick
	Subject_1_t3896557479 * ___onPointerClick_8;

public:
	inline static int32_t get_offset_of_onPointerClick_8() { return static_cast<int32_t>(offsetof(ObservablePointerClickTrigger_t1811202022, ___onPointerClick_8)); }
	inline Subject_1_t3896557479 * get_onPointerClick_8() const { return ___onPointerClick_8; }
	inline Subject_1_t3896557479 ** get_address_of_onPointerClick_8() { return &___onPointerClick_8; }
	inline void set_onPointerClick_8(Subject_1_t3896557479 * value)
	{
		___onPointerClick_8 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerClick_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEPOINTERCLICKTRIGGER_T1811202022_H
#ifndef OBSERVABLEPOINTERDOWNTRIGGER_T2158527878_H
#define OBSERVABLEPOINTERDOWNTRIGGER_T2158527878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservablePointerDownTrigger
struct  ObservablePointerDownTrigger_t2158527878  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservablePointerDownTrigger::onPointerDown
	Subject_1_t3896557479 * ___onPointerDown_8;

public:
	inline static int32_t get_offset_of_onPointerDown_8() { return static_cast<int32_t>(offsetof(ObservablePointerDownTrigger_t2158527878, ___onPointerDown_8)); }
	inline Subject_1_t3896557479 * get_onPointerDown_8() const { return ___onPointerDown_8; }
	inline Subject_1_t3896557479 ** get_address_of_onPointerDown_8() { return &___onPointerDown_8; }
	inline void set_onPointerDown_8(Subject_1_t3896557479 * value)
	{
		___onPointerDown_8 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerDown_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEPOINTERDOWNTRIGGER_T2158527878_H
#ifndef OBSERVABLEPOINTERENTERTRIGGER_T3766810110_H
#define OBSERVABLEPOINTERENTERTRIGGER_T3766810110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservablePointerEnterTrigger
struct  ObservablePointerEnterTrigger_t3766810110  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.EventSystems.PointerEventData> UniRx.Triggers.ObservablePointerEnterTrigger::onPointerEnter
	Subject_1_t3896557479 * ___onPointerEnter_8;

public:
	inline static int32_t get_offset_of_onPointerEnter_8() { return static_cast<int32_t>(offsetof(ObservablePointerEnterTrigger_t3766810110, ___onPointerEnter_8)); }
	inline Subject_1_t3896557479 * get_onPointerEnter_8() const { return ___onPointerEnter_8; }
	inline Subject_1_t3896557479 ** get_address_of_onPointerEnter_8() { return &___onPointerEnter_8; }
	inline void set_onPointerEnter_8(Subject_1_t3896557479 * value)
	{
		___onPointerEnter_8 = value;
		Il2CppCodeGenWriteBarrier((&___onPointerEnter_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEPOINTERENTERTRIGGER_T3766810110_H
#ifndef OBSERVABLETRIGGERTRIGGER_T3353727038_H
#define OBSERVABLETRIGGERTRIGGER_T3353727038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Triggers.ObservableTriggerTrigger
struct  ObservableTriggerTrigger_t3353727038  : public ObservableTriggerBase_t3020654715
{
public:
	// UniRx.Subject`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerTrigger::onTriggerEnter
	Subject_1_t1862003397 * ___onTriggerEnter_8;
	// UniRx.Subject`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerTrigger::onTriggerExit
	Subject_1_t1862003397 * ___onTriggerExit_9;
	// UniRx.Subject`1<UnityEngine.Collider> UniRx.Triggers.ObservableTriggerTrigger::onTriggerStay
	Subject_1_t1862003397 * ___onTriggerStay_10;

public:
	inline static int32_t get_offset_of_onTriggerEnter_8() { return static_cast<int32_t>(offsetof(ObservableTriggerTrigger_t3353727038, ___onTriggerEnter_8)); }
	inline Subject_1_t1862003397 * get_onTriggerEnter_8() const { return ___onTriggerEnter_8; }
	inline Subject_1_t1862003397 ** get_address_of_onTriggerEnter_8() { return &___onTriggerEnter_8; }
	inline void set_onTriggerEnter_8(Subject_1_t1862003397 * value)
	{
		___onTriggerEnter_8 = value;
		Il2CppCodeGenWriteBarrier((&___onTriggerEnter_8), value);
	}

	inline static int32_t get_offset_of_onTriggerExit_9() { return static_cast<int32_t>(offsetof(ObservableTriggerTrigger_t3353727038, ___onTriggerExit_9)); }
	inline Subject_1_t1862003397 * get_onTriggerExit_9() const { return ___onTriggerExit_9; }
	inline Subject_1_t1862003397 ** get_address_of_onTriggerExit_9() { return &___onTriggerExit_9; }
	inline void set_onTriggerExit_9(Subject_1_t1862003397 * value)
	{
		___onTriggerExit_9 = value;
		Il2CppCodeGenWriteBarrier((&___onTriggerExit_9), value);
	}

	inline static int32_t get_offset_of_onTriggerStay_10() { return static_cast<int32_t>(offsetof(ObservableTriggerTrigger_t3353727038, ___onTriggerStay_10)); }
	inline Subject_1_t1862003397 * get_onTriggerStay_10() const { return ___onTriggerStay_10; }
	inline Subject_1_t1862003397 ** get_address_of_onTriggerStay_10() { return &___onTriggerStay_10; }
	inline void set_onTriggerStay_10(Subject_1_t1862003397 * value)
	{
		___onTriggerStay_10 = value;
		Il2CppCodeGenWriteBarrier((&___onTriggerStay_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLETRIGGERTRIGGER_T3353727038_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3100 = { sizeof (ReactiveCommand_t2994750062), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3101 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3101[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3102 = { sizeof (AsyncReactiveCommand_t3750662548), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3103 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3103[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3104 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3104[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3105 = { sizeof (ReactiveCommandExtensions_t1223375871), -1, sizeof(ReactiveCommandExtensions_t1223375871_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3105[2] = 
{
	ReactiveCommandExtensions_t1223375871_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	ReactiveCommandExtensions_t1223375871_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3106 = { sizeof (AsyncReactiveCommandExtensions_t454737267), -1, sizeof(AsyncReactiveCommandExtensions_t454737267_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3106[2] = 
{
	AsyncReactiveCommandExtensions_t454737267_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	AsyncReactiveCommandExtensions_t454737267_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3107 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3107[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3108 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3108[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3109 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3109[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3110 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3111 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3112 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3112[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3113 = { sizeof (ReactiveDictionaryExtensions_t2909995766), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3114 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3115 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3116 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3116[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3117 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3117[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3118 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3118[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3119 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3119[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3120 = { sizeof (ReactivePropertyExtensions_t2855915014), -1, sizeof(ReactivePropertyExtensions_t2855915014_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3120[2] = 
{
	ReactivePropertyExtensions_t2855915014_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	ReactivePropertyExtensions_t2855915014_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3121 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3121[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3122 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3122[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3123 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3123[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3124 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3124[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3125 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3125[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3126 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3126[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3127 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3127[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3128 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3128[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3129 = { sizeof (ObservableAnimatorTrigger_t4059379110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3129[2] = 
{
	ObservableAnimatorTrigger_t4059379110::get_offset_of_onAnimatorIK_8(),
	ObservableAnimatorTrigger_t4059379110::get_offset_of_onAnimatorMove_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3130 = { sizeof (ObservableBeginDragTrigger_t457401454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3130[1] = 
{
	ObservableBeginDragTrigger_t457401454::get_offset_of_onBeginDrag_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3131 = { sizeof (ObservableCancelTrigger_t3203487127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3131[1] = 
{
	ObservableCancelTrigger_t3203487127::get_offset_of_onCancel_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3132 = { sizeof (ObservableCanvasGroupChangedTrigger_t3835595784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3132[1] = 
{
	ObservableCanvasGroupChangedTrigger_t3835595784::get_offset_of_onCanvasGroupChanged_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3133 = { sizeof (ObservableCollision2DTrigger_t573452484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3133[3] = 
{
	ObservableCollision2DTrigger_t573452484::get_offset_of_onCollisionEnter2D_8(),
	ObservableCollision2DTrigger_t573452484::get_offset_of_onCollisionExit2D_9(),
	ObservableCollision2DTrigger_t573452484::get_offset_of_onCollisionStay2D_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3134 = { sizeof (ObservableCollisionTrigger_t3942043460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3134[3] = 
{
	ObservableCollisionTrigger_t3942043460::get_offset_of_onCollisionEnter_8(),
	ObservableCollisionTrigger_t3942043460::get_offset_of_onCollisionExit_9(),
	ObservableCollisionTrigger_t3942043460::get_offset_of_onCollisionStay_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3135 = { sizeof (ObservableDeselectTrigger_t981271967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3135[1] = 
{
	ObservableDeselectTrigger_t981271967::get_offset_of_onDeselect_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3136 = { sizeof (ObservableDestroyTrigger_t3144203490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3136[5] = 
{
	ObservableDestroyTrigger_t3144203490::get_offset_of_calledDestroy_2(),
	ObservableDestroyTrigger_t3144203490::get_offset_of_onDestroy_3(),
	ObservableDestroyTrigger_t3144203490::get_offset_of_disposablesOnDestroy_4(),
	ObservableDestroyTrigger_t3144203490::get_offset_of_U3CIsMonitoredActivateU3Ek__BackingField_5(),
	ObservableDestroyTrigger_t3144203490::get_offset_of_U3CIsActivatedU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3137 = { sizeof (ObservableDragTrigger_t1354517533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3137[1] = 
{
	ObservableDragTrigger_t1354517533::get_offset_of_onDrag_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3138 = { sizeof (ObservableDropTrigger_t3729934644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3138[1] = 
{
	ObservableDropTrigger_t3729934644::get_offset_of_onDrop_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3139 = { sizeof (ObservableEnableTrigger_t332491237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3139[2] = 
{
	ObservableEnableTrigger_t332491237::get_offset_of_onEnable_8(),
	ObservableEnableTrigger_t332491237::get_offset_of_onDisable_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3140 = { sizeof (ObservableEndDragTrigger_t832788659), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3140[1] = 
{
	ObservableEndDragTrigger_t832788659::get_offset_of_onEndDrag_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3141 = { sizeof (ObservableEventTrigger_t2448499564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3141[17] = 
{
	ObservableEventTrigger_t2448499564::get_offset_of_onDeselect_8(),
	ObservableEventTrigger_t2448499564::get_offset_of_onMove_9(),
	ObservableEventTrigger_t2448499564::get_offset_of_onPointerDown_10(),
	ObservableEventTrigger_t2448499564::get_offset_of_onPointerEnter_11(),
	ObservableEventTrigger_t2448499564::get_offset_of_onPointerExit_12(),
	ObservableEventTrigger_t2448499564::get_offset_of_onPointerUp_13(),
	ObservableEventTrigger_t2448499564::get_offset_of_onSelect_14(),
	ObservableEventTrigger_t2448499564::get_offset_of_onPointerClick_15(),
	ObservableEventTrigger_t2448499564::get_offset_of_onSubmit_16(),
	ObservableEventTrigger_t2448499564::get_offset_of_onDrag_17(),
	ObservableEventTrigger_t2448499564::get_offset_of_onBeginDrag_18(),
	ObservableEventTrigger_t2448499564::get_offset_of_onEndDrag_19(),
	ObservableEventTrigger_t2448499564::get_offset_of_onDrop_20(),
	ObservableEventTrigger_t2448499564::get_offset_of_onUpdateSelected_21(),
	ObservableEventTrigger_t2448499564::get_offset_of_onInitializePotentialDrag_22(),
	ObservableEventTrigger_t2448499564::get_offset_of_onCancel_23(),
	ObservableEventTrigger_t2448499564::get_offset_of_onScroll_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3142 = { sizeof (ObservableFixedUpdateTrigger_t1561083989), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3142[1] = 
{
	ObservableFixedUpdateTrigger_t1561083989::get_offset_of_fixedUpdate_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3143 = { sizeof (ObservableInitializePotentialDragTrigger_t1310560508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3143[1] = 
{
	ObservableInitializePotentialDragTrigger_t1310560508::get_offset_of_onInitializePotentialDrag_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3144 = { sizeof (ObservableLateUpdateTrigger_t376606132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3144[1] = 
{
	ObservableLateUpdateTrigger_t376606132::get_offset_of_lateUpdate_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3145 = { sizeof (ObservableMoveTrigger_t613944432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3145[1] = 
{
	ObservableMoveTrigger_t613944432::get_offset_of_onMove_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3146 = { sizeof (ObservableParticleTrigger_t795844121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3146[2] = 
{
	ObservableParticleTrigger_t795844121::get_offset_of_onParticleCollision_8(),
	ObservableParticleTrigger_t795844121::get_offset_of_onParticleTrigger_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3147 = { sizeof (ObservablePointerClickTrigger_t1811202022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3147[1] = 
{
	ObservablePointerClickTrigger_t1811202022::get_offset_of_onPointerClick_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3148 = { sizeof (ObservablePointerDownTrigger_t2158527878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3148[1] = 
{
	ObservablePointerDownTrigger_t2158527878::get_offset_of_onPointerDown_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3149 = { sizeof (ObservablePointerEnterTrigger_t3766810110), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3149[1] = 
{
	ObservablePointerEnterTrigger_t3766810110::get_offset_of_onPointerEnter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3150 = { sizeof (ObservablePointerExitTrigger_t1106360683), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3150[1] = 
{
	ObservablePointerExitTrigger_t1106360683::get_offset_of_onPointerExit_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3151 = { sizeof (ObservablePointerUpTrigger_t3544017431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3151[1] = 
{
	ObservablePointerUpTrigger_t3544017431::get_offset_of_onPointerUp_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3152 = { sizeof (ObservableRectTransformTrigger_t4048819422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3152[2] = 
{
	ObservableRectTransformTrigger_t4048819422::get_offset_of_onRectTransformDimensionsChange_8(),
	ObservableRectTransformTrigger_t4048819422::get_offset_of_onRectTransformRemoved_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3153 = { sizeof (ObservableScrollTrigger_t4106240645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3153[1] = 
{
	ObservableScrollTrigger_t4106240645::get_offset_of_onScroll_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3154 = { sizeof (ObservableSelectTrigger_t629978404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3154[1] = 
{
	ObservableSelectTrigger_t629978404::get_offset_of_onSelect_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3155 = { sizeof (ObservableStateMachineTrigger_t514432755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3155[6] = 
{
	ObservableStateMachineTrigger_t514432755::get_offset_of_onStateExit_2(),
	ObservableStateMachineTrigger_t514432755::get_offset_of_onStateEnter_3(),
	ObservableStateMachineTrigger_t514432755::get_offset_of_onStateIK_4(),
	ObservableStateMachineTrigger_t514432755::get_offset_of_onStateUpdate_5(),
	ObservableStateMachineTrigger_t514432755::get_offset_of_onStateMachineEnter_6(),
	ObservableStateMachineTrigger_t514432755::get_offset_of_onStateMachineExit_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3156 = { sizeof (OnStateInfo_t3956129159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3156[3] = 
{
	OnStateInfo_t3956129159::get_offset_of_U3CAnimatorU3Ek__BackingField_0(),
	OnStateInfo_t3956129159::get_offset_of_U3CStateInfoU3Ek__BackingField_1(),
	OnStateInfo_t3956129159::get_offset_of_U3CLayerIndexU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3157 = { sizeof (OnStateMachineInfo_t1139369480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3157[2] = 
{
	OnStateMachineInfo_t1139369480::get_offset_of_U3CAnimatorU3Ek__BackingField_0(),
	OnStateMachineInfo_t1139369480::get_offset_of_U3CStateMachinePathHashU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3158 = { sizeof (ObservableSubmitTrigger_t3261832817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3158[1] = 
{
	ObservableSubmitTrigger_t3261832817::get_offset_of_onSubmit_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3159 = { sizeof (ObservableTransformChangedTrigger_t829483795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3159[3] = 
{
	ObservableTransformChangedTrigger_t829483795::get_offset_of_onBeforeTransformParentChanged_8(),
	ObservableTransformChangedTrigger_t829483795::get_offset_of_onTransformParentChanged_9(),
	ObservableTransformChangedTrigger_t829483795::get_offset_of_onTransformChildrenChanged_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3160 = { sizeof (ObservableTrigger2DTrigger_t3116596712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3160[3] = 
{
	ObservableTrigger2DTrigger_t3116596712::get_offset_of_onTriggerEnter2D_8(),
	ObservableTrigger2DTrigger_t3116596712::get_offset_of_onTriggerExit2D_9(),
	ObservableTrigger2DTrigger_t3116596712::get_offset_of_onTriggerStay2D_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3161 = { sizeof (ObservableTriggerBase_t3020654715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3161[6] = 
{
	ObservableTriggerBase_t3020654715::get_offset_of_calledAwake_2(),
	ObservableTriggerBase_t3020654715::get_offset_of_awake_3(),
	ObservableTriggerBase_t3020654715::get_offset_of_calledStart_4(),
	ObservableTriggerBase_t3020654715::get_offset_of_start_5(),
	ObservableTriggerBase_t3020654715::get_offset_of_calledDestroy_6(),
	ObservableTriggerBase_t3020654715::get_offset_of_onDestroy_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3162 = { sizeof (ObservableTriggerExtensions_t3436728118), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3163 = { sizeof (ObservableTriggerTrigger_t3353727038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3163[3] = 
{
	ObservableTriggerTrigger_t3353727038::get_offset_of_onTriggerEnter_8(),
	ObservableTriggerTrigger_t3353727038::get_offset_of_onTriggerExit_9(),
	ObservableTriggerTrigger_t3353727038::get_offset_of_onTriggerStay_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3164 = { sizeof (ObservableUpdateSelectedTrigger_t2582606763), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3164[1] = 
{
	ObservableUpdateSelectedTrigger_t2582606763::get_offset_of_onUpdateSelected_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3165 = { sizeof (ObservableUpdateTrigger_t3947578800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3165[1] = 
{
	ObservableUpdateTrigger_t3947578800::get_offset_of_update_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3166 = { sizeof (ObservableVisibleTrigger_t1755347736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3166[2] = 
{
	ObservableVisibleTrigger_t1755347736::get_offset_of_onBecameInvisible_8(),
	ObservableVisibleTrigger_t1755347736::get_offset_of_onBecameVisible_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3167 = { sizeof (UnityEqualityComparer_t3411193022), -1, sizeof(UnityEqualityComparer_t3411193022_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3167[14] = 
{
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_Vector2_0(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_Vector3_1(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_Vector4_2(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_Color_3(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_Rect_4(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_Bounds_5(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_Quaternion_6(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_vector2Type_7(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_vector3Type_8(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_vector4Type_9(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_colorType_10(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_rectType_11(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_boundsType_12(),
	UnityEqualityComparer_t3411193022_StaticFields::get_offset_of_quaternionType_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3168 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3168[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3169 = { sizeof (Vector2EqualityComparer_t3698555085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3170 = { sizeof (Vector3EqualityComparer_t4055612966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3171 = { sizeof (Vector4EqualityComparer_t1796746215), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3172 = { sizeof (ColorEqualityComparer_t1673499971), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3173 = { sizeof (RectEqualityComparer_t3798131596), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3174 = { sizeof (BoundsEqualityComparer_t1049822962), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3175 = { sizeof (QuaternionEqualityComparer_t2214622964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3176 = { sizeof (UnityEventExtensions_t3448704846), -1, sizeof(UnityEventExtensions_t3448704846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3176[1] = 
{
	UnityEventExtensions_t3448704846_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3177 = { sizeof (U3CAsObservableU3Ec__AnonStorey0_t805315905), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3177[1] = 
{
	U3CAsObservableU3Ec__AnonStorey0_t805315905::get_offset_of_unityEvent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3178 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3178[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3179 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3179[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3180 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3180[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3181 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3181[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3182 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3182[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3183 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3183[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3184 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3184[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3185 = { sizeof (UnityGraphicExtensions_t1598321454), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3186 = { sizeof (U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey1_t849930747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3186[1] = 
{
	U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey1_t849930747::get_offset_of_graphic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3187 = { sizeof (U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey0_t159790967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3187[3] = 
{
	U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey0_t159790967::get_offset_of_observer_0(),
	U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey0_t159790967::get_offset_of_registerHandler_1(),
	U3CDirtyLayoutCallbackAsObservableU3Ec__AnonStorey0_t159790967::get_offset_of_U3CU3Ef__refU241_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3188 = { sizeof (U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey3_t1873439977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3188[1] = 
{
	U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey3_t1873439977::get_offset_of_graphic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3189 = { sizeof (U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey2_t1705548818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3189[3] = 
{
	U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey2_t1705548818::get_offset_of_observer_0(),
	U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey2_t1705548818::get_offset_of_registerHandler_1(),
	U3CDirtyMaterialCallbackAsObservableU3Ec__AnonStorey2_t1705548818::get_offset_of_U3CU3Ef__refU243_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3190 = { sizeof (U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey5_t2034475028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3190[1] = 
{
	U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey5_t2034475028::get_offset_of_graphic_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3191 = { sizeof (U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey4_t3780349813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3191[3] = 
{
	U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey4_t3780349813::get_offset_of_observer_0(),
	U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey4_t3780349813::get_offset_of_registerHandler_1(),
	U3CDirtyVerticesCallbackAsObservableU3Ec__AnonStorey4_t3780349813::get_offset_of_U3CU3Ef__refU245_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3192 = { sizeof (UnityUIComponentExtensions_t484683978), -1, sizeof(UnityUIComponentExtensions_t484683978_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3192[8] = 
{
	UnityUIComponentExtensions_t484683978_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	UnityUIComponentExtensions_t484683978_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	UnityUIComponentExtensions_t484683978_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	UnityUIComponentExtensions_t484683978_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	UnityUIComponentExtensions_t484683978_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	UnityUIComponentExtensions_t484683978_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	UnityUIComponentExtensions_t484683978_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	UnityUIComponentExtensions_t484683978_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3193 = { sizeof (YieldInstructionCache_t455193914), -1, sizeof(YieldInstructionCache_t455193914_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3193[2] = 
{
	YieldInstructionCache_t455193914_StaticFields::get_offset_of_WaitForEndOfFrame_0(),
	YieldInstructionCache_t455193914_StaticFields::get_offset_of_WaitForFixedUpdate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3194 = { sizeof (Benchmark01_t1571072624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3194[10] = 
{
	Benchmark01_t1571072624::get_offset_of_BenchmarkType_2(),
	Benchmark01_t1571072624::get_offset_of_TMProFont_3(),
	Benchmark01_t1571072624::get_offset_of_TextMeshFont_4(),
	Benchmark01_t1571072624::get_offset_of_m_textMeshPro_5(),
	Benchmark01_t1571072624::get_offset_of_m_textContainer_6(),
	Benchmark01_t1571072624::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_t1571072624::get_offset_of_m_material01_10(),
	Benchmark01_t1571072624::get_offset_of_m_material02_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3195 = { sizeof (U3CStartU3Ec__Iterator0_t2216151886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3195[5] = 
{
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3196 = { sizeof (Benchmark01_UGUI_t3264177817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3196[10] = 
{
	Benchmark01_UGUI_t3264177817::get_offset_of_BenchmarkType_2(),
	Benchmark01_UGUI_t3264177817::get_offset_of_canvas_3(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TMProFont_4(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TextMeshFont_5(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMeshPro_6(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material01_10(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material02_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3197 = { sizeof (U3CStartU3Ec__Iterator0_t2622988697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3197[5] = 
{
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3198 = { sizeof (Benchmark02_t1571269232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3198[3] = 
{
	Benchmark02_t1571269232::get_offset_of_SpawnType_2(),
	Benchmark02_t1571269232::get_offset_of_NumberOfNPC_3(),
	Benchmark02_t1571269232::get_offset_of_floatingText_Script_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3199 = { sizeof (Benchmark03_t1571203696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3199[3] = 
{
	Benchmark03_t1571203696::get_offset_of_SpawnType_2(),
	Benchmark03_t1571203696::get_offset_of_NumberOfNPC_3(),
	Benchmark03_t1571203696::get_offset_of_TheFont_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
