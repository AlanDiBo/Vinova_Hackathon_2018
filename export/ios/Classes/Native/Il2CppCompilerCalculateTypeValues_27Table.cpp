﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UniRx.IObserver`1<System.Int32>
struct IObserver_1_t400248036;
// System.IDisposable
struct IDisposable_t3640265483;
// UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey1
struct U3CSubscribeCoreU3Ec__AnonStorey1_t788223763;
// UniRx.Operators.RangeObservable
struct RangeObservable_t805001503;
// UniRx.IScheduler
struct IScheduler_t411218504;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef OPERATOROBSERVERBASE_2_T1767045806_H
#define OPERATOROBSERVERBASE_2_T1767045806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObserverBase`2<System.Int32,System.Int32>
struct  OperatorObserverBase_2_t1767045806  : public RuntimeObject
{
public:
	// UniRx.IObserver`1<TResult> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.OperatorObserverBase`2::observer
	RuntimeObject* ___observer_0;
	// System.IDisposable UniRx.Operators.OperatorObserverBase`2::cancel
	RuntimeObject* ___cancel_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t1767045806, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((&___observer_0), value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t1767045806, ___cancel_1)); }
	inline RuntimeObject* get_cancel_1() const { return ___cancel_1; }
	inline RuntimeObject** get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(RuntimeObject* value)
	{
		___cancel_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancel_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVERBASE_2_T1767045806_H
#ifndef OPERATOROBSERVABLEBASE_1_T4238383138_H
#define OPERATOROBSERVABLEBASE_1_T4238383138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<System.Int32>
struct  OperatorObservableBase_1_t4238383138  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t4238383138, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T4238383138_H
#ifndef IMMUTABLERETURNFALSEOBSERVABLE_T3762316750_H
#define IMMUTABLERETURNFALSEOBSERVABLE_T3762316750_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ImmutableReturnFalseObservable
struct  ImmutableReturnFalseObservable_t3762316750  : public RuntimeObject
{
public:

public:
};

struct ImmutableReturnFalseObservable_t3762316750_StaticFields
{
public:
	// UniRx.Operators.ImmutableReturnFalseObservable UniRx.Operators.ImmutableReturnFalseObservable::Instance
	ImmutableReturnFalseObservable_t3762316750 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(ImmutableReturnFalseObservable_t3762316750_StaticFields, ___Instance_0)); }
	inline ImmutableReturnFalseObservable_t3762316750 * get_Instance_0() const { return ___Instance_0; }
	inline ImmutableReturnFalseObservable_t3762316750 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(ImmutableReturnFalseObservable_t3762316750 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMMUTABLERETURNFALSEOBSERVABLE_T3762316750_H
#ifndef IMMUTABLERETURNTRUEOBSERVABLE_T1352146081_H
#define IMMUTABLERETURNTRUEOBSERVABLE_T1352146081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ImmutableReturnTrueObservable
struct  ImmutableReturnTrueObservable_t1352146081  : public RuntimeObject
{
public:

public:
};

struct ImmutableReturnTrueObservable_t1352146081_StaticFields
{
public:
	// UniRx.Operators.ImmutableReturnTrueObservable UniRx.Operators.ImmutableReturnTrueObservable::Instance
	ImmutableReturnTrueObservable_t1352146081 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(ImmutableReturnTrueObservable_t1352146081_StaticFields, ___Instance_0)); }
	inline ImmutableReturnTrueObservable_t1352146081 * get_Instance_0() const { return ___Instance_0; }
	inline ImmutableReturnTrueObservable_t1352146081 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(ImmutableReturnTrueObservable_t1352146081 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMMUTABLERETURNTRUEOBSERVABLE_T1352146081_H
#ifndef IMMUTABLERETURNUNITOBSERVABLE_T4161431038_H
#define IMMUTABLERETURNUNITOBSERVABLE_T4161431038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.ImmutableReturnUnitObservable
struct  ImmutableReturnUnitObservable_t4161431038  : public RuntimeObject
{
public:

public:
};

struct ImmutableReturnUnitObservable_t4161431038_StaticFields
{
public:
	// UniRx.Operators.ImmutableReturnUnitObservable UniRx.Operators.ImmutableReturnUnitObservable::Instance
	ImmutableReturnUnitObservable_t4161431038 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(ImmutableReturnUnitObservable_t4161431038_StaticFields, ___Instance_0)); }
	inline ImmutableReturnUnitObservable_t4161431038 * get_Instance_0() const { return ___Instance_0; }
	inline ImmutableReturnUnitObservable_t4161431038 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(ImmutableReturnUnitObservable_t4161431038 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMMUTABLERETURNUNITOBSERVABLE_T4161431038_H
#ifndef U3CSUBSCRIBECOREU3EC__ANONSTOREY0_T788223764_H
#define U3CSUBSCRIBECOREU3EC__ANONSTOREY0_T788223764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey0
struct  U3CSubscribeCoreU3Ec__AnonStorey0_t788223764  : public RuntimeObject
{
public:
	// System.Int32 UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey0::i
	int32_t ___i_0;
	// UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey1 UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey0::<>f__ref$1
	U3CSubscribeCoreU3Ec__AnonStorey1_t788223763 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_i_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey0_t788223764, ___i_0)); }
	inline int32_t get_i_0() const { return ___i_0; }
	inline int32_t* get_address_of_i_0() { return &___i_0; }
	inline void set_i_0(int32_t value)
	{
		___i_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey0_t788223764, ___U3CU3Ef__refU241_1)); }
	inline U3CSubscribeCoreU3Ec__AnonStorey1_t788223763 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CSubscribeCoreU3Ec__AnonStorey1_t788223763 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CSubscribeCoreU3Ec__AnonStorey1_t788223763 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSUBSCRIBECOREU3EC__ANONSTOREY0_T788223764_H
#ifndef U3CSUBSCRIBECOREU3EC__ANONSTOREY1_T788223763_H
#define U3CSUBSCRIBECOREU3EC__ANONSTOREY1_T788223763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey1
struct  U3CSubscribeCoreU3Ec__AnonStorey1_t788223763  : public RuntimeObject
{
public:
	// UniRx.IObserver`1<System.Int32> UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey1::observer
	RuntimeObject* ___observer_0;
	// UniRx.Operators.RangeObservable UniRx.Operators.RangeObservable/<SubscribeCore>c__AnonStorey1::$this
	RangeObservable_t805001503 * ___U24this_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey1_t788223763, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((&___observer_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSubscribeCoreU3Ec__AnonStorey1_t788223763, ___U24this_1)); }
	inline RangeObservable_t805001503 * get_U24this_1() const { return ___U24this_1; }
	inline RangeObservable_t805001503 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RangeObservable_t805001503 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSUBSCRIBECOREU3EC__ANONSTOREY1_T788223763_H
#ifndef RANGE_T880295778_H
#define RANGE_T880295778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RangeObservable/Range
struct  Range_t880295778  : public OperatorObserverBase_2_t1767045806
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGE_T880295778_H
#ifndef RANGEOBSERVABLE_T805001503_H
#define RANGEOBSERVABLE_T805001503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.RangeObservable
struct  RangeObservable_t805001503  : public OperatorObservableBase_1_t4238383138
{
public:
	// System.Int32 UniRx.Operators.RangeObservable::start
	int32_t ___start_1;
	// System.Int32 UniRx.Operators.RangeObservable::count
	int32_t ___count_2;
	// UniRx.IScheduler UniRx.Operators.RangeObservable::scheduler
	RuntimeObject* ___scheduler_3;

public:
	inline static int32_t get_offset_of_start_1() { return static_cast<int32_t>(offsetof(RangeObservable_t805001503, ___start_1)); }
	inline int32_t get_start_1() const { return ___start_1; }
	inline int32_t* get_address_of_start_1() { return &___start_1; }
	inline void set_start_1(int32_t value)
	{
		___start_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(RangeObservable_t805001503, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_scheduler_3() { return static_cast<int32_t>(offsetof(RangeObservable_t805001503, ___scheduler_3)); }
	inline RuntimeObject* get_scheduler_3() const { return ___scheduler_3; }
	inline RuntimeObject** get_address_of_scheduler_3() { return &___scheduler_3; }
	inline void set_scheduler_3(RuntimeObject* value)
	{
		___scheduler_3 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEOBSERVABLE_T805001503_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2700 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2700[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2701 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2701[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2702 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2702[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2703 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2703[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2704 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2705 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2705[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2706 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2706[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2707 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2707[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2708 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2708[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2709 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2709[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2710 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2710[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2711 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2711[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2712 = { sizeof (RangeObservable_t805001503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2712[3] = 
{
	RangeObservable_t805001503::get_offset_of_start_1(),
	RangeObservable_t805001503::get_offset_of_count_2(),
	RangeObservable_t805001503::get_offset_of_scheduler_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2713 = { sizeof (Range_t880295778), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2714 = { sizeof (U3CSubscribeCoreU3Ec__AnonStorey1_t788223763), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2714[2] = 
{
	U3CSubscribeCoreU3Ec__AnonStorey1_t788223763::get_offset_of_observer_0(),
	U3CSubscribeCoreU3Ec__AnonStorey1_t788223763::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2715 = { sizeof (U3CSubscribeCoreU3Ec__AnonStorey0_t788223764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2715[2] = 
{
	U3CSubscribeCoreU3Ec__AnonStorey0_t788223764::get_offset_of_i_0(),
	U3CSubscribeCoreU3Ec__AnonStorey0_t788223764::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2716 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2716[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2717 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2717[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2718 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2718[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2719 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2719[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2720 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2721 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2721[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2722 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2722[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2723 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2723[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2724 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2724[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2725 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2725[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2726 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2727 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2727[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2728 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2728[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2729 = { sizeof (ImmutableReturnUnitObservable_t4161431038), -1, sizeof(ImmutableReturnUnitObservable_t4161431038_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2729[1] = 
{
	ImmutableReturnUnitObservable_t4161431038_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2730 = { sizeof (ImmutableReturnTrueObservable_t1352146081), -1, sizeof(ImmutableReturnTrueObservable_t1352146081_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2730[1] = 
{
	ImmutableReturnTrueObservable_t1352146081_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2731 = { sizeof (ImmutableReturnFalseObservable_t3762316750), -1, sizeof(ImmutableReturnFalseObservable_t3762316750_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2731[1] = 
{
	ImmutableReturnFalseObservable_t3762316750_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2732 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2732[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2733 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2733[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2734 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2734[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2735 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2735[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2736 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2736[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2737 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2737[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2738 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2738[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2739 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2739[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2740 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2740[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2741 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2742 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2742[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2743 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2743[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2744 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2744[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2745 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2745[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2746 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2746[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2747 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2747[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2748 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2748[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2749 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2749[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2750 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2750[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2751 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2751[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2752 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2752[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2753 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2753[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2754 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2754[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2755 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2755[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2756 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2756[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2757 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2757[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2758 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2758[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2759 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2759[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2760 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2760[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2761 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2761[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2762 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2762[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2763 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2763[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2764 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2764[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2765 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2765[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2766 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2766[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2767 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2767[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2768 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2768[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2769 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2769[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2770 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2770[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2771 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2771[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2772 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2772[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2773 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2773[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2774 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2774[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2775 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2775[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2776 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2776[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2777 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2777[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2778 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2778[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2779 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2779[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2780 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2780[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2781 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2781[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2782 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2782[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2783 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2783[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2784 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2784[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2785 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2785[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2786 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2786[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2787 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2787[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2788 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2788[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2789 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2789[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2790 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2790[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2791 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2791[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2792 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2792[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2793 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2793[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2794 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2794[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2795 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2795[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2796 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2796[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2797 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2797[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2798 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2798[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2799 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2799[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
