﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String[]
struct StringU5BU5D_t1281789340;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_t1300586518;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// socket.io.Socket
struct Socket_t590446146;
// System.String
struct String_t;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t2197129486;
// System.IO.Stream
struct Stream_t1273022909;
// System.Action
struct Action_t1264377477;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Action`1<System.Exception>
struct Action_1_t1609204844;
// System.IO.MemoryStream
struct MemoryStream_t94973147;
// System.Action`1<System.Int64>
struct Action_1_t3909034899;
// System.Action`1<System.Byte[]>
struct Action_1_t4289115252;
// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4
struct U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Collections.Generic.Queue`1<Unity.Linq.GameObjectExtensions/DescendantsEnumerable/InternalUnsafeRefStack>
struct Queue_1_t2375418695;
// Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator[]
struct EnumeratorU5BU5D_t526655246;
// System.Collections.Specialized.NameValueCollection
struct NameValueCollection_t407452768;
// System.Version
struct Version_t3456873960;
// System.Collections.Generic.List`1<System.Byte>
struct List_1_t2606371118;
// StereoController
struct StereoController_t1722192388;
// socket.io.WebSocketTrigger
struct WebSocketTrigger_t496382349;
// socket.io.SocketInitializer/<InitCore>c__Iterator0
struct U3CInitCoreU3Ec__Iterator0_t1544445406;
// System.Action`1<System.String>
struct Action_1_t2019918284;
// WebSocketSharp.PayloadData
struct PayloadData_t688932160;
// System.Void
struct Void_t1185182177;
// System.Exception
struct Exception_t;
// UniRx.ICancelable
struct ICancelable_t3440398893;
// System.Func`2<System.String[],WebSocketSharp.HttpResponse>
struct Func_2_t781510123;
// System.Func`2<System.String[],WebSocketSharp.HttpRequest>
struct Func_2_t913386989;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Func`2<UnityEngine.Transform,System.Boolean>
struct Func_2_t1722577774;
// Unity.Linq.GameObjectExtensions/DescendantsEnumerable/InternalUnsafeRefStack
struct InternalUnsafeRefStack_t2529159201;
// CardboardProfile
struct CardboardProfile_t2246179929;
// MutablePose3D
struct MutablePose3D_t3352419872;
// WebSocketSharp.Net.HttpListenerContext
struct HttpListenerContext_t3723273891;
// WebSocketSharp.Net.ChunkStream
struct ChunkStream_t2280345721;
// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>
struct IEnumerable_1_t93489508;
// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject>
struct IEnumerator_1_t1546207087;
// UnityEngine.WWW
struct WWW_t3688466362;
// UniRx.IObserver`1<socket.io.Socket>
struct IObserver_1_t2334715725;
// socket.io.SocketInitializer
struct SocketInitializer_t383163007;
// socket.io.SocketInitializer/<InitCore>c__Iterator0/<InitCore>c__AnonStorey1
struct U3CInitCoreU3Ec__AnonStorey1_t2077288943;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Action`2<WebSocketSharp.LogData,System.String>
struct Action_2_t488357320;
// System.Diagnostics.StackFrame
struct StackFrame_t3217253059;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// System.Func`2<UnityEngine.GameObject,System.Boolean>
struct Func_2_t4243939292;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.String>>
struct Dictionary_2_t908631615;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>>
struct Dictionary_2_t1805174583;
// System.Uri
struct Uri_t100236324;
// UnityEngine.Material
struct Material_t340375123;
// ICardboardGazePointer
struct ICardboardGazePointer_t3077006920;
// UnityEngine.Camera
struct Camera_t4157153871;
// ICardboardGazeResponder
struct ICardboardGazeResponder_t2918224501;
// System.Func`2<UnityEngine.MonoBehaviour,ICardboardGazePointer>
struct Func_2_t1607421081;
// System.Func`2<ICardboardGazePointer,System.Boolean>
struct Func_2_t1292700427;
// socket.io.SocketManager
struct SocketManager_t1059501173;
// CardboardEye[]
struct CardboardEyeU5BU5D_t2353027340;
// CardboardHead
struct CardboardHead_t545919794;
// System.Func`2<CardboardEye,CardboardHead>
struct Func_2_t3716999315;
// System.Collections.Generic.List`1<UniRx.Tuple`4<socket.io.Socket,System.Boolean,System.Int32,System.DateTime>>
struct List_1_t3697878576;
// System.Collections.Generic.Dictionary`2<System.String,socket.io.WebSocketTrigger>
struct Dictionary_2_t281638648;
// System.IDisposable
struct IDisposable_t3640265483;
// System.Func`2<UniRx.Tuple`4<socket.io.Socket,System.Boolean,System.Int32,System.DateTime>,System.Boolean>
struct Func_2_t454313745;
// System.Predicate`1<UniRx.Tuple`4<socket.io.Socket,System.Boolean,System.Int32,System.DateTime>>
struct Predicate_1_t3051097958;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t537414295;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t2331243652;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3903027533;
// UnityEngine.EventSystems.BaseInput
struct BaseInput_t3630163547;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef TIMESTAMP_T1761014137_H
#define TIMESTAMP_T1761014137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.TimeStamp
struct  TimeStamp_t1761014137  : public RuntimeObject
{
public:

public:
};

struct TimeStamp_t1761014137_StaticFields
{
public:
	// System.String[] socket.io.TimeStamp::_encodeAlphabets
	StringU5BU5D_t1281789340* ____encodeAlphabets_0;

public:
	inline static int32_t get_offset_of__encodeAlphabets_0() { return static_cast<int32_t>(offsetof(TimeStamp_t1761014137_StaticFields, ____encodeAlphabets_0)); }
	inline StringU5BU5D_t1281789340* get__encodeAlphabets_0() const { return ____encodeAlphabets_0; }
	inline StringU5BU5D_t1281789340** get_address_of__encodeAlphabets_0() { return &____encodeAlphabets_0; }
	inline void set__encodeAlphabets_0(StringU5BU5D_t1281789340* value)
	{
		____encodeAlphabets_0 = value;
		Il2CppCodeGenWriteBarrier((&____encodeAlphabets_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESTAMP_T1761014137_H
#ifndef U3CCONTAINSTWICEU3EC__ANONSTOREY1_T3397086364_H
#define U3CCONTAINSTWICEU3EC__ANONSTOREY1_T3397086364_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1
struct  U3CContainsTwiceU3Ec__AnonStorey1_t3397086364  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1::len
	int32_t ___len_0;
	// System.String[] WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1::values
	StringU5BU5D_t1281789340* ___values_1;
	// System.Func`2<System.Int32,System.Boolean> WebSocketSharp.Ext/<ContainsTwice>c__AnonStorey1::contains
	Func_2_t1300586518 * ___contains_2;

public:
	inline static int32_t get_offset_of_len_0() { return static_cast<int32_t>(offsetof(U3CContainsTwiceU3Ec__AnonStorey1_t3397086364, ___len_0)); }
	inline int32_t get_len_0() const { return ___len_0; }
	inline int32_t* get_address_of_len_0() { return &___len_0; }
	inline void set_len_0(int32_t value)
	{
		___len_0 = value;
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(U3CContainsTwiceU3Ec__AnonStorey1_t3397086364, ___values_1)); }
	inline StringU5BU5D_t1281789340* get_values_1() const { return ___values_1; }
	inline StringU5BU5D_t1281789340** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(StringU5BU5D_t1281789340* value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_contains_2() { return static_cast<int32_t>(offsetof(U3CContainsTwiceU3Ec__AnonStorey1_t3397086364, ___contains_2)); }
	inline Func_2_t1300586518 * get_contains_2() const { return ___contains_2; }
	inline Func_2_t1300586518 ** get_address_of_contains_2() { return &___contains_2; }
	inline void set_contains_2(Func_2_t1300586518 * value)
	{
		___contains_2 = value;
		Il2CppCodeGenWriteBarrier((&___contains_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONTAINSTWICEU3EC__ANONSTOREY1_T3397086364_H
#ifndef GAMEOBJECTEXTENSIONS_T3873029940_H
#define GAMEOBJECTEXTENSIONS_T3873029940_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions
struct  GameObjectExtensions_t3873029940  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTEXTENSIONS_T3873029940_H
#ifndef CHUNK_T286195069_H
#define CHUNK_T286195069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.Chunk
struct  Chunk_t286195069  : public RuntimeObject
{
public:
	// System.Byte[] WebSocketSharp.Net.Chunk::_data
	ByteU5BU5D_t4116647657* ____data_0;
	// System.Int32 WebSocketSharp.Net.Chunk::_offset
	int32_t ____offset_1;

public:
	inline static int32_t get_offset_of__data_0() { return static_cast<int32_t>(offsetof(Chunk_t286195069, ____data_0)); }
	inline ByteU5BU5D_t4116647657* get__data_0() const { return ____data_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__data_0() { return &____data_0; }
	inline void set__data_0(ByteU5BU5D_t4116647657* value)
	{
		____data_0 = value;
		Il2CppCodeGenWriteBarrier((&____data_0), value);
	}

	inline static int32_t get_offset_of__offset_1() { return static_cast<int32_t>(offsetof(Chunk_t286195069, ____offset_1)); }
	inline int32_t get__offset_1() const { return ____offset_1; }
	inline int32_t* get_address_of__offset_1() { return &____offset_1; }
	inline void set__offset_1(int32_t value)
	{
		____offset_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNK_T286195069_H
#ifndef U3CSTARTU3EC__ANONSTOREY0_T1296942594_H
#define U3CSTARTU3EC__ANONSTOREY0_T1296942594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sample.Namespace/<Start>c__AnonStorey0
struct  U3CStartU3Ec__AnonStorey0_t1296942594  : public RuntimeObject
{
public:
	// socket.io.Socket Sample.Namespace/<Start>c__AnonStorey0::news
	Socket_t590446146 * ___news_0;
	// socket.io.Socket Sample.Namespace/<Start>c__AnonStorey0::chat
	Socket_t590446146 * ___chat_1;

public:
	inline static int32_t get_offset_of_news_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t1296942594, ___news_0)); }
	inline Socket_t590446146 * get_news_0() const { return ___news_0; }
	inline Socket_t590446146 ** get_address_of_news_0() { return &___news_0; }
	inline void set_news_0(Socket_t590446146 * value)
	{
		___news_0 = value;
		Il2CppCodeGenWriteBarrier((&___news_0), value);
	}

	inline static int32_t get_offset_of_chat_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t1296942594, ___chat_1)); }
	inline Socket_t590446146 * get_chat_1() const { return ___chat_1; }
	inline Socket_t590446146 ** get_address_of_chat_1() { return &___chat_1; }
	inline void set_chat_1(Socket_t590446146 * value)
	{
		___chat_1 = value;
		Il2CppCodeGenWriteBarrier((&___chat_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ANONSTOREY0_T1296942594_H
#ifndef EXT_T2749166018_H
#define EXT_T2749166018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext
struct  Ext_t2749166018  : public RuntimeObject
{
public:

public:
};

struct Ext_t2749166018_StaticFields
{
public:
	// System.Byte[] WebSocketSharp.Ext::_last
	ByteU5BU5D_t4116647657* ____last_0;
	// System.Int32 WebSocketSharp.Ext::_retry
	int32_t ____retry_1;
	// System.Func`2<System.String,System.Boolean> WebSocketSharp.Ext::<>f__am$cache0
	Func_2_t2197129486 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of__last_0() { return static_cast<int32_t>(offsetof(Ext_t2749166018_StaticFields, ____last_0)); }
	inline ByteU5BU5D_t4116647657* get__last_0() const { return ____last_0; }
	inline ByteU5BU5D_t4116647657** get_address_of__last_0() { return &____last_0; }
	inline void set__last_0(ByteU5BU5D_t4116647657* value)
	{
		____last_0 = value;
		Il2CppCodeGenWriteBarrier((&____last_0), value);
	}

	inline static int32_t get_offset_of__retry_1() { return static_cast<int32_t>(offsetof(Ext_t2749166018_StaticFields, ____retry_1)); }
	inline int32_t get__retry_1() const { return ____retry_1; }
	inline int32_t* get_address_of__retry_1() { return &____retry_1; }
	inline void set__retry_1(int32_t value)
	{
		____retry_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(Ext_t2749166018_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t2197129486 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t2197129486 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t2197129486 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXT_T2749166018_H
#ifndef U3CCOPYTOASYNCU3EC__ANONSTOREY2_T3467411496_H
#define U3CCOPYTOASYNCU3EC__ANONSTOREY2_T3467411496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2
struct  U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496  : public RuntimeObject
{
public:
	// System.IO.Stream WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::source
	Stream_t1273022909 * ___source_0;
	// System.Action WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::completed
	Action_t1264377477 * ___completed_1;
	// System.IO.Stream WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::destination
	Stream_t1273022909 * ___destination_2;
	// System.Byte[] WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::buff
	ByteU5BU5D_t4116647657* ___buff_3;
	// System.Int32 WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::bufferLength
	int32_t ___bufferLength_4;
	// System.AsyncCallback WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::callback
	AsyncCallback_t3962456242 * ___callback_5;
	// System.Action`1<System.Exception> WebSocketSharp.Ext/<CopyToAsync>c__AnonStorey2::error
	Action_1_t1609204844 * ___error_6;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496, ___source_0)); }
	inline Stream_t1273022909 * get_source_0() const { return ___source_0; }
	inline Stream_t1273022909 ** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(Stream_t1273022909 * value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}

	inline static int32_t get_offset_of_completed_1() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496, ___completed_1)); }
	inline Action_t1264377477 * get_completed_1() const { return ___completed_1; }
	inline Action_t1264377477 ** get_address_of_completed_1() { return &___completed_1; }
	inline void set_completed_1(Action_t1264377477 * value)
	{
		___completed_1 = value;
		Il2CppCodeGenWriteBarrier((&___completed_1), value);
	}

	inline static int32_t get_offset_of_destination_2() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496, ___destination_2)); }
	inline Stream_t1273022909 * get_destination_2() const { return ___destination_2; }
	inline Stream_t1273022909 ** get_address_of_destination_2() { return &___destination_2; }
	inline void set_destination_2(Stream_t1273022909 * value)
	{
		___destination_2 = value;
		Il2CppCodeGenWriteBarrier((&___destination_2), value);
	}

	inline static int32_t get_offset_of_buff_3() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496, ___buff_3)); }
	inline ByteU5BU5D_t4116647657* get_buff_3() const { return ___buff_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_buff_3() { return &___buff_3; }
	inline void set_buff_3(ByteU5BU5D_t4116647657* value)
	{
		___buff_3 = value;
		Il2CppCodeGenWriteBarrier((&___buff_3), value);
	}

	inline static int32_t get_offset_of_bufferLength_4() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496, ___bufferLength_4)); }
	inline int32_t get_bufferLength_4() const { return ___bufferLength_4; }
	inline int32_t* get_address_of_bufferLength_4() { return &___bufferLength_4; }
	inline void set_bufferLength_4(int32_t value)
	{
		___bufferLength_4 = value;
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496, ___callback_5)); }
	inline AsyncCallback_t3962456242 * get_callback_5() const { return ___callback_5; }
	inline AsyncCallback_t3962456242 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(AsyncCallback_t3962456242 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_error_6() { return static_cast<int32_t>(offsetof(U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496, ___error_6)); }
	inline Action_1_t1609204844 * get_error_6() const { return ___error_6; }
	inline Action_1_t1609204844 ** get_address_of_error_6() { return &___error_6; }
	inline void set_error_6(Action_1_t1609204844 * value)
	{
		___error_6 = value;
		Il2CppCodeGenWriteBarrier((&___error_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOPYTOASYNCU3EC__ANONSTOREY2_T3467411496_H
#ifndef U3CREADBYTESASYNCU3EC__ANONSTOREY4_T1780370748_H
#define U3CREADBYTESASYNCU3EC__ANONSTOREY4_T1780370748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4
struct  U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748  : public RuntimeObject
{
public:
	// System.Int32 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::bufferLength
	int32_t ___bufferLength_0;
	// System.IO.Stream WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::stream
	Stream_t1273022909 * ___stream_1;
	// System.Byte[] WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::buff
	ByteU5BU5D_t4116647657* ___buff_2;
	// System.IO.MemoryStream WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::dest
	MemoryStream_t94973147 * ___dest_3;
	// System.Int32 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::retry
	int32_t ___retry_4;
	// System.Action`1<System.Int64> WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::read
	Action_1_t3909034899 * ___read_5;
	// System.Action`1<System.Byte[]> WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::completed
	Action_1_t4289115252 * ___completed_6;
	// System.Action`1<System.Exception> WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4::error
	Action_1_t1609204844 * ___error_7;

public:
	inline static int32_t get_offset_of_bufferLength_0() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748, ___bufferLength_0)); }
	inline int32_t get_bufferLength_0() const { return ___bufferLength_0; }
	inline int32_t* get_address_of_bufferLength_0() { return &___bufferLength_0; }
	inline void set_bufferLength_0(int32_t value)
	{
		___bufferLength_0 = value;
	}

	inline static int32_t get_offset_of_stream_1() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748, ___stream_1)); }
	inline Stream_t1273022909 * get_stream_1() const { return ___stream_1; }
	inline Stream_t1273022909 ** get_address_of_stream_1() { return &___stream_1; }
	inline void set_stream_1(Stream_t1273022909 * value)
	{
		___stream_1 = value;
		Il2CppCodeGenWriteBarrier((&___stream_1), value);
	}

	inline static int32_t get_offset_of_buff_2() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748, ___buff_2)); }
	inline ByteU5BU5D_t4116647657* get_buff_2() const { return ___buff_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_buff_2() { return &___buff_2; }
	inline void set_buff_2(ByteU5BU5D_t4116647657* value)
	{
		___buff_2 = value;
		Il2CppCodeGenWriteBarrier((&___buff_2), value);
	}

	inline static int32_t get_offset_of_dest_3() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748, ___dest_3)); }
	inline MemoryStream_t94973147 * get_dest_3() const { return ___dest_3; }
	inline MemoryStream_t94973147 ** get_address_of_dest_3() { return &___dest_3; }
	inline void set_dest_3(MemoryStream_t94973147 * value)
	{
		___dest_3 = value;
		Il2CppCodeGenWriteBarrier((&___dest_3), value);
	}

	inline static int32_t get_offset_of_retry_4() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748, ___retry_4)); }
	inline int32_t get_retry_4() const { return ___retry_4; }
	inline int32_t* get_address_of_retry_4() { return &___retry_4; }
	inline void set_retry_4(int32_t value)
	{
		___retry_4 = value;
	}

	inline static int32_t get_offset_of_read_5() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748, ___read_5)); }
	inline Action_1_t3909034899 * get_read_5() const { return ___read_5; }
	inline Action_1_t3909034899 ** get_address_of_read_5() { return &___read_5; }
	inline void set_read_5(Action_1_t3909034899 * value)
	{
		___read_5 = value;
		Il2CppCodeGenWriteBarrier((&___read_5), value);
	}

	inline static int32_t get_offset_of_completed_6() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748, ___completed_6)); }
	inline Action_1_t4289115252 * get_completed_6() const { return ___completed_6; }
	inline Action_1_t4289115252 ** get_address_of_completed_6() { return &___completed_6; }
	inline void set_completed_6(Action_1_t4289115252 * value)
	{
		___completed_6 = value;
		Il2CppCodeGenWriteBarrier((&___completed_6), value);
	}

	inline static int32_t get_offset_of_error_7() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748, ___error_7)); }
	inline Action_1_t1609204844 * get_error_7() const { return ___error_7; }
	inline Action_1_t1609204844 ** get_address_of_error_7() { return &___error_7; }
	inline void set_error_7(Action_1_t1609204844 * value)
	{
		___error_7 = value;
		Il2CppCodeGenWriteBarrier((&___error_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADBYTESASYNCU3EC__ANONSTOREY4_T1780370748_H
#ifndef U3CREADBYTESASYNCU3EC__ANONSTOREY5_T2704290800_H
#define U3CREADBYTESASYNCU3EC__ANONSTOREY5_T2704290800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5
struct  U3CReadBytesAsyncU3Ec__AnonStorey5_t2704290800  : public RuntimeObject
{
public:
	// System.Int64 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5::len
	int64_t ___len_0;
	// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey4/<ReadBytesAsync>c__AnonStorey5::<>f__ref$4
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748 * ___U3CU3Ef__refU244_1;

public:
	inline static int32_t get_offset_of_len_0() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey5_t2704290800, ___len_0)); }
	inline int64_t get_len_0() const { return ___len_0; }
	inline int64_t* get_address_of_len_0() { return &___len_0; }
	inline void set_len_0(int64_t value)
	{
		___len_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU244_1() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey5_t2704290800, ___U3CU3Ef__refU244_1)); }
	inline U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748 * get_U3CU3Ef__refU244_1() const { return ___U3CU3Ef__refU244_1; }
	inline U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748 ** get_address_of_U3CU3Ef__refU244_1() { return &___U3CU3Ef__refU244_1; }
	inline void set_U3CU3Ef__refU244_1(U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748 * value)
	{
		___U3CU3Ef__refU244_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU244_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADBYTESASYNCU3EC__ANONSTOREY5_T2704290800_H
#ifndef U3CSPLITHEADERVALUEU3EC__ITERATOR0_T2991988224_H
#define U3CSPLITHEADERVALUEU3EC__ITERATOR0_T2991988224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0
struct  U3CSplitHeaderValueU3Ec__Iterator0_t2991988224  : public RuntimeObject
{
public:
	// System.String WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::value
	String_t* ___value_0;
	// System.Int32 WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<len>__0
	int32_t ___U3ClenU3E__0_1;
	// System.Char[] WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::separators
	CharU5BU5D_t3528271667* ___separators_2;
	// System.String WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<seps>__0
	String_t* ___U3CsepsU3E__0_3;
	// System.Text.StringBuilder WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<buff>__0
	StringBuilder_t * ___U3CbuffU3E__0_4;
	// System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<escaped>__0
	bool ___U3CescapedU3E__0_5;
	// System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<quoted>__0
	bool ___U3CquotedU3E__0_6;
	// System.Int32 WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_7;
	// System.Char WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::<c>__2
	Il2CppChar ___U3CcU3E__2_8;
	// System.String WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::$current
	String_t* ___U24current_9;
	// System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::$disposing
	bool ___U24disposing_10;
	// System.Int32 WebSocketSharp.Ext/<SplitHeaderValue>c__Iterator0::$PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_U3ClenU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3ClenU3E__0_1)); }
	inline int32_t get_U3ClenU3E__0_1() const { return ___U3ClenU3E__0_1; }
	inline int32_t* get_address_of_U3ClenU3E__0_1() { return &___U3ClenU3E__0_1; }
	inline void set_U3ClenU3E__0_1(int32_t value)
	{
		___U3ClenU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_separators_2() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___separators_2)); }
	inline CharU5BU5D_t3528271667* get_separators_2() const { return ___separators_2; }
	inline CharU5BU5D_t3528271667** get_address_of_separators_2() { return &___separators_2; }
	inline void set_separators_2(CharU5BU5D_t3528271667* value)
	{
		___separators_2 = value;
		Il2CppCodeGenWriteBarrier((&___separators_2), value);
	}

	inline static int32_t get_offset_of_U3CsepsU3E__0_3() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CsepsU3E__0_3)); }
	inline String_t* get_U3CsepsU3E__0_3() const { return ___U3CsepsU3E__0_3; }
	inline String_t** get_address_of_U3CsepsU3E__0_3() { return &___U3CsepsU3E__0_3; }
	inline void set_U3CsepsU3E__0_3(String_t* value)
	{
		___U3CsepsU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsepsU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CbuffU3E__0_4() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CbuffU3E__0_4)); }
	inline StringBuilder_t * get_U3CbuffU3E__0_4() const { return ___U3CbuffU3E__0_4; }
	inline StringBuilder_t ** get_address_of_U3CbuffU3E__0_4() { return &___U3CbuffU3E__0_4; }
	inline void set_U3CbuffU3E__0_4(StringBuilder_t * value)
	{
		___U3CbuffU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbuffU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CescapedU3E__0_5() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CescapedU3E__0_5)); }
	inline bool get_U3CescapedU3E__0_5() const { return ___U3CescapedU3E__0_5; }
	inline bool* get_address_of_U3CescapedU3E__0_5() { return &___U3CescapedU3E__0_5; }
	inline void set_U3CescapedU3E__0_5(bool value)
	{
		___U3CescapedU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CquotedU3E__0_6() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CquotedU3E__0_6)); }
	inline bool get_U3CquotedU3E__0_6() const { return ___U3CquotedU3E__0_6; }
	inline bool* get_address_of_U3CquotedU3E__0_6() { return &___U3CquotedU3E__0_6; }
	inline void set_U3CquotedU3E__0_6(bool value)
	{
		___U3CquotedU3E__0_6 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__1_7() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CiU3E__1_7)); }
	inline int32_t get_U3CiU3E__1_7() const { return ___U3CiU3E__1_7; }
	inline int32_t* get_address_of_U3CiU3E__1_7() { return &___U3CiU3E__1_7; }
	inline void set_U3CiU3E__1_7(int32_t value)
	{
		___U3CiU3E__1_7 = value;
	}

	inline static int32_t get_offset_of_U3CcU3E__2_8() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U3CcU3E__2_8)); }
	inline Il2CppChar get_U3CcU3E__2_8() const { return ___U3CcU3E__2_8; }
	inline Il2CppChar* get_address_of_U3CcU3E__2_8() { return &___U3CcU3E__2_8; }
	inline void set_U3CcU3E__2_8(Il2CppChar value)
	{
		___U3CcU3E__2_8 = value;
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U24current_9)); }
	inline String_t* get_U24current_9() const { return ___U24current_9; }
	inline String_t** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(String_t* value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CSplitHeaderValueU3Ec__Iterator0_t2991988224, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPLITHEADERVALUEU3EC__ITERATOR0_T2991988224_H
#ifndef INTERNALUNSAFEREFSTACK_T2529159201_H
#define INTERNALUNSAFEREFSTACK_T2529159201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/DescendantsEnumerable/InternalUnsafeRefStack
struct  InternalUnsafeRefStack_t2529159201  : public RuntimeObject
{
public:
	// System.Int32 Unity.Linq.GameObjectExtensions/DescendantsEnumerable/InternalUnsafeRefStack::size
	int32_t ___size_1;
	// Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator[] Unity.Linq.GameObjectExtensions/DescendantsEnumerable/InternalUnsafeRefStack::array
	EnumeratorU5BU5D_t526655246* ___array_2;

public:
	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(InternalUnsafeRefStack_t2529159201, ___size_1)); }
	inline int32_t get_size_1() const { return ___size_1; }
	inline int32_t* get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(int32_t value)
	{
		___size_1 = value;
	}

	inline static int32_t get_offset_of_array_2() { return static_cast<int32_t>(offsetof(InternalUnsafeRefStack_t2529159201, ___array_2)); }
	inline EnumeratorU5BU5D_t526655246* get_array_2() const { return ___array_2; }
	inline EnumeratorU5BU5D_t526655246** get_address_of_array_2() { return &___array_2; }
	inline void set_array_2(EnumeratorU5BU5D_t526655246* value)
	{
		___array_2 = value;
		Il2CppCodeGenWriteBarrier((&___array_2), value);
	}
};

struct InternalUnsafeRefStack_t2529159201_StaticFields
{
public:
	// System.Collections.Generic.Queue`1<Unity.Linq.GameObjectExtensions/DescendantsEnumerable/InternalUnsafeRefStack> Unity.Linq.GameObjectExtensions/DescendantsEnumerable/InternalUnsafeRefStack::RefStackPool
	Queue_1_t2375418695 * ___RefStackPool_0;

public:
	inline static int32_t get_offset_of_RefStackPool_0() { return static_cast<int32_t>(offsetof(InternalUnsafeRefStack_t2529159201_StaticFields, ___RefStackPool_0)); }
	inline Queue_1_t2375418695 * get_RefStackPool_0() const { return ___RefStackPool_0; }
	inline Queue_1_t2375418695 ** get_address_of_RefStackPool_0() { return &___RefStackPool_0; }
	inline void set_RefStackPool_0(Queue_1_t2375418695 * value)
	{
		___RefStackPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___RefStackPool_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALUNSAFEREFSTACK_T2529159201_H
#ifndef U3CWRITEBYTESASYNCU3EC__ANONSTOREY6_T169123234_H
#define U3CWRITEBYTESASYNCU3EC__ANONSTOREY6_T169123234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<WriteBytesAsync>c__AnonStorey6
struct  U3CWriteBytesAsyncU3Ec__AnonStorey6_t169123234  : public RuntimeObject
{
public:
	// System.Action WebSocketSharp.Ext/<WriteBytesAsync>c__AnonStorey6::completed
	Action_t1264377477 * ___completed_0;
	// System.IO.MemoryStream WebSocketSharp.Ext/<WriteBytesAsync>c__AnonStorey6::input
	MemoryStream_t94973147 * ___input_1;
	// System.Action`1<System.Exception> WebSocketSharp.Ext/<WriteBytesAsync>c__AnonStorey6::error
	Action_1_t1609204844 * ___error_2;

public:
	inline static int32_t get_offset_of_completed_0() { return static_cast<int32_t>(offsetof(U3CWriteBytesAsyncU3Ec__AnonStorey6_t169123234, ___completed_0)); }
	inline Action_t1264377477 * get_completed_0() const { return ___completed_0; }
	inline Action_t1264377477 ** get_address_of_completed_0() { return &___completed_0; }
	inline void set_completed_0(Action_t1264377477 * value)
	{
		___completed_0 = value;
		Il2CppCodeGenWriteBarrier((&___completed_0), value);
	}

	inline static int32_t get_offset_of_input_1() { return static_cast<int32_t>(offsetof(U3CWriteBytesAsyncU3Ec__AnonStorey6_t169123234, ___input_1)); }
	inline MemoryStream_t94973147 * get_input_1() const { return ___input_1; }
	inline MemoryStream_t94973147 ** get_address_of_input_1() { return &___input_1; }
	inline void set_input_1(MemoryStream_t94973147 * value)
	{
		___input_1 = value;
		Il2CppCodeGenWriteBarrier((&___input_1), value);
	}

	inline static int32_t get_offset_of_error_2() { return static_cast<int32_t>(offsetof(U3CWriteBytesAsyncU3Ec__AnonStorey6_t169123234, ___error_2)); }
	inline Action_1_t1609204844 * get_error_2() const { return ___error_2; }
	inline Action_1_t1609204844 ** get_address_of_error_2() { return &___error_2; }
	inline void set_error_2(Action_1_t1609204844 * value)
	{
		___error_2 = value;
		Il2CppCodeGenWriteBarrier((&___error_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWRITEBYTESASYNCU3EC__ANONSTOREY6_T169123234_H
#ifndef HTTPBASE_T3529262337_H
#define HTTPBASE_T3529262337_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HttpBase
struct  HttpBase_t3529262337  : public RuntimeObject
{
public:
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.HttpBase::_headers
	NameValueCollection_t407452768 * ____headers_0;
	// System.Version WebSocketSharp.HttpBase::_version
	Version_t3456873960 * ____version_2;
	// System.Byte[] WebSocketSharp.HttpBase::EntityBodyData
	ByteU5BU5D_t4116647657* ___EntityBodyData_3;

public:
	inline static int32_t get_offset_of__headers_0() { return static_cast<int32_t>(offsetof(HttpBase_t3529262337, ____headers_0)); }
	inline NameValueCollection_t407452768 * get__headers_0() const { return ____headers_0; }
	inline NameValueCollection_t407452768 ** get_address_of__headers_0() { return &____headers_0; }
	inline void set__headers_0(NameValueCollection_t407452768 * value)
	{
		____headers_0 = value;
		Il2CppCodeGenWriteBarrier((&____headers_0), value);
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(HttpBase_t3529262337, ____version_2)); }
	inline Version_t3456873960 * get__version_2() const { return ____version_2; }
	inline Version_t3456873960 ** get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(Version_t3456873960 * value)
	{
		____version_2 = value;
		Il2CppCodeGenWriteBarrier((&____version_2), value);
	}

	inline static int32_t get_offset_of_EntityBodyData_3() { return static_cast<int32_t>(offsetof(HttpBase_t3529262337, ___EntityBodyData_3)); }
	inline ByteU5BU5D_t4116647657* get_EntityBodyData_3() const { return ___EntityBodyData_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_EntityBodyData_3() { return &___EntityBodyData_3; }
	inline void set_EntityBodyData_3(ByteU5BU5D_t4116647657* value)
	{
		___EntityBodyData_3 = value;
		Il2CppCodeGenWriteBarrier((&___EntityBodyData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPBASE_T3529262337_H
#ifndef U3CREADHEADERSU3EC__ANONSTOREY0_T2304224024_H
#define U3CREADHEADERSU3EC__ANONSTOREY0_T2304224024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HttpBase/<readHeaders>c__AnonStorey0
struct  U3CreadHeadersU3Ec__AnonStorey0_t2304224024  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Byte> WebSocketSharp.HttpBase/<readHeaders>c__AnonStorey0::buff
	List_1_t2606371118 * ___buff_0;
	// System.Int32 WebSocketSharp.HttpBase/<readHeaders>c__AnonStorey0::cnt
	int32_t ___cnt_1;

public:
	inline static int32_t get_offset_of_buff_0() { return static_cast<int32_t>(offsetof(U3CreadHeadersU3Ec__AnonStorey0_t2304224024, ___buff_0)); }
	inline List_1_t2606371118 * get_buff_0() const { return ___buff_0; }
	inline List_1_t2606371118 ** get_address_of_buff_0() { return &___buff_0; }
	inline void set_buff_0(List_1_t2606371118 * value)
	{
		___buff_0 = value;
		Il2CppCodeGenWriteBarrier((&___buff_0), value);
	}

	inline static int32_t get_offset_of_cnt_1() { return static_cast<int32_t>(offsetof(U3CreadHeadersU3Ec__AnonStorey0_t2304224024, ___cnt_1)); }
	inline int32_t get_cnt_1() const { return ___cnt_1; }
	inline int32_t* get_address_of_cnt_1() { return &___cnt_1; }
	inline void set_cnt_1(int32_t value)
	{
		___cnt_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADHEADERSU3EC__ANONSTOREY0_T2304224024_H
#ifndef U3CSTARTU3EC__ANONSTOREY0_T3538019122_H
#define U3CSTARTU3EC__ANONSTOREY0_T3538019122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sample.Events/<Start>c__AnonStorey0
struct  U3CStartU3Ec__AnonStorey0_t3538019122  : public RuntimeObject
{
public:
	// socket.io.Socket Sample.Events/<Start>c__AnonStorey0::socket
	Socket_t590446146 * ___socket_0;

public:
	inline static int32_t get_offset_of_socket_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t3538019122, ___socket_0)); }
	inline Socket_t590446146 * get_socket_0() const { return ___socket_0; }
	inline Socket_t590446146 ** get_address_of_socket_0() { return &___socket_0; }
	inline void set_socket_0(Socket_t590446146 * value)
	{
		___socket_0 = value;
		Il2CppCodeGenWriteBarrier((&___socket_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ANONSTOREY0_T3538019122_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef DECODER_T3794782951_H
#define DECODER_T3794782951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.Decoder
struct  Decoder_t3794782951  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODER_T3794782951_H
#ifndef U3CENDOFFRAMEU3EC__ITERATOR0_T2840111706_H
#define U3CENDOFFRAMEU3EC__ITERATOR0_T2840111706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StereoController/<EndOfFrame>c__Iterator0
struct  U3CEndOfFrameU3Ec__Iterator0_t2840111706  : public RuntimeObject
{
public:
	// StereoController StereoController/<EndOfFrame>c__Iterator0::$this
	StereoController_t1722192388 * ___U24this_0;
	// System.Object StereoController/<EndOfFrame>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean StereoController/<EndOfFrame>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 StereoController/<EndOfFrame>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t2840111706, ___U24this_0)); }
	inline StereoController_t1722192388 * get_U24this_0() const { return ___U24this_0; }
	inline StereoController_t1722192388 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(StereoController_t1722192388 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t2840111706, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t2840111706, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CEndOfFrameU3Ec__Iterator0_t2840111706, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CENDOFFRAMEU3EC__ITERATOR0_T2840111706_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef ENCODER_T3403433762_H
#define ENCODER_T3403433762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.Encoder
struct  Encoder_t3403433762  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODER_T3403433762_H
#ifndef STREAM_T1273022909_H
#define STREAM_T1273022909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_t1273022909  : public RuntimeObject
{
public:

public:
};

struct Stream_t1273022909_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_t1273022909 * ___Null_0;

public:
	inline static int32_t get_offset_of_Null_0() { return static_cast<int32_t>(offsetof(Stream_t1273022909_StaticFields, ___Null_0)); }
	inline Stream_t1273022909 * get_Null_0() const { return ___Null_0; }
	inline Stream_t1273022909 ** get_address_of_Null_0() { return &___Null_0; }
	inline void set_Null_0(Stream_t1273022909 * value)
	{
		___Null_0 = value;
		Il2CppCodeGenWriteBarrier((&___Null_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_T1273022909_H
#ifndef U3CINITCOREU3EC__ANONSTOREY1_T2077288943_H
#define U3CINITCOREU3EC__ANONSTOREY1_T2077288943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.SocketInitializer/<InitCore>c__Iterator0/<InitCore>c__AnonStorey1
struct  U3CInitCoreU3Ec__AnonStorey1_t2077288943  : public RuntimeObject
{
public:
	// socket.io.WebSocketTrigger socket.io.SocketInitializer/<InitCore>c__Iterator0/<InitCore>c__AnonStorey1::webSocketTrigger
	WebSocketTrigger_t496382349 * ___webSocketTrigger_0;
	// socket.io.Socket socket.io.SocketInitializer/<InitCore>c__Iterator0/<InitCore>c__AnonStorey1::capturedSocket
	Socket_t590446146 * ___capturedSocket_1;
	// socket.io.SocketInitializer/<InitCore>c__Iterator0 socket.io.SocketInitializer/<InitCore>c__Iterator0/<InitCore>c__AnonStorey1::<>f__ref$0
	U3CInitCoreU3Ec__Iterator0_t1544445406 * ___U3CU3Ef__refU240_2;

public:
	inline static int32_t get_offset_of_webSocketTrigger_0() { return static_cast<int32_t>(offsetof(U3CInitCoreU3Ec__AnonStorey1_t2077288943, ___webSocketTrigger_0)); }
	inline WebSocketTrigger_t496382349 * get_webSocketTrigger_0() const { return ___webSocketTrigger_0; }
	inline WebSocketTrigger_t496382349 ** get_address_of_webSocketTrigger_0() { return &___webSocketTrigger_0; }
	inline void set_webSocketTrigger_0(WebSocketTrigger_t496382349 * value)
	{
		___webSocketTrigger_0 = value;
		Il2CppCodeGenWriteBarrier((&___webSocketTrigger_0), value);
	}

	inline static int32_t get_offset_of_capturedSocket_1() { return static_cast<int32_t>(offsetof(U3CInitCoreU3Ec__AnonStorey1_t2077288943, ___capturedSocket_1)); }
	inline Socket_t590446146 * get_capturedSocket_1() const { return ___capturedSocket_1; }
	inline Socket_t590446146 ** get_address_of_capturedSocket_1() { return &___capturedSocket_1; }
	inline void set_capturedSocket_1(Socket_t590446146 * value)
	{
		___capturedSocket_1 = value;
		Il2CppCodeGenWriteBarrier((&___capturedSocket_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_2() { return static_cast<int32_t>(offsetof(U3CInitCoreU3Ec__AnonStorey1_t2077288943, ___U3CU3Ef__refU240_2)); }
	inline U3CInitCoreU3Ec__Iterator0_t1544445406 * get_U3CU3Ef__refU240_2() const { return ___U3CU3Ef__refU240_2; }
	inline U3CInitCoreU3Ec__Iterator0_t1544445406 ** get_address_of_U3CU3Ef__refU240_2() { return &___U3CU3Ef__refU240_2; }
	inline void set_U3CU3Ef__refU240_2(U3CInitCoreU3Ec__Iterator0_t1544445406 * value)
	{
		___U3CU3Ef__refU240_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITCOREU3EC__ANONSTOREY1_T2077288943_H
#ifndef U3CRECONNECTU3EC__ANONSTOREY0_T2057279870_H
#define U3CRECONNECTU3EC__ANONSTOREY0_T2057279870_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.SocketManager/<Reconnect>c__AnonStorey0
struct  U3CReconnectU3Ec__AnonStorey0_t2057279870  : public RuntimeObject
{
public:
	// socket.io.Socket socket.io.SocketManager/<Reconnect>c__AnonStorey0::socket
	Socket_t590446146 * ___socket_0;

public:
	inline static int32_t get_offset_of_socket_0() { return static_cast<int32_t>(offsetof(U3CReconnectU3Ec__AnonStorey0_t2057279870, ___socket_0)); }
	inline Socket_t590446146 * get_socket_0() const { return ___socket_0; }
	inline Socket_t590446146 ** get_address_of_socket_0() { return &___socket_0; }
	inline void set_socket_0(Socket_t590446146 * value)
	{
		___socket_0 = value;
		Il2CppCodeGenWriteBarrier((&___socket_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRECONNECTU3EC__ANONSTOREY0_T2057279870_H
#ifndef SYSTEMEVENTHELPER_T107868270_H
#define SYSTEMEVENTHELPER_T107868270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.SystemEventHelper
struct  SystemEventHelper_t107868270  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEVENTHELPER_T107868270_H
#ifndef U3CREADBYTESASYNCU3EC__ANONSTOREY3_T1780370741_H
#define U3CREADBYTESASYNCU3EC__ANONSTOREY3_T1780370741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3
struct  U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741  : public RuntimeObject
{
public:
	// System.IO.Stream WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::stream
	Stream_t1273022909 * ___stream_0;
	// System.Int32 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::retry
	int32_t ___retry_1;
	// System.Byte[] WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::buff
	ByteU5BU5D_t4116647657* ___buff_2;
	// System.Int32 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::offset
	int32_t ___offset_3;
	// System.Int32 WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::length
	int32_t ___length_4;
	// System.AsyncCallback WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::callback
	AsyncCallback_t3962456242 * ___callback_5;
	// System.Action`1<System.Byte[]> WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::completed
	Action_1_t4289115252 * ___completed_6;
	// System.Action`1<System.Exception> WebSocketSharp.Ext/<ReadBytesAsync>c__AnonStorey3::error
	Action_1_t1609204844 * ___error_7;

public:
	inline static int32_t get_offset_of_stream_0() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741, ___stream_0)); }
	inline Stream_t1273022909 * get_stream_0() const { return ___stream_0; }
	inline Stream_t1273022909 ** get_address_of_stream_0() { return &___stream_0; }
	inline void set_stream_0(Stream_t1273022909 * value)
	{
		___stream_0 = value;
		Il2CppCodeGenWriteBarrier((&___stream_0), value);
	}

	inline static int32_t get_offset_of_retry_1() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741, ___retry_1)); }
	inline int32_t get_retry_1() const { return ___retry_1; }
	inline int32_t* get_address_of_retry_1() { return &___retry_1; }
	inline void set_retry_1(int32_t value)
	{
		___retry_1 = value;
	}

	inline static int32_t get_offset_of_buff_2() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741, ___buff_2)); }
	inline ByteU5BU5D_t4116647657* get_buff_2() const { return ___buff_2; }
	inline ByteU5BU5D_t4116647657** get_address_of_buff_2() { return &___buff_2; }
	inline void set_buff_2(ByteU5BU5D_t4116647657* value)
	{
		___buff_2 = value;
		Il2CppCodeGenWriteBarrier((&___buff_2), value);
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741, ___offset_3)); }
	inline int32_t get_offset_3() const { return ___offset_3; }
	inline int32_t* get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(int32_t value)
	{
		___offset_3 = value;
	}

	inline static int32_t get_offset_of_length_4() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741, ___length_4)); }
	inline int32_t get_length_4() const { return ___length_4; }
	inline int32_t* get_address_of_length_4() { return &___length_4; }
	inline void set_length_4(int32_t value)
	{
		___length_4 = value;
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741, ___callback_5)); }
	inline AsyncCallback_t3962456242 * get_callback_5() const { return ___callback_5; }
	inline AsyncCallback_t3962456242 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(AsyncCallback_t3962456242 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_completed_6() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741, ___completed_6)); }
	inline Action_1_t4289115252 * get_completed_6() const { return ___completed_6; }
	inline Action_1_t4289115252 ** get_address_of_completed_6() { return &___completed_6; }
	inline void set_completed_6(Action_1_t4289115252 * value)
	{
		___completed_6 = value;
		Il2CppCodeGenWriteBarrier((&___completed_6), value);
	}

	inline static int32_t get_offset_of_error_7() { return static_cast<int32_t>(offsetof(U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741, ___error_7)); }
	inline Action_1_t1609204844 * get_error_7() const { return ___error_7; }
	inline Action_1_t1609204844 ** get_address_of_error_7() { return &___error_7; }
	inline void set_error_7(Action_1_t1609204844 * value)
	{
		___error_7 = value;
		Il2CppCodeGenWriteBarrier((&___error_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADBYTESASYNCU3EC__ANONSTOREY3_T1780370741_H
#ifndef U3CSTARTU3EC__ANONSTOREY0_T3247028224_H
#define U3CSTARTU3EC__ANONSTOREY0_T3247028224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sample.Acks/<Start>c__AnonStorey0
struct  U3CStartU3Ec__AnonStorey0_t3247028224  : public RuntimeObject
{
public:
	// socket.io.Socket Sample.Acks/<Start>c__AnonStorey0::socket
	Socket_t590446146 * ___socket_0;

public:
	inline static int32_t get_offset_of_socket_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t3247028224, ___socket_0)); }
	inline Socket_t590446146 * get_socket_0() const { return ___socket_0; }
	inline Socket_t590446146 ** get_address_of_socket_0() { return &___socket_0; }
	inline void set_socket_0(Socket_t590446146 * value)
	{
		___socket_0 = value;
		Il2CppCodeGenWriteBarrier((&___socket_0), value);
	}
};

struct U3CStartU3Ec__AnonStorey0_t3247028224_StaticFields
{
public:
	// System.Action`1<System.String> Sample.Acks/<Start>c__AnonStorey0::<>f__am$cache0
	Action_1_t2019918284 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t3247028224_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Action_1_t2019918284 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Action_1_t2019918284 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ANONSTOREY0_T3247028224_H
#ifndef POLLINGURLANSWER_T1140886577_H
#define POLLINGURLANSWER_T1140886577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.SocketInitializer/PollingUrlAnswer
struct  PollingUrlAnswer_t1140886577 
{
public:
	// System.String socket.io.SocketInitializer/PollingUrlAnswer::sid
	String_t* ___sid_0;
	// System.Int32 socket.io.SocketInitializer/PollingUrlAnswer::pingInterval
	int32_t ___pingInterval_1;
	// System.Int32 socket.io.SocketInitializer/PollingUrlAnswer::pingTimeout
	int32_t ___pingTimeout_2;

public:
	inline static int32_t get_offset_of_sid_0() { return static_cast<int32_t>(offsetof(PollingUrlAnswer_t1140886577, ___sid_0)); }
	inline String_t* get_sid_0() const { return ___sid_0; }
	inline String_t** get_address_of_sid_0() { return &___sid_0; }
	inline void set_sid_0(String_t* value)
	{
		___sid_0 = value;
		Il2CppCodeGenWriteBarrier((&___sid_0), value);
	}

	inline static int32_t get_offset_of_pingInterval_1() { return static_cast<int32_t>(offsetof(PollingUrlAnswer_t1140886577, ___pingInterval_1)); }
	inline int32_t get_pingInterval_1() const { return ___pingInterval_1; }
	inline int32_t* get_address_of_pingInterval_1() { return &___pingInterval_1; }
	inline void set_pingInterval_1(int32_t value)
	{
		___pingInterval_1 = value;
	}

	inline static int32_t get_offset_of_pingTimeout_2() { return static_cast<int32_t>(offsetof(PollingUrlAnswer_t1140886577, ___pingTimeout_2)); }
	inline int32_t get_pingTimeout_2() const { return ___pingTimeout_2; }
	inline int32_t* get_address_of_pingTimeout_2() { return &___pingTimeout_2; }
	inline void set_pingTimeout_2(int32_t value)
	{
		___pingTimeout_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of socket.io.SocketInitializer/PollingUrlAnswer
struct PollingUrlAnswer_t1140886577_marshaled_pinvoke
{
	char* ___sid_0;
	int32_t ___pingInterval_1;
	int32_t ___pingTimeout_2;
};
// Native definition for COM marshalling of socket.io.SocketInitializer/PollingUrlAnswer
struct PollingUrlAnswer_t1140886577_marshaled_com
{
	Il2CppChar* ___sid_0;
	int32_t ___pingInterval_1;
	int32_t ___pingTimeout_2;
};
#endif // POLLINGURLANSWER_T1140886577_H
#ifndef CLOSEEVENTARGS_T1876714021_H
#define CLOSEEVENTARGS_T1876714021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CloseEventArgs
struct  CloseEventArgs_t1876714021  : public EventArgs_t3591816995
{
public:
	// System.Boolean WebSocketSharp.CloseEventArgs::_clean
	bool ____clean_1;
	// System.UInt16 WebSocketSharp.CloseEventArgs::_code
	uint16_t ____code_2;
	// WebSocketSharp.PayloadData WebSocketSharp.CloseEventArgs::_payloadData
	PayloadData_t688932160 * ____payloadData_3;
	// System.String WebSocketSharp.CloseEventArgs::_reason
	String_t* ____reason_4;

public:
	inline static int32_t get_offset_of__clean_1() { return static_cast<int32_t>(offsetof(CloseEventArgs_t1876714021, ____clean_1)); }
	inline bool get__clean_1() const { return ____clean_1; }
	inline bool* get_address_of__clean_1() { return &____clean_1; }
	inline void set__clean_1(bool value)
	{
		____clean_1 = value;
	}

	inline static int32_t get_offset_of__code_2() { return static_cast<int32_t>(offsetof(CloseEventArgs_t1876714021, ____code_2)); }
	inline uint16_t get__code_2() const { return ____code_2; }
	inline uint16_t* get_address_of__code_2() { return &____code_2; }
	inline void set__code_2(uint16_t value)
	{
		____code_2 = value;
	}

	inline static int32_t get_offset_of__payloadData_3() { return static_cast<int32_t>(offsetof(CloseEventArgs_t1876714021, ____payloadData_3)); }
	inline PayloadData_t688932160 * get__payloadData_3() const { return ____payloadData_3; }
	inline PayloadData_t688932160 ** get_address_of__payloadData_3() { return &____payloadData_3; }
	inline void set__payloadData_3(PayloadData_t688932160 * value)
	{
		____payloadData_3 = value;
		Il2CppCodeGenWriteBarrier((&____payloadData_3), value);
	}

	inline static int32_t get_offset_of__reason_4() { return static_cast<int32_t>(offsetof(CloseEventArgs_t1876714021, ____reason_4)); }
	inline String_t* get__reason_4() const { return ____reason_4; }
	inline String_t** get_address_of__reason_4() { return &____reason_4; }
	inline void set__reason_4(String_t* value)
	{
		____reason_4 = value;
		Il2CppCodeGenWriteBarrier((&____reason_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSEEVENTARGS_T1876714021_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ERROREVENTARGS_T3420679011_H
#define ERROREVENTARGS_T3420679011_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.ErrorEventArgs
struct  ErrorEventArgs_t3420679011  : public EventArgs_t3591816995
{
public:
	// System.Exception WebSocketSharp.ErrorEventArgs::_exception
	Exception_t * ____exception_1;
	// System.String WebSocketSharp.ErrorEventArgs::_message
	String_t* ____message_2;

public:
	inline static int32_t get_offset_of__exception_1() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t3420679011, ____exception_1)); }
	inline Exception_t * get__exception_1() const { return ____exception_1; }
	inline Exception_t ** get_address_of__exception_1() { return &____exception_1; }
	inline void set__exception_1(Exception_t * value)
	{
		____exception_1 = value;
		Il2CppCodeGenWriteBarrier((&____exception_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t3420679011, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTARGS_T3420679011_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef REQUESTSTREAM_T1020063535_H
#define REQUESTSTREAM_T1020063535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.RequestStream
struct  RequestStream_t1020063535  : public Stream_t1273022909
{
public:
	// System.Int64 WebSocketSharp.Net.RequestStream::_bodyLeft
	int64_t ____bodyLeft_1;
	// System.Byte[] WebSocketSharp.Net.RequestStream::_buffer
	ByteU5BU5D_t4116647657* ____buffer_2;
	// System.Int32 WebSocketSharp.Net.RequestStream::_count
	int32_t ____count_3;
	// System.Boolean WebSocketSharp.Net.RequestStream::_disposed
	bool ____disposed_4;
	// System.Int32 WebSocketSharp.Net.RequestStream::_offset
	int32_t ____offset_5;
	// System.IO.Stream WebSocketSharp.Net.RequestStream::_stream
	Stream_t1273022909 * ____stream_6;

public:
	inline static int32_t get_offset_of__bodyLeft_1() { return static_cast<int32_t>(offsetof(RequestStream_t1020063535, ____bodyLeft_1)); }
	inline int64_t get__bodyLeft_1() const { return ____bodyLeft_1; }
	inline int64_t* get_address_of__bodyLeft_1() { return &____bodyLeft_1; }
	inline void set__bodyLeft_1(int64_t value)
	{
		____bodyLeft_1 = value;
	}

	inline static int32_t get_offset_of__buffer_2() { return static_cast<int32_t>(offsetof(RequestStream_t1020063535, ____buffer_2)); }
	inline ByteU5BU5D_t4116647657* get__buffer_2() const { return ____buffer_2; }
	inline ByteU5BU5D_t4116647657** get_address_of__buffer_2() { return &____buffer_2; }
	inline void set__buffer_2(ByteU5BU5D_t4116647657* value)
	{
		____buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_2), value);
	}

	inline static int32_t get_offset_of__count_3() { return static_cast<int32_t>(offsetof(RequestStream_t1020063535, ____count_3)); }
	inline int32_t get__count_3() const { return ____count_3; }
	inline int32_t* get_address_of__count_3() { return &____count_3; }
	inline void set__count_3(int32_t value)
	{
		____count_3 = value;
	}

	inline static int32_t get_offset_of__disposed_4() { return static_cast<int32_t>(offsetof(RequestStream_t1020063535, ____disposed_4)); }
	inline bool get__disposed_4() const { return ____disposed_4; }
	inline bool* get_address_of__disposed_4() { return &____disposed_4; }
	inline void set__disposed_4(bool value)
	{
		____disposed_4 = value;
	}

	inline static int32_t get_offset_of__offset_5() { return static_cast<int32_t>(offsetof(RequestStream_t1020063535, ____offset_5)); }
	inline int32_t get__offset_5() const { return ____offset_5; }
	inline int32_t* get_address_of__offset_5() { return &____offset_5; }
	inline void set__offset_5(int32_t value)
	{
		____offset_5 = value;
	}

	inline static int32_t get_offset_of__stream_6() { return static_cast<int32_t>(offsetof(RequestStream_t1020063535, ____stream_6)); }
	inline Stream_t1273022909 * get__stream_6() const { return ____stream_6; }
	inline Stream_t1273022909 ** get_address_of__stream_6() { return &____stream_6; }
	inline void set__stream_6(Stream_t1273022909 * value)
	{
		____stream_6 = value;
		Il2CppCodeGenWriteBarrier((&____stream_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTSTREAM_T1020063535_H
#ifndef CANCELLATIONTOKEN_T1265546479_H
#define CANCELLATIONTOKEN_T1265546479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CancellationToken
struct  CancellationToken_t1265546479 
{
public:
	// UniRx.ICancelable UniRx.CancellationToken::source
	RuntimeObject* ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}
};

struct CancellationToken_t1265546479_StaticFields
{
public:
	// UniRx.CancellationToken UniRx.CancellationToken::Empty
	CancellationToken_t1265546479  ___Empty_1;
	// UniRx.CancellationToken UniRx.CancellationToken::None
	CancellationToken_t1265546479  ___None_2;

public:
	inline static int32_t get_offset_of_Empty_1() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479_StaticFields, ___Empty_1)); }
	inline CancellationToken_t1265546479  get_Empty_1() const { return ___Empty_1; }
	inline CancellationToken_t1265546479 * get_address_of_Empty_1() { return &___Empty_1; }
	inline void set_Empty_1(CancellationToken_t1265546479  value)
	{
		___Empty_1 = value;
	}

	inline static int32_t get_offset_of_None_2() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479_StaticFields, ___None_2)); }
	inline CancellationToken_t1265546479  get_None_2() const { return ___None_2; }
	inline CancellationToken_t1265546479 * get_address_of_None_2() { return &___None_2; }
	inline void set_None_2(CancellationToken_t1265546479  value)
	{
		___None_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UniRx.CancellationToken
struct CancellationToken_t1265546479_marshaled_pinvoke
{
	RuntimeObject* ___source_0;
};
// Native definition for COM marshalling of UniRx.CancellationToken
struct CancellationToken_t1265546479_marshaled_com
{
	RuntimeObject* ___source_0;
};
#endif // CANCELLATIONTOKEN_T1265546479_H
#ifndef LAYERMASK_T3493934918_H
#define LAYERMASK_T3493934918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3493934918 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3493934918, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3493934918_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef HTTPREQUEST_T1778308387_H
#define HTTPREQUEST_T1778308387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HttpRequest
struct  HttpRequest_t1778308387  : public HttpBase_t3529262337
{
public:
	// System.String WebSocketSharp.HttpRequest::_method
	String_t* ____method_5;
	// System.String WebSocketSharp.HttpRequest::_uri
	String_t* ____uri_6;
	// System.Boolean WebSocketSharp.HttpRequest::_websocketRequest
	bool ____websocketRequest_7;
	// System.Boolean WebSocketSharp.HttpRequest::_websocketRequestSet
	bool ____websocketRequestSet_8;

public:
	inline static int32_t get_offset_of__method_5() { return static_cast<int32_t>(offsetof(HttpRequest_t1778308387, ____method_5)); }
	inline String_t* get__method_5() const { return ____method_5; }
	inline String_t** get_address_of__method_5() { return &____method_5; }
	inline void set__method_5(String_t* value)
	{
		____method_5 = value;
		Il2CppCodeGenWriteBarrier((&____method_5), value);
	}

	inline static int32_t get_offset_of__uri_6() { return static_cast<int32_t>(offsetof(HttpRequest_t1778308387, ____uri_6)); }
	inline String_t* get__uri_6() const { return ____uri_6; }
	inline String_t** get_address_of__uri_6() { return &____uri_6; }
	inline void set__uri_6(String_t* value)
	{
		____uri_6 = value;
		Il2CppCodeGenWriteBarrier((&____uri_6), value);
	}

	inline static int32_t get_offset_of__websocketRequest_7() { return static_cast<int32_t>(offsetof(HttpRequest_t1778308387, ____websocketRequest_7)); }
	inline bool get__websocketRequest_7() const { return ____websocketRequest_7; }
	inline bool* get_address_of__websocketRequest_7() { return &____websocketRequest_7; }
	inline void set__websocketRequest_7(bool value)
	{
		____websocketRequest_7 = value;
	}

	inline static int32_t get_offset_of__websocketRequestSet_8() { return static_cast<int32_t>(offsetof(HttpRequest_t1778308387, ____websocketRequestSet_8)); }
	inline bool get__websocketRequestSet_8() const { return ____websocketRequestSet_8; }
	inline bool* get_address_of__websocketRequestSet_8() { return &____websocketRequestSet_8; }
	inline void set__websocketRequestSet_8(bool value)
	{
		____websocketRequestSet_8 = value;
	}
};

struct HttpRequest_t1778308387_StaticFields
{
public:
	// System.Func`2<System.String[],WebSocketSharp.HttpResponse> WebSocketSharp.HttpRequest::<>f__mg$cache0
	Func_2_t781510123 * ___U3CU3Ef__mgU24cache0_9;
	// System.Func`2<System.String[],WebSocketSharp.HttpRequest> WebSocketSharp.HttpRequest::<>f__mg$cache1
	Func_2_t913386989 * ___U3CU3Ef__mgU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_9() { return static_cast<int32_t>(offsetof(HttpRequest_t1778308387_StaticFields, ___U3CU3Ef__mgU24cache0_9)); }
	inline Func_2_t781510123 * get_U3CU3Ef__mgU24cache0_9() const { return ___U3CU3Ef__mgU24cache0_9; }
	inline Func_2_t781510123 ** get_address_of_U3CU3Ef__mgU24cache0_9() { return &___U3CU3Ef__mgU24cache0_9; }
	inline void set_U3CU3Ef__mgU24cache0_9(Func_2_t781510123 * value)
	{
		___U3CU3Ef__mgU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_10() { return static_cast<int32_t>(offsetof(HttpRequest_t1778308387_StaticFields, ___U3CU3Ef__mgU24cache1_10)); }
	inline Func_2_t913386989 * get_U3CU3Ef__mgU24cache1_10() const { return ___U3CU3Ef__mgU24cache1_10; }
	inline Func_2_t913386989 ** get_address_of_U3CU3Ef__mgU24cache1_10() { return &___U3CU3Ef__mgU24cache1_10; }
	inline void set_U3CU3Ef__mgU24cache1_10(Func_2_t913386989 * value)
	{
		___U3CU3Ef__mgU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUEST_T1778308387_H
#ifndef HTTPRESPONSE_T1646431521_H
#define HTTPRESPONSE_T1646431521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.HttpResponse
struct  HttpResponse_t1646431521  : public HttpBase_t3529262337
{
public:
	// System.String WebSocketSharp.HttpResponse::_code
	String_t* ____code_5;
	// System.String WebSocketSharp.HttpResponse::_reason
	String_t* ____reason_6;

public:
	inline static int32_t get_offset_of__code_5() { return static_cast<int32_t>(offsetof(HttpResponse_t1646431521, ____code_5)); }
	inline String_t* get__code_5() const { return ____code_5; }
	inline String_t** get_address_of__code_5() { return &____code_5; }
	inline void set__code_5(String_t* value)
	{
		____code_5 = value;
		Il2CppCodeGenWriteBarrier((&____code_5), value);
	}

	inline static int32_t get_offset_of__reason_6() { return static_cast<int32_t>(offsetof(HttpResponse_t1646431521, ____reason_6)); }
	inline String_t* get__reason_6() const { return ____reason_6; }
	inline String_t** get_address_of__reason_6() { return &____reason_6; }
	inline void set__reason_6(String_t* value)
	{
		____reason_6 = value;
		Il2CppCodeGenWriteBarrier((&____reason_6), value);
	}
};

struct HttpResponse_t1646431521_StaticFields
{
public:
	// System.Func`2<System.String[],WebSocketSharp.HttpResponse> WebSocketSharp.HttpResponse::<>f__mg$cache0
	Func_2_t781510123 * ___U3CU3Ef__mgU24cache0_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_7() { return static_cast<int32_t>(offsetof(HttpResponse_t1646431521_StaticFields, ___U3CU3Ef__mgU24cache0_7)); }
	inline Func_2_t781510123 * get_U3CU3Ef__mgU24cache0_7() const { return ___U3CU3Ef__mgU24cache0_7; }
	inline Func_2_t781510123 ** get_address_of_U3CU3Ef__mgU24cache0_7() { return &___U3CU3Ef__mgU24cache0_7; }
	inline void set_U3CU3Ef__mgU24cache0_7(Func_2_t781510123 * value)
	{
		___U3CU3Ef__mgU24cache0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPRESPONSE_T1646431521_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef CHILDRENENUMERABLE_T145725860_H
#define CHILDRENENUMERABLE_T145725860_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/ChildrenEnumerable
struct  ChildrenEnumerable_t145725860 
{
public:
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/ChildrenEnumerable::origin
	GameObject_t1113636619 * ___origin_0;
	// System.Boolean Unity.Linq.GameObjectExtensions/ChildrenEnumerable::withSelf
	bool ___withSelf_1;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(ChildrenEnumerable_t145725860, ___origin_0)); }
	inline GameObject_t1113636619 * get_origin_0() const { return ___origin_0; }
	inline GameObject_t1113636619 ** get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(GameObject_t1113636619 * value)
	{
		___origin_0 = value;
		Il2CppCodeGenWriteBarrier((&___origin_0), value);
	}

	inline static int32_t get_offset_of_withSelf_1() { return static_cast<int32_t>(offsetof(ChildrenEnumerable_t145725860, ___withSelf_1)); }
	inline bool get_withSelf_1() const { return ___withSelf_1; }
	inline bool* get_address_of_withSelf_1() { return &___withSelf_1; }
	inline void set_withSelf_1(bool value)
	{
		___withSelf_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/ChildrenEnumerable
struct ChildrenEnumerable_t145725860_marshaled_pinvoke
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/ChildrenEnumerable
struct ChildrenEnumerable_t145725860_marshaled_com
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
#endif // CHILDRENENUMERABLE_T145725860_H
#ifndef ENUMERATOR_T792918911_H
#define ENUMERATOR_T792918911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/ChildrenEnumerable/Enumerator
struct  Enumerator_t792918911 
{
public:
	// System.Int32 Unity.Linq.GameObjectExtensions/ChildrenEnumerable/Enumerator::childCount
	int32_t ___childCount_0;
	// UnityEngine.Transform Unity.Linq.GameObjectExtensions/ChildrenEnumerable/Enumerator::originTransform
	Transform_t3600365921 * ___originTransform_1;
	// System.Boolean Unity.Linq.GameObjectExtensions/ChildrenEnumerable/Enumerator::canRun
	bool ___canRun_2;
	// System.Boolean Unity.Linq.GameObjectExtensions/ChildrenEnumerable/Enumerator::withSelf
	bool ___withSelf_3;
	// System.Int32 Unity.Linq.GameObjectExtensions/ChildrenEnumerable/Enumerator::currentIndex
	int32_t ___currentIndex_4;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/ChildrenEnumerable/Enumerator::current
	GameObject_t1113636619 * ___current_5;

public:
	inline static int32_t get_offset_of_childCount_0() { return static_cast<int32_t>(offsetof(Enumerator_t792918911, ___childCount_0)); }
	inline int32_t get_childCount_0() const { return ___childCount_0; }
	inline int32_t* get_address_of_childCount_0() { return &___childCount_0; }
	inline void set_childCount_0(int32_t value)
	{
		___childCount_0 = value;
	}

	inline static int32_t get_offset_of_originTransform_1() { return static_cast<int32_t>(offsetof(Enumerator_t792918911, ___originTransform_1)); }
	inline Transform_t3600365921 * get_originTransform_1() const { return ___originTransform_1; }
	inline Transform_t3600365921 ** get_address_of_originTransform_1() { return &___originTransform_1; }
	inline void set_originTransform_1(Transform_t3600365921 * value)
	{
		___originTransform_1 = value;
		Il2CppCodeGenWriteBarrier((&___originTransform_1), value);
	}

	inline static int32_t get_offset_of_canRun_2() { return static_cast<int32_t>(offsetof(Enumerator_t792918911, ___canRun_2)); }
	inline bool get_canRun_2() const { return ___canRun_2; }
	inline bool* get_address_of_canRun_2() { return &___canRun_2; }
	inline void set_canRun_2(bool value)
	{
		___canRun_2 = value;
	}

	inline static int32_t get_offset_of_withSelf_3() { return static_cast<int32_t>(offsetof(Enumerator_t792918911, ___withSelf_3)); }
	inline bool get_withSelf_3() const { return ___withSelf_3; }
	inline bool* get_address_of_withSelf_3() { return &___withSelf_3; }
	inline void set_withSelf_3(bool value)
	{
		___withSelf_3 = value;
	}

	inline static int32_t get_offset_of_currentIndex_4() { return static_cast<int32_t>(offsetof(Enumerator_t792918911, ___currentIndex_4)); }
	inline int32_t get_currentIndex_4() const { return ___currentIndex_4; }
	inline int32_t* get_address_of_currentIndex_4() { return &___currentIndex_4; }
	inline void set_currentIndex_4(int32_t value)
	{
		___currentIndex_4 = value;
	}

	inline static int32_t get_offset_of_current_5() { return static_cast<int32_t>(offsetof(Enumerator_t792918911, ___current_5)); }
	inline GameObject_t1113636619 * get_current_5() const { return ___current_5; }
	inline GameObject_t1113636619 ** get_address_of_current_5() { return &___current_5; }
	inline void set_current_5(GameObject_t1113636619 * value)
	{
		___current_5 = value;
		Il2CppCodeGenWriteBarrier((&___current_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/ChildrenEnumerable/Enumerator
struct Enumerator_t792918911_marshaled_pinvoke
{
	int32_t ___childCount_0;
	Transform_t3600365921 * ___originTransform_1;
	int32_t ___canRun_2;
	int32_t ___withSelf_3;
	int32_t ___currentIndex_4;
	GameObject_t1113636619 * ___current_5;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/ChildrenEnumerable/Enumerator
struct Enumerator_t792918911_marshaled_com
{
	int32_t ___childCount_0;
	Transform_t3600365921 * ___originTransform_1;
	int32_t ___canRun_2;
	int32_t ___withSelf_3;
	int32_t ___currentIndex_4;
	GameObject_t1113636619 * ___current_5;
};
#endif // ENUMERATOR_T792918911_H
#ifndef ANCESTORSENUMERABLE_T953073017_H
#define ANCESTORSENUMERABLE_T953073017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/AncestorsEnumerable
struct  AncestorsEnumerable_t953073017 
{
public:
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/AncestorsEnumerable::origin
	GameObject_t1113636619 * ___origin_0;
	// System.Boolean Unity.Linq.GameObjectExtensions/AncestorsEnumerable::withSelf
	bool ___withSelf_1;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(AncestorsEnumerable_t953073017, ___origin_0)); }
	inline GameObject_t1113636619 * get_origin_0() const { return ___origin_0; }
	inline GameObject_t1113636619 ** get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(GameObject_t1113636619 * value)
	{
		___origin_0 = value;
		Il2CppCodeGenWriteBarrier((&___origin_0), value);
	}

	inline static int32_t get_offset_of_withSelf_1() { return static_cast<int32_t>(offsetof(AncestorsEnumerable_t953073017, ___withSelf_1)); }
	inline bool get_withSelf_1() const { return ___withSelf_1; }
	inline bool* get_address_of_withSelf_1() { return &___withSelf_1; }
	inline void set_withSelf_1(bool value)
	{
		___withSelf_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/AncestorsEnumerable
struct AncestorsEnumerable_t953073017_marshaled_pinvoke
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/AncestorsEnumerable
struct AncestorsEnumerable_t953073017_marshaled_com
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
#endif // ANCESTORSENUMERABLE_T953073017_H
#ifndef ENUMERATOR_T3403847843_H
#define ENUMERATOR_T3403847843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/AncestorsEnumerable/Enumerator
struct  Enumerator_t3403847843 
{
public:
	// System.Boolean Unity.Linq.GameObjectExtensions/AncestorsEnumerable/Enumerator::canRun
	bool ___canRun_0;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/AncestorsEnumerable/Enumerator::current
	GameObject_t1113636619 * ___current_1;
	// UnityEngine.Transform Unity.Linq.GameObjectExtensions/AncestorsEnumerable/Enumerator::currentTransform
	Transform_t3600365921 * ___currentTransform_2;
	// System.Boolean Unity.Linq.GameObjectExtensions/AncestorsEnumerable/Enumerator::withSelf
	bool ___withSelf_3;

public:
	inline static int32_t get_offset_of_canRun_0() { return static_cast<int32_t>(offsetof(Enumerator_t3403847843, ___canRun_0)); }
	inline bool get_canRun_0() const { return ___canRun_0; }
	inline bool* get_address_of_canRun_0() { return &___canRun_0; }
	inline void set_canRun_0(bool value)
	{
		___canRun_0 = value;
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Enumerator_t3403847843, ___current_1)); }
	inline GameObject_t1113636619 * get_current_1() const { return ___current_1; }
	inline GameObject_t1113636619 ** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(GameObject_t1113636619 * value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((&___current_1), value);
	}

	inline static int32_t get_offset_of_currentTransform_2() { return static_cast<int32_t>(offsetof(Enumerator_t3403847843, ___currentTransform_2)); }
	inline Transform_t3600365921 * get_currentTransform_2() const { return ___currentTransform_2; }
	inline Transform_t3600365921 ** get_address_of_currentTransform_2() { return &___currentTransform_2; }
	inline void set_currentTransform_2(Transform_t3600365921 * value)
	{
		___currentTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___currentTransform_2), value);
	}

	inline static int32_t get_offset_of_withSelf_3() { return static_cast<int32_t>(offsetof(Enumerator_t3403847843, ___withSelf_3)); }
	inline bool get_withSelf_3() const { return ___withSelf_3; }
	inline bool* get_address_of_withSelf_3() { return &___withSelf_3; }
	inline void set_withSelf_3(bool value)
	{
		___withSelf_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/AncestorsEnumerable/Enumerator
struct Enumerator_t3403847843_marshaled_pinvoke
{
	int32_t ___canRun_0;
	GameObject_t1113636619 * ___current_1;
	Transform_t3600365921 * ___currentTransform_2;
	int32_t ___withSelf_3;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/AncestorsEnumerable/Enumerator
struct Enumerator_t3403847843_marshaled_com
{
	int32_t ___canRun_0;
	GameObject_t1113636619 * ___current_1;
	Transform_t3600365921 * ___currentTransform_2;
	int32_t ___withSelf_3;
};
#endif // ENUMERATOR_T3403847843_H
#ifndef DESCENDANTSENUMERABLE_T4167108050_H
#define DESCENDANTSENUMERABLE_T4167108050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/DescendantsEnumerable
struct  DescendantsEnumerable_t4167108050 
{
public:
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/DescendantsEnumerable::origin
	GameObject_t1113636619 * ___origin_1;
	// System.Boolean Unity.Linq.GameObjectExtensions/DescendantsEnumerable::withSelf
	bool ___withSelf_2;
	// System.Func`2<UnityEngine.Transform,System.Boolean> Unity.Linq.GameObjectExtensions/DescendantsEnumerable::descendIntoChildren
	Func_2_t1722577774 * ___descendIntoChildren_3;

public:
	inline static int32_t get_offset_of_origin_1() { return static_cast<int32_t>(offsetof(DescendantsEnumerable_t4167108050, ___origin_1)); }
	inline GameObject_t1113636619 * get_origin_1() const { return ___origin_1; }
	inline GameObject_t1113636619 ** get_address_of_origin_1() { return &___origin_1; }
	inline void set_origin_1(GameObject_t1113636619 * value)
	{
		___origin_1 = value;
		Il2CppCodeGenWriteBarrier((&___origin_1), value);
	}

	inline static int32_t get_offset_of_withSelf_2() { return static_cast<int32_t>(offsetof(DescendantsEnumerable_t4167108050, ___withSelf_2)); }
	inline bool get_withSelf_2() const { return ___withSelf_2; }
	inline bool* get_address_of_withSelf_2() { return &___withSelf_2; }
	inline void set_withSelf_2(bool value)
	{
		___withSelf_2 = value;
	}

	inline static int32_t get_offset_of_descendIntoChildren_3() { return static_cast<int32_t>(offsetof(DescendantsEnumerable_t4167108050, ___descendIntoChildren_3)); }
	inline Func_2_t1722577774 * get_descendIntoChildren_3() const { return ___descendIntoChildren_3; }
	inline Func_2_t1722577774 ** get_address_of_descendIntoChildren_3() { return &___descendIntoChildren_3; }
	inline void set_descendIntoChildren_3(Func_2_t1722577774 * value)
	{
		___descendIntoChildren_3 = value;
		Il2CppCodeGenWriteBarrier((&___descendIntoChildren_3), value);
	}
};

struct DescendantsEnumerable_t4167108050_StaticFields
{
public:
	// System.Func`2<UnityEngine.Transform,System.Boolean> Unity.Linq.GameObjectExtensions/DescendantsEnumerable::alwaysTrue
	Func_2_t1722577774 * ___alwaysTrue_0;

public:
	inline static int32_t get_offset_of_alwaysTrue_0() { return static_cast<int32_t>(offsetof(DescendantsEnumerable_t4167108050_StaticFields, ___alwaysTrue_0)); }
	inline Func_2_t1722577774 * get_alwaysTrue_0() const { return ___alwaysTrue_0; }
	inline Func_2_t1722577774 ** get_address_of_alwaysTrue_0() { return &___alwaysTrue_0; }
	inline void set_alwaysTrue_0(Func_2_t1722577774 * value)
	{
		___alwaysTrue_0 = value;
		Il2CppCodeGenWriteBarrier((&___alwaysTrue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/DescendantsEnumerable
struct DescendantsEnumerable_t4167108050_marshaled_pinvoke
{
	GameObject_t1113636619 * ___origin_1;
	int32_t ___withSelf_2;
	Il2CppMethodPointer ___descendIntoChildren_3;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/DescendantsEnumerable
struct DescendantsEnumerable_t4167108050_marshaled_com
{
	GameObject_t1113636619 * ___origin_1;
	int32_t ___withSelf_2;
	Il2CppMethodPointer ___descendIntoChildren_3;
};
#endif // DESCENDANTSENUMERABLE_T4167108050_H
#ifndef ENUMERATOR_T1699612215_H
#define ENUMERATOR_T1699612215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator
struct  Enumerator_t1699612215 
{
public:
	// System.Int32 Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator::childCount
	int32_t ___childCount_0;
	// UnityEngine.Transform Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator::originTransform
	Transform_t3600365921 * ___originTransform_1;
	// System.Boolean Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator::canRun
	bool ___canRun_2;
	// System.Boolean Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator::withSelf
	bool ___withSelf_3;
	// System.Int32 Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator::currentIndex
	int32_t ___currentIndex_4;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator::current
	GameObject_t1113636619 * ___current_5;
	// Unity.Linq.GameObjectExtensions/DescendantsEnumerable/InternalUnsafeRefStack Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator::sharedStack
	InternalUnsafeRefStack_t2529159201 * ___sharedStack_6;
	// System.Func`2<UnityEngine.Transform,System.Boolean> Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator::descendIntoChildren
	Func_2_t1722577774 * ___descendIntoChildren_7;

public:
	inline static int32_t get_offset_of_childCount_0() { return static_cast<int32_t>(offsetof(Enumerator_t1699612215, ___childCount_0)); }
	inline int32_t get_childCount_0() const { return ___childCount_0; }
	inline int32_t* get_address_of_childCount_0() { return &___childCount_0; }
	inline void set_childCount_0(int32_t value)
	{
		___childCount_0 = value;
	}

	inline static int32_t get_offset_of_originTransform_1() { return static_cast<int32_t>(offsetof(Enumerator_t1699612215, ___originTransform_1)); }
	inline Transform_t3600365921 * get_originTransform_1() const { return ___originTransform_1; }
	inline Transform_t3600365921 ** get_address_of_originTransform_1() { return &___originTransform_1; }
	inline void set_originTransform_1(Transform_t3600365921 * value)
	{
		___originTransform_1 = value;
		Il2CppCodeGenWriteBarrier((&___originTransform_1), value);
	}

	inline static int32_t get_offset_of_canRun_2() { return static_cast<int32_t>(offsetof(Enumerator_t1699612215, ___canRun_2)); }
	inline bool get_canRun_2() const { return ___canRun_2; }
	inline bool* get_address_of_canRun_2() { return &___canRun_2; }
	inline void set_canRun_2(bool value)
	{
		___canRun_2 = value;
	}

	inline static int32_t get_offset_of_withSelf_3() { return static_cast<int32_t>(offsetof(Enumerator_t1699612215, ___withSelf_3)); }
	inline bool get_withSelf_3() const { return ___withSelf_3; }
	inline bool* get_address_of_withSelf_3() { return &___withSelf_3; }
	inline void set_withSelf_3(bool value)
	{
		___withSelf_3 = value;
	}

	inline static int32_t get_offset_of_currentIndex_4() { return static_cast<int32_t>(offsetof(Enumerator_t1699612215, ___currentIndex_4)); }
	inline int32_t get_currentIndex_4() const { return ___currentIndex_4; }
	inline int32_t* get_address_of_currentIndex_4() { return &___currentIndex_4; }
	inline void set_currentIndex_4(int32_t value)
	{
		___currentIndex_4 = value;
	}

	inline static int32_t get_offset_of_current_5() { return static_cast<int32_t>(offsetof(Enumerator_t1699612215, ___current_5)); }
	inline GameObject_t1113636619 * get_current_5() const { return ___current_5; }
	inline GameObject_t1113636619 ** get_address_of_current_5() { return &___current_5; }
	inline void set_current_5(GameObject_t1113636619 * value)
	{
		___current_5 = value;
		Il2CppCodeGenWriteBarrier((&___current_5), value);
	}

	inline static int32_t get_offset_of_sharedStack_6() { return static_cast<int32_t>(offsetof(Enumerator_t1699612215, ___sharedStack_6)); }
	inline InternalUnsafeRefStack_t2529159201 * get_sharedStack_6() const { return ___sharedStack_6; }
	inline InternalUnsafeRefStack_t2529159201 ** get_address_of_sharedStack_6() { return &___sharedStack_6; }
	inline void set_sharedStack_6(InternalUnsafeRefStack_t2529159201 * value)
	{
		___sharedStack_6 = value;
		Il2CppCodeGenWriteBarrier((&___sharedStack_6), value);
	}

	inline static int32_t get_offset_of_descendIntoChildren_7() { return static_cast<int32_t>(offsetof(Enumerator_t1699612215, ___descendIntoChildren_7)); }
	inline Func_2_t1722577774 * get_descendIntoChildren_7() const { return ___descendIntoChildren_7; }
	inline Func_2_t1722577774 ** get_address_of_descendIntoChildren_7() { return &___descendIntoChildren_7; }
	inline void set_descendIntoChildren_7(Func_2_t1722577774 * value)
	{
		___descendIntoChildren_7 = value;
		Il2CppCodeGenWriteBarrier((&___descendIntoChildren_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator
struct Enumerator_t1699612215_marshaled_pinvoke
{
	int32_t ___childCount_0;
	Transform_t3600365921 * ___originTransform_1;
	int32_t ___canRun_2;
	int32_t ___withSelf_3;
	int32_t ___currentIndex_4;
	GameObject_t1113636619 * ___current_5;
	InternalUnsafeRefStack_t2529159201 * ___sharedStack_6;
	Il2CppMethodPointer ___descendIntoChildren_7;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator
struct Enumerator_t1699612215_marshaled_com
{
	int32_t ___childCount_0;
	Transform_t3600365921 * ___originTransform_1;
	int32_t ___canRun_2;
	int32_t ___withSelf_3;
	int32_t ___currentIndex_4;
	GameObject_t1113636619 * ___current_5;
	InternalUnsafeRefStack_t2529159201 * ___sharedStack_6;
	Il2CppMethodPointer ___descendIntoChildren_7;
};
#endif // ENUMERATOR_T1699612215_H
#ifndef BEFORESELFENUMERABLE_T2845913035_H
#define BEFORESELFENUMERABLE_T2845913035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable
struct  BeforeSelfEnumerable_t2845913035 
{
public:
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable::origin
	GameObject_t1113636619 * ___origin_0;
	// System.Boolean Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable::withSelf
	bool ___withSelf_1;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(BeforeSelfEnumerable_t2845913035, ___origin_0)); }
	inline GameObject_t1113636619 * get_origin_0() const { return ___origin_0; }
	inline GameObject_t1113636619 ** get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(GameObject_t1113636619 * value)
	{
		___origin_0 = value;
		Il2CppCodeGenWriteBarrier((&___origin_0), value);
	}

	inline static int32_t get_offset_of_withSelf_1() { return static_cast<int32_t>(offsetof(BeforeSelfEnumerable_t2845913035, ___withSelf_1)); }
	inline bool get_withSelf_1() const { return ___withSelf_1; }
	inline bool* get_address_of_withSelf_1() { return &___withSelf_1; }
	inline void set_withSelf_1(bool value)
	{
		___withSelf_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable
struct BeforeSelfEnumerable_t2845913035_marshaled_pinvoke
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable
struct BeforeSelfEnumerable_t2845913035_marshaled_com
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
#endif // BEFORESELFENUMERABLE_T2845913035_H
#ifndef ENUMERATOR_T4036937675_H
#define ENUMERATOR_T4036937675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/Enumerator
struct  Enumerator_t4036937675 
{
public:
	// System.Int32 Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/Enumerator::childCount
	int32_t ___childCount_0;
	// UnityEngine.Transform Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/Enumerator::originTransform
	Transform_t3600365921 * ___originTransform_1;
	// System.Boolean Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/Enumerator::canRun
	bool ___canRun_2;
	// System.Boolean Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/Enumerator::withSelf
	bool ___withSelf_3;
	// System.Int32 Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/Enumerator::currentIndex
	int32_t ___currentIndex_4;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/Enumerator::current
	GameObject_t1113636619 * ___current_5;
	// UnityEngine.Transform Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/Enumerator::parent
	Transform_t3600365921 * ___parent_6;

public:
	inline static int32_t get_offset_of_childCount_0() { return static_cast<int32_t>(offsetof(Enumerator_t4036937675, ___childCount_0)); }
	inline int32_t get_childCount_0() const { return ___childCount_0; }
	inline int32_t* get_address_of_childCount_0() { return &___childCount_0; }
	inline void set_childCount_0(int32_t value)
	{
		___childCount_0 = value;
	}

	inline static int32_t get_offset_of_originTransform_1() { return static_cast<int32_t>(offsetof(Enumerator_t4036937675, ___originTransform_1)); }
	inline Transform_t3600365921 * get_originTransform_1() const { return ___originTransform_1; }
	inline Transform_t3600365921 ** get_address_of_originTransform_1() { return &___originTransform_1; }
	inline void set_originTransform_1(Transform_t3600365921 * value)
	{
		___originTransform_1 = value;
		Il2CppCodeGenWriteBarrier((&___originTransform_1), value);
	}

	inline static int32_t get_offset_of_canRun_2() { return static_cast<int32_t>(offsetof(Enumerator_t4036937675, ___canRun_2)); }
	inline bool get_canRun_2() const { return ___canRun_2; }
	inline bool* get_address_of_canRun_2() { return &___canRun_2; }
	inline void set_canRun_2(bool value)
	{
		___canRun_2 = value;
	}

	inline static int32_t get_offset_of_withSelf_3() { return static_cast<int32_t>(offsetof(Enumerator_t4036937675, ___withSelf_3)); }
	inline bool get_withSelf_3() const { return ___withSelf_3; }
	inline bool* get_address_of_withSelf_3() { return &___withSelf_3; }
	inline void set_withSelf_3(bool value)
	{
		___withSelf_3 = value;
	}

	inline static int32_t get_offset_of_currentIndex_4() { return static_cast<int32_t>(offsetof(Enumerator_t4036937675, ___currentIndex_4)); }
	inline int32_t get_currentIndex_4() const { return ___currentIndex_4; }
	inline int32_t* get_address_of_currentIndex_4() { return &___currentIndex_4; }
	inline void set_currentIndex_4(int32_t value)
	{
		___currentIndex_4 = value;
	}

	inline static int32_t get_offset_of_current_5() { return static_cast<int32_t>(offsetof(Enumerator_t4036937675, ___current_5)); }
	inline GameObject_t1113636619 * get_current_5() const { return ___current_5; }
	inline GameObject_t1113636619 ** get_address_of_current_5() { return &___current_5; }
	inline void set_current_5(GameObject_t1113636619 * value)
	{
		___current_5 = value;
		Il2CppCodeGenWriteBarrier((&___current_5), value);
	}

	inline static int32_t get_offset_of_parent_6() { return static_cast<int32_t>(offsetof(Enumerator_t4036937675, ___parent_6)); }
	inline Transform_t3600365921 * get_parent_6() const { return ___parent_6; }
	inline Transform_t3600365921 ** get_address_of_parent_6() { return &___parent_6; }
	inline void set_parent_6(Transform_t3600365921 * value)
	{
		___parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___parent_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/Enumerator
struct Enumerator_t4036937675_marshaled_pinvoke
{
	int32_t ___childCount_0;
	Transform_t3600365921 * ___originTransform_1;
	int32_t ___canRun_2;
	int32_t ___withSelf_3;
	int32_t ___currentIndex_4;
	GameObject_t1113636619 * ___current_5;
	Transform_t3600365921 * ___parent_6;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/BeforeSelfEnumerable/Enumerator
struct Enumerator_t4036937675_marshaled_com
{
	int32_t ___childCount_0;
	Transform_t3600365921 * ___originTransform_1;
	int32_t ___canRun_2;
	int32_t ___withSelf_3;
	int32_t ___currentIndex_4;
	GameObject_t1113636619 * ___current_5;
	Transform_t3600365921 * ___parent_6;
};
#endif // ENUMERATOR_T4036937675_H
#ifndef ENUMERATOR_T20295438_H
#define ENUMERATOR_T20295438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/Enumerator
struct  Enumerator_t20295438 
{
public:
	// System.Int32 Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/Enumerator::childCount
	int32_t ___childCount_0;
	// UnityEngine.Transform Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/Enumerator::originTransform
	Transform_t3600365921 * ___originTransform_1;
	// System.Boolean Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/Enumerator::canRun
	bool ___canRun_2;
	// System.Boolean Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/Enumerator::withSelf
	bool ___withSelf_3;
	// System.Int32 Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/Enumerator::currentIndex
	int32_t ___currentIndex_4;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/Enumerator::current
	GameObject_t1113636619 * ___current_5;
	// UnityEngine.Transform Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/Enumerator::parent
	Transform_t3600365921 * ___parent_6;

public:
	inline static int32_t get_offset_of_childCount_0() { return static_cast<int32_t>(offsetof(Enumerator_t20295438, ___childCount_0)); }
	inline int32_t get_childCount_0() const { return ___childCount_0; }
	inline int32_t* get_address_of_childCount_0() { return &___childCount_0; }
	inline void set_childCount_0(int32_t value)
	{
		___childCount_0 = value;
	}

	inline static int32_t get_offset_of_originTransform_1() { return static_cast<int32_t>(offsetof(Enumerator_t20295438, ___originTransform_1)); }
	inline Transform_t3600365921 * get_originTransform_1() const { return ___originTransform_1; }
	inline Transform_t3600365921 ** get_address_of_originTransform_1() { return &___originTransform_1; }
	inline void set_originTransform_1(Transform_t3600365921 * value)
	{
		___originTransform_1 = value;
		Il2CppCodeGenWriteBarrier((&___originTransform_1), value);
	}

	inline static int32_t get_offset_of_canRun_2() { return static_cast<int32_t>(offsetof(Enumerator_t20295438, ___canRun_2)); }
	inline bool get_canRun_2() const { return ___canRun_2; }
	inline bool* get_address_of_canRun_2() { return &___canRun_2; }
	inline void set_canRun_2(bool value)
	{
		___canRun_2 = value;
	}

	inline static int32_t get_offset_of_withSelf_3() { return static_cast<int32_t>(offsetof(Enumerator_t20295438, ___withSelf_3)); }
	inline bool get_withSelf_3() const { return ___withSelf_3; }
	inline bool* get_address_of_withSelf_3() { return &___withSelf_3; }
	inline void set_withSelf_3(bool value)
	{
		___withSelf_3 = value;
	}

	inline static int32_t get_offset_of_currentIndex_4() { return static_cast<int32_t>(offsetof(Enumerator_t20295438, ___currentIndex_4)); }
	inline int32_t get_currentIndex_4() const { return ___currentIndex_4; }
	inline int32_t* get_address_of_currentIndex_4() { return &___currentIndex_4; }
	inline void set_currentIndex_4(int32_t value)
	{
		___currentIndex_4 = value;
	}

	inline static int32_t get_offset_of_current_5() { return static_cast<int32_t>(offsetof(Enumerator_t20295438, ___current_5)); }
	inline GameObject_t1113636619 * get_current_5() const { return ___current_5; }
	inline GameObject_t1113636619 ** get_address_of_current_5() { return &___current_5; }
	inline void set_current_5(GameObject_t1113636619 * value)
	{
		___current_5 = value;
		Il2CppCodeGenWriteBarrier((&___current_5), value);
	}

	inline static int32_t get_offset_of_parent_6() { return static_cast<int32_t>(offsetof(Enumerator_t20295438, ___parent_6)); }
	inline Transform_t3600365921 * get_parent_6() const { return ___parent_6; }
	inline Transform_t3600365921 ** get_address_of_parent_6() { return &___parent_6; }
	inline void set_parent_6(Transform_t3600365921 * value)
	{
		___parent_6 = value;
		Il2CppCodeGenWriteBarrier((&___parent_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/Enumerator
struct Enumerator_t20295438_marshaled_pinvoke
{
	int32_t ___childCount_0;
	Transform_t3600365921 * ___originTransform_1;
	int32_t ___canRun_2;
	int32_t ___withSelf_3;
	int32_t ___currentIndex_4;
	GameObject_t1113636619 * ___current_5;
	Transform_t3600365921 * ___parent_6;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/AfterSelfEnumerable/Enumerator
struct Enumerator_t20295438_marshaled_com
{
	int32_t ___childCount_0;
	Transform_t3600365921 * ___originTransform_1;
	int32_t ___canRun_2;
	int32_t ___withSelf_3;
	int32_t ___currentIndex_4;
	GameObject_t1113636619 * ___current_5;
	Transform_t3600365921 * ___parent_6;
};
#endif // ENUMERATOR_T20295438_H
#ifndef AFTERSELFENUMERABLE_T4038755258_H
#define AFTERSELFENUMERABLE_T4038755258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/AfterSelfEnumerable
struct  AfterSelfEnumerable_t4038755258 
{
public:
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/AfterSelfEnumerable::origin
	GameObject_t1113636619 * ___origin_0;
	// System.Boolean Unity.Linq.GameObjectExtensions/AfterSelfEnumerable::withSelf
	bool ___withSelf_1;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(AfterSelfEnumerable_t4038755258, ___origin_0)); }
	inline GameObject_t1113636619 * get_origin_0() const { return ___origin_0; }
	inline GameObject_t1113636619 ** get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(GameObject_t1113636619 * value)
	{
		___origin_0 = value;
		Il2CppCodeGenWriteBarrier((&___origin_0), value);
	}

	inline static int32_t get_offset_of_withSelf_1() { return static_cast<int32_t>(offsetof(AfterSelfEnumerable_t4038755258, ___withSelf_1)); }
	inline bool get_withSelf_1() const { return ___withSelf_1; }
	inline bool* get_address_of_withSelf_1() { return &___withSelf_1; }
	inline void set_withSelf_1(bool value)
	{
		___withSelf_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Unity.Linq.GameObjectExtensions/AfterSelfEnumerable
struct AfterSelfEnumerable_t4038755258_marshaled_pinvoke
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
// Native definition for COM marshalling of Unity.Linq.GameObjectExtensions/AfterSelfEnumerable
struct AfterSelfEnumerable_t4038755258_marshaled_com
{
	GameObject_t1113636619 * ___origin_0;
	int32_t ___withSelf_1;
};
#endif // AFTERSELFENUMERABLE_T4038755258_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef POSE3D_T2649470188_H
#define POSE3D_T2649470188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pose3D
struct  Pose3D_t2649470188  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Pose3D::<Position>k__BackingField
	Vector3_t3722313464  ___U3CPositionU3Ek__BackingField_1;
	// UnityEngine.Quaternion Pose3D::<Orientation>k__BackingField
	Quaternion_t2301928331  ___U3COrientationU3Ek__BackingField_2;
	// UnityEngine.Matrix4x4 Pose3D::<Matrix>k__BackingField
	Matrix4x4_t1817901843  ___U3CMatrixU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Pose3D_t2649470188, ___U3CPositionU3Ek__BackingField_1)); }
	inline Vector3_t3722313464  get_U3CPositionU3Ek__BackingField_1() const { return ___U3CPositionU3Ek__BackingField_1; }
	inline Vector3_t3722313464 * get_address_of_U3CPositionU3Ek__BackingField_1() { return &___U3CPositionU3Ek__BackingField_1; }
	inline void set_U3CPositionU3Ek__BackingField_1(Vector3_t3722313464  value)
	{
		___U3CPositionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3COrientationU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Pose3D_t2649470188, ___U3COrientationU3Ek__BackingField_2)); }
	inline Quaternion_t2301928331  get_U3COrientationU3Ek__BackingField_2() const { return ___U3COrientationU3Ek__BackingField_2; }
	inline Quaternion_t2301928331 * get_address_of_U3COrientationU3Ek__BackingField_2() { return &___U3COrientationU3Ek__BackingField_2; }
	inline void set_U3COrientationU3Ek__BackingField_2(Quaternion_t2301928331  value)
	{
		___U3COrientationU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CMatrixU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Pose3D_t2649470188, ___U3CMatrixU3Ek__BackingField_3)); }
	inline Matrix4x4_t1817901843  get_U3CMatrixU3Ek__BackingField_3() const { return ___U3CMatrixU3Ek__BackingField_3; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CMatrixU3Ek__BackingField_3() { return &___U3CMatrixU3Ek__BackingField_3; }
	inline void set_U3CMatrixU3Ek__BackingField_3(Matrix4x4_t1817901843  value)
	{
		___U3CMatrixU3Ek__BackingField_3 = value;
	}
};

struct Pose3D_t2649470188_StaticFields
{
public:
	// UnityEngine.Matrix4x4 Pose3D::flipZ
	Matrix4x4_t1817901843  ___flipZ_0;

public:
	inline static int32_t get_offset_of_flipZ_0() { return static_cast<int32_t>(offsetof(Pose3D_t2649470188_StaticFields, ___flipZ_0)); }
	inline Matrix4x4_t1817901843  get_flipZ_0() const { return ___flipZ_0; }
	inline Matrix4x4_t1817901843 * get_address_of_flipZ_0() { return &___flipZ_0; }
	inline void set_flipZ_0(Matrix4x4_t1817901843  value)
	{
		___flipZ_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSE3D_T2649470188_H
#ifndef OPCODE_T2755924248_H
#define OPCODE_T2755924248_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Opcode
struct  Opcode_t2755924248 
{
public:
	// System.Byte WebSocketSharp.Opcode::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Opcode_t2755924248, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPCODE_T2755924248_H
#ifndef BASEVRDEVICE_T2561664142_H
#define BASEVRDEVICE_T2561664142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseVRDevice
struct  BaseVRDevice_t2561664142  : public RuntimeObject
{
public:
	// CardboardProfile BaseVRDevice::<Profile>k__BackingField
	CardboardProfile_t2246179929 * ___U3CProfileU3Ek__BackingField_1;
	// MutablePose3D BaseVRDevice::headPose
	MutablePose3D_t3352419872 * ___headPose_2;
	// MutablePose3D BaseVRDevice::leftEyePose
	MutablePose3D_t3352419872 * ___leftEyePose_3;
	// MutablePose3D BaseVRDevice::rightEyePose
	MutablePose3D_t3352419872 * ___rightEyePose_4;
	// UnityEngine.Matrix4x4 BaseVRDevice::leftEyeDistortedProjection
	Matrix4x4_t1817901843  ___leftEyeDistortedProjection_5;
	// UnityEngine.Matrix4x4 BaseVRDevice::rightEyeDistortedProjection
	Matrix4x4_t1817901843  ___rightEyeDistortedProjection_6;
	// UnityEngine.Matrix4x4 BaseVRDevice::leftEyeUndistortedProjection
	Matrix4x4_t1817901843  ___leftEyeUndistortedProjection_7;
	// UnityEngine.Matrix4x4 BaseVRDevice::rightEyeUndistortedProjection
	Matrix4x4_t1817901843  ___rightEyeUndistortedProjection_8;
	// UnityEngine.Rect BaseVRDevice::leftEyeDistortedViewport
	Rect_t2360479859  ___leftEyeDistortedViewport_9;
	// UnityEngine.Rect BaseVRDevice::rightEyeDistortedViewport
	Rect_t2360479859  ___rightEyeDistortedViewport_10;
	// UnityEngine.Rect BaseVRDevice::leftEyeUndistortedViewport
	Rect_t2360479859  ___leftEyeUndistortedViewport_11;
	// UnityEngine.Rect BaseVRDevice::rightEyeUndistortedViewport
	Rect_t2360479859  ___rightEyeUndistortedViewport_12;
	// UnityEngine.Vector2 BaseVRDevice::recommendedTextureSize
	Vector2_t2156229523  ___recommendedTextureSize_13;
	// System.Int32 BaseVRDevice::leftEyeOrientation
	int32_t ___leftEyeOrientation_14;
	// System.Int32 BaseVRDevice::rightEyeOrientation
	int32_t ___rightEyeOrientation_15;
	// System.Boolean BaseVRDevice::triggered
	bool ___triggered_16;
	// System.Boolean BaseVRDevice::tilted
	bool ___tilted_17;
	// System.Boolean BaseVRDevice::profileChanged
	bool ___profileChanged_18;
	// System.Boolean BaseVRDevice::backButtonPressed
	bool ___backButtonPressed_19;

public:
	inline static int32_t get_offset_of_U3CProfileU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___U3CProfileU3Ek__BackingField_1)); }
	inline CardboardProfile_t2246179929 * get_U3CProfileU3Ek__BackingField_1() const { return ___U3CProfileU3Ek__BackingField_1; }
	inline CardboardProfile_t2246179929 ** get_address_of_U3CProfileU3Ek__BackingField_1() { return &___U3CProfileU3Ek__BackingField_1; }
	inline void set_U3CProfileU3Ek__BackingField_1(CardboardProfile_t2246179929 * value)
	{
		___U3CProfileU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProfileU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_headPose_2() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___headPose_2)); }
	inline MutablePose3D_t3352419872 * get_headPose_2() const { return ___headPose_2; }
	inline MutablePose3D_t3352419872 ** get_address_of_headPose_2() { return &___headPose_2; }
	inline void set_headPose_2(MutablePose3D_t3352419872 * value)
	{
		___headPose_2 = value;
		Il2CppCodeGenWriteBarrier((&___headPose_2), value);
	}

	inline static int32_t get_offset_of_leftEyePose_3() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___leftEyePose_3)); }
	inline MutablePose3D_t3352419872 * get_leftEyePose_3() const { return ___leftEyePose_3; }
	inline MutablePose3D_t3352419872 ** get_address_of_leftEyePose_3() { return &___leftEyePose_3; }
	inline void set_leftEyePose_3(MutablePose3D_t3352419872 * value)
	{
		___leftEyePose_3 = value;
		Il2CppCodeGenWriteBarrier((&___leftEyePose_3), value);
	}

	inline static int32_t get_offset_of_rightEyePose_4() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___rightEyePose_4)); }
	inline MutablePose3D_t3352419872 * get_rightEyePose_4() const { return ___rightEyePose_4; }
	inline MutablePose3D_t3352419872 ** get_address_of_rightEyePose_4() { return &___rightEyePose_4; }
	inline void set_rightEyePose_4(MutablePose3D_t3352419872 * value)
	{
		___rightEyePose_4 = value;
		Il2CppCodeGenWriteBarrier((&___rightEyePose_4), value);
	}

	inline static int32_t get_offset_of_leftEyeDistortedProjection_5() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___leftEyeDistortedProjection_5)); }
	inline Matrix4x4_t1817901843  get_leftEyeDistortedProjection_5() const { return ___leftEyeDistortedProjection_5; }
	inline Matrix4x4_t1817901843 * get_address_of_leftEyeDistortedProjection_5() { return &___leftEyeDistortedProjection_5; }
	inline void set_leftEyeDistortedProjection_5(Matrix4x4_t1817901843  value)
	{
		___leftEyeDistortedProjection_5 = value;
	}

	inline static int32_t get_offset_of_rightEyeDistortedProjection_6() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___rightEyeDistortedProjection_6)); }
	inline Matrix4x4_t1817901843  get_rightEyeDistortedProjection_6() const { return ___rightEyeDistortedProjection_6; }
	inline Matrix4x4_t1817901843 * get_address_of_rightEyeDistortedProjection_6() { return &___rightEyeDistortedProjection_6; }
	inline void set_rightEyeDistortedProjection_6(Matrix4x4_t1817901843  value)
	{
		___rightEyeDistortedProjection_6 = value;
	}

	inline static int32_t get_offset_of_leftEyeUndistortedProjection_7() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___leftEyeUndistortedProjection_7)); }
	inline Matrix4x4_t1817901843  get_leftEyeUndistortedProjection_7() const { return ___leftEyeUndistortedProjection_7; }
	inline Matrix4x4_t1817901843 * get_address_of_leftEyeUndistortedProjection_7() { return &___leftEyeUndistortedProjection_7; }
	inline void set_leftEyeUndistortedProjection_7(Matrix4x4_t1817901843  value)
	{
		___leftEyeUndistortedProjection_7 = value;
	}

	inline static int32_t get_offset_of_rightEyeUndistortedProjection_8() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___rightEyeUndistortedProjection_8)); }
	inline Matrix4x4_t1817901843  get_rightEyeUndistortedProjection_8() const { return ___rightEyeUndistortedProjection_8; }
	inline Matrix4x4_t1817901843 * get_address_of_rightEyeUndistortedProjection_8() { return &___rightEyeUndistortedProjection_8; }
	inline void set_rightEyeUndistortedProjection_8(Matrix4x4_t1817901843  value)
	{
		___rightEyeUndistortedProjection_8 = value;
	}

	inline static int32_t get_offset_of_leftEyeDistortedViewport_9() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___leftEyeDistortedViewport_9)); }
	inline Rect_t2360479859  get_leftEyeDistortedViewport_9() const { return ___leftEyeDistortedViewport_9; }
	inline Rect_t2360479859 * get_address_of_leftEyeDistortedViewport_9() { return &___leftEyeDistortedViewport_9; }
	inline void set_leftEyeDistortedViewport_9(Rect_t2360479859  value)
	{
		___leftEyeDistortedViewport_9 = value;
	}

	inline static int32_t get_offset_of_rightEyeDistortedViewport_10() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___rightEyeDistortedViewport_10)); }
	inline Rect_t2360479859  get_rightEyeDistortedViewport_10() const { return ___rightEyeDistortedViewport_10; }
	inline Rect_t2360479859 * get_address_of_rightEyeDistortedViewport_10() { return &___rightEyeDistortedViewport_10; }
	inline void set_rightEyeDistortedViewport_10(Rect_t2360479859  value)
	{
		___rightEyeDistortedViewport_10 = value;
	}

	inline static int32_t get_offset_of_leftEyeUndistortedViewport_11() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___leftEyeUndistortedViewport_11)); }
	inline Rect_t2360479859  get_leftEyeUndistortedViewport_11() const { return ___leftEyeUndistortedViewport_11; }
	inline Rect_t2360479859 * get_address_of_leftEyeUndistortedViewport_11() { return &___leftEyeUndistortedViewport_11; }
	inline void set_leftEyeUndistortedViewport_11(Rect_t2360479859  value)
	{
		___leftEyeUndistortedViewport_11 = value;
	}

	inline static int32_t get_offset_of_rightEyeUndistortedViewport_12() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___rightEyeUndistortedViewport_12)); }
	inline Rect_t2360479859  get_rightEyeUndistortedViewport_12() const { return ___rightEyeUndistortedViewport_12; }
	inline Rect_t2360479859 * get_address_of_rightEyeUndistortedViewport_12() { return &___rightEyeUndistortedViewport_12; }
	inline void set_rightEyeUndistortedViewport_12(Rect_t2360479859  value)
	{
		___rightEyeUndistortedViewport_12 = value;
	}

	inline static int32_t get_offset_of_recommendedTextureSize_13() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___recommendedTextureSize_13)); }
	inline Vector2_t2156229523  get_recommendedTextureSize_13() const { return ___recommendedTextureSize_13; }
	inline Vector2_t2156229523 * get_address_of_recommendedTextureSize_13() { return &___recommendedTextureSize_13; }
	inline void set_recommendedTextureSize_13(Vector2_t2156229523  value)
	{
		___recommendedTextureSize_13 = value;
	}

	inline static int32_t get_offset_of_leftEyeOrientation_14() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___leftEyeOrientation_14)); }
	inline int32_t get_leftEyeOrientation_14() const { return ___leftEyeOrientation_14; }
	inline int32_t* get_address_of_leftEyeOrientation_14() { return &___leftEyeOrientation_14; }
	inline void set_leftEyeOrientation_14(int32_t value)
	{
		___leftEyeOrientation_14 = value;
	}

	inline static int32_t get_offset_of_rightEyeOrientation_15() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___rightEyeOrientation_15)); }
	inline int32_t get_rightEyeOrientation_15() const { return ___rightEyeOrientation_15; }
	inline int32_t* get_address_of_rightEyeOrientation_15() { return &___rightEyeOrientation_15; }
	inline void set_rightEyeOrientation_15(int32_t value)
	{
		___rightEyeOrientation_15 = value;
	}

	inline static int32_t get_offset_of_triggered_16() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___triggered_16)); }
	inline bool get_triggered_16() const { return ___triggered_16; }
	inline bool* get_address_of_triggered_16() { return &___triggered_16; }
	inline void set_triggered_16(bool value)
	{
		___triggered_16 = value;
	}

	inline static int32_t get_offset_of_tilted_17() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___tilted_17)); }
	inline bool get_tilted_17() const { return ___tilted_17; }
	inline bool* get_address_of_tilted_17() { return &___tilted_17; }
	inline void set_tilted_17(bool value)
	{
		___tilted_17 = value;
	}

	inline static int32_t get_offset_of_profileChanged_18() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___profileChanged_18)); }
	inline bool get_profileChanged_18() const { return ___profileChanged_18; }
	inline bool* get_address_of_profileChanged_18() { return &___profileChanged_18; }
	inline void set_profileChanged_18(bool value)
	{
		___profileChanged_18 = value;
	}

	inline static int32_t get_offset_of_backButtonPressed_19() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142, ___backButtonPressed_19)); }
	inline bool get_backButtonPressed_19() const { return ___backButtonPressed_19; }
	inline bool* get_address_of_backButtonPressed_19() { return &___backButtonPressed_19; }
	inline void set_backButtonPressed_19(bool value)
	{
		___backButtonPressed_19 = value;
	}
};

struct BaseVRDevice_t2561664142_StaticFields
{
public:
	// BaseVRDevice BaseVRDevice::device
	BaseVRDevice_t2561664142 * ___device_0;

public:
	inline static int32_t get_offset_of_device_0() { return static_cast<int32_t>(offsetof(BaseVRDevice_t2561664142_StaticFields, ___device_0)); }
	inline BaseVRDevice_t2561664142 * get_device_0() const { return ___device_0; }
	inline BaseVRDevice_t2561664142 ** get_address_of_device_0() { return &___device_0; }
	inline void set_device_0(BaseVRDevice_t2561664142 * value)
	{
		___device_0 = value;
		Il2CppCodeGenWriteBarrier((&___device_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVRDEVICE_T2561664142_H
#ifndef FIN_T411169233_H
#define FIN_T411169233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Fin
struct  Fin_t411169233 
{
public:
	// System.Byte WebSocketSharp.Fin::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Fin_t411169233, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIN_T411169233_H
#ifndef CHUNKEDREQUESTSTREAM_T1674253063_H
#define CHUNKEDREQUESTSTREAM_T1674253063_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.ChunkedRequestStream
struct  ChunkedRequestStream_t1674253063  : public RequestStream_t1020063535
{
public:
	// WebSocketSharp.Net.HttpListenerContext WebSocketSharp.Net.ChunkedRequestStream::_context
	HttpListenerContext_t3723273891 * ____context_8;
	// WebSocketSharp.Net.ChunkStream WebSocketSharp.Net.ChunkedRequestStream::_decoder
	ChunkStream_t2280345721 * ____decoder_9;
	// System.Boolean WebSocketSharp.Net.ChunkedRequestStream::_disposed
	bool ____disposed_10;
	// System.Boolean WebSocketSharp.Net.ChunkedRequestStream::_noMoreData
	bool ____noMoreData_11;

public:
	inline static int32_t get_offset_of__context_8() { return static_cast<int32_t>(offsetof(ChunkedRequestStream_t1674253063, ____context_8)); }
	inline HttpListenerContext_t3723273891 * get__context_8() const { return ____context_8; }
	inline HttpListenerContext_t3723273891 ** get_address_of__context_8() { return &____context_8; }
	inline void set__context_8(HttpListenerContext_t3723273891 * value)
	{
		____context_8 = value;
		Il2CppCodeGenWriteBarrier((&____context_8), value);
	}

	inline static int32_t get_offset_of__decoder_9() { return static_cast<int32_t>(offsetof(ChunkedRequestStream_t1674253063, ____decoder_9)); }
	inline ChunkStream_t2280345721 * get__decoder_9() const { return ____decoder_9; }
	inline ChunkStream_t2280345721 ** get_address_of__decoder_9() { return &____decoder_9; }
	inline void set__decoder_9(ChunkStream_t2280345721 * value)
	{
		____decoder_9 = value;
		Il2CppCodeGenWriteBarrier((&____decoder_9), value);
	}

	inline static int32_t get_offset_of__disposed_10() { return static_cast<int32_t>(offsetof(ChunkedRequestStream_t1674253063, ____disposed_10)); }
	inline bool get__disposed_10() const { return ____disposed_10; }
	inline bool* get_address_of__disposed_10() { return &____disposed_10; }
	inline void set__disposed_10(bool value)
	{
		____disposed_10 = value;
	}

	inline static int32_t get_offset_of__noMoreData_11() { return static_cast<int32_t>(offsetof(ChunkedRequestStream_t1674253063, ____noMoreData_11)); }
	inline bool get__noMoreData_11() const { return ____noMoreData_11; }
	inline bool* get_address_of__noMoreData_11() { return &____noMoreData_11; }
	inline void set__noMoreData_11(bool value)
	{
		____noMoreData_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHUNKEDREQUESTSTREAM_T1674253063_H
#ifndef AUTHENTICATIONSCHEMES_T3195200892_H
#define AUTHENTICATIONSCHEMES_T3195200892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.AuthenticationSchemes
struct  AuthenticationSchemes_t3195200892 
{
public:
	// System.Int32 WebSocketSharp.Net.AuthenticationSchemes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AuthenticationSchemes_t3195200892, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONSCHEMES_T3195200892_H
#ifndef MASK_T3471462035_H
#define MASK_T3471462035_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Mask
struct  Mask_t3471462035 
{
public:
	// System.Byte WebSocketSharp.Mask::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mask_t3471462035, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASK_T3471462035_H
#ifndef LOGLEVEL_T2581836550_H
#define LOGLEVEL_T2581836550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.LogLevel
struct  LogLevel_t2581836550 
{
public:
	// System.Int32 WebSocketSharp.LogLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogLevel_t2581836550, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGLEVEL_T2581836550_H
#ifndef SOCKETPACKETTYPES_T3225958894_H
#define SOCKETPACKETTYPES_T3225958894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.SocketPacketTypes
struct  SocketPacketTypes_t3225958894 
{
public:
	// System.Int32 socket.io.SocketPacketTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SocketPacketTypes_t3225958894, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETPACKETTYPES_T3225958894_H
#ifndef U3CCHILDRENANDSELFU3EC__ITERATOR5_T3958859349_H
#define U3CCHILDRENANDSELFU3EC__ITERATOR5_T3958859349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/<ChildrenAndSelf>c__Iterator5
struct  U3CChildrenAndSelfU3Ec__Iterator5_t3958859349  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject> Unity.Linq.GameObjectExtensions/<ChildrenAndSelf>c__Iterator5::source
	RuntimeObject* ___source_0;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Unity.Linq.GameObjectExtensions/<ChildrenAndSelf>c__Iterator5::$locvar0
	RuntimeObject* ___U24locvar0_1;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/<ChildrenAndSelf>c__Iterator5::<item>__1
	GameObject_t1113636619 * ___U3CitemU3E__1_2;
	// Unity.Linq.GameObjectExtensions/ChildrenEnumerable/Enumerator Unity.Linq.GameObjectExtensions/<ChildrenAndSelf>c__Iterator5::<e>__2
	Enumerator_t792918911  ___U3CeU3E__2_3;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/<ChildrenAndSelf>c__Iterator5::$current
	GameObject_t1113636619 * ___U24current_4;
	// System.Boolean Unity.Linq.GameObjectExtensions/<ChildrenAndSelf>c__Iterator5::$disposing
	bool ___U24disposing_5;
	// System.Int32 Unity.Linq.GameObjectExtensions/<ChildrenAndSelf>c__Iterator5::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CChildrenAndSelfU3Ec__Iterator5_t3958859349, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CChildrenAndSelfU3Ec__Iterator5_t3958859349, ___U24locvar0_1)); }
	inline RuntimeObject* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline RuntimeObject** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(RuntimeObject* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U3CitemU3E__1_2() { return static_cast<int32_t>(offsetof(U3CChildrenAndSelfU3Ec__Iterator5_t3958859349, ___U3CitemU3E__1_2)); }
	inline GameObject_t1113636619 * get_U3CitemU3E__1_2() const { return ___U3CitemU3E__1_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CitemU3E__1_2() { return &___U3CitemU3E__1_2; }
	inline void set_U3CitemU3E__1_2(GameObject_t1113636619 * value)
	{
		___U3CitemU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CitemU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CeU3E__2_3() { return static_cast<int32_t>(offsetof(U3CChildrenAndSelfU3Ec__Iterator5_t3958859349, ___U3CeU3E__2_3)); }
	inline Enumerator_t792918911  get_U3CeU3E__2_3() const { return ___U3CeU3E__2_3; }
	inline Enumerator_t792918911 * get_address_of_U3CeU3E__2_3() { return &___U3CeU3E__2_3; }
	inline void set_U3CeU3E__2_3(Enumerator_t792918911  value)
	{
		___U3CeU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CChildrenAndSelfU3Ec__Iterator5_t3958859349, ___U24current_4)); }
	inline GameObject_t1113636619 * get_U24current_4() const { return ___U24current_4; }
	inline GameObject_t1113636619 ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(GameObject_t1113636619 * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CChildrenAndSelfU3Ec__Iterator5_t3958859349, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CChildrenAndSelfU3Ec__Iterator5_t3958859349, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHILDRENANDSELFU3EC__ITERATOR5_T3958859349_H
#ifndef SYSTEMEVENTS_T3087650121_H
#define SYSTEMEVENTS_T3087650121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.SystemEvents
struct  SystemEvents_t3087650121 
{
public:
	// System.Int32 socket.io.SystemEvents::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SystemEvents_t3087650121, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEVENTS_T3087650121_H
#ifndef TRANSFORMCLONETYPE_T3013038762_H
#define TRANSFORMCLONETYPE_T3013038762_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.TransformCloneType
struct  TransformCloneType_t3013038762 
{
public:
	// System.Int32 Unity.Linq.TransformCloneType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransformCloneType_t3013038762, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCLONETYPE_T3013038762_H
#ifndef TRANSFORMMOVETYPE_T1435850903_H
#define TRANSFORMMOVETYPE_T1435850903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.TransformMoveType
struct  TransformMoveType_t1435850903 
{
public:
	// System.Int32 Unity.Linq.TransformMoveType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransformMoveType_t1435850903, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMMOVETYPE_T1435850903_H
#ifndef DEVICETYPES_T2644824665_H
#define DEVICETYPES_T2644824665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardboardProfile/DeviceTypes
struct  DeviceTypes_t2644824665 
{
public:
	// System.Int32 CardboardProfile/DeviceTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DeviceTypes_t2644824665, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETYPES_T2644824665_H
#ifndef U3CINITCOREU3EC__ITERATOR0_T1544445406_H
#define U3CINITCOREU3EC__ITERATOR0_T1544445406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.SocketInitializer/<InitCore>c__Iterator0
struct  U3CInitCoreU3Ec__Iterator0_t1544445406  : public RuntimeObject
{
public:
	// UnityEngine.WWW socket.io.SocketInitializer/<InitCore>c__Iterator0::<www>__1
	WWW_t3688466362 * ___U3CwwwU3E__1_0;
	// UniRx.CancellationToken socket.io.SocketInitializer/<InitCore>c__Iterator0::cancelToken
	CancellationToken_t1265546479  ___cancelToken_1;
	// UniRx.IObserver`1<socket.io.Socket> socket.io.SocketInitializer/<InitCore>c__Iterator0::observer
	RuntimeObject* ___observer_2;
	// System.Int32 socket.io.SocketInitializer/<InitCore>c__Iterator0::<textIndex>__1
	int32_t ___U3CtextIndexU3E__1_3;
	// socket.io.SocketInitializer socket.io.SocketInitializer/<InitCore>c__Iterator0::$this
	SocketInitializer_t383163007 * ___U24this_4;
	// System.Object socket.io.SocketInitializer/<InitCore>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean socket.io.SocketInitializer/<InitCore>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 socket.io.SocketInitializer/<InitCore>c__Iterator0::$PC
	int32_t ___U24PC_7;
	// socket.io.SocketInitializer/<InitCore>c__Iterator0/<InitCore>c__AnonStorey1 socket.io.SocketInitializer/<InitCore>c__Iterator0::$locvar0
	U3CInitCoreU3Ec__AnonStorey1_t2077288943 * ___U24locvar0_8;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__1_0() { return static_cast<int32_t>(offsetof(U3CInitCoreU3Ec__Iterator0_t1544445406, ___U3CwwwU3E__1_0)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__1_0() const { return ___U3CwwwU3E__1_0; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__1_0() { return &___U3CwwwU3E__1_0; }
	inline void set_U3CwwwU3E__1_0(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__1_0), value);
	}

	inline static int32_t get_offset_of_cancelToken_1() { return static_cast<int32_t>(offsetof(U3CInitCoreU3Ec__Iterator0_t1544445406, ___cancelToken_1)); }
	inline CancellationToken_t1265546479  get_cancelToken_1() const { return ___cancelToken_1; }
	inline CancellationToken_t1265546479 * get_address_of_cancelToken_1() { return &___cancelToken_1; }
	inline void set_cancelToken_1(CancellationToken_t1265546479  value)
	{
		___cancelToken_1 = value;
	}

	inline static int32_t get_offset_of_observer_2() { return static_cast<int32_t>(offsetof(U3CInitCoreU3Ec__Iterator0_t1544445406, ___observer_2)); }
	inline RuntimeObject* get_observer_2() const { return ___observer_2; }
	inline RuntimeObject** get_address_of_observer_2() { return &___observer_2; }
	inline void set_observer_2(RuntimeObject* value)
	{
		___observer_2 = value;
		Il2CppCodeGenWriteBarrier((&___observer_2), value);
	}

	inline static int32_t get_offset_of_U3CtextIndexU3E__1_3() { return static_cast<int32_t>(offsetof(U3CInitCoreU3Ec__Iterator0_t1544445406, ___U3CtextIndexU3E__1_3)); }
	inline int32_t get_U3CtextIndexU3E__1_3() const { return ___U3CtextIndexU3E__1_3; }
	inline int32_t* get_address_of_U3CtextIndexU3E__1_3() { return &___U3CtextIndexU3E__1_3; }
	inline void set_U3CtextIndexU3E__1_3(int32_t value)
	{
		___U3CtextIndexU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CInitCoreU3Ec__Iterator0_t1544445406, ___U24this_4)); }
	inline SocketInitializer_t383163007 * get_U24this_4() const { return ___U24this_4; }
	inline SocketInitializer_t383163007 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SocketInitializer_t383163007 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CInitCoreU3Ec__Iterator0_t1544445406, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CInitCoreU3Ec__Iterator0_t1544445406, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CInitCoreU3Ec__Iterator0_t1544445406, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_8() { return static_cast<int32_t>(offsetof(U3CInitCoreU3Ec__Iterator0_t1544445406, ___U24locvar0_8)); }
	inline U3CInitCoreU3Ec__AnonStorey1_t2077288943 * get_U24locvar0_8() const { return ___U24locvar0_8; }
	inline U3CInitCoreU3Ec__AnonStorey1_t2077288943 ** get_address_of_U24locvar0_8() { return &___U24locvar0_8; }
	inline void set_U24locvar0_8(U3CInitCoreU3Ec__AnonStorey1_t2077288943 * value)
	{
		___U24locvar0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINITCOREU3EC__ITERATOR0_T1544445406_H
#ifndef ENGINEPACKETTYPES_T2926055861_H
#define ENGINEPACKETTYPES_T2926055861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.EnginePacketTypes
struct  EnginePacketTypes_t2926055861 
{
public:
	// System.Int32 socket.io.EnginePacketTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EnginePacketTypes_t2926055861, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENGINEPACKETTYPES_T2926055861_H
#ifndef U3CCHILDRENU3EC__ITERATOR4_T3988886754_H
#define U3CCHILDRENU3EC__ITERATOR4_T3988886754_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/<Children>c__Iterator4
struct  U3CChildrenU3Ec__Iterator4_t3988886754  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject> Unity.Linq.GameObjectExtensions/<Children>c__Iterator4::source
	RuntimeObject* ___source_0;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Unity.Linq.GameObjectExtensions/<Children>c__Iterator4::$locvar0
	RuntimeObject* ___U24locvar0_1;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/<Children>c__Iterator4::<item>__1
	GameObject_t1113636619 * ___U3CitemU3E__1_2;
	// Unity.Linq.GameObjectExtensions/ChildrenEnumerable/Enumerator Unity.Linq.GameObjectExtensions/<Children>c__Iterator4::<e>__2
	Enumerator_t792918911  ___U3CeU3E__2_3;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/<Children>c__Iterator4::$current
	GameObject_t1113636619 * ___U24current_4;
	// System.Boolean Unity.Linq.GameObjectExtensions/<Children>c__Iterator4::$disposing
	bool ___U24disposing_5;
	// System.Int32 Unity.Linq.GameObjectExtensions/<Children>c__Iterator4::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CChildrenU3Ec__Iterator4_t3988886754, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CChildrenU3Ec__Iterator4_t3988886754, ___U24locvar0_1)); }
	inline RuntimeObject* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline RuntimeObject** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(RuntimeObject* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U3CitemU3E__1_2() { return static_cast<int32_t>(offsetof(U3CChildrenU3Ec__Iterator4_t3988886754, ___U3CitemU3E__1_2)); }
	inline GameObject_t1113636619 * get_U3CitemU3E__1_2() const { return ___U3CitemU3E__1_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CitemU3E__1_2() { return &___U3CitemU3E__1_2; }
	inline void set_U3CitemU3E__1_2(GameObject_t1113636619 * value)
	{
		___U3CitemU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CitemU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CeU3E__2_3() { return static_cast<int32_t>(offsetof(U3CChildrenU3Ec__Iterator4_t3988886754, ___U3CeU3E__2_3)); }
	inline Enumerator_t792918911  get_U3CeU3E__2_3() const { return ___U3CeU3E__2_3; }
	inline Enumerator_t792918911 * get_address_of_U3CeU3E__2_3() { return &___U3CeU3E__2_3; }
	inline void set_U3CeU3E__2_3(Enumerator_t792918911  value)
	{
		___U3CeU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CChildrenU3Ec__Iterator4_t3988886754, ___U24current_4)); }
	inline GameObject_t1113636619 * get_U24current_4() const { return ___U24current_4; }
	inline GameObject_t1113636619 ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(GameObject_t1113636619 * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CChildrenU3Ec__Iterator4_t3988886754, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CChildrenU3Ec__Iterator4_t3988886754, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHILDRENU3EC__ITERATOR4_T3988886754_H
#ifndef U3CANCESTORSU3EC__ITERATOR0_T258879025_H
#define U3CANCESTORSU3EC__ITERATOR0_T258879025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/<Ancestors>c__Iterator0
struct  U3CAncestorsU3Ec__Iterator0_t258879025  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject> Unity.Linq.GameObjectExtensions/<Ancestors>c__Iterator0::source
	RuntimeObject* ___source_0;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Unity.Linq.GameObjectExtensions/<Ancestors>c__Iterator0::$locvar0
	RuntimeObject* ___U24locvar0_1;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/<Ancestors>c__Iterator0::<item>__1
	GameObject_t1113636619 * ___U3CitemU3E__1_2;
	// Unity.Linq.GameObjectExtensions/AncestorsEnumerable/Enumerator Unity.Linq.GameObjectExtensions/<Ancestors>c__Iterator0::<e>__2
	Enumerator_t3403847843  ___U3CeU3E__2_3;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/<Ancestors>c__Iterator0::$current
	GameObject_t1113636619 * ___U24current_4;
	// System.Boolean Unity.Linq.GameObjectExtensions/<Ancestors>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Unity.Linq.GameObjectExtensions/<Ancestors>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ec__Iterator0_t258879025, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ec__Iterator0_t258879025, ___U24locvar0_1)); }
	inline RuntimeObject* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline RuntimeObject** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(RuntimeObject* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U3CitemU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ec__Iterator0_t258879025, ___U3CitemU3E__1_2)); }
	inline GameObject_t1113636619 * get_U3CitemU3E__1_2() const { return ___U3CitemU3E__1_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CitemU3E__1_2() { return &___U3CitemU3E__1_2; }
	inline void set_U3CitemU3E__1_2(GameObject_t1113636619 * value)
	{
		___U3CitemU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CitemU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CeU3E__2_3() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ec__Iterator0_t258879025, ___U3CeU3E__2_3)); }
	inline Enumerator_t3403847843  get_U3CeU3E__2_3() const { return ___U3CeU3E__2_3; }
	inline Enumerator_t3403847843 * get_address_of_U3CeU3E__2_3() { return &___U3CeU3E__2_3; }
	inline void set_U3CeU3E__2_3(Enumerator_t3403847843  value)
	{
		___U3CeU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ec__Iterator0_t258879025, ___U24current_4)); }
	inline GameObject_t1113636619 * get_U24current_4() const { return ___U24current_4; }
	inline GameObject_t1113636619 ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(GameObject_t1113636619 * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ec__Iterator0_t258879025, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAncestorsU3Ec__Iterator0_t258879025, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANCESTORSU3EC__ITERATOR0_T258879025_H
#ifndef COMPRESSIONMETHOD_T1062973517_H
#define COMPRESSIONMETHOD_T1062973517_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CompressionMethod
struct  CompressionMethod_t1062973517 
{
public:
	// System.Byte WebSocketSharp.CompressionMethod::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CompressionMethod_t1062973517, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPRESSIONMETHOD_T1062973517_H
#ifndef U3CDESCENDANTSU3EC__ITERATOR2_T3999579382_H
#define U3CDESCENDANTSU3EC__ITERATOR2_T3999579382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/<Descendants>c__Iterator2
struct  U3CDescendantsU3Ec__Iterator2_t3999579382  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject> Unity.Linq.GameObjectExtensions/<Descendants>c__Iterator2::source
	RuntimeObject* ___source_0;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Unity.Linq.GameObjectExtensions/<Descendants>c__Iterator2::$locvar0
	RuntimeObject* ___U24locvar0_1;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/<Descendants>c__Iterator2::<item>__1
	GameObject_t1113636619 * ___U3CitemU3E__1_2;
	// System.Func`2<UnityEngine.Transform,System.Boolean> Unity.Linq.GameObjectExtensions/<Descendants>c__Iterator2::descendIntoChildren
	Func_2_t1722577774 * ___descendIntoChildren_3;
	// Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator Unity.Linq.GameObjectExtensions/<Descendants>c__Iterator2::<e>__2
	Enumerator_t1699612215  ___U3CeU3E__2_4;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/<Descendants>c__Iterator2::$current
	GameObject_t1113636619 * ___U24current_5;
	// System.Boolean Unity.Linq.GameObjectExtensions/<Descendants>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 Unity.Linq.GameObjectExtensions/<Descendants>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator2_t3999579382, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator2_t3999579382, ___U24locvar0_1)); }
	inline RuntimeObject* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline RuntimeObject** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(RuntimeObject* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U3CitemU3E__1_2() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator2_t3999579382, ___U3CitemU3E__1_2)); }
	inline GameObject_t1113636619 * get_U3CitemU3E__1_2() const { return ___U3CitemU3E__1_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CitemU3E__1_2() { return &___U3CitemU3E__1_2; }
	inline void set_U3CitemU3E__1_2(GameObject_t1113636619 * value)
	{
		___U3CitemU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CitemU3E__1_2), value);
	}

	inline static int32_t get_offset_of_descendIntoChildren_3() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator2_t3999579382, ___descendIntoChildren_3)); }
	inline Func_2_t1722577774 * get_descendIntoChildren_3() const { return ___descendIntoChildren_3; }
	inline Func_2_t1722577774 ** get_address_of_descendIntoChildren_3() { return &___descendIntoChildren_3; }
	inline void set_descendIntoChildren_3(Func_2_t1722577774 * value)
	{
		___descendIntoChildren_3 = value;
		Il2CppCodeGenWriteBarrier((&___descendIntoChildren_3), value);
	}

	inline static int32_t get_offset_of_U3CeU3E__2_4() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator2_t3999579382, ___U3CeU3E__2_4)); }
	inline Enumerator_t1699612215  get_U3CeU3E__2_4() const { return ___U3CeU3E__2_4; }
	inline Enumerator_t1699612215 * get_address_of_U3CeU3E__2_4() { return &___U3CeU3E__2_4; }
	inline void set_U3CeU3E__2_4(Enumerator_t1699612215  value)
	{
		___U3CeU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator2_t3999579382, ___U24current_5)); }
	inline GameObject_t1113636619 * get_U24current_5() const { return ___U24current_5; }
	inline GameObject_t1113636619 ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(GameObject_t1113636619 * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator2_t3999579382, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator2_t3999579382, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESCENDANTSU3EC__ITERATOR2_T3999579382_H
#ifndef CLOSESTATUSCODE_T3786097442_H
#define CLOSESTATUSCODE_T3786097442_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.CloseStatusCode
struct  CloseStatusCode_t3786097442 
{
public:
	// System.UInt16 WebSocketSharp.CloseStatusCode::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloseStatusCode_t3786097442, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOSESTATUSCODE_T3786097442_H
#ifndef U3CDESCENDANTSANDSELFU3EC__ITERATOR3_T3308521177_H
#define U3CDESCENDANTSANDSELFU3EC__ITERATOR3_T3308521177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/<DescendantsAndSelf>c__Iterator3
struct  U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject> Unity.Linq.GameObjectExtensions/<DescendantsAndSelf>c__Iterator3::source
	RuntimeObject* ___source_0;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Unity.Linq.GameObjectExtensions/<DescendantsAndSelf>c__Iterator3::$locvar0
	RuntimeObject* ___U24locvar0_1;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/<DescendantsAndSelf>c__Iterator3::<item>__1
	GameObject_t1113636619 * ___U3CitemU3E__1_2;
	// System.Func`2<UnityEngine.Transform,System.Boolean> Unity.Linq.GameObjectExtensions/<DescendantsAndSelf>c__Iterator3::descendIntoChildren
	Func_2_t1722577774 * ___descendIntoChildren_3;
	// Unity.Linq.GameObjectExtensions/DescendantsEnumerable/Enumerator Unity.Linq.GameObjectExtensions/<DescendantsAndSelf>c__Iterator3::<e>__2
	Enumerator_t1699612215  ___U3CeU3E__2_4;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/<DescendantsAndSelf>c__Iterator3::$current
	GameObject_t1113636619 * ___U24current_5;
	// System.Boolean Unity.Linq.GameObjectExtensions/<DescendantsAndSelf>c__Iterator3::$disposing
	bool ___U24disposing_6;
	// System.Int32 Unity.Linq.GameObjectExtensions/<DescendantsAndSelf>c__Iterator3::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177, ___U24locvar0_1)); }
	inline RuntimeObject* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline RuntimeObject** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(RuntimeObject* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U3CitemU3E__1_2() { return static_cast<int32_t>(offsetof(U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177, ___U3CitemU3E__1_2)); }
	inline GameObject_t1113636619 * get_U3CitemU3E__1_2() const { return ___U3CitemU3E__1_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CitemU3E__1_2() { return &___U3CitemU3E__1_2; }
	inline void set_U3CitemU3E__1_2(GameObject_t1113636619 * value)
	{
		___U3CitemU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CitemU3E__1_2), value);
	}

	inline static int32_t get_offset_of_descendIntoChildren_3() { return static_cast<int32_t>(offsetof(U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177, ___descendIntoChildren_3)); }
	inline Func_2_t1722577774 * get_descendIntoChildren_3() const { return ___descendIntoChildren_3; }
	inline Func_2_t1722577774 ** get_address_of_descendIntoChildren_3() { return &___descendIntoChildren_3; }
	inline void set_descendIntoChildren_3(Func_2_t1722577774 * value)
	{
		___descendIntoChildren_3 = value;
		Il2CppCodeGenWriteBarrier((&___descendIntoChildren_3), value);
	}

	inline static int32_t get_offset_of_U3CeU3E__2_4() { return static_cast<int32_t>(offsetof(U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177, ___U3CeU3E__2_4)); }
	inline Enumerator_t1699612215  get_U3CeU3E__2_4() const { return ___U3CeU3E__2_4; }
	inline Enumerator_t1699612215 * get_address_of_U3CeU3E__2_4() { return &___U3CeU3E__2_4; }
	inline void set_U3CeU3E__2_4(Enumerator_t1699612215  value)
	{
		___U3CeU3E__2_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177, ___U24current_5)); }
	inline GameObject_t1113636619 * get_U24current_5() const { return ___U24current_5; }
	inline GameObject_t1113636619 ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(GameObject_t1113636619 * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESCENDANTSANDSELFU3EC__ITERATOR3_T3308521177_H
#ifndef U3CANCESTORSANDSELFU3EC__ITERATOR1_T2941390560_H
#define U3CANCESTORSANDSELFU3EC__ITERATOR1_T2941390560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.GameObjectExtensions/<AncestorsAndSelf>c__Iterator1
struct  U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject> Unity.Linq.GameObjectExtensions/<AncestorsAndSelf>c__Iterator1::source
	RuntimeObject* ___source_0;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Unity.Linq.GameObjectExtensions/<AncestorsAndSelf>c__Iterator1::$locvar0
	RuntimeObject* ___U24locvar0_1;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/<AncestorsAndSelf>c__Iterator1::<item>__1
	GameObject_t1113636619 * ___U3CitemU3E__1_2;
	// Unity.Linq.GameObjectExtensions/AncestorsEnumerable/Enumerator Unity.Linq.GameObjectExtensions/<AncestorsAndSelf>c__Iterator1::<e>__2
	Enumerator_t3403847843  ___U3CeU3E__2_3;
	// UnityEngine.GameObject Unity.Linq.GameObjectExtensions/<AncestorsAndSelf>c__Iterator1::$current
	GameObject_t1113636619 * ___U24current_4;
	// System.Boolean Unity.Linq.GameObjectExtensions/<AncestorsAndSelf>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 Unity.Linq.GameObjectExtensions/<AncestorsAndSelf>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560, ___U24locvar0_1)); }
	inline RuntimeObject* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline RuntimeObject** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(RuntimeObject* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U3CitemU3E__1_2() { return static_cast<int32_t>(offsetof(U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560, ___U3CitemU3E__1_2)); }
	inline GameObject_t1113636619 * get_U3CitemU3E__1_2() const { return ___U3CitemU3E__1_2; }
	inline GameObject_t1113636619 ** get_address_of_U3CitemU3E__1_2() { return &___U3CitemU3E__1_2; }
	inline void set_U3CitemU3E__1_2(GameObject_t1113636619 * value)
	{
		___U3CitemU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CitemU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CeU3E__2_3() { return static_cast<int32_t>(offsetof(U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560, ___U3CeU3E__2_3)); }
	inline Enumerator_t3403847843  get_U3CeU3E__2_3() const { return ___U3CeU3E__2_3; }
	inline Enumerator_t3403847843 * get_address_of_U3CeU3E__2_3() { return &___U3CeU3E__2_3; }
	inline void set_U3CeU3E__2_3(Enumerator_t3403847843  value)
	{
		___U3CeU3E__2_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560, ___U24current_4)); }
	inline GameObject_t1113636619 * get_U24current_4() const { return ___U24current_4; }
	inline GameObject_t1113636619 ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(GameObject_t1113636619 * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANCESTORSANDSELFU3EC__ITERATOR1_T2941390560_H
#ifndef BYTEORDER_T2496067899_H
#define BYTEORDER_T2496067899_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.ByteOrder
struct  ByteOrder_t2496067899 
{
public:
	// System.Int32 WebSocketSharp.ByteOrder::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ByteOrder_t2496067899, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEORDER_T2496067899_H
#ifndef BASECARDBOARDDEVICE_T548636460_H
#define BASECARDBOARDDEVICE_T548636460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseCardboardDevice
struct  BaseCardboardDevice_t548636460  : public BaseVRDevice_t2561664142
{
public:
	// System.Single[] BaseCardboardDevice::headData
	SingleU5BU5D_t1444911251* ___headData_25;
	// System.Single[] BaseCardboardDevice::viewData
	SingleU5BU5D_t1444911251* ___viewData_26;
	// System.Single[] BaseCardboardDevice::profileData
	SingleU5BU5D_t1444911251* ___profileData_27;
	// UnityEngine.Matrix4x4 BaseCardboardDevice::headView
	Matrix4x4_t1817901843  ___headView_28;
	// UnityEngine.Matrix4x4 BaseCardboardDevice::leftEyeView
	Matrix4x4_t1817901843  ___leftEyeView_29;
	// UnityEngine.Matrix4x4 BaseCardboardDevice::rightEyeView
	Matrix4x4_t1817901843  ___rightEyeView_30;
	// System.Boolean BaseCardboardDevice::debugDisableNativeProjections
	bool ___debugDisableNativeProjections_31;
	// System.Boolean BaseCardboardDevice::debugDisableNativeUILayer
	bool ___debugDisableNativeUILayer_32;

public:
	inline static int32_t get_offset_of_headData_25() { return static_cast<int32_t>(offsetof(BaseCardboardDevice_t548636460, ___headData_25)); }
	inline SingleU5BU5D_t1444911251* get_headData_25() const { return ___headData_25; }
	inline SingleU5BU5D_t1444911251** get_address_of_headData_25() { return &___headData_25; }
	inline void set_headData_25(SingleU5BU5D_t1444911251* value)
	{
		___headData_25 = value;
		Il2CppCodeGenWriteBarrier((&___headData_25), value);
	}

	inline static int32_t get_offset_of_viewData_26() { return static_cast<int32_t>(offsetof(BaseCardboardDevice_t548636460, ___viewData_26)); }
	inline SingleU5BU5D_t1444911251* get_viewData_26() const { return ___viewData_26; }
	inline SingleU5BU5D_t1444911251** get_address_of_viewData_26() { return &___viewData_26; }
	inline void set_viewData_26(SingleU5BU5D_t1444911251* value)
	{
		___viewData_26 = value;
		Il2CppCodeGenWriteBarrier((&___viewData_26), value);
	}

	inline static int32_t get_offset_of_profileData_27() { return static_cast<int32_t>(offsetof(BaseCardboardDevice_t548636460, ___profileData_27)); }
	inline SingleU5BU5D_t1444911251* get_profileData_27() const { return ___profileData_27; }
	inline SingleU5BU5D_t1444911251** get_address_of_profileData_27() { return &___profileData_27; }
	inline void set_profileData_27(SingleU5BU5D_t1444911251* value)
	{
		___profileData_27 = value;
		Il2CppCodeGenWriteBarrier((&___profileData_27), value);
	}

	inline static int32_t get_offset_of_headView_28() { return static_cast<int32_t>(offsetof(BaseCardboardDevice_t548636460, ___headView_28)); }
	inline Matrix4x4_t1817901843  get_headView_28() const { return ___headView_28; }
	inline Matrix4x4_t1817901843 * get_address_of_headView_28() { return &___headView_28; }
	inline void set_headView_28(Matrix4x4_t1817901843  value)
	{
		___headView_28 = value;
	}

	inline static int32_t get_offset_of_leftEyeView_29() { return static_cast<int32_t>(offsetof(BaseCardboardDevice_t548636460, ___leftEyeView_29)); }
	inline Matrix4x4_t1817901843  get_leftEyeView_29() const { return ___leftEyeView_29; }
	inline Matrix4x4_t1817901843 * get_address_of_leftEyeView_29() { return &___leftEyeView_29; }
	inline void set_leftEyeView_29(Matrix4x4_t1817901843  value)
	{
		___leftEyeView_29 = value;
	}

	inline static int32_t get_offset_of_rightEyeView_30() { return static_cast<int32_t>(offsetof(BaseCardboardDevice_t548636460, ___rightEyeView_30)); }
	inline Matrix4x4_t1817901843  get_rightEyeView_30() const { return ___rightEyeView_30; }
	inline Matrix4x4_t1817901843 * get_address_of_rightEyeView_30() { return &___rightEyeView_30; }
	inline void set_rightEyeView_30(Matrix4x4_t1817901843  value)
	{
		___rightEyeView_30 = value;
	}

	inline static int32_t get_offset_of_debugDisableNativeProjections_31() { return static_cast<int32_t>(offsetof(BaseCardboardDevice_t548636460, ___debugDisableNativeProjections_31)); }
	inline bool get_debugDisableNativeProjections_31() const { return ___debugDisableNativeProjections_31; }
	inline bool* get_address_of_debugDisableNativeProjections_31() { return &___debugDisableNativeProjections_31; }
	inline void set_debugDisableNativeProjections_31(bool value)
	{
		___debugDisableNativeProjections_31 = value;
	}

	inline static int32_t get_offset_of_debugDisableNativeUILayer_32() { return static_cast<int32_t>(offsetof(BaseCardboardDevice_t548636460, ___debugDisableNativeUILayer_32)); }
	inline bool get_debugDisableNativeUILayer_32() const { return ___debugDisableNativeUILayer_32; }
	inline bool* get_address_of_debugDisableNativeUILayer_32() { return &___debugDisableNativeUILayer_32; }
	inline void set_debugDisableNativeUILayer_32(bool value)
	{
		___debugDisableNativeUILayer_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASECARDBOARDDEVICE_T548636460_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_10)); }
	inline TimeSpan_t881159249  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t881159249 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t881159249  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_12)); }
	inline DateTime_t3738529785  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t3738529785  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_13)); }
	inline DateTime_t3738529785  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t3738529785 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t3738529785  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef MUTABLEPOSE3D_T3352419872_H
#define MUTABLEPOSE3D_T3352419872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MutablePose3D
struct  MutablePose3D_t3352419872  : public Pose3D_t2649470188
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MUTABLEPOSE3D_T3352419872_H
#ifndef PACKET_T3733431259_H
#define PACKET_T3733431259_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.Packet
struct  Packet_t3733431259  : public RuntimeObject
{
public:
	// socket.io.EnginePacketTypes socket.io.Packet::enginePktType
	int32_t ___enginePktType_0;
	// socket.io.SocketPacketTypes socket.io.Packet::socketPktType
	int32_t ___socketPktType_1;
	// System.Int32 socket.io.Packet::id
	int32_t ___id_2;
	// System.String socket.io.Packet::nsp
	String_t* ___nsp_3;
	// System.String socket.io.Packet::body
	String_t* ___body_4;

public:
	inline static int32_t get_offset_of_enginePktType_0() { return static_cast<int32_t>(offsetof(Packet_t3733431259, ___enginePktType_0)); }
	inline int32_t get_enginePktType_0() const { return ___enginePktType_0; }
	inline int32_t* get_address_of_enginePktType_0() { return &___enginePktType_0; }
	inline void set_enginePktType_0(int32_t value)
	{
		___enginePktType_0 = value;
	}

	inline static int32_t get_offset_of_socketPktType_1() { return static_cast<int32_t>(offsetof(Packet_t3733431259, ___socketPktType_1)); }
	inline int32_t get_socketPktType_1() const { return ___socketPktType_1; }
	inline int32_t* get_address_of_socketPktType_1() { return &___socketPktType_1; }
	inline void set_socketPktType_1(int32_t value)
	{
		___socketPktType_1 = value;
	}

	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(Packet_t3733431259, ___id_2)); }
	inline int32_t get_id_2() const { return ___id_2; }
	inline int32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(int32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_nsp_3() { return static_cast<int32_t>(offsetof(Packet_t3733431259, ___nsp_3)); }
	inline String_t* get_nsp_3() const { return ___nsp_3; }
	inline String_t** get_address_of_nsp_3() { return &___nsp_3; }
	inline void set_nsp_3(String_t* value)
	{
		___nsp_3 = value;
		Il2CppCodeGenWriteBarrier((&___nsp_3), value);
	}

	inline static int32_t get_offset_of_body_4() { return static_cast<int32_t>(offsetof(Packet_t3733431259, ___body_4)); }
	inline String_t* get_body_4() const { return ___body_4; }
	inline String_t** get_address_of_body_4() { return &___body_4; }
	inline void set_body_4(String_t* value)
	{
		___body_4 = value;
		Il2CppCodeGenWriteBarrier((&___body_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PACKET_T3733431259_H
#ifndef LOGGER_T4025333586_H
#define LOGGER_T4025333586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Logger
struct  Logger_t4025333586  : public RuntimeObject
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Logger::_file
	String_t* ____file_0;
	// WebSocketSharp.LogLevel modreq(System.Runtime.CompilerServices.IsVolatile) WebSocketSharp.Logger::_level
	int32_t ____level_1;
	// System.Action`2<WebSocketSharp.LogData,System.String> WebSocketSharp.Logger::_output
	Action_2_t488357320 * ____output_2;
	// System.Object WebSocketSharp.Logger::_sync
	RuntimeObject * ____sync_3;

public:
	inline static int32_t get_offset_of__file_0() { return static_cast<int32_t>(offsetof(Logger_t4025333586, ____file_0)); }
	inline String_t* get__file_0() const { return ____file_0; }
	inline String_t** get_address_of__file_0() { return &____file_0; }
	inline void set__file_0(String_t* value)
	{
		____file_0 = value;
		Il2CppCodeGenWriteBarrier((&____file_0), value);
	}

	inline static int32_t get_offset_of__level_1() { return static_cast<int32_t>(offsetof(Logger_t4025333586, ____level_1)); }
	inline int32_t get__level_1() const { return ____level_1; }
	inline int32_t* get_address_of__level_1() { return &____level_1; }
	inline void set__level_1(int32_t value)
	{
		____level_1 = value;
	}

	inline static int32_t get_offset_of__output_2() { return static_cast<int32_t>(offsetof(Logger_t4025333586, ____output_2)); }
	inline Action_2_t488357320 * get__output_2() const { return ____output_2; }
	inline Action_2_t488357320 ** get_address_of__output_2() { return &____output_2; }
	inline void set__output_2(Action_2_t488357320 * value)
	{
		____output_2 = value;
		Il2CppCodeGenWriteBarrier((&____output_2), value);
	}

	inline static int32_t get_offset_of__sync_3() { return static_cast<int32_t>(offsetof(Logger_t4025333586, ____sync_3)); }
	inline RuntimeObject * get__sync_3() const { return ____sync_3; }
	inline RuntimeObject ** get_address_of__sync_3() { return &____sync_3; }
	inline void set__sync_3(RuntimeObject * value)
	{
		____sync_3 = value;
		Il2CppCodeGenWriteBarrier((&____sync_3), value);
	}
};

struct Logger_t4025333586_StaticFields
{
public:
	// System.Action`2<WebSocketSharp.LogData,System.String> WebSocketSharp.Logger::<>f__mg$cache0
	Action_2_t488357320 * ___U3CU3Ef__mgU24cache0_4;
	// System.Action`2<WebSocketSharp.LogData,System.String> WebSocketSharp.Logger::<>f__mg$cache1
	Action_2_t488357320 * ___U3CU3Ef__mgU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_4() { return static_cast<int32_t>(offsetof(Logger_t4025333586_StaticFields, ___U3CU3Ef__mgU24cache0_4)); }
	inline Action_2_t488357320 * get_U3CU3Ef__mgU24cache0_4() const { return ___U3CU3Ef__mgU24cache0_4; }
	inline Action_2_t488357320 ** get_address_of_U3CU3Ef__mgU24cache0_4() { return &___U3CU3Ef__mgU24cache0_4; }
	inline void set_U3CU3Ef__mgU24cache0_4(Action_2_t488357320 * value)
	{
		___U3CU3Ef__mgU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_5() { return static_cast<int32_t>(offsetof(Logger_t4025333586_StaticFields, ___U3CU3Ef__mgU24cache1_5)); }
	inline Action_2_t488357320 * get_U3CU3Ef__mgU24cache1_5() const { return ___U3CU3Ef__mgU24cache1_5; }
	inline Action_2_t488357320 ** get_address_of_U3CU3Ef__mgU24cache1_5() { return &___U3CU3Ef__mgU24cache1_5; }
	inline void set_U3CU3Ef__mgU24cache1_5(Action_2_t488357320 * value)
	{
		___U3CU3Ef__mgU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGGER_T4025333586_H
#ifndef MESSAGEEVENTARGS_T2225057723_H
#define MESSAGEEVENTARGS_T2225057723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.MessageEventArgs
struct  MessageEventArgs_t2225057723  : public EventArgs_t3591816995
{
public:
	// System.String WebSocketSharp.MessageEventArgs::_data
	String_t* ____data_1;
	// System.Boolean WebSocketSharp.MessageEventArgs::_dataSet
	bool ____dataSet_2;
	// WebSocketSharp.Opcode WebSocketSharp.MessageEventArgs::_opcode
	uint8_t ____opcode_3;
	// System.Byte[] WebSocketSharp.MessageEventArgs::_rawData
	ByteU5BU5D_t4116647657* ____rawData_4;

public:
	inline static int32_t get_offset_of__data_1() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2225057723, ____data_1)); }
	inline String_t* get__data_1() const { return ____data_1; }
	inline String_t** get_address_of__data_1() { return &____data_1; }
	inline void set__data_1(String_t* value)
	{
		____data_1 = value;
		Il2CppCodeGenWriteBarrier((&____data_1), value);
	}

	inline static int32_t get_offset_of__dataSet_2() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2225057723, ____dataSet_2)); }
	inline bool get__dataSet_2() const { return ____dataSet_2; }
	inline bool* get_address_of__dataSet_2() { return &____dataSet_2; }
	inline void set__dataSet_2(bool value)
	{
		____dataSet_2 = value;
	}

	inline static int32_t get_offset_of__opcode_3() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2225057723, ____opcode_3)); }
	inline uint8_t get__opcode_3() const { return ____opcode_3; }
	inline uint8_t* get_address_of__opcode_3() { return &____opcode_3; }
	inline void set__opcode_3(uint8_t value)
	{
		____opcode_3 = value;
	}

	inline static int32_t get_offset_of__rawData_4() { return static_cast<int32_t>(offsetof(MessageEventArgs_t2225057723, ____rawData_4)); }
	inline ByteU5BU5D_t4116647657* get__rawData_4() const { return ____rawData_4; }
	inline ByteU5BU5D_t4116647657** get_address_of__rawData_4() { return &____rawData_4; }
	inline void set__rawData_4(ByteU5BU5D_t4116647657* value)
	{
		____rawData_4 = value;
		Il2CppCodeGenWriteBarrier((&____rawData_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGEEVENTARGS_T2225057723_H
#ifndef AUTHENTICATIONBASE_T238123992_H
#define AUTHENTICATIONBASE_T238123992_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.AuthenticationBase
struct  AuthenticationBase_t238123992  : public RuntimeObject
{
public:
	// WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Net.AuthenticationBase::_scheme
	int32_t ____scheme_0;
	// System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.AuthenticationBase::Parameters
	NameValueCollection_t407452768 * ___Parameters_1;

public:
	inline static int32_t get_offset_of__scheme_0() { return static_cast<int32_t>(offsetof(AuthenticationBase_t238123992, ____scheme_0)); }
	inline int32_t get__scheme_0() const { return ____scheme_0; }
	inline int32_t* get_address_of__scheme_0() { return &____scheme_0; }
	inline void set__scheme_0(int32_t value)
	{
		____scheme_0 = value;
	}

	inline static int32_t get_offset_of_Parameters_1() { return static_cast<int32_t>(offsetof(AuthenticationBase_t238123992, ___Parameters_1)); }
	inline NameValueCollection_t407452768 * get_Parameters_1() const { return ___Parameters_1; }
	inline NameValueCollection_t407452768 ** get_address_of_Parameters_1() { return &___Parameters_1; }
	inline void set_Parameters_1(NameValueCollection_t407452768 * value)
	{
		___Parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___Parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONBASE_T238123992_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef CARDBOARDIOSDEVICE_T1377430273_H
#define CARDBOARDIOSDEVICE_T1377430273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardboardiOSDevice
struct  CardboardiOSDevice_t1377430273  : public BaseCardboardDevice_t548636460
{
public:
	// System.Boolean CardboardiOSDevice::isOpenGL
	bool ___isOpenGL_34;

public:
	inline static int32_t get_offset_of_isOpenGL_34() { return static_cast<int32_t>(offsetof(CardboardiOSDevice_t1377430273, ___isOpenGL_34)); }
	inline bool get_isOpenGL_34() const { return ___isOpenGL_34; }
	inline bool* get_address_of_isOpenGL_34() { return &___isOpenGL_34; }
	inline void set_isOpenGL_34(bool value)
	{
		___isOpenGL_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARDBOARDIOSDEVICE_T1377430273_H
#ifndef LOGDATA_T2329603299_H
#define LOGDATA_T2329603299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.LogData
struct  LogData_t2329603299  : public RuntimeObject
{
public:
	// System.Diagnostics.StackFrame WebSocketSharp.LogData::_caller
	StackFrame_t3217253059 * ____caller_0;
	// System.DateTime WebSocketSharp.LogData::_date
	DateTime_t3738529785  ____date_1;
	// WebSocketSharp.LogLevel WebSocketSharp.LogData::_level
	int32_t ____level_2;
	// System.String WebSocketSharp.LogData::_message
	String_t* ____message_3;

public:
	inline static int32_t get_offset_of__caller_0() { return static_cast<int32_t>(offsetof(LogData_t2329603299, ____caller_0)); }
	inline StackFrame_t3217253059 * get__caller_0() const { return ____caller_0; }
	inline StackFrame_t3217253059 ** get_address_of__caller_0() { return &____caller_0; }
	inline void set__caller_0(StackFrame_t3217253059 * value)
	{
		____caller_0 = value;
		Il2CppCodeGenWriteBarrier((&____caller_0), value);
	}

	inline static int32_t get_offset_of__date_1() { return static_cast<int32_t>(offsetof(LogData_t2329603299, ____date_1)); }
	inline DateTime_t3738529785  get__date_1() const { return ____date_1; }
	inline DateTime_t3738529785 * get_address_of__date_1() { return &____date_1; }
	inline void set__date_1(DateTime_t3738529785  value)
	{
		____date_1 = value;
	}

	inline static int32_t get_offset_of__level_2() { return static_cast<int32_t>(offsetof(LogData_t2329603299, ____level_2)); }
	inline int32_t get__level_2() const { return ____level_2; }
	inline int32_t* get_address_of__level_2() { return &____level_2; }
	inline void set__level_2(int32_t value)
	{
		____level_2 = value;
	}

	inline static int32_t get_offset_of__message_3() { return static_cast<int32_t>(offsetof(LogData_t2329603299, ____message_3)); }
	inline String_t* get__message_3() const { return ____message_3; }
	inline String_t** get_address_of__message_3() { return &____message_3; }
	inline void set__message_3(String_t* value)
	{
		____message_3 = value;
		Il2CppCodeGenWriteBarrier((&____message_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGDATA_T2329603299_H
#ifndef AUTHENTICATIONCHALLENGE_T3244447305_H
#define AUTHENTICATIONCHALLENGE_T3244447305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.AuthenticationChallenge
struct  AuthenticationChallenge_t3244447305  : public AuthenticationBase_t238123992
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONCHALLENGE_T3244447305_H
#ifndef AUTHENTICATIONRESPONSE_T867023951_H
#define AUTHENTICATIONRESPONSE_T867023951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebSocketSharp.Net.AuthenticationResponse
struct  AuthenticationResponse_t867023951  : public AuthenticationBase_t238123992
{
public:
	// System.UInt32 WebSocketSharp.Net.AuthenticationResponse::_nonceCount
	uint32_t ____nonceCount_2;

public:
	inline static int32_t get_offset_of__nonceCount_2() { return static_cast<int32_t>(offsetof(AuthenticationResponse_t867023951, ____nonceCount_2)); }
	inline uint32_t get__nonceCount_2() const { return ____nonceCount_2; }
	inline uint32_t* get_address_of__nonceCount_2() { return &____nonceCount_2; }
	inline void set__nonceCount_2(uint32_t value)
	{
		____nonceCount_2 = value;
	}
};

struct AuthenticationResponse_t867023951_StaticFields
{
public:
	// System.Func`2<System.String,System.Boolean> WebSocketSharp.Net.AuthenticationResponse::<>f__am$cache0
	Func_2_t2197129486 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(AuthenticationResponse_t867023951_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t2197129486 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t2197129486 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t2197129486 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONRESPONSE_T867023951_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SAMPLESCENESCRIPT_T979009726_H
#define SAMPLESCENESCRIPT_T979009726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Unity.Linq.Sample.SampleSceneScript
struct  SampleSceneScript_t979009726  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject[] Unity.Linq.Sample.SampleSceneScript::array
	GameObjectU5BU5D_t3328599146* ___array_2;

public:
	inline static int32_t get_offset_of_array_2() { return static_cast<int32_t>(offsetof(SampleSceneScript_t979009726, ___array_2)); }
	inline GameObjectU5BU5D_t3328599146* get_array_2() const { return ___array_2; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_array_2() { return &___array_2; }
	inline void set_array_2(GameObjectU5BU5D_t3328599146* value)
	{
		___array_2 = value;
		Il2CppCodeGenWriteBarrier((&___array_2), value);
	}
};

struct SampleSceneScript_t979009726_StaticFields
{
public:
	// System.Func`2<UnityEngine.Transform,System.Boolean> Unity.Linq.Sample.SampleSceneScript::<>f__am$cache0
	Func_2_t1722577774 * ___U3CU3Ef__amU24cache0_3;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> Unity.Linq.Sample.SampleSceneScript::<>f__am$cache1
	Func_2_t4243939292 * ___U3CU3Ef__amU24cache1_4;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> Unity.Linq.Sample.SampleSceneScript::<>f__am$cache2
	Func_2_t4243939292 * ___U3CU3Ef__amU24cache2_5;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> Unity.Linq.Sample.SampleSceneScript::<>f__am$cache3
	Func_2_t4243939292 * ___U3CU3Ef__amU24cache3_6;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> Unity.Linq.Sample.SampleSceneScript::<>f__am$cache4
	Func_2_t4243939292 * ___U3CU3Ef__amU24cache4_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(SampleSceneScript_t979009726_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline Func_2_t1722577774 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline Func_2_t1722577774 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(Func_2_t1722577774 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_4() { return static_cast<int32_t>(offsetof(SampleSceneScript_t979009726_StaticFields, ___U3CU3Ef__amU24cache1_4)); }
	inline Func_2_t4243939292 * get_U3CU3Ef__amU24cache1_4() const { return ___U3CU3Ef__amU24cache1_4; }
	inline Func_2_t4243939292 ** get_address_of_U3CU3Ef__amU24cache1_4() { return &___U3CU3Ef__amU24cache1_4; }
	inline void set_U3CU3Ef__amU24cache1_4(Func_2_t4243939292 * value)
	{
		___U3CU3Ef__amU24cache1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_5() { return static_cast<int32_t>(offsetof(SampleSceneScript_t979009726_StaticFields, ___U3CU3Ef__amU24cache2_5)); }
	inline Func_2_t4243939292 * get_U3CU3Ef__amU24cache2_5() const { return ___U3CU3Ef__amU24cache2_5; }
	inline Func_2_t4243939292 ** get_address_of_U3CU3Ef__amU24cache2_5() { return &___U3CU3Ef__amU24cache2_5; }
	inline void set_U3CU3Ef__amU24cache2_5(Func_2_t4243939292 * value)
	{
		___U3CU3Ef__amU24cache2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_6() { return static_cast<int32_t>(offsetof(SampleSceneScript_t979009726_StaticFields, ___U3CU3Ef__amU24cache3_6)); }
	inline Func_2_t4243939292 * get_U3CU3Ef__amU24cache3_6() const { return ___U3CU3Ef__amU24cache3_6; }
	inline Func_2_t4243939292 ** get_address_of_U3CU3Ef__amU24cache3_6() { return &___U3CU3Ef__amU24cache3_6; }
	inline void set_U3CU3Ef__amU24cache3_6(Func_2_t4243939292 * value)
	{
		___U3CU3Ef__amU24cache3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_7() { return static_cast<int32_t>(offsetof(SampleSceneScript_t979009726_StaticFields, ___U3CU3Ef__amU24cache4_7)); }
	inline Func_2_t4243939292 * get_U3CU3Ef__amU24cache4_7() const { return ___U3CU3Ef__amU24cache4_7; }
	inline Func_2_t4243939292 ** get_address_of_U3CU3Ef__amU24cache4_7() { return &___U3CU3Ef__amU24cache4_7; }
	inline void set_U3CU3Ef__amU24cache4_7(Func_2_t4243939292 * value)
	{
		___U3CU3Ef__amU24cache4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLESCENESCRIPT_T979009726_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef SOCKETINITIALIZER_T383163007_H
#define SOCKETINITIALIZER_T383163007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.SocketInitializer
struct  SocketInitializer_t383163007  : public MonoBehaviour_t3962482529
{
public:
	// socket.io.Socket socket.io.SocketInitializer::<Socket>k__BackingField
	Socket_t590446146 * ___U3CSocketU3Ek__BackingField_2;
	// System.Boolean socket.io.SocketInitializer::<Reconnection>k__BackingField
	bool ___U3CReconnectionU3Ek__BackingField_3;
	// System.Int32 socket.io.SocketInitializer::<ReconnectionAttempts>k__BackingField
	int32_t ___U3CReconnectionAttemptsU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> socket.io.SocketInitializer::_urlQueries
	Dictionary_2_t1632706988 * ____urlQueries_5;

public:
	inline static int32_t get_offset_of_U3CSocketU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SocketInitializer_t383163007, ___U3CSocketU3Ek__BackingField_2)); }
	inline Socket_t590446146 * get_U3CSocketU3Ek__BackingField_2() const { return ___U3CSocketU3Ek__BackingField_2; }
	inline Socket_t590446146 ** get_address_of_U3CSocketU3Ek__BackingField_2() { return &___U3CSocketU3Ek__BackingField_2; }
	inline void set_U3CSocketU3Ek__BackingField_2(Socket_t590446146 * value)
	{
		___U3CSocketU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSocketU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CReconnectionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SocketInitializer_t383163007, ___U3CReconnectionU3Ek__BackingField_3)); }
	inline bool get_U3CReconnectionU3Ek__BackingField_3() const { return ___U3CReconnectionU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CReconnectionU3Ek__BackingField_3() { return &___U3CReconnectionU3Ek__BackingField_3; }
	inline void set_U3CReconnectionU3Ek__BackingField_3(bool value)
	{
		___U3CReconnectionU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionAttemptsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SocketInitializer_t383163007, ___U3CReconnectionAttemptsU3Ek__BackingField_4)); }
	inline int32_t get_U3CReconnectionAttemptsU3Ek__BackingField_4() const { return ___U3CReconnectionAttemptsU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CReconnectionAttemptsU3Ek__BackingField_4() { return &___U3CReconnectionAttemptsU3Ek__BackingField_4; }
	inline void set_U3CReconnectionAttemptsU3Ek__BackingField_4(int32_t value)
	{
		___U3CReconnectionAttemptsU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of__urlQueries_5() { return static_cast<int32_t>(offsetof(SocketInitializer_t383163007, ____urlQueries_5)); }
	inline Dictionary_2_t1632706988 * get__urlQueries_5() const { return ____urlQueries_5; }
	inline Dictionary_2_t1632706988 ** get_address_of__urlQueries_5() { return &____urlQueries_5; }
	inline void set__urlQueries_5(Dictionary_2_t1632706988 * value)
	{
		____urlQueries_5 = value;
		Il2CppCodeGenWriteBarrier((&____urlQueries_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETINITIALIZER_T383163007_H
#ifndef SOCKET_T590446146_H
#define SOCKET_T590446146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.Socket
struct  Socket_t590446146  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 socket.io.Socket::_idGenerator
	int32_t ____idGenerator_2;
	// System.Action socket.io.Socket::<OnConnect>k__BackingField
	Action_t1264377477 * ___U3COnConnectU3Ek__BackingField_3;
	// System.Action socket.io.Socket::<OnConnectTimeOut>k__BackingField
	Action_t1264377477 * ___U3COnConnectTimeOutU3Ek__BackingField_4;
	// System.Action socket.io.Socket::<OnReconnectAttempt>k__BackingField
	Action_t1264377477 * ___U3COnReconnectAttemptU3Ek__BackingField_5;
	// System.Action socket.io.Socket::<OnReconnectFailed>k__BackingField
	Action_t1264377477 * ___U3COnReconnectFailedU3Ek__BackingField_6;
	// System.Action socket.io.Socket::<OnDisconnect>k__BackingField
	Action_t1264377477 * ___U3COnDisconnectU3Ek__BackingField_7;
	// System.Action`1<System.Int32> socket.io.Socket::<OnReconnect>k__BackingField
	Action_1_t3123413348 * ___U3COnReconnectU3Ek__BackingField_8;
	// System.Action`1<System.Int32> socket.io.Socket::<OnReconnecting>k__BackingField
	Action_1_t3123413348 * ___U3COnReconnectingU3Ek__BackingField_9;
	// System.Action`1<System.Exception> socket.io.Socket::<OnConnectError>k__BackingField
	Action_1_t1609204844 * ___U3COnConnectErrorU3Ek__BackingField_10;
	// System.Action`1<System.Exception> socket.io.Socket::<OnReconnectError>k__BackingField
	Action_1_t1609204844 * ___U3COnReconnectErrorU3Ek__BackingField_11;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Action`1<System.String>> socket.io.Socket::_acks
	Dictionary_2_t908631615 * ____acks_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<System.String>> socket.io.Socket::_handlers
	Dictionary_2_t1805174583 * ____handlers_13;
	// System.Uri socket.io.Socket::<Url>k__BackingField
	Uri_t100236324 * ___U3CUrlU3Ek__BackingField_14;
	// socket.io.WebSocketTrigger socket.io.Socket::_webSocketTrigger
	WebSocketTrigger_t496382349 * ____webSocketTrigger_15;

public:
	inline static int32_t get_offset_of__idGenerator_2() { return static_cast<int32_t>(offsetof(Socket_t590446146, ____idGenerator_2)); }
	inline int32_t get__idGenerator_2() const { return ____idGenerator_2; }
	inline int32_t* get_address_of__idGenerator_2() { return &____idGenerator_2; }
	inline void set__idGenerator_2(int32_t value)
	{
		____idGenerator_2 = value;
	}

	inline static int32_t get_offset_of_U3COnConnectU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Socket_t590446146, ___U3COnConnectU3Ek__BackingField_3)); }
	inline Action_t1264377477 * get_U3COnConnectU3Ek__BackingField_3() const { return ___U3COnConnectU3Ek__BackingField_3; }
	inline Action_t1264377477 ** get_address_of_U3COnConnectU3Ek__BackingField_3() { return &___U3COnConnectU3Ek__BackingField_3; }
	inline void set_U3COnConnectU3Ek__BackingField_3(Action_t1264377477 * value)
	{
		___U3COnConnectU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnConnectU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3COnConnectTimeOutU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Socket_t590446146, ___U3COnConnectTimeOutU3Ek__BackingField_4)); }
	inline Action_t1264377477 * get_U3COnConnectTimeOutU3Ek__BackingField_4() const { return ___U3COnConnectTimeOutU3Ek__BackingField_4; }
	inline Action_t1264377477 ** get_address_of_U3COnConnectTimeOutU3Ek__BackingField_4() { return &___U3COnConnectTimeOutU3Ek__BackingField_4; }
	inline void set_U3COnConnectTimeOutU3Ek__BackingField_4(Action_t1264377477 * value)
	{
		___U3COnConnectTimeOutU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnConnectTimeOutU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3COnReconnectAttemptU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Socket_t590446146, ___U3COnReconnectAttemptU3Ek__BackingField_5)); }
	inline Action_t1264377477 * get_U3COnReconnectAttemptU3Ek__BackingField_5() const { return ___U3COnReconnectAttemptU3Ek__BackingField_5; }
	inline Action_t1264377477 ** get_address_of_U3COnReconnectAttemptU3Ek__BackingField_5() { return &___U3COnReconnectAttemptU3Ek__BackingField_5; }
	inline void set_U3COnReconnectAttemptU3Ek__BackingField_5(Action_t1264377477 * value)
	{
		___U3COnReconnectAttemptU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnReconnectAttemptU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3COnReconnectFailedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Socket_t590446146, ___U3COnReconnectFailedU3Ek__BackingField_6)); }
	inline Action_t1264377477 * get_U3COnReconnectFailedU3Ek__BackingField_6() const { return ___U3COnReconnectFailedU3Ek__BackingField_6; }
	inline Action_t1264377477 ** get_address_of_U3COnReconnectFailedU3Ek__BackingField_6() { return &___U3COnReconnectFailedU3Ek__BackingField_6; }
	inline void set_U3COnReconnectFailedU3Ek__BackingField_6(Action_t1264377477 * value)
	{
		___U3COnReconnectFailedU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnReconnectFailedU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3COnDisconnectU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Socket_t590446146, ___U3COnDisconnectU3Ek__BackingField_7)); }
	inline Action_t1264377477 * get_U3COnDisconnectU3Ek__BackingField_7() const { return ___U3COnDisconnectU3Ek__BackingField_7; }
	inline Action_t1264377477 ** get_address_of_U3COnDisconnectU3Ek__BackingField_7() { return &___U3COnDisconnectU3Ek__BackingField_7; }
	inline void set_U3COnDisconnectU3Ek__BackingField_7(Action_t1264377477 * value)
	{
		___U3COnDisconnectU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDisconnectU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3COnReconnectU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Socket_t590446146, ___U3COnReconnectU3Ek__BackingField_8)); }
	inline Action_1_t3123413348 * get_U3COnReconnectU3Ek__BackingField_8() const { return ___U3COnReconnectU3Ek__BackingField_8; }
	inline Action_1_t3123413348 ** get_address_of_U3COnReconnectU3Ek__BackingField_8() { return &___U3COnReconnectU3Ek__BackingField_8; }
	inline void set_U3COnReconnectU3Ek__BackingField_8(Action_1_t3123413348 * value)
	{
		___U3COnReconnectU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnReconnectU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COnReconnectingU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Socket_t590446146, ___U3COnReconnectingU3Ek__BackingField_9)); }
	inline Action_1_t3123413348 * get_U3COnReconnectingU3Ek__BackingField_9() const { return ___U3COnReconnectingU3Ek__BackingField_9; }
	inline Action_1_t3123413348 ** get_address_of_U3COnReconnectingU3Ek__BackingField_9() { return &___U3COnReconnectingU3Ek__BackingField_9; }
	inline void set_U3COnReconnectingU3Ek__BackingField_9(Action_1_t3123413348 * value)
	{
		___U3COnReconnectingU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnReconnectingU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3COnConnectErrorU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Socket_t590446146, ___U3COnConnectErrorU3Ek__BackingField_10)); }
	inline Action_1_t1609204844 * get_U3COnConnectErrorU3Ek__BackingField_10() const { return ___U3COnConnectErrorU3Ek__BackingField_10; }
	inline Action_1_t1609204844 ** get_address_of_U3COnConnectErrorU3Ek__BackingField_10() { return &___U3COnConnectErrorU3Ek__BackingField_10; }
	inline void set_U3COnConnectErrorU3Ek__BackingField_10(Action_1_t1609204844 * value)
	{
		___U3COnConnectErrorU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnConnectErrorU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3COnReconnectErrorU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Socket_t590446146, ___U3COnReconnectErrorU3Ek__BackingField_11)); }
	inline Action_1_t1609204844 * get_U3COnReconnectErrorU3Ek__BackingField_11() const { return ___U3COnReconnectErrorU3Ek__BackingField_11; }
	inline Action_1_t1609204844 ** get_address_of_U3COnReconnectErrorU3Ek__BackingField_11() { return &___U3COnReconnectErrorU3Ek__BackingField_11; }
	inline void set_U3COnReconnectErrorU3Ek__BackingField_11(Action_1_t1609204844 * value)
	{
		___U3COnReconnectErrorU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnReconnectErrorU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of__acks_12() { return static_cast<int32_t>(offsetof(Socket_t590446146, ____acks_12)); }
	inline Dictionary_2_t908631615 * get__acks_12() const { return ____acks_12; }
	inline Dictionary_2_t908631615 ** get_address_of__acks_12() { return &____acks_12; }
	inline void set__acks_12(Dictionary_2_t908631615 * value)
	{
		____acks_12 = value;
		Il2CppCodeGenWriteBarrier((&____acks_12), value);
	}

	inline static int32_t get_offset_of__handlers_13() { return static_cast<int32_t>(offsetof(Socket_t590446146, ____handlers_13)); }
	inline Dictionary_2_t1805174583 * get__handlers_13() const { return ____handlers_13; }
	inline Dictionary_2_t1805174583 ** get_address_of__handlers_13() { return &____handlers_13; }
	inline void set__handlers_13(Dictionary_2_t1805174583 * value)
	{
		____handlers_13 = value;
		Il2CppCodeGenWriteBarrier((&____handlers_13), value);
	}

	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Socket_t590446146, ___U3CUrlU3Ek__BackingField_14)); }
	inline Uri_t100236324 * get_U3CUrlU3Ek__BackingField_14() const { return ___U3CUrlU3Ek__BackingField_14; }
	inline Uri_t100236324 ** get_address_of_U3CUrlU3Ek__BackingField_14() { return &___U3CUrlU3Ek__BackingField_14; }
	inline void set_U3CUrlU3Ek__BackingField_14(Uri_t100236324 * value)
	{
		___U3CUrlU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUrlU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of__webSocketTrigger_15() { return static_cast<int32_t>(offsetof(Socket_t590446146, ____webSocketTrigger_15)); }
	inline WebSocketTrigger_t496382349 * get__webSocketTrigger_15() const { return ____webSocketTrigger_15; }
	inline WebSocketTrigger_t496382349 ** get_address_of__webSocketTrigger_15() { return &____webSocketTrigger_15; }
	inline void set__webSocketTrigger_15(WebSocketTrigger_t496382349 * value)
	{
		____webSocketTrigger_15 = value;
		Il2CppCodeGenWriteBarrier((&____webSocketTrigger_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKET_T590446146_H
#ifndef NAMESPACE_T1335637669_H
#define NAMESPACE_T1335637669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sample.Namespace
struct  Namespace_t1335637669  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Namespace_t1335637669_StaticFields
{
public:
	// System.Action`1<System.String> Sample.Namespace::<>f__am$cache0
	Action_1_t2019918284 * ___U3CU3Ef__amU24cache0_2;
	// System.Action`1<System.String> Sample.Namespace::<>f__am$cache1
	Action_1_t2019918284 * ___U3CU3Ef__amU24cache1_3;
	// System.Action`1<System.String> Sample.Namespace::<>f__am$cache2
	Action_1_t2019918284 * ___U3CU3Ef__amU24cache2_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(Namespace_t1335637669_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_1_t2019918284 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_1_t2019918284 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(Namespace_t1335637669_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_1_t2019918284 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_1_t2019918284 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(Namespace_t1335637669_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Action_1_t2019918284 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Action_1_t2019918284 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Action_1_t2019918284 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACE_T1335637669_H
#ifndef ACKS_T3201466755_H
#define ACKS_T3201466755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sample.Acks
struct  Acks_t3201466755  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACKS_T3201466755_H
#ifndef CONNECT_T3274095400_H
#define CONNECT_T3274095400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sample.Connect
struct  Connect_t3274095400  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Connect_t3274095400_StaticFields
{
public:
	// System.Action Sample.Connect::<>f__am$cache0
	Action_t1264377477 * ___U3CU3Ef__amU24cache0_2;
	// System.Action`1<System.Int32> Sample.Connect::<>f__am$cache1
	Action_1_t3123413348 * ___U3CU3Ef__amU24cache1_3;
	// System.Action Sample.Connect::<>f__am$cache2
	Action_t1264377477 * ___U3CU3Ef__amU24cache2_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(Connect_t3274095400_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(Connect_t3274095400_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_1_t3123413348 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_1_t3123413348 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_1_t3123413348 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(Connect_t3274095400_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Action_t1264377477 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Action_t1264377477 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Action_t1264377477 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONNECT_T3274095400_H
#ifndef EVENTS_T4104173631_H
#define EVENTS_T4104173631_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sample.Events
struct  Events_t4104173631  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTS_T4104173631_H
#ifndef CARDBOARDRETICLE_T3327900497_H
#define CARDBOARDRETICLE_T3327900497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardboardReticle
struct  CardboardReticle_t3327900497  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 CardboardReticle::reticleSegments
	int32_t ___reticleSegments_2;
	// System.Single CardboardReticle::reticleGrowthSpeed
	float ___reticleGrowthSpeed_3;
	// UnityEngine.Material CardboardReticle::materialComp
	Material_t340375123 * ___materialComp_4;
	// UnityEngine.GameObject CardboardReticle::targetObj
	GameObject_t1113636619 * ___targetObj_5;
	// System.Single CardboardReticle::reticleInnerAngle
	float ___reticleInnerAngle_6;
	// System.Single CardboardReticle::reticleOuterAngle
	float ___reticleOuterAngle_7;
	// System.Single CardboardReticle::reticleDistanceInMeters
	float ___reticleDistanceInMeters_8;
	// System.Single CardboardReticle::reticleInnerDiameter
	float ___reticleInnerDiameter_14;
	// System.Single CardboardReticle::reticleOuterDiameter
	float ___reticleOuterDiameter_15;

public:
	inline static int32_t get_offset_of_reticleSegments_2() { return static_cast<int32_t>(offsetof(CardboardReticle_t3327900497, ___reticleSegments_2)); }
	inline int32_t get_reticleSegments_2() const { return ___reticleSegments_2; }
	inline int32_t* get_address_of_reticleSegments_2() { return &___reticleSegments_2; }
	inline void set_reticleSegments_2(int32_t value)
	{
		___reticleSegments_2 = value;
	}

	inline static int32_t get_offset_of_reticleGrowthSpeed_3() { return static_cast<int32_t>(offsetof(CardboardReticle_t3327900497, ___reticleGrowthSpeed_3)); }
	inline float get_reticleGrowthSpeed_3() const { return ___reticleGrowthSpeed_3; }
	inline float* get_address_of_reticleGrowthSpeed_3() { return &___reticleGrowthSpeed_3; }
	inline void set_reticleGrowthSpeed_3(float value)
	{
		___reticleGrowthSpeed_3 = value;
	}

	inline static int32_t get_offset_of_materialComp_4() { return static_cast<int32_t>(offsetof(CardboardReticle_t3327900497, ___materialComp_4)); }
	inline Material_t340375123 * get_materialComp_4() const { return ___materialComp_4; }
	inline Material_t340375123 ** get_address_of_materialComp_4() { return &___materialComp_4; }
	inline void set_materialComp_4(Material_t340375123 * value)
	{
		___materialComp_4 = value;
		Il2CppCodeGenWriteBarrier((&___materialComp_4), value);
	}

	inline static int32_t get_offset_of_targetObj_5() { return static_cast<int32_t>(offsetof(CardboardReticle_t3327900497, ___targetObj_5)); }
	inline GameObject_t1113636619 * get_targetObj_5() const { return ___targetObj_5; }
	inline GameObject_t1113636619 ** get_address_of_targetObj_5() { return &___targetObj_5; }
	inline void set_targetObj_5(GameObject_t1113636619 * value)
	{
		___targetObj_5 = value;
		Il2CppCodeGenWriteBarrier((&___targetObj_5), value);
	}

	inline static int32_t get_offset_of_reticleInnerAngle_6() { return static_cast<int32_t>(offsetof(CardboardReticle_t3327900497, ___reticleInnerAngle_6)); }
	inline float get_reticleInnerAngle_6() const { return ___reticleInnerAngle_6; }
	inline float* get_address_of_reticleInnerAngle_6() { return &___reticleInnerAngle_6; }
	inline void set_reticleInnerAngle_6(float value)
	{
		___reticleInnerAngle_6 = value;
	}

	inline static int32_t get_offset_of_reticleOuterAngle_7() { return static_cast<int32_t>(offsetof(CardboardReticle_t3327900497, ___reticleOuterAngle_7)); }
	inline float get_reticleOuterAngle_7() const { return ___reticleOuterAngle_7; }
	inline float* get_address_of_reticleOuterAngle_7() { return &___reticleOuterAngle_7; }
	inline void set_reticleOuterAngle_7(float value)
	{
		___reticleOuterAngle_7 = value;
	}

	inline static int32_t get_offset_of_reticleDistanceInMeters_8() { return static_cast<int32_t>(offsetof(CardboardReticle_t3327900497, ___reticleDistanceInMeters_8)); }
	inline float get_reticleDistanceInMeters_8() const { return ___reticleDistanceInMeters_8; }
	inline float* get_address_of_reticleDistanceInMeters_8() { return &___reticleDistanceInMeters_8; }
	inline void set_reticleDistanceInMeters_8(float value)
	{
		___reticleDistanceInMeters_8 = value;
	}

	inline static int32_t get_offset_of_reticleInnerDiameter_14() { return static_cast<int32_t>(offsetof(CardboardReticle_t3327900497, ___reticleInnerDiameter_14)); }
	inline float get_reticleInnerDiameter_14() const { return ___reticleInnerDiameter_14; }
	inline float* get_address_of_reticleInnerDiameter_14() { return &___reticleInnerDiameter_14; }
	inline void set_reticleInnerDiameter_14(float value)
	{
		___reticleInnerDiameter_14 = value;
	}

	inline static int32_t get_offset_of_reticleOuterDiameter_15() { return static_cast<int32_t>(offsetof(CardboardReticle_t3327900497, ___reticleOuterDiameter_15)); }
	inline float get_reticleOuterDiameter_15() const { return ___reticleOuterDiameter_15; }
	inline float* get_address_of_reticleOuterDiameter_15() { return &___reticleOuterDiameter_15; }
	inline void set_reticleOuterDiameter_15(float value)
	{
		___reticleOuterDiameter_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARDBOARDRETICLE_T3327900497_H
#ifndef CARDBOARDGAZE_T2557755591_H
#define CARDBOARDGAZE_T2557755591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardboardGaze
struct  CardboardGaze_t2557755591  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CardboardGaze::pointerObject
	GameObject_t1113636619 * ___pointerObject_2;
	// ICardboardGazePointer CardboardGaze::pointer
	RuntimeObject* ___pointer_3;
	// UnityEngine.Camera CardboardGaze::<cam>k__BackingField
	Camera_t4157153871 * ___U3CcamU3Ek__BackingField_4;
	// UnityEngine.LayerMask CardboardGaze::mask
	LayerMask_t3493934918  ___mask_5;
	// ICardboardGazeResponder CardboardGaze::currentTarget
	RuntimeObject* ___currentTarget_6;
	// UnityEngine.GameObject CardboardGaze::currentGazeObject
	GameObject_t1113636619 * ___currentGazeObject_7;
	// UnityEngine.Vector3 CardboardGaze::lastIntersectPosition
	Vector3_t3722313464  ___lastIntersectPosition_8;
	// System.Boolean CardboardGaze::cardboardTrigger
	bool ___cardboardTrigger_9;

public:
	inline static int32_t get_offset_of_pointerObject_2() { return static_cast<int32_t>(offsetof(CardboardGaze_t2557755591, ___pointerObject_2)); }
	inline GameObject_t1113636619 * get_pointerObject_2() const { return ___pointerObject_2; }
	inline GameObject_t1113636619 ** get_address_of_pointerObject_2() { return &___pointerObject_2; }
	inline void set_pointerObject_2(GameObject_t1113636619 * value)
	{
		___pointerObject_2 = value;
		Il2CppCodeGenWriteBarrier((&___pointerObject_2), value);
	}

	inline static int32_t get_offset_of_pointer_3() { return static_cast<int32_t>(offsetof(CardboardGaze_t2557755591, ___pointer_3)); }
	inline RuntimeObject* get_pointer_3() const { return ___pointer_3; }
	inline RuntimeObject** get_address_of_pointer_3() { return &___pointer_3; }
	inline void set_pointer_3(RuntimeObject* value)
	{
		___pointer_3 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_3), value);
	}

	inline static int32_t get_offset_of_U3CcamU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CardboardGaze_t2557755591, ___U3CcamU3Ek__BackingField_4)); }
	inline Camera_t4157153871 * get_U3CcamU3Ek__BackingField_4() const { return ___U3CcamU3Ek__BackingField_4; }
	inline Camera_t4157153871 ** get_address_of_U3CcamU3Ek__BackingField_4() { return &___U3CcamU3Ek__BackingField_4; }
	inline void set_U3CcamU3Ek__BackingField_4(Camera_t4157153871 * value)
	{
		___U3CcamU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcamU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_mask_5() { return static_cast<int32_t>(offsetof(CardboardGaze_t2557755591, ___mask_5)); }
	inline LayerMask_t3493934918  get_mask_5() const { return ___mask_5; }
	inline LayerMask_t3493934918 * get_address_of_mask_5() { return &___mask_5; }
	inline void set_mask_5(LayerMask_t3493934918  value)
	{
		___mask_5 = value;
	}

	inline static int32_t get_offset_of_currentTarget_6() { return static_cast<int32_t>(offsetof(CardboardGaze_t2557755591, ___currentTarget_6)); }
	inline RuntimeObject* get_currentTarget_6() const { return ___currentTarget_6; }
	inline RuntimeObject** get_address_of_currentTarget_6() { return &___currentTarget_6; }
	inline void set_currentTarget_6(RuntimeObject* value)
	{
		___currentTarget_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentTarget_6), value);
	}

	inline static int32_t get_offset_of_currentGazeObject_7() { return static_cast<int32_t>(offsetof(CardboardGaze_t2557755591, ___currentGazeObject_7)); }
	inline GameObject_t1113636619 * get_currentGazeObject_7() const { return ___currentGazeObject_7; }
	inline GameObject_t1113636619 ** get_address_of_currentGazeObject_7() { return &___currentGazeObject_7; }
	inline void set_currentGazeObject_7(GameObject_t1113636619 * value)
	{
		___currentGazeObject_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentGazeObject_7), value);
	}

	inline static int32_t get_offset_of_lastIntersectPosition_8() { return static_cast<int32_t>(offsetof(CardboardGaze_t2557755591, ___lastIntersectPosition_8)); }
	inline Vector3_t3722313464  get_lastIntersectPosition_8() const { return ___lastIntersectPosition_8; }
	inline Vector3_t3722313464 * get_address_of_lastIntersectPosition_8() { return &___lastIntersectPosition_8; }
	inline void set_lastIntersectPosition_8(Vector3_t3722313464  value)
	{
		___lastIntersectPosition_8 = value;
	}

	inline static int32_t get_offset_of_cardboardTrigger_9() { return static_cast<int32_t>(offsetof(CardboardGaze_t2557755591, ___cardboardTrigger_9)); }
	inline bool get_cardboardTrigger_9() const { return ___cardboardTrigger_9; }
	inline bool* get_address_of_cardboardTrigger_9() { return &___cardboardTrigger_9; }
	inline void set_cardboardTrigger_9(bool value)
	{
		___cardboardTrigger_9 = value;
	}
};

struct CardboardGaze_t2557755591_StaticFields
{
public:
	// System.Func`2<UnityEngine.MonoBehaviour,ICardboardGazePointer> CardboardGaze::<>f__am$cache0
	Func_2_t1607421081 * ___U3CU3Ef__amU24cache0_10;
	// System.Func`2<ICardboardGazePointer,System.Boolean> CardboardGaze::<>f__am$cache1
	Func_2_t1292700427 * ___U3CU3Ef__amU24cache1_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_10() { return static_cast<int32_t>(offsetof(CardboardGaze_t2557755591_StaticFields, ___U3CU3Ef__amU24cache0_10)); }
	inline Func_2_t1607421081 * get_U3CU3Ef__amU24cache0_10() const { return ___U3CU3Ef__amU24cache0_10; }
	inline Func_2_t1607421081 ** get_address_of_U3CU3Ef__amU24cache0_10() { return &___U3CU3Ef__amU24cache0_10; }
	inline void set_U3CU3Ef__amU24cache0_10(Func_2_t1607421081 * value)
	{
		___U3CU3Ef__amU24cache0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_11() { return static_cast<int32_t>(offsetof(CardboardGaze_t2557755591_StaticFields, ___U3CU3Ef__amU24cache1_11)); }
	inline Func_2_t1292700427 * get_U3CU3Ef__amU24cache1_11() const { return ___U3CU3Ef__amU24cache1_11; }
	inline Func_2_t1292700427 ** get_address_of_U3CU3Ef__amU24cache1_11() { return &___U3CU3Ef__amU24cache1_11; }
	inline void set_U3CU3Ef__amU24cache1_11(Func_2_t1292700427 * value)
	{
		___U3CU3Ef__amU24cache1_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARDBOARDGAZE_T2557755591_H
#ifndef STEREORENDEREFFECT_T2285492824_H
#define STEREORENDEREFFECT_T2285492824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StereoRenderEffect
struct  StereoRenderEffect_t2285492824  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material StereoRenderEffect::material
	Material_t340375123 * ___material_2;
	// UnityEngine.Camera StereoRenderEffect::cam
	Camera_t4157153871 * ___cam_3;

public:
	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(StereoRenderEffect_t2285492824, ___material_2)); }
	inline Material_t340375123 * get_material_2() const { return ___material_2; }
	inline Material_t340375123 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t340375123 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}

	inline static int32_t get_offset_of_cam_3() { return static_cast<int32_t>(offsetof(StereoRenderEffect_t2285492824, ___cam_3)); }
	inline Camera_t4157153871 * get_cam_3() const { return ___cam_3; }
	inline Camera_t4157153871 ** get_address_of_cam_3() { return &___cam_3; }
	inline void set_cam_3(Camera_t4157153871 * value)
	{
		___cam_3 = value;
		Il2CppCodeGenWriteBarrier((&___cam_3), value);
	}
};

struct StereoRenderEffect_t2285492824_StaticFields
{
public:
	// UnityEngine.Rect StereoRenderEffect::fullRect
	Rect_t2360479859  ___fullRect_4;

public:
	inline static int32_t get_offset_of_fullRect_4() { return static_cast<int32_t>(offsetof(StereoRenderEffect_t2285492824_StaticFields, ___fullRect_4)); }
	inline Rect_t2360479859  get_fullRect_4() const { return ___fullRect_4; }
	inline Rect_t2360479859 * get_address_of_fullRect_4() { return &___fullRect_4; }
	inline void set_fullRect_4(Rect_t2360479859  value)
	{
		___fullRect_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREORENDEREFFECT_T2285492824_H
#ifndef MONOSINGLETON_1_T2737195425_H
#define MONOSINGLETON_1_T2737195425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.MonoSingleton`1<socket.io.SocketManager>
struct  MonoSingleton_1_t2737195425  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct MonoSingleton_1_t2737195425_StaticFields
{
public:
	// T socket.io.MonoSingleton`1::_instance
	SocketManager_t1059501173 * ____instance_2;
	// System.Object socket.io.MonoSingleton`1::_lock
	RuntimeObject * ____lock_3;
	// System.Boolean socket.io.MonoSingleton`1::applicationIsQuitting
	bool ___applicationIsQuitting_4;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t2737195425_StaticFields, ____instance_2)); }
	inline SocketManager_t1059501173 * get__instance_2() const { return ____instance_2; }
	inline SocketManager_t1059501173 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(SocketManager_t1059501173 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}

	inline static int32_t get_offset_of__lock_3() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t2737195425_StaticFields, ____lock_3)); }
	inline RuntimeObject * get__lock_3() const { return ____lock_3; }
	inline RuntimeObject ** get_address_of__lock_3() { return &____lock_3; }
	inline void set__lock_3(RuntimeObject * value)
	{
		____lock_3 = value;
		Il2CppCodeGenWriteBarrier((&____lock_3), value);
	}

	inline static int32_t get_offset_of_applicationIsQuitting_4() { return static_cast<int32_t>(offsetof(MonoSingleton_1_t2737195425_StaticFields, ___applicationIsQuitting_4)); }
	inline bool get_applicationIsQuitting_4() const { return ___applicationIsQuitting_4; }
	inline bool* get_address_of_applicationIsQuitting_4() { return &___applicationIsQuitting_4; }
	inline void set_applicationIsQuitting_4(bool value)
	{
		___applicationIsQuitting_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOSINGLETON_1_T2737195425_H
#ifndef STEREOCONTROLLER_T1722192388_H
#define STEREOCONTROLLER_T1722192388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StereoController
struct  StereoController_t1722192388  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean StereoController::directRender
	bool ___directRender_2;
	// System.Boolean StereoController::keepStereoUpdated
	bool ___keepStereoUpdated_3;
	// System.Single StereoController::stereoMultiplier
	float ___stereoMultiplier_4;
	// System.Single StereoController::matchMonoFOV
	float ___matchMonoFOV_5;
	// System.Single StereoController::matchByZoom
	float ___matchByZoom_6;
	// UnityEngine.Transform StereoController::centerOfInterest
	Transform_t3600365921 * ___centerOfInterest_7;
	// System.Single StereoController::radiusOfInterest
	float ___radiusOfInterest_8;
	// System.Boolean StereoController::checkStereoComfort
	bool ___checkStereoComfort_9;
	// System.Single StereoController::stereoAdjustSmoothing
	float ___stereoAdjustSmoothing_10;
	// System.Single StereoController::screenParallax
	float ___screenParallax_11;
	// System.Single StereoController::stereoPaddingX
	float ___stereoPaddingX_12;
	// System.Single StereoController::stereoPaddingY
	float ___stereoPaddingY_13;
	// System.Boolean StereoController::renderedStereo
	bool ___renderedStereo_14;
	// CardboardEye[] StereoController::eyes
	CardboardEyeU5BU5D_t2353027340* ___eyes_15;
	// CardboardHead StereoController::head
	CardboardHead_t545919794 * ___head_16;
	// UnityEngine.Camera StereoController::<cam>k__BackingField
	Camera_t4157153871 * ___U3CcamU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_directRender_2() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___directRender_2)); }
	inline bool get_directRender_2() const { return ___directRender_2; }
	inline bool* get_address_of_directRender_2() { return &___directRender_2; }
	inline void set_directRender_2(bool value)
	{
		___directRender_2 = value;
	}

	inline static int32_t get_offset_of_keepStereoUpdated_3() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___keepStereoUpdated_3)); }
	inline bool get_keepStereoUpdated_3() const { return ___keepStereoUpdated_3; }
	inline bool* get_address_of_keepStereoUpdated_3() { return &___keepStereoUpdated_3; }
	inline void set_keepStereoUpdated_3(bool value)
	{
		___keepStereoUpdated_3 = value;
	}

	inline static int32_t get_offset_of_stereoMultiplier_4() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___stereoMultiplier_4)); }
	inline float get_stereoMultiplier_4() const { return ___stereoMultiplier_4; }
	inline float* get_address_of_stereoMultiplier_4() { return &___stereoMultiplier_4; }
	inline void set_stereoMultiplier_4(float value)
	{
		___stereoMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_matchMonoFOV_5() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___matchMonoFOV_5)); }
	inline float get_matchMonoFOV_5() const { return ___matchMonoFOV_5; }
	inline float* get_address_of_matchMonoFOV_5() { return &___matchMonoFOV_5; }
	inline void set_matchMonoFOV_5(float value)
	{
		___matchMonoFOV_5 = value;
	}

	inline static int32_t get_offset_of_matchByZoom_6() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___matchByZoom_6)); }
	inline float get_matchByZoom_6() const { return ___matchByZoom_6; }
	inline float* get_address_of_matchByZoom_6() { return &___matchByZoom_6; }
	inline void set_matchByZoom_6(float value)
	{
		___matchByZoom_6 = value;
	}

	inline static int32_t get_offset_of_centerOfInterest_7() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___centerOfInterest_7)); }
	inline Transform_t3600365921 * get_centerOfInterest_7() const { return ___centerOfInterest_7; }
	inline Transform_t3600365921 ** get_address_of_centerOfInterest_7() { return &___centerOfInterest_7; }
	inline void set_centerOfInterest_7(Transform_t3600365921 * value)
	{
		___centerOfInterest_7 = value;
		Il2CppCodeGenWriteBarrier((&___centerOfInterest_7), value);
	}

	inline static int32_t get_offset_of_radiusOfInterest_8() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___radiusOfInterest_8)); }
	inline float get_radiusOfInterest_8() const { return ___radiusOfInterest_8; }
	inline float* get_address_of_radiusOfInterest_8() { return &___radiusOfInterest_8; }
	inline void set_radiusOfInterest_8(float value)
	{
		___radiusOfInterest_8 = value;
	}

	inline static int32_t get_offset_of_checkStereoComfort_9() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___checkStereoComfort_9)); }
	inline bool get_checkStereoComfort_9() const { return ___checkStereoComfort_9; }
	inline bool* get_address_of_checkStereoComfort_9() { return &___checkStereoComfort_9; }
	inline void set_checkStereoComfort_9(bool value)
	{
		___checkStereoComfort_9 = value;
	}

	inline static int32_t get_offset_of_stereoAdjustSmoothing_10() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___stereoAdjustSmoothing_10)); }
	inline float get_stereoAdjustSmoothing_10() const { return ___stereoAdjustSmoothing_10; }
	inline float* get_address_of_stereoAdjustSmoothing_10() { return &___stereoAdjustSmoothing_10; }
	inline void set_stereoAdjustSmoothing_10(float value)
	{
		___stereoAdjustSmoothing_10 = value;
	}

	inline static int32_t get_offset_of_screenParallax_11() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___screenParallax_11)); }
	inline float get_screenParallax_11() const { return ___screenParallax_11; }
	inline float* get_address_of_screenParallax_11() { return &___screenParallax_11; }
	inline void set_screenParallax_11(float value)
	{
		___screenParallax_11 = value;
	}

	inline static int32_t get_offset_of_stereoPaddingX_12() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___stereoPaddingX_12)); }
	inline float get_stereoPaddingX_12() const { return ___stereoPaddingX_12; }
	inline float* get_address_of_stereoPaddingX_12() { return &___stereoPaddingX_12; }
	inline void set_stereoPaddingX_12(float value)
	{
		___stereoPaddingX_12 = value;
	}

	inline static int32_t get_offset_of_stereoPaddingY_13() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___stereoPaddingY_13)); }
	inline float get_stereoPaddingY_13() const { return ___stereoPaddingY_13; }
	inline float* get_address_of_stereoPaddingY_13() { return &___stereoPaddingY_13; }
	inline void set_stereoPaddingY_13(float value)
	{
		___stereoPaddingY_13 = value;
	}

	inline static int32_t get_offset_of_renderedStereo_14() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___renderedStereo_14)); }
	inline bool get_renderedStereo_14() const { return ___renderedStereo_14; }
	inline bool* get_address_of_renderedStereo_14() { return &___renderedStereo_14; }
	inline void set_renderedStereo_14(bool value)
	{
		___renderedStereo_14 = value;
	}

	inline static int32_t get_offset_of_eyes_15() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___eyes_15)); }
	inline CardboardEyeU5BU5D_t2353027340* get_eyes_15() const { return ___eyes_15; }
	inline CardboardEyeU5BU5D_t2353027340** get_address_of_eyes_15() { return &___eyes_15; }
	inline void set_eyes_15(CardboardEyeU5BU5D_t2353027340* value)
	{
		___eyes_15 = value;
		Il2CppCodeGenWriteBarrier((&___eyes_15), value);
	}

	inline static int32_t get_offset_of_head_16() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___head_16)); }
	inline CardboardHead_t545919794 * get_head_16() const { return ___head_16; }
	inline CardboardHead_t545919794 ** get_address_of_head_16() { return &___head_16; }
	inline void set_head_16(CardboardHead_t545919794 * value)
	{
		___head_16 = value;
		Il2CppCodeGenWriteBarrier((&___head_16), value);
	}

	inline static int32_t get_offset_of_U3CcamU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(StereoController_t1722192388, ___U3CcamU3Ek__BackingField_17)); }
	inline Camera_t4157153871 * get_U3CcamU3Ek__BackingField_17() const { return ___U3CcamU3Ek__BackingField_17; }
	inline Camera_t4157153871 ** get_address_of_U3CcamU3Ek__BackingField_17() { return &___U3CcamU3Ek__BackingField_17; }
	inline void set_U3CcamU3Ek__BackingField_17(Camera_t4157153871 * value)
	{
		___U3CcamU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcamU3Ek__BackingField_17), value);
	}
};

struct StereoController_t1722192388_StaticFields
{
public:
	// System.Func`2<CardboardEye,CardboardHead> StereoController::<>f__am$cache0
	Func_2_t3716999315 * ___U3CU3Ef__amU24cache0_18;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_18() { return static_cast<int32_t>(offsetof(StereoController_t1722192388_StaticFields, ___U3CU3Ef__amU24cache0_18)); }
	inline Func_2_t3716999315 * get_U3CU3Ef__amU24cache0_18() const { return ___U3CU3Ef__amU24cache0_18; }
	inline Func_2_t3716999315 ** get_address_of_U3CU3Ef__amU24cache0_18() { return &___U3CU3Ef__amU24cache0_18; }
	inline void set_U3CU3Ef__amU24cache0_18(Func_2_t3716999315 * value)
	{
		___U3CU3Ef__amU24cache0_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOCONTROLLER_T1722192388_H
#ifndef SOCKETMANAGER_T1059501173_H
#define SOCKETMANAGER_T1059501173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// socket.io.SocketManager
struct  SocketManager_t1059501173  : public MonoSingleton_1_t2737195425
{
public:
	// System.Int32 socket.io.SocketManager::<TimeOut>k__BackingField
	int32_t ___U3CTimeOutU3Ek__BackingField_5;
	// System.Boolean socket.io.SocketManager::<Reconnection>k__BackingField
	bool ___U3CReconnectionU3Ek__BackingField_6;
	// System.Int32 socket.io.SocketManager::<ReconnectionAttempts>k__BackingField
	int32_t ___U3CReconnectionAttemptsU3Ek__BackingField_7;
	// System.Int32 socket.io.SocketManager::<ReconnectionDelay>k__BackingField
	int32_t ___U3CReconnectionDelayU3Ek__BackingField_8;
	// socket.io.SocketInitializer socket.io.SocketManager::_socketInit
	SocketInitializer_t383163007 * ____socketInit_9;
	// System.Collections.Generic.List`1<UniRx.Tuple`4<socket.io.Socket,System.Boolean,System.Int32,System.DateTime>> socket.io.SocketManager::_connectRequests
	List_1_t3697878576 * ____connectRequests_10;
	// System.Collections.Generic.Dictionary`2<System.String,socket.io.WebSocketTrigger> socket.io.SocketManager::_webSocketTriggers
	Dictionary_2_t281638648 * ____webSocketTriggers_11;
	// System.IDisposable socket.io.SocketManager::_cancelConnectRequest
	RuntimeObject* ____cancelConnectRequest_12;

public:
	inline static int32_t get_offset_of_U3CTimeOutU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SocketManager_t1059501173, ___U3CTimeOutU3Ek__BackingField_5)); }
	inline int32_t get_U3CTimeOutU3Ek__BackingField_5() const { return ___U3CTimeOutU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CTimeOutU3Ek__BackingField_5() { return &___U3CTimeOutU3Ek__BackingField_5; }
	inline void set_U3CTimeOutU3Ek__BackingField_5(int32_t value)
	{
		___U3CTimeOutU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SocketManager_t1059501173, ___U3CReconnectionU3Ek__BackingField_6)); }
	inline bool get_U3CReconnectionU3Ek__BackingField_6() const { return ___U3CReconnectionU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CReconnectionU3Ek__BackingField_6() { return &___U3CReconnectionU3Ek__BackingField_6; }
	inline void set_U3CReconnectionU3Ek__BackingField_6(bool value)
	{
		___U3CReconnectionU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionAttemptsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(SocketManager_t1059501173, ___U3CReconnectionAttemptsU3Ek__BackingField_7)); }
	inline int32_t get_U3CReconnectionAttemptsU3Ek__BackingField_7() const { return ___U3CReconnectionAttemptsU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CReconnectionAttemptsU3Ek__BackingField_7() { return &___U3CReconnectionAttemptsU3Ek__BackingField_7; }
	inline void set_U3CReconnectionAttemptsU3Ek__BackingField_7(int32_t value)
	{
		___U3CReconnectionAttemptsU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CReconnectionDelayU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(SocketManager_t1059501173, ___U3CReconnectionDelayU3Ek__BackingField_8)); }
	inline int32_t get_U3CReconnectionDelayU3Ek__BackingField_8() const { return ___U3CReconnectionDelayU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CReconnectionDelayU3Ek__BackingField_8() { return &___U3CReconnectionDelayU3Ek__BackingField_8; }
	inline void set_U3CReconnectionDelayU3Ek__BackingField_8(int32_t value)
	{
		___U3CReconnectionDelayU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of__socketInit_9() { return static_cast<int32_t>(offsetof(SocketManager_t1059501173, ____socketInit_9)); }
	inline SocketInitializer_t383163007 * get__socketInit_9() const { return ____socketInit_9; }
	inline SocketInitializer_t383163007 ** get_address_of__socketInit_9() { return &____socketInit_9; }
	inline void set__socketInit_9(SocketInitializer_t383163007 * value)
	{
		____socketInit_9 = value;
		Il2CppCodeGenWriteBarrier((&____socketInit_9), value);
	}

	inline static int32_t get_offset_of__connectRequests_10() { return static_cast<int32_t>(offsetof(SocketManager_t1059501173, ____connectRequests_10)); }
	inline List_1_t3697878576 * get__connectRequests_10() const { return ____connectRequests_10; }
	inline List_1_t3697878576 ** get_address_of__connectRequests_10() { return &____connectRequests_10; }
	inline void set__connectRequests_10(List_1_t3697878576 * value)
	{
		____connectRequests_10 = value;
		Il2CppCodeGenWriteBarrier((&____connectRequests_10), value);
	}

	inline static int32_t get_offset_of__webSocketTriggers_11() { return static_cast<int32_t>(offsetof(SocketManager_t1059501173, ____webSocketTriggers_11)); }
	inline Dictionary_2_t281638648 * get__webSocketTriggers_11() const { return ____webSocketTriggers_11; }
	inline Dictionary_2_t281638648 ** get_address_of__webSocketTriggers_11() { return &____webSocketTriggers_11; }
	inline void set__webSocketTriggers_11(Dictionary_2_t281638648 * value)
	{
		____webSocketTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((&____webSocketTriggers_11), value);
	}

	inline static int32_t get_offset_of__cancelConnectRequest_12() { return static_cast<int32_t>(offsetof(SocketManager_t1059501173, ____cancelConnectRequest_12)); }
	inline RuntimeObject* get__cancelConnectRequest_12() const { return ____cancelConnectRequest_12; }
	inline RuntimeObject** get_address_of__cancelConnectRequest_12() { return &____cancelConnectRequest_12; }
	inline void set__cancelConnectRequest_12(RuntimeObject* value)
	{
		____cancelConnectRequest_12 = value;
		Il2CppCodeGenWriteBarrier((&____cancelConnectRequest_12), value);
	}
};

struct SocketManager_t1059501173_StaticFields
{
public:
	// System.Func`2<UniRx.Tuple`4<socket.io.Socket,System.Boolean,System.Int32,System.DateTime>,System.Boolean> socket.io.SocketManager::<>f__am$cache0
	Func_2_t454313745 * ___U3CU3Ef__amU24cache0_13;
	// System.Predicate`1<UniRx.Tuple`4<socket.io.Socket,System.Boolean,System.Int32,System.DateTime>> socket.io.SocketManager::<>f__am$cache1
	Predicate_1_t3051097958 * ___U3CU3Ef__amU24cache1_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_13() { return static_cast<int32_t>(offsetof(SocketManager_t1059501173_StaticFields, ___U3CU3Ef__amU24cache0_13)); }
	inline Func_2_t454313745 * get_U3CU3Ef__amU24cache0_13() const { return ___U3CU3Ef__amU24cache0_13; }
	inline Func_2_t454313745 ** get_address_of_U3CU3Ef__amU24cache0_13() { return &___U3CU3Ef__amU24cache0_13; }
	inline void set_U3CU3Ef__amU24cache0_13(Func_2_t454313745 * value)
	{
		___U3CU3Ef__amU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_14() { return static_cast<int32_t>(offsetof(SocketManager_t1059501173_StaticFields, ___U3CU3Ef__amU24cache1_14)); }
	inline Predicate_1_t3051097958 * get_U3CU3Ef__amU24cache1_14() const { return ___U3CU3Ef__amU24cache1_14; }
	inline Predicate_1_t3051097958 ** get_address_of_U3CU3Ef__amU24cache1_14() { return &___U3CU3Ef__amU24cache1_14; }
	inline void set_U3CU3Ef__amU24cache1_14(Predicate_1_t3051097958 * value)
	{
		___U3CU3Ef__amU24cache1_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOCKETMANAGER_T1059501173_H
#ifndef BASEINPUTMODULE_T2019268878_H
#define BASEINPUTMODULE_T2019268878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.BaseInputModule
struct  BaseInputModule_t2019268878  : public UIBehaviour_t3495933518
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.BaseInputModule::m_RaycastResultCache
	List_1_t537414295 * ___m_RaycastResultCache_2;
	// UnityEngine.EventSystems.AxisEventData UnityEngine.EventSystems.BaseInputModule::m_AxisEventData
	AxisEventData_t2331243652 * ___m_AxisEventData_3;
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseInputModule::m_EventSystem
	EventSystem_t1003666588 * ___m_EventSystem_4;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.BaseInputModule::m_BaseEventData
	BaseEventData_t3903027533 * ___m_BaseEventData_5;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_InputOverride
	BaseInput_t3630163547 * ___m_InputOverride_6;
	// UnityEngine.EventSystems.BaseInput UnityEngine.EventSystems.BaseInputModule::m_DefaultInput
	BaseInput_t3630163547 * ___m_DefaultInput_7;

public:
	inline static int32_t get_offset_of_m_RaycastResultCache_2() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_RaycastResultCache_2)); }
	inline List_1_t537414295 * get_m_RaycastResultCache_2() const { return ___m_RaycastResultCache_2; }
	inline List_1_t537414295 ** get_address_of_m_RaycastResultCache_2() { return &___m_RaycastResultCache_2; }
	inline void set_m_RaycastResultCache_2(List_1_t537414295 * value)
	{
		___m_RaycastResultCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_RaycastResultCache_2), value);
	}

	inline static int32_t get_offset_of_m_AxisEventData_3() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_AxisEventData_3)); }
	inline AxisEventData_t2331243652 * get_m_AxisEventData_3() const { return ___m_AxisEventData_3; }
	inline AxisEventData_t2331243652 ** get_address_of_m_AxisEventData_3() { return &___m_AxisEventData_3; }
	inline void set_m_AxisEventData_3(AxisEventData_t2331243652 * value)
	{
		___m_AxisEventData_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AxisEventData_3), value);
	}

	inline static int32_t get_offset_of_m_EventSystem_4() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_EventSystem_4)); }
	inline EventSystem_t1003666588 * get_m_EventSystem_4() const { return ___m_EventSystem_4; }
	inline EventSystem_t1003666588 ** get_address_of_m_EventSystem_4() { return &___m_EventSystem_4; }
	inline void set_m_EventSystem_4(EventSystem_t1003666588 * value)
	{
		___m_EventSystem_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventSystem_4), value);
	}

	inline static int32_t get_offset_of_m_BaseEventData_5() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_BaseEventData_5)); }
	inline BaseEventData_t3903027533 * get_m_BaseEventData_5() const { return ___m_BaseEventData_5; }
	inline BaseEventData_t3903027533 ** get_address_of_m_BaseEventData_5() { return &___m_BaseEventData_5; }
	inline void set_m_BaseEventData_5(BaseEventData_t3903027533 * value)
	{
		___m_BaseEventData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseEventData_5), value);
	}

	inline static int32_t get_offset_of_m_InputOverride_6() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_InputOverride_6)); }
	inline BaseInput_t3630163547 * get_m_InputOverride_6() const { return ___m_InputOverride_6; }
	inline BaseInput_t3630163547 ** get_address_of_m_InputOverride_6() { return &___m_InputOverride_6; }
	inline void set_m_InputOverride_6(BaseInput_t3630163547 * value)
	{
		___m_InputOverride_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_InputOverride_6), value);
	}

	inline static int32_t get_offset_of_m_DefaultInput_7() { return static_cast<int32_t>(offsetof(BaseInputModule_t2019268878, ___m_DefaultInput_7)); }
	inline BaseInput_t3630163547 * get_m_DefaultInput_7() const { return ___m_DefaultInput_7; }
	inline BaseInput_t3630163547 ** get_address_of_m_DefaultInput_7() { return &___m_DefaultInput_7; }
	inline void set_m_DefaultInput_7(BaseInput_t3630163547 * value)
	{
		___m_DefaultInput_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_DefaultInput_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEINPUTMODULE_T2019268878_H
#ifndef GAZEINPUTMODULE_T3918405069_H
#define GAZEINPUTMODULE_T3918405069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GazeInputModule
struct  GazeInputModule_t3918405069  : public BaseInputModule_t2019268878
{
public:
	// System.Boolean GazeInputModule::vrModeOnly
	bool ___vrModeOnly_8;
	// System.Single GazeInputModule::clickTime
	float ___clickTime_9;
	// UnityEngine.Vector2 GazeInputModule::hotspot
	Vector2_t2156229523  ___hotspot_10;
	// UnityEngine.EventSystems.PointerEventData GazeInputModule::pointerData
	PointerEventData_t3807901092 * ___pointerData_11;
	// UnityEngine.Vector2 GazeInputModule::lastHeadPose
	Vector2_t2156229523  ___lastHeadPose_12;
	// System.Boolean GazeInputModule::isActive
	bool ___isActive_14;

public:
	inline static int32_t get_offset_of_vrModeOnly_8() { return static_cast<int32_t>(offsetof(GazeInputModule_t3918405069, ___vrModeOnly_8)); }
	inline bool get_vrModeOnly_8() const { return ___vrModeOnly_8; }
	inline bool* get_address_of_vrModeOnly_8() { return &___vrModeOnly_8; }
	inline void set_vrModeOnly_8(bool value)
	{
		___vrModeOnly_8 = value;
	}

	inline static int32_t get_offset_of_clickTime_9() { return static_cast<int32_t>(offsetof(GazeInputModule_t3918405069, ___clickTime_9)); }
	inline float get_clickTime_9() const { return ___clickTime_9; }
	inline float* get_address_of_clickTime_9() { return &___clickTime_9; }
	inline void set_clickTime_9(float value)
	{
		___clickTime_9 = value;
	}

	inline static int32_t get_offset_of_hotspot_10() { return static_cast<int32_t>(offsetof(GazeInputModule_t3918405069, ___hotspot_10)); }
	inline Vector2_t2156229523  get_hotspot_10() const { return ___hotspot_10; }
	inline Vector2_t2156229523 * get_address_of_hotspot_10() { return &___hotspot_10; }
	inline void set_hotspot_10(Vector2_t2156229523  value)
	{
		___hotspot_10 = value;
	}

	inline static int32_t get_offset_of_pointerData_11() { return static_cast<int32_t>(offsetof(GazeInputModule_t3918405069, ___pointerData_11)); }
	inline PointerEventData_t3807901092 * get_pointerData_11() const { return ___pointerData_11; }
	inline PointerEventData_t3807901092 ** get_address_of_pointerData_11() { return &___pointerData_11; }
	inline void set_pointerData_11(PointerEventData_t3807901092 * value)
	{
		___pointerData_11 = value;
		Il2CppCodeGenWriteBarrier((&___pointerData_11), value);
	}

	inline static int32_t get_offset_of_lastHeadPose_12() { return static_cast<int32_t>(offsetof(GazeInputModule_t3918405069, ___lastHeadPose_12)); }
	inline Vector2_t2156229523  get_lastHeadPose_12() const { return ___lastHeadPose_12; }
	inline Vector2_t2156229523 * get_address_of_lastHeadPose_12() { return &___lastHeadPose_12; }
	inline void set_lastHeadPose_12(Vector2_t2156229523  value)
	{
		___lastHeadPose_12 = value;
	}

	inline static int32_t get_offset_of_isActive_14() { return static_cast<int32_t>(offsetof(GazeInputModule_t3918405069, ___isActive_14)); }
	inline bool get_isActive_14() const { return ___isActive_14; }
	inline bool* get_address_of_isActive_14() { return &___isActive_14; }
	inline void set_isActive_14(bool value)
	{
		___isActive_14 = value;
	}
};

struct GazeInputModule_t3918405069_StaticFields
{
public:
	// ICardboardGazePointer GazeInputModule::cardboardPointer
	RuntimeObject* ___cardboardPointer_13;

public:
	inline static int32_t get_offset_of_cardboardPointer_13() { return static_cast<int32_t>(offsetof(GazeInputModule_t3918405069_StaticFields, ___cardboardPointer_13)); }
	inline RuntimeObject* get_cardboardPointer_13() const { return ___cardboardPointer_13; }
	inline RuntimeObject** get_address_of_cardboardPointer_13() { return &___cardboardPointer_13; }
	inline void set_cardboardPointer_13(RuntimeObject* value)
	{
		___cardboardPointer_13 = value;
		Il2CppCodeGenWriteBarrier((&___cardboardPointer_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAZEINPUTMODULE_T3918405069_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (DeviceTypes_t2644824665)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2200[4] = 
{
	DeviceTypes_t2644824665::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (GazeInputModule_t3918405069), -1, sizeof(GazeInputModule_t3918405069_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2201[7] = 
{
	GazeInputModule_t3918405069::get_offset_of_vrModeOnly_8(),
	GazeInputModule_t3918405069::get_offset_of_clickTime_9(),
	GazeInputModule_t3918405069::get_offset_of_hotspot_10(),
	GazeInputModule_t3918405069::get_offset_of_pointerData_11(),
	GazeInputModule_t3918405069::get_offset_of_lastHeadPose_12(),
	GazeInputModule_t3918405069_StaticFields::get_offset_of_cardboardPointer_13(),
	GazeInputModule_t3918405069::get_offset_of_isActive_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (Pose3D_t2649470188), -1, sizeof(Pose3D_t2649470188_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2202[4] = 
{
	Pose3D_t2649470188_StaticFields::get_offset_of_flipZ_0(),
	Pose3D_t2649470188::get_offset_of_U3CPositionU3Ek__BackingField_1(),
	Pose3D_t2649470188::get_offset_of_U3COrientationU3Ek__BackingField_2(),
	Pose3D_t2649470188::get_offset_of_U3CMatrixU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (MutablePose3D_t3352419872), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (StereoController_t1722192388), -1, sizeof(StereoController_t1722192388_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2204[17] = 
{
	StereoController_t1722192388::get_offset_of_directRender_2(),
	StereoController_t1722192388::get_offset_of_keepStereoUpdated_3(),
	StereoController_t1722192388::get_offset_of_stereoMultiplier_4(),
	StereoController_t1722192388::get_offset_of_matchMonoFOV_5(),
	StereoController_t1722192388::get_offset_of_matchByZoom_6(),
	StereoController_t1722192388::get_offset_of_centerOfInterest_7(),
	StereoController_t1722192388::get_offset_of_radiusOfInterest_8(),
	StereoController_t1722192388::get_offset_of_checkStereoComfort_9(),
	StereoController_t1722192388::get_offset_of_stereoAdjustSmoothing_10(),
	StereoController_t1722192388::get_offset_of_screenParallax_11(),
	StereoController_t1722192388::get_offset_of_stereoPaddingX_12(),
	StereoController_t1722192388::get_offset_of_stereoPaddingY_13(),
	StereoController_t1722192388::get_offset_of_renderedStereo_14(),
	StereoController_t1722192388::get_offset_of_eyes_15(),
	StereoController_t1722192388::get_offset_of_head_16(),
	StereoController_t1722192388::get_offset_of_U3CcamU3Ek__BackingField_17(),
	StereoController_t1722192388_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (U3CEndOfFrameU3Ec__Iterator0_t2840111706), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[4] = 
{
	U3CEndOfFrameU3Ec__Iterator0_t2840111706::get_offset_of_U24this_0(),
	U3CEndOfFrameU3Ec__Iterator0_t2840111706::get_offset_of_U24current_1(),
	U3CEndOfFrameU3Ec__Iterator0_t2840111706::get_offset_of_U24disposing_2(),
	U3CEndOfFrameU3Ec__Iterator0_t2840111706::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (StereoRenderEffect_t2285492824), -1, sizeof(StereoRenderEffect_t2285492824_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2206[3] = 
{
	StereoRenderEffect_t2285492824::get_offset_of_material_2(),
	StereoRenderEffect_t2285492824::get_offset_of_cam_3(),
	StereoRenderEffect_t2285492824_StaticFields::get_offset_of_fullRect_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (CardboardGaze_t2557755591), -1, sizeof(CardboardGaze_t2557755591_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2207[10] = 
{
	CardboardGaze_t2557755591::get_offset_of_pointerObject_2(),
	CardboardGaze_t2557755591::get_offset_of_pointer_3(),
	CardboardGaze_t2557755591::get_offset_of_U3CcamU3Ek__BackingField_4(),
	CardboardGaze_t2557755591::get_offset_of_mask_5(),
	CardboardGaze_t2557755591::get_offset_of_currentTarget_6(),
	CardboardGaze_t2557755591::get_offset_of_currentGazeObject_7(),
	CardboardGaze_t2557755591::get_offset_of_lastIntersectPosition_8(),
	CardboardGaze_t2557755591::get_offset_of_cardboardTrigger_9(),
	CardboardGaze_t2557755591_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_10(),
	CardboardGaze_t2557755591_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (CardboardReticle_t3327900497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[14] = 
{
	CardboardReticle_t3327900497::get_offset_of_reticleSegments_2(),
	CardboardReticle_t3327900497::get_offset_of_reticleGrowthSpeed_3(),
	CardboardReticle_t3327900497::get_offset_of_materialComp_4(),
	CardboardReticle_t3327900497::get_offset_of_targetObj_5(),
	CardboardReticle_t3327900497::get_offset_of_reticleInnerAngle_6(),
	CardboardReticle_t3327900497::get_offset_of_reticleOuterAngle_7(),
	CardboardReticle_t3327900497::get_offset_of_reticleDistanceInMeters_8(),
	0,
	0,
	0,
	0,
	0,
	CardboardReticle_t3327900497::get_offset_of_reticleInnerDiameter_14(),
	CardboardReticle_t3327900497::get_offset_of_reticleOuterDiameter_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (BaseCardboardDevice_t548636460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[14] = 
{
	0,
	0,
	0,
	0,
	0,
	BaseCardboardDevice_t548636460::get_offset_of_headData_25(),
	BaseCardboardDevice_t548636460::get_offset_of_viewData_26(),
	BaseCardboardDevice_t548636460::get_offset_of_profileData_27(),
	BaseCardboardDevice_t548636460::get_offset_of_headView_28(),
	BaseCardboardDevice_t548636460::get_offset_of_leftEyeView_29(),
	BaseCardboardDevice_t548636460::get_offset_of_rightEyeView_30(),
	BaseCardboardDevice_t548636460::get_offset_of_debugDisableNativeProjections_31(),
	BaseCardboardDevice_t548636460::get_offset_of_debugDisableNativeUILayer_32(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (BaseVRDevice_t2561664142), -1, sizeof(BaseVRDevice_t2561664142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2212[20] = 
{
	BaseVRDevice_t2561664142_StaticFields::get_offset_of_device_0(),
	BaseVRDevice_t2561664142::get_offset_of_U3CProfileU3Ek__BackingField_1(),
	BaseVRDevice_t2561664142::get_offset_of_headPose_2(),
	BaseVRDevice_t2561664142::get_offset_of_leftEyePose_3(),
	BaseVRDevice_t2561664142::get_offset_of_rightEyePose_4(),
	BaseVRDevice_t2561664142::get_offset_of_leftEyeDistortedProjection_5(),
	BaseVRDevice_t2561664142::get_offset_of_rightEyeDistortedProjection_6(),
	BaseVRDevice_t2561664142::get_offset_of_leftEyeUndistortedProjection_7(),
	BaseVRDevice_t2561664142::get_offset_of_rightEyeUndistortedProjection_8(),
	BaseVRDevice_t2561664142::get_offset_of_leftEyeDistortedViewport_9(),
	BaseVRDevice_t2561664142::get_offset_of_rightEyeDistortedViewport_10(),
	BaseVRDevice_t2561664142::get_offset_of_leftEyeUndistortedViewport_11(),
	BaseVRDevice_t2561664142::get_offset_of_rightEyeUndistortedViewport_12(),
	BaseVRDevice_t2561664142::get_offset_of_recommendedTextureSize_13(),
	BaseVRDevice_t2561664142::get_offset_of_leftEyeOrientation_14(),
	BaseVRDevice_t2561664142::get_offset_of_rightEyeOrientation_15(),
	BaseVRDevice_t2561664142::get_offset_of_triggered_16(),
	BaseVRDevice_t2561664142::get_offset_of_tilted_17(),
	BaseVRDevice_t2561664142::get_offset_of_profileChanged_18(),
	BaseVRDevice_t2561664142::get_offset_of_backButtonPressed_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (CardboardiOSDevice_t1377430273), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[1] = 
{
	CardboardiOSDevice_t1377430273::get_offset_of_isOpenGL_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (Acks_t3201466755), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (U3CStartU3Ec__AnonStorey0_t3247028224), -1, sizeof(U3CStartU3Ec__AnonStorey0_t3247028224_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2215[2] = 
{
	U3CStartU3Ec__AnonStorey0_t3247028224::get_offset_of_socket_0(),
	U3CStartU3Ec__AnonStorey0_t3247028224_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (Connect_t3274095400), -1, sizeof(Connect_t3274095400_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2216[3] = 
{
	Connect_t3274095400_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	Connect_t3274095400_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
	Connect_t3274095400_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (Events_t4104173631), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (U3CStartU3Ec__AnonStorey0_t3538019122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[1] = 
{
	U3CStartU3Ec__AnonStorey0_t3538019122::get_offset_of_socket_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (Namespace_t1335637669), -1, sizeof(Namespace_t1335637669_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2219[3] = 
{
	Namespace_t1335637669_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	Namespace_t1335637669_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
	Namespace_t1335637669_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (U3CStartU3Ec__AnonStorey0_t1296942594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[2] = 
{
	U3CStartU3Ec__AnonStorey0_t1296942594::get_offset_of_news_0(),
	U3CStartU3Ec__AnonStorey0_t1296942594::get_offset_of_chat_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (SampleSceneScript_t979009726), -1, sizeof(SampleSceneScript_t979009726_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2221[6] = 
{
	SampleSceneScript_t979009726::get_offset_of_array_2(),
	SampleSceneScript_t979009726_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
	SampleSceneScript_t979009726_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_4(),
	SampleSceneScript_t979009726_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_5(),
	SampleSceneScript_t979009726_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_6(),
	SampleSceneScript_t979009726_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (GameObjectExtensions_t3873029940), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (ChildrenEnumerable_t145725860)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[2] = 
{
	ChildrenEnumerable_t145725860::get_offset_of_origin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChildrenEnumerable_t145725860::get_offset_of_withSelf_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (Enumerator_t792918911)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[6] = 
{
	Enumerator_t792918911::get_offset_of_childCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t792918911::get_offset_of_originTransform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t792918911::get_offset_of_canRun_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t792918911::get_offset_of_withSelf_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t792918911::get_offset_of_currentIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t792918911::get_offset_of_current_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (AncestorsEnumerable_t953073017)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[2] = 
{
	AncestorsEnumerable_t953073017::get_offset_of_origin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AncestorsEnumerable_t953073017::get_offset_of_withSelf_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (Enumerator_t3403847843)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[4] = 
{
	Enumerator_t3403847843::get_offset_of_canRun_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t3403847843::get_offset_of_current_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t3403847843::get_offset_of_currentTransform_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t3403847843::get_offset_of_withSelf_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (DescendantsEnumerable_t4167108050)+ sizeof (RuntimeObject), -1, sizeof(DescendantsEnumerable_t4167108050_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2231[4] = 
{
	DescendantsEnumerable_t4167108050_StaticFields::get_offset_of_alwaysTrue_0(),
	DescendantsEnumerable_t4167108050::get_offset_of_origin_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DescendantsEnumerable_t4167108050::get_offset_of_withSelf_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	DescendantsEnumerable_t4167108050::get_offset_of_descendIntoChildren_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (InternalUnsafeRefStack_t2529159201), -1, sizeof(InternalUnsafeRefStack_t2529159201_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2232[3] = 
{
	InternalUnsafeRefStack_t2529159201_StaticFields::get_offset_of_RefStackPool_0(),
	InternalUnsafeRefStack_t2529159201::get_offset_of_size_1(),
	InternalUnsafeRefStack_t2529159201::get_offset_of_array_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (Enumerator_t1699612215)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[8] = 
{
	Enumerator_t1699612215::get_offset_of_childCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t1699612215::get_offset_of_originTransform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t1699612215::get_offset_of_canRun_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t1699612215::get_offset_of_withSelf_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t1699612215::get_offset_of_currentIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t1699612215::get_offset_of_current_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t1699612215::get_offset_of_sharedStack_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t1699612215::get_offset_of_descendIntoChildren_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (BeforeSelfEnumerable_t2845913035)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[2] = 
{
	BeforeSelfEnumerable_t2845913035::get_offset_of_origin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	BeforeSelfEnumerable_t2845913035::get_offset_of_withSelf_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (Enumerator_t4036937675)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2237[7] = 
{
	Enumerator_t4036937675::get_offset_of_childCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t4036937675::get_offset_of_originTransform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t4036937675::get_offset_of_canRun_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t4036937675::get_offset_of_withSelf_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t4036937675::get_offset_of_currentIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t4036937675::get_offset_of_current_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t4036937675::get_offset_of_parent_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2239[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (AfterSelfEnumerable_t4038755258)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[2] = 
{
	AfterSelfEnumerable_t4038755258::get_offset_of_origin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AfterSelfEnumerable_t4038755258::get_offset_of_withSelf_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (Enumerator_t20295438)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[7] = 
{
	Enumerator_t20295438::get_offset_of_childCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t20295438::get_offset_of_originTransform_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t20295438::get_offset_of_canRun_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t20295438::get_offset_of_withSelf_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t20295438::get_offset_of_currentIndex_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t20295438::get_offset_of_current_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Enumerator_t20295438::get_offset_of_parent_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2243[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (U3CAncestorsU3Ec__Iterator0_t258879025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[7] = 
{
	U3CAncestorsU3Ec__Iterator0_t258879025::get_offset_of_source_0(),
	U3CAncestorsU3Ec__Iterator0_t258879025::get_offset_of_U24locvar0_1(),
	U3CAncestorsU3Ec__Iterator0_t258879025::get_offset_of_U3CitemU3E__1_2(),
	U3CAncestorsU3Ec__Iterator0_t258879025::get_offset_of_U3CeU3E__2_3(),
	U3CAncestorsU3Ec__Iterator0_t258879025::get_offset_of_U24current_4(),
	U3CAncestorsU3Ec__Iterator0_t258879025::get_offset_of_U24disposing_5(),
	U3CAncestorsU3Ec__Iterator0_t258879025::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[7] = 
{
	U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560::get_offset_of_source_0(),
	U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560::get_offset_of_U24locvar0_1(),
	U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560::get_offset_of_U3CitemU3E__1_2(),
	U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560::get_offset_of_U3CeU3E__2_3(),
	U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560::get_offset_of_U24current_4(),
	U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560::get_offset_of_U24disposing_5(),
	U3CAncestorsAndSelfU3Ec__Iterator1_t2941390560::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (U3CDescendantsU3Ec__Iterator2_t3999579382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[8] = 
{
	U3CDescendantsU3Ec__Iterator2_t3999579382::get_offset_of_source_0(),
	U3CDescendantsU3Ec__Iterator2_t3999579382::get_offset_of_U24locvar0_1(),
	U3CDescendantsU3Ec__Iterator2_t3999579382::get_offset_of_U3CitemU3E__1_2(),
	U3CDescendantsU3Ec__Iterator2_t3999579382::get_offset_of_descendIntoChildren_3(),
	U3CDescendantsU3Ec__Iterator2_t3999579382::get_offset_of_U3CeU3E__2_4(),
	U3CDescendantsU3Ec__Iterator2_t3999579382::get_offset_of_U24current_5(),
	U3CDescendantsU3Ec__Iterator2_t3999579382::get_offset_of_U24disposing_6(),
	U3CDescendantsU3Ec__Iterator2_t3999579382::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2247[8] = 
{
	U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177::get_offset_of_source_0(),
	U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177::get_offset_of_U24locvar0_1(),
	U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177::get_offset_of_U3CitemU3E__1_2(),
	U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177::get_offset_of_descendIntoChildren_3(),
	U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177::get_offset_of_U3CeU3E__2_4(),
	U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177::get_offset_of_U24current_5(),
	U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177::get_offset_of_U24disposing_6(),
	U3CDescendantsAndSelfU3Ec__Iterator3_t3308521177::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (U3CChildrenU3Ec__Iterator4_t3988886754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2248[7] = 
{
	U3CChildrenU3Ec__Iterator4_t3988886754::get_offset_of_source_0(),
	U3CChildrenU3Ec__Iterator4_t3988886754::get_offset_of_U24locvar0_1(),
	U3CChildrenU3Ec__Iterator4_t3988886754::get_offset_of_U3CitemU3E__1_2(),
	U3CChildrenU3Ec__Iterator4_t3988886754::get_offset_of_U3CeU3E__2_3(),
	U3CChildrenU3Ec__Iterator4_t3988886754::get_offset_of_U24current_4(),
	U3CChildrenU3Ec__Iterator4_t3988886754::get_offset_of_U24disposing_5(),
	U3CChildrenU3Ec__Iterator4_t3988886754::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (U3CChildrenAndSelfU3Ec__Iterator5_t3958859349), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[7] = 
{
	U3CChildrenAndSelfU3Ec__Iterator5_t3958859349::get_offset_of_source_0(),
	U3CChildrenAndSelfU3Ec__Iterator5_t3958859349::get_offset_of_U24locvar0_1(),
	U3CChildrenAndSelfU3Ec__Iterator5_t3958859349::get_offset_of_U3CitemU3E__1_2(),
	U3CChildrenAndSelfU3Ec__Iterator5_t3958859349::get_offset_of_U3CeU3E__2_3(),
	U3CChildrenAndSelfU3Ec__Iterator5_t3958859349::get_offset_of_U24current_4(),
	U3CChildrenAndSelfU3Ec__Iterator5_t3958859349::get_offset_of_U24disposing_5(),
	U3CChildrenAndSelfU3Ec__Iterator5_t3958859349::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (TransformCloneType_t3013038762)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2251[5] = 
{
	TransformCloneType_t3013038762::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (TransformMoveType_t1435850903)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2252[4] = 
{
	TransformMoveType_t1435850903::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (Decoder_t3794782951), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (Encoder_t3403433762), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (EnginePacketTypes_t2926055861)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2255[9] = 
{
	EnginePacketTypes_t2926055861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (SocketPacketTypes_t3225958894)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2256[10] = 
{
	SocketPacketTypes_t3225958894::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (Packet_t3733431259), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2257[5] = 
{
	Packet_t3733431259::get_offset_of_enginePktType_0(),
	Packet_t3733431259::get_offset_of_socketPktType_1(),
	Packet_t3733431259::get_offset_of_id_2(),
	Packet_t3733431259::get_offset_of_nsp_3(),
	Packet_t3733431259::get_offset_of_body_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (Socket_t590446146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[14] = 
{
	Socket_t590446146::get_offset_of__idGenerator_2(),
	Socket_t590446146::get_offset_of_U3COnConnectU3Ek__BackingField_3(),
	Socket_t590446146::get_offset_of_U3COnConnectTimeOutU3Ek__BackingField_4(),
	Socket_t590446146::get_offset_of_U3COnReconnectAttemptU3Ek__BackingField_5(),
	Socket_t590446146::get_offset_of_U3COnReconnectFailedU3Ek__BackingField_6(),
	Socket_t590446146::get_offset_of_U3COnDisconnectU3Ek__BackingField_7(),
	Socket_t590446146::get_offset_of_U3COnReconnectU3Ek__BackingField_8(),
	Socket_t590446146::get_offset_of_U3COnReconnectingU3Ek__BackingField_9(),
	Socket_t590446146::get_offset_of_U3COnConnectErrorU3Ek__BackingField_10(),
	Socket_t590446146::get_offset_of_U3COnReconnectErrorU3Ek__BackingField_11(),
	Socket_t590446146::get_offset_of__acks_12(),
	Socket_t590446146::get_offset_of__handlers_13(),
	Socket_t590446146::get_offset_of_U3CUrlU3Ek__BackingField_14(),
	Socket_t590446146::get_offset_of__webSocketTrigger_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (SocketInitializer_t383163007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2260[4] = 
{
	SocketInitializer_t383163007::get_offset_of_U3CSocketU3Ek__BackingField_2(),
	SocketInitializer_t383163007::get_offset_of_U3CReconnectionU3Ek__BackingField_3(),
	SocketInitializer_t383163007::get_offset_of_U3CReconnectionAttemptsU3Ek__BackingField_4(),
	SocketInitializer_t383163007::get_offset_of__urlQueries_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (PollingUrlAnswer_t1140886577)+ sizeof (RuntimeObject), sizeof(PollingUrlAnswer_t1140886577_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2261[3] = 
{
	PollingUrlAnswer_t1140886577::get_offset_of_sid_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PollingUrlAnswer_t1140886577::get_offset_of_pingInterval_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PollingUrlAnswer_t1140886577::get_offset_of_pingTimeout_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (U3CInitCoreU3Ec__Iterator0_t1544445406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[9] = 
{
	U3CInitCoreU3Ec__Iterator0_t1544445406::get_offset_of_U3CwwwU3E__1_0(),
	U3CInitCoreU3Ec__Iterator0_t1544445406::get_offset_of_cancelToken_1(),
	U3CInitCoreU3Ec__Iterator0_t1544445406::get_offset_of_observer_2(),
	U3CInitCoreU3Ec__Iterator0_t1544445406::get_offset_of_U3CtextIndexU3E__1_3(),
	U3CInitCoreU3Ec__Iterator0_t1544445406::get_offset_of_U24this_4(),
	U3CInitCoreU3Ec__Iterator0_t1544445406::get_offset_of_U24current_5(),
	U3CInitCoreU3Ec__Iterator0_t1544445406::get_offset_of_U24disposing_6(),
	U3CInitCoreU3Ec__Iterator0_t1544445406::get_offset_of_U24PC_7(),
	U3CInitCoreU3Ec__Iterator0_t1544445406::get_offset_of_U24locvar0_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (U3CInitCoreU3Ec__AnonStorey1_t2077288943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[3] = 
{
	U3CInitCoreU3Ec__AnonStorey1_t2077288943::get_offset_of_webSocketTrigger_0(),
	U3CInitCoreU3Ec__AnonStorey1_t2077288943::get_offset_of_capturedSocket_1(),
	U3CInitCoreU3Ec__AnonStorey1_t2077288943::get_offset_of_U3CU3Ef__refU240_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (SocketManager_t1059501173), -1, sizeof(SocketManager_t1059501173_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2264[10] = 
{
	SocketManager_t1059501173::get_offset_of_U3CTimeOutU3Ek__BackingField_5(),
	SocketManager_t1059501173::get_offset_of_U3CReconnectionU3Ek__BackingField_6(),
	SocketManager_t1059501173::get_offset_of_U3CReconnectionAttemptsU3Ek__BackingField_7(),
	SocketManager_t1059501173::get_offset_of_U3CReconnectionDelayU3Ek__BackingField_8(),
	SocketManager_t1059501173::get_offset_of__socketInit_9(),
	SocketManager_t1059501173::get_offset_of__connectRequests_10(),
	SocketManager_t1059501173::get_offset_of__webSocketTriggers_11(),
	SocketManager_t1059501173::get_offset_of__cancelConnectRequest_12(),
	SocketManager_t1059501173_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
	SocketManager_t1059501173_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (U3CReconnectU3Ec__AnonStorey0_t2057279870), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2265[1] = 
{
	U3CReconnectU3Ec__AnonStorey0_t2057279870::get_offset_of_socket_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (SystemEvents_t3087650121)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2266[11] = 
{
	SystemEvents_t3087650121::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (SystemEventHelper_t107868270), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (TimeStamp_t1761014137), -1, sizeof(TimeStamp_t1761014137_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2268[1] = 
{
	TimeStamp_t1761014137_StaticFields::get_offset_of__encodeAlphabets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (ByteOrder_t2496067899)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2269[3] = 
{
	ByteOrder_t2496067899::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (CloseEventArgs_t1876714021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[4] = 
{
	CloseEventArgs_t1876714021::get_offset_of__clean_1(),
	CloseEventArgs_t1876714021::get_offset_of__code_2(),
	CloseEventArgs_t1876714021::get_offset_of__payloadData_3(),
	CloseEventArgs_t1876714021::get_offset_of__reason_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (CloseStatusCode_t3786097442)+ sizeof (RuntimeObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2271[14] = 
{
	CloseStatusCode_t3786097442::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (CompressionMethod_t1062973517)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2272[3] = 
{
	CompressionMethod_t1062973517::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (ErrorEventArgs_t3420679011), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[2] = 
{
	ErrorEventArgs_t3420679011::get_offset_of__exception_1(),
	ErrorEventArgs_t3420679011::get_offset_of__message_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (Ext_t2749166018), -1, sizeof(Ext_t2749166018_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2274[4] = 
{
	Ext_t2749166018_StaticFields::get_offset_of__last_0(),
	Ext_t2749166018_StaticFields::get_offset_of__retry_1(),
	0,
	Ext_t2749166018_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (U3CContainsTwiceU3Ec__AnonStorey1_t3397086364), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[3] = 
{
	U3CContainsTwiceU3Ec__AnonStorey1_t3397086364::get_offset_of_len_0(),
	U3CContainsTwiceU3Ec__AnonStorey1_t3397086364::get_offset_of_values_1(),
	U3CContainsTwiceU3Ec__AnonStorey1_t3397086364::get_offset_of_contains_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[7] = 
{
	U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496::get_offset_of_source_0(),
	U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496::get_offset_of_completed_1(),
	U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496::get_offset_of_destination_2(),
	U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496::get_offset_of_buff_3(),
	U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496::get_offset_of_bufferLength_4(),
	U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496::get_offset_of_callback_5(),
	U3CCopyToAsyncU3Ec__AnonStorey2_t3467411496::get_offset_of_error_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[8] = 
{
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741::get_offset_of_stream_0(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741::get_offset_of_retry_1(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741::get_offset_of_buff_2(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741::get_offset_of_offset_3(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741::get_offset_of_length_4(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741::get_offset_of_callback_5(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741::get_offset_of_completed_6(),
	U3CReadBytesAsyncU3Ec__AnonStorey3_t1780370741::get_offset_of_error_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[8] = 
{
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748::get_offset_of_bufferLength_0(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748::get_offset_of_stream_1(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748::get_offset_of_buff_2(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748::get_offset_of_dest_3(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748::get_offset_of_retry_4(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748::get_offset_of_read_5(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748::get_offset_of_completed_6(),
	U3CReadBytesAsyncU3Ec__AnonStorey4_t1780370748::get_offset_of_error_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (U3CReadBytesAsyncU3Ec__AnonStorey5_t2704290800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[2] = 
{
	U3CReadBytesAsyncU3Ec__AnonStorey5_t2704290800::get_offset_of_len_0(),
	U3CReadBytesAsyncU3Ec__AnonStorey5_t2704290800::get_offset_of_U3CU3Ef__refU244_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (U3CSplitHeaderValueU3Ec__Iterator0_t2991988224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[12] = 
{
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_value_0(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3ClenU3E__0_1(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_separators_2(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CsepsU3E__0_3(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CbuffU3E__0_4(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CescapedU3E__0_5(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CquotedU3E__0_6(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CiU3E__1_7(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U3CcU3E__2_8(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U24current_9(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U24disposing_10(),
	U3CSplitHeaderValueU3Ec__Iterator0_t2991988224::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (U3CWriteBytesAsyncU3Ec__AnonStorey6_t169123234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[3] = 
{
	U3CWriteBytesAsyncU3Ec__AnonStorey6_t169123234::get_offset_of_completed_0(),
	U3CWriteBytesAsyncU3Ec__AnonStorey6_t169123234::get_offset_of_input_1(),
	U3CWriteBytesAsyncU3Ec__AnonStorey6_t169123234::get_offset_of_error_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2282[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (Fin_t411169233)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2283[3] = 
{
	Fin_t411169233::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (HttpBase_t3529262337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[5] = 
{
	HttpBase_t3529262337::get_offset_of__headers_0(),
	0,
	HttpBase_t3529262337::get_offset_of__version_2(),
	HttpBase_t3529262337::get_offset_of_EntityBodyData_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (U3CreadHeadersU3Ec__AnonStorey0_t2304224024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[2] = 
{
	U3CreadHeadersU3Ec__AnonStorey0_t2304224024::get_offset_of_buff_0(),
	U3CreadHeadersU3Ec__AnonStorey0_t2304224024::get_offset_of_cnt_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (HttpRequest_t1778308387), -1, sizeof(HttpRequest_t1778308387_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2287[6] = 
{
	HttpRequest_t1778308387::get_offset_of__method_5(),
	HttpRequest_t1778308387::get_offset_of__uri_6(),
	HttpRequest_t1778308387::get_offset_of__websocketRequest_7(),
	HttpRequest_t1778308387::get_offset_of__websocketRequestSet_8(),
	HttpRequest_t1778308387_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_9(),
	HttpRequest_t1778308387_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (HttpResponse_t1646431521), -1, sizeof(HttpResponse_t1646431521_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2288[3] = 
{
	HttpResponse_t1646431521::get_offset_of__code_5(),
	HttpResponse_t1646431521::get_offset_of__reason_6(),
	HttpResponse_t1646431521_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (LogData_t2329603299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2289[4] = 
{
	LogData_t2329603299::get_offset_of__caller_0(),
	LogData_t2329603299::get_offset_of__date_1(),
	LogData_t2329603299::get_offset_of__level_2(),
	LogData_t2329603299::get_offset_of__message_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (Logger_t4025333586), -1, sizeof(Logger_t4025333586_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2290[6] = 
{
	Logger_t4025333586::get_offset_of__file_0(),
	Logger_t4025333586::get_offset_of__level_1(),
	Logger_t4025333586::get_offset_of__output_2(),
	Logger_t4025333586::get_offset_of__sync_3(),
	Logger_t4025333586_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
	Logger_t4025333586_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (LogLevel_t2581836550)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2291[7] = 
{
	LogLevel_t2581836550::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (Mask_t3471462035)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2292[3] = 
{
	Mask_t3471462035::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (MessageEventArgs_t2225057723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2293[4] = 
{
	MessageEventArgs_t2225057723::get_offset_of__data_1(),
	MessageEventArgs_t2225057723::get_offset_of__dataSet_2(),
	MessageEventArgs_t2225057723::get_offset_of__opcode_3(),
	MessageEventArgs_t2225057723::get_offset_of__rawData_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (AuthenticationBase_t238123992), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2294[2] = 
{
	AuthenticationBase_t238123992::get_offset_of__scheme_0(),
	AuthenticationBase_t238123992::get_offset_of_Parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (AuthenticationChallenge_t3244447305), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (AuthenticationResponse_t867023951), -1, sizeof(AuthenticationResponse_t867023951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2296[2] = 
{
	AuthenticationResponse_t867023951::get_offset_of__nonceCount_2(),
	AuthenticationResponse_t867023951_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (AuthenticationSchemes_t3195200892)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2297[5] = 
{
	AuthenticationSchemes_t3195200892::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (Chunk_t286195069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2298[2] = 
{
	Chunk_t286195069::get_offset_of__data_0(),
	Chunk_t286195069::get_offset_of__offset_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (ChunkedRequestStream_t1674253063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2299[5] = 
{
	0,
	ChunkedRequestStream_t1674253063::get_offset_of__context_8(),
	ChunkedRequestStream_t1674253063::get_offset_of__decoder_9(),
	ChunkedRequestStream_t1674253063::get_offset_of__disposed_10(),
	ChunkedRequestStream_t1674253063::get_offset_of__noMoreData_11(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
