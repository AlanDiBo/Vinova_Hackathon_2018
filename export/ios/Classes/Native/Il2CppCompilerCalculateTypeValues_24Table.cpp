﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Collections.Generic.List`1<System.IDisposable>
struct List_1_t817372929;
// UniRx.RefCountDisposable
struct RefCountDisposable_t3209185398;
// System.IDisposable
struct IDisposable_t3640265483;
// UniRx.BooleanDisposable
struct BooleanDisposable_t84760918;
// UniRx.Triggers.ObservableDestroyTrigger
struct ObservableDestroyTrigger_t3144203490;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.Action
struct Action_t1264377477;
// UniRx.IScheduler
struct IScheduler_t411218504;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UniRx.AsyncSubject`1<UniRx.Unit>
struct AsyncSubject_1_t4049165666;
// UniRx.Observable/<ToAsync>c__AnonStoreyD
struct U3CToAsyncU3Ec__AnonStoreyD_t2567764606;
// System.Action`1<System.IAsyncResult>
struct Action_1_t939472046;
// System.Func`1<System.Collections.IEnumerator>
struct Func_1_t1283030885;
// System.Func`2<UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_2_t181625433;
// System.Net.HttpWebRequest
struct HttpWebRequest_t1669436515;
// UniRx.ObservableYieldInstruction`1<System.String>
struct ObservableYieldInstruction_1_t885211930;
// UniRx.Examples.Sample06_ConvertToCoroutine
struct Sample06_ConvertToCoroutine_t3751354639;
// System.Func`2<UniRx.Examples.Sample06_ConvertToCoroutine,UnityEngine.Transform>
struct Func_2_t585923468;
// System.Func`2<UnityEngine.Transform,System.Boolean>
struct Func_2_t1722577774;
// UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator0/<ComplexCoroutineTest>c__AnonStorey2
struct U3CComplexCoroutineTestU3Ec__AnonStorey2_t4184177592;
// System.String
struct String_t;
// System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>
struct Action_1_t2739833248;
// System.Func`2<System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>,UnityEngine.Application/LogCallback>
struct Func_2_t1411985452;
// System.Action`1<UnityEngine.Application/LogCallback>
struct Action_1_t3760676225;
// System.Func`1<UniRx.Unit>
struct Func_1_t2791996114;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.Action`1<System.Int64>
struct Action_1_t3909034899;
// System.Action`1<UniRx.Unit>
struct Action_1_t3534717062;
// System.Action`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>
struct Action_1_t1105093476;
// UniRx.Diagnostics.Logger
struct Logger_t3861635025;
// System.Func`2<UniRx.Diagnostics.LogEntry,System.Boolean>
struct Func_2_t1305246854;
// System.Action`1<UniRx.Diagnostics.LogEntry>
struct Action_1_t1313974708;
// UniRx.Examples.Sample09_EventHandling
struct Sample09_EventHandling_t917274147;
// UniRx.IReactiveProperty`1<System.Int64>
struct IReactiveProperty_1_t1344063197;
// UniRx.IReactiveProperty`1<System.Boolean>
struct IReactiveProperty_1_t1999751154;
// System.Func`2<System.Int64,System.Boolean>
struct Func_2_t3474512971;
// UniRx.ICancelable
struct ICancelable_t3440398893;
// System.IDisposable[]
struct IDisposableU5BU5D_t3584190570;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.WWW
struct WWW_t3688466362;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3591720268;
// System.Collections.Generic.HashSet`1<System.Type>
struct HashSet_1_t1048894234;
// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t1439315263;
// System.Func`3<System.Int64,UniRx.Unit,System.Int64>
struct Func_3_t2605432362;
// System.Func`3<UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken,System.Collections.IEnumerator>
struct Func_3_t2376649402;
// UniRx.IObserver`1<System.Int64>
struct IObserver_1_t1185869587;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t811551750;
// UniRx.ICustomYieldInstructionErrorHandler
struct ICustomYieldInstructionErrorHandler_t1341746478;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UniRx.IntReactiveProperty
struct IntReactiveProperty_t3243525985;
// UniRx.Examples.Enemy
struct Enemy_t2629818976;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t2197129486;
// System.Func`2<System.Single,System.String>
struct Func_2_t4110414121;
// System.Func`2<System.Boolean,System.Boolean>
struct Func_2_t3812758338;
// System.Func`2<System.Collections.Generic.IList`1<System.Int64>,System.Boolean>
struct Func_2_t2970184680;
// System.Action`1<System.Collections.Generic.IList`1<System.Int64>>
struct Action_1_t1429387386;
// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>
struct EventHandler_1_t3151752610;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// UniRx.CompositeDisposable
struct CompositeDisposable_t3924054141;
// UniRx.Subject`1<System.Int32>
struct Subject_1_t3039602140;
// System.Func`2<System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>
struct Func_2_t2522298254;
// System.Func`2<System.Action`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>>
struct Func_2_t6994052;
// UniRx.ReactiveCollection`1<UnityEngine.GameObject>
struct ReactiveCollection_1_t2124128606;
// System.Action`1<UniRx.CollectionRemoveEvent`1<UnityEngine.GameObject>>
struct Action_1_t192939710;
// System.Func`2<UnityEngine.GameObject,System.Boolean>
struct Func_2_t4243939292;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef COMPOSITEDISPOSABLE_T3924054141_H
#define COMPOSITEDISPOSABLE_T3924054141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CompositeDisposable
struct  CompositeDisposable_t3924054141  : public RuntimeObject
{
public:
	// System.Object UniRx.CompositeDisposable::_gate
	RuntimeObject * ____gate_0;
	// System.Boolean UniRx.CompositeDisposable::_disposed
	bool ____disposed_1;
	// System.Collections.Generic.List`1<System.IDisposable> UniRx.CompositeDisposable::_disposables
	List_1_t817372929 * ____disposables_2;
	// System.Int32 UniRx.CompositeDisposable::_count
	int32_t ____count_3;

public:
	inline static int32_t get_offset_of__gate_0() { return static_cast<int32_t>(offsetof(CompositeDisposable_t3924054141, ____gate_0)); }
	inline RuntimeObject * get__gate_0() const { return ____gate_0; }
	inline RuntimeObject ** get_address_of__gate_0() { return &____gate_0; }
	inline void set__gate_0(RuntimeObject * value)
	{
		____gate_0 = value;
		Il2CppCodeGenWriteBarrier((&____gate_0), value);
	}

	inline static int32_t get_offset_of__disposed_1() { return static_cast<int32_t>(offsetof(CompositeDisposable_t3924054141, ____disposed_1)); }
	inline bool get__disposed_1() const { return ____disposed_1; }
	inline bool* get_address_of__disposed_1() { return &____disposed_1; }
	inline void set__disposed_1(bool value)
	{
		____disposed_1 = value;
	}

	inline static int32_t get_offset_of__disposables_2() { return static_cast<int32_t>(offsetof(CompositeDisposable_t3924054141, ____disposables_2)); }
	inline List_1_t817372929 * get__disposables_2() const { return ____disposables_2; }
	inline List_1_t817372929 ** get_address_of__disposables_2() { return &____disposables_2; }
	inline void set__disposables_2(List_1_t817372929 * value)
	{
		____disposables_2 = value;
		Il2CppCodeGenWriteBarrier((&____disposables_2), value);
	}

	inline static int32_t get_offset_of__count_3() { return static_cast<int32_t>(offsetof(CompositeDisposable_t3924054141, ____count_3)); }
	inline int32_t get__count_3() const { return ____count_3; }
	inline int32_t* get_address_of__count_3() { return &____count_3; }
	inline void set__count_3(int32_t value)
	{
		____count_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSITEDISPOSABLE_T3924054141_H
#ifndef INNERDISPOSABLE_T657274241_H
#define INNERDISPOSABLE_T657274241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.RefCountDisposable/InnerDisposable
struct  InnerDisposable_t657274241  : public RuntimeObject
{
public:
	// UniRx.RefCountDisposable UniRx.RefCountDisposable/InnerDisposable::_parent
	RefCountDisposable_t3209185398 * ____parent_0;
	// System.Object UniRx.RefCountDisposable/InnerDisposable::parentLock
	RuntimeObject * ___parentLock_1;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(InnerDisposable_t657274241, ____parent_0)); }
	inline RefCountDisposable_t3209185398 * get__parent_0() const { return ____parent_0; }
	inline RefCountDisposable_t3209185398 ** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(RefCountDisposable_t3209185398 * value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier((&____parent_0), value);
	}

	inline static int32_t get_offset_of_parentLock_1() { return static_cast<int32_t>(offsetof(InnerDisposable_t657274241, ___parentLock_1)); }
	inline RuntimeObject * get_parentLock_1() const { return ___parentLock_1; }
	inline RuntimeObject ** get_address_of_parentLock_1() { return &___parentLock_1; }
	inline void set_parentLock_1(RuntimeObject * value)
	{
		___parentLock_1 = value;
		Il2CppCodeGenWriteBarrier((&___parentLock_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INNERDISPOSABLE_T657274241_H
#ifndef REFCOUNTDISPOSABLE_T3209185398_H
#define REFCOUNTDISPOSABLE_T3209185398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.RefCountDisposable
struct  RefCountDisposable_t3209185398  : public RuntimeObject
{
public:
	// System.Object UniRx.RefCountDisposable::_gate
	RuntimeObject * ____gate_0;
	// System.IDisposable UniRx.RefCountDisposable::_disposable
	RuntimeObject* ____disposable_1;
	// System.Boolean UniRx.RefCountDisposable::_isPrimaryDisposed
	bool ____isPrimaryDisposed_2;
	// System.Int32 UniRx.RefCountDisposable::_count
	int32_t ____count_3;

public:
	inline static int32_t get_offset_of__gate_0() { return static_cast<int32_t>(offsetof(RefCountDisposable_t3209185398, ____gate_0)); }
	inline RuntimeObject * get__gate_0() const { return ____gate_0; }
	inline RuntimeObject ** get_address_of__gate_0() { return &____gate_0; }
	inline void set__gate_0(RuntimeObject * value)
	{
		____gate_0 = value;
		Il2CppCodeGenWriteBarrier((&____gate_0), value);
	}

	inline static int32_t get_offset_of__disposable_1() { return static_cast<int32_t>(offsetof(RefCountDisposable_t3209185398, ____disposable_1)); }
	inline RuntimeObject* get__disposable_1() const { return ____disposable_1; }
	inline RuntimeObject** get_address_of__disposable_1() { return &____disposable_1; }
	inline void set__disposable_1(RuntimeObject* value)
	{
		____disposable_1 = value;
		Il2CppCodeGenWriteBarrier((&____disposable_1), value);
	}

	inline static int32_t get_offset_of__isPrimaryDisposed_2() { return static_cast<int32_t>(offsetof(RefCountDisposable_t3209185398, ____isPrimaryDisposed_2)); }
	inline bool get__isPrimaryDisposed_2() const { return ____isPrimaryDisposed_2; }
	inline bool* get_address_of__isPrimaryDisposed_2() { return &____isPrimaryDisposed_2; }
	inline void set__isPrimaryDisposed_2(bool value)
	{
		____isPrimaryDisposed_2 = value;
	}

	inline static int32_t get_offset_of__count_3() { return static_cast<int32_t>(offsetof(RefCountDisposable_t3209185398, ____count_3)); }
	inline int32_t get__count_3() const { return ____count_3; }
	inline int32_t* get_address_of__count_3() { return &____count_3; }
	inline void set__count_3(int32_t value)
	{
		____count_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFCOUNTDISPOSABLE_T3209185398_H
#ifndef MULTIPLEASSIGNMENTDISPOSABLE_T1738612192_H
#define MULTIPLEASSIGNMENTDISPOSABLE_T1738612192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MultipleAssignmentDisposable
struct  MultipleAssignmentDisposable_t1738612192  : public RuntimeObject
{
public:
	// System.Object UniRx.MultipleAssignmentDisposable::gate
	RuntimeObject * ___gate_1;
	// System.IDisposable UniRx.MultipleAssignmentDisposable::current
	RuntimeObject* ___current_2;

public:
	inline static int32_t get_offset_of_gate_1() { return static_cast<int32_t>(offsetof(MultipleAssignmentDisposable_t1738612192, ___gate_1)); }
	inline RuntimeObject * get_gate_1() const { return ___gate_1; }
	inline RuntimeObject ** get_address_of_gate_1() { return &___gate_1; }
	inline void set_gate_1(RuntimeObject * value)
	{
		___gate_1 = value;
		Il2CppCodeGenWriteBarrier((&___gate_1), value);
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(MultipleAssignmentDisposable_t1738612192, ___current_2)); }
	inline RuntimeObject* get_current_2() const { return ___current_2; }
	inline RuntimeObject** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(RuntimeObject* value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((&___current_2), value);
	}
};

struct MultipleAssignmentDisposable_t1738612192_StaticFields
{
public:
	// UniRx.BooleanDisposable UniRx.MultipleAssignmentDisposable::True
	BooleanDisposable_t84760918 * ___True_0;

public:
	inline static int32_t get_offset_of_True_0() { return static_cast<int32_t>(offsetof(MultipleAssignmentDisposable_t1738612192_StaticFields, ___True_0)); }
	inline BooleanDisposable_t84760918 * get_True_0() const { return ___True_0; }
	inline BooleanDisposable_t84760918 ** get_address_of_True_0() { return &___True_0; }
	inline void set_True_0(BooleanDisposable_t84760918 * value)
	{
		___True_0 = value;
		Il2CppCodeGenWriteBarrier((&___True_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIPLEASSIGNMENTDISPOSABLE_T1738612192_H
#ifndef U3CMONITORTRIGGERHEALTHU3EC__ITERATOR0_T998410463_H
#define U3CMONITORTRIGGERHEALTHU3EC__ITERATOR0_T998410463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.DisposableExtensions/<MonitorTriggerHealth>c__Iterator0
struct  U3CMonitorTriggerHealthU3Ec__Iterator0_t998410463  : public RuntimeObject
{
public:
	// UniRx.Triggers.ObservableDestroyTrigger UniRx.DisposableExtensions/<MonitorTriggerHealth>c__Iterator0::trigger
	ObservableDestroyTrigger_t3144203490 * ___trigger_0;
	// UnityEngine.GameObject UniRx.DisposableExtensions/<MonitorTriggerHealth>c__Iterator0::targetGameObject
	GameObject_t1113636619 * ___targetGameObject_1;
	// System.Object UniRx.DisposableExtensions/<MonitorTriggerHealth>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UniRx.DisposableExtensions/<MonitorTriggerHealth>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 UniRx.DisposableExtensions/<MonitorTriggerHealth>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_trigger_0() { return static_cast<int32_t>(offsetof(U3CMonitorTriggerHealthU3Ec__Iterator0_t998410463, ___trigger_0)); }
	inline ObservableDestroyTrigger_t3144203490 * get_trigger_0() const { return ___trigger_0; }
	inline ObservableDestroyTrigger_t3144203490 ** get_address_of_trigger_0() { return &___trigger_0; }
	inline void set_trigger_0(ObservableDestroyTrigger_t3144203490 * value)
	{
		___trigger_0 = value;
		Il2CppCodeGenWriteBarrier((&___trigger_0), value);
	}

	inline static int32_t get_offset_of_targetGameObject_1() { return static_cast<int32_t>(offsetof(U3CMonitorTriggerHealthU3Ec__Iterator0_t998410463, ___targetGameObject_1)); }
	inline GameObject_t1113636619 * get_targetGameObject_1() const { return ___targetGameObject_1; }
	inline GameObject_t1113636619 ** get_address_of_targetGameObject_1() { return &___targetGameObject_1; }
	inline void set_targetGameObject_1(GameObject_t1113636619 * value)
	{
		___targetGameObject_1 = value;
		Il2CppCodeGenWriteBarrier((&___targetGameObject_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CMonitorTriggerHealthU3Ec__Iterator0_t998410463, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CMonitorTriggerHealthU3Ec__Iterator0_t998410463, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CMonitorTriggerHealthU3Ec__Iterator0_t998410463, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMONITORTRIGGERHEALTHU3EC__ITERATOR0_T998410463_H
#ifndef DISPOSABLEEXTENSIONS_T1054371994_H
#define DISPOSABLEEXTENSIONS_T1054371994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.DisposableExtensions
struct  DisposableExtensions_t1054371994  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPOSABLEEXTENSIONS_T1054371994_H
#ifndef ANONYMOUSDISPOSABLE_T4034176521_H
#define ANONYMOUSDISPOSABLE_T4034176521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Disposable/AnonymousDisposable
struct  AnonymousDisposable_t4034176521  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Disposable/AnonymousDisposable::isDisposed
	bool ___isDisposed_0;
	// System.Action UniRx.Disposable/AnonymousDisposable::dispose
	Action_t1264377477 * ___dispose_1;

public:
	inline static int32_t get_offset_of_isDisposed_0() { return static_cast<int32_t>(offsetof(AnonymousDisposable_t4034176521, ___isDisposed_0)); }
	inline bool get_isDisposed_0() const { return ___isDisposed_0; }
	inline bool* get_address_of_isDisposed_0() { return &___isDisposed_0; }
	inline void set_isDisposed_0(bool value)
	{
		___isDisposed_0 = value;
	}

	inline static int32_t get_offset_of_dispose_1() { return static_cast<int32_t>(offsetof(AnonymousDisposable_t4034176521, ___dispose_1)); }
	inline Action_t1264377477 * get_dispose_1() const { return ___dispose_1; }
	inline Action_t1264377477 ** get_address_of_dispose_1() { return &___dispose_1; }
	inline void set_dispose_1(Action_t1264377477 * value)
	{
		___dispose_1 = value;
		Il2CppCodeGenWriteBarrier((&___dispose_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANONYMOUSDISPOSABLE_T4034176521_H
#ifndef EMPTYDISPOSABLE_T1744265291_H
#define EMPTYDISPOSABLE_T1744265291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Disposable/EmptyDisposable
struct  EmptyDisposable_t1744265291  : public RuntimeObject
{
public:

public:
};

struct EmptyDisposable_t1744265291_StaticFields
{
public:
	// UniRx.Disposable/EmptyDisposable UniRx.Disposable/EmptyDisposable::Singleton
	EmptyDisposable_t1744265291 * ___Singleton_0;

public:
	inline static int32_t get_offset_of_Singleton_0() { return static_cast<int32_t>(offsetof(EmptyDisposable_t1744265291_StaticFields, ___Singleton_0)); }
	inline EmptyDisposable_t1744265291 * get_Singleton_0() const { return ___Singleton_0; }
	inline EmptyDisposable_t1744265291 ** get_address_of_Singleton_0() { return &___Singleton_0; }
	inline void set_Singleton_0(EmptyDisposable_t1744265291 * value)
	{
		___Singleton_0 = value;
		Il2CppCodeGenWriteBarrier((&___Singleton_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYDISPOSABLE_T1744265291_H
#ifndef DISPOSABLE_T665628049_H
#define DISPOSABLE_T665628049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Disposable
struct  Disposable_t665628049  : public RuntimeObject
{
public:

public:
};

struct Disposable_t665628049_StaticFields
{
public:
	// System.IDisposable UniRx.Disposable::Empty
	RuntimeObject* ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Disposable_t665628049_StaticFields, ___Empty_0)); }
	inline RuntimeObject* get_Empty_0() const { return ___Empty_0; }
	inline RuntimeObject** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(RuntimeObject* value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPOSABLE_T665628049_H
#ifndef SERIALDISPOSABLE_T3525249344_H
#define SERIALDISPOSABLE_T3525249344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.SerialDisposable
struct  SerialDisposable_t3525249344  : public RuntimeObject
{
public:
	// System.Object UniRx.SerialDisposable::gate
	RuntimeObject * ___gate_0;
	// System.IDisposable UniRx.SerialDisposable::current
	RuntimeObject* ___current_1;
	// System.Boolean UniRx.SerialDisposable::disposed
	bool ___disposed_2;

public:
	inline static int32_t get_offset_of_gate_0() { return static_cast<int32_t>(offsetof(SerialDisposable_t3525249344, ___gate_0)); }
	inline RuntimeObject * get_gate_0() const { return ___gate_0; }
	inline RuntimeObject ** get_address_of_gate_0() { return &___gate_0; }
	inline void set_gate_0(RuntimeObject * value)
	{
		___gate_0 = value;
		Il2CppCodeGenWriteBarrier((&___gate_0), value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(SerialDisposable_t3525249344, ___current_1)); }
	inline RuntimeObject* get_current_1() const { return ___current_1; }
	inline RuntimeObject** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(RuntimeObject* value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((&___current_1), value);
	}

	inline static int32_t get_offset_of_disposed_2() { return static_cast<int32_t>(offsetof(SerialDisposable_t3525249344, ___disposed_2)); }
	inline bool get_disposed_2() const { return ___disposed_2; }
	inline bool* get_address_of_disposed_2() { return &___disposed_2; }
	inline void set_disposed_2(bool value)
	{
		___disposed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALDISPOSABLE_T3525249344_H
#ifndef BOOLEANDISPOSABLE_T84760918_H
#define BOOLEANDISPOSABLE_T84760918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.BooleanDisposable
struct  BooleanDisposable_t84760918  : public RuntimeObject
{
public:
	// System.Boolean UniRx.BooleanDisposable::<IsDisposed>k__BackingField
	bool ___U3CIsDisposedU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsDisposedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BooleanDisposable_t84760918, ___U3CIsDisposedU3Ek__BackingField_0)); }
	inline bool get_U3CIsDisposedU3Ek__BackingField_0() const { return ___U3CIsDisposedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsDisposedU3Ek__BackingField_0() { return &___U3CIsDisposedU3Ek__BackingField_0; }
	inline void set_U3CIsDisposedU3Ek__BackingField_0(bool value)
	{
		___U3CIsDisposedU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEANDISPOSABLE_T84760918_H
#ifndef U3CTOASYNCU3EC__ANONSTOREYD_T2567764606_H
#define U3CTOASYNCU3EC__ANONSTOREYD_T2567764606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<ToAsync>c__AnonStoreyD
struct  U3CToAsyncU3Ec__AnonStoreyD_t2567764606  : public RuntimeObject
{
public:
	// UniRx.IScheduler UniRx.Observable/<ToAsync>c__AnonStoreyD::scheduler
	RuntimeObject* ___scheduler_0;
	// System.Action UniRx.Observable/<ToAsync>c__AnonStoreyD::action
	Action_t1264377477 * ___action_1;

public:
	inline static int32_t get_offset_of_scheduler_0() { return static_cast<int32_t>(offsetof(U3CToAsyncU3Ec__AnonStoreyD_t2567764606, ___scheduler_0)); }
	inline RuntimeObject* get_scheduler_0() const { return ___scheduler_0; }
	inline RuntimeObject** get_address_of_scheduler_0() { return &___scheduler_0; }
	inline void set_scheduler_0(RuntimeObject* value)
	{
		___scheduler_0 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_0), value);
	}

	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(U3CToAsyncU3Ec__AnonStoreyD_t2567764606, ___action_1)); }
	inline Action_t1264377477 * get_action_1() const { return ___action_1; }
	inline Action_t1264377477 ** get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(Action_t1264377477 * value)
	{
		___action_1 = value;
		Il2CppCodeGenWriteBarrier((&___action_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTOASYNCU3EC__ANONSTOREYD_T2567764606_H
#ifndef SCHEDULEDDISPOSABLE_T4090582988_H
#define SCHEDULEDDISPOSABLE_T4090582988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ScheduledDisposable
struct  ScheduledDisposable_t4090582988  : public RuntimeObject
{
public:
	// UniRx.IScheduler UniRx.ScheduledDisposable::scheduler
	RuntimeObject* ___scheduler_0;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.ScheduledDisposable::disposable
	RuntimeObject* ___disposable_1;
	// System.Int32 UniRx.ScheduledDisposable::isDisposed
	int32_t ___isDisposed_2;

public:
	inline static int32_t get_offset_of_scheduler_0() { return static_cast<int32_t>(offsetof(ScheduledDisposable_t4090582988, ___scheduler_0)); }
	inline RuntimeObject* get_scheduler_0() const { return ___scheduler_0; }
	inline RuntimeObject** get_address_of_scheduler_0() { return &___scheduler_0; }
	inline void set_scheduler_0(RuntimeObject* value)
	{
		___scheduler_0 = value;
		Il2CppCodeGenWriteBarrier((&___scheduler_0), value);
	}

	inline static int32_t get_offset_of_disposable_1() { return static_cast<int32_t>(offsetof(ScheduledDisposable_t4090582988, ___disposable_1)); }
	inline RuntimeObject* get_disposable_1() const { return ___disposable_1; }
	inline RuntimeObject** get_address_of_disposable_1() { return &___disposable_1; }
	inline void set_disposable_1(RuntimeObject* value)
	{
		___disposable_1 = value;
		Il2CppCodeGenWriteBarrier((&___disposable_1), value);
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ScheduledDisposable_t4090582988, ___isDisposed_2)); }
	inline int32_t get_isDisposed_2() const { return ___isDisposed_2; }
	inline int32_t* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(int32_t value)
	{
		___isDisposed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEDULEDDISPOSABLE_T4090582988_H
#ifndef SINGLEASSIGNMENTDISPOSABLE_T4102667663_H
#define SINGLEASSIGNMENTDISPOSABLE_T4102667663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.SingleAssignmentDisposable
struct  SingleAssignmentDisposable_t4102667663  : public RuntimeObject
{
public:
	// System.Object UniRx.SingleAssignmentDisposable::gate
	RuntimeObject * ___gate_0;
	// System.IDisposable UniRx.SingleAssignmentDisposable::current
	RuntimeObject* ___current_1;
	// System.Boolean UniRx.SingleAssignmentDisposable::disposed
	bool ___disposed_2;

public:
	inline static int32_t get_offset_of_gate_0() { return static_cast<int32_t>(offsetof(SingleAssignmentDisposable_t4102667663, ___gate_0)); }
	inline RuntimeObject * get_gate_0() const { return ___gate_0; }
	inline RuntimeObject ** get_address_of_gate_0() { return &___gate_0; }
	inline void set_gate_0(RuntimeObject * value)
	{
		___gate_0 = value;
		Il2CppCodeGenWriteBarrier((&___gate_0), value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(SingleAssignmentDisposable_t4102667663, ___current_1)); }
	inline RuntimeObject* get_current_1() const { return ___current_1; }
	inline RuntimeObject** get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(RuntimeObject* value)
	{
		___current_1 = value;
		Il2CppCodeGenWriteBarrier((&___current_1), value);
	}

	inline static int32_t get_offset_of_disposed_2() { return static_cast<int32_t>(offsetof(SingleAssignmentDisposable_t4102667663, ___disposed_2)); }
	inline bool get_disposed_2() const { return ___disposed_2; }
	inline bool* get_address_of_disposed_2() { return &___disposed_2; }
	inline void set_disposed_2(bool value)
	{
		___disposed_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLEASSIGNMENTDISPOSABLE_T4102667663_H
#ifndef STABLECOMPOSITEDISPOSABLE_T519652846_H
#define STABLECOMPOSITEDISPOSABLE_T519652846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.StableCompositeDisposable
struct  StableCompositeDisposable_t519652846  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STABLECOMPOSITEDISPOSABLE_T519652846_H
#ifndef U3CTIMERFRAMEU3EC__ANONSTOREY2B_T3688766031_H
#define U3CTIMERFRAMEU3EC__ANONSTOREY2B_T3688766031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<TimerFrame>c__AnonStorey2B
struct  U3CTimerFrameU3Ec__AnonStorey2B_t3688766031  : public RuntimeObject
{
public:
	// System.Int32 UniRx.Observable/<TimerFrame>c__AnonStorey2B::dueTimeFrameCount
	int32_t ___dueTimeFrameCount_0;
	// System.Int32 UniRx.Observable/<TimerFrame>c__AnonStorey2B::periodFrameCount
	int32_t ___periodFrameCount_1;

public:
	inline static int32_t get_offset_of_dueTimeFrameCount_0() { return static_cast<int32_t>(offsetof(U3CTimerFrameU3Ec__AnonStorey2B_t3688766031, ___dueTimeFrameCount_0)); }
	inline int32_t get_dueTimeFrameCount_0() const { return ___dueTimeFrameCount_0; }
	inline int32_t* get_address_of_dueTimeFrameCount_0() { return &___dueTimeFrameCount_0; }
	inline void set_dueTimeFrameCount_0(int32_t value)
	{
		___dueTimeFrameCount_0 = value;
	}

	inline static int32_t get_offset_of_periodFrameCount_1() { return static_cast<int32_t>(offsetof(U3CTimerFrameU3Ec__AnonStorey2B_t3688766031, ___periodFrameCount_1)); }
	inline int32_t get_periodFrameCount_1() const { return ___periodFrameCount_1; }
	inline int32_t* get_address_of_periodFrameCount_1() { return &___periodFrameCount_1; }
	inline void set_periodFrameCount_1(int32_t value)
	{
		___periodFrameCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMERFRAMEU3EC__ANONSTOREY2B_T3688766031_H
#ifndef U3CTIMERFRAMEU3EC__ANONSTOREY2A_T1732450895_H
#define U3CTIMERFRAMEU3EC__ANONSTOREY2A_T1732450895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<TimerFrame>c__AnonStorey2A
struct  U3CTimerFrameU3Ec__AnonStorey2A_t1732450895  : public RuntimeObject
{
public:
	// System.Int32 UniRx.Observable/<TimerFrame>c__AnonStorey2A::dueTimeFrameCount
	int32_t ___dueTimeFrameCount_0;

public:
	inline static int32_t get_offset_of_dueTimeFrameCount_0() { return static_cast<int32_t>(offsetof(U3CTimerFrameU3Ec__AnonStorey2A_t1732450895, ___dueTimeFrameCount_0)); }
	inline int32_t get_dueTimeFrameCount_0() const { return ___dueTimeFrameCount_0; }
	inline int32_t* get_address_of_dueTimeFrameCount_0() { return &___dueTimeFrameCount_0; }
	inline void set_dueTimeFrameCount_0(int32_t value)
	{
		___dueTimeFrameCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMERFRAMEU3EC__ANONSTOREY2A_T1732450895_H
#ifndef U3CTOOBSERVABLEU3EC__ANONSTOREY29_T737702686_H
#define U3CTOOBSERVABLEU3EC__ANONSTOREY29_T737702686_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<ToObservable>c__AnonStorey29
struct  U3CToObservableU3Ec__AnonStorey29_t737702686  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator UniRx.Observable/<ToObservable>c__AnonStorey29::coroutine
	RuntimeObject* ___coroutine_0;
	// System.Boolean UniRx.Observable/<ToObservable>c__AnonStorey29::publishEveryYield
	bool ___publishEveryYield_1;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CToObservableU3Ec__AnonStorey29_t737702686, ___coroutine_0)); }
	inline RuntimeObject* get_coroutine_0() const { return ___coroutine_0; }
	inline RuntimeObject** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(RuntimeObject* value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_0), value);
	}

	inline static int32_t get_offset_of_publishEveryYield_1() { return static_cast<int32_t>(offsetof(U3CToObservableU3Ec__AnonStorey29_t737702686, ___publishEveryYield_1)); }
	inline bool get_publishEveryYield_1() const { return ___publishEveryYield_1; }
	inline bool* get_address_of_publishEveryYield_1() { return &___publishEveryYield_1; }
	inline void set_publishEveryYield_1(bool value)
	{
		___publishEveryYield_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTOOBSERVABLEU3EC__ANONSTOREY29_T737702686_H
#ifndef U3CTOASYNCU3EC__ANONSTOREYE_T2090547987_H
#define U3CTOASYNCU3EC__ANONSTOREYE_T2090547987_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<ToAsync>c__AnonStoreyD/<ToAsync>c__AnonStoreyE
struct  U3CToAsyncU3Ec__AnonStoreyE_t2090547987  : public RuntimeObject
{
public:
	// UniRx.AsyncSubject`1<UniRx.Unit> UniRx.Observable/<ToAsync>c__AnonStoreyD/<ToAsync>c__AnonStoreyE::subject
	AsyncSubject_1_t4049165666 * ___subject_0;
	// UniRx.Observable/<ToAsync>c__AnonStoreyD UniRx.Observable/<ToAsync>c__AnonStoreyD/<ToAsync>c__AnonStoreyE::<>f__ref$13
	U3CToAsyncU3Ec__AnonStoreyD_t2567764606 * ___U3CU3Ef__refU2413_1;

public:
	inline static int32_t get_offset_of_subject_0() { return static_cast<int32_t>(offsetof(U3CToAsyncU3Ec__AnonStoreyE_t2090547987, ___subject_0)); }
	inline AsyncSubject_1_t4049165666 * get_subject_0() const { return ___subject_0; }
	inline AsyncSubject_1_t4049165666 ** get_address_of_subject_0() { return &___subject_0; }
	inline void set_subject_0(AsyncSubject_1_t4049165666 * value)
	{
		___subject_0 = value;
		Il2CppCodeGenWriteBarrier((&___subject_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU2413_1() { return static_cast<int32_t>(offsetof(U3CToAsyncU3Ec__AnonStoreyE_t2090547987, ___U3CU3Ef__refU2413_1)); }
	inline U3CToAsyncU3Ec__AnonStoreyD_t2567764606 * get_U3CU3Ef__refU2413_1() const { return ___U3CU3Ef__refU2413_1; }
	inline U3CToAsyncU3Ec__AnonStoreyD_t2567764606 ** get_address_of_U3CU3Ef__refU2413_1() { return &___U3CU3Ef__refU2413_1; }
	inline void set_U3CU3Ef__refU2413_1(U3CToAsyncU3Ec__AnonStoreyD_t2567764606 * value)
	{
		___U3CU3Ef__refU2413_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU2413_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTOASYNCU3EC__ANONSTOREYE_T2090547987_H
#ifndef U3CFROMASYNCPATTERNU3EC__ANONSTOREY1A_T1162034728_H
#define U3CFROMASYNCPATTERNU3EC__ANONSTOREY1A_T1162034728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<FromAsyncPattern>c__AnonStorey1A
struct  U3CFromAsyncPatternU3Ec__AnonStorey1A_t1162034728  : public RuntimeObject
{
public:
	// System.Action`1<System.IAsyncResult> UniRx.Observable/<FromAsyncPattern>c__AnonStorey1A::end
	Action_1_t939472046 * ___end_0;

public:
	inline static int32_t get_offset_of_end_0() { return static_cast<int32_t>(offsetof(U3CFromAsyncPatternU3Ec__AnonStorey1A_t1162034728, ___end_0)); }
	inline Action_1_t939472046 * get_end_0() const { return ___end_0; }
	inline Action_1_t939472046 ** get_address_of_end_0() { return &___end_0; }
	inline void set_end_0(Action_1_t939472046 * value)
	{
		___end_0 = value;
		Il2CppCodeGenWriteBarrier((&___end_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFROMASYNCPATTERNU3EC__ANONSTOREY1A_T1162034728_H
#ifndef U3CFROMCOROUTINEU3EC__ANONSTOREY1D_T4004376715_H
#define U3CFROMCOROUTINEU3EC__ANONSTOREY1D_T4004376715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<FromCoroutine>c__AnonStorey1D
struct  U3CFromCoroutineU3Ec__AnonStorey1D_t4004376715  : public RuntimeObject
{
public:
	// System.Func`1<System.Collections.IEnumerator> UniRx.Observable/<FromCoroutine>c__AnonStorey1D::coroutine
	Func_1_t1283030885 * ___coroutine_0;
	// System.Boolean UniRx.Observable/<FromCoroutine>c__AnonStorey1D::publishEveryYield
	bool ___publishEveryYield_1;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CFromCoroutineU3Ec__AnonStorey1D_t4004376715, ___coroutine_0)); }
	inline Func_1_t1283030885 * get_coroutine_0() const { return ___coroutine_0; }
	inline Func_1_t1283030885 ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Func_1_t1283030885 * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_0), value);
	}

	inline static int32_t get_offset_of_publishEveryYield_1() { return static_cast<int32_t>(offsetof(U3CFromCoroutineU3Ec__AnonStorey1D_t4004376715, ___publishEveryYield_1)); }
	inline bool get_publishEveryYield_1() const { return ___publishEveryYield_1; }
	inline bool* get_address_of_publishEveryYield_1() { return &___publishEveryYield_1; }
	inline void set_publishEveryYield_1(bool value)
	{
		___publishEveryYield_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFROMCOROUTINEU3EC__ANONSTOREY1D_T4004376715_H
#ifndef U3CFROMCOROUTINEU3EC__ANONSTOREY1E_T1275493360_H
#define U3CFROMCOROUTINEU3EC__ANONSTOREY1E_T1275493360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<FromCoroutine>c__AnonStorey1E
struct  U3CFromCoroutineU3Ec__AnonStorey1E_t1275493360  : public RuntimeObject
{
public:
	// System.Func`2<UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable/<FromCoroutine>c__AnonStorey1E::coroutine
	Func_2_t181625433 * ___coroutine_0;
	// System.Boolean UniRx.Observable/<FromCoroutine>c__AnonStorey1E::publishEveryYield
	bool ___publishEveryYield_1;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CFromCoroutineU3Ec__AnonStorey1E_t1275493360, ___coroutine_0)); }
	inline Func_2_t181625433 * get_coroutine_0() const { return ___coroutine_0; }
	inline Func_2_t181625433 ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Func_2_t181625433 * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_0), value);
	}

	inline static int32_t get_offset_of_publishEveryYield_1() { return static_cast<int32_t>(offsetof(U3CFromCoroutineU3Ec__AnonStorey1E_t1275493360, ___publishEveryYield_1)); }
	inline bool get_publishEveryYield_1() const { return ___publishEveryYield_1; }
	inline bool* get_address_of_publishEveryYield_1() { return &___publishEveryYield_1; }
	inline void set_publishEveryYield_1(bool value)
	{
		___publishEveryYield_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFROMCOROUTINEU3EC__ANONSTOREY1E_T1275493360_H
#ifndef U3CFROMMICROCOROUTINEU3EC__ANONSTOREY1F_T3627648857_H
#define U3CFROMMICROCOROUTINEU3EC__ANONSTOREY1F_T3627648857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<FromMicroCoroutine>c__AnonStorey1F
struct  U3CFromMicroCoroutineU3Ec__AnonStorey1F_t3627648857  : public RuntimeObject
{
public:
	// System.Func`1<System.Collections.IEnumerator> UniRx.Observable/<FromMicroCoroutine>c__AnonStorey1F::coroutine
	Func_1_t1283030885 * ___coroutine_0;
	// System.Boolean UniRx.Observable/<FromMicroCoroutine>c__AnonStorey1F::publishEveryYield
	bool ___publishEveryYield_1;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CFromMicroCoroutineU3Ec__AnonStorey1F_t3627648857, ___coroutine_0)); }
	inline Func_1_t1283030885 * get_coroutine_0() const { return ___coroutine_0; }
	inline Func_1_t1283030885 ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Func_1_t1283030885 * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_0), value);
	}

	inline static int32_t get_offset_of_publishEveryYield_1() { return static_cast<int32_t>(offsetof(U3CFromMicroCoroutineU3Ec__AnonStorey1F_t3627648857, ___publishEveryYield_1)); }
	inline bool get_publishEveryYield_1() const { return ___publishEveryYield_1; }
	inline bool* get_address_of_publishEveryYield_1() { return &___publishEveryYield_1; }
	inline void set_publishEveryYield_1(bool value)
	{
		___publishEveryYield_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFROMMICROCOROUTINEU3EC__ANONSTOREY1F_T3627648857_H
#ifndef U3CFROMMICROCOROUTINEU3EC__ANONSTOREY20_T4078298410_H
#define U3CFROMMICROCOROUTINEU3EC__ANONSTOREY20_T4078298410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<FromMicroCoroutine>c__AnonStorey20
struct  U3CFromMicroCoroutineU3Ec__AnonStorey20_t4078298410  : public RuntimeObject
{
public:
	// System.Func`2<UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable/<FromMicroCoroutine>c__AnonStorey20::coroutine
	Func_2_t181625433 * ___coroutine_0;
	// System.Boolean UniRx.Observable/<FromMicroCoroutine>c__AnonStorey20::publishEveryYield
	bool ___publishEveryYield_1;

public:
	inline static int32_t get_offset_of_coroutine_0() { return static_cast<int32_t>(offsetof(U3CFromMicroCoroutineU3Ec__AnonStorey20_t4078298410, ___coroutine_0)); }
	inline Func_2_t181625433 * get_coroutine_0() const { return ___coroutine_0; }
	inline Func_2_t181625433 ** get_address_of_coroutine_0() { return &___coroutine_0; }
	inline void set_coroutine_0(Func_2_t181625433 * value)
	{
		___coroutine_0 = value;
		Il2CppCodeGenWriteBarrier((&___coroutine_0), value);
	}

	inline static int32_t get_offset_of_publishEveryYield_1() { return static_cast<int32_t>(offsetof(U3CFromMicroCoroutineU3Ec__AnonStorey20_t4078298410, ___publishEveryYield_1)); }
	inline bool get_publishEveryYield_1() const { return ___publishEveryYield_1; }
	inline bool* get_address_of_publishEveryYield_1() { return &___publishEveryYield_1; }
	inline void set_publishEveryYield_1(bool value)
	{
		___publishEveryYield_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFROMMICROCOROUTINEU3EC__ANONSTOREY20_T4078298410_H
#ifndef U3CGETRESPONSEASOBSERVABLEU3EC__ANONSTOREY2_T3350246897_H
#define U3CGETRESPONSEASOBSERVABLEU3EC__ANONSTOREY2_T3350246897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.WebRequestExtensions/<GetResponseAsObservable>c__AnonStorey2
struct  U3CGetResponseAsObservableU3Ec__AnonStorey2_t3350246897  : public RuntimeObject
{
public:
	// System.Net.HttpWebRequest UniRx.WebRequestExtensions/<GetResponseAsObservable>c__AnonStorey2::request
	HttpWebRequest_t1669436515 * ___request_0;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CGetResponseAsObservableU3Ec__AnonStorey2_t3350246897, ___request_0)); }
	inline HttpWebRequest_t1669436515 * get_request_0() const { return ___request_0; }
	inline HttpWebRequest_t1669436515 ** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(HttpWebRequest_t1669436515 * value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETRESPONSEASOBSERVABLEU3EC__ANONSTOREY2_T3350246897_H
#ifndef U3CASYNCBU3EC__ITERATOR1_T2525684525_H
#define U3CASYNCBU3EC__ITERATOR1_T2525684525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__Iterator1
struct  U3CAsyncBU3Ec__Iterator1_t2525684525  : public RuntimeObject
{
public:
	// System.Object UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__Iterator1::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__Iterator1::$disposing
	bool ___U24disposing_1;
	// System.Int32 UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncB>c__Iterator1::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CAsyncBU3Ec__Iterator1_t2525684525, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CAsyncBU3Ec__Iterator1_t2525684525, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CAsyncBU3Ec__Iterator1_t2525684525, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASYNCBU3EC__ITERATOR1_T2525684525_H
#ifndef U3CASYNCAU3EC__ITERATOR0_T2523753990_H
#define U3CASYNCAU3EC__ITERATOR0_T2523753990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__Iterator0
struct  U3CAsyncAU3Ec__Iterator0_t2523753990  : public RuntimeObject
{
public:
	// System.Object UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 UniRx.Examples.Sample07_OrchestratIEnumerator/<AsyncA>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CAsyncAU3Ec__Iterator0_t2523753990, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CAsyncAU3Ec__Iterator0_t2523753990, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CAsyncAU3Ec__Iterator0_t2523753990, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CASYNCAU3EC__ITERATOR0_T2523753990_H
#ifndef U3CTESTNEWCUSTOMYIELDINSTRUCTIONU3EC__ITERATOR1_T3391453989_H
#define U3CTESTNEWCUSTOMYIELDINSTRUCTIONU3EC__ITERATOR1_T3391453989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator1
struct  U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989  : public RuntimeObject
{
public:
	// UniRx.ObservableYieldInstruction`1<System.String> UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator1::<o>__0
	ObservableYieldInstruction_1_t885211930 * ___U3CoU3E__0_0;
	// UniRx.Examples.Sample06_ConvertToCoroutine UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator1::$this
	Sample06_ConvertToCoroutine_t3751354639 * ___U24this_1;
	// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CoU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989, ___U3CoU3E__0_0)); }
	inline ObservableYieldInstruction_1_t885211930 * get_U3CoU3E__0_0() const { return ___U3CoU3E__0_0; }
	inline ObservableYieldInstruction_1_t885211930 ** get_address_of_U3CoU3E__0_0() { return &___U3CoU3E__0_0; }
	inline void set_U3CoU3E__0_0(ObservableYieldInstruction_1_t885211930 * value)
	{
		___U3CoU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CoU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989, ___U24this_1)); }
	inline Sample06_ConvertToCoroutine_t3751354639 * get_U24this_1() const { return ___U24this_1; }
	inline Sample06_ConvertToCoroutine_t3751354639 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Sample06_ConvertToCoroutine_t3751354639 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

struct U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989_StaticFields
{
public:
	// System.Func`2<UniRx.Examples.Sample06_ConvertToCoroutine,UnityEngine.Transform> UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator1::<>f__am$cache0
	Func_2_t585923468 * ___U3CU3Ef__amU24cache0_5;
	// System.Func`2<UnityEngine.Transform,System.Boolean> UniRx.Examples.Sample06_ConvertToCoroutine/<TestNewCustomYieldInstruction>c__Iterator1::<>f__am$cache1
	Func_2_t1722577774 * ___U3CU3Ef__amU24cache1_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_2_t585923468 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_2_t585923468 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_2_t585923468 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline Func_2_t1722577774 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline Func_2_t1722577774 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(Func_2_t1722577774 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTESTNEWCUSTOMYIELDINSTRUCTIONU3EC__ITERATOR1_T3391453989_H
#ifndef U3CCOMPLEXCOROUTINETESTU3EC__ANONSTOREY2_T4184177592_H
#define U3CCOMPLEXCOROUTINETESTU3EC__ANONSTOREY2_T4184177592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator0/<ComplexCoroutineTest>c__AnonStorey2
struct  U3CComplexCoroutineTestU3Ec__AnonStorey2_t4184177592  : public RuntimeObject
{
public:
	// System.Int32 UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator0/<ComplexCoroutineTest>c__AnonStorey2::v
	int32_t ___v_0;

public:
	inline static int32_t get_offset_of_v_0() { return static_cast<int32_t>(offsetof(U3CComplexCoroutineTestU3Ec__AnonStorey2_t4184177592, ___v_0)); }
	inline int32_t get_v_0() const { return ___v_0; }
	inline int32_t* get_address_of_v_0() { return &___v_0; }
	inline void set_v_0(int32_t value)
	{
		___v_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOMPLEXCOROUTINETESTU3EC__ANONSTOREY2_T4184177592_H
#ifndef U3CCOMPLEXCOROUTINETESTU3EC__ITERATOR0_T2970897030_H
#define U3CCOMPLEXCOROUTINETESTU3EC__ITERATOR0_T2970897030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator0
struct  U3CComplexCoroutineTestU3Ec__Iterator0_t2970897030  : public RuntimeObject
{
public:
	// System.Object UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator0::$PC
	int32_t ___U24PC_2;
	// UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator0/<ComplexCoroutineTest>c__AnonStorey2 UniRx.Examples.Sample06_ConvertToCoroutine/<ComplexCoroutineTest>c__Iterator0::$locvar0
	U3CComplexCoroutineTestU3Ec__AnonStorey2_t4184177592 * ___U24locvar0_3;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CComplexCoroutineTestU3Ec__Iterator0_t2970897030, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CComplexCoroutineTestU3Ec__Iterator0_t2970897030, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CComplexCoroutineTestU3Ec__Iterator0_t2970897030, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_3() { return static_cast<int32_t>(offsetof(U3CComplexCoroutineTestU3Ec__Iterator0_t2970897030, ___U24locvar0_3)); }
	inline U3CComplexCoroutineTestU3Ec__AnonStorey2_t4184177592 * get_U24locvar0_3() const { return ___U24locvar0_3; }
	inline U3CComplexCoroutineTestU3Ec__AnonStorey2_t4184177592 ** get_address_of_U24locvar0_3() { return &___U24locvar0_3; }
	inline void set_U24locvar0_3(U3CComplexCoroutineTestU3Ec__AnonStorey2_t4184177592 * value)
	{
		___U24locvar0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCOMPLEXCOROUTINETESTU3EC__ITERATOR0_T2970897030_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CGETWWWU3EC__ANONSTOREY1_T1511513620_H
#define U3CGETWWWU3EC__ANONSTOREY1_T1511513620_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWW>c__AnonStorey1
struct  U3CGetWWWU3Ec__AnonStorey1_t1511513620  : public RuntimeObject
{
public:
	// System.String UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWW>c__AnonStorey1::url
	String_t* ___url_0;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CGetWWWU3Ec__AnonStorey1_t1511513620, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETWWWU3EC__ANONSTOREY1_T1511513620_H
#ifndef SAMPLE05_CONVERTFROMCOROUTINE_T1417798464_H
#define SAMPLE05_CONVERTFROMCOROUTINE_T1417798464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample05_ConvertFromCoroutine
struct  Sample05_ConvertFromCoroutine_t1417798464  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE05_CONVERTFROMCOROUTINE_T1417798464_H
#ifndef U3CLOGCALLBACKASOBSERVABLEU3EC__ANONSTOREY0_T2625726255_H
#define U3CLOGCALLBACKASOBSERVABLEU3EC__ANONSTOREY0_T2625726255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper/<LogCallbackAsObservable>c__AnonStorey0
struct  U3CLogCallbackAsObservableU3Ec__AnonStorey0_t2625726255  : public RuntimeObject
{
public:
	// System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback> UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper/<LogCallbackAsObservable>c__AnonStorey0::h
	Action_1_t2739833248 * ___h_0;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(U3CLogCallbackAsObservableU3Ec__AnonStorey0_t2625726255, ___h_0)); }
	inline Action_1_t2739833248 * get_h_0() const { return ___h_0; }
	inline Action_1_t2739833248 ** get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(Action_1_t2739833248 * value)
	{
		___h_0 = value;
		Il2CppCodeGenWriteBarrier((&___h_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOGCALLBACKASOBSERVABLEU3EC__ANONSTOREY0_T2625726255_H
#ifndef LOGHELPER_T4028092818_H
#define LOGHELPER_T4028092818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper
struct  LogHelper_t4028092818  : public RuntimeObject
{
public:

public:
};

struct LogHelper_t4028092818_StaticFields
{
public:
	// System.Func`2<System.Action`1<UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback>,UnityEngine.Application/LogCallback> UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper::<>f__am$cache0
	Func_2_t1411985452 * ___U3CU3Ef__amU24cache0_0;
	// System.Action`1<UnityEngine.Application/LogCallback> UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper::<>f__am$cache1
	Action_1_t3760676225 * ___U3CU3Ef__amU24cache1_1;
	// System.Action`1<UnityEngine.Application/LogCallback> UniRx.Examples.Sample04_ConvertFromUnityCallback/LogHelper::<>f__am$cache2
	Action_1_t3760676225 * ___U3CU3Ef__amU24cache2_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LogHelper_t4028092818_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t1411985452 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t1411985452 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t1411985452 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LogHelper_t4028092818_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Action_1_t3760676225 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Action_1_t3760676225 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Action_1_t3760676225 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LogHelper_t4028092818_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Action_1_t3760676225 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Action_1_t3760676225 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Action_1_t3760676225 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGHELPER_T4028092818_H
#ifndef EVENTARGS_T3591816995_H
#define EVENTARGS_T3591816995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3591816995  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3591816995_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3591816995 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3591816995_StaticFields, ___Empty_0)); }
	inline EventArgs_t3591816995 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3591816995 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3591816995 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3591816995_H
#ifndef U3CTESTASYNCU3EC__ITERATOR0_T2949466201_H
#define U3CTESTASYNCU3EC__ITERATOR0_T2949466201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__Iterator0
struct  U3CTestAsyncU3Ec__Iterator0_t2949466201  : public RuntimeObject
{
public:
	// System.Object UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 UniRx.Examples.Sample10_MainThreadDispatcher/<TestAsync>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CTestAsyncU3Ec__Iterator0_t2949466201, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CTestAsyncU3Ec__Iterator0_t2949466201, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CTestAsyncU3Ec__Iterator0_t2949466201, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTESTASYNCU3EC__ITERATOR0_T2949466201_H
#ifndef SAMPLE10_MAINTHREADDISPATCHER_T3078734430_H
#define SAMPLE10_MAINTHREADDISPATCHER_T3078734430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample10_MainThreadDispatcher
struct  Sample10_MainThreadDispatcher_t3078734430  : public RuntimeObject
{
public:

public:
};

struct Sample10_MainThreadDispatcher_t3078734430_StaticFields
{
public:
	// System.Func`1<UniRx.Unit> UniRx.Examples.Sample10_MainThreadDispatcher::<>f__mg$cache0
	Func_1_t2791996114 * ___U3CU3Ef__mgU24cache0_0;
	// System.Action`1<System.Object> UniRx.Examples.Sample10_MainThreadDispatcher::<>f__am$cache0
	Action_1_t3252573759 * ___U3CU3Ef__amU24cache0_1;
	// System.Action`1<System.Int64> UniRx.Examples.Sample10_MainThreadDispatcher::<>f__am$cache1
	Action_1_t3909034899 * ___U3CU3Ef__amU24cache1_2;
	// System.Action`1<UniRx.Unit> UniRx.Examples.Sample10_MainThreadDispatcher::<>f__am$cache2
	Action_1_t3534717062 * ___U3CU3Ef__amU24cache2_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(Sample10_MainThreadDispatcher_t3078734430_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline Func_1_t2791996114 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline Func_1_t2791996114 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(Func_1_t2791996114 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(Sample10_MainThreadDispatcher_t3078734430_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Action_1_t3252573759 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Action_1_t3252573759 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Action_1_t3252573759 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_2() { return static_cast<int32_t>(offsetof(Sample10_MainThreadDispatcher_t3078734430_StaticFields, ___U3CU3Ef__amU24cache1_2)); }
	inline Action_1_t3909034899 * get_U3CU3Ef__amU24cache1_2() const { return ___U3CU3Ef__amU24cache1_2; }
	inline Action_1_t3909034899 ** get_address_of_U3CU3Ef__amU24cache1_2() { return &___U3CU3Ef__amU24cache1_2; }
	inline void set_U3CU3Ef__amU24cache1_2(Action_1_t3909034899 * value)
	{
		___U3CU3Ef__amU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_3() { return static_cast<int32_t>(offsetof(Sample10_MainThreadDispatcher_t3078734430_StaticFields, ___U3CU3Ef__amU24cache2_3)); }
	inline Action_1_t3534717062 * get_U3CU3Ef__amU24cache2_3() const { return ___U3CU3Ef__amU24cache2_3; }
	inline Action_1_t3534717062 ** get_address_of_U3CU3Ef__amU24cache2_3() { return &___U3CU3Ef__amU24cache2_3; }
	inline void set_U3CU3Ef__amU24cache2_3(Action_1_t3534717062 * value)
	{
		___U3CU3Ef__amU24cache2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE10_MAINTHREADDISPATCHER_T3078734430_H
#ifndef U3CSTARTU3EC__ANONSTOREY0_T4189425361_H
#define U3CSTARTU3EC__ANONSTOREY0_T4189425361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey0
struct  U3CStartU3Ec__AnonStorey0_t4189425361  : public RuntimeObject
{
public:
	// System.Action`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs> UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey0::h
	Action_1_t1105093476 * ___h_0;

public:
	inline static int32_t get_offset_of_h_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t4189425361, ___h_0)); }
	inline Action_1_t1105093476 * get_h_0() const { return ___h_0; }
	inline Action_1_t1105093476 ** get_address_of_h_0() { return &___h_0; }
	inline void set_h_0(Action_1_t1105093476 * value)
	{
		___h_0 = value;
		Il2CppCodeGenWriteBarrier((&___h_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ANONSTOREY0_T4189425361_H
#ifndef SAMPLE11_LOGGER_T748867482_H
#define SAMPLE11_LOGGER_T748867482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample11_Logger
struct  Sample11_Logger_t748867482  : public RuntimeObject
{
public:

public:
};

struct Sample11_Logger_t748867482_StaticFields
{
public:
	// UniRx.Diagnostics.Logger UniRx.Examples.Sample11_Logger::logger
	Logger_t3861635025 * ___logger_0;
	// System.Func`2<UniRx.Diagnostics.LogEntry,System.Boolean> UniRx.Examples.Sample11_Logger::<>f__am$cache0
	Func_2_t1305246854 * ___U3CU3Ef__amU24cache0_1;
	// System.Action`1<UniRx.Diagnostics.LogEntry> UniRx.Examples.Sample11_Logger::<>f__am$cache1
	Action_1_t1313974708 * ___U3CU3Ef__amU24cache1_2;

public:
	inline static int32_t get_offset_of_logger_0() { return static_cast<int32_t>(offsetof(Sample11_Logger_t748867482_StaticFields, ___logger_0)); }
	inline Logger_t3861635025 * get_logger_0() const { return ___logger_0; }
	inline Logger_t3861635025 ** get_address_of_logger_0() { return &___logger_0; }
	inline void set_logger_0(Logger_t3861635025 * value)
	{
		___logger_0 = value;
		Il2CppCodeGenWriteBarrier((&___logger_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(Sample11_Logger_t748867482_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t1305246854 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t1305246854 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t1305246854 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_2() { return static_cast<int32_t>(offsetof(Sample11_Logger_t748867482_StaticFields, ___U3CU3Ef__amU24cache1_2)); }
	inline Action_1_t1313974708 * get_U3CU3Ef__amU24cache1_2() const { return ___U3CU3Ef__amU24cache1_2; }
	inline Action_1_t1313974708 ** get_address_of_U3CU3Ef__amU24cache1_2() { return &___U3CU3Ef__amU24cache1_2; }
	inline void set_U3CU3Ef__amU24cache1_2(Action_1_t1313974708 * value)
	{
		___U3CU3Ef__amU24cache1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE11_LOGGER_T748867482_H
#ifndef U3CSTARTU3EC__ANONSTOREY1_T1460542006_H
#define U3CSTARTU3EC__ANONSTOREY1_T1460542006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey1
struct  U3CStartU3Ec__AnonStorey1_t1460542006  : public RuntimeObject
{
public:
	// System.Int32 UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey1::capture
	int32_t ___capture_0;
	// UniRx.Examples.Sample09_EventHandling UniRx.Examples.Sample09_EventHandling/<Start>c__AnonStorey1::$this
	Sample09_EventHandling_t917274147 * ___U24this_1;

public:
	inline static int32_t get_offset_of_capture_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey1_t1460542006, ___capture_0)); }
	inline int32_t get_capture_0() const { return ___capture_0; }
	inline int32_t* get_address_of_capture_0() { return &___capture_0; }
	inline void set_capture_0(int32_t value)
	{
		___capture_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey1_t1460542006, ___U24this_1)); }
	inline Sample09_EventHandling_t917274147 * get_U24this_1() const { return ___U24this_1; }
	inline Sample09_EventHandling_t917274147 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Sample09_EventHandling_t917274147 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ANONSTOREY1_T1460542006_H
#ifndef ENEMY_T2629818976_H
#define ENEMY_T2629818976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Enemy
struct  Enemy_t2629818976  : public RuntimeObject
{
public:
	// UniRx.IReactiveProperty`1<System.Int64> UniRx.Examples.Enemy::<CurrentHp>k__BackingField
	RuntimeObject* ___U3CCurrentHpU3Ek__BackingField_0;
	// UniRx.IReactiveProperty`1<System.Boolean> UniRx.Examples.Enemy::<IsDead>k__BackingField
	RuntimeObject* ___U3CIsDeadU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCurrentHpU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Enemy_t2629818976, ___U3CCurrentHpU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CCurrentHpU3Ek__BackingField_0() const { return ___U3CCurrentHpU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CCurrentHpU3Ek__BackingField_0() { return &___U3CCurrentHpU3Ek__BackingField_0; }
	inline void set_U3CCurrentHpU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CCurrentHpU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentHpU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIsDeadU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Enemy_t2629818976, ___U3CIsDeadU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CIsDeadU3Ek__BackingField_1() const { return ___U3CIsDeadU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CIsDeadU3Ek__BackingField_1() { return &___U3CIsDeadU3Ek__BackingField_1; }
	inline void set_U3CIsDeadU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CIsDeadU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIsDeadU3Ek__BackingField_1), value);
	}
};

struct Enemy_t2629818976_StaticFields
{
public:
	// System.Func`2<System.Int64,System.Boolean> UniRx.Examples.Enemy::<>f__am$cache0
	Func_2_t3474512971 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(Enemy_t2629818976_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t3474512971 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t3474512971 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t3474512971 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMY_T2629818976_H
#ifndef WEBREQUESTEXTENSIONS_T2028226060_H
#define WEBREQUESTEXTENSIONS_T2028226060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.WebRequestExtensions
struct  WebRequestExtensions_t2028226060  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTEXTENSIONS_T2028226060_H
#ifndef CANCELLATIONTOKEN_T1265546479_H
#define CANCELLATIONTOKEN_T1265546479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CancellationToken
struct  CancellationToken_t1265546479 
{
public:
	// UniRx.ICancelable UniRx.CancellationToken::source
	RuntimeObject* ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}
};

struct CancellationToken_t1265546479_StaticFields
{
public:
	// UniRx.CancellationToken UniRx.CancellationToken::Empty
	CancellationToken_t1265546479  ___Empty_1;
	// UniRx.CancellationToken UniRx.CancellationToken::None
	CancellationToken_t1265546479  ___None_2;

public:
	inline static int32_t get_offset_of_Empty_1() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479_StaticFields, ___Empty_1)); }
	inline CancellationToken_t1265546479  get_Empty_1() const { return ___Empty_1; }
	inline CancellationToken_t1265546479 * get_address_of_Empty_1() { return &___Empty_1; }
	inline void set_Empty_1(CancellationToken_t1265546479  value)
	{
		___Empty_1 = value;
	}

	inline static int32_t get_offset_of_None_2() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479_StaticFields, ___None_2)); }
	inline CancellationToken_t1265546479  get_None_2() const { return ___None_2; }
	inline CancellationToken_t1265546479 * get_address_of_None_2() { return &___None_2; }
	inline void set_None_2(CancellationToken_t1265546479  value)
	{
		___None_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UniRx.CancellationToken
struct CancellationToken_t1265546479_marshaled_pinvoke
{
	RuntimeObject* ___source_0;
};
// Native definition for COM marshalling of UniRx.CancellationToken
struct CancellationToken_t1265546479_marshaled_com
{
	RuntimeObject* ___source_0;
};
#endif // CANCELLATIONTOKEN_T1265546479_H
#ifndef TRINARY_T3668327564_H
#define TRINARY_T3668327564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.StableCompositeDisposable/Trinary
struct  Trinary_t3668327564  : public StableCompositeDisposable_t519652846
{
public:
	// System.Int32 UniRx.StableCompositeDisposable/Trinary::disposedCallCount
	int32_t ___disposedCallCount_0;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Trinary::_disposable1
	RuntimeObject* ____disposable1_1;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Trinary::_disposable2
	RuntimeObject* ____disposable2_2;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Trinary::_disposable3
	RuntimeObject* ____disposable3_3;

public:
	inline static int32_t get_offset_of_disposedCallCount_0() { return static_cast<int32_t>(offsetof(Trinary_t3668327564, ___disposedCallCount_0)); }
	inline int32_t get_disposedCallCount_0() const { return ___disposedCallCount_0; }
	inline int32_t* get_address_of_disposedCallCount_0() { return &___disposedCallCount_0; }
	inline void set_disposedCallCount_0(int32_t value)
	{
		___disposedCallCount_0 = value;
	}

	inline static int32_t get_offset_of__disposable1_1() { return static_cast<int32_t>(offsetof(Trinary_t3668327564, ____disposable1_1)); }
	inline RuntimeObject* get__disposable1_1() const { return ____disposable1_1; }
	inline RuntimeObject** get_address_of__disposable1_1() { return &____disposable1_1; }
	inline void set__disposable1_1(RuntimeObject* value)
	{
		____disposable1_1 = value;
		Il2CppCodeGenWriteBarrier((&____disposable1_1), value);
	}

	inline static int32_t get_offset_of__disposable2_2() { return static_cast<int32_t>(offsetof(Trinary_t3668327564, ____disposable2_2)); }
	inline RuntimeObject* get__disposable2_2() const { return ____disposable2_2; }
	inline RuntimeObject** get_address_of__disposable2_2() { return &____disposable2_2; }
	inline void set__disposable2_2(RuntimeObject* value)
	{
		____disposable2_2 = value;
		Il2CppCodeGenWriteBarrier((&____disposable2_2), value);
	}

	inline static int32_t get_offset_of__disposable3_3() { return static_cast<int32_t>(offsetof(Trinary_t3668327564, ____disposable3_3)); }
	inline RuntimeObject* get__disposable3_3() const { return ____disposable3_3; }
	inline RuntimeObject** get_address_of__disposable3_3() { return &____disposable3_3; }
	inline void set__disposable3_3(RuntimeObject* value)
	{
		____disposable3_3 = value;
		Il2CppCodeGenWriteBarrier((&____disposable3_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRINARY_T3668327564_H
#ifndef BINARY_T2304246015_H
#define BINARY_T2304246015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.StableCompositeDisposable/Binary
struct  Binary_t2304246015  : public StableCompositeDisposable_t519652846
{
public:
	// System.Int32 UniRx.StableCompositeDisposable/Binary::disposedCallCount
	int32_t ___disposedCallCount_0;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Binary::_disposable1
	RuntimeObject* ____disposable1_1;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Binary::_disposable2
	RuntimeObject* ____disposable2_2;

public:
	inline static int32_t get_offset_of_disposedCallCount_0() { return static_cast<int32_t>(offsetof(Binary_t2304246015, ___disposedCallCount_0)); }
	inline int32_t get_disposedCallCount_0() const { return ___disposedCallCount_0; }
	inline int32_t* get_address_of_disposedCallCount_0() { return &___disposedCallCount_0; }
	inline void set_disposedCallCount_0(int32_t value)
	{
		___disposedCallCount_0 = value;
	}

	inline static int32_t get_offset_of__disposable1_1() { return static_cast<int32_t>(offsetof(Binary_t2304246015, ____disposable1_1)); }
	inline RuntimeObject* get__disposable1_1() const { return ____disposable1_1; }
	inline RuntimeObject** get_address_of__disposable1_1() { return &____disposable1_1; }
	inline void set__disposable1_1(RuntimeObject* value)
	{
		____disposable1_1 = value;
		Il2CppCodeGenWriteBarrier((&____disposable1_1), value);
	}

	inline static int32_t get_offset_of__disposable2_2() { return static_cast<int32_t>(offsetof(Binary_t2304246015, ____disposable2_2)); }
	inline RuntimeObject* get__disposable2_2() const { return ____disposable2_2; }
	inline RuntimeObject** get_address_of__disposable2_2() { return &____disposable2_2; }
	inline void set__disposable2_2(RuntimeObject* value)
	{
		____disposable2_2 = value;
		Il2CppCodeGenWriteBarrier((&____disposable2_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARY_T2304246015_H
#ifndef NARYUNSAFE_T2030350842_H
#define NARYUNSAFE_T2030350842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.StableCompositeDisposable/NAryUnsafe
struct  NAryUnsafe_t2030350842  : public StableCompositeDisposable_t519652846
{
public:
	// System.Int32 UniRx.StableCompositeDisposable/NAryUnsafe::disposedCallCount
	int32_t ___disposedCallCount_0;
	// System.IDisposable[] modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/NAryUnsafe::_disposables
	IDisposableU5BU5D_t3584190570* ____disposables_1;

public:
	inline static int32_t get_offset_of_disposedCallCount_0() { return static_cast<int32_t>(offsetof(NAryUnsafe_t2030350842, ___disposedCallCount_0)); }
	inline int32_t get_disposedCallCount_0() const { return ___disposedCallCount_0; }
	inline int32_t* get_address_of_disposedCallCount_0() { return &___disposedCallCount_0; }
	inline void set_disposedCallCount_0(int32_t value)
	{
		___disposedCallCount_0 = value;
	}

	inline static int32_t get_offset_of__disposables_1() { return static_cast<int32_t>(offsetof(NAryUnsafe_t2030350842, ____disposables_1)); }
	inline IDisposableU5BU5D_t3584190570* get__disposables_1() const { return ____disposables_1; }
	inline IDisposableU5BU5D_t3584190570** get_address_of__disposables_1() { return &____disposables_1; }
	inline void set__disposables_1(IDisposableU5BU5D_t3584190570* value)
	{
		____disposables_1 = value;
		Il2CppCodeGenWriteBarrier((&____disposables_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NARYUNSAFE_T2030350842_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef MYEVENTARGS_T932625881_H
#define MYEVENTARGS_T932625881_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample09_EventHandling/MyEventArgs
struct  MyEventArgs_t932625881  : public EventArgs_t3591816995
{
public:
	// System.Int32 UniRx.Examples.Sample09_EventHandling/MyEventArgs::<MyProperty>k__BackingField
	int32_t ___U3CMyPropertyU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMyPropertyU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MyEventArgs_t932625881, ___U3CMyPropertyU3Ek__BackingField_1)); }
	inline int32_t get_U3CMyPropertyU3Ek__BackingField_1() const { return ___U3CMyPropertyU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CMyPropertyU3Ek__BackingField_1() { return &___U3CMyPropertyU3Ek__BackingField_1; }
	inline void set_U3CMyPropertyU3Ek__BackingField_1(int32_t value)
	{
		___U3CMyPropertyU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYEVENTARGS_T932625881_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef NARY_T2515936203_H
#define NARY_T2515936203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.StableCompositeDisposable/NAry
struct  NAry_t2515936203  : public StableCompositeDisposable_t519652846
{
public:
	// System.Int32 UniRx.StableCompositeDisposable/NAry::disposedCallCount
	int32_t ___disposedCallCount_0;
	// System.Collections.Generic.List`1<System.IDisposable> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/NAry::_disposables
	List_1_t817372929 * ____disposables_1;

public:
	inline static int32_t get_offset_of_disposedCallCount_0() { return static_cast<int32_t>(offsetof(NAry_t2515936203, ___disposedCallCount_0)); }
	inline int32_t get_disposedCallCount_0() const { return ___disposedCallCount_0; }
	inline int32_t* get_address_of_disposedCallCount_0() { return &___disposedCallCount_0; }
	inline void set_disposedCallCount_0(int32_t value)
	{
		___disposedCallCount_0 = value;
	}

	inline static int32_t get_offset_of__disposables_1() { return static_cast<int32_t>(offsetof(NAry_t2515936203, ____disposables_1)); }
	inline List_1_t817372929 * get__disposables_1() const { return ____disposables_1; }
	inline List_1_t817372929 ** get_address_of__disposables_1() { return &____disposables_1; }
	inline void set__disposables_1(List_1_t817372929 * value)
	{
		____disposables_1 = value;
		Il2CppCodeGenWriteBarrier((&____disposables_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NARY_T2515936203_H
#ifndef QUATERNARY_T846636199_H
#define QUATERNARY_T846636199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.StableCompositeDisposable/Quaternary
struct  Quaternary_t846636199  : public StableCompositeDisposable_t519652846
{
public:
	// System.Int32 UniRx.StableCompositeDisposable/Quaternary::disposedCallCount
	int32_t ___disposedCallCount_0;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Quaternary::_disposable1
	RuntimeObject* ____disposable1_1;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Quaternary::_disposable2
	RuntimeObject* ____disposable2_2;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Quaternary::_disposable3
	RuntimeObject* ____disposable3_3;
	// System.IDisposable modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.StableCompositeDisposable/Quaternary::_disposable4
	RuntimeObject* ____disposable4_4;

public:
	inline static int32_t get_offset_of_disposedCallCount_0() { return static_cast<int32_t>(offsetof(Quaternary_t846636199, ___disposedCallCount_0)); }
	inline int32_t get_disposedCallCount_0() const { return ___disposedCallCount_0; }
	inline int32_t* get_address_of_disposedCallCount_0() { return &___disposedCallCount_0; }
	inline void set_disposedCallCount_0(int32_t value)
	{
		___disposedCallCount_0 = value;
	}

	inline static int32_t get_offset_of__disposable1_1() { return static_cast<int32_t>(offsetof(Quaternary_t846636199, ____disposable1_1)); }
	inline RuntimeObject* get__disposable1_1() const { return ____disposable1_1; }
	inline RuntimeObject** get_address_of__disposable1_1() { return &____disposable1_1; }
	inline void set__disposable1_1(RuntimeObject* value)
	{
		____disposable1_1 = value;
		Il2CppCodeGenWriteBarrier((&____disposable1_1), value);
	}

	inline static int32_t get_offset_of__disposable2_2() { return static_cast<int32_t>(offsetof(Quaternary_t846636199, ____disposable2_2)); }
	inline RuntimeObject* get__disposable2_2() const { return ____disposable2_2; }
	inline RuntimeObject** get_address_of__disposable2_2() { return &____disposable2_2; }
	inline void set__disposable2_2(RuntimeObject* value)
	{
		____disposable2_2 = value;
		Il2CppCodeGenWriteBarrier((&____disposable2_2), value);
	}

	inline static int32_t get_offset_of__disposable3_3() { return static_cast<int32_t>(offsetof(Quaternary_t846636199, ____disposable3_3)); }
	inline RuntimeObject* get__disposable3_3() const { return ____disposable3_3; }
	inline RuntimeObject** get_address_of__disposable3_3() { return &____disposable3_3; }
	inline void set__disposable3_3(RuntimeObject* value)
	{
		____disposable3_3 = value;
		Il2CppCodeGenWriteBarrier((&____disposable3_3), value);
	}

	inline static int32_t get_offset_of__disposable4_4() { return static_cast<int32_t>(offsetof(Quaternary_t846636199, ____disposable4_4)); }
	inline RuntimeObject* get__disposable4_4() const { return ____disposable4_4; }
	inline RuntimeObject** get_address_of__disposable4_4() { return &____disposable4_4; }
	inline void set__disposable4_4(RuntimeObject* value)
	{
		____disposable4_4 = value;
		Il2CppCodeGenWriteBarrier((&____disposable4_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNARY_T846636199_H
#ifndef LOGTYPE_T73765434_H
#define LOGTYPE_T73765434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LogType
struct  LogType_t73765434 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogType_t73765434, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_T73765434_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef U3CGETWWWCOREU3EC__ITERATOR0_T1065486712_H
#define U3CGETWWWCOREU3EC__ITERATOR0_T1065486712_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator0
struct  U3CGetWWWCoreU3Ec__Iterator0_t1065486712  : public RuntimeObject
{
public:
	// System.String UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator0::url
	String_t* ___url_0;
	// UnityEngine.WWW UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// UniRx.CancellationToken UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator0::cancellationToken
	CancellationToken_t1265546479  ___cancellationToken_2;
	// UniRx.IObserver`1<System.String> UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator0::observer
	RuntimeObject* ___observer_3;
	// System.Object UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UniRx.Examples.Sample05_ConvertFromCoroutine/<GetWWWCore>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator0_t1065486712, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator0_t1065486712, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_cancellationToken_2() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator0_t1065486712, ___cancellationToken_2)); }
	inline CancellationToken_t1265546479  get_cancellationToken_2() const { return ___cancellationToken_2; }
	inline CancellationToken_t1265546479 * get_address_of_cancellationToken_2() { return &___cancellationToken_2; }
	inline void set_cancellationToken_2(CancellationToken_t1265546479  value)
	{
		___cancellationToken_2 = value;
	}

	inline static int32_t get_offset_of_observer_3() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator0_t1065486712, ___observer_3)); }
	inline RuntimeObject* get_observer_3() const { return ___observer_3; }
	inline RuntimeObject** get_address_of_observer_3() { return &___observer_3; }
	inline void set_observer_3(RuntimeObject* value)
	{
		___observer_3 = value;
		Il2CppCodeGenWriteBarrier((&___observer_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator0_t1065486712, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator0_t1065486712, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CGetWWWCoreU3Ec__Iterator0_t1065486712, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETWWWCOREU3EC__ITERATOR0_T1065486712_H
#ifndef OBSERVABLE_T2677550507_H
#define OBSERVABLE_T2677550507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable
struct  Observable_t2677550507  : public RuntimeObject
{
public:

public:
};

struct Observable_t2677550507_StaticFields
{
public:
	// System.TimeSpan UniRx.Observable::InfiniteTimeSpan
	TimeSpan_t881159249  ___InfiniteTimeSpan_0;
	// System.Collections.Generic.HashSet`1<System.Type> UniRx.Observable::YieldInstructionTypes
	HashSet_1_t1048894234 * ___YieldInstructionTypes_1;
	// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__mg$cache0
	Func_3_t1439315263 * ___U3CU3Ef__mgU24cache0_2;
	// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__mg$cache1
	Func_3_t1439315263 * ___U3CU3Ef__mgU24cache1_3;
	// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__mg$cache2
	Func_3_t1439315263 * ___U3CU3Ef__mgU24cache2_4;
	// System.Func`3<System.Int64,UniRx.Unit,System.Int64> UniRx.Observable::<>f__am$cache0
	Func_3_t2605432362 * ___U3CU3Ef__amU24cache0_5;
	// System.Func`3<System.Int64,UniRx.Unit,System.Int64> UniRx.Observable::<>f__am$cache1
	Func_3_t2605432362 * ___U3CU3Ef__amU24cache1_6;
	// System.Func`3<UniRx.IObserver`1<System.Int64>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__am$cache2
	Func_3_t1439315263 * ___U3CU3Ef__amU24cache2_7;
	// System.Func`3<UniRx.IObserver`1<UniRx.Unit>,UniRx.CancellationToken,System.Collections.IEnumerator> UniRx.Observable::<>f__mg$cache3
	Func_3_t2376649402 * ___U3CU3Ef__mgU24cache3_8;

public:
	inline static int32_t get_offset_of_InfiniteTimeSpan_0() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___InfiniteTimeSpan_0)); }
	inline TimeSpan_t881159249  get_InfiniteTimeSpan_0() const { return ___InfiniteTimeSpan_0; }
	inline TimeSpan_t881159249 * get_address_of_InfiniteTimeSpan_0() { return &___InfiniteTimeSpan_0; }
	inline void set_InfiniteTimeSpan_0(TimeSpan_t881159249  value)
	{
		___InfiniteTimeSpan_0 = value;
	}

	inline static int32_t get_offset_of_YieldInstructionTypes_1() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___YieldInstructionTypes_1)); }
	inline HashSet_1_t1048894234 * get_YieldInstructionTypes_1() const { return ___YieldInstructionTypes_1; }
	inline HashSet_1_t1048894234 ** get_address_of_YieldInstructionTypes_1() { return &___YieldInstructionTypes_1; }
	inline void set_YieldInstructionTypes_1(HashSet_1_t1048894234 * value)
	{
		___YieldInstructionTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&___YieldInstructionTypes_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_2() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__mgU24cache0_2)); }
	inline Func_3_t1439315263 * get_U3CU3Ef__mgU24cache0_2() const { return ___U3CU3Ef__mgU24cache0_2; }
	inline Func_3_t1439315263 ** get_address_of_U3CU3Ef__mgU24cache0_2() { return &___U3CU3Ef__mgU24cache0_2; }
	inline void set_U3CU3Ef__mgU24cache0_2(Func_3_t1439315263 * value)
	{
		___U3CU3Ef__mgU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_3() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__mgU24cache1_3)); }
	inline Func_3_t1439315263 * get_U3CU3Ef__mgU24cache1_3() const { return ___U3CU3Ef__mgU24cache1_3; }
	inline Func_3_t1439315263 ** get_address_of_U3CU3Ef__mgU24cache1_3() { return &___U3CU3Ef__mgU24cache1_3; }
	inline void set_U3CU3Ef__mgU24cache1_3(Func_3_t1439315263 * value)
	{
		___U3CU3Ef__mgU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache2_4() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__mgU24cache2_4)); }
	inline Func_3_t1439315263 * get_U3CU3Ef__mgU24cache2_4() const { return ___U3CU3Ef__mgU24cache2_4; }
	inline Func_3_t1439315263 ** get_address_of_U3CU3Ef__mgU24cache2_4() { return &___U3CU3Ef__mgU24cache2_4; }
	inline void set_U3CU3Ef__mgU24cache2_4(Func_3_t1439315263 * value)
	{
		___U3CU3Ef__mgU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline Func_3_t2605432362 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline Func_3_t2605432362 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(Func_3_t2605432362 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline Func_3_t2605432362 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline Func_3_t2605432362 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(Func_3_t2605432362 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_7() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__amU24cache2_7)); }
	inline Func_3_t1439315263 * get_U3CU3Ef__amU24cache2_7() const { return ___U3CU3Ef__amU24cache2_7; }
	inline Func_3_t1439315263 ** get_address_of_U3CU3Ef__amU24cache2_7() { return &___U3CU3Ef__amU24cache2_7; }
	inline void set_U3CU3Ef__amU24cache2_7(Func_3_t1439315263 * value)
	{
		___U3CU3Ef__amU24cache2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache3_8() { return static_cast<int32_t>(offsetof(Observable_t2677550507_StaticFields, ___U3CU3Ef__mgU24cache3_8)); }
	inline Func_3_t2376649402 * get_U3CU3Ef__mgU24cache3_8() const { return ___U3CU3Ef__mgU24cache3_8; }
	inline Func_3_t2376649402 ** get_address_of_U3CU3Ef__mgU24cache3_8() { return &___U3CU3Ef__mgU24cache3_8; }
	inline void set_U3CU3Ef__mgU24cache3_8(Func_3_t2376649402 * value)
	{
		___U3CU3Ef__mgU24cache3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache3_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLE_T2677550507_H
#ifndef EVERYAFTERUPDATEINVOKER_T1199703476_H
#define EVERYAFTERUPDATEINVOKER_T1199703476_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/EveryAfterUpdateInvoker
struct  EveryAfterUpdateInvoker_t1199703476  : public RuntimeObject
{
public:
	// System.Int64 UniRx.Observable/EveryAfterUpdateInvoker::count
	int64_t ___count_0;
	// UniRx.IObserver`1<System.Int64> UniRx.Observable/EveryAfterUpdateInvoker::observer
	RuntimeObject* ___observer_1;
	// UniRx.CancellationToken UniRx.Observable/EveryAfterUpdateInvoker::cancellationToken
	CancellationToken_t1265546479  ___cancellationToken_2;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(EveryAfterUpdateInvoker_t1199703476, ___count_0)); }
	inline int64_t get_count_0() const { return ___count_0; }
	inline int64_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int64_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_observer_1() { return static_cast<int32_t>(offsetof(EveryAfterUpdateInvoker_t1199703476, ___observer_1)); }
	inline RuntimeObject* get_observer_1() const { return ___observer_1; }
	inline RuntimeObject** get_address_of_observer_1() { return &___observer_1; }
	inline void set_observer_1(RuntimeObject* value)
	{
		___observer_1 = value;
		Il2CppCodeGenWriteBarrier((&___observer_1), value);
	}

	inline static int32_t get_offset_of_cancellationToken_2() { return static_cast<int32_t>(offsetof(EveryAfterUpdateInvoker_t1199703476, ___cancellationToken_2)); }
	inline CancellationToken_t1265546479  get_cancellationToken_2() const { return ___cancellationToken_2; }
	inline CancellationToken_t1265546479 * get_address_of_cancellationToken_2() { return &___cancellationToken_2; }
	inline void set_cancellationToken_2(CancellationToken_t1265546479  value)
	{
		___cancellationToken_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVERYAFTERUPDATEINVOKER_T1199703476_H
#ifndef U3CWRAPENUMERATORU3EC__ITERATOR2_T3733957475_H
#define U3CWRAPENUMERATORU3EC__ITERATOR2_T3733957475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<WrapEnumerator>c__Iterator2
struct  U3CWrapEnumeratorU3Ec__Iterator2_t3733957475  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Observable/<WrapEnumerator>c__Iterator2::<hasNext>__0
	bool ___U3ChasNextU3E__0_0;
	// System.Boolean UniRx.Observable/<WrapEnumerator>c__Iterator2::<raisedError>__0
	bool ___U3CraisedErrorU3E__0_1;
	// System.Collections.IEnumerator UniRx.Observable/<WrapEnumerator>c__Iterator2::enumerator
	RuntimeObject* ___enumerator_2;
	// UniRx.IObserver`1<UniRx.Unit> UniRx.Observable/<WrapEnumerator>c__Iterator2::observer
	RuntimeObject* ___observer_3;
	// System.Boolean UniRx.Observable/<WrapEnumerator>c__Iterator2::publishEveryYield
	bool ___publishEveryYield_4;
	// System.Object UniRx.Observable/<WrapEnumerator>c__Iterator2::<current>__1
	RuntimeObject * ___U3CcurrentU3E__1_5;
	// UniRx.ICustomYieldInstructionErrorHandler UniRx.Observable/<WrapEnumerator>c__Iterator2::<customHandler>__1
	RuntimeObject* ___U3CcustomHandlerU3E__1_6;
	// UniRx.CancellationToken UniRx.Observable/<WrapEnumerator>c__Iterator2::cancellationToken
	CancellationToken_t1265546479  ___cancellationToken_7;
	// System.Object UniRx.Observable/<WrapEnumerator>c__Iterator2::$current
	RuntimeObject * ___U24current_8;
	// System.Boolean UniRx.Observable/<WrapEnumerator>c__Iterator2::$disposing
	bool ___U24disposing_9;
	// System.Int32 UniRx.Observable/<WrapEnumerator>c__Iterator2::$PC
	int32_t ___U24PC_10;

public:
	inline static int32_t get_offset_of_U3ChasNextU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__Iterator2_t3733957475, ___U3ChasNextU3E__0_0)); }
	inline bool get_U3ChasNextU3E__0_0() const { return ___U3ChasNextU3E__0_0; }
	inline bool* get_address_of_U3ChasNextU3E__0_0() { return &___U3ChasNextU3E__0_0; }
	inline void set_U3ChasNextU3E__0_0(bool value)
	{
		___U3ChasNextU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CraisedErrorU3E__0_1() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__Iterator2_t3733957475, ___U3CraisedErrorU3E__0_1)); }
	inline bool get_U3CraisedErrorU3E__0_1() const { return ___U3CraisedErrorU3E__0_1; }
	inline bool* get_address_of_U3CraisedErrorU3E__0_1() { return &___U3CraisedErrorU3E__0_1; }
	inline void set_U3CraisedErrorU3E__0_1(bool value)
	{
		___U3CraisedErrorU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_enumerator_2() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__Iterator2_t3733957475, ___enumerator_2)); }
	inline RuntimeObject* get_enumerator_2() const { return ___enumerator_2; }
	inline RuntimeObject** get_address_of_enumerator_2() { return &___enumerator_2; }
	inline void set_enumerator_2(RuntimeObject* value)
	{
		___enumerator_2 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_2), value);
	}

	inline static int32_t get_offset_of_observer_3() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__Iterator2_t3733957475, ___observer_3)); }
	inline RuntimeObject* get_observer_3() const { return ___observer_3; }
	inline RuntimeObject** get_address_of_observer_3() { return &___observer_3; }
	inline void set_observer_3(RuntimeObject* value)
	{
		___observer_3 = value;
		Il2CppCodeGenWriteBarrier((&___observer_3), value);
	}

	inline static int32_t get_offset_of_publishEveryYield_4() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__Iterator2_t3733957475, ___publishEveryYield_4)); }
	inline bool get_publishEveryYield_4() const { return ___publishEveryYield_4; }
	inline bool* get_address_of_publishEveryYield_4() { return &___publishEveryYield_4; }
	inline void set_publishEveryYield_4(bool value)
	{
		___publishEveryYield_4 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentU3E__1_5() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__Iterator2_t3733957475, ___U3CcurrentU3E__1_5)); }
	inline RuntimeObject * get_U3CcurrentU3E__1_5() const { return ___U3CcurrentU3E__1_5; }
	inline RuntimeObject ** get_address_of_U3CcurrentU3E__1_5() { return &___U3CcurrentU3E__1_5; }
	inline void set_U3CcurrentU3E__1_5(RuntimeObject * value)
	{
		___U3CcurrentU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcurrentU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U3CcustomHandlerU3E__1_6() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__Iterator2_t3733957475, ___U3CcustomHandlerU3E__1_6)); }
	inline RuntimeObject* get_U3CcustomHandlerU3E__1_6() const { return ___U3CcustomHandlerU3E__1_6; }
	inline RuntimeObject** get_address_of_U3CcustomHandlerU3E__1_6() { return &___U3CcustomHandlerU3E__1_6; }
	inline void set_U3CcustomHandlerU3E__1_6(RuntimeObject* value)
	{
		___U3CcustomHandlerU3E__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcustomHandlerU3E__1_6), value);
	}

	inline static int32_t get_offset_of_cancellationToken_7() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__Iterator2_t3733957475, ___cancellationToken_7)); }
	inline CancellationToken_t1265546479  get_cancellationToken_7() const { return ___cancellationToken_7; }
	inline CancellationToken_t1265546479 * get_address_of_cancellationToken_7() { return &___cancellationToken_7; }
	inline void set_cancellationToken_7(CancellationToken_t1265546479  value)
	{
		___cancellationToken_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__Iterator2_t3733957475, ___U24current_8)); }
	inline RuntimeObject * get_U24current_8() const { return ___U24current_8; }
	inline RuntimeObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(RuntimeObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_8), value);
	}

	inline static int32_t get_offset_of_U24disposing_9() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__Iterator2_t3733957475, ___U24disposing_9)); }
	inline bool get_U24disposing_9() const { return ___U24disposing_9; }
	inline bool* get_address_of_U24disposing_9() { return &___U24disposing_9; }
	inline void set_U24disposing_9(bool value)
	{
		___U24disposing_9 = value;
	}

	inline static int32_t get_offset_of_U24PC_10() { return static_cast<int32_t>(offsetof(U3CWrapEnumeratorU3Ec__Iterator2_t3733957475, ___U24PC_10)); }
	inline int32_t get_U24PC_10() const { return ___U24PC_10; }
	inline int32_t* get_address_of_U24PC_10() { return &___U24PC_10; }
	inline void set_U24PC_10(int32_t value)
	{
		___U24PC_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWRAPENUMERATORU3EC__ITERATOR2_T3733957475_H
#ifndef U3CWRAPTOCANCELLABLEENUMERATORU3EC__ITERATOR4_T1899200075_H
#define U3CWRAPTOCANCELLABLEENUMERATORU3EC__ITERATOR4_T1899200075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<WrapToCancellableEnumerator>c__Iterator4
struct  U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Observable/<WrapToCancellableEnumerator>c__Iterator4::<hasNext>__0
	bool ___U3ChasNextU3E__0_0;
	// System.Collections.IEnumerator UniRx.Observable/<WrapToCancellableEnumerator>c__Iterator4::enumerator
	RuntimeObject* ___enumerator_1;
	// UniRx.CancellationToken UniRx.Observable/<WrapToCancellableEnumerator>c__Iterator4::cancellationToken
	CancellationToken_t1265546479  ___cancellationToken_2;
	// System.Object UniRx.Observable/<WrapToCancellableEnumerator>c__Iterator4::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UniRx.Observable/<WrapToCancellableEnumerator>c__Iterator4::$disposing
	bool ___U24disposing_4;
	// System.Int32 UniRx.Observable/<WrapToCancellableEnumerator>c__Iterator4::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3ChasNextU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075, ___U3ChasNextU3E__0_0)); }
	inline bool get_U3ChasNextU3E__0_0() const { return ___U3ChasNextU3E__0_0; }
	inline bool* get_address_of_U3ChasNextU3E__0_0() { return &___U3ChasNextU3E__0_0; }
	inline void set_U3ChasNextU3E__0_0(bool value)
	{
		___U3ChasNextU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_enumerator_1() { return static_cast<int32_t>(offsetof(U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075, ___enumerator_1)); }
	inline RuntimeObject* get_enumerator_1() const { return ___enumerator_1; }
	inline RuntimeObject** get_address_of_enumerator_1() { return &___enumerator_1; }
	inline void set_enumerator_1(RuntimeObject* value)
	{
		___enumerator_1 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_1), value);
	}

	inline static int32_t get_offset_of_cancellationToken_2() { return static_cast<int32_t>(offsetof(U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075, ___cancellationToken_2)); }
	inline CancellationToken_t1265546479  get_cancellationToken_2() const { return ___cancellationToken_2; }
	inline CancellationToken_t1265546479 * get_address_of_cancellationToken_2() { return &___cancellationToken_2; }
	inline void set_cancellationToken_2(CancellationToken_t1265546479  value)
	{
		___cancellationToken_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWRAPTOCANCELLABLEENUMERATORU3EC__ITERATOR4_T1899200075_H
#ifndef U3CEVERYCYCLECOREU3EC__ITERATOR5_T1540769718_H
#define U3CEVERYCYCLECOREU3EC__ITERATOR5_T1540769718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<EveryCycleCore>c__Iterator5
struct  U3CEveryCycleCoreU3Ec__Iterator5_t1540769718  : public RuntimeObject
{
public:
	// UniRx.CancellationToken UniRx.Observable/<EveryCycleCore>c__Iterator5::cancellationToken
	CancellationToken_t1265546479  ___cancellationToken_0;
	// System.Int64 UniRx.Observable/<EveryCycleCore>c__Iterator5::<count>__0
	int64_t ___U3CcountU3E__0_1;
	// UniRx.IObserver`1<System.Int64> UniRx.Observable/<EveryCycleCore>c__Iterator5::observer
	RuntimeObject* ___observer_2;
	// System.Object UniRx.Observable/<EveryCycleCore>c__Iterator5::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean UniRx.Observable/<EveryCycleCore>c__Iterator5::$disposing
	bool ___U24disposing_4;
	// System.Int32 UniRx.Observable/<EveryCycleCore>c__Iterator5::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_cancellationToken_0() { return static_cast<int32_t>(offsetof(U3CEveryCycleCoreU3Ec__Iterator5_t1540769718, ___cancellationToken_0)); }
	inline CancellationToken_t1265546479  get_cancellationToken_0() const { return ___cancellationToken_0; }
	inline CancellationToken_t1265546479 * get_address_of_cancellationToken_0() { return &___cancellationToken_0; }
	inline void set_cancellationToken_0(CancellationToken_t1265546479  value)
	{
		___cancellationToken_0 = value;
	}

	inline static int32_t get_offset_of_U3CcountU3E__0_1() { return static_cast<int32_t>(offsetof(U3CEveryCycleCoreU3Ec__Iterator5_t1540769718, ___U3CcountU3E__0_1)); }
	inline int64_t get_U3CcountU3E__0_1() const { return ___U3CcountU3E__0_1; }
	inline int64_t* get_address_of_U3CcountU3E__0_1() { return &___U3CcountU3E__0_1; }
	inline void set_U3CcountU3E__0_1(int64_t value)
	{
		___U3CcountU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_observer_2() { return static_cast<int32_t>(offsetof(U3CEveryCycleCoreU3Ec__Iterator5_t1540769718, ___observer_2)); }
	inline RuntimeObject* get_observer_2() const { return ___observer_2; }
	inline RuntimeObject** get_address_of_observer_2() { return &___observer_2; }
	inline void set_observer_2(RuntimeObject* value)
	{
		___observer_2 = value;
		Il2CppCodeGenWriteBarrier((&___observer_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CEveryCycleCoreU3Ec__Iterator5_t1540769718, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CEveryCycleCoreU3Ec__Iterator5_t1540769718, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CEveryCycleCoreU3Ec__Iterator5_t1540769718, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEVERYCYCLECOREU3EC__ITERATOR5_T1540769718_H
#ifndef U3CNEXTFRAMECOREU3EC__ITERATOR6_T77210722_H
#define U3CNEXTFRAMECOREU3EC__ITERATOR6_T77210722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<NextFrameCore>c__Iterator6
struct  U3CNextFrameCoreU3Ec__Iterator6_t77210722  : public RuntimeObject
{
public:
	// UniRx.CancellationToken UniRx.Observable/<NextFrameCore>c__Iterator6::cancellation
	CancellationToken_t1265546479  ___cancellation_0;
	// UniRx.IObserver`1<UniRx.Unit> UniRx.Observable/<NextFrameCore>c__Iterator6::observer
	RuntimeObject* ___observer_1;
	// System.Object UniRx.Observable/<NextFrameCore>c__Iterator6::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean UniRx.Observable/<NextFrameCore>c__Iterator6::$disposing
	bool ___U24disposing_3;
	// System.Int32 UniRx.Observable/<NextFrameCore>c__Iterator6::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_cancellation_0() { return static_cast<int32_t>(offsetof(U3CNextFrameCoreU3Ec__Iterator6_t77210722, ___cancellation_0)); }
	inline CancellationToken_t1265546479  get_cancellation_0() const { return ___cancellation_0; }
	inline CancellationToken_t1265546479 * get_address_of_cancellation_0() { return &___cancellation_0; }
	inline void set_cancellation_0(CancellationToken_t1265546479  value)
	{
		___cancellation_0 = value;
	}

	inline static int32_t get_offset_of_observer_1() { return static_cast<int32_t>(offsetof(U3CNextFrameCoreU3Ec__Iterator6_t77210722, ___observer_1)); }
	inline RuntimeObject* get_observer_1() const { return ___observer_1; }
	inline RuntimeObject** get_address_of_observer_1() { return &___observer_1; }
	inline void set_observer_1(RuntimeObject* value)
	{
		___observer_1 = value;
		Il2CppCodeGenWriteBarrier((&___observer_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CNextFrameCoreU3Ec__Iterator6_t77210722, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CNextFrameCoreU3Ec__Iterator6_t77210722, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CNextFrameCoreU3Ec__Iterator6_t77210722, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CNEXTFRAMECOREU3EC__ITERATOR6_T77210722_H
#ifndef U3CTIMERFRAMECOREU3EC__ITERATOR7_T1077573192_H
#define U3CTIMERFRAMECOREU3EC__ITERATOR7_T1077573192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<TimerFrameCore>c__Iterator7
struct  U3CTimerFrameCoreU3Ec__Iterator7_t1077573192  : public RuntimeObject
{
public:
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator7::dueTimeFrameCount
	int32_t ___dueTimeFrameCount_0;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator7::<currentFrame>__0
	int32_t ___U3CcurrentFrameU3E__0_1;
	// UniRx.CancellationToken UniRx.Observable/<TimerFrameCore>c__Iterator7::cancel
	CancellationToken_t1265546479  ___cancel_2;
	// UniRx.IObserver`1<System.Int64> UniRx.Observable/<TimerFrameCore>c__Iterator7::observer
	RuntimeObject* ___observer_3;
	// System.Object UniRx.Observable/<TimerFrameCore>c__Iterator7::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UniRx.Observable/<TimerFrameCore>c__Iterator7::$disposing
	bool ___U24disposing_5;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator7::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_dueTimeFrameCount_0() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator7_t1077573192, ___dueTimeFrameCount_0)); }
	inline int32_t get_dueTimeFrameCount_0() const { return ___dueTimeFrameCount_0; }
	inline int32_t* get_address_of_dueTimeFrameCount_0() { return &___dueTimeFrameCount_0; }
	inline void set_dueTimeFrameCount_0(int32_t value)
	{
		___dueTimeFrameCount_0 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentFrameU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator7_t1077573192, ___U3CcurrentFrameU3E__0_1)); }
	inline int32_t get_U3CcurrentFrameU3E__0_1() const { return ___U3CcurrentFrameU3E__0_1; }
	inline int32_t* get_address_of_U3CcurrentFrameU3E__0_1() { return &___U3CcurrentFrameU3E__0_1; }
	inline void set_U3CcurrentFrameU3E__0_1(int32_t value)
	{
		___U3CcurrentFrameU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_cancel_2() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator7_t1077573192, ___cancel_2)); }
	inline CancellationToken_t1265546479  get_cancel_2() const { return ___cancel_2; }
	inline CancellationToken_t1265546479 * get_address_of_cancel_2() { return &___cancel_2; }
	inline void set_cancel_2(CancellationToken_t1265546479  value)
	{
		___cancel_2 = value;
	}

	inline static int32_t get_offset_of_observer_3() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator7_t1077573192, ___observer_3)); }
	inline RuntimeObject* get_observer_3() const { return ___observer_3; }
	inline RuntimeObject** get_address_of_observer_3() { return &___observer_3; }
	inline void set_observer_3(RuntimeObject* value)
	{
		___observer_3 = value;
		Il2CppCodeGenWriteBarrier((&___observer_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator7_t1077573192, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator7_t1077573192, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator7_t1077573192, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMERFRAMECOREU3EC__ITERATOR7_T1077573192_H
#ifndef U3CTIMERFRAMECOREU3EC__ITERATOR8_T1077573207_H
#define U3CTIMERFRAMECOREU3EC__ITERATOR8_T1077573207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Observable/<TimerFrameCore>c__Iterator8
struct  U3CTimerFrameCoreU3Ec__Iterator8_t1077573207  : public RuntimeObject
{
public:
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator8::dueTimeFrameCount
	int32_t ___dueTimeFrameCount_0;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator8::periodFrameCount
	int32_t ___periodFrameCount_1;
	// System.Int64 UniRx.Observable/<TimerFrameCore>c__Iterator8::<sendCount>__0
	int64_t ___U3CsendCountU3E__0_2;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator8::<currentFrame>__0
	int32_t ___U3CcurrentFrameU3E__0_3;
	// UniRx.CancellationToken UniRx.Observable/<TimerFrameCore>c__Iterator8::cancel
	CancellationToken_t1265546479  ___cancel_4;
	// UniRx.IObserver`1<System.Int64> UniRx.Observable/<TimerFrameCore>c__Iterator8::observer
	RuntimeObject* ___observer_5;
	// System.Object UniRx.Observable/<TimerFrameCore>c__Iterator8::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean UniRx.Observable/<TimerFrameCore>c__Iterator8::$disposing
	bool ___U24disposing_7;
	// System.Int32 UniRx.Observable/<TimerFrameCore>c__Iterator8::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_dueTimeFrameCount_0() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator8_t1077573207, ___dueTimeFrameCount_0)); }
	inline int32_t get_dueTimeFrameCount_0() const { return ___dueTimeFrameCount_0; }
	inline int32_t* get_address_of_dueTimeFrameCount_0() { return &___dueTimeFrameCount_0; }
	inline void set_dueTimeFrameCount_0(int32_t value)
	{
		___dueTimeFrameCount_0 = value;
	}

	inline static int32_t get_offset_of_periodFrameCount_1() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator8_t1077573207, ___periodFrameCount_1)); }
	inline int32_t get_periodFrameCount_1() const { return ___periodFrameCount_1; }
	inline int32_t* get_address_of_periodFrameCount_1() { return &___periodFrameCount_1; }
	inline void set_periodFrameCount_1(int32_t value)
	{
		___periodFrameCount_1 = value;
	}

	inline static int32_t get_offset_of_U3CsendCountU3E__0_2() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator8_t1077573207, ___U3CsendCountU3E__0_2)); }
	inline int64_t get_U3CsendCountU3E__0_2() const { return ___U3CsendCountU3E__0_2; }
	inline int64_t* get_address_of_U3CsendCountU3E__0_2() { return &___U3CsendCountU3E__0_2; }
	inline void set_U3CsendCountU3E__0_2(int64_t value)
	{
		___U3CsendCountU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentFrameU3E__0_3() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator8_t1077573207, ___U3CcurrentFrameU3E__0_3)); }
	inline int32_t get_U3CcurrentFrameU3E__0_3() const { return ___U3CcurrentFrameU3E__0_3; }
	inline int32_t* get_address_of_U3CcurrentFrameU3E__0_3() { return &___U3CcurrentFrameU3E__0_3; }
	inline void set_U3CcurrentFrameU3E__0_3(int32_t value)
	{
		___U3CcurrentFrameU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_cancel_4() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator8_t1077573207, ___cancel_4)); }
	inline CancellationToken_t1265546479  get_cancel_4() const { return ___cancel_4; }
	inline CancellationToken_t1265546479 * get_address_of_cancel_4() { return &___cancel_4; }
	inline void set_cancel_4(CancellationToken_t1265546479  value)
	{
		___cancel_4 = value;
	}

	inline static int32_t get_offset_of_observer_5() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator8_t1077573207, ___observer_5)); }
	inline RuntimeObject* get_observer_5() const { return ___observer_5; }
	inline RuntimeObject** get_address_of_observer_5() { return &___observer_5; }
	inline void set_observer_5(RuntimeObject* value)
	{
		___observer_5 = value;
		Il2CppCodeGenWriteBarrier((&___observer_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator8_t1077573207, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator8_t1077573207, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CTimerFrameCoreU3Ec__Iterator8_t1077573207, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTIMERFRAMECOREU3EC__ITERATOR8_T1077573207_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef LOGCALLBACK_T2567365653_H
#define LOGCALLBACK_T2567365653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback
struct  LogCallback_t2567365653  : public RuntimeObject
{
public:
	// System.String UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback::Condition
	String_t* ___Condition_0;
	// System.String UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback::StackTrace
	String_t* ___StackTrace_1;
	// UnityEngine.LogType UniRx.Examples.Sample04_ConvertFromUnityCallback/LogCallback::LogType
	int32_t ___LogType_2;

public:
	inline static int32_t get_offset_of_Condition_0() { return static_cast<int32_t>(offsetof(LogCallback_t2567365653, ___Condition_0)); }
	inline String_t* get_Condition_0() const { return ___Condition_0; }
	inline String_t** get_address_of_Condition_0() { return &___Condition_0; }
	inline void set_Condition_0(String_t* value)
	{
		___Condition_0 = value;
		Il2CppCodeGenWriteBarrier((&___Condition_0), value);
	}

	inline static int32_t get_offset_of_StackTrace_1() { return static_cast<int32_t>(offsetof(LogCallback_t2567365653, ___StackTrace_1)); }
	inline String_t* get_StackTrace_1() const { return ___StackTrace_1; }
	inline String_t** get_address_of_StackTrace_1() { return &___StackTrace_1; }
	inline void set_StackTrace_1(String_t* value)
	{
		___StackTrace_1 = value;
		Il2CppCodeGenWriteBarrier((&___StackTrace_1), value);
	}

	inline static int32_t get_offset_of_LogType_2() { return static_cast<int32_t>(offsetof(LogCallback_t2567365653, ___LogType_2)); }
	inline int32_t get_LogType_2() const { return ___LogType_2; }
	inline int32_t* get_address_of_LogType_2() { return &___LogType_2; }
	inline void set_LogType_2(int32_t value)
	{
		___LogType_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGCALLBACK_T2567365653_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SAMPLE12_REACTIVEPROPERTY_T1497491507_H
#define SAMPLE12_REACTIVEPROPERTY_T1497491507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample12_ReactiveProperty
struct  Sample12_ReactiveProperty_t1497491507  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button UniRx.Examples.Sample12_ReactiveProperty::MyButton
	Button_t4055032469 * ___MyButton_2;
	// UnityEngine.UI.Toggle UniRx.Examples.Sample12_ReactiveProperty::MyToggle
	Toggle_t2735377061 * ___MyToggle_3;
	// UnityEngine.UI.InputField UniRx.Examples.Sample12_ReactiveProperty::MyInput
	InputField_t3762917431 * ___MyInput_4;
	// UnityEngine.UI.Text UniRx.Examples.Sample12_ReactiveProperty::MyText
	Text_t1901882714 * ___MyText_5;
	// UnityEngine.UI.Slider UniRx.Examples.Sample12_ReactiveProperty::MySlider
	Slider_t3903728902 * ___MySlider_6;
	// UniRx.IntReactiveProperty UniRx.Examples.Sample12_ReactiveProperty::IntRxProp
	IntReactiveProperty_t3243525985 * ___IntRxProp_7;
	// UniRx.Examples.Enemy UniRx.Examples.Sample12_ReactiveProperty::enemy
	Enemy_t2629818976 * ___enemy_8;

public:
	inline static int32_t get_offset_of_MyButton_2() { return static_cast<int32_t>(offsetof(Sample12_ReactiveProperty_t1497491507, ___MyButton_2)); }
	inline Button_t4055032469 * get_MyButton_2() const { return ___MyButton_2; }
	inline Button_t4055032469 ** get_address_of_MyButton_2() { return &___MyButton_2; }
	inline void set_MyButton_2(Button_t4055032469 * value)
	{
		___MyButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___MyButton_2), value);
	}

	inline static int32_t get_offset_of_MyToggle_3() { return static_cast<int32_t>(offsetof(Sample12_ReactiveProperty_t1497491507, ___MyToggle_3)); }
	inline Toggle_t2735377061 * get_MyToggle_3() const { return ___MyToggle_3; }
	inline Toggle_t2735377061 ** get_address_of_MyToggle_3() { return &___MyToggle_3; }
	inline void set_MyToggle_3(Toggle_t2735377061 * value)
	{
		___MyToggle_3 = value;
		Il2CppCodeGenWriteBarrier((&___MyToggle_3), value);
	}

	inline static int32_t get_offset_of_MyInput_4() { return static_cast<int32_t>(offsetof(Sample12_ReactiveProperty_t1497491507, ___MyInput_4)); }
	inline InputField_t3762917431 * get_MyInput_4() const { return ___MyInput_4; }
	inline InputField_t3762917431 ** get_address_of_MyInput_4() { return &___MyInput_4; }
	inline void set_MyInput_4(InputField_t3762917431 * value)
	{
		___MyInput_4 = value;
		Il2CppCodeGenWriteBarrier((&___MyInput_4), value);
	}

	inline static int32_t get_offset_of_MyText_5() { return static_cast<int32_t>(offsetof(Sample12_ReactiveProperty_t1497491507, ___MyText_5)); }
	inline Text_t1901882714 * get_MyText_5() const { return ___MyText_5; }
	inline Text_t1901882714 ** get_address_of_MyText_5() { return &___MyText_5; }
	inline void set_MyText_5(Text_t1901882714 * value)
	{
		___MyText_5 = value;
		Il2CppCodeGenWriteBarrier((&___MyText_5), value);
	}

	inline static int32_t get_offset_of_MySlider_6() { return static_cast<int32_t>(offsetof(Sample12_ReactiveProperty_t1497491507, ___MySlider_6)); }
	inline Slider_t3903728902 * get_MySlider_6() const { return ___MySlider_6; }
	inline Slider_t3903728902 ** get_address_of_MySlider_6() { return &___MySlider_6; }
	inline void set_MySlider_6(Slider_t3903728902 * value)
	{
		___MySlider_6 = value;
		Il2CppCodeGenWriteBarrier((&___MySlider_6), value);
	}

	inline static int32_t get_offset_of_IntRxProp_7() { return static_cast<int32_t>(offsetof(Sample12_ReactiveProperty_t1497491507, ___IntRxProp_7)); }
	inline IntReactiveProperty_t3243525985 * get_IntRxProp_7() const { return ___IntRxProp_7; }
	inline IntReactiveProperty_t3243525985 ** get_address_of_IntRxProp_7() { return &___IntRxProp_7; }
	inline void set_IntRxProp_7(IntReactiveProperty_t3243525985 * value)
	{
		___IntRxProp_7 = value;
		Il2CppCodeGenWriteBarrier((&___IntRxProp_7), value);
	}

	inline static int32_t get_offset_of_enemy_8() { return static_cast<int32_t>(offsetof(Sample12_ReactiveProperty_t1497491507, ___enemy_8)); }
	inline Enemy_t2629818976 * get_enemy_8() const { return ___enemy_8; }
	inline Enemy_t2629818976 ** get_address_of_enemy_8() { return &___enemy_8; }
	inline void set_enemy_8(Enemy_t2629818976 * value)
	{
		___enemy_8 = value;
		Il2CppCodeGenWriteBarrier((&___enemy_8), value);
	}
};

struct Sample12_ReactiveProperty_t1497491507_StaticFields
{
public:
	// System.Func`2<System.String,System.Boolean> UniRx.Examples.Sample12_ReactiveProperty::<>f__am$cache0
	Func_2_t2197129486 * ___U3CU3Ef__amU24cache0_9;
	// System.Func`2<System.Single,System.String> UniRx.Examples.Sample12_ReactiveProperty::<>f__am$cache1
	Func_2_t4110414121 * ___U3CU3Ef__amU24cache1_10;
	// System.Func`2<System.Boolean,System.Boolean> UniRx.Examples.Sample12_ReactiveProperty::<>f__am$cache2
	Func_2_t3812758338 * ___U3CU3Ef__amU24cache2_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(Sample12_ReactiveProperty_t1497491507_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Func_2_t2197129486 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Func_2_t2197129486 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Func_2_t2197129486 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(Sample12_ReactiveProperty_t1497491507_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Func_2_t4110414121 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Func_2_t4110414121 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Func_2_t4110414121 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_11() { return static_cast<int32_t>(offsetof(Sample12_ReactiveProperty_t1497491507_StaticFields, ___U3CU3Ef__amU24cache2_11)); }
	inline Func_2_t3812758338 * get_U3CU3Ef__amU24cache2_11() const { return ___U3CU3Ef__amU24cache2_11; }
	inline Func_2_t3812758338 ** get_address_of_U3CU3Ef__amU24cache2_11() { return &___U3CU3Ef__amU24cache2_11; }
	inline void set_U3CU3Ef__amU24cache2_11(Func_2_t3812758338 * value)
	{
		___U3CU3Ef__amU24cache2_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE12_REACTIVEPROPERTY_T1497491507_H
#ifndef SAMPLE06_CONVERTTOCOROUTINE_T3751354639_H
#define SAMPLE06_CONVERTTOCOROUTINE_T3751354639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample06_ConvertToCoroutine
struct  Sample06_ConvertToCoroutine_t3751354639  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE06_CONVERTTOCOROUTINE_T3751354639_H
#ifndef SAMPLE07_ORCHESTRATIENUMERATOR_T3797225793_H
#define SAMPLE07_ORCHESTRATIENUMERATOR_T3797225793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample07_OrchestratIEnumerator
struct  Sample07_OrchestratIEnumerator_t3797225793  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE07_ORCHESTRATIENUMERATOR_T3797225793_H
#ifndef SAMPLE08_DETECTDOUBLECLICK_T3898043094_H
#define SAMPLE08_DETECTDOUBLECLICK_T3898043094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample08_DetectDoubleClick
struct  Sample08_DetectDoubleClick_t3898043094  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Sample08_DetectDoubleClick_t3898043094_StaticFields
{
public:
	// System.Func`2<System.Int64,System.Boolean> UniRx.Examples.Sample08_DetectDoubleClick::<>f__am$cache0
	Func_2_t3474512971 * ___U3CU3Ef__amU24cache0_2;
	// System.Func`2<System.Collections.Generic.IList`1<System.Int64>,System.Boolean> UniRx.Examples.Sample08_DetectDoubleClick::<>f__am$cache1
	Func_2_t2970184680 * ___U3CU3Ef__amU24cache1_3;
	// System.Action`1<System.Collections.Generic.IList`1<System.Int64>> UniRx.Examples.Sample08_DetectDoubleClick::<>f__am$cache2
	Action_1_t1429387386 * ___U3CU3Ef__amU24cache2_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(Sample08_DetectDoubleClick_t3898043094_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t3474512971 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t3474512971 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t3474512971 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(Sample08_DetectDoubleClick_t3898043094_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Func_2_t2970184680 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Func_2_t2970184680 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Func_2_t2970184680 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(Sample08_DetectDoubleClick_t3898043094_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Action_1_t1429387386 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Action_1_t1429387386 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Action_1_t1429387386 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE08_DETECTDOUBLECLICK_T3898043094_H
#ifndef SAMPLE09_EVENTHANDLING_T917274147_H
#define SAMPLE09_EVENTHANDLING_T917274147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample09_EventHandling
struct  Sample09_EventHandling_t917274147  : public MonoBehaviour_t3962482529
{
public:
	// System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs> UniRx.Examples.Sample09_EventHandling::FooBar
	EventHandler_1_t3151752610 * ___FooBar_2;
	// System.Action`1<System.Int32> UniRx.Examples.Sample09_EventHandling::FooFoo
	Action_1_t3123413348 * ___FooFoo_3;
	// UniRx.CompositeDisposable UniRx.Examples.Sample09_EventHandling::disposables
	CompositeDisposable_t3924054141 * ___disposables_4;
	// UniRx.Subject`1<System.Int32> UniRx.Examples.Sample09_EventHandling::onBarBar
	Subject_1_t3039602140 * ___onBarBar_5;

public:
	inline static int32_t get_offset_of_FooBar_2() { return static_cast<int32_t>(offsetof(Sample09_EventHandling_t917274147, ___FooBar_2)); }
	inline EventHandler_1_t3151752610 * get_FooBar_2() const { return ___FooBar_2; }
	inline EventHandler_1_t3151752610 ** get_address_of_FooBar_2() { return &___FooBar_2; }
	inline void set_FooBar_2(EventHandler_1_t3151752610 * value)
	{
		___FooBar_2 = value;
		Il2CppCodeGenWriteBarrier((&___FooBar_2), value);
	}

	inline static int32_t get_offset_of_FooFoo_3() { return static_cast<int32_t>(offsetof(Sample09_EventHandling_t917274147, ___FooFoo_3)); }
	inline Action_1_t3123413348 * get_FooFoo_3() const { return ___FooFoo_3; }
	inline Action_1_t3123413348 ** get_address_of_FooFoo_3() { return &___FooFoo_3; }
	inline void set_FooFoo_3(Action_1_t3123413348 * value)
	{
		___FooFoo_3 = value;
		Il2CppCodeGenWriteBarrier((&___FooFoo_3), value);
	}

	inline static int32_t get_offset_of_disposables_4() { return static_cast<int32_t>(offsetof(Sample09_EventHandling_t917274147, ___disposables_4)); }
	inline CompositeDisposable_t3924054141 * get_disposables_4() const { return ___disposables_4; }
	inline CompositeDisposable_t3924054141 ** get_address_of_disposables_4() { return &___disposables_4; }
	inline void set_disposables_4(CompositeDisposable_t3924054141 * value)
	{
		___disposables_4 = value;
		Il2CppCodeGenWriteBarrier((&___disposables_4), value);
	}

	inline static int32_t get_offset_of_onBarBar_5() { return static_cast<int32_t>(offsetof(Sample09_EventHandling_t917274147, ___onBarBar_5)); }
	inline Subject_1_t3039602140 * get_onBarBar_5() const { return ___onBarBar_5; }
	inline Subject_1_t3039602140 ** get_address_of_onBarBar_5() { return &___onBarBar_5; }
	inline void set_onBarBar_5(Subject_1_t3039602140 * value)
	{
		___onBarBar_5 = value;
		Il2CppCodeGenWriteBarrier((&___onBarBar_5), value);
	}
};

struct Sample09_EventHandling_t917274147_StaticFields
{
public:
	// System.Func`2<System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>> UniRx.Examples.Sample09_EventHandling::<>f__am$cache0
	Func_2_t2522298254 * ___U3CU3Ef__amU24cache0_6;
	// System.Func`2<System.Action`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>,System.EventHandler`1<UniRx.Examples.Sample09_EventHandling/MyEventArgs>> UniRx.Examples.Sample09_EventHandling::<>f__am$cache1
	Func_2_t6994052 * ___U3CU3Ef__amU24cache1_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(Sample09_EventHandling_t917274147_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Func_2_t2522298254 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Func_2_t2522298254 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Func_2_t2522298254 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_7() { return static_cast<int32_t>(offsetof(Sample09_EventHandling_t917274147_StaticFields, ___U3CU3Ef__amU24cache1_7)); }
	inline Func_2_t6994052 * get_U3CU3Ef__amU24cache1_7() const { return ___U3CU3Ef__amU24cache1_7; }
	inline Func_2_t6994052 ** get_address_of_U3CU3Ef__amU24cache1_7() { return &___U3CU3Ef__amU24cache1_7; }
	inline void set_U3CU3Ef__amU24cache1_7(Func_2_t6994052 * value)
	{
		___U3CU3Ef__amU24cache1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE09_EVENTHANDLING_T917274147_H
#ifndef SAMPLE13_TODOAPP_T4025886585_H
#define SAMPLE13_TODOAPP_T4025886585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Examples.Sample13_ToDoApp
struct  Sample13_ToDoApp_t4025886585  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text UniRx.Examples.Sample13_ToDoApp::Title
	Text_t1901882714 * ___Title_2;
	// UnityEngine.UI.InputField UniRx.Examples.Sample13_ToDoApp::ToDoInput
	InputField_t3762917431 * ___ToDoInput_3;
	// UnityEngine.UI.Button UniRx.Examples.Sample13_ToDoApp::AddButton
	Button_t4055032469 * ___AddButton_4;
	// UnityEngine.UI.Button UniRx.Examples.Sample13_ToDoApp::ClearButton
	Button_t4055032469 * ___ClearButton_5;
	// UnityEngine.GameObject UniRx.Examples.Sample13_ToDoApp::TodoList
	GameObject_t1113636619 * ___TodoList_6;
	// UnityEngine.GameObject UniRx.Examples.Sample13_ToDoApp::SampleItemPrefab
	GameObject_t1113636619 * ___SampleItemPrefab_7;
	// UniRx.ReactiveCollection`1<UnityEngine.GameObject> UniRx.Examples.Sample13_ToDoApp::toDos
	ReactiveCollection_1_t2124128606 * ___toDos_8;

public:
	inline static int32_t get_offset_of_Title_2() { return static_cast<int32_t>(offsetof(Sample13_ToDoApp_t4025886585, ___Title_2)); }
	inline Text_t1901882714 * get_Title_2() const { return ___Title_2; }
	inline Text_t1901882714 ** get_address_of_Title_2() { return &___Title_2; }
	inline void set_Title_2(Text_t1901882714 * value)
	{
		___Title_2 = value;
		Il2CppCodeGenWriteBarrier((&___Title_2), value);
	}

	inline static int32_t get_offset_of_ToDoInput_3() { return static_cast<int32_t>(offsetof(Sample13_ToDoApp_t4025886585, ___ToDoInput_3)); }
	inline InputField_t3762917431 * get_ToDoInput_3() const { return ___ToDoInput_3; }
	inline InputField_t3762917431 ** get_address_of_ToDoInput_3() { return &___ToDoInput_3; }
	inline void set_ToDoInput_3(InputField_t3762917431 * value)
	{
		___ToDoInput_3 = value;
		Il2CppCodeGenWriteBarrier((&___ToDoInput_3), value);
	}

	inline static int32_t get_offset_of_AddButton_4() { return static_cast<int32_t>(offsetof(Sample13_ToDoApp_t4025886585, ___AddButton_4)); }
	inline Button_t4055032469 * get_AddButton_4() const { return ___AddButton_4; }
	inline Button_t4055032469 ** get_address_of_AddButton_4() { return &___AddButton_4; }
	inline void set_AddButton_4(Button_t4055032469 * value)
	{
		___AddButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___AddButton_4), value);
	}

	inline static int32_t get_offset_of_ClearButton_5() { return static_cast<int32_t>(offsetof(Sample13_ToDoApp_t4025886585, ___ClearButton_5)); }
	inline Button_t4055032469 * get_ClearButton_5() const { return ___ClearButton_5; }
	inline Button_t4055032469 ** get_address_of_ClearButton_5() { return &___ClearButton_5; }
	inline void set_ClearButton_5(Button_t4055032469 * value)
	{
		___ClearButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___ClearButton_5), value);
	}

	inline static int32_t get_offset_of_TodoList_6() { return static_cast<int32_t>(offsetof(Sample13_ToDoApp_t4025886585, ___TodoList_6)); }
	inline GameObject_t1113636619 * get_TodoList_6() const { return ___TodoList_6; }
	inline GameObject_t1113636619 ** get_address_of_TodoList_6() { return &___TodoList_6; }
	inline void set_TodoList_6(GameObject_t1113636619 * value)
	{
		___TodoList_6 = value;
		Il2CppCodeGenWriteBarrier((&___TodoList_6), value);
	}

	inline static int32_t get_offset_of_SampleItemPrefab_7() { return static_cast<int32_t>(offsetof(Sample13_ToDoApp_t4025886585, ___SampleItemPrefab_7)); }
	inline GameObject_t1113636619 * get_SampleItemPrefab_7() const { return ___SampleItemPrefab_7; }
	inline GameObject_t1113636619 ** get_address_of_SampleItemPrefab_7() { return &___SampleItemPrefab_7; }
	inline void set_SampleItemPrefab_7(GameObject_t1113636619 * value)
	{
		___SampleItemPrefab_7 = value;
		Il2CppCodeGenWriteBarrier((&___SampleItemPrefab_7), value);
	}

	inline static int32_t get_offset_of_toDos_8() { return static_cast<int32_t>(offsetof(Sample13_ToDoApp_t4025886585, ___toDos_8)); }
	inline ReactiveCollection_1_t2124128606 * get_toDos_8() const { return ___toDos_8; }
	inline ReactiveCollection_1_t2124128606 ** get_address_of_toDos_8() { return &___toDos_8; }
	inline void set_toDos_8(ReactiveCollection_1_t2124128606 * value)
	{
		___toDos_8 = value;
		Il2CppCodeGenWriteBarrier((&___toDos_8), value);
	}
};

struct Sample13_ToDoApp_t4025886585_StaticFields
{
public:
	// System.Func`2<System.String,System.Boolean> UniRx.Examples.Sample13_ToDoApp::<>f__am$cache0
	Func_2_t2197129486 * ___U3CU3Ef__amU24cache0_9;
	// System.Func`2<System.String,System.Boolean> UniRx.Examples.Sample13_ToDoApp::<>f__am$cache1
	Func_2_t2197129486 * ___U3CU3Ef__amU24cache1_10;
	// System.Action`1<UniRx.CollectionRemoveEvent`1<UnityEngine.GameObject>> UniRx.Examples.Sample13_ToDoApp::<>f__am$cache2
	Action_1_t192939710 * ___U3CU3Ef__amU24cache2_11;
	// System.Func`2<UnityEngine.GameObject,System.Boolean> UniRx.Examples.Sample13_ToDoApp::<>f__am$cache3
	Func_2_t4243939292 * ___U3CU3Ef__amU24cache3_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(Sample13_ToDoApp_t4025886585_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Func_2_t2197129486 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Func_2_t2197129486 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Func_2_t2197129486 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(Sample13_ToDoApp_t4025886585_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Func_2_t2197129486 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Func_2_t2197129486 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Func_2_t2197129486 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_11() { return static_cast<int32_t>(offsetof(Sample13_ToDoApp_t4025886585_StaticFields, ___U3CU3Ef__amU24cache2_11)); }
	inline Action_1_t192939710 * get_U3CU3Ef__amU24cache2_11() const { return ___U3CU3Ef__amU24cache2_11; }
	inline Action_1_t192939710 ** get_address_of_U3CU3Ef__amU24cache2_11() { return &___U3CU3Ef__amU24cache2_11; }
	inline void set_U3CU3Ef__amU24cache2_11(Action_1_t192939710 * value)
	{
		___U3CU3Ef__amU24cache2_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_12() { return static_cast<int32_t>(offsetof(Sample13_ToDoApp_t4025886585_StaticFields, ___U3CU3Ef__amU24cache3_12)); }
	inline Func_2_t4243939292 * get_U3CU3Ef__amU24cache3_12() const { return ___U3CU3Ef__amU24cache3_12; }
	inline Func_2_t4243939292 ** get_address_of_U3CU3Ef__amU24cache3_12() { return &___U3CU3Ef__amU24cache3_12; }
	inline void set_U3CU3Ef__amU24cache3_12(Func_2_t4243939292 * value)
	{
		___U3CU3Ef__amU24cache3_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE13_TODOAPP_T4025886585_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (LogCallback_t2567365653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2400[3] = 
{
	LogCallback_t2567365653::get_offset_of_Condition_0(),
	LogCallback_t2567365653::get_offset_of_StackTrace_1(),
	LogCallback_t2567365653::get_offset_of_LogType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (LogHelper_t4028092818), -1, sizeof(LogHelper_t4028092818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2401[3] = 
{
	LogHelper_t4028092818_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LogHelper_t4028092818_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LogHelper_t4028092818_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (U3CLogCallbackAsObservableU3Ec__AnonStorey0_t2625726255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[1] = 
{
	U3CLogCallbackAsObservableU3Ec__AnonStorey0_t2625726255::get_offset_of_h_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (Sample05_ConvertFromCoroutine_t1417798464), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (U3CGetWWWU3Ec__AnonStorey1_t1511513620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2404[1] = 
{
	U3CGetWWWU3Ec__AnonStorey1_t1511513620::get_offset_of_url_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (U3CGetWWWCoreU3Ec__Iterator0_t1065486712), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[7] = 
{
	U3CGetWWWCoreU3Ec__Iterator0_t1065486712::get_offset_of_url_0(),
	U3CGetWWWCoreU3Ec__Iterator0_t1065486712::get_offset_of_U3CwwwU3E__0_1(),
	U3CGetWWWCoreU3Ec__Iterator0_t1065486712::get_offset_of_cancellationToken_2(),
	U3CGetWWWCoreU3Ec__Iterator0_t1065486712::get_offset_of_observer_3(),
	U3CGetWWWCoreU3Ec__Iterator0_t1065486712::get_offset_of_U24current_4(),
	U3CGetWWWCoreU3Ec__Iterator0_t1065486712::get_offset_of_U24disposing_5(),
	U3CGetWWWCoreU3Ec__Iterator0_t1065486712::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (Sample06_ConvertToCoroutine_t3751354639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (U3CComplexCoroutineTestU3Ec__Iterator0_t2970897030), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[4] = 
{
	U3CComplexCoroutineTestU3Ec__Iterator0_t2970897030::get_offset_of_U24current_0(),
	U3CComplexCoroutineTestU3Ec__Iterator0_t2970897030::get_offset_of_U24disposing_1(),
	U3CComplexCoroutineTestU3Ec__Iterator0_t2970897030::get_offset_of_U24PC_2(),
	U3CComplexCoroutineTestU3Ec__Iterator0_t2970897030::get_offset_of_U24locvar0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (U3CComplexCoroutineTestU3Ec__AnonStorey2_t4184177592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2408[1] = 
{
	U3CComplexCoroutineTestU3Ec__AnonStorey2_t4184177592::get_offset_of_v_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989), -1, sizeof(U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2409[7] = 
{
	U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989::get_offset_of_U3CoU3E__0_0(),
	U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989::get_offset_of_U24this_1(),
	U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989::get_offset_of_U24current_2(),
	U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989::get_offset_of_U24disposing_3(),
	U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989::get_offset_of_U24PC_4(),
	U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	U3CTestNewCustomYieldInstructionU3Ec__Iterator1_t3391453989_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (Sample07_OrchestratIEnumerator_t3797225793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (U3CAsyncAU3Ec__Iterator0_t2523753990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2411[3] = 
{
	U3CAsyncAU3Ec__Iterator0_t2523753990::get_offset_of_U24current_0(),
	U3CAsyncAU3Ec__Iterator0_t2523753990::get_offset_of_U24disposing_1(),
	U3CAsyncAU3Ec__Iterator0_t2523753990::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (U3CAsyncBU3Ec__Iterator1_t2525684525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[3] = 
{
	U3CAsyncBU3Ec__Iterator1_t2525684525::get_offset_of_U24current_0(),
	U3CAsyncBU3Ec__Iterator1_t2525684525::get_offset_of_U24disposing_1(),
	U3CAsyncBU3Ec__Iterator1_t2525684525::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (Sample08_DetectDoubleClick_t3898043094), -1, sizeof(Sample08_DetectDoubleClick_t3898043094_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2413[3] = 
{
	Sample08_DetectDoubleClick_t3898043094_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	Sample08_DetectDoubleClick_t3898043094_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
	Sample08_DetectDoubleClick_t3898043094_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (Sample09_EventHandling_t917274147), -1, sizeof(Sample09_EventHandling_t917274147_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2414[6] = 
{
	Sample09_EventHandling_t917274147::get_offset_of_FooBar_2(),
	Sample09_EventHandling_t917274147::get_offset_of_FooFoo_3(),
	Sample09_EventHandling_t917274147::get_offset_of_disposables_4(),
	Sample09_EventHandling_t917274147::get_offset_of_onBarBar_5(),
	Sample09_EventHandling_t917274147_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
	Sample09_EventHandling_t917274147_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (MyEventArgs_t932625881), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[1] = 
{
	MyEventArgs_t932625881::get_offset_of_U3CMyPropertyU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (U3CStartU3Ec__AnonStorey1_t1460542006), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[2] = 
{
	U3CStartU3Ec__AnonStorey1_t1460542006::get_offset_of_capture_0(),
	U3CStartU3Ec__AnonStorey1_t1460542006::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (U3CStartU3Ec__AnonStorey0_t4189425361), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2417[1] = 
{
	U3CStartU3Ec__AnonStorey0_t4189425361::get_offset_of_h_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (Sample10_MainThreadDispatcher_t3078734430), -1, sizeof(Sample10_MainThreadDispatcher_t3078734430_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2418[4] = 
{
	Sample10_MainThreadDispatcher_t3078734430_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_0(),
	Sample10_MainThreadDispatcher_t3078734430_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
	Sample10_MainThreadDispatcher_t3078734430_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_2(),
	Sample10_MainThreadDispatcher_t3078734430_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (U3CTestAsyncU3Ec__Iterator0_t2949466201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[3] = 
{
	U3CTestAsyncU3Ec__Iterator0_t2949466201::get_offset_of_U24current_0(),
	U3CTestAsyncU3Ec__Iterator0_t2949466201::get_offset_of_U24disposing_1(),
	U3CTestAsyncU3Ec__Iterator0_t2949466201::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (Sample11_Logger_t748867482), -1, sizeof(Sample11_Logger_t748867482_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2420[3] = 
{
	Sample11_Logger_t748867482_StaticFields::get_offset_of_logger_0(),
	Sample11_Logger_t748867482_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
	Sample11_Logger_t748867482_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (Sample12_ReactiveProperty_t1497491507), -1, sizeof(Sample12_ReactiveProperty_t1497491507_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2421[10] = 
{
	Sample12_ReactiveProperty_t1497491507::get_offset_of_MyButton_2(),
	Sample12_ReactiveProperty_t1497491507::get_offset_of_MyToggle_3(),
	Sample12_ReactiveProperty_t1497491507::get_offset_of_MyInput_4(),
	Sample12_ReactiveProperty_t1497491507::get_offset_of_MyText_5(),
	Sample12_ReactiveProperty_t1497491507::get_offset_of_MySlider_6(),
	Sample12_ReactiveProperty_t1497491507::get_offset_of_IntRxProp_7(),
	Sample12_ReactiveProperty_t1497491507::get_offset_of_enemy_8(),
	Sample12_ReactiveProperty_t1497491507_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	Sample12_ReactiveProperty_t1497491507_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
	Sample12_ReactiveProperty_t1497491507_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (Enemy_t2629818976), -1, sizeof(Enemy_t2629818976_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2422[3] = 
{
	Enemy_t2629818976::get_offset_of_U3CCurrentHpU3Ek__BackingField_0(),
	Enemy_t2629818976::get_offset_of_U3CIsDeadU3Ek__BackingField_1(),
	Enemy_t2629818976_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (Sample13_ToDoApp_t4025886585), -1, sizeof(Sample13_ToDoApp_t4025886585_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2423[11] = 
{
	Sample13_ToDoApp_t4025886585::get_offset_of_Title_2(),
	Sample13_ToDoApp_t4025886585::get_offset_of_ToDoInput_3(),
	Sample13_ToDoApp_t4025886585::get_offset_of_AddButton_4(),
	Sample13_ToDoApp_t4025886585::get_offset_of_ClearButton_5(),
	Sample13_ToDoApp_t4025886585::get_offset_of_TodoList_6(),
	Sample13_ToDoApp_t4025886585::get_offset_of_SampleItemPrefab_7(),
	Sample13_ToDoApp_t4025886585::get_offset_of_toDos_8(),
	Sample13_ToDoApp_t4025886585_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	Sample13_ToDoApp_t4025886585_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
	Sample13_ToDoApp_t4025886585_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_11(),
	Sample13_ToDoApp_t4025886585_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (WebRequestExtensions_t2028226060), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2426[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (U3CGetResponseAsObservableU3Ec__AnonStorey2_t3350246897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2427[1] = 
{
	U3CGetResponseAsObservableU3Ec__AnonStorey2_t3350246897::get_offset_of_request_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (BooleanDisposable_t84760918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[1] = 
{
	BooleanDisposable_t84760918::get_offset_of_U3CIsDisposedU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (CompositeDisposable_t3924054141), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2429[5] = 
{
	CompositeDisposable_t3924054141::get_offset_of__gate_0(),
	CompositeDisposable_t3924054141::get_offset_of__disposed_1(),
	CompositeDisposable_t3924054141::get_offset_of__disposables_2(),
	CompositeDisposable_t3924054141::get_offset_of__count_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (Disposable_t665628049), -1, sizeof(Disposable_t665628049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2431[1] = 
{
	Disposable_t665628049_StaticFields::get_offset_of_Empty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (EmptyDisposable_t1744265291), -1, sizeof(EmptyDisposable_t1744265291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2432[1] = 
{
	EmptyDisposable_t1744265291_StaticFields::get_offset_of_Singleton_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (AnonymousDisposable_t4034176521), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[2] = 
{
	AnonymousDisposable_t4034176521::get_offset_of_isDisposed_0(),
	AnonymousDisposable_t4034176521::get_offset_of_dispose_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (DisposableExtensions_t1054371994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (U3CMonitorTriggerHealthU3Ec__Iterator0_t998410463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2436[5] = 
{
	U3CMonitorTriggerHealthU3Ec__Iterator0_t998410463::get_offset_of_trigger_0(),
	U3CMonitorTriggerHealthU3Ec__Iterator0_t998410463::get_offset_of_targetGameObject_1(),
	U3CMonitorTriggerHealthU3Ec__Iterator0_t998410463::get_offset_of_U24current_2(),
	U3CMonitorTriggerHealthU3Ec__Iterator0_t998410463::get_offset_of_U24disposing_3(),
	U3CMonitorTriggerHealthU3Ec__Iterator0_t998410463::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (MultipleAssignmentDisposable_t1738612192), -1, sizeof(MultipleAssignmentDisposable_t1738612192_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2438[3] = 
{
	MultipleAssignmentDisposable_t1738612192_StaticFields::get_offset_of_True_0(),
	MultipleAssignmentDisposable_t1738612192::get_offset_of_gate_1(),
	MultipleAssignmentDisposable_t1738612192::get_offset_of_current_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (RefCountDisposable_t3209185398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2439[4] = 
{
	RefCountDisposable_t3209185398::get_offset_of__gate_0(),
	RefCountDisposable_t3209185398::get_offset_of__disposable_1(),
	RefCountDisposable_t3209185398::get_offset_of__isPrimaryDisposed_2(),
	RefCountDisposable_t3209185398::get_offset_of__count_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (InnerDisposable_t657274241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2440[2] = 
{
	InnerDisposable_t657274241::get_offset_of__parent_0(),
	InnerDisposable_t657274241::get_offset_of_parentLock_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (Observable_t2677550507), -1, sizeof(Observable_t2677550507_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2441[9] = 
{
	Observable_t2677550507_StaticFields::get_offset_of_InfiniteTimeSpan_0(),
	Observable_t2677550507_StaticFields::get_offset_of_YieldInstructionTypes_1(),
	Observable_t2677550507_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_2(),
	Observable_t2677550507_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_3(),
	Observable_t2677550507_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_4(),
	Observable_t2677550507_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	Observable_t2677550507_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
	Observable_t2677550507_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_7(),
	Observable_t2677550507_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2442[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2443[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (EveryAfterUpdateInvoker_t1199703476), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2444[3] = 
{
	EveryAfterUpdateInvoker_t1199703476::get_offset_of_count_0(),
	EveryAfterUpdateInvoker_t1199703476::get_offset_of_observer_1(),
	EveryAfterUpdateInvoker_t1199703476::get_offset_of_cancellationToken_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2445[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2446[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2447[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2448[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2449[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (U3CToAsyncU3Ec__AnonStoreyD_t2567764606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2450[2] = 
{
	U3CToAsyncU3Ec__AnonStoreyD_t2567764606::get_offset_of_scheduler_0(),
	U3CToAsyncU3Ec__AnonStoreyD_t2567764606::get_offset_of_action_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (U3CToAsyncU3Ec__AnonStoreyE_t2090547987), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2451[2] = 
{
	U3CToAsyncU3Ec__AnonStoreyE_t2090547987::get_offset_of_subject_0(),
	U3CToAsyncU3Ec__AnonStoreyE_t2090547987::get_offset_of_U3CU3Ef__refU2413_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2452[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2453[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2454[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2455[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2456[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2457[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2458[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2459[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2460[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2461[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2462[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (U3CFromAsyncPatternU3Ec__AnonStorey1A_t1162034728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2463[1] = 
{
	U3CFromAsyncPatternU3Ec__AnonStorey1A_t1162034728::get_offset_of_end_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2464[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2465[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (U3CFromCoroutineU3Ec__AnonStorey1D_t4004376715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2466[2] = 
{
	U3CFromCoroutineU3Ec__AnonStorey1D_t4004376715::get_offset_of_coroutine_0(),
	U3CFromCoroutineU3Ec__AnonStorey1D_t4004376715::get_offset_of_publishEveryYield_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (U3CFromCoroutineU3Ec__AnonStorey1E_t1275493360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2467[2] = 
{
	U3CFromCoroutineU3Ec__AnonStorey1E_t1275493360::get_offset_of_coroutine_0(),
	U3CFromCoroutineU3Ec__AnonStorey1E_t1275493360::get_offset_of_publishEveryYield_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (U3CFromMicroCoroutineU3Ec__AnonStorey1F_t3627648857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2468[2] = 
{
	U3CFromMicroCoroutineU3Ec__AnonStorey1F_t3627648857::get_offset_of_coroutine_0(),
	U3CFromMicroCoroutineU3Ec__AnonStorey1F_t3627648857::get_offset_of_publishEveryYield_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (U3CFromMicroCoroutineU3Ec__AnonStorey20_t4078298410), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2469[2] = 
{
	U3CFromMicroCoroutineU3Ec__AnonStorey20_t4078298410::get_offset_of_coroutine_0(),
	U3CFromMicroCoroutineU3Ec__AnonStorey20_t4078298410::get_offset_of_publishEveryYield_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (U3CWrapEnumeratorU3Ec__Iterator2_t3733957475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2470[11] = 
{
	U3CWrapEnumeratorU3Ec__Iterator2_t3733957475::get_offset_of_U3ChasNextU3E__0_0(),
	U3CWrapEnumeratorU3Ec__Iterator2_t3733957475::get_offset_of_U3CraisedErrorU3E__0_1(),
	U3CWrapEnumeratorU3Ec__Iterator2_t3733957475::get_offset_of_enumerator_2(),
	U3CWrapEnumeratorU3Ec__Iterator2_t3733957475::get_offset_of_observer_3(),
	U3CWrapEnumeratorU3Ec__Iterator2_t3733957475::get_offset_of_publishEveryYield_4(),
	U3CWrapEnumeratorU3Ec__Iterator2_t3733957475::get_offset_of_U3CcurrentU3E__1_5(),
	U3CWrapEnumeratorU3Ec__Iterator2_t3733957475::get_offset_of_U3CcustomHandlerU3E__1_6(),
	U3CWrapEnumeratorU3Ec__Iterator2_t3733957475::get_offset_of_cancellationToken_7(),
	U3CWrapEnumeratorU3Ec__Iterator2_t3733957475::get_offset_of_U24current_8(),
	U3CWrapEnumeratorU3Ec__Iterator2_t3733957475::get_offset_of_U24disposing_9(),
	U3CWrapEnumeratorU3Ec__Iterator2_t3733957475::get_offset_of_U24PC_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2471[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2473[11] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2476[6] = 
{
	U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075::get_offset_of_U3ChasNextU3E__0_0(),
	U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075::get_offset_of_enumerator_1(),
	U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075::get_offset_of_cancellationToken_2(),
	U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075::get_offset_of_U24current_3(),
	U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075::get_offset_of_U24disposing_4(),
	U3CWrapToCancellableEnumeratorU3Ec__Iterator4_t1899200075::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2478[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2479[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (U3CToObservableU3Ec__AnonStorey29_t737702686), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2481[2] = 
{
	U3CToObservableU3Ec__AnonStorey29_t737702686::get_offset_of_coroutine_0(),
	U3CToObservableU3Ec__AnonStorey29_t737702686::get_offset_of_publishEveryYield_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (U3CEveryCycleCoreU3Ec__Iterator5_t1540769718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2482[6] = 
{
	U3CEveryCycleCoreU3Ec__Iterator5_t1540769718::get_offset_of_cancellationToken_0(),
	U3CEveryCycleCoreU3Ec__Iterator5_t1540769718::get_offset_of_U3CcountU3E__0_1(),
	U3CEveryCycleCoreU3Ec__Iterator5_t1540769718::get_offset_of_observer_2(),
	U3CEveryCycleCoreU3Ec__Iterator5_t1540769718::get_offset_of_U24current_3(),
	U3CEveryCycleCoreU3Ec__Iterator5_t1540769718::get_offset_of_U24disposing_4(),
	U3CEveryCycleCoreU3Ec__Iterator5_t1540769718::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (U3CNextFrameCoreU3Ec__Iterator6_t77210722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[5] = 
{
	U3CNextFrameCoreU3Ec__Iterator6_t77210722::get_offset_of_cancellation_0(),
	U3CNextFrameCoreU3Ec__Iterator6_t77210722::get_offset_of_observer_1(),
	U3CNextFrameCoreU3Ec__Iterator6_t77210722::get_offset_of_U24current_2(),
	U3CNextFrameCoreU3Ec__Iterator6_t77210722::get_offset_of_U24disposing_3(),
	U3CNextFrameCoreU3Ec__Iterator6_t77210722::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (U3CTimerFrameU3Ec__AnonStorey2A_t1732450895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2484[1] = 
{
	U3CTimerFrameU3Ec__AnonStorey2A_t1732450895::get_offset_of_dueTimeFrameCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (U3CTimerFrameU3Ec__AnonStorey2B_t3688766031), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[2] = 
{
	U3CTimerFrameU3Ec__AnonStorey2B_t3688766031::get_offset_of_dueTimeFrameCount_0(),
	U3CTimerFrameU3Ec__AnonStorey2B_t3688766031::get_offset_of_periodFrameCount_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (U3CTimerFrameCoreU3Ec__Iterator7_t1077573192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[7] = 
{
	U3CTimerFrameCoreU3Ec__Iterator7_t1077573192::get_offset_of_dueTimeFrameCount_0(),
	U3CTimerFrameCoreU3Ec__Iterator7_t1077573192::get_offset_of_U3CcurrentFrameU3E__0_1(),
	U3CTimerFrameCoreU3Ec__Iterator7_t1077573192::get_offset_of_cancel_2(),
	U3CTimerFrameCoreU3Ec__Iterator7_t1077573192::get_offset_of_observer_3(),
	U3CTimerFrameCoreU3Ec__Iterator7_t1077573192::get_offset_of_U24current_4(),
	U3CTimerFrameCoreU3Ec__Iterator7_t1077573192::get_offset_of_U24disposing_5(),
	U3CTimerFrameCoreU3Ec__Iterator7_t1077573192::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (U3CTimerFrameCoreU3Ec__Iterator8_t1077573207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[9] = 
{
	U3CTimerFrameCoreU3Ec__Iterator8_t1077573207::get_offset_of_dueTimeFrameCount_0(),
	U3CTimerFrameCoreU3Ec__Iterator8_t1077573207::get_offset_of_periodFrameCount_1(),
	U3CTimerFrameCoreU3Ec__Iterator8_t1077573207::get_offset_of_U3CsendCountU3E__0_2(),
	U3CTimerFrameCoreU3Ec__Iterator8_t1077573207::get_offset_of_U3CcurrentFrameU3E__0_3(),
	U3CTimerFrameCoreU3Ec__Iterator8_t1077573207::get_offset_of_cancel_4(),
	U3CTimerFrameCoreU3Ec__Iterator8_t1077573207::get_offset_of_observer_5(),
	U3CTimerFrameCoreU3Ec__Iterator8_t1077573207::get_offset_of_U24current_6(),
	U3CTimerFrameCoreU3Ec__Iterator8_t1077573207::get_offset_of_U24disposing_7(),
	U3CTimerFrameCoreU3Ec__Iterator8_t1077573207::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (ScheduledDisposable_t4090582988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2489[3] = 
{
	ScheduledDisposable_t4090582988::get_offset_of_scheduler_0(),
	ScheduledDisposable_t4090582988::get_offset_of_disposable_1(),
	ScheduledDisposable_t4090582988::get_offset_of_isDisposed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (SerialDisposable_t3525249344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[3] = 
{
	SerialDisposable_t3525249344::get_offset_of_gate_0(),
	SerialDisposable_t3525249344::get_offset_of_current_1(),
	SerialDisposable_t3525249344::get_offset_of_disposed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (SingleAssignmentDisposable_t4102667663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[3] = 
{
	SingleAssignmentDisposable_t4102667663::get_offset_of_gate_0(),
	SingleAssignmentDisposable_t4102667663::get_offset_of_current_1(),
	SingleAssignmentDisposable_t4102667663::get_offset_of_disposed_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (StableCompositeDisposable_t519652846), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (Binary_t2304246015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2493[3] = 
{
	Binary_t2304246015::get_offset_of_disposedCallCount_0(),
	Binary_t2304246015::get_offset_of__disposable1_1(),
	Binary_t2304246015::get_offset_of__disposable2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (Trinary_t3668327564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2494[4] = 
{
	Trinary_t3668327564::get_offset_of_disposedCallCount_0(),
	Trinary_t3668327564::get_offset_of__disposable1_1(),
	Trinary_t3668327564::get_offset_of__disposable2_2(),
	Trinary_t3668327564::get_offset_of__disposable3_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (Quaternary_t846636199), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2495[5] = 
{
	Quaternary_t846636199::get_offset_of_disposedCallCount_0(),
	Quaternary_t846636199::get_offset_of__disposable1_1(),
	Quaternary_t846636199::get_offset_of__disposable2_2(),
	Quaternary_t846636199::get_offset_of__disposable3_3(),
	Quaternary_t846636199::get_offset_of__disposable4_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (NAry_t2515936203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[2] = 
{
	NAry_t2515936203::get_offset_of_disposedCallCount_0(),
	NAry_t2515936203::get_offset_of__disposables_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (NAryUnsafe_t2030350842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2497[2] = 
{
	NAryUnsafe_t2030350842::get_offset_of_disposedCallCount_0(),
	NAryUnsafe_t2030350842::get_offset_of__disposables_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
