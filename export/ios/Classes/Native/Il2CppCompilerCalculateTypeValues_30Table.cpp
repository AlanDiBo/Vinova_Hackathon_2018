﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// UniRx.IProgress`1<System.Single>
struct IProgress_1_t3968412695;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// UniRx.Operators.BatchFrameObservable/BatchFrame
struct BatchFrame_t3337927563;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// UnityEngine.Coroutine[]
struct CoroutineU5BU5D_t1698289742;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UniRx.IObserver`1<UniRx.Unit>
struct IObserver_1_t811551750;
// System.IDisposable
struct IDisposable_t3640265483;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UniRx.MainThreadDispatcher
struct MainThreadDispatcher_t3684499304;
// UniRx.Operators.BatchFrameObservable
struct BatchFrameObservable_t2539759103;
// UniRx.BooleanDisposable
struct BooleanDisposable_t84760918;
// UniRx.ICancelable
struct ICancelable_t3440398893;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// UnityEngine.WWW
struct WWW_t3688466362;
// UniRx.IObserver`1<UnityEngine.AssetBundle>
struct IObserver_1_t2898176831;
// UniRx.IObserver`1<System.Byte[]>
struct IObserver_1_t1565949940;
// UniRx.IObserver`1<System.String>
struct IObserver_1_t3591720268;
// UniRx.IObserver`1<UnityEngine.WWW>
struct IObserver_1_t1137768645;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Quaternion>
struct IEqualityComparer_1_t114293053;
// UniRx.Subject`1<UnityEngine.Quaternion>
struct Subject_1_t2390584718;
// System.Exception
struct Exception_t;
// UniRx.IObservable`1<UniRx.Unit>
struct IObservable_1_t1485381605;
// System.Func`2<UniRx.LazyTask,UnityEngine.Coroutine>
struct Func_2_t2678796183;
// UniRx.IPresenter[]
struct IPresenterU5BU5D_t1029123997;
// UniRx.Subject`1<UniRx.Unit>
struct Subject_1_t3450905854;
// UniRx.IPresenter
struct IPresenter_t2289494932;
// UniRx.InternalUtil.ThreadSafeQueueWorker
struct ThreadSafeQueueWorker_t2171548691;
// System.Action`1<System.Exception>
struct Action_1_t1609204844;
// UniRx.InternalUtil.MicroCoroutine
struct MicroCoroutine_t3506682403;
// UniRx.Subject`1<System.Boolean>
struct Subject_1_t185944352;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CLOADFROMCACHEORDOWNLOADU3EC__ANONSTOREY14_T3766483179_H
#define U3CLOADFROMCACHEORDOWNLOADU3EC__ANONSTOREY14_T3766483179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey14
struct  U3CLoadFromCacheOrDownloadU3Ec__AnonStorey14_t3766483179  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey14::url
	String_t* ___url_0;
	// System.Int32 UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey14::version
	int32_t ___version_1;
	// System.UInt32 UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey14::crc
	uint32_t ___crc_2;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey14::progress
	RuntimeObject* ___progress_3;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey14_t3766483179, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey14_t3766483179, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_crc_2() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey14_t3766483179, ___crc_2)); }
	inline uint32_t get_crc_2() const { return ___crc_2; }
	inline uint32_t* get_address_of_crc_2() { return &___crc_2; }
	inline void set_crc_2(uint32_t value)
	{
		___crc_2 = value;
	}

	inline static int32_t get_offset_of_progress_3() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey14_t3766483179, ___progress_3)); }
	inline RuntimeObject* get_progress_3() const { return ___progress_3; }
	inline RuntimeObject** get_address_of_progress_3() { return &___progress_3; }
	inline void set_progress_3(RuntimeObject* value)
	{
		___progress_3 = value;
		Il2CppCodeGenWriteBarrier((&___progress_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADFROMCACHEORDOWNLOADU3EC__ANONSTOREY14_T3766483179_H
#ifndef U3CPOSTANDGETBYTESU3EC__ANONSTOREYD_T1040859663_H
#define U3CPOSTANDGETBYTESU3EC__ANONSTOREYD_T1040859663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyD
struct  U3CPostAndGetBytesU3Ec__AnonStoreyD_t1040859663  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyD::url
	String_t* ___url_0;
	// UnityEngine.WWWForm UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyD::content
	WWWForm_t4064702195 * ___content_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyD::progress
	RuntimeObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyD_t1040859663, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyD_t1040859663, ___content_1)); }
	inline WWWForm_t4064702195 * get_content_1() const { return ___content_1; }
	inline WWWForm_t4064702195 ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(WWWForm_t4064702195 * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyD_t1040859663, ___progress_2)); }
	inline RuntimeObject* get_progress_2() const { return ___progress_2; }
	inline RuntimeObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(RuntimeObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier((&___progress_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTANDGETBYTESU3EC__ANONSTOREYD_T1040859663_H
#ifndef U3CPOSTANDGETBYTESU3EC__ANONSTOREYC_T2963173964_H
#define U3CPOSTANDGETBYTESU3EC__ANONSTOREYC_T2963173964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyC
struct  U3CPostAndGetBytesU3Ec__AnonStoreyC_t2963173964  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyC::url
	String_t* ___url_0;
	// System.Byte[] UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyC::postData
	ByteU5BU5D_t4116647657* ___postData_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyC::headers
	Dictionary_2_t1632706988 * ___headers_2;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyC::progress
	RuntimeObject* ___progress_3;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyC_t2963173964, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_postData_1() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyC_t2963173964, ___postData_1)); }
	inline ByteU5BU5D_t4116647657* get_postData_1() const { return ___postData_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_postData_1() { return &___postData_1; }
	inline void set_postData_1(ByteU5BU5D_t4116647657* value)
	{
		___postData_1 = value;
		Il2CppCodeGenWriteBarrier((&___postData_1), value);
	}

	inline static int32_t get_offset_of_headers_2() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyC_t2963173964, ___headers_2)); }
	inline Dictionary_2_t1632706988 * get_headers_2() const { return ___headers_2; }
	inline Dictionary_2_t1632706988 ** get_address_of_headers_2() { return &___headers_2; }
	inline void set_headers_2(Dictionary_2_t1632706988 * value)
	{
		___headers_2 = value;
		Il2CppCodeGenWriteBarrier((&___headers_2), value);
	}

	inline static int32_t get_offset_of_progress_3() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyC_t2963173964, ___progress_3)); }
	inline RuntimeObject* get_progress_3() const { return ___progress_3; }
	inline RuntimeObject** get_address_of_progress_3() { return &___progress_3; }
	inline void set_progress_3(RuntimeObject* value)
	{
		___progress_3 = value;
		Il2CppCodeGenWriteBarrier((&___progress_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTANDGETBYTESU3EC__ANONSTOREYC_T2963173964_H
#ifndef U3CPOSTANDGETBYTESU3EC__ANONSTOREYB_T234290609_H
#define U3CPOSTANDGETBYTESU3EC__ANONSTOREYB_T234290609_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyB
struct  U3CPostAndGetBytesU3Ec__AnonStoreyB_t234290609  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyB::url
	String_t* ___url_0;
	// System.Byte[] UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyB::postData
	ByteU5BU5D_t4116647657* ___postData_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyB::progress
	RuntimeObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyB_t234290609, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_postData_1() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyB_t234290609, ___postData_1)); }
	inline ByteU5BU5D_t4116647657* get_postData_1() const { return ___postData_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_postData_1() { return &___postData_1; }
	inline void set_postData_1(ByteU5BU5D_t4116647657* value)
	{
		___postData_1 = value;
		Il2CppCodeGenWriteBarrier((&___postData_1), value);
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyB_t234290609, ___progress_2)); }
	inline RuntimeObject* get_progress_2() const { return ___progress_2; }
	inline RuntimeObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(RuntimeObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier((&___progress_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTANDGETBYTESU3EC__ANONSTOREYB_T234290609_H
#ifndef U3CPOSTU3EC__ANONSTOREYA_T1430582499_H
#define U3CPOSTU3EC__ANONSTOREYA_T1430582499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<Post>c__AnonStoreyA
struct  U3CPostU3Ec__AnonStoreyA_t1430582499  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<Post>c__AnonStoreyA::url
	String_t* ___url_0;
	// UnityEngine.WWWForm UniRx.ObservableWWW/<Post>c__AnonStoreyA::content
	WWWForm_t4064702195 * ___content_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<Post>c__AnonStoreyA::contentHeaders
	Dictionary_2_t1632706988 * ___contentHeaders_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<Post>c__AnonStoreyA::headers
	Dictionary_2_t1632706988 * ___headers_3;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<Post>c__AnonStoreyA::progress
	RuntimeObject* ___progress_4;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStoreyA_t1430582499, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStoreyA_t1430582499, ___content_1)); }
	inline WWWForm_t4064702195 * get_content_1() const { return ___content_1; }
	inline WWWForm_t4064702195 ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(WWWForm_t4064702195 * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_contentHeaders_2() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStoreyA_t1430582499, ___contentHeaders_2)); }
	inline Dictionary_2_t1632706988 * get_contentHeaders_2() const { return ___contentHeaders_2; }
	inline Dictionary_2_t1632706988 ** get_address_of_contentHeaders_2() { return &___contentHeaders_2; }
	inline void set_contentHeaders_2(Dictionary_2_t1632706988 * value)
	{
		___contentHeaders_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentHeaders_2), value);
	}

	inline static int32_t get_offset_of_headers_3() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStoreyA_t1430582499, ___headers_3)); }
	inline Dictionary_2_t1632706988 * get_headers_3() const { return ___headers_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_headers_3() { return &___headers_3; }
	inline void set_headers_3(Dictionary_2_t1632706988 * value)
	{
		___headers_3 = value;
		Il2CppCodeGenWriteBarrier((&___headers_3), value);
	}

	inline static int32_t get_offset_of_progress_4() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStoreyA_t1430582499, ___progress_4)); }
	inline RuntimeObject* get_progress_4() const { return ___progress_4; }
	inline RuntimeObject** get_address_of_progress_4() { return &___progress_4; }
	inline void set_progress_4(RuntimeObject* value)
	{
		___progress_4 = value;
		Il2CppCodeGenWriteBarrier((&___progress_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTU3EC__ANONSTOREYA_T1430582499_H
#ifndef U3CPOSTU3EC__ANONSTOREY9_T3546084579_H
#define U3CPOSTU3EC__ANONSTOREY9_T3546084579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<Post>c__AnonStorey9
struct  U3CPostU3Ec__AnonStorey9_t3546084579  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<Post>c__AnonStorey9::url
	String_t* ___url_0;
	// UnityEngine.WWWForm UniRx.ObservableWWW/<Post>c__AnonStorey9::content
	WWWForm_t4064702195 * ___content_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<Post>c__AnonStorey9::progress
	RuntimeObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey9_t3546084579, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey9_t3546084579, ___content_1)); }
	inline WWWForm_t4064702195 * get_content_1() const { return ___content_1; }
	inline WWWForm_t4064702195 ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(WWWForm_t4064702195 * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey9_t3546084579, ___progress_2)); }
	inline RuntimeObject* get_progress_2() const { return ___progress_2; }
	inline RuntimeObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(RuntimeObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier((&___progress_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTU3EC__ANONSTOREY9_T3546084579_H
#ifndef U3CPOSTU3EC__ANONSTOREY8_T1207432419_H
#define U3CPOSTU3EC__ANONSTOREY8_T1207432419_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<Post>c__AnonStorey8
struct  U3CPostU3Ec__AnonStorey8_t1207432419  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<Post>c__AnonStorey8::url
	String_t* ___url_0;
	// System.Byte[] UniRx.ObservableWWW/<Post>c__AnonStorey8::postData
	ByteU5BU5D_t4116647657* ___postData_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<Post>c__AnonStorey8::headers
	Dictionary_2_t1632706988 * ___headers_2;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<Post>c__AnonStorey8::progress
	RuntimeObject* ___progress_3;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey8_t1207432419, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_postData_1() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey8_t1207432419, ___postData_1)); }
	inline ByteU5BU5D_t4116647657* get_postData_1() const { return ___postData_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_postData_1() { return &___postData_1; }
	inline void set_postData_1(ByteU5BU5D_t4116647657* value)
	{
		___postData_1 = value;
		Il2CppCodeGenWriteBarrier((&___postData_1), value);
	}

	inline static int32_t get_offset_of_headers_2() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey8_t1207432419, ___headers_2)); }
	inline Dictionary_2_t1632706988 * get_headers_2() const { return ___headers_2; }
	inline Dictionary_2_t1632706988 ** get_address_of_headers_2() { return &___headers_2; }
	inline void set_headers_2(Dictionary_2_t1632706988 * value)
	{
		___headers_2 = value;
		Il2CppCodeGenWriteBarrier((&___headers_2), value);
	}

	inline static int32_t get_offset_of_progress_3() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey8_t1207432419, ___progress_3)); }
	inline RuntimeObject* get_progress_3() const { return ___progress_3; }
	inline RuntimeObject** get_address_of_progress_3() { return &___progress_3; }
	inline void set_progress_3(RuntimeObject* value)
	{
		___progress_3 = value;
		Il2CppCodeGenWriteBarrier((&___progress_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTU3EC__ANONSTOREY8_T1207432419_H
#ifndef U3CPOSTU3EC__ANONSTOREY7_T869725411_H
#define U3CPOSTU3EC__ANONSTOREY7_T869725411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<Post>c__AnonStorey7
struct  U3CPostU3Ec__AnonStorey7_t869725411  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<Post>c__AnonStorey7::url
	String_t* ___url_0;
	// System.Byte[] UniRx.ObservableWWW/<Post>c__AnonStorey7::postData
	ByteU5BU5D_t4116647657* ___postData_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<Post>c__AnonStorey7::progress
	RuntimeObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey7_t869725411, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_postData_1() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey7_t869725411, ___postData_1)); }
	inline ByteU5BU5D_t4116647657* get_postData_1() const { return ___postData_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_postData_1() { return &___postData_1; }
	inline void set_postData_1(ByteU5BU5D_t4116647657* value)
	{
		___postData_1 = value;
		Il2CppCodeGenWriteBarrier((&___postData_1), value);
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CPostU3Ec__AnonStorey7_t869725411, ___progress_2)); }
	inline RuntimeObject* get_progress_2() const { return ___progress_2; }
	inline RuntimeObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(RuntimeObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier((&___progress_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTU3EC__ANONSTOREY7_T869725411_H
#ifndef U3CGETWWWU3EC__ANONSTOREY6_T794373360_H
#define U3CGETWWWU3EC__ANONSTOREY6_T794373360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<GetWWW>c__AnonStorey6
struct  U3CGetWWWU3Ec__AnonStorey6_t794373360  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<GetWWW>c__AnonStorey6::url
	String_t* ___url_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<GetWWW>c__AnonStorey6::headers
	Dictionary_2_t1632706988 * ___headers_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<GetWWW>c__AnonStorey6::progress
	RuntimeObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CGetWWWU3Ec__AnonStorey6_t794373360, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_headers_1() { return static_cast<int32_t>(offsetof(U3CGetWWWU3Ec__AnonStorey6_t794373360, ___headers_1)); }
	inline Dictionary_2_t1632706988 * get_headers_1() const { return ___headers_1; }
	inline Dictionary_2_t1632706988 ** get_address_of_headers_1() { return &___headers_1; }
	inline void set_headers_1(Dictionary_2_t1632706988 * value)
	{
		___headers_1 = value;
		Il2CppCodeGenWriteBarrier((&___headers_1), value);
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CGetWWWU3Ec__AnonStorey6_t794373360, ___progress_2)); }
	inline RuntimeObject* get_progress_2() const { return ___progress_2; }
	inline RuntimeObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(RuntimeObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier((&___progress_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETWWWU3EC__ANONSTOREY6_T794373360_H
#ifndef U3CPOSTANDGETBYTESU3EC__ANONSTOREYE_T3769743018_H
#define U3CPOSTANDGETBYTESU3EC__ANONSTOREYE_T3769743018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyE
struct  U3CPostAndGetBytesU3Ec__AnonStoreyE_t3769743018  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyE::url
	String_t* ___url_0;
	// UnityEngine.WWWForm UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyE::content
	WWWForm_t4064702195 * ___content_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyE::contentHeaders
	Dictionary_2_t1632706988 * ___contentHeaders_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyE::headers
	Dictionary_2_t1632706988 * ___headers_3;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<PostAndGetBytes>c__AnonStoreyE::progress
	RuntimeObject* ___progress_4;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyE_t3769743018, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyE_t3769743018, ___content_1)); }
	inline WWWForm_t4064702195 * get_content_1() const { return ___content_1; }
	inline WWWForm_t4064702195 ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(WWWForm_t4064702195 * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_contentHeaders_2() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyE_t3769743018, ___contentHeaders_2)); }
	inline Dictionary_2_t1632706988 * get_contentHeaders_2() const { return ___contentHeaders_2; }
	inline Dictionary_2_t1632706988 ** get_address_of_contentHeaders_2() { return &___contentHeaders_2; }
	inline void set_contentHeaders_2(Dictionary_2_t1632706988 * value)
	{
		___contentHeaders_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentHeaders_2), value);
	}

	inline static int32_t get_offset_of_headers_3() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyE_t3769743018, ___headers_3)); }
	inline Dictionary_2_t1632706988 * get_headers_3() const { return ___headers_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_headers_3() { return &___headers_3; }
	inline void set_headers_3(Dictionary_2_t1632706988 * value)
	{
		___headers_3 = value;
		Il2CppCodeGenWriteBarrier((&___headers_3), value);
	}

	inline static int32_t get_offset_of_progress_4() { return static_cast<int32_t>(offsetof(U3CPostAndGetBytesU3Ec__AnonStoreyE_t3769743018, ___progress_4)); }
	inline RuntimeObject* get_progress_4() const { return ___progress_4; }
	inline RuntimeObject** get_address_of_progress_4() { return &___progress_4; }
	inline void set_progress_4(RuntimeObject* value)
	{
		___progress_4 = value;
		Il2CppCodeGenWriteBarrier((&___progress_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTANDGETBYTESU3EC__ANONSTOREYE_T3769743018_H
#ifndef U3CEMPTYENUMERATORU3EC__ITERATOR0_T3471758210_H
#define U3CEMPTYENUMERATORU3EC__ITERATOR0_T3471758210_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObserveExtensions/<EmptyEnumerator>c__Iterator0
struct  U3CEmptyEnumeratorU3Ec__Iterator0_t3471758210  : public RuntimeObject
{
public:
	// System.Object UniRx.ObserveExtensions/<EmptyEnumerator>c__Iterator0::$current
	RuntimeObject * ___U24current_0;
	// System.Boolean UniRx.ObserveExtensions/<EmptyEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_1;
	// System.Int32 UniRx.ObserveExtensions/<EmptyEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CEmptyEnumeratorU3Ec__Iterator0_t3471758210, ___U24current_0)); }
	inline RuntimeObject * get_U24current_0() const { return ___U24current_0; }
	inline RuntimeObject ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(RuntimeObject * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CEmptyEnumeratorU3Ec__Iterator0_t3471758210, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CEmptyEnumeratorU3Ec__Iterator0_t3471758210, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEMPTYENUMERATORU3EC__ITERATOR0_T3471758210_H
#ifndef REUSABLEENUMERATOR_T911091657_H
#define REUSABLEENUMERATOR_T911091657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BatchFrameObservable/BatchFrame/ReusableEnumerator
struct  ReusableEnumerator_t911091657  : public RuntimeObject
{
public:
	// UniRx.Operators.BatchFrameObservable/BatchFrame UniRx.Operators.BatchFrameObservable/BatchFrame/ReusableEnumerator::parent
	BatchFrame_t3337927563 * ___parent_0;
	// System.Int32 UniRx.Operators.BatchFrameObservable/BatchFrame/ReusableEnumerator::currentFrame
	int32_t ___currentFrame_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(ReusableEnumerator_t911091657, ___parent_0)); }
	inline BatchFrame_t3337927563 * get_parent_0() const { return ___parent_0; }
	inline BatchFrame_t3337927563 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(BatchFrame_t3337927563 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_currentFrame_1() { return static_cast<int32_t>(offsetof(ReusableEnumerator_t911091657, ___currentFrame_1)); }
	inline int32_t get_currentFrame_1() const { return ___currentFrame_1; }
	inline int32_t* get_address_of_currentFrame_1() { return &___currentFrame_1; }
	inline void set_currentFrame_1(int32_t value)
	{
		___currentFrame_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REUSABLEENUMERATOR_T911091657_H
#ifndef REACTIVECOLLECTIONEXTENSIONS_T1881372181_H
#define REACTIVECOLLECTIONEXTENSIONS_T1881372181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveCollectionExtensions
struct  ReactiveCollectionExtensions_t1881372181  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVECOLLECTIONEXTENSIONS_T1881372181_H
#ifndef U3CLOADFROMCACHEORDOWNLOADU3EC__ANONSTOREY13_T2957179115_H
#define U3CLOADFROMCACHEORDOWNLOADU3EC__ANONSTOREY13_T2957179115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey13
struct  U3CLoadFromCacheOrDownloadU3Ec__AnonStorey13_t2957179115  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey13::url
	String_t* ___url_0;
	// System.Int32 UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey13::version
	int32_t ___version_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey13::progress
	RuntimeObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey13_t2957179115, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey13_t2957179115, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey13_t2957179115, ___progress_2)); }
	inline RuntimeObject* get_progress_2() const { return ___progress_2; }
	inline RuntimeObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(RuntimeObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier((&___progress_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADFROMCACHEORDOWNLOADU3EC__ANONSTOREY13_T2957179115_H
#ifndef U3CPOSTWWWU3EC__ANONSTOREY12_T3399653964_H
#define U3CPOSTWWWU3EC__ANONSTOREY12_T3399653964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<PostWWW>c__AnonStorey12
struct  U3CPostWWWU3Ec__AnonStorey12_t3399653964  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<PostWWW>c__AnonStorey12::url
	String_t* ___url_0;
	// UnityEngine.WWWForm UniRx.ObservableWWW/<PostWWW>c__AnonStorey12::content
	WWWForm_t4064702195 * ___content_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<PostWWW>c__AnonStorey12::contentHeaders
	Dictionary_2_t1632706988 * ___contentHeaders_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<PostWWW>c__AnonStorey12::headers
	Dictionary_2_t1632706988 * ___headers_3;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<PostWWW>c__AnonStorey12::progress
	RuntimeObject* ___progress_4;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey12_t3399653964, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey12_t3399653964, ___content_1)); }
	inline WWWForm_t4064702195 * get_content_1() const { return ___content_1; }
	inline WWWForm_t4064702195 ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(WWWForm_t4064702195 * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_contentHeaders_2() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey12_t3399653964, ___contentHeaders_2)); }
	inline Dictionary_2_t1632706988 * get_contentHeaders_2() const { return ___contentHeaders_2; }
	inline Dictionary_2_t1632706988 ** get_address_of_contentHeaders_2() { return &___contentHeaders_2; }
	inline void set_contentHeaders_2(Dictionary_2_t1632706988 * value)
	{
		___contentHeaders_2 = value;
		Il2CppCodeGenWriteBarrier((&___contentHeaders_2), value);
	}

	inline static int32_t get_offset_of_headers_3() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey12_t3399653964, ___headers_3)); }
	inline Dictionary_2_t1632706988 * get_headers_3() const { return ___headers_3; }
	inline Dictionary_2_t1632706988 ** get_address_of_headers_3() { return &___headers_3; }
	inline void set_headers_3(Dictionary_2_t1632706988 * value)
	{
		___headers_3 = value;
		Il2CppCodeGenWriteBarrier((&___headers_3), value);
	}

	inline static int32_t get_offset_of_progress_4() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey12_t3399653964, ___progress_4)); }
	inline RuntimeObject* get_progress_4() const { return ___progress_4; }
	inline RuntimeObject** get_address_of_progress_4() { return &___progress_4; }
	inline void set_progress_4(RuntimeObject* value)
	{
		___progress_4 = value;
		Il2CppCodeGenWriteBarrier((&___progress_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTWWWU3EC__ANONSTOREY12_T3399653964_H
#ifndef U3CPOSTWWWU3EC__ANONSTOREY11_T1825675852_H
#define U3CPOSTWWWU3EC__ANONSTOREY11_T1825675852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<PostWWW>c__AnonStorey11
struct  U3CPostWWWU3Ec__AnonStorey11_t1825675852  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<PostWWW>c__AnonStorey11::url
	String_t* ___url_0;
	// UnityEngine.WWWForm UniRx.ObservableWWW/<PostWWW>c__AnonStorey11::content
	WWWForm_t4064702195 * ___content_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<PostWWW>c__AnonStorey11::progress
	RuntimeObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey11_t1825675852, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey11_t1825675852, ___content_1)); }
	inline WWWForm_t4064702195 * get_content_1() const { return ___content_1; }
	inline WWWForm_t4064702195 ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(WWWForm_t4064702195 * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey11_t1825675852, ___progress_2)); }
	inline RuntimeObject* get_progress_2() const { return ___progress_2; }
	inline RuntimeObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(RuntimeObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier((&___progress_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTWWWU3EC__ANONSTOREY11_T1825675852_H
#ifndef U3CPOSTWWWU3EC__ANONSTOREY10_T3781990988_H
#define U3CPOSTWWWU3EC__ANONSTOREY10_T3781990988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<PostWWW>c__AnonStorey10
struct  U3CPostWWWU3Ec__AnonStorey10_t3781990988  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<PostWWW>c__AnonStorey10::url
	String_t* ___url_0;
	// System.Byte[] UniRx.ObservableWWW/<PostWWW>c__AnonStorey10::postData
	ByteU5BU5D_t4116647657* ___postData_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<PostWWW>c__AnonStorey10::headers
	Dictionary_2_t1632706988 * ___headers_2;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<PostWWW>c__AnonStorey10::progress
	RuntimeObject* ___progress_3;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey10_t3781990988, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_postData_1() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey10_t3781990988, ___postData_1)); }
	inline ByteU5BU5D_t4116647657* get_postData_1() const { return ___postData_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_postData_1() { return &___postData_1; }
	inline void set_postData_1(ByteU5BU5D_t4116647657* value)
	{
		___postData_1 = value;
		Il2CppCodeGenWriteBarrier((&___postData_1), value);
	}

	inline static int32_t get_offset_of_headers_2() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey10_t3781990988, ___headers_2)); }
	inline Dictionary_2_t1632706988 * get_headers_2() const { return ___headers_2; }
	inline Dictionary_2_t1632706988 ** get_address_of_headers_2() { return &___headers_2; }
	inline void set_headers_2(Dictionary_2_t1632706988 * value)
	{
		___headers_2 = value;
		Il2CppCodeGenWriteBarrier((&___headers_2), value);
	}

	inline static int32_t get_offset_of_progress_3() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStorey10_t3781990988, ___progress_3)); }
	inline RuntimeObject* get_progress_3() const { return ___progress_3; }
	inline RuntimeObject** get_address_of_progress_3() { return &___progress_3; }
	inline void set_progress_3(RuntimeObject* value)
	{
		___progress_3 = value;
		Il2CppCodeGenWriteBarrier((&___progress_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTWWWU3EC__ANONSTOREY10_T3781990988_H
#ifndef U3CPOSTWWWU3EC__ANONSTOREYF_T1147649635_H
#define U3CPOSTWWWU3EC__ANONSTOREYF_T1147649635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<PostWWW>c__AnonStoreyF
struct  U3CPostWWWU3Ec__AnonStoreyF_t1147649635  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<PostWWW>c__AnonStoreyF::url
	String_t* ___url_0;
	// System.Byte[] UniRx.ObservableWWW/<PostWWW>c__AnonStoreyF::postData
	ByteU5BU5D_t4116647657* ___postData_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<PostWWW>c__AnonStoreyF::progress
	RuntimeObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStoreyF_t1147649635, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_postData_1() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStoreyF_t1147649635, ___postData_1)); }
	inline ByteU5BU5D_t4116647657* get_postData_1() const { return ___postData_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_postData_1() { return &___postData_1; }
	inline void set_postData_1(ByteU5BU5D_t4116647657* value)
	{
		___postData_1 = value;
		Il2CppCodeGenWriteBarrier((&___postData_1), value);
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CPostWWWU3Ec__AnonStoreyF_t1147649635, ___progress_2)); }
	inline RuntimeObject* get_progress_2() const { return ___progress_2; }
	inline RuntimeObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(RuntimeObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier((&___progress_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOSTWWWU3EC__ANONSTOREYF_T1147649635_H
#ifndef U3CGETANDGETBYTESU3EC__ANONSTOREY5_T2468502_H
#define U3CGETANDGETBYTESU3EC__ANONSTOREY5_T2468502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<GetAndGetBytes>c__AnonStorey5
struct  U3CGetAndGetBytesU3Ec__AnonStorey5_t2468502  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<GetAndGetBytes>c__AnonStorey5::url
	String_t* ___url_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<GetAndGetBytes>c__AnonStorey5::headers
	Dictionary_2_t1632706988 * ___headers_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<GetAndGetBytes>c__AnonStorey5::progress
	RuntimeObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CGetAndGetBytesU3Ec__AnonStorey5_t2468502, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_headers_1() { return static_cast<int32_t>(offsetof(U3CGetAndGetBytesU3Ec__AnonStorey5_t2468502, ___headers_1)); }
	inline Dictionary_2_t1632706988 * get_headers_1() const { return ___headers_1; }
	inline Dictionary_2_t1632706988 ** get_address_of_headers_1() { return &___headers_1; }
	inline void set_headers_1(Dictionary_2_t1632706988 * value)
	{
		___headers_1 = value;
		Il2CppCodeGenWriteBarrier((&___headers_1), value);
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CGetAndGetBytesU3Ec__AnonStorey5_t2468502, ___progress_2)); }
	inline RuntimeObject* get_progress_2() const { return ___progress_2; }
	inline RuntimeObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(RuntimeObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier((&___progress_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETANDGETBYTESU3EC__ANONSTOREY5_T2468502_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef OPERATOROBSERVABLEBASE_1_T354719556_H
#define OPERATOROBSERVABLEBASE_1_T354719556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObservableBase`1<UniRx.Unit>
struct  OperatorObservableBase_1_t354719556  : public RuntimeObject
{
public:
	// System.Boolean UniRx.Operators.OperatorObservableBase`1::isRequiredSubscribeOnCurrentThread
	bool ___isRequiredSubscribeOnCurrentThread_0;

public:
	inline static int32_t get_offset_of_isRequiredSubscribeOnCurrentThread_0() { return static_cast<int32_t>(offsetof(OperatorObservableBase_1_t354719556, ___isRequiredSubscribeOnCurrentThread_0)); }
	inline bool get_isRequiredSubscribeOnCurrentThread_0() const { return ___isRequiredSubscribeOnCurrentThread_0; }
	inline bool* get_address_of_isRequiredSubscribeOnCurrentThread_0() { return &___isRequiredSubscribeOnCurrentThread_0; }
	inline void set_isRequiredSubscribeOnCurrentThread_0(bool value)
	{
		___isRequiredSubscribeOnCurrentThread_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVABLEBASE_1_T354719556_H
#ifndef LAZYTASKEXTENSIONS_T3183993102_H
#define LAZYTASKEXTENSIONS_T3183993102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.LazyTaskExtensions
struct  LazyTaskExtensions_t3183993102  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYTASKEXTENSIONS_T3183993102_H
#ifndef U3CWHENALLCOREU3EC__ITERATOR0_T2684419830_H
#define U3CWHENALLCOREU3EC__ITERATOR0_T2684419830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.LazyTask/<WhenAllCore>c__Iterator0
struct  U3CWhenAllCoreU3Ec__Iterator0_t2684419830  : public RuntimeObject
{
public:
	// UnityEngine.Coroutine[] UniRx.LazyTask/<WhenAllCore>c__Iterator0::coroutines
	CoroutineU5BU5D_t1698289742* ___coroutines_0;
	// UnityEngine.Coroutine[] UniRx.LazyTask/<WhenAllCore>c__Iterator0::$locvar0
	CoroutineU5BU5D_t1698289742* ___U24locvar0_1;
	// System.Int32 UniRx.LazyTask/<WhenAllCore>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// UnityEngine.Coroutine UniRx.LazyTask/<WhenAllCore>c__Iterator0::<item>__1
	Coroutine_t3829159415 * ___U3CitemU3E__1_3;
	// System.Object UniRx.LazyTask/<WhenAllCore>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean UniRx.LazyTask/<WhenAllCore>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 UniRx.LazyTask/<WhenAllCore>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_coroutines_0() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator0_t2684419830, ___coroutines_0)); }
	inline CoroutineU5BU5D_t1698289742* get_coroutines_0() const { return ___coroutines_0; }
	inline CoroutineU5BU5D_t1698289742** get_address_of_coroutines_0() { return &___coroutines_0; }
	inline void set_coroutines_0(CoroutineU5BU5D_t1698289742* value)
	{
		___coroutines_0 = value;
		Il2CppCodeGenWriteBarrier((&___coroutines_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator0_t2684419830, ___U24locvar0_1)); }
	inline CoroutineU5BU5D_t1698289742* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline CoroutineU5BU5D_t1698289742** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(CoroutineU5BU5D_t1698289742* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator0_t2684419830, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U3CitemU3E__1_3() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator0_t2684419830, ___U3CitemU3E__1_3)); }
	inline Coroutine_t3829159415 * get_U3CitemU3E__1_3() const { return ___U3CitemU3E__1_3; }
	inline Coroutine_t3829159415 ** get_address_of_U3CitemU3E__1_3() { return &___U3CitemU3E__1_3; }
	inline void set_U3CitemU3E__1_3(Coroutine_t3829159415 * value)
	{
		___U3CitemU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CitemU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator0_t2684419830, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator0_t2684419830, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CWhenAllCoreU3Ec__Iterator0_t2684419830, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWHENALLCOREU3EC__ITERATOR0_T2684419830_H
#ifndef OPERATOROBSERVERBASE_2_T3781804006_H
#define OPERATOROBSERVERBASE_2_T3781804006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.OperatorObserverBase`2<UniRx.Unit,UniRx.Unit>
struct  OperatorObserverBase_2_t3781804006  : public RuntimeObject
{
public:
	// UniRx.IObserver`1<TResult> modreq(System.Runtime.CompilerServices.IsVolatile) UniRx.Operators.OperatorObserverBase`2::observer
	RuntimeObject* ___observer_0;
	// System.IDisposable UniRx.Operators.OperatorObserverBase`2::cancel
	RuntimeObject* ___cancel_1;

public:
	inline static int32_t get_offset_of_observer_0() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t3781804006, ___observer_0)); }
	inline RuntimeObject* get_observer_0() const { return ___observer_0; }
	inline RuntimeObject** get_address_of_observer_0() { return &___observer_0; }
	inline void set_observer_0(RuntimeObject* value)
	{
		___observer_0 = value;
		Il2CppCodeGenWriteBarrier((&___observer_0), value);
	}

	inline static int32_t get_offset_of_cancel_1() { return static_cast<int32_t>(offsetof(OperatorObserverBase_2_t3781804006, ___cancel_1)); }
	inline RuntimeObject* get_cancel_1() const { return ___cancel_1; }
	inline RuntimeObject** get_address_of_cancel_1() { return &___cancel_1; }
	inline void set_cancel_1(RuntimeObject* value)
	{
		___cancel_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancel_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATOROBSERVERBASE_2_T3781804006_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CSENDSTARTCOROUTINEU3EC__ANONSTOREY3_T1416387401_H
#define U3CSENDSTARTCOROUTINEU3EC__ANONSTOREY3_T1416387401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MainThreadDispatcher/<SendStartCoroutine>c__AnonStorey3
struct  U3CSendStartCoroutineU3Ec__AnonStorey3_t1416387401  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator UniRx.MainThreadDispatcher/<SendStartCoroutine>c__AnonStorey3::routine
	RuntimeObject* ___routine_0;

public:
	inline static int32_t get_offset_of_routine_0() { return static_cast<int32_t>(offsetof(U3CSendStartCoroutineU3Ec__AnonStorey3_t1416387401, ___routine_0)); }
	inline RuntimeObject* get_routine_0() const { return ___routine_0; }
	inline RuntimeObject** get_address_of_routine_0() { return &___routine_0; }
	inline void set_routine_0(RuntimeObject* value)
	{
		___routine_0 = value;
		Il2CppCodeGenWriteBarrier((&___routine_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDSTARTCOROUTINEU3EC__ANONSTOREY3_T1416387401_H
#ifndef U3CGETU3EC__ANONSTOREY4_T1801188099_H
#define U3CGETU3EC__ANONSTOREY4_T1801188099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<Get>c__AnonStorey4
struct  U3CGetU3Ec__AnonStorey4_t1801188099  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<Get>c__AnonStorey4::url
	String_t* ___url_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.ObservableWWW/<Get>c__AnonStorey4::headers
	Dictionary_2_t1632706988 * ___headers_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<Get>c__AnonStorey4::progress
	RuntimeObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CGetU3Ec__AnonStorey4_t1801188099, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_headers_1() { return static_cast<int32_t>(offsetof(U3CGetU3Ec__AnonStorey4_t1801188099, ___headers_1)); }
	inline Dictionary_2_t1632706988 * get_headers_1() const { return ___headers_1; }
	inline Dictionary_2_t1632706988 ** get_address_of_headers_1() { return &___headers_1; }
	inline void set_headers_1(Dictionary_2_t1632706988 * value)
	{
		___headers_1 = value;
		Il2CppCodeGenWriteBarrier((&___headers_1), value);
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CGetU3Ec__AnonStorey4_t1801188099, ___progress_2)); }
	inline RuntimeObject* get_progress_2() const { return ___progress_2; }
	inline RuntimeObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(RuntimeObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier((&___progress_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETU3EC__ANONSTOREY4_T1801188099_H
#ifndef OBSERVABLEWWW_T1879783302_H
#define OBSERVABLEWWW_T1879783302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW
struct  ObservableWWW_t1879783302  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVABLEWWW_T1879783302_H
#ifndef OBSERVEEXTENSIONS_T3076931129_H
#define OBSERVEEXTENSIONS_T3076931129_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObserveExtensions
struct  ObserveExtensions_t3076931129  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBSERVEEXTENSIONS_T3076931129_H
#ifndef FRAMECOUNTTYPEEXTENSIONS_T2621917262_H
#define FRAMECOUNTTYPEEXTENSIONS_T2621917262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.FrameCountTypeExtensions
struct  FrameCountTypeExtensions_t2621917262  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMECOUNTTYPEEXTENSIONS_T2621917262_H
#ifndef U3CRUNENDOFFRAMEMICROCOROUTINEU3EC__ITERATOR2_T4169616078_H
#define U3CRUNENDOFFRAMEMICROCOROUTINEU3EC__ITERATOR2_T4169616078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MainThreadDispatcher/<RunEndOfFrameMicroCoroutine>c__Iterator2
struct  U3CRunEndOfFrameMicroCoroutineU3Ec__Iterator2_t4169616078  : public RuntimeObject
{
public:
	// UniRx.MainThreadDispatcher UniRx.MainThreadDispatcher/<RunEndOfFrameMicroCoroutine>c__Iterator2::$this
	MainThreadDispatcher_t3684499304 * ___U24this_0;
	// System.Object UniRx.MainThreadDispatcher/<RunEndOfFrameMicroCoroutine>c__Iterator2::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UniRx.MainThreadDispatcher/<RunEndOfFrameMicroCoroutine>c__Iterator2::$disposing
	bool ___U24disposing_2;
	// System.Int32 UniRx.MainThreadDispatcher/<RunEndOfFrameMicroCoroutine>c__Iterator2::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRunEndOfFrameMicroCoroutineU3Ec__Iterator2_t4169616078, ___U24this_0)); }
	inline MainThreadDispatcher_t3684499304 * get_U24this_0() const { return ___U24this_0; }
	inline MainThreadDispatcher_t3684499304 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MainThreadDispatcher_t3684499304 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRunEndOfFrameMicroCoroutineU3Ec__Iterator2_t4169616078, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRunEndOfFrameMicroCoroutineU3Ec__Iterator2_t4169616078, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRunEndOfFrameMicroCoroutineU3Ec__Iterator2_t4169616078, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNENDOFFRAMEMICROCOROUTINEU3EC__ITERATOR2_T4169616078_H
#ifndef U3CRUNUPDATEMICROCOROUTINEU3EC__ITERATOR0_T3229616799_H
#define U3CRUNUPDATEMICROCOROUTINEU3EC__ITERATOR0_T3229616799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MainThreadDispatcher/<RunUpdateMicroCoroutine>c__Iterator0
struct  U3CRunUpdateMicroCoroutineU3Ec__Iterator0_t3229616799  : public RuntimeObject
{
public:
	// UniRx.MainThreadDispatcher UniRx.MainThreadDispatcher/<RunUpdateMicroCoroutine>c__Iterator0::$this
	MainThreadDispatcher_t3684499304 * ___U24this_0;
	// System.Object UniRx.MainThreadDispatcher/<RunUpdateMicroCoroutine>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UniRx.MainThreadDispatcher/<RunUpdateMicroCoroutine>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UniRx.MainThreadDispatcher/<RunUpdateMicroCoroutine>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRunUpdateMicroCoroutineU3Ec__Iterator0_t3229616799, ___U24this_0)); }
	inline MainThreadDispatcher_t3684499304 * get_U24this_0() const { return ___U24this_0; }
	inline MainThreadDispatcher_t3684499304 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MainThreadDispatcher_t3684499304 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRunUpdateMicroCoroutineU3Ec__Iterator0_t3229616799, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRunUpdateMicroCoroutineU3Ec__Iterator0_t3229616799, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRunUpdateMicroCoroutineU3Ec__Iterator0_t3229616799, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNUPDATEMICROCOROUTINEU3EC__ITERATOR0_T3229616799_H
#ifndef U3CRUNFIXEDUPDATEMICROCOROUTINEU3EC__ITERATOR1_T4089570234_H
#define U3CRUNFIXEDUPDATEMICROCOROUTINEU3EC__ITERATOR1_T4089570234_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MainThreadDispatcher/<RunFixedUpdateMicroCoroutine>c__Iterator1
struct  U3CRunFixedUpdateMicroCoroutineU3Ec__Iterator1_t4089570234  : public RuntimeObject
{
public:
	// UniRx.MainThreadDispatcher UniRx.MainThreadDispatcher/<RunFixedUpdateMicroCoroutine>c__Iterator1::$this
	MainThreadDispatcher_t3684499304 * ___U24this_0;
	// System.Object UniRx.MainThreadDispatcher/<RunFixedUpdateMicroCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UniRx.MainThreadDispatcher/<RunFixedUpdateMicroCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 UniRx.MainThreadDispatcher/<RunFixedUpdateMicroCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CRunFixedUpdateMicroCoroutineU3Ec__Iterator1_t4089570234, ___U24this_0)); }
	inline MainThreadDispatcher_t3684499304 * get_U24this_0() const { return ___U24this_0; }
	inline MainThreadDispatcher_t3684499304 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MainThreadDispatcher_t3684499304 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CRunFixedUpdateMicroCoroutineU3Ec__Iterator1_t4089570234, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CRunFixedUpdateMicroCoroutineU3Ec__Iterator1_t4089570234, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CRunFixedUpdateMicroCoroutineU3Ec__Iterator1_t4089570234, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRUNFIXEDUPDATEMICROCOROUTINEU3EC__ITERATOR1_T4089570234_H
#ifndef HASH128_T2357739769_H
#define HASH128_T2357739769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Hash128
struct  Hash128_t2357739769 
{
public:
	// System.UInt32 UnityEngine.Hash128::m_u32_0
	uint32_t ___m_u32_0_0;
	// System.UInt32 UnityEngine.Hash128::m_u32_1
	uint32_t ___m_u32_1_1;
	// System.UInt32 UnityEngine.Hash128::m_u32_2
	uint32_t ___m_u32_2_2;
	// System.UInt32 UnityEngine.Hash128::m_u32_3
	uint32_t ___m_u32_3_3;

public:
	inline static int32_t get_offset_of_m_u32_0_0() { return static_cast<int32_t>(offsetof(Hash128_t2357739769, ___m_u32_0_0)); }
	inline uint32_t get_m_u32_0_0() const { return ___m_u32_0_0; }
	inline uint32_t* get_address_of_m_u32_0_0() { return &___m_u32_0_0; }
	inline void set_m_u32_0_0(uint32_t value)
	{
		___m_u32_0_0 = value;
	}

	inline static int32_t get_offset_of_m_u32_1_1() { return static_cast<int32_t>(offsetof(Hash128_t2357739769, ___m_u32_1_1)); }
	inline uint32_t get_m_u32_1_1() const { return ___m_u32_1_1; }
	inline uint32_t* get_address_of_m_u32_1_1() { return &___m_u32_1_1; }
	inline void set_m_u32_1_1(uint32_t value)
	{
		___m_u32_1_1 = value;
	}

	inline static int32_t get_offset_of_m_u32_2_2() { return static_cast<int32_t>(offsetof(Hash128_t2357739769, ___m_u32_2_2)); }
	inline uint32_t get_m_u32_2_2() const { return ___m_u32_2_2; }
	inline uint32_t* get_address_of_m_u32_2_2() { return &___m_u32_2_2; }
	inline void set_m_u32_2_2(uint32_t value)
	{
		___m_u32_2_2 = value;
	}

	inline static int32_t get_offset_of_m_u32_3_3() { return static_cast<int32_t>(offsetof(Hash128_t2357739769, ___m_u32_3_3)); }
	inline uint32_t get_m_u32_3_3() const { return ___m_u32_3_3; }
	inline uint32_t* get_address_of_m_u32_3_3() { return &___m_u32_3_3; }
	inline void set_m_u32_3_3(uint32_t value)
	{
		___m_u32_3_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASH128_T2357739769_H
#ifndef UNIT_T3362249467_H
#define UNIT_T3362249467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Unit
struct  Unit_t3362249467 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Unit_t3362249467__padding[1];
	};

public:
};

struct Unit_t3362249467_StaticFields
{
public:
	// UniRx.Unit UniRx.Unit::default
	Unit_t3362249467  ___default_0;

public:
	inline static int32_t get_offset_of_default_0() { return static_cast<int32_t>(offsetof(Unit_t3362249467_StaticFields, ___default_0)); }
	inline Unit_t3362249467  get_default_0() const { return ___default_0; }
	inline Unit_t3362249467 * get_address_of_default_0() { return &___default_0; }
	inline void set_default_0(Unit_t3362249467  value)
	{
		___default_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_T3362249467_H
#ifndef BATCHFRAME_T3337927563_H
#define BATCHFRAME_T3337927563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BatchFrameObservable/BatchFrame
struct  BatchFrame_t3337927563  : public OperatorObserverBase_2_t3781804006
{
public:
	// UniRx.Operators.BatchFrameObservable UniRx.Operators.BatchFrameObservable/BatchFrame::parent
	BatchFrameObservable_t2539759103 * ___parent_2;
	// System.Object UniRx.Operators.BatchFrameObservable/BatchFrame::gate
	RuntimeObject * ___gate_3;
	// UniRx.BooleanDisposable UniRx.Operators.BatchFrameObservable/BatchFrame::cancellationToken
	BooleanDisposable_t84760918 * ___cancellationToken_4;
	// System.Collections.IEnumerator UniRx.Operators.BatchFrameObservable/BatchFrame::timer
	RuntimeObject* ___timer_5;
	// System.Boolean UniRx.Operators.BatchFrameObservable/BatchFrame::isRunning
	bool ___isRunning_6;
	// System.Boolean UniRx.Operators.BatchFrameObservable/BatchFrame::isCompleted
	bool ___isCompleted_7;

public:
	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(BatchFrame_t3337927563, ___parent_2)); }
	inline BatchFrameObservable_t2539759103 * get_parent_2() const { return ___parent_2; }
	inline BatchFrameObservable_t2539759103 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(BatchFrameObservable_t2539759103 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___parent_2), value);
	}

	inline static int32_t get_offset_of_gate_3() { return static_cast<int32_t>(offsetof(BatchFrame_t3337927563, ___gate_3)); }
	inline RuntimeObject * get_gate_3() const { return ___gate_3; }
	inline RuntimeObject ** get_address_of_gate_3() { return &___gate_3; }
	inline void set_gate_3(RuntimeObject * value)
	{
		___gate_3 = value;
		Il2CppCodeGenWriteBarrier((&___gate_3), value);
	}

	inline static int32_t get_offset_of_cancellationToken_4() { return static_cast<int32_t>(offsetof(BatchFrame_t3337927563, ___cancellationToken_4)); }
	inline BooleanDisposable_t84760918 * get_cancellationToken_4() const { return ___cancellationToken_4; }
	inline BooleanDisposable_t84760918 ** get_address_of_cancellationToken_4() { return &___cancellationToken_4; }
	inline void set_cancellationToken_4(BooleanDisposable_t84760918 * value)
	{
		___cancellationToken_4 = value;
		Il2CppCodeGenWriteBarrier((&___cancellationToken_4), value);
	}

	inline static int32_t get_offset_of_timer_5() { return static_cast<int32_t>(offsetof(BatchFrame_t3337927563, ___timer_5)); }
	inline RuntimeObject* get_timer_5() const { return ___timer_5; }
	inline RuntimeObject** get_address_of_timer_5() { return &___timer_5; }
	inline void set_timer_5(RuntimeObject* value)
	{
		___timer_5 = value;
		Il2CppCodeGenWriteBarrier((&___timer_5), value);
	}

	inline static int32_t get_offset_of_isRunning_6() { return static_cast<int32_t>(offsetof(BatchFrame_t3337927563, ___isRunning_6)); }
	inline bool get_isRunning_6() const { return ___isRunning_6; }
	inline bool* get_address_of_isRunning_6() { return &___isRunning_6; }
	inline void set_isRunning_6(bool value)
	{
		___isRunning_6 = value;
	}

	inline static int32_t get_offset_of_isCompleted_7() { return static_cast<int32_t>(offsetof(BatchFrame_t3337927563, ___isCompleted_7)); }
	inline bool get_isCompleted_7() const { return ___isCompleted_7; }
	inline bool* get_address_of_isCompleted_7() { return &___isCompleted_7; }
	inline void set_isCompleted_7(bool value)
	{
		___isCompleted_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATCHFRAME_T3337927563_H
#ifndef PROPERTYATTRIBUTE_T3677895545_H
#define PROPERTYATTRIBUTE_T3677895545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t3677895545  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T3677895545_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef CANCELLATIONTOKEN_T1265546479_H
#define CANCELLATIONTOKEN_T1265546479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.CancellationToken
struct  CancellationToken_t1265546479 
{
public:
	// UniRx.ICancelable UniRx.CancellationToken::source
	RuntimeObject* ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479, ___source_0)); }
	inline RuntimeObject* get_source_0() const { return ___source_0; }
	inline RuntimeObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(RuntimeObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}
};

struct CancellationToken_t1265546479_StaticFields
{
public:
	// UniRx.CancellationToken UniRx.CancellationToken::Empty
	CancellationToken_t1265546479  ___Empty_1;
	// UniRx.CancellationToken UniRx.CancellationToken::None
	CancellationToken_t1265546479  ___None_2;

public:
	inline static int32_t get_offset_of_Empty_1() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479_StaticFields, ___Empty_1)); }
	inline CancellationToken_t1265546479  get_Empty_1() const { return ___Empty_1; }
	inline CancellationToken_t1265546479 * get_address_of_Empty_1() { return &___Empty_1; }
	inline void set_Empty_1(CancellationToken_t1265546479  value)
	{
		___Empty_1 = value;
	}

	inline static int32_t get_offset_of_None_2() { return static_cast<int32_t>(offsetof(CancellationToken_t1265546479_StaticFields, ___None_2)); }
	inline CancellationToken_t1265546479  get_None_2() const { return ___None_2; }
	inline CancellationToken_t1265546479 * get_address_of_None_2() { return &___None_2; }
	inline void set_None_2(CancellationToken_t1265546479  value)
	{
		___None_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UniRx.CancellationToken
struct CancellationToken_t1265546479_marshaled_pinvoke
{
	RuntimeObject* ___source_0;
};
// Native definition for COM marshalling of UniRx.CancellationToken
struct CancellationToken_t1265546479_marshaled_com
{
	RuntimeObject* ___source_0;
};
#endif // CANCELLATIONTOKEN_T1265546479_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INSPECTORDISPLAYATTRIBUTE_T2165086330_H
#define INSPECTORDISPLAYATTRIBUTE_T2165086330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.InspectorDisplayAttribute
struct  InspectorDisplayAttribute_t2165086330  : public PropertyAttribute_t3677895545
{
public:
	// System.String UniRx.InspectorDisplayAttribute::<FieldName>k__BackingField
	String_t* ___U3CFieldNameU3Ek__BackingField_0;
	// System.Boolean UniRx.InspectorDisplayAttribute::<NotifyPropertyChanged>k__BackingField
	bool ___U3CNotifyPropertyChangedU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFieldNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InspectorDisplayAttribute_t2165086330, ___U3CFieldNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CFieldNameU3Ek__BackingField_0() const { return ___U3CFieldNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CFieldNameU3Ek__BackingField_0() { return &___U3CFieldNameU3Ek__BackingField_0; }
	inline void set_U3CFieldNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CFieldNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFieldNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CNotifyPropertyChangedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InspectorDisplayAttribute_t2165086330, ___U3CNotifyPropertyChangedU3Ek__BackingField_1)); }
	inline bool get_U3CNotifyPropertyChangedU3Ek__BackingField_1() const { return ___U3CNotifyPropertyChangedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CNotifyPropertyChangedU3Ek__BackingField_1() { return &___U3CNotifyPropertyChangedU3Ek__BackingField_1; }
	inline void set_U3CNotifyPropertyChangedU3Ek__BackingField_1(bool value)
	{
		___U3CNotifyPropertyChangedU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSPECTORDISPLAYATTRIBUTE_T2165086330_H
#ifndef MAINTHREADDISPATCHTYPE_T3527556734_H
#define MAINTHREADDISPATCHTYPE_T3527556734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MainThreadDispatchType
struct  MainThreadDispatchType_t3527556734 
{
public:
	// System.Int32 UniRx.MainThreadDispatchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MainThreadDispatchType_t3527556734, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINTHREADDISPATCHTYPE_T3527556734_H
#ifndef FRAMECOUNTTYPE_T3331626185_H
#define FRAMECOUNTTYPE_T3331626185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.FrameCountType
struct  FrameCountType_t3331626185 
{
public:
	// System.Int32 UniRx.FrameCountType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FrameCountType_t3331626185, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMECOUNTTYPE_T3331626185_H
#ifndef CULLINGMODE_T3030417749_H
#define CULLINGMODE_T3030417749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MainThreadDispatcher/CullingMode
struct  CullingMode_t3030417749 
{
public:
	// System.Int32 UniRx.MainThreadDispatcher/CullingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CullingMode_t3030417749, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULLINGMODE_T3030417749_H
#ifndef HTTPSTATUSCODE_T3035121829_H
#define HTTPSTATUSCODE_T3035121829_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStatusCode
struct  HttpStatusCode_t3035121829 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpStatusCode_t3035121829, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_T3035121829_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RANGEREACTIVEPROPERTYATTRIBUTE_T2057313354_H
#define RANGEREACTIVEPROPERTYATTRIBUTE_T2057313354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.RangeReactivePropertyAttribute
struct  RangeReactivePropertyAttribute_t2057313354  : public PropertyAttribute_t3677895545
{
public:
	// System.Single UniRx.RangeReactivePropertyAttribute::<Min>k__BackingField
	float ___U3CMinU3Ek__BackingField_0;
	// System.Single UniRx.RangeReactivePropertyAttribute::<Max>k__BackingField
	float ___U3CMaxU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CMinU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RangeReactivePropertyAttribute_t2057313354, ___U3CMinU3Ek__BackingField_0)); }
	inline float get_U3CMinU3Ek__BackingField_0() const { return ___U3CMinU3Ek__BackingField_0; }
	inline float* get_address_of_U3CMinU3Ek__BackingField_0() { return &___U3CMinU3Ek__BackingField_0; }
	inline void set_U3CMinU3Ek__BackingField_0(float value)
	{
		___U3CMinU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CMaxU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RangeReactivePropertyAttribute_t2057313354, ___U3CMaxU3Ek__BackingField_1)); }
	inline float get_U3CMaxU3Ek__BackingField_1() const { return ___U3CMaxU3Ek__BackingField_1; }
	inline float* get_address_of_U3CMaxU3Ek__BackingField_1() { return &___U3CMaxU3Ek__BackingField_1; }
	inline void set_U3CMaxU3Ek__BackingField_1(float value)
	{
		___U3CMaxU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEREACTIVEPROPERTYATTRIBUTE_T2057313354_H
#ifndef MULTILINEREACTIVEPROPERTYATTRIBUTE_T2064446684_H
#define MULTILINEREACTIVEPROPERTYATTRIBUTE_T2064446684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MultilineReactivePropertyAttribute
struct  MultilineReactivePropertyAttribute_t2064446684  : public PropertyAttribute_t3677895545
{
public:
	// System.Int32 UniRx.MultilineReactivePropertyAttribute::<Lines>k__BackingField
	int32_t ___U3CLinesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CLinesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MultilineReactivePropertyAttribute_t2064446684, ___U3CLinesU3Ek__BackingField_0)); }
	inline int32_t get_U3CLinesU3Ek__BackingField_0() const { return ___U3CLinesU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CLinesU3Ek__BackingField_0() { return &___U3CLinesU3Ek__BackingField_0; }
	inline void set_U3CLinesU3Ek__BackingField_0(int32_t value)
	{
		___U3CLinesU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTILINEREACTIVEPROPERTYATTRIBUTE_T2064446684_H
#ifndef TASKSTATUS_T4183509909_H
#define TASKSTATUS_T4183509909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.LazyTask/TaskStatus
struct  TaskStatus_t4183509909 
{
public:
	// System.Int32 UniRx.LazyTask/TaskStatus::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TaskStatus_t4183509909, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKSTATUS_T4183509909_H
#ifndef U3CFETCHASSETBUNDLEU3EC__ITERATOR3_T3693207334_H
#define U3CFETCHASSETBUNDLEU3EC__ITERATOR3_T3693207334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator3
struct  U3CFetchAssetBundleU3Ec__Iterator3_t3693207334  : public RuntimeObject
{
public:
	// UnityEngine.WWW UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator3::www
	WWW_t3688466362 * ___www_0;
	// UnityEngine.WWW UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator3::$locvar0
	WWW_t3688466362 * ___U24locvar0_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator3::reportProgress
	RuntimeObject* ___reportProgress_2;
	// UniRx.CancellationToken UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator3::cancel
	CancellationToken_t1265546479  ___cancel_3;
	// UniRx.IObserver`1<UnityEngine.AssetBundle> UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator3::observer
	RuntimeObject* ___observer_4;
	// System.Object UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator3::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator3::$disposing
	bool ___U24disposing_6;
	// System.Int32 UniRx.ObservableWWW/<FetchAssetBundle>c__Iterator3::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_www_0() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator3_t3693207334, ___www_0)); }
	inline WWW_t3688466362 * get_www_0() const { return ___www_0; }
	inline WWW_t3688466362 ** get_address_of_www_0() { return &___www_0; }
	inline void set_www_0(WWW_t3688466362 * value)
	{
		___www_0 = value;
		Il2CppCodeGenWriteBarrier((&___www_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator3_t3693207334, ___U24locvar0_1)); }
	inline WWW_t3688466362 * get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline WWW_t3688466362 ** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(WWW_t3688466362 * value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_reportProgress_2() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator3_t3693207334, ___reportProgress_2)); }
	inline RuntimeObject* get_reportProgress_2() const { return ___reportProgress_2; }
	inline RuntimeObject** get_address_of_reportProgress_2() { return &___reportProgress_2; }
	inline void set_reportProgress_2(RuntimeObject* value)
	{
		___reportProgress_2 = value;
		Il2CppCodeGenWriteBarrier((&___reportProgress_2), value);
	}

	inline static int32_t get_offset_of_cancel_3() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator3_t3693207334, ___cancel_3)); }
	inline CancellationToken_t1265546479  get_cancel_3() const { return ___cancel_3; }
	inline CancellationToken_t1265546479 * get_address_of_cancel_3() { return &___cancel_3; }
	inline void set_cancel_3(CancellationToken_t1265546479  value)
	{
		___cancel_3 = value;
	}

	inline static int32_t get_offset_of_observer_4() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator3_t3693207334, ___observer_4)); }
	inline RuntimeObject* get_observer_4() const { return ___observer_4; }
	inline RuntimeObject** get_address_of_observer_4() { return &___observer_4; }
	inline void set_observer_4(RuntimeObject* value)
	{
		___observer_4 = value;
		Il2CppCodeGenWriteBarrier((&___observer_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator3_t3693207334, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator3_t3693207334, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFetchAssetBundleU3Ec__Iterator3_t3693207334, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHASSETBUNDLEU3EC__ITERATOR3_T3693207334_H
#ifndef U3CFETCHBYTESU3EC__ITERATOR2_T135672608_H
#define U3CFETCHBYTESU3EC__ITERATOR2_T135672608_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<FetchBytes>c__Iterator2
struct  U3CFetchBytesU3Ec__Iterator2_t135672608  : public RuntimeObject
{
public:
	// UnityEngine.WWW UniRx.ObservableWWW/<FetchBytes>c__Iterator2::www
	WWW_t3688466362 * ___www_0;
	// UnityEngine.WWW UniRx.ObservableWWW/<FetchBytes>c__Iterator2::$locvar0
	WWW_t3688466362 * ___U24locvar0_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<FetchBytes>c__Iterator2::reportProgress
	RuntimeObject* ___reportProgress_2;
	// UniRx.CancellationToken UniRx.ObservableWWW/<FetchBytes>c__Iterator2::cancel
	CancellationToken_t1265546479  ___cancel_3;
	// UniRx.IObserver`1<System.Byte[]> UniRx.ObservableWWW/<FetchBytes>c__Iterator2::observer
	RuntimeObject* ___observer_4;
	// System.Object UniRx.ObservableWWW/<FetchBytes>c__Iterator2::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean UniRx.ObservableWWW/<FetchBytes>c__Iterator2::$disposing
	bool ___U24disposing_6;
	// System.Int32 UniRx.ObservableWWW/<FetchBytes>c__Iterator2::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_www_0() { return static_cast<int32_t>(offsetof(U3CFetchBytesU3Ec__Iterator2_t135672608, ___www_0)); }
	inline WWW_t3688466362 * get_www_0() const { return ___www_0; }
	inline WWW_t3688466362 ** get_address_of_www_0() { return &___www_0; }
	inline void set_www_0(WWW_t3688466362 * value)
	{
		___www_0 = value;
		Il2CppCodeGenWriteBarrier((&___www_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CFetchBytesU3Ec__Iterator2_t135672608, ___U24locvar0_1)); }
	inline WWW_t3688466362 * get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline WWW_t3688466362 ** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(WWW_t3688466362 * value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_reportProgress_2() { return static_cast<int32_t>(offsetof(U3CFetchBytesU3Ec__Iterator2_t135672608, ___reportProgress_2)); }
	inline RuntimeObject* get_reportProgress_2() const { return ___reportProgress_2; }
	inline RuntimeObject** get_address_of_reportProgress_2() { return &___reportProgress_2; }
	inline void set_reportProgress_2(RuntimeObject* value)
	{
		___reportProgress_2 = value;
		Il2CppCodeGenWriteBarrier((&___reportProgress_2), value);
	}

	inline static int32_t get_offset_of_cancel_3() { return static_cast<int32_t>(offsetof(U3CFetchBytesU3Ec__Iterator2_t135672608, ___cancel_3)); }
	inline CancellationToken_t1265546479  get_cancel_3() const { return ___cancel_3; }
	inline CancellationToken_t1265546479 * get_address_of_cancel_3() { return &___cancel_3; }
	inline void set_cancel_3(CancellationToken_t1265546479  value)
	{
		___cancel_3 = value;
	}

	inline static int32_t get_offset_of_observer_4() { return static_cast<int32_t>(offsetof(U3CFetchBytesU3Ec__Iterator2_t135672608, ___observer_4)); }
	inline RuntimeObject* get_observer_4() const { return ___observer_4; }
	inline RuntimeObject** get_address_of_observer_4() { return &___observer_4; }
	inline void set_observer_4(RuntimeObject* value)
	{
		___observer_4 = value;
		Il2CppCodeGenWriteBarrier((&___observer_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFetchBytesU3Ec__Iterator2_t135672608, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFetchBytesU3Ec__Iterator2_t135672608, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFetchBytesU3Ec__Iterator2_t135672608, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHBYTESU3EC__ITERATOR2_T135672608_H
#ifndef U3CFETCHTEXTU3EC__ITERATOR1_T3169890160_H
#define U3CFETCHTEXTU3EC__ITERATOR1_T3169890160_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<FetchText>c__Iterator1
struct  U3CFetchTextU3Ec__Iterator1_t3169890160  : public RuntimeObject
{
public:
	// UnityEngine.WWW UniRx.ObservableWWW/<FetchText>c__Iterator1::www
	WWW_t3688466362 * ___www_0;
	// UnityEngine.WWW UniRx.ObservableWWW/<FetchText>c__Iterator1::$locvar0
	WWW_t3688466362 * ___U24locvar0_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<FetchText>c__Iterator1::reportProgress
	RuntimeObject* ___reportProgress_2;
	// UniRx.CancellationToken UniRx.ObservableWWW/<FetchText>c__Iterator1::cancel
	CancellationToken_t1265546479  ___cancel_3;
	// UniRx.IObserver`1<System.String> UniRx.ObservableWWW/<FetchText>c__Iterator1::observer
	RuntimeObject* ___observer_4;
	// System.Object UniRx.ObservableWWW/<FetchText>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean UniRx.ObservableWWW/<FetchText>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 UniRx.ObservableWWW/<FetchText>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_www_0() { return static_cast<int32_t>(offsetof(U3CFetchTextU3Ec__Iterator1_t3169890160, ___www_0)); }
	inline WWW_t3688466362 * get_www_0() const { return ___www_0; }
	inline WWW_t3688466362 ** get_address_of_www_0() { return &___www_0; }
	inline void set_www_0(WWW_t3688466362 * value)
	{
		___www_0 = value;
		Il2CppCodeGenWriteBarrier((&___www_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CFetchTextU3Ec__Iterator1_t3169890160, ___U24locvar0_1)); }
	inline WWW_t3688466362 * get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline WWW_t3688466362 ** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(WWW_t3688466362 * value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_reportProgress_2() { return static_cast<int32_t>(offsetof(U3CFetchTextU3Ec__Iterator1_t3169890160, ___reportProgress_2)); }
	inline RuntimeObject* get_reportProgress_2() const { return ___reportProgress_2; }
	inline RuntimeObject** get_address_of_reportProgress_2() { return &___reportProgress_2; }
	inline void set_reportProgress_2(RuntimeObject* value)
	{
		___reportProgress_2 = value;
		Il2CppCodeGenWriteBarrier((&___reportProgress_2), value);
	}

	inline static int32_t get_offset_of_cancel_3() { return static_cast<int32_t>(offsetof(U3CFetchTextU3Ec__Iterator1_t3169890160, ___cancel_3)); }
	inline CancellationToken_t1265546479  get_cancel_3() const { return ___cancel_3; }
	inline CancellationToken_t1265546479 * get_address_of_cancel_3() { return &___cancel_3; }
	inline void set_cancel_3(CancellationToken_t1265546479  value)
	{
		___cancel_3 = value;
	}

	inline static int32_t get_offset_of_observer_4() { return static_cast<int32_t>(offsetof(U3CFetchTextU3Ec__Iterator1_t3169890160, ___observer_4)); }
	inline RuntimeObject* get_observer_4() const { return ___observer_4; }
	inline RuntimeObject** get_address_of_observer_4() { return &___observer_4; }
	inline void set_observer_4(RuntimeObject* value)
	{
		___observer_4 = value;
		Il2CppCodeGenWriteBarrier((&___observer_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFetchTextU3Ec__Iterator1_t3169890160, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFetchTextU3Ec__Iterator1_t3169890160, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFetchTextU3Ec__Iterator1_t3169890160, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHTEXTU3EC__ITERATOR1_T3169890160_H
#ifndef U3CFETCHU3EC__ITERATOR0_T2218155811_H
#define U3CFETCHU3EC__ITERATOR0_T2218155811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<Fetch>c__Iterator0
struct  U3CFetchU3Ec__Iterator0_t2218155811  : public RuntimeObject
{
public:
	// UnityEngine.WWW UniRx.ObservableWWW/<Fetch>c__Iterator0::www
	WWW_t3688466362 * ___www_0;
	// UnityEngine.WWW UniRx.ObservableWWW/<Fetch>c__Iterator0::$locvar0
	WWW_t3688466362 * ___U24locvar0_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<Fetch>c__Iterator0::reportProgress
	RuntimeObject* ___reportProgress_2;
	// UniRx.CancellationToken UniRx.ObservableWWW/<Fetch>c__Iterator0::cancel
	CancellationToken_t1265546479  ___cancel_3;
	// UniRx.IObserver`1<UnityEngine.WWW> UniRx.ObservableWWW/<Fetch>c__Iterator0::observer
	RuntimeObject* ___observer_4;
	// System.Object UniRx.ObservableWWW/<Fetch>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean UniRx.ObservableWWW/<Fetch>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 UniRx.ObservableWWW/<Fetch>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_www_0() { return static_cast<int32_t>(offsetof(U3CFetchU3Ec__Iterator0_t2218155811, ___www_0)); }
	inline WWW_t3688466362 * get_www_0() const { return ___www_0; }
	inline WWW_t3688466362 ** get_address_of_www_0() { return &___www_0; }
	inline void set_www_0(WWW_t3688466362 * value)
	{
		___www_0 = value;
		Il2CppCodeGenWriteBarrier((&___www_0), value);
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CFetchU3Ec__Iterator0_t2218155811, ___U24locvar0_1)); }
	inline WWW_t3688466362 * get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline WWW_t3688466362 ** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(WWW_t3688466362 * value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_1), value);
	}

	inline static int32_t get_offset_of_reportProgress_2() { return static_cast<int32_t>(offsetof(U3CFetchU3Ec__Iterator0_t2218155811, ___reportProgress_2)); }
	inline RuntimeObject* get_reportProgress_2() const { return ___reportProgress_2; }
	inline RuntimeObject** get_address_of_reportProgress_2() { return &___reportProgress_2; }
	inline void set_reportProgress_2(RuntimeObject* value)
	{
		___reportProgress_2 = value;
		Il2CppCodeGenWriteBarrier((&___reportProgress_2), value);
	}

	inline static int32_t get_offset_of_cancel_3() { return static_cast<int32_t>(offsetof(U3CFetchU3Ec__Iterator0_t2218155811, ___cancel_3)); }
	inline CancellationToken_t1265546479  get_cancel_3() const { return ___cancel_3; }
	inline CancellationToken_t1265546479 * get_address_of_cancel_3() { return &___cancel_3; }
	inline void set_cancel_3(CancellationToken_t1265546479  value)
	{
		___cancel_3 = value;
	}

	inline static int32_t get_offset_of_observer_4() { return static_cast<int32_t>(offsetof(U3CFetchU3Ec__Iterator0_t2218155811, ___observer_4)); }
	inline RuntimeObject* get_observer_4() const { return ___observer_4; }
	inline RuntimeObject** get_address_of_observer_4() { return &___observer_4; }
	inline void set_observer_4(RuntimeObject* value)
	{
		___observer_4 = value;
		Il2CppCodeGenWriteBarrier((&___observer_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFetchU3Ec__Iterator0_t2218155811, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFetchU3Ec__Iterator0_t2218155811, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFetchU3Ec__Iterator0_t2218155811, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFETCHU3EC__ITERATOR0_T2218155811_H
#ifndef REACTIVEPROPERTY_1_T1536653225_H
#define REACTIVEPROPERTY_1_T1536653225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ReactiveProperty`1<UnityEngine.Quaternion>
struct  ReactiveProperty_1_t1536653225  : public RuntimeObject
{
public:
	// System.Boolean UniRx.ReactiveProperty`1::canPublishValueOnSubscribe
	bool ___canPublishValueOnSubscribe_1;
	// System.Boolean UniRx.ReactiveProperty`1::isDisposed
	bool ___isDisposed_2;
	// T UniRx.ReactiveProperty`1::value
	Quaternion_t2301928331  ___value_3;
	// UniRx.Subject`1<T> UniRx.ReactiveProperty`1::publisher
	Subject_1_t2390584718 * ___publisher_4;
	// System.IDisposable UniRx.ReactiveProperty`1::sourceConnection
	RuntimeObject* ___sourceConnection_5;
	// System.Exception UniRx.ReactiveProperty`1::lastException
	Exception_t * ___lastException_6;

public:
	inline static int32_t get_offset_of_canPublishValueOnSubscribe_1() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1536653225, ___canPublishValueOnSubscribe_1)); }
	inline bool get_canPublishValueOnSubscribe_1() const { return ___canPublishValueOnSubscribe_1; }
	inline bool* get_address_of_canPublishValueOnSubscribe_1() { return &___canPublishValueOnSubscribe_1; }
	inline void set_canPublishValueOnSubscribe_1(bool value)
	{
		___canPublishValueOnSubscribe_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1536653225, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}

	inline static int32_t get_offset_of_value_3() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1536653225, ___value_3)); }
	inline Quaternion_t2301928331  get_value_3() const { return ___value_3; }
	inline Quaternion_t2301928331 * get_address_of_value_3() { return &___value_3; }
	inline void set_value_3(Quaternion_t2301928331  value)
	{
		___value_3 = value;
	}

	inline static int32_t get_offset_of_publisher_4() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1536653225, ___publisher_4)); }
	inline Subject_1_t2390584718 * get_publisher_4() const { return ___publisher_4; }
	inline Subject_1_t2390584718 ** get_address_of_publisher_4() { return &___publisher_4; }
	inline void set_publisher_4(Subject_1_t2390584718 * value)
	{
		___publisher_4 = value;
		Il2CppCodeGenWriteBarrier((&___publisher_4), value);
	}

	inline static int32_t get_offset_of_sourceConnection_5() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1536653225, ___sourceConnection_5)); }
	inline RuntimeObject* get_sourceConnection_5() const { return ___sourceConnection_5; }
	inline RuntimeObject** get_address_of_sourceConnection_5() { return &___sourceConnection_5; }
	inline void set_sourceConnection_5(RuntimeObject* value)
	{
		___sourceConnection_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceConnection_5), value);
	}

	inline static int32_t get_offset_of_lastException_6() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1536653225, ___lastException_6)); }
	inline Exception_t * get_lastException_6() const { return ___lastException_6; }
	inline Exception_t ** get_address_of_lastException_6() { return &___lastException_6; }
	inline void set_lastException_6(Exception_t * value)
	{
		___lastException_6 = value;
		Il2CppCodeGenWriteBarrier((&___lastException_6), value);
	}
};

struct ReactiveProperty_1_t1536653225_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<T> UniRx.ReactiveProperty`1::defaultEqualityComparer
	RuntimeObject* ___defaultEqualityComparer_0;

public:
	inline static int32_t get_offset_of_defaultEqualityComparer_0() { return static_cast<int32_t>(offsetof(ReactiveProperty_1_t1536653225_StaticFields, ___defaultEqualityComparer_0)); }
	inline RuntimeObject* get_defaultEqualityComparer_0() const { return ___defaultEqualityComparer_0; }
	inline RuntimeObject** get_address_of_defaultEqualityComparer_0() { return &___defaultEqualityComparer_0; }
	inline void set_defaultEqualityComparer_0(RuntimeObject* value)
	{
		___defaultEqualityComparer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultEqualityComparer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REACTIVEPROPERTY_1_T1536653225_H
#ifndef U3CLOADFROMCACHEORDOWNLOADU3EC__ANONSTOREY15_T1810168043_H
#define U3CLOADFROMCACHEORDOWNLOADU3EC__ANONSTOREY15_T1810168043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey15
struct  U3CLoadFromCacheOrDownloadU3Ec__AnonStorey15_t1810168043  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey15::url
	String_t* ___url_0;
	// UnityEngine.Hash128 UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey15::hash128
	Hash128_t2357739769  ___hash128_1;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey15::progress
	RuntimeObject* ___progress_2;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey15_t1810168043, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_hash128_1() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey15_t1810168043, ___hash128_1)); }
	inline Hash128_t2357739769  get_hash128_1() const { return ___hash128_1; }
	inline Hash128_t2357739769 * get_address_of_hash128_1() { return &___hash128_1; }
	inline void set_hash128_1(Hash128_t2357739769  value)
	{
		___hash128_1 = value;
	}

	inline static int32_t get_offset_of_progress_2() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey15_t1810168043, ___progress_2)); }
	inline RuntimeObject* get_progress_2() const { return ___progress_2; }
	inline RuntimeObject** get_address_of_progress_2() { return &___progress_2; }
	inline void set_progress_2(RuntimeObject* value)
	{
		___progress_2 = value;
		Il2CppCodeGenWriteBarrier((&___progress_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADFROMCACHEORDOWNLOADU3EC__ANONSTOREY15_T1810168043_H
#ifndef U3CLOADFROMCACHEORDOWNLOADU3EC__ANONSTOREY16_T4148820203_H
#define U3CLOADFROMCACHEORDOWNLOADU3EC__ANONSTOREY16_T4148820203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey16
struct  U3CLoadFromCacheOrDownloadU3Ec__AnonStorey16_t4148820203  : public RuntimeObject
{
public:
	// System.String UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey16::url
	String_t* ___url_0;
	// UnityEngine.Hash128 UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey16::hash128
	Hash128_t2357739769  ___hash128_1;
	// System.UInt32 UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey16::crc
	uint32_t ___crc_2;
	// UniRx.IProgress`1<System.Single> UniRx.ObservableWWW/<LoadFromCacheOrDownload>c__AnonStorey16::progress
	RuntimeObject* ___progress_3;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey16_t4148820203, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_hash128_1() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey16_t4148820203, ___hash128_1)); }
	inline Hash128_t2357739769  get_hash128_1() const { return ___hash128_1; }
	inline Hash128_t2357739769 * get_address_of_hash128_1() { return &___hash128_1; }
	inline void set_hash128_1(Hash128_t2357739769  value)
	{
		___hash128_1 = value;
	}

	inline static int32_t get_offset_of_crc_2() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey16_t4148820203, ___crc_2)); }
	inline uint32_t get_crc_2() const { return ___crc_2; }
	inline uint32_t* get_address_of_crc_2() { return &___crc_2; }
	inline void set_crc_2(uint32_t value)
	{
		___crc_2 = value;
	}

	inline static int32_t get_offset_of_progress_3() { return static_cast<int32_t>(offsetof(U3CLoadFromCacheOrDownloadU3Ec__AnonStorey16_t4148820203, ___progress_3)); }
	inline RuntimeObject* get_progress_3() const { return ___progress_3; }
	inline RuntimeObject** get_address_of_progress_3() { return &___progress_3; }
	inline void set_progress_3(RuntimeObject* value)
	{
		___progress_3 = value;
		Il2CppCodeGenWriteBarrier((&___progress_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADFROMCACHEORDOWNLOADU3EC__ANONSTOREY16_T4148820203_H
#ifndef BATCHFRAMEOBSERVABLE_T2539759103_H
#define BATCHFRAMEOBSERVABLE_T2539759103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.Operators.BatchFrameObservable
struct  BatchFrameObservable_t2539759103  : public OperatorObservableBase_1_t354719556
{
public:
	// UniRx.IObservable`1<UniRx.Unit> UniRx.Operators.BatchFrameObservable::source
	RuntimeObject* ___source_1;
	// System.Int32 UniRx.Operators.BatchFrameObservable::frameCount
	int32_t ___frameCount_2;
	// UniRx.FrameCountType UniRx.Operators.BatchFrameObservable::frameCountType
	int32_t ___frameCountType_3;

public:
	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(BatchFrameObservable_t2539759103, ___source_1)); }
	inline RuntimeObject* get_source_1() const { return ___source_1; }
	inline RuntimeObject** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(RuntimeObject* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_frameCount_2() { return static_cast<int32_t>(offsetof(BatchFrameObservable_t2539759103, ___frameCount_2)); }
	inline int32_t get_frameCount_2() const { return ___frameCount_2; }
	inline int32_t* get_address_of_frameCount_2() { return &___frameCount_2; }
	inline void set_frameCount_2(int32_t value)
	{
		___frameCount_2 = value;
	}

	inline static int32_t get_offset_of_frameCountType_3() { return static_cast<int32_t>(offsetof(BatchFrameObservable_t2539759103, ___frameCountType_3)); }
	inline int32_t get_frameCountType_3() const { return ___frameCountType_3; }
	inline int32_t* get_address_of_frameCountType_3() { return &___frameCountType_3; }
	inline void set_frameCountType_3(int32_t value)
	{
		___frameCountType_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BATCHFRAMEOBSERVABLE_T2539759103_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef WWWERROREXCEPTION_T1771212499_H
#define WWWERROREXCEPTION_T1771212499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.WWWErrorException
struct  WWWErrorException_t1771212499  : public Exception_t
{
public:
	// System.String UniRx.WWWErrorException::<RawErrorMessage>k__BackingField
	String_t* ___U3CRawErrorMessageU3Ek__BackingField_11;
	// System.Boolean UniRx.WWWErrorException::<HasResponse>k__BackingField
	bool ___U3CHasResponseU3Ek__BackingField_12;
	// System.String UniRx.WWWErrorException::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_13;
	// System.Net.HttpStatusCode UniRx.WWWErrorException::<StatusCode>k__BackingField
	int32_t ___U3CStatusCodeU3Ek__BackingField_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> UniRx.WWWErrorException::<ResponseHeaders>k__BackingField
	Dictionary_2_t1632706988 * ___U3CResponseHeadersU3Ek__BackingField_15;
	// UnityEngine.WWW UniRx.WWWErrorException::<WWW>k__BackingField
	WWW_t3688466362 * ___U3CWWWU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CRawErrorMessageU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(WWWErrorException_t1771212499, ___U3CRawErrorMessageU3Ek__BackingField_11)); }
	inline String_t* get_U3CRawErrorMessageU3Ek__BackingField_11() const { return ___U3CRawErrorMessageU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CRawErrorMessageU3Ek__BackingField_11() { return &___U3CRawErrorMessageU3Ek__BackingField_11; }
	inline void set_U3CRawErrorMessageU3Ek__BackingField_11(String_t* value)
	{
		___U3CRawErrorMessageU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawErrorMessageU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CHasResponseU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(WWWErrorException_t1771212499, ___U3CHasResponseU3Ek__BackingField_12)); }
	inline bool get_U3CHasResponseU3Ek__BackingField_12() const { return ___U3CHasResponseU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CHasResponseU3Ek__BackingField_12() { return &___U3CHasResponseU3Ek__BackingField_12; }
	inline void set_U3CHasResponseU3Ek__BackingField_12(bool value)
	{
		___U3CHasResponseU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(WWWErrorException_t1771212499, ___U3CTextU3Ek__BackingField_13)); }
	inline String_t* get_U3CTextU3Ek__BackingField_13() const { return ___U3CTextU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_13() { return &___U3CTextU3Ek__BackingField_13; }
	inline void set_U3CTextU3Ek__BackingField_13(String_t* value)
	{
		___U3CTextU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CStatusCodeU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(WWWErrorException_t1771212499, ___U3CStatusCodeU3Ek__BackingField_14)); }
	inline int32_t get_U3CStatusCodeU3Ek__BackingField_14() const { return ___U3CStatusCodeU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CStatusCodeU3Ek__BackingField_14() { return &___U3CStatusCodeU3Ek__BackingField_14; }
	inline void set_U3CStatusCodeU3Ek__BackingField_14(int32_t value)
	{
		___U3CStatusCodeU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CResponseHeadersU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(WWWErrorException_t1771212499, ___U3CResponseHeadersU3Ek__BackingField_15)); }
	inline Dictionary_2_t1632706988 * get_U3CResponseHeadersU3Ek__BackingField_15() const { return ___U3CResponseHeadersU3Ek__BackingField_15; }
	inline Dictionary_2_t1632706988 ** get_address_of_U3CResponseHeadersU3Ek__BackingField_15() { return &___U3CResponseHeadersU3Ek__BackingField_15; }
	inline void set_U3CResponseHeadersU3Ek__BackingField_15(Dictionary_2_t1632706988 * value)
	{
		___U3CResponseHeadersU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseHeadersU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CWWWU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(WWWErrorException_t1771212499, ___U3CWWWU3Ek__BackingField_16)); }
	inline WWW_t3688466362 * get_U3CWWWU3Ek__BackingField_16() const { return ___U3CWWWU3Ek__BackingField_16; }
	inline WWW_t3688466362 ** get_address_of_U3CWWWU3Ek__BackingField_16() { return &___U3CWWWU3Ek__BackingField_16; }
	inline void set_U3CWWWU3Ek__BackingField_16(WWW_t3688466362 * value)
	{
		___U3CWWWU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CWWWU3Ek__BackingField_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWERROREXCEPTION_T1771212499_H
#ifndef LAZYTASK_T1721948414_H
#define LAZYTASK_T1721948414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.LazyTask
struct  LazyTask_t1721948414  : public RuntimeObject
{
public:
	// UniRx.LazyTask/TaskStatus UniRx.LazyTask::<Status>k__BackingField
	int32_t ___U3CStatusU3Ek__BackingField_0;
	// UniRx.BooleanDisposable UniRx.LazyTask::cancellation
	BooleanDisposable_t84760918 * ___cancellation_1;

public:
	inline static int32_t get_offset_of_U3CStatusU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(LazyTask_t1721948414, ___U3CStatusU3Ek__BackingField_0)); }
	inline int32_t get_U3CStatusU3Ek__BackingField_0() const { return ___U3CStatusU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CStatusU3Ek__BackingField_0() { return &___U3CStatusU3Ek__BackingField_0; }
	inline void set_U3CStatusU3Ek__BackingField_0(int32_t value)
	{
		___U3CStatusU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_cancellation_1() { return static_cast<int32_t>(offsetof(LazyTask_t1721948414, ___cancellation_1)); }
	inline BooleanDisposable_t84760918 * get_cancellation_1() const { return ___cancellation_1; }
	inline BooleanDisposable_t84760918 ** get_address_of_cancellation_1() { return &___cancellation_1; }
	inline void set_cancellation_1(BooleanDisposable_t84760918 * value)
	{
		___cancellation_1 = value;
		Il2CppCodeGenWriteBarrier((&___cancellation_1), value);
	}
};

struct LazyTask_t1721948414_StaticFields
{
public:
	// System.Func`2<UniRx.LazyTask,UnityEngine.Coroutine> UniRx.LazyTask::<>f__am$cache0
	Func_2_t2678796183 * ___U3CU3Ef__amU24cache0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_2() { return static_cast<int32_t>(offsetof(LazyTask_t1721948414_StaticFields, ___U3CU3Ef__amU24cache0_2)); }
	inline Func_2_t2678796183 * get_U3CU3Ef__amU24cache0_2() const { return ___U3CU3Ef__amU24cache0_2; }
	inline Func_2_t2678796183 ** get_address_of_U3CU3Ef__amU24cache0_2() { return &___U3CU3Ef__amU24cache0_2; }
	inline void set_U3CU3Ef__amU24cache0_2(Func_2_t2678796183 * value)
	{
		___U3CU3Ef__amU24cache0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYTASK_T1721948414_H
#ifndef QUATERNIONREACTIVEPROPERTY_T2539028965_H
#define QUATERNIONREACTIVEPROPERTY_T2539028965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.QuaternionReactiveProperty
struct  QuaternionReactiveProperty_t2539028965  : public ReactiveProperty_1_t1536653225
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONREACTIVEPROPERTY_T2539028965_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef PRESENTERBASE_1_T4151662397_H
#define PRESENTERBASE_1_T4151662397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.PresenterBase`1<UniRx.Unit>
struct  PresenterBase_1_t4151662397  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 UniRx.PresenterBase`1::childrenCount
	int32_t ___childrenCount_3;
	// System.Int32 UniRx.PresenterBase`1::currentCalledCount
	int32_t ___currentCalledCount_4;
	// System.Boolean UniRx.PresenterBase`1::isAwaken
	bool ___isAwaken_5;
	// System.Boolean UniRx.PresenterBase`1::isInitialized
	bool ___isInitialized_6;
	// System.Boolean UniRx.PresenterBase`1::isStartedCapturePhase
	bool ___isStartedCapturePhase_7;
	// UniRx.Subject`1<UniRx.Unit> UniRx.PresenterBase`1::initializeSubject
	Subject_1_t3450905854 * ___initializeSubject_8;
	// UniRx.IPresenter[] UniRx.PresenterBase`1::children
	IPresenterU5BU5D_t1029123997* ___children_9;
	// UniRx.IPresenter UniRx.PresenterBase`1::parent
	RuntimeObject* ___parent_10;
	// T UniRx.PresenterBase`1::argument
	Unit_t3362249467  ___argument_11;

public:
	inline static int32_t get_offset_of_childrenCount_3() { return static_cast<int32_t>(offsetof(PresenterBase_1_t4151662397, ___childrenCount_3)); }
	inline int32_t get_childrenCount_3() const { return ___childrenCount_3; }
	inline int32_t* get_address_of_childrenCount_3() { return &___childrenCount_3; }
	inline void set_childrenCount_3(int32_t value)
	{
		___childrenCount_3 = value;
	}

	inline static int32_t get_offset_of_currentCalledCount_4() { return static_cast<int32_t>(offsetof(PresenterBase_1_t4151662397, ___currentCalledCount_4)); }
	inline int32_t get_currentCalledCount_4() const { return ___currentCalledCount_4; }
	inline int32_t* get_address_of_currentCalledCount_4() { return &___currentCalledCount_4; }
	inline void set_currentCalledCount_4(int32_t value)
	{
		___currentCalledCount_4 = value;
	}

	inline static int32_t get_offset_of_isAwaken_5() { return static_cast<int32_t>(offsetof(PresenterBase_1_t4151662397, ___isAwaken_5)); }
	inline bool get_isAwaken_5() const { return ___isAwaken_5; }
	inline bool* get_address_of_isAwaken_5() { return &___isAwaken_5; }
	inline void set_isAwaken_5(bool value)
	{
		___isAwaken_5 = value;
	}

	inline static int32_t get_offset_of_isInitialized_6() { return static_cast<int32_t>(offsetof(PresenterBase_1_t4151662397, ___isInitialized_6)); }
	inline bool get_isInitialized_6() const { return ___isInitialized_6; }
	inline bool* get_address_of_isInitialized_6() { return &___isInitialized_6; }
	inline void set_isInitialized_6(bool value)
	{
		___isInitialized_6 = value;
	}

	inline static int32_t get_offset_of_isStartedCapturePhase_7() { return static_cast<int32_t>(offsetof(PresenterBase_1_t4151662397, ___isStartedCapturePhase_7)); }
	inline bool get_isStartedCapturePhase_7() const { return ___isStartedCapturePhase_7; }
	inline bool* get_address_of_isStartedCapturePhase_7() { return &___isStartedCapturePhase_7; }
	inline void set_isStartedCapturePhase_7(bool value)
	{
		___isStartedCapturePhase_7 = value;
	}

	inline static int32_t get_offset_of_initializeSubject_8() { return static_cast<int32_t>(offsetof(PresenterBase_1_t4151662397, ___initializeSubject_8)); }
	inline Subject_1_t3450905854 * get_initializeSubject_8() const { return ___initializeSubject_8; }
	inline Subject_1_t3450905854 ** get_address_of_initializeSubject_8() { return &___initializeSubject_8; }
	inline void set_initializeSubject_8(Subject_1_t3450905854 * value)
	{
		___initializeSubject_8 = value;
		Il2CppCodeGenWriteBarrier((&___initializeSubject_8), value);
	}

	inline static int32_t get_offset_of_children_9() { return static_cast<int32_t>(offsetof(PresenterBase_1_t4151662397, ___children_9)); }
	inline IPresenterU5BU5D_t1029123997* get_children_9() const { return ___children_9; }
	inline IPresenterU5BU5D_t1029123997** get_address_of_children_9() { return &___children_9; }
	inline void set_children_9(IPresenterU5BU5D_t1029123997* value)
	{
		___children_9 = value;
		Il2CppCodeGenWriteBarrier((&___children_9), value);
	}

	inline static int32_t get_offset_of_parent_10() { return static_cast<int32_t>(offsetof(PresenterBase_1_t4151662397, ___parent_10)); }
	inline RuntimeObject* get_parent_10() const { return ___parent_10; }
	inline RuntimeObject** get_address_of_parent_10() { return &___parent_10; }
	inline void set_parent_10(RuntimeObject* value)
	{
		___parent_10 = value;
		Il2CppCodeGenWriteBarrier((&___parent_10), value);
	}

	inline static int32_t get_offset_of_argument_11() { return static_cast<int32_t>(offsetof(PresenterBase_1_t4151662397, ___argument_11)); }
	inline Unit_t3362249467  get_argument_11() const { return ___argument_11; }
	inline Unit_t3362249467 * get_address_of_argument_11() { return &___argument_11; }
	inline void set_argument_11(Unit_t3362249467  value)
	{
		___argument_11 = value;
	}
};

struct PresenterBase_1_t4151662397_StaticFields
{
public:
	// UniRx.IPresenter[] UniRx.PresenterBase`1::EmptyChildren
	IPresenterU5BU5D_t1029123997* ___EmptyChildren_2;

public:
	inline static int32_t get_offset_of_EmptyChildren_2() { return static_cast<int32_t>(offsetof(PresenterBase_1_t4151662397_StaticFields, ___EmptyChildren_2)); }
	inline IPresenterU5BU5D_t1029123997* get_EmptyChildren_2() const { return ___EmptyChildren_2; }
	inline IPresenterU5BU5D_t1029123997** get_address_of_EmptyChildren_2() { return &___EmptyChildren_2; }
	inline void set_EmptyChildren_2(IPresenterU5BU5D_t1029123997* value)
	{
		___EmptyChildren_2 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyChildren_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESENTERBASE_1_T4151662397_H
#ifndef MAINTHREADDISPATCHER_T3684499304_H
#define MAINTHREADDISPATCHER_T3684499304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.MainThreadDispatcher
struct  MainThreadDispatcher_t3684499304  : public MonoBehaviour_t3962482529
{
public:
	// UniRx.InternalUtil.ThreadSafeQueueWorker UniRx.MainThreadDispatcher::queueWorker
	ThreadSafeQueueWorker_t2171548691 * ___queueWorker_3;
	// System.Action`1<System.Exception> UniRx.MainThreadDispatcher::unhandledExceptionCallback
	Action_1_t1609204844 * ___unhandledExceptionCallback_4;
	// UniRx.InternalUtil.MicroCoroutine UniRx.MainThreadDispatcher::updateMicroCoroutine
	MicroCoroutine_t3506682403 * ___updateMicroCoroutine_5;
	// UniRx.InternalUtil.MicroCoroutine UniRx.MainThreadDispatcher::fixedUpdateMicroCoroutine
	MicroCoroutine_t3506682403 * ___fixedUpdateMicroCoroutine_6;
	// UniRx.InternalUtil.MicroCoroutine UniRx.MainThreadDispatcher::endOfFrameMicroCoroutine
	MicroCoroutine_t3506682403 * ___endOfFrameMicroCoroutine_7;
	// UniRx.Subject`1<UniRx.Unit> UniRx.MainThreadDispatcher::update
	Subject_1_t3450905854 * ___update_12;
	// UniRx.Subject`1<UniRx.Unit> UniRx.MainThreadDispatcher::lateUpdate
	Subject_1_t3450905854 * ___lateUpdate_13;
	// UniRx.Subject`1<System.Boolean> UniRx.MainThreadDispatcher::onApplicationFocus
	Subject_1_t185944352 * ___onApplicationFocus_14;
	// UniRx.Subject`1<System.Boolean> UniRx.MainThreadDispatcher::onApplicationPause
	Subject_1_t185944352 * ___onApplicationPause_15;
	// UniRx.Subject`1<UniRx.Unit> UniRx.MainThreadDispatcher::onApplicationQuit
	Subject_1_t3450905854 * ___onApplicationQuit_16;

public:
	inline static int32_t get_offset_of_queueWorker_3() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304, ___queueWorker_3)); }
	inline ThreadSafeQueueWorker_t2171548691 * get_queueWorker_3() const { return ___queueWorker_3; }
	inline ThreadSafeQueueWorker_t2171548691 ** get_address_of_queueWorker_3() { return &___queueWorker_3; }
	inline void set_queueWorker_3(ThreadSafeQueueWorker_t2171548691 * value)
	{
		___queueWorker_3 = value;
		Il2CppCodeGenWriteBarrier((&___queueWorker_3), value);
	}

	inline static int32_t get_offset_of_unhandledExceptionCallback_4() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304, ___unhandledExceptionCallback_4)); }
	inline Action_1_t1609204844 * get_unhandledExceptionCallback_4() const { return ___unhandledExceptionCallback_4; }
	inline Action_1_t1609204844 ** get_address_of_unhandledExceptionCallback_4() { return &___unhandledExceptionCallback_4; }
	inline void set_unhandledExceptionCallback_4(Action_1_t1609204844 * value)
	{
		___unhandledExceptionCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___unhandledExceptionCallback_4), value);
	}

	inline static int32_t get_offset_of_updateMicroCoroutine_5() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304, ___updateMicroCoroutine_5)); }
	inline MicroCoroutine_t3506682403 * get_updateMicroCoroutine_5() const { return ___updateMicroCoroutine_5; }
	inline MicroCoroutine_t3506682403 ** get_address_of_updateMicroCoroutine_5() { return &___updateMicroCoroutine_5; }
	inline void set_updateMicroCoroutine_5(MicroCoroutine_t3506682403 * value)
	{
		___updateMicroCoroutine_5 = value;
		Il2CppCodeGenWriteBarrier((&___updateMicroCoroutine_5), value);
	}

	inline static int32_t get_offset_of_fixedUpdateMicroCoroutine_6() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304, ___fixedUpdateMicroCoroutine_6)); }
	inline MicroCoroutine_t3506682403 * get_fixedUpdateMicroCoroutine_6() const { return ___fixedUpdateMicroCoroutine_6; }
	inline MicroCoroutine_t3506682403 ** get_address_of_fixedUpdateMicroCoroutine_6() { return &___fixedUpdateMicroCoroutine_6; }
	inline void set_fixedUpdateMicroCoroutine_6(MicroCoroutine_t3506682403 * value)
	{
		___fixedUpdateMicroCoroutine_6 = value;
		Il2CppCodeGenWriteBarrier((&___fixedUpdateMicroCoroutine_6), value);
	}

	inline static int32_t get_offset_of_endOfFrameMicroCoroutine_7() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304, ___endOfFrameMicroCoroutine_7)); }
	inline MicroCoroutine_t3506682403 * get_endOfFrameMicroCoroutine_7() const { return ___endOfFrameMicroCoroutine_7; }
	inline MicroCoroutine_t3506682403 ** get_address_of_endOfFrameMicroCoroutine_7() { return &___endOfFrameMicroCoroutine_7; }
	inline void set_endOfFrameMicroCoroutine_7(MicroCoroutine_t3506682403 * value)
	{
		___endOfFrameMicroCoroutine_7 = value;
		Il2CppCodeGenWriteBarrier((&___endOfFrameMicroCoroutine_7), value);
	}

	inline static int32_t get_offset_of_update_12() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304, ___update_12)); }
	inline Subject_1_t3450905854 * get_update_12() const { return ___update_12; }
	inline Subject_1_t3450905854 ** get_address_of_update_12() { return &___update_12; }
	inline void set_update_12(Subject_1_t3450905854 * value)
	{
		___update_12 = value;
		Il2CppCodeGenWriteBarrier((&___update_12), value);
	}

	inline static int32_t get_offset_of_lateUpdate_13() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304, ___lateUpdate_13)); }
	inline Subject_1_t3450905854 * get_lateUpdate_13() const { return ___lateUpdate_13; }
	inline Subject_1_t3450905854 ** get_address_of_lateUpdate_13() { return &___lateUpdate_13; }
	inline void set_lateUpdate_13(Subject_1_t3450905854 * value)
	{
		___lateUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___lateUpdate_13), value);
	}

	inline static int32_t get_offset_of_onApplicationFocus_14() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304, ___onApplicationFocus_14)); }
	inline Subject_1_t185944352 * get_onApplicationFocus_14() const { return ___onApplicationFocus_14; }
	inline Subject_1_t185944352 ** get_address_of_onApplicationFocus_14() { return &___onApplicationFocus_14; }
	inline void set_onApplicationFocus_14(Subject_1_t185944352 * value)
	{
		___onApplicationFocus_14 = value;
		Il2CppCodeGenWriteBarrier((&___onApplicationFocus_14), value);
	}

	inline static int32_t get_offset_of_onApplicationPause_15() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304, ___onApplicationPause_15)); }
	inline Subject_1_t185944352 * get_onApplicationPause_15() const { return ___onApplicationPause_15; }
	inline Subject_1_t185944352 ** get_address_of_onApplicationPause_15() { return &___onApplicationPause_15; }
	inline void set_onApplicationPause_15(Subject_1_t185944352 * value)
	{
		___onApplicationPause_15 = value;
		Il2CppCodeGenWriteBarrier((&___onApplicationPause_15), value);
	}

	inline static int32_t get_offset_of_onApplicationQuit_16() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304, ___onApplicationQuit_16)); }
	inline Subject_1_t3450905854 * get_onApplicationQuit_16() const { return ___onApplicationQuit_16; }
	inline Subject_1_t3450905854 ** get_address_of_onApplicationQuit_16() { return &___onApplicationQuit_16; }
	inline void set_onApplicationQuit_16(Subject_1_t3450905854 * value)
	{
		___onApplicationQuit_16 = value;
		Il2CppCodeGenWriteBarrier((&___onApplicationQuit_16), value);
	}
};

struct MainThreadDispatcher_t3684499304_StaticFields
{
public:
	// UniRx.MainThreadDispatcher/CullingMode UniRx.MainThreadDispatcher::cullingMode
	int32_t ___cullingMode_2;
	// UniRx.MainThreadDispatcher UniRx.MainThreadDispatcher::instance
	MainThreadDispatcher_t3684499304 * ___instance_8;
	// System.Boolean UniRx.MainThreadDispatcher::initialized
	bool ___initialized_9;
	// System.Boolean UniRx.MainThreadDispatcher::isQuitting
	bool ___isQuitting_10;
	// System.Action`1<System.Exception> UniRx.MainThreadDispatcher::<>f__am$cache0
	Action_1_t1609204844 * ___U3CU3Ef__amU24cache0_17;

public:
	inline static int32_t get_offset_of_cullingMode_2() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304_StaticFields, ___cullingMode_2)); }
	inline int32_t get_cullingMode_2() const { return ___cullingMode_2; }
	inline int32_t* get_address_of_cullingMode_2() { return &___cullingMode_2; }
	inline void set_cullingMode_2(int32_t value)
	{
		___cullingMode_2 = value;
	}

	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304_StaticFields, ___instance_8)); }
	inline MainThreadDispatcher_t3684499304 * get_instance_8() const { return ___instance_8; }
	inline MainThreadDispatcher_t3684499304 ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(MainThreadDispatcher_t3684499304 * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___instance_8), value);
	}

	inline static int32_t get_offset_of_initialized_9() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304_StaticFields, ___initialized_9)); }
	inline bool get_initialized_9() const { return ___initialized_9; }
	inline bool* get_address_of_initialized_9() { return &___initialized_9; }
	inline void set_initialized_9(bool value)
	{
		___initialized_9 = value;
	}

	inline static int32_t get_offset_of_isQuitting_10() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304_StaticFields, ___isQuitting_10)); }
	inline bool get_isQuitting_10() const { return ___isQuitting_10; }
	inline bool* get_address_of_isQuitting_10() { return &___isQuitting_10; }
	inline void set_isQuitting_10(bool value)
	{
		___isQuitting_10 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_17() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304_StaticFields, ___U3CU3Ef__amU24cache0_17)); }
	inline Action_1_t1609204844 * get_U3CU3Ef__amU24cache0_17() const { return ___U3CU3Ef__amU24cache0_17; }
	inline Action_1_t1609204844 ** get_address_of_U3CU3Ef__amU24cache0_17() { return &___U3CU3Ef__amU24cache0_17; }
	inline void set_U3CU3Ef__amU24cache0_17(Action_1_t1609204844 * value)
	{
		___U3CU3Ef__amU24cache0_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_17), value);
	}
};

struct MainThreadDispatcher_t3684499304_ThreadStaticFields
{
public:
	// System.Object UniRx.MainThreadDispatcher::mainThreadToken
	RuntimeObject * ___mainThreadToken_11;

public:
	inline static int32_t get_offset_of_mainThreadToken_11() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t3684499304_ThreadStaticFields, ___mainThreadToken_11)); }
	inline RuntimeObject * get_mainThreadToken_11() const { return ___mainThreadToken_11; }
	inline RuntimeObject ** get_address_of_mainThreadToken_11() { return &___mainThreadToken_11; }
	inline void set_mainThreadToken_11(RuntimeObject * value)
	{
		___mainThreadToken_11 = value;
		Il2CppCodeGenWriteBarrier((&___mainThreadToken_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINTHREADDISPATCHER_T3684499304_H
#ifndef PRESENTERBASE_T3151094974_H
#define PRESENTERBASE_T3151094974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UniRx.PresenterBase
struct  PresenterBase_t3151094974  : public PresenterBase_1_t4151662397
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESENTERBASE_T3151094974_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3000 = { sizeof (QuaternionReactiveProperty_t2539028965), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3001 = { sizeof (InspectorDisplayAttribute_t2165086330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3001[2] = 
{
	InspectorDisplayAttribute_t2165086330::get_offset_of_U3CFieldNameU3Ek__BackingField_0(),
	InspectorDisplayAttribute_t2165086330::get_offset_of_U3CNotifyPropertyChangedU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3002 = { sizeof (MultilineReactivePropertyAttribute_t2064446684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3002[1] = 
{
	MultilineReactivePropertyAttribute_t2064446684::get_offset_of_U3CLinesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3003 = { sizeof (RangeReactivePropertyAttribute_t2057313354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3003[2] = 
{
	RangeReactivePropertyAttribute_t2057313354::get_offset_of_U3CMinU3Ek__BackingField_0(),
	RangeReactivePropertyAttribute_t2057313354::get_offset_of_U3CMaxU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3004 = { sizeof (LazyTask_t1721948414), -1, sizeof(LazyTask_t1721948414_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3004[3] = 
{
	LazyTask_t1721948414::get_offset_of_U3CStatusU3Ek__BackingField_0(),
	LazyTask_t1721948414::get_offset_of_cancellation_1(),
	LazyTask_t1721948414_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3005 = { sizeof (TaskStatus_t4183509909)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3005[6] = 
{
	TaskStatus_t4183509909::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3006 = { sizeof (U3CWhenAllCoreU3Ec__Iterator0_t2684419830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3006[7] = 
{
	U3CWhenAllCoreU3Ec__Iterator0_t2684419830::get_offset_of_coroutines_0(),
	U3CWhenAllCoreU3Ec__Iterator0_t2684419830::get_offset_of_U24locvar0_1(),
	U3CWhenAllCoreU3Ec__Iterator0_t2684419830::get_offset_of_U24locvar1_2(),
	U3CWhenAllCoreU3Ec__Iterator0_t2684419830::get_offset_of_U3CitemU3E__1_3(),
	U3CWhenAllCoreU3Ec__Iterator0_t2684419830::get_offset_of_U24current_4(),
	U3CWhenAllCoreU3Ec__Iterator0_t2684419830::get_offset_of_U24disposing_5(),
	U3CWhenAllCoreU3Ec__Iterator0_t2684419830::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3007 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3007[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3008 = { sizeof (LazyTaskExtensions_t3183993102), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3009 = { sizeof (MainThreadDispatcher_t3684499304), -1, sizeof(MainThreadDispatcher_t3684499304_StaticFields), sizeof(MainThreadDispatcher_t3684499304_ThreadStaticFields) };
extern const int32_t g_FieldOffsetTable3009[16] = 
{
	MainThreadDispatcher_t3684499304_StaticFields::get_offset_of_cullingMode_2(),
	MainThreadDispatcher_t3684499304::get_offset_of_queueWorker_3(),
	MainThreadDispatcher_t3684499304::get_offset_of_unhandledExceptionCallback_4(),
	MainThreadDispatcher_t3684499304::get_offset_of_updateMicroCoroutine_5(),
	MainThreadDispatcher_t3684499304::get_offset_of_fixedUpdateMicroCoroutine_6(),
	MainThreadDispatcher_t3684499304::get_offset_of_endOfFrameMicroCoroutine_7(),
	MainThreadDispatcher_t3684499304_StaticFields::get_offset_of_instance_8(),
	MainThreadDispatcher_t3684499304_StaticFields::get_offset_of_initialized_9(),
	MainThreadDispatcher_t3684499304_StaticFields::get_offset_of_isQuitting_10(),
	THREAD_STATIC_FIELD_OFFSET,
	MainThreadDispatcher_t3684499304::get_offset_of_update_12(),
	MainThreadDispatcher_t3684499304::get_offset_of_lateUpdate_13(),
	MainThreadDispatcher_t3684499304::get_offset_of_onApplicationFocus_14(),
	MainThreadDispatcher_t3684499304::get_offset_of_onApplicationPause_15(),
	MainThreadDispatcher_t3684499304::get_offset_of_onApplicationQuit_16(),
	MainThreadDispatcher_t3684499304_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3010 = { sizeof (CullingMode_t3030417749)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3010[4] = 
{
	CullingMode_t3030417749::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3011 = { sizeof (U3CSendStartCoroutineU3Ec__AnonStorey3_t1416387401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3011[1] = 
{
	U3CSendStartCoroutineU3Ec__AnonStorey3_t1416387401::get_offset_of_routine_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3012 = { sizeof (U3CRunUpdateMicroCoroutineU3Ec__Iterator0_t3229616799), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3012[4] = 
{
	U3CRunUpdateMicroCoroutineU3Ec__Iterator0_t3229616799::get_offset_of_U24this_0(),
	U3CRunUpdateMicroCoroutineU3Ec__Iterator0_t3229616799::get_offset_of_U24current_1(),
	U3CRunUpdateMicroCoroutineU3Ec__Iterator0_t3229616799::get_offset_of_U24disposing_2(),
	U3CRunUpdateMicroCoroutineU3Ec__Iterator0_t3229616799::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3013 = { sizeof (U3CRunFixedUpdateMicroCoroutineU3Ec__Iterator1_t4089570234), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3013[4] = 
{
	U3CRunFixedUpdateMicroCoroutineU3Ec__Iterator1_t4089570234::get_offset_of_U24this_0(),
	U3CRunFixedUpdateMicroCoroutineU3Ec__Iterator1_t4089570234::get_offset_of_U24current_1(),
	U3CRunFixedUpdateMicroCoroutineU3Ec__Iterator1_t4089570234::get_offset_of_U24disposing_2(),
	U3CRunFixedUpdateMicroCoroutineU3Ec__Iterator1_t4089570234::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3014 = { sizeof (U3CRunEndOfFrameMicroCoroutineU3Ec__Iterator2_t4169616078), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3014[4] = 
{
	U3CRunEndOfFrameMicroCoroutineU3Ec__Iterator2_t4169616078::get_offset_of_U24this_0(),
	U3CRunEndOfFrameMicroCoroutineU3Ec__Iterator2_t4169616078::get_offset_of_U24current_1(),
	U3CRunEndOfFrameMicroCoroutineU3Ec__Iterator2_t4169616078::get_offset_of_U24disposing_2(),
	U3CRunEndOfFrameMicroCoroutineU3Ec__Iterator2_t4169616078::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3015 = { sizeof (FrameCountType_t3331626185)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3015[4] = 
{
	FrameCountType_t3331626185::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3016 = { sizeof (MainThreadDispatchType_t3527556734)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3016[7] = 
{
	MainThreadDispatchType_t3527556734::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3017 = { sizeof (FrameCountTypeExtensions_t2621917262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3018 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3019 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3019[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3020 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3020[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3021 = { sizeof (ObservableWWW_t1879783302), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3022 = { sizeof (U3CGetU3Ec__AnonStorey4_t1801188099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3022[3] = 
{
	U3CGetU3Ec__AnonStorey4_t1801188099::get_offset_of_url_0(),
	U3CGetU3Ec__AnonStorey4_t1801188099::get_offset_of_headers_1(),
	U3CGetU3Ec__AnonStorey4_t1801188099::get_offset_of_progress_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3023 = { sizeof (U3CGetAndGetBytesU3Ec__AnonStorey5_t2468502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3023[3] = 
{
	U3CGetAndGetBytesU3Ec__AnonStorey5_t2468502::get_offset_of_url_0(),
	U3CGetAndGetBytesU3Ec__AnonStorey5_t2468502::get_offset_of_headers_1(),
	U3CGetAndGetBytesU3Ec__AnonStorey5_t2468502::get_offset_of_progress_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3024 = { sizeof (U3CGetWWWU3Ec__AnonStorey6_t794373360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3024[3] = 
{
	U3CGetWWWU3Ec__AnonStorey6_t794373360::get_offset_of_url_0(),
	U3CGetWWWU3Ec__AnonStorey6_t794373360::get_offset_of_headers_1(),
	U3CGetWWWU3Ec__AnonStorey6_t794373360::get_offset_of_progress_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3025 = { sizeof (U3CPostU3Ec__AnonStorey7_t869725411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3025[3] = 
{
	U3CPostU3Ec__AnonStorey7_t869725411::get_offset_of_url_0(),
	U3CPostU3Ec__AnonStorey7_t869725411::get_offset_of_postData_1(),
	U3CPostU3Ec__AnonStorey7_t869725411::get_offset_of_progress_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3026 = { sizeof (U3CPostU3Ec__AnonStorey8_t1207432419), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3026[4] = 
{
	U3CPostU3Ec__AnonStorey8_t1207432419::get_offset_of_url_0(),
	U3CPostU3Ec__AnonStorey8_t1207432419::get_offset_of_postData_1(),
	U3CPostU3Ec__AnonStorey8_t1207432419::get_offset_of_headers_2(),
	U3CPostU3Ec__AnonStorey8_t1207432419::get_offset_of_progress_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3027 = { sizeof (U3CPostU3Ec__AnonStorey9_t3546084579), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3027[3] = 
{
	U3CPostU3Ec__AnonStorey9_t3546084579::get_offset_of_url_0(),
	U3CPostU3Ec__AnonStorey9_t3546084579::get_offset_of_content_1(),
	U3CPostU3Ec__AnonStorey9_t3546084579::get_offset_of_progress_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3028 = { sizeof (U3CPostU3Ec__AnonStoreyA_t1430582499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3028[5] = 
{
	U3CPostU3Ec__AnonStoreyA_t1430582499::get_offset_of_url_0(),
	U3CPostU3Ec__AnonStoreyA_t1430582499::get_offset_of_content_1(),
	U3CPostU3Ec__AnonStoreyA_t1430582499::get_offset_of_contentHeaders_2(),
	U3CPostU3Ec__AnonStoreyA_t1430582499::get_offset_of_headers_3(),
	U3CPostU3Ec__AnonStoreyA_t1430582499::get_offset_of_progress_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3029 = { sizeof (U3CPostAndGetBytesU3Ec__AnonStoreyB_t234290609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3029[3] = 
{
	U3CPostAndGetBytesU3Ec__AnonStoreyB_t234290609::get_offset_of_url_0(),
	U3CPostAndGetBytesU3Ec__AnonStoreyB_t234290609::get_offset_of_postData_1(),
	U3CPostAndGetBytesU3Ec__AnonStoreyB_t234290609::get_offset_of_progress_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3030 = { sizeof (U3CPostAndGetBytesU3Ec__AnonStoreyC_t2963173964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3030[4] = 
{
	U3CPostAndGetBytesU3Ec__AnonStoreyC_t2963173964::get_offset_of_url_0(),
	U3CPostAndGetBytesU3Ec__AnonStoreyC_t2963173964::get_offset_of_postData_1(),
	U3CPostAndGetBytesU3Ec__AnonStoreyC_t2963173964::get_offset_of_headers_2(),
	U3CPostAndGetBytesU3Ec__AnonStoreyC_t2963173964::get_offset_of_progress_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3031 = { sizeof (U3CPostAndGetBytesU3Ec__AnonStoreyD_t1040859663), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3031[3] = 
{
	U3CPostAndGetBytesU3Ec__AnonStoreyD_t1040859663::get_offset_of_url_0(),
	U3CPostAndGetBytesU3Ec__AnonStoreyD_t1040859663::get_offset_of_content_1(),
	U3CPostAndGetBytesU3Ec__AnonStoreyD_t1040859663::get_offset_of_progress_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3032 = { sizeof (U3CPostAndGetBytesU3Ec__AnonStoreyE_t3769743018), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3032[5] = 
{
	U3CPostAndGetBytesU3Ec__AnonStoreyE_t3769743018::get_offset_of_url_0(),
	U3CPostAndGetBytesU3Ec__AnonStoreyE_t3769743018::get_offset_of_content_1(),
	U3CPostAndGetBytesU3Ec__AnonStoreyE_t3769743018::get_offset_of_contentHeaders_2(),
	U3CPostAndGetBytesU3Ec__AnonStoreyE_t3769743018::get_offset_of_headers_3(),
	U3CPostAndGetBytesU3Ec__AnonStoreyE_t3769743018::get_offset_of_progress_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3033 = { sizeof (U3CPostWWWU3Ec__AnonStoreyF_t1147649635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3033[3] = 
{
	U3CPostWWWU3Ec__AnonStoreyF_t1147649635::get_offset_of_url_0(),
	U3CPostWWWU3Ec__AnonStoreyF_t1147649635::get_offset_of_postData_1(),
	U3CPostWWWU3Ec__AnonStoreyF_t1147649635::get_offset_of_progress_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3034 = { sizeof (U3CPostWWWU3Ec__AnonStorey10_t3781990988), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3034[4] = 
{
	U3CPostWWWU3Ec__AnonStorey10_t3781990988::get_offset_of_url_0(),
	U3CPostWWWU3Ec__AnonStorey10_t3781990988::get_offset_of_postData_1(),
	U3CPostWWWU3Ec__AnonStorey10_t3781990988::get_offset_of_headers_2(),
	U3CPostWWWU3Ec__AnonStorey10_t3781990988::get_offset_of_progress_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3035 = { sizeof (U3CPostWWWU3Ec__AnonStorey11_t1825675852), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3035[3] = 
{
	U3CPostWWWU3Ec__AnonStorey11_t1825675852::get_offset_of_url_0(),
	U3CPostWWWU3Ec__AnonStorey11_t1825675852::get_offset_of_content_1(),
	U3CPostWWWU3Ec__AnonStorey11_t1825675852::get_offset_of_progress_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3036 = { sizeof (U3CPostWWWU3Ec__AnonStorey12_t3399653964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3036[5] = 
{
	U3CPostWWWU3Ec__AnonStorey12_t3399653964::get_offset_of_url_0(),
	U3CPostWWWU3Ec__AnonStorey12_t3399653964::get_offset_of_content_1(),
	U3CPostWWWU3Ec__AnonStorey12_t3399653964::get_offset_of_contentHeaders_2(),
	U3CPostWWWU3Ec__AnonStorey12_t3399653964::get_offset_of_headers_3(),
	U3CPostWWWU3Ec__AnonStorey12_t3399653964::get_offset_of_progress_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3037 = { sizeof (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey13_t2957179115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3037[3] = 
{
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey13_t2957179115::get_offset_of_url_0(),
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey13_t2957179115::get_offset_of_version_1(),
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey13_t2957179115::get_offset_of_progress_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3038 = { sizeof (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey14_t3766483179), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3038[4] = 
{
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey14_t3766483179::get_offset_of_url_0(),
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey14_t3766483179::get_offset_of_version_1(),
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey14_t3766483179::get_offset_of_crc_2(),
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey14_t3766483179::get_offset_of_progress_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3039 = { sizeof (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey15_t1810168043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3039[3] = 
{
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey15_t1810168043::get_offset_of_url_0(),
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey15_t1810168043::get_offset_of_hash128_1(),
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey15_t1810168043::get_offset_of_progress_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3040 = { sizeof (U3CLoadFromCacheOrDownloadU3Ec__AnonStorey16_t4148820203), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3040[4] = 
{
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey16_t4148820203::get_offset_of_url_0(),
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey16_t4148820203::get_offset_of_hash128_1(),
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey16_t4148820203::get_offset_of_crc_2(),
	U3CLoadFromCacheOrDownloadU3Ec__AnonStorey16_t4148820203::get_offset_of_progress_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3041 = { sizeof (U3CFetchU3Ec__Iterator0_t2218155811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3041[8] = 
{
	U3CFetchU3Ec__Iterator0_t2218155811::get_offset_of_www_0(),
	U3CFetchU3Ec__Iterator0_t2218155811::get_offset_of_U24locvar0_1(),
	U3CFetchU3Ec__Iterator0_t2218155811::get_offset_of_reportProgress_2(),
	U3CFetchU3Ec__Iterator0_t2218155811::get_offset_of_cancel_3(),
	U3CFetchU3Ec__Iterator0_t2218155811::get_offset_of_observer_4(),
	U3CFetchU3Ec__Iterator0_t2218155811::get_offset_of_U24current_5(),
	U3CFetchU3Ec__Iterator0_t2218155811::get_offset_of_U24disposing_6(),
	U3CFetchU3Ec__Iterator0_t2218155811::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3042 = { sizeof (U3CFetchTextU3Ec__Iterator1_t3169890160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3042[8] = 
{
	U3CFetchTextU3Ec__Iterator1_t3169890160::get_offset_of_www_0(),
	U3CFetchTextU3Ec__Iterator1_t3169890160::get_offset_of_U24locvar0_1(),
	U3CFetchTextU3Ec__Iterator1_t3169890160::get_offset_of_reportProgress_2(),
	U3CFetchTextU3Ec__Iterator1_t3169890160::get_offset_of_cancel_3(),
	U3CFetchTextU3Ec__Iterator1_t3169890160::get_offset_of_observer_4(),
	U3CFetchTextU3Ec__Iterator1_t3169890160::get_offset_of_U24current_5(),
	U3CFetchTextU3Ec__Iterator1_t3169890160::get_offset_of_U24disposing_6(),
	U3CFetchTextU3Ec__Iterator1_t3169890160::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3043 = { sizeof (U3CFetchBytesU3Ec__Iterator2_t135672608), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3043[8] = 
{
	U3CFetchBytesU3Ec__Iterator2_t135672608::get_offset_of_www_0(),
	U3CFetchBytesU3Ec__Iterator2_t135672608::get_offset_of_U24locvar0_1(),
	U3CFetchBytesU3Ec__Iterator2_t135672608::get_offset_of_reportProgress_2(),
	U3CFetchBytesU3Ec__Iterator2_t135672608::get_offset_of_cancel_3(),
	U3CFetchBytesU3Ec__Iterator2_t135672608::get_offset_of_observer_4(),
	U3CFetchBytesU3Ec__Iterator2_t135672608::get_offset_of_U24current_5(),
	U3CFetchBytesU3Ec__Iterator2_t135672608::get_offset_of_U24disposing_6(),
	U3CFetchBytesU3Ec__Iterator2_t135672608::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3044 = { sizeof (U3CFetchAssetBundleU3Ec__Iterator3_t3693207334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3044[8] = 
{
	U3CFetchAssetBundleU3Ec__Iterator3_t3693207334::get_offset_of_www_0(),
	U3CFetchAssetBundleU3Ec__Iterator3_t3693207334::get_offset_of_U24locvar0_1(),
	U3CFetchAssetBundleU3Ec__Iterator3_t3693207334::get_offset_of_reportProgress_2(),
	U3CFetchAssetBundleU3Ec__Iterator3_t3693207334::get_offset_of_cancel_3(),
	U3CFetchAssetBundleU3Ec__Iterator3_t3693207334::get_offset_of_observer_4(),
	U3CFetchAssetBundleU3Ec__Iterator3_t3693207334::get_offset_of_U24current_5(),
	U3CFetchAssetBundleU3Ec__Iterator3_t3693207334::get_offset_of_U24disposing_6(),
	U3CFetchAssetBundleU3Ec__Iterator3_t3693207334::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3045 = { sizeof (WWWErrorException_t1771212499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3045[6] = 
{
	WWWErrorException_t1771212499::get_offset_of_U3CRawErrorMessageU3Ek__BackingField_11(),
	WWWErrorException_t1771212499::get_offset_of_U3CHasResponseU3Ek__BackingField_12(),
	WWWErrorException_t1771212499::get_offset_of_U3CTextU3Ek__BackingField_13(),
	WWWErrorException_t1771212499::get_offset_of_U3CStatusCodeU3Ek__BackingField_14(),
	WWWErrorException_t1771212499::get_offset_of_U3CResponseHeadersU3Ek__BackingField_15(),
	WWWErrorException_t1771212499::get_offset_of_U3CWWWU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3046 = { sizeof (ObserveExtensions_t3076931129), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3047 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3047[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3048 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3048[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3049 = { sizeof (U3CEmptyEnumeratorU3Ec__Iterator0_t3471758210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3049[3] = 
{
	U3CEmptyEnumeratorU3Ec__Iterator0_t3471758210::get_offset_of_U24current_0(),
	U3CEmptyEnumeratorU3Ec__Iterator0_t3471758210::get_offset_of_U24disposing_1(),
	U3CEmptyEnumeratorU3Ec__Iterator0_t3471758210::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3050 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3050[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3051 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3051[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3052 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3052[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3053 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3053[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3054 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3054[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3055 = { sizeof (BatchFrameObservable_t2539759103), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3055[3] = 
{
	BatchFrameObservable_t2539759103::get_offset_of_source_1(),
	BatchFrameObservable_t2539759103::get_offset_of_frameCount_2(),
	BatchFrameObservable_t2539759103::get_offset_of_frameCountType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3056 = { sizeof (BatchFrame_t3337927563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3056[6] = 
{
	BatchFrame_t3337927563::get_offset_of_parent_2(),
	BatchFrame_t3337927563::get_offset_of_gate_3(),
	BatchFrame_t3337927563::get_offset_of_cancellationToken_4(),
	BatchFrame_t3337927563::get_offset_of_timer_5(),
	BatchFrame_t3337927563::get_offset_of_isRunning_6(),
	BatchFrame_t3337927563::get_offset_of_isCompleted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3057 = { sizeof (ReusableEnumerator_t911091657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3057[2] = 
{
	ReusableEnumerator_t911091657::get_offset_of_parent_0(),
	ReusableEnumerator_t911091657::get_offset_of_currentFrame_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3058 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3058[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3059 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3059[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3060 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3060[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3061 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3061[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3062 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3062[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3063 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3063[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3064 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3064[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3065 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3065[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3066 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3066[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3067 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3067[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3068 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3069 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3069[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3070 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3071 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3071[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3072 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3072[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3073 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3073[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3074 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3074[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3075 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3075[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3076 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3076[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3077 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3077[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3078 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3078[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3079 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3079[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3080 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3080[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3081 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3081[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3082 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3082[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3083 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3083[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3084 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3084[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3085 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3085[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3086 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3086[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3087 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3088 = { sizeof (PresenterBase_t3151094974), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3089 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3089[10] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3090 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3090[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3091 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3091[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3092 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3092[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3093 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3093[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3094 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3095 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3096 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3096[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3097 = { sizeof (ReactiveCollectionExtensions_t1881372181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3098 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3099 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
