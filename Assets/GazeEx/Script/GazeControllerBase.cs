﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GazeControllerBase : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		
	public abstract void OnWaitDone ();
	public abstract void OnPointEnter ();
	public abstract void OnPointExit ();
}
