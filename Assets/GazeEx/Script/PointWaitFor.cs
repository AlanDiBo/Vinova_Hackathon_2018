﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PointWaitFor : MonoBehaviour {

	public static float timeDuration { get; set; }
	public float duration=0.01f;
	// Use this for in  itialization
	bool isFill = true;


	//当视选进入的时候触发
	public void FillIn()
	{
		if(isFill)
		{
			gameObject.SetActive(true);
			isFill = false;
			StartCoroutine(WaitForit());
		}
	}

	public void FillOut()
	{
		gameObject.SetActive(false);
		isFill = true;
		Debug.Log("111");
		this.gameObject.GetComponent<Image>().fillAmount = 0f;

	}

	//这里设置一个协程来控制UI 的显示
	IEnumerator  WaitForit()
	{
		for(float i=0;i<=100;i++)
		{
			this.gameObject.GetComponent<Image>().fillAmount =0.01f*i;
			yield return new WaitForSeconds(duration);
			//Debug.Log("3333");
		}
		timeDuration = this.gameObject.GetComponent<Image>().fillAmount;
		isFill = true;
	}

	public static bool IsGo()
	{
		if (timeDuration == 1)
		{
			return true;
		}
		else
			return false;


	}

}


