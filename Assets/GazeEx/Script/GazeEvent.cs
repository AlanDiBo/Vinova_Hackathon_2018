﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class GazeEvent : MonoBehaviour,IPointerEnterHandler,IPointerExitHandler {


//	public delegate void SetWaitEvent();
//	[SerializeField]
//	public event SetWaitEvent OnSetWaitEvent;
	public delegate void GazeEnter ();
	public static GazeEnter OnGazeEnter;

	public delegate void GazeExit ();

	public static GazeExit OnGazeExit;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void OnPointerEnter(PointerEventData eventData)
	{
		print (OnGazeEnter);
		if (OnGazeEnter != null) {
			ReticleController.OnRetEnter ();		/*
			EventTrigger etg = this.GetComponent<EventTrigger> ();
			//etg.triggers.*/
			GazeControllerBase GazeControl = this.GetComponent<GazeControllerBase> ();
			if (GazeControl != null) {
				GazeObject.OnWaitEvent = GazeControl.OnWaitDone;
				OnGazeEnter ();
			}
		}	
	}
	public void OnPointerExit(PointerEventData eventData)
	{
		if (OnGazeExit != null) {
			ReticleController.OnRetExit ();
			GazeControllerBase GazeControl = this.GetComponent<GazeControllerBase> ();
			if (GazeControl != null) {
				OnGazeExit ();
			}
		}	}


}
