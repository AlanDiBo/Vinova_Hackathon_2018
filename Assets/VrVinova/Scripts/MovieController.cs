﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MovieController : MonoBehaviour {
	private const int MAX_SIZE_OF_MOVIE_EACH_PAGE = 6;
	private static int oldPages = 0;
	private static int curPages = 0;
	public static string[] videoUrl;
	private static bool isCallbackSuccess = false;
	public GameObject processLoading;
	public GameObject[] itemMovie;
	public GameObject[] itemButton;
	private static List<MovieModel> movieList;
	public string SceneName;
	public static string nextPageToken;
	public static string prevPageToken;
	public static bool isPrev = false;
	public static string keyword = "Movie";

	void Awake () {
		DontDestroyOnLoad (transform.gameObject);
	}

	// Use this for initialization
	void Start () {
		videoUrl = new string[6];

		movieList = new List<MovieModel>();
		foreach(GameObject item in itemMovie){
			item.SetActive (false);
		}
//		foreach (GameObject item in itemButton) {
//			item.SetActive (false);
//		}
		processLoading.SetActive(true);
		StartCoroutine(getVideos (keyword, prevPageToken));
		StartCoroutine(SetUpData ());
		//end
	}

	// Update is called once per frame
	void Update () {
		if (oldPages != curPages) {
			processLoading.SetActive(true);
			oldPages = curPages;
			if (isPrev) {
				StartCoroutine(getVideos (keyword,prevPageToken));
			} else {
				StartCoroutine(getVideos (keyword,nextPageToken));
			}
		}
		if (isCallbackSuccess) {
			StartCoroutine(SetUpData ());
			processLoading.SetActive(false);
			isCallbackSuccess = false;
		}
	}

	public void OnDataCallbackSuccess(string jsonResult){
		MovieResult result = JsonUtility.FromJson<MovieResult> (jsonResult);
		movieList = result.movies;
		nextPageToken = result.nextPageToken;
		prevPageToken = result.prevPageToken;
		isCallbackSuccess = true;
//		SetUpData ();
	}

	public void onVoiceSuccess(string data){
		keyword = data;
		StartCoroutine(getVideos (keyword,prevPageToken));
	}

	IEnumerator SetUpData(){
		if (movieList.Count > 0) {
			for (int i = 0; i < MAX_SIZE_OF_MOVIE_EACH_PAGE; i++) {
				if (movieList [i] != null) {
					itemMovie [i].SetActive (true);
					videoUrl [i] = movieList[i].videoUrl;
					itemMovie [i].transform.GetChild (1).GetChild (0).GetComponent<TextMeshProUGUI>().text = movieList[i].snippet.title;
					itemMovie [i].transform.GetChild (1).GetChild (1).GetComponent<TextMeshProUGUI>().text = movieList[i].snippet.description;
					using (WWW www = new WWW(movieList[i].snippet.thumbnails.high.url))
					{
						// Wait for download to complete
						yield return www;
						itemMovie [i].transform.GetChild (0).GetComponent<RawImage>().texture = www.texture;
					}
				} else {
					itemMovie [i].SetActive(false);
				}
			}
		}
	}
	public static void OnNextPage(){
		curPages += 1;
		isPrev = false;
	}
	public static void OnPreviousPage(){
		curPages -= 1;
		isPrev = true;

	}
	public static void OnLoadMovie(int index){
		PlayerPrefs.SetString ("video_url", videoUrl[index]);
		SceneManager.LoadScene("VROutdoorCinemaMenuGearVR");
	}

	IEnumerator getVideos (string keyword, string pageToken){
		string url = "https://vinova-hackathon.herokuapp.com/videos/search?q=" + keyword + "&pageToken=" + pageToken;
		WWW www = new WWW (url);
		yield return www;
		string data = www.text;
		OnDataCallbackSuccess (data);
	}

	//Unity => IOS
//	[DllImport ("__Internal")]
//	private static extern void _FetchMovieCMethod (int page);
}