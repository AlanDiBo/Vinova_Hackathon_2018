﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CinemaController : GazeControllerBase {

	public string SceneName;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public override void OnWaitDone ()
	{
		SceneManager.LoadScene(SceneName);
	}

	public override void OnPointEnter ()
	{
	}

	public override void OnPointExit ()
	{
	}
}