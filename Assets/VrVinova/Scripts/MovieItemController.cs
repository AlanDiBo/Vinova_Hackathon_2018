﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovieItemController : GazeControllerBase {
	private const int MOVIE_BUTTON_1 = 0;
	private const int MOVIE_BUTTON_2 = 1;
	private const int MOVIE_BUTTON_3 = 2;
	private const int MOVIE_BUTTON_4 = 3;
	private const int MOVIE_BUTTON_5 = 4;
	private const int MOVIE_BUTTON_6 = 5;
	private const int NEXT_BUTTON = 6;
	private const int PREVIOUS_BUTTON = 7;
	private const int VOICE_BUTTON = 8;
	public int type = -1;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public override void OnWaitDone ()
	{
		Debug.Log ("Alan OnWaitDoneeeeeeeeeeeee");
		switch (type) {
		case MOVIE_BUTTON_1:
		case MOVIE_BUTTON_2:
		case MOVIE_BUTTON_3:
		case MOVIE_BUTTON_4:
		case MOVIE_BUTTON_5:
		case MOVIE_BUTTON_6:
			MovieController.OnLoadMovie (type);
			break;
		case NEXT_BUTTON:
			MovieController.OnNextPage ();
			break;
		case PREVIOUS_BUTTON:
			MovieController.OnPreviousPage ();
			break;
		case VOICE_BUTTON:
			VoiceController.onVoiceCommand ();
			break;
		}
	
	}

	public override void OnPointEnter ()
	{
	}

	public override void OnPointExit ()
	{
	}
}
