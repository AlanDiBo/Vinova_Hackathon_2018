﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class MovieResult
{
	public List<MovieModel> movies;
	public string nextPageToken;
	public string prevPageToken;
}
