﻿using System;

[Serializable]
public class HighThumbnailItem
{
	public string url;
	public int width;
	public int height;
}
