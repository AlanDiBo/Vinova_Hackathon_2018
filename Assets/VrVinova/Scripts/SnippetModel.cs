﻿using System;

[Serializable]
public class SnippetModel
{
	public string publishedAt;
	public string channelId;
	public string title;
	public string description;
	public ThumbnailsModel thumbnails;
	public string channelTitle;
	public string liveBroadcastContent;
}