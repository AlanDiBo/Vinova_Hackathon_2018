﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class StreamingVideo : MonoBehaviour {

	public MeshRenderer tvOnScreen;

	private VideoPlayer videoPlayer;
	private VideoSource videoSource;
	private AudioSource audioSource;

	private string videoId = "";

	// Use this for initialization
	void Start () {
		videoId = PlayerPrefs.GetString ("video_url");
		Application.runInBackground = true;
//		leftTV = (GameObject) Instantiate(tvOnLeft);
//		rightTV = (GameObject) Instantiate(tvOnRight);
		StartCoroutine(playVideo(videoId));
	}

	IEnumerator playVideo(string url)
	{

		//Add VideoPlayer to the GameObject
		videoPlayer = gameObject.AddComponent<VideoPlayer>();

		//Add AudioSource
		audioSource = gameObject.AddComponent<AudioSource>();
//		if (AudioController.instance.getAudioSource () == null) {
//			AudioSource audioSource = gameObject.AddComponent<AudioSource> ();
//			AudioController.instance.setAudioSource (audioSource);
//		}
		//Disable Play on Awake for both Video and Audio
		videoPlayer.playOnAwake = false;
		audioSource.playOnAwake = false;
		audioSource.Pause();

		//We want to play from video clip not from url

		videoPlayer.source = VideoSource.VideoClip;

		// Vide clip from Url
		videoPlayer.source = VideoSource.Url;
		videoPlayer.url = url;

		videoPlayer.controlledAudioTrackCount = 1;
		//Set Audio Output to AudioSource
		videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

		//Assign the Audio from Video to AudioSource to be played
		videoPlayer.EnableAudioTrack(0, true);
		videoPlayer.SetTargetAudioSource(0, audioSource);

		//Set video To Play then prepare Audio to prevent Buffering
//		videoPlayer.clip = videoToPlay;
		videoPlayer.Prepare();

		//Wait until video is prepared
		while (!videoPlayer.isPrepared)
		{
			yield return null;
		}

		Debug.Log("Done Preparing Video");

		//Assign the Texture from Video to RawImage to be displayed
//		foreach(MeshRenderer item in tvOnScreen){
//			item.material.SetTexture("Screen", videoPlayer.texture);
//		}
		tvOnScreen.material.SetTexture("Screen", videoPlayer.texture);

		//Play Video
		videoPlayer.Play();

		Debug.Log("Playing Video");
		while (videoPlayer.isPlaying)
		{
			Debug.LogWarning("Video Time: " + Mathf.FloorToInt((float)videoPlayer.time));
			if((float)videoPlayer.time >= 0.5f){
				//Play Sound
				audioSource.Play();
			}
			yield return null;
		}

		Debug.Log("Done Playing Video");
	}

	// Update is called once per frame
	void Update () {
		
	}
}
